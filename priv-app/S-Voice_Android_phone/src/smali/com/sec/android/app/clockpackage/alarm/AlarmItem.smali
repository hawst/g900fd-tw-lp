.class public final Lcom/sec/android/app/clockpackage/alarm/AlarmItem;
.super Ljava/lang/Object;
.source "AlarmItem.java"


# static fields
.field private static final DEBUG:Z = true

.field public static final FLAG_NOTIFICATION_DAILY_BRIEFING:I = 0x10

.field public static final FLAG_NOTIFICATION_SNOOZE:I = 0x100

.field public static final FLAG_NOTIFICATION_SUBDUE:I = 0x1

.field private static final MINUTE_TO_MILLI:I = 0xea60

.field private static final TAG:Ljava/lang/String; = "AlarmItem"


# instance fields
.field public activate:I

.field public alarmAlertTime:J

.field public alarmName:Ljava/lang/String;

.field public alarmSoundTone:I

.field public alarmSoundType:I

.field public alarmTime:I

.field public alarmVolume:I

.field public createTime:J

.field public dailyBriefing:I

.field public id:I

.field public notificationType:I

.field public repeatType:I

.field public snoozeActivate:Z

.field public snoozeDoneCount:I

.field public snoozeDuration:I

.field public snoozeRepeat:I

.field public soundUri:Ljava/lang/String;

.field public subdueActivate:Z

.field public subdueDuration:I

.field public subdueTone:I

.field public subdueUri:I


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const-wide/16 v3, -0x1

    const/4 v2, 0x1

    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->id:I

    .line 21
    iput v1, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 22
    iput-wide v3, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->createTime:J

    .line 23
    iput-wide v3, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 24
    iput v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmTime:I

    .line 25
    iput v1, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->repeatType:I

    .line 26
    iput v1, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->notificationType:I

    .line 27
    iput-boolean v1, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeActivate:Z

    .line 28
    iput v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDuration:I

    .line 29
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeRepeat:I

    .line 30
    iput v1, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 31
    iput v1, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->dailyBriefing:I

    .line 32
    iput-boolean v1, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueActivate:Z

    .line 33
    iput v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    .line 34
    iput v1, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueTone:I

    .line 35
    iput v1, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmSoundType:I

    .line 36
    iput v1, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmSoundTone:I

    .line 37
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmVolume:I

    .line 39
    iput v1, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueUri:I

    .line 40
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->soundUri:Ljava/lang/String;

    .line 41
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmName:Ljava/lang/String;

    .line 11
    return-void
.end method

.method private calculateAlertTime()V
    .locals 22

    .prologue
    .line 313
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 314
    .local v5, "c":Ljava/util/Calendar;
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    .line 316
    .local v6, "currentMillis":J
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    packed-switch v18, :pswitch_data_0

    .line 1173
    :goto_0
    :pswitch_0
    return-void

    .line 319
    :pswitch_1
    const-wide/16 v18, -0x1

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 320
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    goto :goto_0

    .line 325
    :pswitch_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueActivate:Z

    move/from16 v18, v0

    if-eqz v18, :cond_32

    .line 327
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeActivate:Z

    move/from16 v18, v0

    if-eqz v18, :cond_27

    .line 329
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    cmp-long v18, v6, v18

    if-lez v18, :cond_9

    .line 333
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 334
    .local v10, "newAlarmTime":J
    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .local v9, "i":I
    :goto_1
    sget-object v18, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_SNOOZE_COUNT_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeRepeat:I

    move/from16 v19, v0

    aget v18, v18, v19

    move/from16 v0, v18

    if-lt v9, v0, :cond_0

    .line 357
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 359
    const/16 v18, 0x3

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 360
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 361
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    .line 362
    sget-object v20, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    move/from16 v21, v0

    aget v20, v20, v21

    const v21, 0xea60

    mul-int v20, v20, v21

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 361
    sub-long v12, v18, v20

    .line 363
    .local v12, "nextSmartAlert":J
    :goto_2
    cmp-long v18, v6, v12

    if-gtz v18, :cond_4

    .line 369
    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 372
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    if-nez v18, :cond_5

    .line 373
    const-string/jumbo v18, "AlarmItem"

    .line 374
    const-string/jumbo v19, "----------------- ALARM_INACTIVE -----------------"

    .line 373
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    const-string/jumbo v18, "AlarmItem"

    .line 376
    const-string/jumbo v19, "next alert : snooze active, subdue active"

    .line 375
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    const-string/jumbo v18, "AlarmItem"

    const-string/jumbo v19, "alarm done"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 335
    .end local v12    # "nextSmartAlert":J
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 336
    sget-object v18, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDuration:I

    move/from16 v19, v0

    aget v18, v18, v19

    const v19, 0xea60

    mul-int v18, v18, v19

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    add-long v10, v10, v18

    .line 337
    cmp-long v18, v10, v6

    if-lez v18, :cond_3

    .line 339
    const/16 v18, 0x2

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 340
    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 343
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_2

    .line 344
    const-string/jumbo v18, "AlarmItem"

    .line 345
    const-string/jumbo v19, "----------------- ALARM_ACTIVE -----------------"

    .line 344
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    :cond_1
    :goto_3
    const-string/jumbo v18, "AlarmItem"

    .line 350
    const-string/jumbo v19, "next alert : snooze active, subdue active"

    .line 349
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    const-string/jumbo v18, "AlarmItem"

    .line 352
    const-string/jumbo v19, "found some next snooze valid. set new snooze 1"

    .line 351
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 346
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_1

    .line 347
    const-string/jumbo v18, "AlarmItem"

    .line 348
    const-string/jumbo v19, "----------------- ALARM_SNOOZE -----------------"

    .line 347
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 334
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_1

    .line 364
    .restart local v12    # "nextSmartAlert":J
    :cond_4
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 365
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 366
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    .line 367
    sget-object v20, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    move/from16 v21, v0

    aget v20, v20, v21

    const v21, 0xea60

    mul-int v20, v20, v21

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 366
    sub-long v12, v18, v20

    goto/16 :goto_2

    .line 379
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_7

    .line 380
    const-string/jumbo v18, "AlarmItem"

    .line 381
    const-string/jumbo v19, "----------------- ALARM_ACTIVE -----------------"

    .line 380
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    :cond_6
    :goto_4
    const-string/jumbo v18, "AlarmItem"

    .line 389
    const-string/jumbo v19, "next alert : snooze active, subdue active"

    .line 388
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 382
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    .line 383
    const-string/jumbo v18, "AlarmItem"

    .line 384
    const-string/jumbo v19, "----------------- ALARM_SNOOZE -----------------"

    .line 383
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 385
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x3

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_6

    .line 386
    const-string/jumbo v18, "AlarmItem"

    .line 387
    const-string/jumbo v19, "----------------- ALARM_SUBDUE -----------------"

    .line 386
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 397
    .end local v9    # "i":I
    .end local v10    # "newAlarmTime":J
    .end local v12    # "nextSmartAlert":J
    :cond_9
    const-wide/16 v16, 0x0

    .line 398
    .local v16, "subdueMillis":J
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    move/from16 v18, v0

    if-nez v18, :cond_a

    .line 399
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    .line 400
    sget-object v20, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    move/from16 v21, v0

    aget v20, v20, v21

    const v21, 0xea60

    mul-int v20, v20, v21

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 399
    sub-long v16, v18, v20

    .line 402
    :cond_a
    cmp-long v18, v16, v6

    if-lez v18, :cond_d

    .line 404
    const/16 v18, 0x3

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 405
    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 408
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_c

    .line 409
    const-string/jumbo v18, "AlarmItem"

    .line 410
    const-string/jumbo v19, "----------------- ALARM_ACTIVE -----------------"

    .line 409
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    :cond_b
    :goto_5
    const-string/jumbo v18, "AlarmItem"

    .line 415
    const-string/jumbo v19, "next alert : snooze active, subdue active"

    .line 414
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    const-string/jumbo v18, "AlarmItem"

    const-string/jumbo v19, "set new smart alarm as selected"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 411
    :cond_c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_b

    .line 412
    const-string/jumbo v18, "AlarmItem"

    .line 413
    const-string/jumbo v19, "----------------- ALARM_SNOOZE -----------------"

    .line 412
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 420
    :cond_d
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    cmp-long v18, v6, v18

    if-lez v18, :cond_1f

    .line 421
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    move/from16 v18, v0

    sget-object v19, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_SNOOZE_COUNT_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeRepeat:I

    move/from16 v20, v0

    aget v19, v19, v20

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_17

    .line 423
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 424
    .restart local v10    # "newAlarmTime":J
    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .restart local v9    # "i":I
    :goto_6
    sget-object v18, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_SNOOZE_COUNT_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeRepeat:I

    move/from16 v19, v0

    aget v18, v18, v19

    move/from16 v0, v18

    if-le v9, v0, :cond_e

    .line 448
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 464
    const/16 v18, 0x3

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 465
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 466
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    .line 467
    sget-object v20, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    move/from16 v21, v0

    aget v20, v20, v21

    const v21, 0xea60

    mul-int v20, v20, v21

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 466
    sub-long v12, v18, v20

    .line 468
    .restart local v12    # "nextSmartAlert":J
    :goto_7
    cmp-long v18, v6, v12

    if-gtz v18, :cond_12

    .line 474
    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 477
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    if-nez v18, :cond_13

    .line 478
    const-string/jumbo v18, "AlarmItem"

    .line 479
    const-string/jumbo v19, "----------------- ALARM_INACTIVE -----------------"

    .line 478
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    const-string/jumbo v18, "AlarmItem"

    .line 481
    const-string/jumbo v19, "next alert : snooze active, subdue active"

    .line 480
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    const-string/jumbo v18, "AlarmItem"

    const-string/jumbo v19, "alarm done"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 425
    .end local v12    # "nextSmartAlert":J
    :cond_e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 426
    sget-object v18, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDuration:I

    move/from16 v19, v0

    aget v18, v18, v19

    const v19, 0xea60

    mul-int v18, v18, v19

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    add-long v10, v10, v18

    .line 427
    cmp-long v18, v10, v6

    if-lez v18, :cond_11

    .line 430
    const/16 v18, 0x2

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 431
    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 434
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_10

    .line 435
    const-string/jumbo v18, "AlarmItem"

    .line 436
    const-string/jumbo v19, "----------------- ALARM_ACTIVE -----------------"

    .line 435
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    :cond_f
    :goto_8
    const-string/jumbo v18, "AlarmItem"

    .line 441
    const-string/jumbo v19, "next alert : snooze active, subdue active"

    .line 440
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    const-string/jumbo v18, "AlarmItem"

    .line 443
    const-string/jumbo v19, "found some next snooze valid. set new snooze 2"

    .line 442
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 437
    :cond_10
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_f

    .line 438
    const-string/jumbo v18, "AlarmItem"

    .line 439
    const-string/jumbo v19, "----------------- ALARM_SNOOZE -----------------"

    .line 438
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 424
    :cond_11
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_6

    .line 469
    .restart local v12    # "nextSmartAlert":J
    :cond_12
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 470
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 471
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    .line 472
    sget-object v20, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    move/from16 v21, v0

    aget v20, v20, v21

    const v21, 0xea60

    mul-int v20, v20, v21

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 471
    sub-long v12, v18, v20

    goto/16 :goto_7

    .line 484
    :cond_13
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_15

    .line 485
    const-string/jumbo v18, "AlarmItem"

    .line 486
    const-string/jumbo v19, "----------------- ALARM_ACTIVE -----------------"

    .line 485
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    :cond_14
    :goto_9
    const-string/jumbo v18, "AlarmItem"

    .line 494
    const-string/jumbo v19, "next alert : snooze active, subdue active"

    .line 493
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 487
    :cond_15
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_16

    .line 488
    const-string/jumbo v18, "AlarmItem"

    .line 489
    const-string/jumbo v19, "----------------- ALARM_SNOOZE -----------------"

    .line 488
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 490
    :cond_16
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x3

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_14

    .line 491
    const-string/jumbo v18, "AlarmItem"

    .line 492
    const-string/jumbo v19, "----------------- ALARM_SUBDUE -----------------"

    .line 491
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 503
    .end local v9    # "i":I
    .end local v10    # "newAlarmTime":J
    .end local v12    # "nextSmartAlert":J
    :cond_17
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 506
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->repeatType:I

    move/from16 v18, v0

    and-int/lit8 v18, v18, 0xf

    add-int/lit8 v18, v18, -0x1

    if-nez v18, :cond_18

    .line 507
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getAlertDayCount()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_19

    .line 508
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 509
    const-wide/16 v18, -0x1

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 529
    :cond_18
    :goto_a
    const/16 v18, 0x3

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 530
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 531
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    .line 532
    sget-object v20, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    move/from16 v21, v0

    aget v20, v20, v21

    const v21, 0xea60

    mul-int v20, v20, v21

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 531
    sub-long v12, v18, v20

    .line 533
    .restart local v12    # "nextSmartAlert":J
    :goto_b
    cmp-long v18, v6, v12

    if-gtz v18, :cond_1a

    .line 539
    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 542
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    if-nez v18, :cond_1b

    .line 543
    const-string/jumbo v18, "AlarmItem"

    .line 544
    const-string/jumbo v19, "----------------- ALARM_INACTIVE -----------------"

    .line 543
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    const-string/jumbo v18, "AlarmItem"

    .line 546
    const-string/jumbo v19, "next alert : snooze active, subdue active"

    .line 545
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    const-string/jumbo v18, "AlarmItem"

    const-string/jumbo v19, "alarm done"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 511
    .end local v12    # "nextSmartAlert":J
    :cond_19
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->clearRepeatDay(Ljava/util/Calendar;)V

    goto :goto_a

    .line 534
    .restart local v12    # "nextSmartAlert":J
    :cond_1a
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 535
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 536
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    .line 537
    sget-object v20, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    move/from16 v21, v0

    aget v20, v20, v21

    const v21, 0xea60

    mul-int v20, v20, v21

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 536
    sub-long v12, v18, v20

    goto :goto_b

    .line 549
    :cond_1b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_1d

    .line 550
    const-string/jumbo v18, "AlarmItem"

    .line 551
    const-string/jumbo v19, "----------------- ALARM_ACTIVE -----------------"

    .line 550
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    :cond_1c
    :goto_c
    const-string/jumbo v18, "AlarmItem"

    .line 559
    const-string/jumbo v19, "next alert : snooze active, subdue active"

    .line 558
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 552
    :cond_1d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_1e

    .line 553
    const-string/jumbo v18, "AlarmItem"

    .line 554
    const-string/jumbo v19, "----------------- ALARM_SNOOZE -----------------"

    .line 553
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c

    .line 555
    :cond_1e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x3

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_1c

    .line 556
    const-string/jumbo v18, "AlarmItem"

    .line 557
    const-string/jumbo v19, "----------------- ALARM_SUBDUE -----------------"

    .line 556
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c

    .line 569
    .end local v12    # "nextSmartAlert":J
    :cond_1f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    move/from16 v18, v0

    sget-object v19, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_SNOOZE_COUNT_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeRepeat:I

    move/from16 v20, v0

    aget v19, v19, v20

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_23

    .line 571
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->repeatType:I

    move/from16 v18, v0

    and-int/lit8 v18, v18, 0xf

    add-int/lit8 v18, v18, -0x1

    if-nez v18, :cond_21

    .line 572
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getAlertDayCount()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_20

    .line 573
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 574
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 575
    const-wide/16 v18, -0x1

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 577
    const-string/jumbo v18, "AlarmItem"

    .line 578
    const-string/jumbo v19, "----------------- ALARM_INACTIVE -----------------"

    .line 577
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    const-string/jumbo v18, "AlarmItem"

    .line 580
    const-string/jumbo v19, "next alert : snooze active, subdue active"

    .line 579
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 581
    const-string/jumbo v18, "AlarmItem"

    .line 582
    const-string/jumbo v19, "snooze done. inactive alarm."

    .line 581
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 586
    :cond_20
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->clearRepeatDay(Ljava/util/Calendar;)V

    .line 608
    :cond_21
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 609
    const/16 v18, 0x3

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 610
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 611
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    .line 612
    sget-object v20, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    move/from16 v21, v0

    aget v20, v20, v21

    const v21, 0xea60

    mul-int v20, v20, v21

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 611
    sub-long v12, v18, v20

    .line 613
    .restart local v12    # "nextSmartAlert":J
    :goto_d
    cmp-long v18, v6, v12

    if-gtz v18, :cond_22

    .line 619
    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 622
    const-string/jumbo v18, "AlarmItem"

    .line 623
    const-string/jumbo v19, "----------------- ALARM_SUBDUE -----------------"

    .line 622
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 624
    const-string/jumbo v18, "AlarmItem"

    .line 625
    const-string/jumbo v19, "next alert : snooze active, subdue active"

    .line 624
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 626
    const-string/jumbo v18, "AlarmItem"

    .line 627
    const-string/jumbo v19, "snooze done. set next smart alarm."

    .line 626
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 614
    :cond_22
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 615
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 616
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    .line 617
    sget-object v20, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    move/from16 v21, v0

    aget v20, v20, v21

    const v21, 0xea60

    mul-int v20, v20, v21

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 616
    sub-long v12, v18, v20

    goto :goto_d

    .line 631
    .end local v12    # "nextSmartAlert":J
    :cond_23
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    move/from16 v18, v0

    if-lez v18, :cond_25

    .line 632
    const/16 v18, 0x2

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 637
    :goto_e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_26

    .line 638
    const-string/jumbo v18, "AlarmItem"

    .line 639
    const-string/jumbo v19, "----------------- ALARM_ACTIVE -----------------"

    .line 638
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 643
    :cond_24
    :goto_f
    const-string/jumbo v18, "AlarmItem"

    .line 644
    const-string/jumbo v19, "next alert : snooze active, subdue active"

    .line 643
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    const-string/jumbo v18, "AlarmItem"

    const-string/jumbo v19, "set new alarm as selected"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 634
    :cond_25
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    goto :goto_e

    .line 640
    :cond_26
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_24

    .line 641
    const-string/jumbo v18, "AlarmItem"

    .line 642
    const-string/jumbo v19, "----------------- ALARM_SNOOZE -----------------"

    .line 641
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_f

    .line 653
    .end local v16    # "subdueMillis":J
    :cond_27
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 654
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    .line 655
    sget-object v20, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    move/from16 v21, v0

    aget v20, v20, v21

    const v21, 0xea60

    mul-int v20, v20, v21

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 654
    sub-long v16, v18, v20

    .line 658
    .restart local v16    # "subdueMillis":J
    cmp-long v18, v16, v6

    if-lez v18, :cond_2a

    .line 660
    const/16 v18, 0x3

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 661
    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 664
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_29

    .line 665
    const-string/jumbo v18, "AlarmItem"

    .line 666
    const-string/jumbo v19, "----------------- ALARM_ACTIVE -----------------"

    .line 665
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    :cond_28
    :goto_10
    const-string/jumbo v18, "AlarmItem"

    .line 671
    const-string/jumbo v19, "next alert : snooze inactive, subdue active"

    .line 670
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    const-string/jumbo v18, "AlarmItem"

    const-string/jumbo v19, "set new smart alarm as selected"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 667
    :cond_29
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_28

    .line 668
    const-string/jumbo v18, "AlarmItem"

    .line 669
    const-string/jumbo v19, "----------------- ALARM_SNOOZE -----------------"

    .line 668
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_10

    .line 676
    :cond_2a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->repeatType:I

    move/from16 v18, v0

    and-int/lit8 v18, v18, 0xf

    add-int/lit8 v18, v18, -0x1

    if-nez v18, :cond_2c

    .line 677
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getAlertDayCount()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_2b

    .line 678
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 679
    const-wide/16 v18, -0x1

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 680
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 683
    const-string/jumbo v18, "AlarmItem"

    .line 684
    const-string/jumbo v19, "----------------- ALARM_INACTIVE -----------------"

    .line 683
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 685
    const-string/jumbo v18, "AlarmItem"

    .line 686
    const-string/jumbo v19, "next alert : snooze inactive, subdue active"

    .line 685
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    const-string/jumbo v18, "AlarmItem"

    .line 688
    const-string/jumbo v19, "snooze end up. set new smart alarm at next day. (2)"

    .line 687
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 692
    :cond_2b
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->clearRepeatDay(Ljava/util/Calendar;)V

    .line 695
    :cond_2c
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 708
    const/16 v18, 0x3

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 709
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 710
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    .line 711
    sget-object v20, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    move/from16 v21, v0

    aget v20, v20, v21

    const v21, 0xea60

    mul-int v20, v20, v21

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 710
    sub-long v12, v18, v20

    .line 712
    .restart local v12    # "nextSmartAlert":J
    :goto_11
    cmp-long v18, v6, v12

    if-gtz v18, :cond_2d

    .line 718
    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 721
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    if-nez v18, :cond_2e

    .line 722
    const-string/jumbo v18, "AlarmItem"

    .line 723
    const-string/jumbo v19, "----------------- ALARM_INACTIVE -----------------"

    .line 722
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    const-string/jumbo v18, "AlarmItem"

    .line 725
    const-string/jumbo v19, "next alert : snooze active, subdue active"

    .line 724
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 726
    const-string/jumbo v18, "AlarmItem"

    const-string/jumbo v19, "alarm done"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 713
    :cond_2d
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 714
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 715
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    .line 716
    sget-object v20, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    move/from16 v21, v0

    aget v20, v20, v21

    const v21, 0xea60

    mul-int v20, v20, v21

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 715
    sub-long v12, v18, v20

    goto :goto_11

    .line 728
    :cond_2e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_30

    .line 729
    const-string/jumbo v18, "AlarmItem"

    .line 730
    const-string/jumbo v19, "----------------- ALARM_ACTIVE -----------------"

    .line 729
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 737
    :cond_2f
    :goto_12
    const-string/jumbo v18, "AlarmItem"

    .line 738
    const-string/jumbo v19, "next alert : snooze inactive, subdue active"

    .line 737
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 731
    :cond_30
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_31

    .line 732
    const-string/jumbo v18, "AlarmItem"

    .line 733
    const-string/jumbo v19, "----------------- ALARM_SNOOZE -----------------"

    .line 732
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_12

    .line 734
    :cond_31
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x3

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_2f

    .line 735
    const-string/jumbo v18, "AlarmItem"

    .line 736
    const-string/jumbo v19, "----------------- ALARM_SUBDUE -----------------"

    .line 735
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_12

    .line 748
    .end local v12    # "nextSmartAlert":J
    .end local v16    # "subdueMillis":J
    :cond_32
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeActivate:Z

    move/from16 v18, v0

    if-eqz v18, :cond_4c

    .line 750
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    move/from16 v18, v0

    sget-object v19, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_SNOOZE_COUNT_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeRepeat:I

    move/from16 v20, v0

    aget v19, v19, v20

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_3a

    .line 751
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->repeatType:I

    move/from16 v18, v0

    and-int/lit8 v18, v18, 0xe

    if-nez v18, :cond_36

    .line 752
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getAlertDayCount()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_35

    .line 753
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 754
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 755
    const-wide/16 v18, -0x1

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 758
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_34

    .line 759
    const-string/jumbo v18, "AlarmItem"

    .line 760
    const-string/jumbo v19, "----------------- ALARM_ACTIVE -----------------"

    .line 759
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 764
    :cond_33
    :goto_13
    const-string/jumbo v18, "AlarmItem"

    .line 765
    const-string/jumbo v19, "next alert : snooze active, subdue inactive"

    .line 764
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 766
    const-string/jumbo v18, "AlarmItem"

    .line 767
    const-string/jumbo v19, "all snooze had finished. clear alarm."

    .line 766
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 761
    :cond_34
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_33

    .line 762
    const-string/jumbo v18, "AlarmItem"

    .line 763
    const-string/jumbo v19, "----------------- ALARM_SNOOZE -----------------"

    .line 762
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_13

    .line 771
    :cond_35
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->clearRepeatDay(Ljava/util/Calendar;)V

    .line 774
    :cond_36
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertDayOffset(Ljava/util/Calendar;)I

    move-result v8

    .line 775
    .local v8, "dayOffset":I
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 780
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 781
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 783
    :goto_14
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    cmp-long v18, v6, v18

    if-gtz v18, :cond_38

    .line 791
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_39

    .line 792
    const-string/jumbo v18, "AlarmItem"

    .line 793
    const-string/jumbo v19, "----------------- ALARM_ACTIVE -----------------"

    .line 792
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 797
    :cond_37
    :goto_15
    const-string/jumbo v18, "AlarmItem"

    .line 798
    const-string/jumbo v19, "next alert : snooze active, subdue inactive"

    .line 797
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 799
    const-string/jumbo v18, "AlarmItem"

    .line 800
    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "new alarm set as normal alarm with snooze on next ("

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 801
    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, ") day"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 800
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 799
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 784
    :cond_38
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 787
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    goto :goto_14

    .line 794
    :cond_39
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_37

    .line 795
    const-string/jumbo v18, "AlarmItem"

    .line 796
    const-string/jumbo v19, "----------------- ALARM_SNOOZE -----------------"

    .line 795
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_15

    .line 806
    .end local v8    # "dayOffset":I
    :cond_3a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_43

    .line 807
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    cmp-long v18, v18, v6

    if-lez v18, :cond_3c

    .line 808
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    move/from16 v18, v0

    if-lez v18, :cond_3b

    .line 809
    const/16 v18, 0x2

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 812
    const-string/jumbo v18, "AlarmItem"

    .line 813
    const-string/jumbo v19, "----------------- ALARM_SNOOZE -----------------"

    .line 812
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 814
    const-string/jumbo v18, "AlarmItem"

    .line 815
    const-string/jumbo v19, "next alert : snooze active, subdue inactive"

    .line 814
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 816
    const-string/jumbo v18, "AlarmItem"

    .line 817
    const-string/jumbo v19, "active alarm changed as snooze"

    .line 816
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 822
    :cond_3b
    const-string/jumbo v18, "AlarmItem"

    .line 823
    const-string/jumbo v19, "----------------- ALARM_ACTIVE -----------------"

    .line 822
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 824
    const-string/jumbo v18, "AlarmItem"

    .line 825
    const-string/jumbo v19, "next alert : snooze active, subdue inactive"

    .line 824
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 826
    const-string/jumbo v18, "AlarmItem"

    const-string/jumbo v19, "active alarm set"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 831
    :cond_3c
    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .restart local v9    # "i":I
    :goto_16
    sget-object v18, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_SNOOZE_COUNT_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeRepeat:I

    move/from16 v19, v0

    aget v18, v18, v19

    move/from16 v0, v18

    if-lt v9, v0, :cond_3e

    .line 838
    :cond_3d
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    cmp-long v18, v18, v6

    if-lez v18, :cond_3f

    .line 839
    const/16 v18, 0x2

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 842
    const-string/jumbo v18, "AlarmItem"

    .line 843
    const-string/jumbo v19, "----------------- ALARM_SNOOZE -----------------"

    .line 842
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 844
    const-string/jumbo v18, "AlarmItem"

    .line 845
    const-string/jumbo v19, "next alert : snooze active, subdue inactive"

    .line 844
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 846
    const-string/jumbo v18, "AlarmItem"

    const-string/jumbo v19, "found next snooze."

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 832
    :cond_3e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 833
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    sget-object v20, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDuration:I

    move/from16 v21, v0

    aget v20, v20, v21

    const v21, 0xea60

    mul-int v20, v20, v21

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    add-long v18, v18, v20

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 834
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    cmp-long v18, v18, v6

    if-gtz v18, :cond_3d

    .line 831
    add-int/lit8 v9, v9, 0x1

    goto :goto_16

    .line 850
    :cond_3f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->repeatType:I

    move/from16 v18, v0

    and-int/lit8 v18, v18, 0xe

    if-nez v18, :cond_41

    .line 851
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getAlertDayCount()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_40

    .line 852
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 853
    const-wide/16 v18, -0x1

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 854
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 857
    const-string/jumbo v18, "AlarmItem"

    .line 858
    const-string/jumbo v19, "----------------- ALARM_SNOOZE -----------------"

    .line 857
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 859
    const-string/jumbo v18, "AlarmItem"

    .line 860
    const-string/jumbo v19, "next alert : snooze active, subdue inactive"

    .line 859
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 861
    const-string/jumbo v18, "AlarmItem"

    .line 862
    const-string/jumbo v19, "alarm fired but no snooze can be alert."

    .line 861
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 866
    :cond_40
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->clearRepeatDay(Ljava/util/Calendar;)V

    .line 870
    :cond_41
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 871
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 877
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertDayOffset(Ljava/util/Calendar;)I

    move-result v8

    .line 880
    .restart local v8    # "dayOffset":I
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 882
    :goto_17
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    cmp-long v18, v6, v18

    if-gtz v18, :cond_42

    .line 891
    const-string/jumbo v18, "AlarmItem"

    .line 892
    const-string/jumbo v19, "----------------- ALARM_SNOOZE -----------------"

    .line 891
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 893
    const-string/jumbo v18, "AlarmItem"

    .line 894
    const-string/jumbo v19, "next alert : snooze active, subdue inactive"

    .line 893
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 895
    const-string/jumbo v18, "AlarmItem"

    .line 896
    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "change to active. new alarm set as normal alarm with snooze on next ("

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 897
    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, ") day"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 896
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 895
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 883
    :cond_42
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 887
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    goto :goto_17

    .line 902
    .end local v8    # "dayOffset":I
    .end local v9    # "i":I
    :cond_43
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_49

    .line 903
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    cmp-long v18, v18, v6

    if-gez v18, :cond_44

    .line 904
    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .restart local v9    # "i":I
    :goto_18
    sget-object v18, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_SNOOZE_COUNT_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeRepeat:I

    move/from16 v19, v0

    aget v18, v18, v19

    move/from16 v0, v18

    if-lt v9, v0, :cond_45

    .line 912
    .end local v9    # "i":I
    :cond_44
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    move/from16 v18, v0

    sget-object v19, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_SNOOZE_COUNT_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeRepeat:I

    move/from16 v20, v0

    aget v19, v19, v20

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_49

    .line 913
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->repeatType:I

    move/from16 v18, v0

    and-int/lit8 v18, v18, 0xe

    if-nez v18, :cond_47

    .line 914
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getAlertDayCount()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_46

    .line 915
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 916
    const-wide/16 v18, -0x1

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 917
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 920
    const-string/jumbo v18, "AlarmItem"

    .line 921
    const-string/jumbo v19, "----------------- ALARM_SNOOZE -----------------"

    .line 920
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 922
    const-string/jumbo v18, "AlarmItem"

    .line 923
    const-string/jumbo v19, "next alert : snooze active, subdue inactive"

    .line 922
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 924
    const-string/jumbo v18, "AlarmItem"

    .line 925
    const-string/jumbo v19, "snooze end. change as inactive alarm."

    .line 924
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 905
    .restart local v9    # "i":I
    :cond_45
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 906
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    sget-object v20, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDuration:I

    move/from16 v21, v0

    aget v20, v20, v21

    const v21, 0xea60

    mul-int v20, v20, v21

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    add-long v18, v18, v20

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 907
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    cmp-long v18, v18, v6

    if-gtz v18, :cond_44

    .line 904
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_18

    .line 929
    .end local v9    # "i":I
    :cond_46
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->clearRepeatDay(Ljava/util/Calendar;)V

    .line 934
    :cond_47
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 936
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertDayOffset(Ljava/util/Calendar;)I

    move-result v8

    .line 937
    .restart local v8    # "dayOffset":I
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 939
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 941
    :goto_19
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    cmp-long v18, v6, v18

    if-gtz v18, :cond_48

    .line 950
    const-string/jumbo v18, "AlarmItem"

    .line 951
    const-string/jumbo v19, "----------------- ALARM_SNOOZE -----------------"

    .line 950
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 952
    const-string/jumbo v18, "AlarmItem"

    .line 953
    const-string/jumbo v19, "next alert : snooze active, subdue inactive"

    .line 952
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 954
    const-string/jumbo v18, "AlarmItem"

    .line 955
    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "change to active. new alarm set as normal alarm with snooze on next ("

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 956
    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, ") day"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 955
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 954
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 942
    :cond_48
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 946
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    goto :goto_19

    .line 963
    .end local v8    # "dayOffset":I
    :cond_49
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_4b

    .line 964
    const-string/jumbo v18, "AlarmItem"

    .line 965
    const-string/jumbo v19, "----------------- ALARM_ACTIVE -----------------"

    .line 964
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 969
    :cond_4a
    :goto_1a
    const-string/jumbo v18, "AlarmItem"

    .line 970
    const-string/jumbo v19, "next alert : snooze active, subdue inactive"

    .line 969
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 971
    const-string/jumbo v18, "AlarmItem"

    const-string/jumbo v19, "set next snooze."

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 966
    :cond_4b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_4a

    .line 967
    const-string/jumbo v18, "AlarmItem"

    .line 968
    const-string/jumbo v19, "----------------- ALARM_SNOOZE -----------------"

    .line 967
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1a

    .line 977
    :cond_4c
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    cmp-long v18, v6, v18

    if-lez v18, :cond_52

    .line 978
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->repeatType:I

    move/from16 v18, v0

    and-int/lit8 v18, v18, 0xe

    if-nez v18, :cond_4e

    .line 979
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getAlertDayCount()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_4d

    .line 980
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 981
    const-wide/16 v18, -0x1

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 982
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 985
    const-string/jumbo v18, "AlarmItem"

    .line 986
    const-string/jumbo v19, "----------------- ALARM_INACTIVE -----------------"

    .line 985
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 987
    const-string/jumbo v18, "AlarmItem"

    .line 988
    const-string/jumbo v19, "next alert : snooze inactive, subdue inactive"

    .line 987
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 989
    const-string/jumbo v18, "AlarmItem"

    const-string/jumbo v19, "alarm set as tomorrow."

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 993
    :cond_4d
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->clearRepeatDay(Ljava/util/Calendar;)V

    .line 996
    :cond_4e
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 998
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertDayOffset(Ljava/util/Calendar;)I

    move-result v8

    .line 999
    .restart local v8    # "dayOffset":I
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 1001
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 1003
    :goto_1b
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    cmp-long v18, v6, v18

    if-gtz v18, :cond_50

    .line 1011
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_51

    .line 1012
    const-string/jumbo v18, "AlarmItem"

    .line 1013
    const-string/jumbo v19, "----------------- ALARM_ACTIVE -----------------"

    .line 1012
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1017
    :cond_4f
    :goto_1c
    const-string/jumbo v18, "AlarmItem"

    .line 1018
    const-string/jumbo v19, "next alert : snooze inactive, subdue inactive"

    .line 1017
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1019
    const-string/jumbo v18, "AlarmItem"

    .line 1020
    new-instance v19, Ljava/lang/StringBuilder;

    const-string/jumbo v20, "new alarm set as normal alarm on next ("

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1021
    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, ") day"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 1020
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 1019
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1004
    :cond_50
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1007
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    goto :goto_1b

    .line 1014
    :cond_51
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_4f

    .line 1015
    const-string/jumbo v18, "AlarmItem"

    .line 1016
    const-string/jumbo v19, "----------------- ALARM_SNOOZE -----------------"

    .line 1015
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1c

    .line 1027
    .end local v8    # "dayOffset":I
    :cond_52
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_54

    .line 1028
    const-string/jumbo v18, "AlarmItem"

    .line 1029
    const-string/jumbo v19, "----------------- ALARM_ACTIVE -----------------"

    .line 1028
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1033
    :cond_53
    :goto_1d
    const-string/jumbo v18, "AlarmItem"

    .line 1034
    const-string/jumbo v19, "next alert : snooze inactive, subdue inactive"

    .line 1033
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1035
    const-string/jumbo v18, "AlarmItem"

    const-string/jumbo v19, "valid alarm as one shot alert."

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1030
    :cond_54
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_53

    .line 1031
    const-string/jumbo v18, "AlarmItem"

    .line 1032
    const-string/jumbo v19, "----------------- ALARM_SNOOZE -----------------"

    .line 1031
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1d

    .line 1044
    :pswitch_3
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    cmp-long v18, v18, v6

    if-lez v18, :cond_55

    .line 1047
    const-string/jumbo v18, "AlarmItem"

    .line 1048
    const-string/jumbo v19, "----------------- ALARM_SUBDUE -----------------"

    .line 1047
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1049
    const-string/jumbo v18, "AlarmItem"

    const-string/jumbo v19, "next alert : snooze whatever, subdue active"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1050
    const-string/jumbo v18, "AlarmItem"

    const-string/jumbo v19, "just let smart alarm alert."

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1054
    :cond_55
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    .line 1055
    sget-object v20, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    move/from16 v21, v0

    aget v20, v20, v21

    const v21, 0xea60

    mul-int v20, v20, v21

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 1054
    add-long v3, v18, v20

    .line 1056
    .local v3, "activeAlertTime":J
    cmp-long v18, v3, v6

    if-lez v18, :cond_56

    .line 1058
    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 1059
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 1062
    const-string/jumbo v18, "AlarmItem"

    .line 1063
    const-string/jumbo v19, "----------------- ALARM_SUBDUE -----------------"

    .line 1062
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1064
    const-string/jumbo v18, "AlarmItem"

    .line 1065
    const-string/jumbo v19, "next alert : snooze whatever, subdue active"

    .line 1064
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1066
    const-string/jumbo v18, "AlarmItem"

    const-string/jumbo v19, "smart alarm has gone but we got active."

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1070
    :cond_56
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeActivate:Z

    move/from16 v18, v0

    if-eqz v18, :cond_5d

    .line 1072
    move-wide v14, v3

    .line 1073
    .local v14, "snoozeTimeMillis":J
    :cond_57
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    move/from16 v18, v0

    sget-object v19, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_SNOOZE_COUNT_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeRepeat:I

    move/from16 v20, v0

    aget v19, v19, v20

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_58

    .line 1080
    :goto_1e
    cmp-long v18, v14, v6

    if-gez v18, :cond_5c

    .line 1081
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->repeatType:I

    move/from16 v18, v0

    and-int/lit8 v18, v18, 0xe

    if-nez v18, :cond_5a

    .line 1083
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getAlertDayCount()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_59

    .line 1084
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 1085
    const-wide/16 v18, -0x1

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 1086
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 1089
    const-string/jumbo v18, "AlarmItem"

    .line 1090
    const-string/jumbo v19, "----------------- ALARM_SUBDUE -----------------"

    .line 1089
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1091
    const-string/jumbo v18, "AlarmItem"

    .line 1092
    const-string/jumbo v19, "next alert : snooze whatever, subdue active"

    .line 1091
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1093
    const-string/jumbo v18, "AlarmItem"

    .line 1094
    const-string/jumbo v19, "all snooze also useless. inactive this alarm."

    .line 1093
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1074
    :cond_58
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 1075
    sget-object v18, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDuration:I

    move/from16 v19, v0

    aget v18, v18, v19

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    add-long v14, v14, v18

    .line 1076
    cmp-long v18, v14, v6

    if-lez v18, :cond_57

    goto :goto_1e

    .line 1098
    :cond_59
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->clearRepeatDay(Ljava/util/Calendar;)V

    .line 1105
    :cond_5a
    const/16 v18, 0x3

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 1106
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 1107
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    .line 1108
    sget-object v20, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    move/from16 v21, v0

    aget v20, v20, v21

    const v21, 0xea60

    mul-int v20, v20, v21

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 1107
    sub-long v12, v18, v20

    .line 1109
    .restart local v12    # "nextSmartAlert":J
    :goto_1f
    cmp-long v18, v6, v12

    if-gtz v18, :cond_5b

    .line 1115
    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 1117
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 1120
    const-string/jumbo v18, "AlarmItem"

    .line 1121
    const-string/jumbo v19, "----------------- ALARM_SUBDUE -----------------"

    .line 1120
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1122
    const-string/jumbo v18, "AlarmItem"

    .line 1123
    const-string/jumbo v19, "next alert : snooze whatever, subdue active"

    .line 1122
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1124
    const-string/jumbo v18, "AlarmItem"

    .line 1125
    const-string/jumbo v19, "find other day to alert smart alarm."

    .line 1124
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1110
    :cond_5b
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1111
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 1112
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    move-wide/from16 v18, v0

    .line 1113
    sget-object v20, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    move/from16 v21, v0

    aget v20, v20, v21

    const v21, 0xea60

    mul-int v20, v20, v21

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 1112
    sub-long v12, v18, v20

    goto :goto_1f

    .line 1129
    .end local v12    # "nextSmartAlert":J
    :cond_5c
    const/16 v18, 0x2

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 1130
    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 1133
    const-string/jumbo v18, "AlarmItem"

    .line 1134
    const-string/jumbo v19, "----------------- ALARM_SUBDUE -----------------"

    .line 1133
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1135
    const-string/jumbo v18, "AlarmItem"

    .line 1136
    const-string/jumbo v19, "next alert : snooze whatever, subdue active"

    .line 1135
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1137
    const-string/jumbo v18, "AlarmItem"

    const-string/jumbo v19, "found snooze to alert."

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1142
    .end local v14    # "snoozeTimeMillis":J
    :cond_5d
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 1143
    const-wide/16 v18, -0x1

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 1144
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 1147
    const-string/jumbo v18, "AlarmItem"

    .line 1148
    const-string/jumbo v19, "----------------- ALARM_SUBDUE -----------------"

    .line 1147
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1149
    const-string/jumbo v18, "AlarmItem"

    .line 1150
    const-string/jumbo v19, "next alert : snooze whatever, subdue active"

    .line 1149
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1151
    const-string/jumbo v18, "AlarmItem"

    const-string/jumbo v19, "go inactive."

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1160
    .end local v3    # "activeAlertTime":J
    :pswitch_4
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 1163
    const-string/jumbo v18, "AlarmItem"

    .line 1164
    const-string/jumbo v19, "----------------- ALARM_SUBDUE_NEXT -----------------"

    .line 1163
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1165
    const-string/jumbo v18, "AlarmItem"

    const-string/jumbo v19, "next alert : snooze whatever, subdue active"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1166
    const-string/jumbo v18, "AlarmItem"

    const-string/jumbo v19, "we are going to active cause get next alarm."

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 316
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static final createItemFromCursor(Landroid/database/Cursor;)Lcom/sec/android/app/clockpackage/alarm/AlarmItem;
    .locals 6
    .param p0, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1178
    new-instance v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;

    invoke-direct {v0}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;-><init>()V

    .line 1180
    .local v0, "item":Lcom/sec/android/app/clockpackage/alarm/AlarmItem;
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->id:I

    .line 1181
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 1182
    const/4 v1, 0x2

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->createTime:J

    .line 1183
    const/4 v1, 0x3

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 1184
    const/4 v1, 0x4

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmTime:I

    .line 1185
    const/4 v1, 0x5

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->repeatType:I

    .line 1186
    const/4 v1, 0x6

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->notificationType:I

    .line 1187
    const/4 v1, 0x7

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v2, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeActivate:Z

    .line 1189
    const/16 v1, 0x8

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDuration:I

    .line 1190
    const/16 v1, 0x9

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeRepeat:I

    .line 1191
    const/16 v1, 0xa

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 1192
    const/16 v1, 0xb

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->dailyBriefing:I

    .line 1193
    const/16 v1, 0xc

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v2, :cond_1

    :goto_1
    iput-boolean v2, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueActivate:Z

    .line 1195
    const/16 v1, 0xd

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    .line 1196
    const/16 v1, 0xe

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueTone:I

    .line 1197
    const/16 v1, 0xf

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmSoundType:I

    .line 1198
    const/16 v1, 0x10

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmSoundTone:I

    .line 1199
    const/16 v1, 0x11

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmVolume:I

    .line 1200
    const/16 v1, 0x12

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueUri:I

    .line 1201
    const/16 v1, 0x13

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->soundUri:Ljava/lang/String;

    .line 1202
    const/16 v1, 0x14

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmName:Ljava/lang/String;

    .line 1204
    return-object v0

    :cond_0
    move v1, v3

    .line 1188
    goto :goto_0

    :cond_1
    move v2, v3

    .line 1194
    goto :goto_1
.end method

.method private getNextAlertTime(Ljava/util/Calendar;)J
    .locals 3
    .param p1, "c"    # Ljava/util/Calendar;

    .prologue
    const/4 v2, 0x0

    .line 138
    const/4 v0, 0x6

    invoke-virtual {p0, p1}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertDayOffset(Ljava/util/Calendar;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->add(II)V

    .line 141
    const/16 v0, 0xb

    iget v1, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmTime:I

    div-int/lit8 v1, v1, 0x64

    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 142
    const/16 v0, 0xc

    iget v1, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmTime:I

    rem-int/lit8 v1, v1, 0x64

    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 143
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 144
    const/16 v0, 0xe

    invoke-virtual {p1, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 147
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public calculateFirstAlertTime()V
    .locals 12

    .prologue
    const v11, 0xea60

    .line 154
    const-string/jumbo v7, "AlarmItem"

    const-string/jumbo v8, "calculateFirstAlertTime"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 157
    .local v0, "c":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 159
    .local v3, "currentMillis":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 160
    .local v1, "c1":Ljava/util/Calendar;
    iget-wide v7, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    invoke-virtual {v1, v7, v8}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 162
    const-string/jumbo v7, "AlarmItem"

    .line 163
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "calendar:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "system:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 164
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 163
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 162
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    const-string/jumbo v7, "AlarmItem"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "alarmAlertTime"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v9, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "currentMillis:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 167
    invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 166
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 171
    .local v2, "cr":Ljava/util/Calendar;
    iget-wide v7, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    invoke-virtual {v2, v7, v8}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 172
    const/4 v7, 0x6

    const/4 v8, -0x1

    invoke-virtual {v2, v7, v8}, Ljava/util/Calendar;->add(II)V

    .line 174
    invoke-direct {p0, v2}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v7

    iput-wide v7, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 176
    iget v7, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    const/4 v8, 0x3

    if-ne v7, v8, :cond_3

    .line 177
    const-wide/16 v5, 0x0

    .line 178
    .local v5, "subdueTime":J
    iget-wide v7, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 179
    sget-object v9, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    iget v10, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    aget v9, v9, v10

    mul-int/2addr v9, v11

    int-to-long v9, v9

    .line 178
    sub-long v5, v7, v9

    .line 181
    iget-wide v7, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    cmp-long v7, v7, v3

    if-lez v7, :cond_1

    cmp-long v7, v5, v3

    if-gez v7, :cond_1

    .line 182
    const/4 v7, 0x1

    iput v7, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 203
    .end local v5    # "subdueTime":J
    :cond_0
    :goto_0
    return-void

    .line 183
    .restart local v5    # "subdueTime":J
    :cond_1
    cmp-long v7, v5, v3

    if-lez v7, :cond_2

    .line 184
    iput-wide v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    goto :goto_0

    .line 185
    :cond_2
    iget-wide v7, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    cmp-long v7, v7, v3

    if-gez v7, :cond_0

    .line 189
    iget-wide v7, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    invoke-virtual {v0, v7, v8}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 190
    invoke-direct {p0, v2}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v7

    iput-wide v7, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 191
    iget-wide v7, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    sget-object v9, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    iget v10, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    aget v9, v9, v10

    mul-int/2addr v9, v11

    int-to-long v9, v9

    sub-long/2addr v7, v9

    iput-wide v7, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    goto :goto_0

    .line 195
    .end local v5    # "subdueTime":J
    :cond_3
    const-string/jumbo v7, "AlarmItem"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "alarmAlertTime"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v9, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "currentMillis:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 196
    invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 195
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-wide v7, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    cmp-long v7, v7, v3

    if-gez v7, :cond_0

    .line 198
    iget-wide v7, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    invoke-virtual {v0, v7, v8}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 200
    invoke-direct {p0, v0}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v7

    iput-wide v7, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    goto :goto_0
.end method

.method public calculateNextAlertTime()V
    .locals 5

    .prologue
    const v4, 0xea60

    .line 260
    const-string/jumbo v0, "AlarmItem"

    const-string/jumbo v1, "calculateNextAlertTime"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    iget v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    packed-switch v0, :pswitch_data_0

    .line 282
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->calculateAlertTime()V

    .line 283
    return-void

    .line 265
    :pswitch_0
    iget-boolean v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeActivate:Z

    if-eqz v0, :cond_0

    .line 266
    iget v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 267
    iget-wide v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    sget-object v2, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    iget v3, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDuration:I

    aget v2, v2, v3

    mul-int/2addr v2, v4

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    goto :goto_0

    .line 273
    :pswitch_1
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 274
    iget-wide v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    sget-object v2, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    iget v3, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    aget v2, v2, v3

    mul-int/2addr v2, v4

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    goto :goto_0

    .line 262
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public calculateNextSmartAlertTime()V
    .locals 10

    .prologue
    const v9, 0xea60

    const/4 v7, 0x1

    .line 206
    const-string/jumbo v5, "AlarmItem"

    const-string/jumbo v6, "calculateNextSmartAlertTime"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    invoke-virtual {p0}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->calculateOriginalAlertTime()V

    .line 209
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 210
    .local v0, "c":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    .line 212
    .local v1, "currentMillis":J
    iget v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->repeatType:I

    and-int/lit8 v5, v5, 0xf

    if-ne v5, v7, :cond_0

    .line 213
    invoke-virtual {p0}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getAlertDayCount()I

    move-result v5

    if-ne v5, v7, :cond_1

    .line 214
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 220
    :cond_0
    :goto_0
    const-string/jumbo v5, "AlarmItem"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "active : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    iget-wide v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 222
    invoke-direct {p0, v0}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 223
    iget-wide v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 224
    sget-object v7, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    iget v8, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    aget v7, v7, v8

    mul-int/2addr v7, v9

    int-to-long v7, v7

    .line 223
    sub-long v3, v5, v7

    .line 225
    .local v3, "nextSmartAlert":J
    :goto_1
    cmp-long v5, v1, v3

    if-gtz v5, :cond_2

    .line 231
    iput-wide v3, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 247
    return-void

    .line 216
    .end local v3    # "nextSmartAlert":J
    :cond_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->clearRepeatDay(Ljava/util/Calendar;)V

    goto :goto_0

    .line 226
    .restart local v3    # "nextSmartAlert":J
    :cond_2
    iget-wide v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 227
    invoke-direct {p0, v0}, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->getNextAlertTime(Ljava/util/Calendar;)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 228
    iget-wide v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 229
    sget-object v7, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    iget v8, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    aget v7, v7, v8

    mul-int/2addr v7, v9

    int-to-long v7, v7

    .line 228
    sub-long v3, v5, v7

    goto :goto_1
.end method

.method public calculateOriginalAlertTime()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 250
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 251
    .local v0, "c":Ljava/util/Calendar;
    const/16 v1, 0xb

    iget v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmTime:I

    div-int/lit8 v2, v2, 0x64

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 252
    const/16 v1, 0xc

    iget v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmTime:I

    rem-int/lit8 v2, v2, 0x64

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 253
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 254
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 256
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 257
    return-void
.end method

.method public clearRepeatDay(Ljava/util/Calendar;)V
    .locals 6
    .param p1, "c"    # Ljava/util/Calendar;

    .prologue
    .line 1319
    const/4 v3, 0x7

    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 1320
    .local v1, "offset":I
    const/4 v2, 0x1

    .line 1321
    .local v2, "operator":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    rsub-int/lit8 v3, v1, 0x7

    if-le v0, v3, :cond_0

    .line 1325
    const-string/jumbo v3, "AlarmItem"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "offset:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1326
    const-string/jumbo v3, "AlarmItem"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "repeat type : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->repeatType:I

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1327
    const-string/jumbo v3, "AlarmItem"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "operator : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1328
    const-string/jumbo v5, "\n~operator : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    xor-int/lit8 v5, v2, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1327
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1329
    iget v3, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->repeatType:I

    xor-int/lit8 v4, v2, -0x1

    and-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->repeatType:I

    .line 1330
    const-string/jumbo v3, "AlarmItem"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "repeat type : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->repeatType:I

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1332
    return-void

    .line 1322
    :cond_0
    shl-int/lit8 v2, v2, 0x4

    .line 1321
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0
.end method

.method public getAlertDayCount()I
    .locals 6

    .prologue
    .line 1304
    const/4 v1, 0x0

    .line 1305
    .local v1, "nCount":I
    const/4 v2, 0x1

    .line 1306
    .local v2, "operator":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v3, 0x7

    if-lt v0, v3, :cond_0

    .line 1313
    const-string/jumbo v3, "AlarmItem"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "nCount : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1315
    return v1

    .line 1307
    :cond_0
    shl-int/lit8 v2, v2, 0x4

    .line 1308
    iget v3, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->repeatType:I

    and-int/2addr v3, v2

    if-lez v3, :cond_1

    .line 1309
    add-int/lit8 v1, v1, 0x1

    .line 1306
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getContentValues()Landroid/content/ContentValues;
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 103
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 105
    .local v1, "value":Landroid/content/ContentValues;
    const/4 v0, 0x0

    .line 106
    .local v0, "alarmType":I
    iget-boolean v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeActivate:Z

    if-eqz v2, :cond_0

    .line 107
    or-int/lit16 v0, v0, 0x100

    .line 108
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueActivate:Z

    if-eqz v2, :cond_1

    .line 109
    or-int/lit8 v0, v0, 0x1

    .line 111
    :cond_1
    const-string/jumbo v2, "active"

    iget v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 112
    const-string/jumbo v2, "createtime"

    iget-wide v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->createTime:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 113
    const-string/jumbo v2, "alerttime"

    iget-wide v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 114
    const-string/jumbo v2, "alarmtime"

    iget v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmTime:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 115
    const-string/jumbo v2, "repeattype"

    iget v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->repeatType:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 116
    const-string/jumbo v2, "notitype"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 117
    const-string/jumbo v5, "snzactive"

    iget-boolean v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeActivate:Z

    if-eqz v2, :cond_2

    move v2, v3

    :goto_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 118
    const-string/jumbo v2, "snzduration"

    iget v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDuration:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 119
    const-string/jumbo v2, "snzrepeat"

    iget v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeRepeat:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 120
    const-string/jumbo v2, "snzcount"

    iget v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 121
    const-string/jumbo v2, "dailybrief"

    iget v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->dailyBriefing:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 122
    const-string/jumbo v2, "sbdactive"

    iget-boolean v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueActivate:Z

    if-eqz v5, :cond_3

    :goto_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 123
    const-string/jumbo v2, "sbdduration"

    iget v3, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 124
    const-string/jumbo v2, "sbdtone"

    iget v3, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueTone:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 125
    const-string/jumbo v2, "alarmsound"

    iget v3, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmSoundType:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 126
    const-string/jumbo v2, "alarmtone"

    iget v3, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmSoundTone:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 127
    const-string/jumbo v2, "volume"

    iget v3, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmVolume:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 128
    const-string/jumbo v2, "sbduri"

    iget v3, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueUri:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 129
    const-string/jumbo v2, "alarmuri"

    iget-object v3, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->soundUri:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const-string/jumbo v2, "name"

    iget-object v3, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    return-object v1

    :cond_2
    move v2, v4

    .line 117
    goto/16 :goto_0

    :cond_3
    move v3, v4

    .line 122
    goto :goto_1
.end method

.method public getNextAlertDayOffset(Ljava/util/Calendar;)I
    .locals 7
    .param p1, "c"    # Ljava/util/Calendar;

    .prologue
    const/4 v6, 0x7

    .line 1287
    invoke-virtual {p1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 1288
    .local v0, "day":I
    const/high16 v1, 0x10000000

    .line 1289
    .local v1, "dayOffset":I
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    if-le v2, v6, :cond_1

    .line 1300
    const/4 v2, 0x0

    .end local v2    # "i":I
    :cond_0
    return v2

    .line 1290
    .restart local v2    # "i":I
    :cond_1
    add-int v3, v0, v2

    .line 1291
    .local v3, "nextDay":I
    if-le v3, v6, :cond_2

    .line 1292
    add-int/lit8 v3, v3, -0x7

    .line 1294
    :cond_2
    mul-int/lit8 v5, v3, 0x4

    shr-int v4, v1, v5

    .line 1295
    .local v4, "operator":I
    iget v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->repeatType:I

    shr-int/lit8 v5, v5, 0x4

    and-int/2addr v5, v4

    if-gtz v5, :cond_0

    .line 1289
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public readFromIntent(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 72
    const-string/jumbo v2, "com.samsung.sec.android.clockpackage.alarm.ALARM_DATA"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 73
    .local v0, "alarmData":[B
    if-eqz v0, :cond_0

    .line 74
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 75
    .local v1, "in":Landroid/os/Parcel;
    array-length v2, v0

    invoke-virtual {v1, v0, v4, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 76
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 78
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->id:I

    .line 79
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    .line 80
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->createTime:J

    .line 81
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    .line 82
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmTime:I

    .line 83
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->repeatType:I

    .line 84
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->notificationType:I

    .line 85
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_1

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeActivate:Z

    .line 86
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDuration:I

    .line 87
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeRepeat:I

    .line 88
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    .line 89
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->dailyBriefing:I

    .line 90
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_2

    :goto_1
    iput-boolean v3, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueActivate:Z

    .line 91
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    .line 92
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueTone:I

    .line 93
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmSoundType:I

    .line 94
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmSoundTone:I

    .line 95
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmVolume:I

    .line 96
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueUri:I

    .line 97
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->soundUri:Ljava/lang/String;

    .line 98
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmName:Ljava/lang/String;

    .line 100
    .end local v1    # "in":Landroid/os/Parcel;
    :cond_0
    return-void

    .restart local v1    # "in":Landroid/os/Parcel;
    :cond_1
    move v2, v4

    .line 85
    goto :goto_0

    :cond_2
    move v3, v4

    .line 90
    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1208
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1210
    .local v1, "ret":Ljava/lang/StringBuilder;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1211
    .local v0, "c":Ljava/util/Calendar;
    iget-wide v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1213
    const-string/jumbo v2, "id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1214
    iget v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->id:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1215
    const-string/jumbo v2, ", \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1216
    const-string/jumbo v2, "activate : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1217
    iget v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1218
    const-string/jumbo v2, ", \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1219
    const-string/jumbo v2, "createTime : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1220
    iget-wide v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->createTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1221
    const-string/jumbo v2, ", \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1222
    const-string/jumbo v2, "AlertTime : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1223
    iget-wide v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1224
    const-string/jumbo v2, ", \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1226
    const-string/jumbo v2, "AlertT___ (at cal) : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1227
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/clockpackage/alarm/Alarm;->digitToAlphabetStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1228
    const-string/jumbo v2, ", \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1229
    const-string/jumbo v2, "alarmT___ : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1230
    iget v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmTime:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/clockpackage/alarm/Alarm;->digitToAlphabetStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1231
    const-string/jumbo v2, ", \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1233
    const-string/jumbo v2, "repeatType : 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1234
    iget v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->repeatType:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1235
    const-string/jumbo v2, ", \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1236
    const-string/jumbo v2, "notificationType : 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1237
    iget v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->notificationType:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1238
    const-string/jumbo v2, ", \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1239
    const-string/jumbo v2, "snoozeActivate : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1240
    iget-boolean v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeActivate:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1241
    const-string/jumbo v2, ", \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1242
    const-string/jumbo v2, "snoozeDuration : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1243
    sget-object v2, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    iget v3, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDuration:I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1244
    const-string/jumbo v2, ", \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1245
    const-string/jumbo v2, "snoozeRepeat : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1246
    sget-object v2, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_SNOOZE_COUNT_TABLE:[I

    iget v3, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeRepeat:I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1247
    const-string/jumbo v2, ", \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1248
    const-string/jumbo v2, "snoozeDoneCount : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1249
    iget v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1250
    const-string/jumbo v2, ", \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1251
    const-string/jumbo v2, "dailyBriefing : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1252
    iget v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->dailyBriefing:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1253
    const-string/jumbo v2, ", \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1254
    const-string/jumbo v2, "subdueActivate : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1255
    iget-boolean v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueActivate:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1256
    const-string/jumbo v2, ", \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1257
    const-string/jumbo v2, "subdueDuration : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1258
    iget v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1259
    const-string/jumbo v2, ", \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1260
    const-string/jumbo v2, "subdueTone : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1261
    iget v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueTone:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1262
    const-string/jumbo v2, ", \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1263
    const-string/jumbo v2, "alarmSoundType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1264
    iget v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmSoundType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1265
    const-string/jumbo v2, ", \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1266
    const-string/jumbo v2, "alarmSoundTone : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1267
    iget v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmSoundTone:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1268
    const-string/jumbo v2, ", \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1269
    const-string/jumbo v2, "alarmVolume : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1270
    iget v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmVolume:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1271
    const-string/jumbo v2, ", \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1272
    const-string/jumbo v2, "subdueUri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1273
    iget v2, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueUri:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1274
    const-string/jumbo v2, ", \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1283
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public writeToParcel(Landroid/os/Parcel;)V
    .locals 5
    .param p1, "dest"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    iget v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->id:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 49
    iget v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->activate:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 50
    iget-wide v3, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->createTime:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    .line 51
    iget-wide v3, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmAlertTime:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    .line 52
    iget v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 53
    iget v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->repeatType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 54
    iget v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->notificationType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 55
    iget-boolean v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeActivate:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 56
    iget v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDuration:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 57
    iget v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeRepeat:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 58
    iget v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->snoozeDoneCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 59
    iget v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->dailyBriefing:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 60
    iget-boolean v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueActivate:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 61
    iget v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueDuration:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 62
    iget v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueTone:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 63
    iget v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmSoundType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 64
    iget v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmSoundTone:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 65
    iget v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmVolume:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 66
    iget v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->subdueUri:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->soundUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/alarm/AlarmItem;->alarmName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 69
    return-void

    :cond_0
    move v0, v2

    .line 55
    goto :goto_0

    :cond_1
    move v1, v2

    .line 60
    goto :goto_1
.end method
