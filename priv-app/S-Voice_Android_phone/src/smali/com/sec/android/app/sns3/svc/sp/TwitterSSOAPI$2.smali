.class final Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$2;
.super Ljava/lang/Object;
.source "TwitterSSOAPI.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->bindTwitterService(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 263
    invoke-static {p2}, Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;

    move-result-object v0

    # setter for: Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->mSnsTwitterForAuthToken:Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->access$302(Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;)Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;

    .line 264
    # getter for: Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->access$200()Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->getTwAccountInfo(Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;)V

    .line 265
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 268
    const/4 v0, 0x0

    # setter for: Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->mSnsTwitterForAuthToken:Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->access$302(Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;)Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;

    .line 269
    return-void
.end method
