.class Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$1$1;
.super Ljava/lang/Object;
.source "FacebookSSOAPI.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$1;->onResponse(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$1;

.field final synthetic val$account:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$1;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;)V
    .locals 0

    .prologue
    .line 195
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$1$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$1;

    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$1$1;->val$account:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 198
    # getter for: Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->access$000()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "fbaccount  = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$1$1;->val$account:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$1$1;->val$account:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    if-eqz v6, :cond_2

    .line 200
    const-string/jumbo v6, "facebook_account"

    const/4 v7, 0x1

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 202
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$1$1;->val$account:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;->mPic:Ljava/lang/String;

    const-string/jumbo v7, ".jpg"

    invoke-virtual {v6, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 203
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$1$1;->val$account:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;->mPic:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v3

    .line 204
    .local v3, "length":I
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$1$1;->val$account:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$1$1;->val$account:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;->mPic:Ljava/lang/String;

    const/4 v8, 0x0

    add-int/lit8 v9, v3, -0x6

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;->mPic:Ljava/lang/String;

    .line 205
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$1$1;->val$account:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    iget-object v8, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;->mPic:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v8, "_q.jpg"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;->mPic:Ljava/lang/String;

    .line 208
    .end local v3    # "length":I
    :cond_0
    const-string/jumbo v6, "facebook_account_name"

    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$1$1;->val$account:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;->mName:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const-string/jumbo v6, "facebook_picture_url"

    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$1$1;->val$account:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;->mPic:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$1$1;->val$account:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    iget-object v5, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;->mPic:Ljava/lang/String;

    .line 213
    .local v5, "url":Ljava/lang/String;
    :try_start_0
    new-instance v4, Ljava/net/URL;

    invoke-direct {v4, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 214
    .local v4, "picURL":Ljava/net/URL;
    invoke-virtual {v4}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v6

    invoke-static {v6}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 215
    .local v2, "image":Landroid/graphics/Bitmap;
    const/16 v6, 0x8

    invoke-static {v6, v2}, Lcom/vlingo/midas/util/SocialUtils;->setNetworkPicture(ILandroid/graphics/Bitmap;)V

    .line 216
    # getter for: Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->access$100()Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 217
    # getter for: Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->access$100()Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;

    move-result-object v6

    invoke-interface {v6}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;->onChangeAccountInfo()V

    .line 220
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 221
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->continueSocialUpdateAll(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    .end local v0    # "context":Landroid/content/Context;
    .end local v2    # "image":Landroid/graphics/Bitmap;
    .end local v4    # "picURL":Ljava/net/URL;
    .end local v5    # "url":Ljava/lang/String;
    :cond_2
    :goto_0
    return-void

    .line 222
    .restart local v5    # "url":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 223
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 226
    # getter for: Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->access$100()Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 227
    # getter for: Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->access$100()Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;

    move-result-object v6

    invoke-interface {v6}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;->onChangeAccountInfo()V

    goto :goto_0
.end method
