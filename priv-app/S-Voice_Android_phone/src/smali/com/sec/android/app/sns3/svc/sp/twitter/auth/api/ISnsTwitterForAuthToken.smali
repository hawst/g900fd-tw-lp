.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;
.super Ljava/lang/Object;
.source "ISnsTwitterForAuthToken.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken$Stub;
    }
.end annotation


# virtual methods
.method public abstract getAuthTokenInfo()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getMyAccountInfo(Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterCallbackMyAccountInfo;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
