.class final Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$2;
.super Ljava/lang/Object;
.source "FacebookSSOAPI.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->bindFacebookService(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 269
    invoke-static {p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookForAuthToken$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookForAuthToken;

    move-result-object v0

    # setter for: Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->mSnsFacebookForAuthToken:Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookForAuthToken;
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->access$202(Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookForAuthToken;)Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookForAuthToken;

    .line 270
    # getter for: Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->access$100()Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->getFbAccountInfo(Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;)V

    .line 271
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 274
    const/4 v0, 0x0

    # setter for: Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->mSnsFacebookForAuthToken:Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookForAuthToken;
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->access$202(Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookForAuthToken;)Lcom/sec/android/app/sns3/svc/sp/facebook/auth/api/ISnsFacebookForAuthToken;

    .line 275
    return-void
.end method
