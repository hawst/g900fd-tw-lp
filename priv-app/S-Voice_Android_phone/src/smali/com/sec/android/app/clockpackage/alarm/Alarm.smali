.class public Lcom/sec/android/app/clockpackage/alarm/Alarm;
.super Ljava/lang/Object;
.source "Alarm.java"


# static fields
.field public static final ALARM_ACTIVE:I = 0x1

.field public static final ALARM_AT_EVERYDAY:I = 0x1

.field public static final ALARM_AT_ONCE:I = 0x0

.field public static final ALARM_AT_WEEKDAY:I = 0x2

.field public static final ALARM_AT_WEEKLY:I = 0x3

.field public static final ALARM_DATA:Ljava/lang/String; = "com.samsung.sec.android.clockpackage.alarm.ALARM_DATA"

.field public static final ALARM_DEFAULT_VOLUME:I = 0x7

.field public static final ALARM_DURATION_TABLE:[I

.field public static final ALARM_INACTIVE:I = 0x0

.field public static final ALARM_SNOOZE:I = 0x2

.field public static final ALARM_SNOOZE_COUNT_TABLE:[I

.field public static final ALARM_SUBDUE:I = 0x3

.field public static final ALARM_SUBDUE_NEXT:I = 0xa

.field public static final BROADCAST_ALARM:Ljava/lang/String; = "com.samsung.sec.android.clockpackage.alarm.ALARM_ALERT"

.field public static final DEBUG_ENG:Z = false

.field public static final NOTIFY_ALARM_CHANGE:Ljava/lang/String; = "com.samsung.sec.android.clockpackage.alarm.NOTIFY_ALARM_CHANGE"

.field private static final TAG:Ljava/lang/String; = "Alarm"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x5

    .line 26
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_SNOOZE_COUNT_TABLE:[I

    .line 27
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/clockpackage/alarm/Alarm;->ALARM_DURATION_TABLE:[I

    .line 9
    return-void

    .line 26
    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x5
        0xa
    .end array-data

    .line 27
    :array_1
    .array-data 4
        0x3
        0x5
        0xa
        0xf
        0x1e
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final digitToAlphabetStr(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "oldStr"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0x9

    .line 47
    const/4 v1, 0x0

    .line 48
    .local v1, "i":I
    const/16 v3, 0xa

    new-array v0, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "A"

    aput-object v4, v0, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "B"

    aput-object v4, v0, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "C"

    aput-object v4, v0, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "D"

    aput-object v4, v0, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "E"

    aput-object v4, v0, v3

    const/4 v3, 0x5

    const-string/jumbo v4, "F"

    aput-object v4, v0, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "G"

    aput-object v4, v0, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "H"

    aput-object v4, v0, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "I"

    aput-object v4, v0, v3

    const-string/jumbo v3, "J"

    aput-object v3, v0, v5

    .line 49
    .local v0, "alphabet":[Ljava/lang/String;
    move-object v2, p0

    .line 51
    .local v2, "newStr":Ljava/lang/String;
    const-string/jumbo v3, ":"

    const-string/jumbo v4, "##"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 52
    const/4 v1, 0x0

    :goto_0
    if-le v1, v5, :cond_0

    .line 60
    return-object v2

    .line 53
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aget-object v4, v0, v1

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 52
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static isMirroringEnabled()Z
    .locals 4

    .prologue
    .line 31
    const/4 v1, 0x0

    .line 33
    .local v1, "isMirroringEnabled":Z
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string/jumbo v3, "CscFeature_Common_EnableUiDisplayMirroring"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 34
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "curLanguage":Ljava/lang/String;
    const-string/jumbo v2, "ar"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "iw"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "he"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 37
    :cond_0
    const/4 v1, 0x1

    .line 41
    .end local v0    # "curLanguage":Ljava/lang/String;
    :cond_1
    return v1
.end method
