.class Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$1$1;
.super Ljava/lang/Object;
.source "TwitterSSOAPI.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$1;->onResponse(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$1;

.field final synthetic val$account:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$1;Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;)V
    .locals 0

    .prologue
    .line 195
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$1$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$1;

    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$1$1;->val$account:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 198
    # getter for: Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->access$000()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "twaccount  = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$1$1;->val$account:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$1$1;->val$account:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;

    if-eqz v6, :cond_1

    .line 200
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    # setter for: Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->lastSuccessfullUpdate:J
    invoke-static {v6, v7}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->access$102(J)J

    .line 201
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 202
    .local v2, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v6, "twitter_account_name"

    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$1$1;->val$account:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;->mName:Ljava/lang/String;

    invoke-interface {v2, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 203
    const-string/jumbo v6, "twitter_picture_url"

    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$1$1;->val$account:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;->mPic:Ljava/lang/String;

    invoke-interface {v2, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 204
    invoke-static {v2}, Lcom/vlingo/core/internal/settings/Settings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    .line 205
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$1$1;->val$account:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;

    iget-object v5, v6, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;->mPic:Ljava/lang/String;

    .line 208
    .local v5, "url":Ljava/lang/String;
    :try_start_0
    new-instance v4, Ljava/net/URL;

    invoke-direct {v4, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 209
    .local v4, "picURL":Ljava/net/URL;
    invoke-virtual {v4}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v6

    invoke-static {v6}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 210
    .local v3, "image":Landroid/graphics/Bitmap;
    const/4 v6, 0x4

    invoke-static {v6, v3}, Lcom/vlingo/midas/util/SocialUtils;->setNetworkPicture(ILandroid/graphics/Bitmap;)V

    .line 211
    # getter for: Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->access$200()Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 212
    # getter for: Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->ssoCallback:Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->access$200()Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;

    move-result-object v6

    invoke-interface {v6}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;->onChangeAccountInfo()V

    .line 215
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 216
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->continueSocialUpdateAll(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    .end local v0    # "context":Landroid/content/Context;
    .end local v2    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v3    # "image":Landroid/graphics/Bitmap;
    .end local v4    # "picURL":Ljava/net/URL;
    .end local v5    # "url":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 217
    .restart local v2    # "editor":Landroid/content/SharedPreferences$Editor;
    .restart local v5    # "url":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 218
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
