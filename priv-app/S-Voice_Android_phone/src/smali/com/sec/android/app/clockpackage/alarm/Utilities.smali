.class public Lcom/sec/android/app/clockpackage/alarm/Utilities;
.super Ljava/lang/Object;
.source "Utilities.java"


# static fields
.field public static final ACTION_ALARM_CHANGED:Ljava/lang/String; = "android.intent.action.ALARM_CHANGED"

.field public static final EXTRA_ALARMSET:Ljava/lang/String; = "alarmSet"

.field private static mBuilder:Ljava/lang/StringBuilder;

.field private static mFmt:Ljava/util/Formatter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 11
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v0, Lcom/sec/android/app/clockpackage/alarm/Utilities;->mBuilder:Ljava/lang/StringBuilder;

    .line 13
    new-instance v0, Ljava/util/Formatter;

    sget-object v1, Lcom/sec/android/app/clockpackage/alarm/Utilities;->mBuilder:Ljava/lang/StringBuilder;

    invoke-direct {v0, v1}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;)V

    sput-object v0, Lcom/sec/android/app/clockpackage/alarm/Utilities;->mFmt:Ljava/util/Formatter;

    .line 9
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final getAmPmHour(I)I
    .locals 2
    .param p0, "h"    # I

    .prologue
    .line 90
    const/4 v0, 0x1

    .line 92
    .local v0, "mul":I
    const/16 v1, 0xc

    if-ge p0, v1, :cond_0

    .line 94
    const/4 v0, -0x1

    .line 98
    :cond_0
    rem-int/lit8 p0, p0, 0xc

    .line 100
    if-nez p0, :cond_1

    .line 102
    const/16 p0, 0xc

    .line 104
    :cond_1
    mul-int v1, v0, p0

    return v1
.end method

.method public static final toDigitString(I)Ljava/lang/String;
    .locals 4
    .param p0, "number"    # I

    .prologue
    const/4 v3, 0x0

    .line 35
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/Object;

    .line 37
    .local v0, "mArgs":[Ljava/lang/Object;
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    .line 39
    sget-object v1, Lcom/sec/android/app/clockpackage/alarm/Utilities;->mBuilder:Ljava/lang/StringBuilder;

    sget-object v2, Lcom/sec/android/app/clockpackage/alarm/Utilities;->mBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v1, v3, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 41
    sget-object v1, Lcom/sec/android/app/clockpackage/alarm/Utilities;->mFmt:Ljava/util/Formatter;

    const-string/jumbo v2, "%d"

    invoke-virtual {v1, v2, v0}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 43
    sget-object v1, Lcom/sec/android/app/clockpackage/alarm/Utilities;->mFmt:Ljava/util/Formatter;

    invoke-virtual {v1}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static final toTwoDigitString(I)Ljava/lang/String;
    .locals 4
    .param p0, "number"    # I

    .prologue
    const/4 v3, 0x0

    .line 21
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/Object;

    .line 23
    .local v0, "mArgs":[Ljava/lang/Object;
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    .line 25
    sget-object v1, Lcom/sec/android/app/clockpackage/alarm/Utilities;->mBuilder:Ljava/lang/StringBuilder;

    sget-object v2, Lcom/sec/android/app/clockpackage/alarm/Utilities;->mBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v1, v3, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 27
    sget-object v1, Lcom/sec/android/app/clockpackage/alarm/Utilities;->mFmt:Ljava/util/Formatter;

    const-string/jumbo v2, "%02d"

    invoke-virtual {v1, v2, v0}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 29
    sget-object v1, Lcom/sec/android/app/clockpackage/alarm/Utilities;->mFmt:Ljava/util/Formatter;

    invoke-virtual {v1}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static final updateIndicatorAlarm(Landroid/content/Context;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 49
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 50
    sget-object v1, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 52
    const-string/jumbo v3, "active > 0"

    move-object v4, v2

    move-object v5, v2

    .line 49
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 55
    .local v8, "c":Landroid/database/Cursor;
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 57
    .local v7, "alarmCount":I
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 59
    const/4 v8, 0x0

    .line 61
    new-instance v6, Landroid/content/Intent;

    const-string/jumbo v0, "android.intent.action.ALARM_CHANGED"

    invoke-direct {v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 63
    .local v6, "alarmChanged":Landroid/content/Intent;
    const-string/jumbo v1, "alarmSet"

    if-lez v7, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v6, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 65
    invoke-virtual {p0, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 67
    return-void

    .line 63
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
