.class public Lcom/sec/android/app/clockpackage/alarm/AutoPowerUp;
.super Ljava/lang/Object;
.source "AutoPowerUp.java"


# static fields
.field static final KEY_AUTO_POWER_UP:Ljava/lang/String; = "auto_power_up"

.field private static final TAG:Ljava/lang/String; = "AutoPowerUp"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDataString(ZJ)Ljava/lang/String;
    .locals 11
    .param p0, "isEnable"    # Z
    .param p1, "time"    # J

    .prologue
    const/16 v10, 0xa

    .line 78
    const-string/jumbo v2, "0000000000000"

    .line 84
    .local v2, "mReturnString":Ljava/lang/String;
    if-eqz p0, :cond_0

    const-wide/16 v8, -0x1

    cmp-long v8, p1, v8

    if-nez v8, :cond_1

    :cond_0
    move-object v3, v2

    .line 112
    .end local v2    # "mReturnString":Ljava/lang/String;
    .local v3, "mReturnString":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 90
    .end local v3    # "mReturnString":Ljava/lang/String;
    .restart local v2    # "mReturnString":Ljava/lang/String;
    :cond_1
    new-instance v6, Landroid/text/format/Time;

    const-string/jumbo v8, "UTC"

    invoke-direct {v6, v8}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 93
    .local v6, "t":Landroid/text/format/Time;
    const-wide/32 v8, 0xea60

    sub-long v8, p1, v8

    invoke-virtual {v6, v8, v9}, Landroid/text/format/Time;->set(J)V

    .line 95
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/text/format/Time;->normalize(Z)J

    .line 96
    new-instance v8, Ljava/lang/StringBuilder;

    iget v9, v6, Landroid/text/format/Time;->year:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 99
    .local v7, "yearString":Ljava/lang/String;
    iget v8, v6, Landroid/text/format/Time;->month:I

    add-int/lit8 v8, v8, 0x1

    if-ge v8, v10, :cond_2

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "0"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v9, v6, Landroid/text/format/Time;->month:I

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 102
    .local v5, "monthString":Ljava/lang/String;
    :goto_1
    iget v8, v6, Landroid/text/format/Time;->monthDay:I

    if-ge v8, v10, :cond_3

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "0"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v9, v6, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 105
    .local v0, "dayString":Ljava/lang/String;
    :goto_2
    iget v8, v6, Landroid/text/format/Time;->hour:I

    if-ge v8, v10, :cond_4

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "0"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v9, v6, Landroid/text/format/Time;->hour:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 107
    .local v1, "hourString":Ljava/lang/String;
    :goto_3
    iget v8, v6, Landroid/text/format/Time;->minute:I

    if-ge v8, v10, :cond_5

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "0"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v9, v6, Landroid/text/format/Time;->minute:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 109
    .local v4, "minuteString":Ljava/lang/String;
    :goto_4
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "1"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 110
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 109
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 112
    .end local v2    # "mReturnString":Ljava/lang/String;
    .restart local v3    # "mReturnString":Ljava/lang/String;
    goto/16 :goto_0

    .line 100
    .end local v0    # "dayString":Ljava/lang/String;
    .end local v1    # "hourString":Ljava/lang/String;
    .end local v3    # "mReturnString":Ljava/lang/String;
    .end local v4    # "minuteString":Ljava/lang/String;
    .end local v5    # "monthString":Ljava/lang/String;
    .restart local v2    # "mReturnString":Ljava/lang/String;
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    iget v9, v6, Landroid/text/format/Time;->month:I

    add-int/lit8 v9, v9, 0x1

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 102
    .restart local v5    # "monthString":Ljava/lang/String;
    :cond_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 103
    iget v9, v6, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 105
    .restart local v0    # "dayString":Ljava/lang/String;
    :cond_4
    new-instance v8, Ljava/lang/StringBuilder;

    iget v9, v6, Landroid/text/format/Time;->hour:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 107
    .restart local v1    # "hourString":Ljava/lang/String;
    :cond_5
    new-instance v8, Ljava/lang/StringBuilder;

    iget v9, v6, Landroid/text/format/Time;->minute:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_4
.end method

.method public static sendData(Landroid/content/Context;J)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "time"    # J

    .prologue
    .line 31
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v6

    const-string/jumbo v7, "CscFeature_Clock_ExclusiveEnablingAutoPowerSetting"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 32
    const/4 v2, 0x0

    .line 34
    .local v2, "isAutoPowerOnOffEnable":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v7, "auto_power_on_off"

    invoke-static {v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 41
    :goto_0
    if-lez v2, :cond_1

    .line 60
    .end local v2    # "isAutoPowerOnOffEnable":I
    :cond_0
    :goto_1
    return-void

    .line 35
    .restart local v2    # "isAutoPowerOnOffEnable":I
    :catch_0
    move-exception v1

    .line 38
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string/jumbo v6, "AutoPowerUp"

    const-string/jumbo v7, "Can\'t find isAutoPowerOnOffEnable : "

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 46
    .end local v1    # "e":Landroid/provider/Settings$SettingNotFoundException;
    .end local v2    # "isAutoPowerOnOffEnable":I
    :cond_1
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 48
    .local v5, "sp":Landroid/content/SharedPreferences;
    const-string/jumbo v6, "auto_power_up"

    const/4 v7, 0x1

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 49
    .local v3, "isAutoPowerUp":Z
    const-string/jumbo v6, "alarm"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 51
    .local v0, "am":Landroid/app/AlarmManager;
    invoke-static {v3, p1, p2}, Lcom/sec/android/app/clockpackage/alarm/AutoPowerUp;->getDataString(ZJ)Ljava/lang/String;

    move-result-object v4

    .line 53
    .local v4, "mData":Ljava/lang/String;
    const-string/jumbo v6, ""

    if-eq v4, v6, :cond_0

    if-eqz v4, :cond_0

    .line 57
    const-string/jumbo v6, "AutoPowerUp"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "enabled: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", time: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", mData: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    invoke-virtual {v0, v4}, Landroid/app/AlarmManager;->setAutoPowerUp(Ljava/lang/String;)V

    goto :goto_1
.end method
