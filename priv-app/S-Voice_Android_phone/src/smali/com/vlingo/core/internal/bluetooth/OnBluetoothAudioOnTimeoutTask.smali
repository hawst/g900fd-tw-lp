.class public final Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;
.super Ljava/lang/Object;
.source "OnBluetoothAudioOnTimeoutTask.java"

# interfaces
.implements Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;


# instance fields
.field onBluetoothOnTask:Ljava/lang/Runnable;

.field onBluetoothOnTaskThreadHandler:Landroid/os/Handler;

.field private timeoutTask:Ljava/util/TimerTask;

.field private timer:Ljava/util/Timer;


# direct methods
.method private constructor <init>(JLjava/lang/Runnable;Landroid/os/Handler;)V
    .locals 2
    .param p1, "timeout"    # J
    .param p3, "task"    # Ljava/lang/Runnable;
    .param p4, "taskThreadHandler"    # Landroid/os/Handler;

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object v0, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->timer:Ljava/util/Timer;

    .line 18
    iput-object v0, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->timeoutTask:Ljava/util/TimerTask;

    .line 19
    iput-object v0, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->onBluetoothOnTask:Ljava/lang/Runnable;

    .line 20
    iput-object v0, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->onBluetoothOnTaskThreadHandler:Landroid/os/Handler;

    .line 34
    iput-object p3, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->onBluetoothOnTask:Ljava/lang/Runnable;

    .line 35
    iput-object p4, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->onBluetoothOnTaskThreadHandler:Landroid/os/Handler;

    .line 37
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 39
    invoke-static {}, Lcom/vlingo/core/internal/util/TimerSingleton;->getTimer()Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->timer:Ljava/util/Timer;

    .line 40
    new-instance v0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask$1;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask$1;-><init>(Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->timeoutTask:Ljava/util/TimerTask;

    .line 49
    iget-object v0, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->timeoutTask:Ljava/util/TimerTask;

    invoke-virtual {v0, v1, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 52
    :cond_0
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->addListener(Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;)V

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->performTask()V

    return-void
.end method

.method private declared-synchronized performTask()V
    .locals 4

    .prologue
    .line 57
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->removeListener(Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;)V

    .line 59
    iget-object v0, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->timeoutTask:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->timeoutTask:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->timeoutTask:Ljava/util/TimerTask;

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->onBluetoothOnTask:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 67
    iget-object v0, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->onBluetoothOnTaskThreadHandler:Landroid/os/Handler;

    if-nez v0, :cond_2

    .line 70
    iget-object v0, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->onBluetoothOnTask:Ljava/lang/Runnable;

    const-wide/16 v1, 0xc8

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V

    .line 76
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->onBluetoothOnTask:Ljava/lang/Runnable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    :cond_1
    monitor-exit p0

    return-void

    .line 74
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->onBluetoothOnTaskThreadHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->onBluetoothOnTask:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static runTaskOnBluetoothAudioOn(JLjava/lang/Runnable;)Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;
    .locals 2
    .param p0, "timeout"    # J
    .param p2, "task"    # Ljava/lang/Runnable;

    .prologue
    .line 24
    new-instance v0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;-><init>(JLjava/lang/Runnable;Landroid/os/Handler;)V

    return-object v0
.end method

.method public static runTaskOnBluetoothAudioOn(JLjava/lang/Runnable;Landroid/os/Handler;)Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;
    .locals 1
    .param p0, "timeout"    # J
    .param p2, "task"    # Ljava/lang/Runnable;
    .param p3, "taskThreadHandler"    # Landroid/os/Handler;

    .prologue
    .line 29
    new-instance v0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;-><init>(JLjava/lang/Runnable;Landroid/os/Handler;)V

    return-object v0
.end method


# virtual methods
.method public declared-synchronized cancel()V
    .locals 1

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->removeListener(Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;)V

    .line 86
    iget-object v0, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->timeoutTask:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->timeoutTask:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->timeoutTask:Ljava/util/TimerTask;

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->onBluetoothOnTask:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->onBluetoothOnTask:Ljava/lang/Runnable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    :cond_1
    monitor-exit p0

    return-void

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onBluetoothServiceConnected()V
    .locals 0

    .prologue
    .line 112
    return-void
.end method

.method public onHeadsetConnected()V
    .locals 0

    .prologue
    .line 118
    return-void
.end method

.method public onHeadsetDisconnected()V
    .locals 0

    .prologue
    .line 124
    return-void
.end method

.method public onScoConnected()V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->performTask()V

    .line 101
    return-void
.end method

.method public onScoDisconnected()V
    .locals 0

    .prologue
    .line 106
    return-void
.end method
