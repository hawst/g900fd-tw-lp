.class public Lcom/vlingo/core/internal/contacts/contentprovider/ContactDatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "ContactDatabaseHelper.java"


# static fields
.field public static final DATABASE_NAME:Ljava/lang/String; = "contactsManager.db"

.field private static final DATABASE_VERSION:I = 0x3


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    const-string/jumbo v0, "contactsManager.db"

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 19
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 23
    invoke-static {p1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 25
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 29
    invoke-static {p1, p2, p3}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 31
    return-void
.end method
