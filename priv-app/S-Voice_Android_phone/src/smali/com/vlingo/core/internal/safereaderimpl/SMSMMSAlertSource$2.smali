.class Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$2;
.super Landroid/content/BroadcastReceiver;
.source "SMSMMSAlertSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;)V
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$2;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 268
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 270
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 272
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->resetMsgReadoutTimestamp()V

    .line 274
    :cond_1
    return-void
.end method
