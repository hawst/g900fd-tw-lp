.class final Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder$1;
.super Ljava/lang/Object;
.source "ServerPreferenceBuilder.java"

# interfaces
.implements Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceUpdated(Landroid/preference/Preference;)V
    .locals 1
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 27
    move-object v0, p1

    check-cast v0, Lcom/vlingo/core/internal/debug/ServerPreferenceValue;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/debug/ServerPreferenceValue;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 28
    return-void
.end method
