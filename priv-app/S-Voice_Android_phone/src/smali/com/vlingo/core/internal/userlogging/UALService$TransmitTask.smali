.class Lcom/vlingo/core/internal/userlogging/UALService$TransmitTask;
.super Ljava/util/TimerTask;
.source "UALService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/userlogging/UALService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TransmitTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/userlogging/UALService;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/userlogging/UALService;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/vlingo/core/internal/userlogging/UALService$TransmitTask;->this$0:Lcom/vlingo/core/internal/userlogging/UALService;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/userlogging/UALService;Lcom/vlingo/core/internal/userlogging/UALService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/userlogging/UALService;
    .param p2, "x1"    # Lcom/vlingo/core/internal/userlogging/UALService$1;

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/userlogging/UALService$TransmitTask;-><init>(Lcom/vlingo/core/internal/userlogging/UALService;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 126
    iget-object v1, p0, Lcom/vlingo/core/internal/userlogging/UALService$TransmitTask;->this$0:Lcom/vlingo/core/internal/userlogging/UALService;

    monitor-enter v1

    .line 127
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/userlogging/UALService$TransmitTask;->this$0:Lcom/vlingo/core/internal/userlogging/UALService;

    # getter for: Lcom/vlingo/core/internal/userlogging/UALService;->mTransmitTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/vlingo/core/internal/userlogging/UALService;->access$300(Lcom/vlingo/core/internal/userlogging/UALService;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/vlingo/core/internal/userlogging/UALService$TransmitTask;->this$0:Lcom/vlingo/core/internal/userlogging/UALService;

    # getter for: Lcom/vlingo/core/internal/userlogging/UALService;->mTransmitTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/vlingo/core/internal/userlogging/UALService;->access$300(Lcom/vlingo/core/internal/userlogging/UALService;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 129
    iget-object v0, p0, Lcom/vlingo/core/internal/userlogging/UALService$TransmitTask;->this$0:Lcom/vlingo/core/internal/userlogging/UALService;

    const/4 v2, 0x0

    # setter for: Lcom/vlingo/core/internal/userlogging/UALService;->mTransmitTimer:Ljava/util/Timer;
    invoke-static {v0, v2}, Lcom/vlingo/core/internal/userlogging/UALService;->access$302(Lcom/vlingo/core/internal/userlogging/UALService;Ljava/util/Timer;)Ljava/util/Timer;

    .line 130
    iget-object v0, p0, Lcom/vlingo/core/internal/userlogging/UALService$TransmitTask;->this$0:Lcom/vlingo/core/internal/userlogging/UALService;

    # invokes: Lcom/vlingo/core/internal/userlogging/UALService;->transmitData()V
    invoke-static {v0}, Lcom/vlingo/core/internal/userlogging/UALService;->access$400(Lcom/vlingo/core/internal/userlogging/UALService;)V

    .line 132
    :cond_0
    monitor-exit v1

    .line 133
    return-void

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
