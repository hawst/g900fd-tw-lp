.class public Lcom/vlingo/core/internal/contacts/ContactDBUtil;
.super Ljava/lang/Object;
.source "ContactDBUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/contacts/IContactDBUtil;


# static fields
.field public static final EVENT_LUNAR_CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.pantech.cursor.item/lunar_event"

.field protected static final RULE_SETS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/vlingo/core/internal/contacts/ContactDBUtil;

.field static step:I


# instance fields
.field private mSkipNormalCheck:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 51
    const-class v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->TAG:Ljava/lang/String;

    .line 59
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->instance:Lcom/vlingo/core/internal/contacts/ContactDBUtil;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->RULE_SETS:Ljava/util/List;

    .line 63
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->RULE_SETS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->RULE_SETS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/rules/KoreanSamelessContactRuleSet;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/rules/KoreanSamelessContactRuleSet;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->RULE_SETS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->RULE_SETS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->RULE_SETS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/rules/FrenchContactRuleSet;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/rules/FrenchContactRuleSet;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->RULE_SETS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/rules/EFIGSContactRuleSet;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/rules/EFIGSContactRuleSet;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    const/4 v0, 0x0

    sput v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->step:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->mSkipNormalCheck:Z

    return-void
.end method

.method protected static applyScores(Ljava/util/HashMap;Ljava/util/HashMap;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;Lcom/vlingo/core/internal/contacts/ContactExitCriteria;Ljava/lang/String;)V
    .locals 6
    .param p2, "scorer"    # Lcom/vlingo/core/internal/contacts/scoring/ContactScore;
    .param p3, "exitCriteria"    # Lcom/vlingo/core/internal/contacts/ContactExitCriteria;
    .param p4, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Lcom/vlingo/core/internal/contacts/scoring/ContactScore;",
            "Lcom/vlingo/core/internal/contacts/ContactExitCriteria;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "matchTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p1, "localMatches":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/high16 v5, 0x40a00000    # 5.0f

    .line 513
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 514
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 515
    .local v2, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {p2, p4, v2}, Lcom/vlingo/core/internal/contacts/scoring/ContactScore;->getScore(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactMatch;)I

    move-result v3

    int-to-float v3, v3

    iput v3, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    .line 516
    iget v3, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    float-to-int v3, v3

    invoke-virtual {p3, v3}, Lcom/vlingo/core/internal/contacts/ContactExitCriteria;->tallyScore(I)Z

    goto :goto_0

    .line 518
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v2    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_0
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 519
    .restart local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 520
    .restart local v2    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget v3, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    float-to-int v3, v3

    invoke-virtual {p3, v3}, Lcom/vlingo/core/internal/contacts/ContactExitCriteria;->keepMatch(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 522
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget v3, v3, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    iget v4, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    div-float/2addr v4, v5

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 523
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 526
    :cond_2
    iget v3, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    div-float/2addr v3, v5

    iput v3, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    goto :goto_1

    .line 528
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v2    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_3
    return-void
.end method

.method private getContactCursor(Landroid/content/Context;)Landroid/database/Cursor;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 973
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 978
    :goto_0
    return-object v4

    .line 976
    :cond_0
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string/jumbo v1, "data2"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string/jumbo v1, "data3"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string/jumbo v1, "data1"

    aput-object v1, v2, v0

    .line 977
    .local v2, "proj":[Ljava/lang/String;
    const-string/jumbo v3, "mimetype=\'vnd.android.cursor.item/name\' AND in_visible_group=1"

    .line 978
    .local v3, "where":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    goto :goto_0
.end method

.method private getContactDetails(Landroid/content/Context;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)I
    .locals 44
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 539
    .local p2, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    .local p3, "matchTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p4, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/16 v32, 0x0

    .line 540
    .local v32, "count":I
    const/4 v2, 0x7

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "contact_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "data1"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "data2"

    aput-object v3, v4, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "data3"

    aput-object v3, v4, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "mimetype"

    aput-object v3, v4, v2

    const/4 v2, 0x5

    const-string/jumbo v3, "is_super_primary"

    aput-object v3, v4, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "data_id"

    aput-object v3, v4, v2

    .line 553
    .local v4, "proj":[Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 554
    const/4 v2, 0x0

    .line 658
    :goto_0
    return v2

    .line 580
    .local v5, "where":Ljava/lang/String;
    .local v6, "rawContactIDsString":[Ljava/lang/String;
    .local v22, "c":Landroid/database/Cursor;
    .local v23, "colIndexContactID":I
    .local v24, "colIndexData1":I
    .local v25, "colIndexData2":I
    .local v26, "colIndexData3":I
    .local v27, "colIsDefault":I
    .local v28, "colMime":I
    .local v29, "coldataId":I
    .local v34, "fetchCount":I
    .local v35, "fetchList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .local v36, "firstTime":Z
    .local v37, "i":I
    .local v38, "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract$RawContactsEntity;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v7, "contact_id"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 585
    const-wide/16 v40, -0x1

    .line 587
    .local v40, "lastContactID":J
    if-eqz v22, :cond_4

    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 588
    if-eqz v36, :cond_1

    .line 589
    const-string/jumbo v2, "contact_id"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 590
    const-string/jumbo v2, "data1"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 591
    const-string/jumbo v2, "data2"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    .line 592
    const-string/jumbo v2, "data3"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    .line 593
    const-string/jumbo v2, "mimetype"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    .line 594
    const-string/jumbo v2, "is_super_primary"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    .line 595
    const-string/jumbo v2, "data_id"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v29

    .line 596
    const/16 v36, 0x0

    .line 598
    :cond_1
    const/4 v8, 0x0

    .line 600
    .local v8, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_2
    invoke-interface/range {v22 .. v23}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v30

    .line 601
    .local v30, "contactID":J
    cmp-long v2, v40, v30

    if-nez v2, :cond_3

    if-nez v8, :cond_6

    .line 602
    :cond_3
    move-wide/from16 v40, v30

    .line 603
    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    check-cast v8, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 604
    .restart local v8    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-nez v8, :cond_6

    .line 650
    :goto_1
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_2

    .line 653
    .end local v8    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v30    # "contactID":J
    :cond_4
    if-eqz v22, :cond_5

    .line 654
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 556
    .end local v5    # "where":Ljava/lang/String;
    .end local v6    # "rawContactIDsString":[Ljava/lang/String;
    .end local v22    # "c":Landroid/database/Cursor;
    .end local v23    # "colIndexContactID":I
    .end local v24    # "colIndexData1":I
    .end local v25    # "colIndexData2":I
    .end local v26    # "colIndexData3":I
    .end local v27    # "colIsDefault":I
    .end local v28    # "colMime":I
    .end local v29    # "coldataId":I
    .end local v34    # "fetchCount":I
    .end local v35    # "fetchList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v36    # "firstTime":Z
    .end local v37    # "i":I
    .end local v38    # "i$":Ljava/util/Iterator;
    .end local v40    # "lastContactID":J
    :cond_5
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_10

    .line 558
    const/16 v2, 0x1f4

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v34

    .line 559
    .restart local v34    # "fetchCount":I
    const/4 v2, 0x0

    move-object/from16 v0, p4

    move/from16 v1, v34

    invoke-interface {v0, v2, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v35

    .line 561
    .restart local v35    # "fetchList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, p4

    move/from16 v1, v34

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p4

    .line 563
    move-object/from16 v0, v35

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/contacts/ContactDBQueryUtil;->getWhereForContactDetails(Ljava/util/List;Ljava/util/EnumSet;)Ljava/lang/String;

    move-result-object v5

    .line 565
    .restart local v5    # "where":Ljava/lang/String;
    const/16 v36, 0x1

    .line 566
    .restart local v36    # "firstTime":Z
    const/16 v23, 0x0

    .restart local v23    # "colIndexContactID":I
    const/16 v24, 0x0

    .restart local v24    # "colIndexData1":I
    const/16 v25, 0x0

    .restart local v25    # "colIndexData2":I
    const/16 v26, 0x0

    .restart local v26    # "colIndexData3":I
    const/16 v28, 0x0

    .restart local v28    # "colMime":I
    const/16 v27, 0x0

    .restart local v27    # "colIsDefault":I
    const/16 v29, 0x0

    .line 567
    .restart local v29    # "coldataId":I
    const/16 v22, 0x0

    .line 572
    .restart local v22    # "c":Landroid/database/Cursor;
    :try_start_1
    move/from16 v0, v34

    new-array v6, v0, [Ljava/lang/String;

    .line 574
    .restart local v6    # "rawContactIDsString":[Ljava/lang/String;
    const/16 v37, 0x0

    .line 575
    .restart local v37    # "i":I
    invoke-interface/range {v35 .. v35}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v38

    .restart local v38    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/Long;

    .line 576
    .local v39, "l":Ljava/lang/Long;
    invoke-static/range {v39 .. v39}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v37

    .line 577
    add-int/lit8 v37, v37, 0x1

    goto :goto_2

    .line 608
    .end local v39    # "l":Ljava/lang/Long;
    .restart local v8    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v30    # "contactID":J
    .restart local v40    # "lastContactID":J
    :cond_6
    const/4 v7, 0x0

    .line 609
    .local v7, "data":Lcom/vlingo/core/internal/contacts/ContactData;
    move-object/from16 v0, v22

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 610
    .local v10, "data1":Ljava/lang/String;
    move-object/from16 v0, v22

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 611
    .local v18, "data2":I
    move-object/from16 v0, v22

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 612
    .local v11, "label":Ljava/lang/String;
    move-object/from16 v0, v22

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v42

    .line 613
    .local v42, "mime":Ljava/lang/String;
    move-object/from16 v0, v22

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 614
    .local v13, "dataId":I
    move-object/from16 v0, v22

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 615
    .local v12, "isDefault":I
    const-string/jumbo v2, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, v42

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 616
    invoke-static {v11}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    if-nez v18, :cond_8

    .line 617
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    sget-object v9, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Phone:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    invoke-direct/range {v7 .. v13}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;Ljava/lang/String;II)V

    .line 621
    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    :goto_3
    invoke-virtual {v8, v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addPhone(Lcom/vlingo/core/internal/contacts/ContactData;)V

    .line 649
    :cond_7
    :goto_4
    add-int/lit8 v32, v32, 0x1

    goto/16 :goto_1

    .line 619
    :cond_8
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    sget-object v16, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Phone:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    move-object v14, v7

    move-object v15, v8

    move-object/from16 v17, v10

    move/from16 v19, v12

    move/from16 v20, v13

    invoke-direct/range {v14 .. v20}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;III)V

    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    goto :goto_3

    .line 622
    :cond_9
    const-string/jumbo v2, "vnd.android.cursor.item/email_v2"

    move-object/from16 v0, v42

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 623
    invoke-static {v11}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b

    if-nez v18, :cond_b

    .line 624
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    sget-object v9, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Email:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    invoke-direct/range {v7 .. v12}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;Ljava/lang/String;I)V

    .line 628
    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    :goto_5
    invoke-virtual {v8, v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addEmail(Lcom/vlingo/core/internal/contacts/ContactData;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 653
    .end local v6    # "rawContactIDsString":[Ljava/lang/String;
    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v8    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v10    # "data1":Ljava/lang/String;
    .end local v11    # "label":Ljava/lang/String;
    .end local v12    # "isDefault":I
    .end local v13    # "dataId":I
    .end local v18    # "data2":I
    .end local v30    # "contactID":J
    .end local v37    # "i":I
    .end local v38    # "i$":Ljava/util/Iterator;
    .end local v40    # "lastContactID":J
    .end local v42    # "mime":Ljava/lang/String;
    :catchall_0
    move-exception v2

    if-eqz v22, :cond_a

    .line 654
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v2

    .line 626
    .restart local v6    # "rawContactIDsString":[Ljava/lang/String;
    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    .restart local v8    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v10    # "data1":Ljava/lang/String;
    .restart local v11    # "label":Ljava/lang/String;
    .restart local v12    # "isDefault":I
    .restart local v13    # "dataId":I
    .restart local v18    # "data2":I
    .restart local v30    # "contactID":J
    .restart local v37    # "i":I
    .restart local v38    # "i$":Ljava/util/Iterator;
    .restart local v40    # "lastContactID":J
    .restart local v42    # "mime":Ljava/lang/String;
    :cond_b
    :try_start_2
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    sget-object v16, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Email:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    move-object v14, v7

    move-object v15, v8

    move-object/from16 v17, v10

    move/from16 v19, v12

    invoke-direct/range {v14 .. v19}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    goto :goto_5

    .line 629
    :cond_c
    const-string/jumbo v2, "vnd.android.cursor.item/postal-address_v2"

    move-object/from16 v0, v42

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 630
    invoke-static {v11}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_d

    if-nez v18, :cond_d

    .line 631
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    sget-object v9, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Address:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    invoke-direct/range {v7 .. v12}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;Ljava/lang/String;I)V

    .line 635
    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    :goto_6
    invoke-virtual {v8, v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addAddress(Lcom/vlingo/core/internal/contacts/ContactData;)V

    goto :goto_4

    .line 633
    :cond_d
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    sget-object v16, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Address:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    move-object v14, v7

    move-object v15, v8

    move-object/from16 v17, v10

    move/from16 v19, v12

    invoke-direct/range {v14 .. v19}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    goto :goto_6

    .line 636
    :cond_e
    const-string/jumbo v2, "vnd.android.cursor.item/contact_event"

    move-object/from16 v0, v42

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    const-string/jumbo v2, "vnd.pantech.cursor.item/lunar_event"

    move-object/from16 v0, v42

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 637
    :cond_f
    new-instance v43, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy-MM-dd"

    move-object/from16 v0, v43

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 638
    .local v43, "sdfInput":Ljava/text/SimpleDateFormat;
    const/16 v17, 0x0

    .line 640
    .local v17, "formattedBirthday":Ljava/lang/String;
    :try_start_3
    move-object/from16 v0, v43

    invoke-virtual {v0, v10}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v21

    .line 641
    .local v21, "birthday":Ljava/util/Date;
    invoke-static/range {p1 .. p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/text/ParseException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v17

    .line 646
    .end local v21    # "birthday":Ljava/util/Date;
    :goto_7
    :try_start_4
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    sget-object v16, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Birthday:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    move-object v14, v7

    move-object v15, v8

    move/from16 v19, v12

    invoke-direct/range {v14 .. v19}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    .line 647
    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v8, v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addBirthday(Lcom/vlingo/core/internal/contacts/ContactData;)V

    goto/16 :goto_4

    .line 642
    :catch_0
    move-exception v33

    .line 643
    .local v33, "e":Ljava/text/ParseException;
    sget-object v2, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "date string did not match \"yyyy-MM-dd\""

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 644
    move-object/from16 v17, v10

    goto :goto_7

    .end local v5    # "where":Ljava/lang/String;
    .end local v6    # "rawContactIDsString":[Ljava/lang/String;
    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v8    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v10    # "data1":Ljava/lang/String;
    .end local v11    # "label":Ljava/lang/String;
    .end local v12    # "isDefault":I
    .end local v13    # "dataId":I
    .end local v17    # "formattedBirthday":Ljava/lang/String;
    .end local v18    # "data2":I
    .end local v22    # "c":Landroid/database/Cursor;
    .end local v23    # "colIndexContactID":I
    .end local v24    # "colIndexData1":I
    .end local v25    # "colIndexData2":I
    .end local v26    # "colIndexData3":I
    .end local v27    # "colIsDefault":I
    .end local v28    # "colMime":I
    .end local v29    # "coldataId":I
    .end local v30    # "contactID":J
    .end local v33    # "e":Ljava/text/ParseException;
    .end local v34    # "fetchCount":I
    .end local v35    # "fetchList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v36    # "firstTime":Z
    .end local v37    # "i":I
    .end local v38    # "i$":Ljava/util/Iterator;
    .end local v40    # "lastContactID":J
    .end local v42    # "mime":Ljava/lang/String;
    .end local v43    # "sdfInput":Ljava/text/SimpleDateFormat;
    :cond_10
    move/from16 v2, v32

    .line 658
    goto/16 :goto_0
.end method

.method private getExactContactMatchByQuery(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 19
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nameSelection"    # Ljava/lang/String;

    .prologue
    .line 921
    const/16 v1, 0x8

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "contact_id"

    aput-object v2, v3, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "raw_contact_id"

    aput-object v2, v3, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "lookup"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "display_name"

    aput-object v2, v3, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "starred"

    aput-object v2, v3, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "data1"

    aput-object v2, v3, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "times_contacted"

    aput-object v2, v3, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "phonetic_name"

    aput-object v2, v3, v1

    .line 931
    .local v3, "projection":[Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") AND ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "mimetype=\'vnd.android.cursor.item/name\' OR mimetype=\'vnd.android.cursor.item/nickname\' OR mimetype=\'vnd.android.cursor.item/organization\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 933
    .local v4, "selection":Ljava/lang/String;
    const/4 v13, 0x0

    .line 934
    .local v13, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    const/4 v14, 0x0

    .line 938
    .local v14, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 959
    if-eqz v14, :cond_0

    .line 960
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object v5, v13

    .line 964
    .end local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :goto_0
    return-object v13

    .line 941
    .restart local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_1
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 943
    if-eqz v14, :cond_5

    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 944
    const-string/jumbo v1, "contact_id"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    .line 945
    .local v12, "contactId":Ljava/lang/Long;
    const-string/jumbo v1, "lookup"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 946
    .local v10, "lookupKey":Ljava/lang/String;
    const-string/jumbo v1, "display_name"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 947
    .local v6, "displayName":Ljava/lang/String;
    const-string/jumbo v1, "starred"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 948
    .local v17, "starred":I
    const-string/jumbo v1, "times_contacted"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 949
    .local v18, "timesContacted":I
    const-string/jumbo v1, "data1"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 950
    .local v15, "data1":Ljava/lang/String;
    const-string/jumbo v1, "phonetic_name"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 952
    .local v7, "phoneticName":Ljava/lang/String;
    new-instance v5, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v1, 0x1

    move/from16 v0, v17

    if-ne v0, v1, :cond_3

    const/4 v11, 0x1

    :goto_1
    invoke-direct/range {v5 .. v11}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 953
    .end local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .local v5, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :try_start_2
    move/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->setTimesContacted(I)V

    .line 954
    invoke-virtual {v5, v15}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addExtraName(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 959
    .end local v6    # "displayName":Ljava/lang/String;
    .end local v7    # "phoneticName":Ljava/lang/String;
    .end local v10    # "lookupKey":Ljava/lang/String;
    .end local v12    # "contactId":Ljava/lang/Long;
    .end local v15    # "data1":Ljava/lang/String;
    .end local v17    # "starred":I
    .end local v18    # "timesContacted":I
    :goto_2
    if-eqz v14, :cond_2

    .line 960
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_3
    move-object v13, v5

    .line 964
    .end local v5    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    goto/16 :goto_0

    .line 952
    .restart local v6    # "displayName":Ljava/lang/String;
    .restart local v7    # "phoneticName":Ljava/lang/String;
    .restart local v10    # "lookupKey":Ljava/lang/String;
    .restart local v12    # "contactId":Ljava/lang/Long;
    .restart local v15    # "data1":Ljava/lang/String;
    .restart local v17    # "starred":I
    .restart local v18    # "timesContacted":I
    :cond_3
    const/4 v11, 0x0

    goto :goto_1

    .line 956
    .end local v6    # "displayName":Ljava/lang/String;
    .end local v7    # "phoneticName":Ljava/lang/String;
    .end local v10    # "lookupKey":Ljava/lang/String;
    .end local v12    # "contactId":Ljava/lang/Long;
    .end local v15    # "data1":Ljava/lang/String;
    .end local v17    # "starred":I
    .end local v18    # "timesContacted":I
    :catch_0
    move-exception v16

    move-object v5, v13

    .line 957
    .end local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v5    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .local v16, "ex":Ljava/lang/Exception;
    :goto_4
    :try_start_3
    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "getExactContactMatch: Caught Exception: "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v8, "\n"

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static/range {v16 .. v16}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 959
    if-eqz v14, :cond_2

    .line 960
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 959
    .end local v5    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v16    # "ex":Ljava/lang/Exception;
    .restart local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :catchall_0
    move-exception v1

    move-object v5, v13

    .end local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v5    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :goto_5
    if-eqz v14, :cond_4

    .line 960
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v1

    .line 959
    :catchall_1
    move-exception v1

    goto :goto_5

    .line 956
    .restart local v6    # "displayName":Ljava/lang/String;
    .restart local v7    # "phoneticName":Ljava/lang/String;
    .restart local v10    # "lookupKey":Ljava/lang/String;
    .restart local v12    # "contactId":Ljava/lang/Long;
    .restart local v15    # "data1":Ljava/lang/String;
    .restart local v17    # "starred":I
    .restart local v18    # "timesContacted":I
    :catch_1
    move-exception v16

    goto :goto_4

    .end local v5    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v6    # "displayName":Ljava/lang/String;
    .end local v7    # "phoneticName":Ljava/lang/String;
    .end local v10    # "lookupKey":Ljava/lang/String;
    .end local v12    # "contactId":Ljava/lang/Long;
    .end local v15    # "data1":Ljava/lang/String;
    .end local v17    # "starred":I
    .end local v18    # "timesContacted":I
    .restart local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_5
    move-object v5, v13

    .end local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v5    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    goto :goto_2
.end method

.method private static getFacebookAccountTypeAndName(Landroid/content/Context;)[Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 818
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v5

    .line 819
    .local v5, "mgr":Landroid/accounts/AccountManager;
    invoke-virtual {v5}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    .line 820
    .local v1, "accounts":[Landroid/accounts/Account;
    move-object v2, v1

    .local v2, "arr$":[Landroid/accounts/Account;
    array-length v4, v2

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v2, v3

    .line 823
    .local v0, "a":Landroid/accounts/Account;
    iget-object v6, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v6, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "facebook"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 824
    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v8, v6, v7

    .line 827
    .end local v0    # "a":Landroid/accounts/Account;
    :goto_1
    return-object v6

    .line 820
    .restart local v0    # "a":Landroid/accounts/Account;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 827
    .end local v0    # "a":Landroid/accounts/Account;
    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public static declared-synchronized getInstance()Lcom/vlingo/core/internal/contacts/ContactDBUtil;
    .locals 2

    .prologue
    .line 73
    const-class v1, Lcom/vlingo/core/internal/contacts/ContactDBUtil;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->instance:Lcom/vlingo/core/internal/contacts/ContactDBUtil;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;

    invoke-direct {v0}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->instance:Lcom/vlingo/core/internal/contacts/ContactDBUtil;

    .line 76
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->instance:Lcom/vlingo/core/internal/contacts/ContactDBUtil;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getSocialContactData(Landroid/content/Context;Ljava/util/HashMap;Ljava/util/List;)I
    .locals 24
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 680
    .local p2, "matchTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p3, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/16 v16, 0x0

    .line 682
    .local v16, "count":I
    invoke-static/range {p1 .. p1}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->getFacebookAccountTypeAndName(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v18

    .line 684
    .local v18, "facebookAccountTypeAndName":[Ljava/lang/String;
    if-eqz v18, :cond_9

    .line 685
    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v5, "contact_id"

    aput-object v5, v3, v1

    const/4 v1, 0x1

    const-string/jumbo v5, "sourceid"

    aput-object v5, v3, v1

    .line 690
    .local v3, "proj":[Ljava/lang/String;
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    .line 691
    .local v23, "whereBuilder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "_id"

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v5, " IN ("

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 692
    const/16 v17, 0x0

    .line 693
    .local v17, "counter":I
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_0
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v1

    move/from16 v0, v20

    if-ge v0, v1, :cond_1

    .line 694
    const-string/jumbo v1, "?"

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 695
    add-int/lit8 v17, v17, 0x1

    .line 696
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v1

    move/from16 v0, v17

    if-ge v0, v1, :cond_0

    .line 697
    const-string/jumbo v1, ","

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 693
    :cond_0
    add-int/lit8 v20, v20, 0x1

    goto :goto_0

    .line 700
    :cond_1
    const-string/jumbo v1, ")"

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 702
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string/jumbo v5, "account_name"

    const/4 v7, 0x1

    aget-object v7, v18, v7

    invoke-virtual {v1, v5, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string/jumbo v5, "account_type"

    const/4 v7, 0x0

    aget-object v7, v18, v7

    invoke-virtual {v1, v5, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 709
    .local v2, "rawContactUri":Landroid/net/Uri;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 710
    const/4 v1, 0x0

    .line 743
    .end local v2    # "rawContactUri":Landroid/net/Uri;
    .end local v3    # "proj":[Ljava/lang/String;
    .end local v17    # "counter":I
    .end local v20    # "i":I
    .end local v23    # "whereBuilder":Ljava/lang/StringBuilder;
    :goto_1
    return v1

    .line 712
    .restart local v2    # "rawContactUri":Landroid/net/Uri;
    .restart local v3    # "proj":[Ljava/lang/String;
    .restart local v17    # "counter":I
    .restart local v20    # "i":I
    .restart local v23    # "whereBuilder":Ljava/lang/StringBuilder;
    :cond_2
    const/16 v19, 0x1

    .line 713
    .local v19, "firstTime":Z
    const/4 v12, 0x0

    .local v12, "colIndexContactID":I
    const/4 v13, 0x0

    .line 714
    .local v13, "colIndexSource":I
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .local v21, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_2
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Long;

    .line 715
    .local v22, "rawID":Ljava/lang/Long;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "_id="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v22

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 716
    .local v4, "where":Ljava/lang/String;
    const/4 v11, 0x0

    .line 718
    .local v11, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 719
    if-eqz v11, :cond_7

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 720
    if-eqz v19, :cond_4

    .line 721
    const-string/jumbo v1, "contact_id"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 722
    const-string/jumbo v1, "sourceid"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 723
    const/16 v19, 0x0

    .line 725
    :cond_4
    invoke-interface {v11, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 726
    .local v14, "contactID":J
    invoke-interface {v11, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 727
    .local v8, "source":Ljava/lang/String;
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 728
    .local v6, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-eqz v8, :cond_5

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_6

    .line 729
    :cond_5
    const-string/jumbo v8, "facebook"

    .line 730
    :cond_6
    new-instance v5, Lcom/vlingo/core/internal/contacts/ContactData;

    sget-object v7, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Facebook:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    const/16 v9, 0x7d1

    const/4 v10, 0x0

    invoke-direct/range {v5 .. v10}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    invoke-virtual {v6, v5}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addSocial(Lcom/vlingo/core/internal/contacts/ContactData;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 731
    add-int/lit8 v16, v16, 0x1

    .line 737
    .end local v6    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v8    # "source":Ljava/lang/String;
    .end local v14    # "contactID":J
    :cond_7
    if-eqz v11, :cond_3

    .line 738
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 733
    :catch_0
    move-exception v1

    .line 737
    if-eqz v11, :cond_3

    .line 738
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 737
    :catchall_0
    move-exception v1

    if-eqz v11, :cond_8

    .line 738
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v1

    .end local v2    # "rawContactUri":Landroid/net/Uri;
    .end local v3    # "proj":[Ljava/lang/String;
    .end local v4    # "where":Ljava/lang/String;
    .end local v11    # "c":Landroid/database/Cursor;
    .end local v12    # "colIndexContactID":I
    .end local v13    # "colIndexSource":I
    .end local v17    # "counter":I
    .end local v19    # "firstTime":Z
    .end local v20    # "i":I
    .end local v21    # "i$":Ljava/util/Iterator;
    .end local v22    # "rawID":Ljava/lang/Long;
    .end local v23    # "whereBuilder":Ljava/lang/StringBuilder;
    :cond_9
    move/from16 v1, v16

    .line 743
    goto/16 :goto_1
.end method


# virtual methods
.method protected additionalPhoneticMatching(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactRule;Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)V
    .locals 21
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "rule"    # Lcom/vlingo/core/internal/contacts/ContactRule;
    .param p4, "ruleSet"    # Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/ContactRule;",
            "Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 453
    .local p5, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    .local p6, "localMatches":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p7, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 455
    .local v6, "phoneticLocalMatches":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    move-object/from16 v0, p4

    instance-of v1, v0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;

    if-nez v1, :cond_0

    move-object/from16 v0, p4

    instance-of v1, v0, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;

    if-nez v1, :cond_0

    .line 456
    move-object/from16 v0, p6

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 507
    :goto_0
    return-void

    .line 460
    :cond_0
    invoke-virtual/range {p6 .. p6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 461
    .local v16, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    move-object/from16 v0, p4

    instance-of v1, v0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;

    if-eqz v1, :cond_2

    .line 462
    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    move-object/from16 v1, p4

    .line 463
    check-cast v1, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 464
    .local v4, "newQueryWithPhoneticName":Ljava/lang/String;
    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->skipExtraData()Z

    move-result v5

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v7, p7

    invoke-virtual/range {v1 .. v7}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->findMatchesByNamePerPass(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;Ljava/util/List;)Z

    goto :goto_1

    .end local v4    # "newQueryWithPhoneticName":Ljava/lang/String;
    :cond_2
    move-object/from16 v15, p4

    .line 467
    check-cast v15, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;

    .line 468
    .local v15, "japanRuleSet":Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/core/internal/contacts/ContactRule;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "Partial Match"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 469
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneticFirstName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v19, 0x1

    .line 470
    .local v19, "queryHasFirstName":Z
    :goto_2
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneticLastName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v20, 0x1

    .line 471
    .local v20, "queryHasLastName":Z
    :goto_3
    if-eqz v19, :cond_9

    if-eqz v20, :cond_9

    .line 482
    .end local v19    # "queryHasFirstName":Z
    .end local v20    # "queryHasLastName":Z
    :cond_3
    :goto_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneticLastName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneticFirstName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 483
    .local v18, "phoneticName":Ljava/lang/String;
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->normalizeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 484
    .local v17, "nameNormalized":Ljava/lang/String;
    const/4 v10, 0x0

    .line 485
    .local v10, "query":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 486
    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;->queryToWhereClauseWithPhoneticFullName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 488
    :cond_4
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 489
    :cond_5
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneticFirstName()Ljava/lang/String;

    move-result-object v18

    .line 490
    invoke-static/range {v18 .. v18}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 491
    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;->queryToWhereClauseWithPhoneticFirstName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 500
    :cond_6
    :goto_5
    if-eqz v10, :cond_1

    .line 501
    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->skipExtraData()Z

    move-result v11

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object v12, v6

    move-object/from16 v13, p7

    invoke-virtual/range {v7 .. v13}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->findMatchesByNamePerPass(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;Ljava/util/List;)Z

    goto/16 :goto_1

    .line 469
    .end local v10    # "query":Ljava/lang/String;
    .end local v17    # "nameNormalized":Ljava/lang/String;
    .end local v18    # "phoneticName":Ljava/lang/String;
    :cond_7
    const/16 v19, 0x0

    goto/16 :goto_2

    .line 470
    .restart local v19    # "queryHasFirstName":Z
    :cond_8
    const/16 v20, 0x0

    goto :goto_3

    .line 473
    .restart local v20    # "queryHasLastName":Z
    :cond_9
    if-eqz v19, :cond_a

    .line 474
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneticFirstName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v15, v1}, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;->queryToWhereClauseWithPhoneticFirstName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->skipExtraData()Z

    move-result v11

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object v12, v6

    move-object/from16 v13, p7

    invoke-virtual/range {v7 .. v13}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->findMatchesByNamePerPass(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;Ljava/util/List;)Z

    goto/16 :goto_4

    .line 476
    :cond_a
    if-eqz v20, :cond_3

    .line 477
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneticLastName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v15, v1}, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;->queryToWhereClauseWithPhoneticLastName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->skipExtraData()Z

    move-result v11

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object v12, v6

    move-object/from16 v13, p7

    invoke-virtual/range {v7 .. v13}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->findMatchesByNamePerPass(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;Ljava/util/List;)Z

    goto/16 :goto_4

    .line 493
    .end local v19    # "queryHasFirstName":Z
    .end local v20    # "queryHasLastName":Z
    .restart local v10    # "query":Ljava/lang/String;
    .restart local v17    # "nameNormalized":Ljava/lang/String;
    .restart local v18    # "phoneticName":Ljava/lang/String;
    :cond_b
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_c

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 494
    :cond_c
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneticLastName()Ljava/lang/String;

    move-result-object v18

    .line 495
    invoke-static/range {v18 .. v18}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 496
    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;->queryToWhereClauseWithPhoneticLastName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    goto :goto_5

    .line 506
    .end local v10    # "query":Ljava/lang/String;
    .end local v15    # "japanRuleSet":Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;
    .end local v16    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v17    # "nameNormalized":Ljava/lang/String;
    .end local v18    # "phoneticName":Ljava/lang/String;
    :cond_d
    move-object/from16 v0, p6

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    goto/16 :goto_0
.end method

.method public convertEmailType(I)I
    .locals 1
    .param p1, "emailType"    # I

    .prologue
    .line 663
    const/4 v0, 0x0

    .line 664
    .local v0, "ret":I
    packed-switch p1, :pswitch_data_0

    .line 673
    const/4 v0, 0x7

    .line 676
    :goto_0
    return v0

    .line 666
    :pswitch_0
    const/4 v0, 0x3

    .line 667
    goto :goto_0

    .line 669
    :pswitch_1
    const/4 v0, 0x1

    .line 670
    goto :goto_0

    .line 664
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public findMatchesByName(Landroid/content/Context;Ljava/lang/String;Ljava/util/HashMap;Ljava/util/List;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 186
    .local p3, "matchTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p4, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 197
    :cond_0
    return-void

    .line 189
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->mSkipNormalCheck:Z

    .line 190
    invoke-static {p2}, Lcom/vlingo/core/internal/contacts/ContactDBQueryUtil;->queryToWhereClauseForMatchesByName(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 191
    .local v10, "where":[Ljava/lang/String;
    move-object v7, v10

    .local v7, "arr$":[Ljava/lang/String;
    array-length v9, v7

    .local v9, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_0
    if-ge v8, v9, :cond_0

    aget-object v3, v7, v8

    .line 193
    .local v3, "sWhere":Ljava/lang/String;
    invoke-static {p2}, Lcom/vlingo/core/internal/util/StringUtils;->nameIsKorean(Ljava/lang/String;)Z

    move-result v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->findMatchesByNamePerPass(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method

.method public findMatchesByName2(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;Z)V
    .locals 30
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p6, "keepUnrelatedContacts"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 213
    .local p3, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    .local p4, "matchTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p5, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 217
    :cond_1
    const/16 v24, 0x0

    .line 218
    .local v24, "isQueryHasLatinSymbols":Z
    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->RULE_SETS:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_2
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;

    .line 219
    .local v14, "ruleSet":Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;
    if-eqz v24, :cond_3

    instance-of v4, v14, Lcom/vlingo/core/internal/contacts/rules/EFIGSContactRuleSet;

    if-eqz v4, :cond_2

    .line 222
    :cond_3
    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->canProcess(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 223
    invoke-virtual {v14}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->getExitCriteria()Lcom/vlingo/core/internal/contacts/ContactExitCriteria;

    move-result-object v20

    .line 224
    .local v20, "exitCriteria":Lcom/vlingo/core/internal/contacts/ContactExitCriteria;
    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->generateRules(Ljava/lang/String;)Ljava/util/List;

    move-result-object v28

    .line 225
    .local v28, "rules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactRule;>;"
    const/16 v27, 0x0

    .line 226
    .local v27, "nRuleCnt":I
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .local v23, "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/vlingo/core/internal/contacts/ContactRule;

    .line 228
    .local v13, "rule":Lcom/vlingo/core/internal/contacts/ContactRule;
    invoke-virtual {v13}, Lcom/vlingo/core/internal/contacts/ContactRule;->getQuery()Ljava/lang/String;

    move-result-object v7

    .line 231
    .local v7, "query":Ljava/lang/String;
    add-int/lit8 v27, v27, 0x1

    .line 232
    invoke-virtual {v13}, Lcom/vlingo/core/internal/contacts/ContactRule;->getScore()Lcom/vlingo/core/internal/contacts/scoring/ContactScore;

    move-result-object v29

    .line 234
    .local v29, "scorer":Lcom/vlingo/core/internal/contacts/scoring/ContactScore;
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 235
    .local v9, "localMatches":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->mSkipNormalCheck:Z

    .line 236
    invoke-virtual {v14}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->skipExtraData()Z

    move-result v8

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v10, p5

    invoke-virtual/range {v4 .. v10}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->findMatchesByNamePerPass(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;Ljava/util/List;)Z

    .line 239
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->mSkipNormalCheck:Z

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move-object/from16 v12, p2

    move-object/from16 v15, p3

    move-object/from16 v16, v9

    move-object/from16 v17, p5

    .line 240
    invoke-virtual/range {v10 .. v17}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->additionalPhoneticMatching(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactRule;Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)V

    .line 243
    invoke-virtual {v9}, Ljava/util/HashMap;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    .line 245
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    invoke-virtual {v0, v1, v2, v9, v3}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->getAllContactData(Landroid/content/Context;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)I

    move-result v18

    .line 250
    .local v18, "count":I
    invoke-virtual {v9}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v25

    .line 251
    .local v25, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;>;"
    :cond_5
    :goto_1
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 252
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/util/Map$Entry;

    .line 253
    .local v19, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 254
    .local v26, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-nez p6, :cond_6

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/contacts/ContactMatch;->containsData()Z

    move-result v4

    if-eqz v4, :cond_7

    :cond_6
    move-object/from16 v0, v26

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->hasLookupData(Ljava/util/EnumSet;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 257
    :cond_7
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 265
    .end local v19    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v26    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_8
    move-object/from16 v0, p4

    move-object/from16 v1, v29

    move-object/from16 v2, v20

    move-object/from16 v3, p2

    invoke-static {v0, v9, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->applyScores(Ljava/util/HashMap;Ljava/util/HashMap;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;Lcom/vlingo/core/internal/contacts/ContactExitCriteria;Ljava/lang/String;)V

    .line 267
    invoke-virtual/range {v20 .. v20}, Lcom/vlingo/core/internal/contacts/ContactExitCriteria;->returnMatches()Z

    move-result v21

    .line 270
    .local v21, "exitCriteriaFromLoop":Z
    instance-of v4, v14, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;

    if-eqz v4, :cond_9

    const-string/jumbo v4, "\\s+"

    const-string/jumbo v5, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_9

    .line 271
    const/16 v21, 0x0

    .line 275
    :cond_9
    if-eqz v21, :cond_4

    invoke-virtual {v9}, Ljava/util/HashMap;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    goto/16 :goto_0

    .line 281
    .end local v7    # "query":Ljava/lang/String;
    .end local v9    # "localMatches":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v13    # "rule":Lcom/vlingo/core/internal/contacts/ContactRule;
    .end local v18    # "count":I
    .end local v21    # "exitCriteriaFromLoop":Z
    .end local v25    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;>;"
    .end local v29    # "scorer":Lcom/vlingo/core/internal/contacts/scoring/ContactScore;
    :cond_a
    instance-of v4, v14, Lcom/vlingo/core/internal/contacts/rules/EFIGSContactRuleSet;

    if-nez v4, :cond_0

    invoke-virtual/range {p4 .. p4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 283
    instance-of v4, v14, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;

    if-eqz v4, :cond_b

    invoke-virtual/range {p4 .. p4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 289
    :cond_b
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/util/StringUtils;->stringHasLatinSymbols(Ljava/lang/String;)Z

    move-result v24

    .line 290
    if-nez v24, :cond_2

    goto/16 :goto_0
.end method

.method public findMatchesByNamePerPass(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;Ljava/util/List;)Z
    .locals 36
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "where"    # Ljava/lang/String;
    .param p4, "skipExtraData"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 312
    .local p5, "matchTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p6, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/16 v2, 0xc

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v11, "contact_id"

    aput-object v11, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v11, "raw_contact_id"

    aput-object v11, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v11, "lookup"

    aput-object v11, v4, v2

    const/4 v2, 0x3

    const-string/jumbo v11, "display_name"

    aput-object v11, v4, v2

    const/4 v2, 0x4

    const-string/jumbo v11, "starred"

    aput-object v11, v4, v2

    const/4 v2, 0x5

    const-string/jumbo v11, "data1"

    aput-object v11, v4, v2

    const/4 v2, 0x6

    const-string/jumbo v11, "data2"

    aput-object v11, v4, v2

    const/4 v2, 0x7

    const-string/jumbo v11, "data3"

    aput-object v11, v4, v2

    const/16 v2, 0x8

    const-string/jumbo v11, "data7"

    aput-object v11, v4, v2

    const/16 v2, 0x9

    const-string/jumbo v11, "data9"

    aput-object v11, v4, v2

    const/16 v2, 0xa

    const-string/jumbo v11, "times_contacted"

    aput-object v11, v4, v2

    const/16 v2, 0xb

    const-string/jumbo v11, "phonetic_name"

    aput-object v11, v4, v2

    .line 327
    .local v4, "proj":[Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    .line 329
    .local v3, "lookupUri":Landroid/net/Uri;
    const/16 v33, 0x0

    .line 332
    .local v33, "retVal":Z
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->mSkipNormalCheck:Z

    if-nez v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 333
    const/4 v2, 0x0

    .line 409
    :goto_0
    return v2

    .line 337
    :cond_0
    const/4 v12, 0x0

    .line 338
    .local v12, "c":Landroid/database/Cursor;
    const/16 v32, 0x0

    .line 341
    .local v32, "recordsFound":I
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v5, p3

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 342
    if-eqz v12, :cond_2

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 343
    const-string/jumbo v2, "raw_contact_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 344
    .local v22, "colIndexRawContactID":I
    const-string/jumbo v2, "contact_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 345
    .local v13, "colIndexContactID":I
    const-string/jumbo v2, "lookup"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 346
    .local v19, "colIndexLookupKey":I
    const-string/jumbo v2, "display_name"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 347
    .local v20, "colIndexName":I
    const-string/jumbo v2, "starred"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 348
    .local v23, "colIndexStarred":I
    const-string/jumbo v2, "data1"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 349
    .local v14, "colIndexData1":I
    const-string/jumbo v2, "data2"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 350
    .local v15, "colIndexData2":I
    const-string/jumbo v2, "data3"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 351
    .local v16, "colIndexData3":I
    const-string/jumbo v2, "data7"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 352
    .local v17, "colIndexData7":I
    const-string/jumbo v2, "data9"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 353
    .local v18, "colIndexData9":I
    const-string/jumbo v2, "times_contacted"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 354
    .local v24, "colIndexTimesContacted":I
    const-string/jumbo v2, "phonetic_name"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 357
    .local v21, "colIndexPhoneticName":I
    :cond_1
    invoke-interface {v12, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 358
    .local v8, "contactID":J
    move/from16 v0, v20

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 359
    .local v6, "name":Ljava/lang/String;
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 362
    .local v5, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-eqz p4, :cond_4

    move-object/from16 v0, p2

    invoke-static {v0, v6}, Lcom/vlingo/core/internal/util/StringUtils;->matchesKoreanNamePattern(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 396
    :goto_1
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 404
    .end local v5    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v6    # "name":Ljava/lang/String;
    .end local v8    # "contactID":J
    .end local v13    # "colIndexContactID":I
    .end local v14    # "colIndexData1":I
    .end local v15    # "colIndexData2":I
    .end local v16    # "colIndexData3":I
    .end local v17    # "colIndexData7":I
    .end local v18    # "colIndexData9":I
    .end local v19    # "colIndexLookupKey":I
    .end local v20    # "colIndexName":I
    .end local v21    # "colIndexPhoneticName":I
    .end local v22    # "colIndexRawContactID":I
    .end local v23    # "colIndexStarred":I
    .end local v24    # "colIndexTimesContacted":I
    :cond_2
    if-eqz v12, :cond_3

    .line 405
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_2
    move/from16 v2, v33

    .line 409
    goto/16 :goto_0

    .line 366
    .restart local v5    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v6    # "name":Ljava/lang/String;
    .restart local v8    # "contactID":J
    .restart local v13    # "colIndexContactID":I
    .restart local v14    # "colIndexData1":I
    .restart local v15    # "colIndexData2":I
    .restart local v16    # "colIndexData3":I
    .restart local v17    # "colIndexData7":I
    .restart local v18    # "colIndexData9":I
    .restart local v19    # "colIndexLookupKey":I
    .restart local v20    # "colIndexName":I
    .restart local v21    # "colIndexPhoneticName":I
    .restart local v22    # "colIndexRawContactID":I
    .restart local v23    # "colIndexStarred":I
    .restart local v24    # "colIndexTimesContacted":I
    :cond_4
    :try_start_1
    invoke-interface {v12, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 368
    .local v25, "data1":Ljava/lang/String;
    if-eqz v5, :cond_6

    .line 369
    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addExtraName(Ljava/lang/String;)V

    .line 388
    :goto_3
    move/from16 v0, v22

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v30

    .line 389
    .local v30, "rawContactID":J
    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 390
    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 391
    move-wide/from16 v0, v30

    invoke-virtual {v5, v0, v1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->setRawContactID(J)V

    .line 392
    add-int/lit8 v32, v32, 0x1

    .line 395
    :cond_5
    const/16 v33, 0x1

    goto :goto_1

    .line 372
    .end local v30    # "rawContactID":J
    :cond_6
    invoke-interface {v12, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 373
    .local v26, "firstName":Ljava/lang/String;
    move/from16 v0, v16

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 374
    .local v27, "lastName":Ljava/lang/String;
    move/from16 v0, v17

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 375
    .local v28, "phoneticFirstName":Ljava/lang/String;
    move/from16 v0, v18

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v29

    .line 376
    .local v29, "phoneticLastName":Ljava/lang/String;
    move/from16 v0, v24

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v35

    .line 377
    .local v35, "timesContacted":I
    move/from16 v0, v21

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 379
    .local v7, "phoneticName":Ljava/lang/String;
    move/from16 v0, v19

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 380
    .local v10, "lookupKey":Ljava/lang/String;
    move/from16 v0, v23

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v34

    .line 381
    .local v34, "starred":I
    new-instance v5, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .end local v5    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    const/4 v2, 0x1

    move/from16 v0, v34

    if-ne v0, v2, :cond_7

    const/4 v11, 0x1

    :goto_4
    invoke-direct/range {v5 .. v11}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V

    .line 382
    .restart local v5    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v0, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 383
    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addExtraName(Ljava/lang/String;)V

    .line 384
    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v5, v0, v1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addNameFields(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v5, v0, v1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addPhoneticFields(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    move/from16 v0, v35

    invoke-virtual {v5, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->setTimesContacted(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 398
    .end local v5    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v6    # "name":Ljava/lang/String;
    .end local v7    # "phoneticName":Ljava/lang/String;
    .end local v8    # "contactID":J
    .end local v10    # "lookupKey":Ljava/lang/String;
    .end local v13    # "colIndexContactID":I
    .end local v14    # "colIndexData1":I
    .end local v15    # "colIndexData2":I
    .end local v16    # "colIndexData3":I
    .end local v17    # "colIndexData7":I
    .end local v18    # "colIndexData9":I
    .end local v19    # "colIndexLookupKey":I
    .end local v20    # "colIndexName":I
    .end local v21    # "colIndexPhoneticName":I
    .end local v22    # "colIndexRawContactID":I
    .end local v23    # "colIndexStarred":I
    .end local v24    # "colIndexTimesContacted":I
    .end local v25    # "data1":Ljava/lang/String;
    .end local v26    # "firstName":Ljava/lang/String;
    .end local v27    # "lastName":Ljava/lang/String;
    .end local v28    # "phoneticFirstName":Ljava/lang/String;
    .end local v29    # "phoneticLastName":Ljava/lang/String;
    .end local v34    # "starred":I
    .end local v35    # "timesContacted":I
    :catch_0
    move-exception v2

    .line 404
    if-eqz v12, :cond_3

    .line 405
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 381
    .restart local v6    # "name":Ljava/lang/String;
    .restart local v7    # "phoneticName":Ljava/lang/String;
    .restart local v8    # "contactID":J
    .restart local v10    # "lookupKey":Ljava/lang/String;
    .restart local v13    # "colIndexContactID":I
    .restart local v14    # "colIndexData1":I
    .restart local v15    # "colIndexData2":I
    .restart local v16    # "colIndexData3":I
    .restart local v17    # "colIndexData7":I
    .restart local v18    # "colIndexData9":I
    .restart local v19    # "colIndexLookupKey":I
    .restart local v20    # "colIndexName":I
    .restart local v21    # "colIndexPhoneticName":I
    .restart local v22    # "colIndexRawContactID":I
    .restart local v23    # "colIndexStarred":I
    .restart local v24    # "colIndexTimesContacted":I
    .restart local v25    # "data1":Ljava/lang/String;
    .restart local v26    # "firstName":Ljava/lang/String;
    .restart local v27    # "lastName":Ljava/lang/String;
    .restart local v28    # "phoneticFirstName":Ljava/lang/String;
    .restart local v29    # "phoneticLastName":Ljava/lang/String;
    .restart local v34    # "starred":I
    .restart local v35    # "timesContacted":I
    :cond_7
    const/4 v11, 0x0

    goto :goto_4

    .line 404
    .end local v6    # "name":Ljava/lang/String;
    .end local v7    # "phoneticName":Ljava/lang/String;
    .end local v8    # "contactID":J
    .end local v10    # "lookupKey":Ljava/lang/String;
    .end local v13    # "colIndexContactID":I
    .end local v14    # "colIndexData1":I
    .end local v15    # "colIndexData2":I
    .end local v16    # "colIndexData3":I
    .end local v17    # "colIndexData7":I
    .end local v18    # "colIndexData9":I
    .end local v19    # "colIndexLookupKey":I
    .end local v20    # "colIndexName":I
    .end local v21    # "colIndexPhoneticName":I
    .end local v22    # "colIndexRawContactID":I
    .end local v23    # "colIndexStarred":I
    .end local v24    # "colIndexTimesContacted":I
    .end local v25    # "data1":Ljava/lang/String;
    .end local v26    # "firstName":Ljava/lang/String;
    .end local v27    # "lastName":Ljava/lang/String;
    .end local v28    # "phoneticFirstName":Ljava/lang/String;
    .end local v29    # "phoneticLastName":Ljava/lang/String;
    .end local v34    # "starred":I
    .end local v35    # "timesContacted":I
    :catchall_0
    move-exception v2

    if-eqz v12, :cond_8

    .line 405
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v2
.end method

.method public getAllContactData(Landroid/content/Context;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 424
    .local p2, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    .local p3, "localMatches":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p4, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/4 v0, 0x0

    .line 425
    .local v0, "count":I
    invoke-virtual {p3}, Ljava/util/HashMap;->size()I

    move-result v1

    if-lez v1, :cond_1

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->PHONE_NUMBER:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-virtual {p2, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->EMAIL_ADDRESS:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-virtual {p2, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->ADDRESS:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-virtual {p2, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->BIRTHDAY:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-virtual {p2, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactType;->UNDEFINED:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {p2, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 433
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->getContactDetails(Landroid/content/Context;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)I

    move-result v0

    .line 440
    :cond_1
    const/4 v0, 0x0

    .line 441
    invoke-virtual {p3}, Ljava/util/HashMap;->size()I

    move-result v1

    if-lez v1, :cond_3

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->SOCIAL_NETWORK:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-virtual {p2, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p2}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 442
    :cond_2
    invoke-direct {p0, p1, p3, p4}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->getSocialContactData(Landroid/content/Context;Ljava/util/HashMap;Ljava/util/List;)I

    move-result v0

    .line 445
    :cond_3
    return v0
.end method

.method public getContactDataByEmail(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactData;
    .locals 30
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "email"    # Ljava/lang/String;

    .prologue
    .line 756
    const/16 v18, 0x0

    .line 757
    .local v18, "c":Landroid/database/Cursor;
    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    .line 760
    .local v3, "lookupUri":Landroid/net/Uri;
    const/16 v2, 0xa

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v12, "contact_id"

    aput-object v12, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v12, "raw_contact_id"

    aput-object v12, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v12, "lookup"

    aput-object v12, v4, v2

    const/4 v2, 0x3

    const-string/jumbo v12, "display_name"

    aput-object v12, v4, v2

    const/4 v2, 0x4

    const-string/jumbo v12, "starred"

    aput-object v12, v4, v2

    const/4 v2, 0x5

    const-string/jumbo v12, "data1"

    aput-object v12, v4, v2

    const/4 v2, 0x6

    const-string/jumbo v12, "data2"

    aput-object v12, v4, v2

    const/4 v2, 0x7

    const-string/jumbo v12, "is_super_primary"

    aput-object v12, v4, v2

    const/16 v2, 0x8

    const-string/jumbo v12, "times_contacted"

    aput-object v12, v4, v2

    const/16 v2, 0x9

    const-string/jumbo v12, "phonetic_name"

    aput-object v12, v4, v2

    .line 774
    .local v4, "proj":[Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 775
    const/4 v12, 0x0

    .line 814
    :cond_0
    :goto_0
    return-object v12

    .line 777
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "data1 like \'"

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v12, "\'"

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 782
    .local v5, "where":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 783
    if-eqz v18, :cond_3

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->isLast()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 784
    const-string/jumbo v2, "contact_id"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 785
    .local v19, "colIndexContactID":I
    const-string/jumbo v2, "lookup"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 786
    .local v23, "colIndexLookupKey":I
    const-string/jumbo v2, "display_name"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 787
    .local v24, "colIndexName":I
    const-string/jumbo v2, "starred"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    .line 788
    .local v26, "colIndexStarred":I
    const-string/jumbo v2, "data1"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 789
    .local v21, "colIndexEmail":I
    const-string/jumbo v2, "data2"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 790
    .local v20, "colIndexData2":I
    const-string/jumbo v2, "is_super_primary"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 791
    .local v22, "colIndexIsDefault":I
    const-string/jumbo v2, "times_contacted"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    .line 792
    .local v27, "colIndexTimesContacted":I
    const-string/jumbo v2, "phonetic_name"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    .line 794
    .local v25, "colIndexPhoneticName":I
    invoke-interface/range {v18 .. v19}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 795
    .local v9, "contactID":J
    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 796
    .local v11, "lookupKey":Ljava/lang/String;
    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 797
    .local v7, "name":Ljava/lang/String;
    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v28

    .line 798
    .local v28, "starred":I
    move-object/from16 v0, v18

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 799
    .local v8, "phoneticName":Ljava/lang/String;
    new-instance v6, Lcom/vlingo/core/internal/contacts/ContactMatch;

    const/4 v2, 0x1

    move/from16 v0, v28

    if-ne v0, v2, :cond_2

    const/4 v12, 0x1

    :goto_1
    invoke-direct/range {v6 .. v12}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V

    .line 800
    .local v6, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 801
    .local v15, "data1":Ljava/lang/String;
    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 802
    .local v16, "data2":I
    move-object/from16 v0, v18

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v29

    .line 803
    .local v29, "timesContacted":I
    invoke-virtual {v6, v15}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addExtraName(Ljava/lang/String;)V

    .line 804
    move/from16 v0, v29

    invoke-virtual {v6, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->setTimesContacted(I)V

    .line 805
    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 807
    .local v17, "isDefault":I
    new-instance v12, Lcom/vlingo/core/internal/contacts/ContactData;

    sget-object v14, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Email:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    move-object v13, v6

    invoke-direct/range {v12 .. v17}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 810
    if-eqz v18, :cond_0

    .line 811
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 799
    .end local v6    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v15    # "data1":Ljava/lang/String;
    .end local v16    # "data2":I
    .end local v17    # "isDefault":I
    .end local v29    # "timesContacted":I
    :cond_2
    const/4 v12, 0x0

    goto :goto_1

    .line 810
    .end local v7    # "name":Ljava/lang/String;
    .end local v8    # "phoneticName":Ljava/lang/String;
    .end local v9    # "contactID":J
    .end local v11    # "lookupKey":Ljava/lang/String;
    .end local v19    # "colIndexContactID":I
    .end local v20    # "colIndexData2":I
    .end local v21    # "colIndexEmail":I
    .end local v22    # "colIndexIsDefault":I
    .end local v23    # "colIndexLookupKey":I
    .end local v24    # "colIndexName":I
    .end local v25    # "colIndexPhoneticName":I
    .end local v26    # "colIndexStarred":I
    .end local v27    # "colIndexTimesContacted":I
    .end local v28    # "starred":I
    :cond_3
    if-eqz v18, :cond_4

    .line 811
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 814
    :cond_4
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 810
    :catchall_0
    move-exception v2

    if-eqz v18, :cond_5

    .line 811
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v2
.end method

.method public getContactMatchByPhoneNumber(Ljava/lang/String;Landroid/content/Context;)Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 18
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1021
    const/4 v1, 0x4

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v6, "_id"

    aput-object v6, v3, v1

    const/4 v1, 0x1

    const-string/jumbo v6, "lookup"

    aput-object v6, v3, v1

    const/4 v1, 0x2

    const-string/jumbo v6, "display_name"

    aput-object v6, v3, v1

    const/4 v1, 0x3

    const-string/jumbo v6, "starred"

    aput-object v6, v3, v1

    .line 1022
    .local v3, "projection":[Ljava/lang/String;
    const/16 v16, 0x0

    .line 1023
    .local v16, "cursor":Landroid/database/Cursor;
    if-eqz p1, :cond_2

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 1024
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1025
    const/4 v4, 0x0

    .line 1051
    :goto_0
    return-object v4

    .line 1027
    :cond_0
    sget-object v1, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static/range {p1 .. p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1028
    .local v2, "contactUri":Landroid/net/Uri;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1032
    const/4 v4, 0x0

    .line 1034
    .local v4, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-static/range {v16 .. v16}, Lcom/vlingo/core/internal/util/CursorUtil;->isValid(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->getCount()I

    move-result v1

    const/4 v6, 0x1

    if-lt v1, v6, :cond_5

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1036
    :try_start_0
    const-string/jumbo v1, "_id"

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    .line 1037
    .local v15, "contactId":Ljava/lang/Long;
    const-string/jumbo v1, "lookup"

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1038
    .local v8, "lookupKey":Ljava/lang/String;
    const-string/jumbo v1, "display_name"

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1039
    .local v5, "displayName":Ljava/lang/String;
    const-string/jumbo v1, "starred"

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 1041
    .local v17, "starred":I
    new-instance v4, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .end local v4    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v1, 0x1

    move/from16 v0, v17

    if-ne v0, v1, :cond_3

    const/4 v9, 0x1

    :goto_1
    invoke-direct/range {v4 .. v9}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;JLjava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1043
    .restart local v4    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-eqz v16, :cond_1

    .line 1044
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 1050
    .end local v5    # "displayName":Ljava/lang/String;
    .end local v8    # "lookupKey":Ljava/lang/String;
    .end local v15    # "contactId":Ljava/lang/Long;
    .end local v17    # "starred":I
    :cond_1
    :goto_2
    new-instance v9, Lcom/vlingo/core/internal/contacts/ContactData;

    sget-object v11, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Phone:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v10, v4

    move-object/from16 v12, p1

    invoke-direct/range {v9 .. v14}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    invoke-virtual {v4, v9}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addPhone(Lcom/vlingo/core/internal/contacts/ContactData;)V

    goto/16 :goto_0

    .line 1030
    .end local v2    # "contactUri":Landroid/net/Uri;
    .end local v4    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_2
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 1041
    .restart local v2    # "contactUri":Landroid/net/Uri;
    .restart local v5    # "displayName":Ljava/lang/String;
    .restart local v8    # "lookupKey":Ljava/lang/String;
    .restart local v15    # "contactId":Ljava/lang/Long;
    .restart local v17    # "starred":I
    :cond_3
    const/4 v9, 0x0

    goto :goto_1

    .line 1043
    .end local v5    # "displayName":Ljava/lang/String;
    .end local v8    # "lookupKey":Ljava/lang/String;
    .end local v15    # "contactId":Ljava/lang/Long;
    .end local v17    # "starred":I
    :catchall_0
    move-exception v1

    if-eqz v16, :cond_4

    .line 1044
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v1

    .line 1048
    .restart local v4    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_5
    new-instance v4, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .end local v4    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    const-wide/16 v11, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v9, v4

    move-object/from16 v10, p1

    invoke-direct/range {v9 .. v14}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;JLjava/lang/String;Z)V

    .restart local v4    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    goto :goto_2
.end method

.method public getContactMatches(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;Z)Ljava/util/List;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "query"    # Ljava/lang/String;
    .param p4, "keepUnrelatedContacts"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;Z)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    .local p3, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    const-string/jumbo v0, "new_contact_match_algo"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->getContactMatches(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;ZZ)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getContactMatches(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;ZZ)Ljava/util/List;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "query"    # Ljava/lang/String;
    .param p4, "keepUnrelatedContacts"    # Z
    .param p5, "newAlgo"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;ZZ)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    .local p3, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "getContactMatches()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 139
    .local v4, "matchTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 144
    .local v5, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    if-eqz p5, :cond_1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v6, p4

    .line 145
    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->findMatchesByName2(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;Z)V

    .line 146
    new-instance v10, Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v10, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 165
    .local v10, "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "**found "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " contact matches"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    return-object v10

    .line 148
    .end local v10    # "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_1
    invoke-virtual {p0, p1, p2, v4, v5}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->findMatchesByName(Landroid/content/Context;Ljava/lang/String;Ljava/util/HashMap;Ljava/util/List;)V

    .line 150
    invoke-virtual {p0, p1, p3, v4, v5}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->getAllContactData(Landroid/content/Context;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)I

    move-result v7

    .line 155
    .local v7, "count":I
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 156
    .restart local v10    # "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 157
    .local v9, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-nez p4, :cond_3

    invoke-virtual {v9}, Lcom/vlingo/core/internal/contacts/ContactMatch;->containsData()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_3
    invoke-virtual {v9, p3}, Lcom/vlingo/core/internal/contacts/ContactMatch;->hasLookupData(Ljava/util/EnumSet;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 158
    invoke-interface {v10, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getExactContactMatch(Landroid/content/Context;J)Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # J

    .prologue
    .line 103
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "contact_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "idSelection":Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->getExactContactMatchByQuery(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v1

    return-object v1
.end method

.method public getExactContactMatch(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "query"    # Ljava/lang/String;

    .prologue
    .line 87
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 88
    :cond_0
    const/4 v1, 0x0

    .line 93
    :goto_0
    return-object v1

    .line 92
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "data1 LIKE \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 93
    .local v0, "nameSelection":Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->getExactContactMatchByQuery(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v1

    goto :goto_0
.end method

.method public isAddressBookEmpty(Landroid/content/Context;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 837
    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v0, "contact_id"

    aput-object v0, v2, v8

    .line 838
    .local v2, "proj":[Ljava/lang/String;
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    .line 839
    .local v1, "lookupUri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 842
    .local v6, "c":Landroid/database/Cursor;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 853
    :goto_0
    return v8

    .line 847
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 848
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 849
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    move v0, v7

    .line 852
    :goto_1
    if-eqz v6, :cond_1

    .line 853
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    move v8, v0

    goto :goto_0

    :cond_2
    move v0, v8

    .line 849
    goto :goto_1

    .line 852
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 853
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public isProviderStatusNormal(Landroid/content/ContentResolver;)Z
    .locals 9
    .param p1, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 991
    const-string/jumbo v0, "content://com.android.contacts/provider_status"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v0, "status"

    aput-object v0, v2, v4

    const/4 v0, 0x1

    const-string/jumbo v4, "data1"

    aput-object v4, v2, v0

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 992
    .local v6, "c":Landroid/database/Cursor;
    const/4 v8, -0x1

    .line 993
    .local v8, "status":I
    const/4 v7, 0x0

    .line 995
    .local v7, "retVal":Z
    if-eqz v6, :cond_1

    .line 997
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 998
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 1000
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1007
    :cond_1
    if-nez v8, :cond_2

    .line 1008
    const/4 v7, 0x1

    .line 1010
    :cond_2
    return v7

    .line 1000
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public populateContactMapping(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 865
    const/4 v0, 0x0

    .line 868
    .local v0, "cur":Landroid/database/Cursor;
    :try_start_0
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->getContactCursor(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v0

    .line 869
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 870
    .local v7, "updatedContactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v0, :cond_4

    .line 872
    const-string/jumbo v8, "data2"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 873
    .local v6, "givenNameColumn":I
    const-string/jumbo v8, "data3"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 874
    .local v4, "familyNameColumn":I
    const-string/jumbo v8, "data1"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 875
    .local v2, "displayNameColumn":I
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 878
    :cond_0
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 879
    .local v5, "givenName":Ljava/lang/String;
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 880
    .local v3, "familyName":Ljava/lang/String;
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 883
    .local v1, "displayName":Ljava/lang/String;
    if-eqz v5, :cond_1

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 887
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 890
    :cond_1
    if-eqz v3, :cond_2

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 894
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 897
    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 901
    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 903
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-nez v8, :cond_0

    .line 906
    .end local v1    # "displayName":Ljava/lang/String;
    .end local v2    # "displayNameColumn":I
    .end local v3    # "familyName":Ljava/lang/String;
    .end local v4    # "familyNameColumn":I
    .end local v5    # "givenName":Ljava/lang/String;
    .end local v6    # "givenNameColumn":I
    :cond_4
    invoke-static {v7}, Lcom/vlingo/core/internal/settings/Settings;->setContactNameList(Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 909
    if-eqz v0, :cond_5

    .line 913
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 918
    :cond_5
    :goto_0
    return-void

    .line 909
    .end local v7    # "updatedContactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catchall_0
    move-exception v8

    if-eqz v0, :cond_6

    .line 913
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 915
    :cond_6
    :goto_1
    throw v8

    .line 914
    .restart local v7    # "updatedContactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_0
    move-exception v8

    goto :goto_0

    .end local v7    # "updatedContactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_1
    move-exception v9

    goto :goto_1
.end method
