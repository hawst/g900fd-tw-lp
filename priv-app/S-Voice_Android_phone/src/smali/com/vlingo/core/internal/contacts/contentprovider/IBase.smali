.class public interface abstract Lcom/vlingo/core/internal/contacts/contentprovider/IBase;
.super Ljava/lang/Object;
.source "IBase.java"


# static fields
.field public static final AND:Ljava/lang/String; = " AND "

.field public static final AS:Ljava/lang/String; = " AS "

.field public static final AS_SELECT:Ljava/lang/String; = " AS SELECT "

.field public static final AUTOINCREMENT:Ljava/lang/String; = " AUTOINCREMENT "

.field public static final CLOSING_BRACKET:Ljava/lang/String; = ")"

.field public static final COLUMN_ID:Ljava/lang/String; = "_id"

.field public static final COMMA:Ljava/lang/String; = ", "

.field public static final CREATE_TABLE:Ljava/lang/String; = "CREATE TABLE "

.field public static final CREATE_VIEW:Ljava/lang/String; = "CREATE VIEW "

.field public static final DOT:Ljava/lang/String; = "."

.field public static final EQUALS:Ljava/lang/String; = "="

.field public static final FROM:Ljava/lang/String; = " FROM "

.field public static final OPENING_BRACKET:Ljava/lang/String; = "("

.field public static final PRIMARY_KEY:Ljava/lang/String; = " PRIMARY KEY "

.field public static final SEMICOLON:Ljava/lang/String; = ";"

.field public static final SPACE:Ljava/lang/String; = " "

.field public static final WHERE:Ljava/lang/String; = " WHERE "


# virtual methods
.method public abstract getContactsContractUri()Landroid/net/Uri;
.end method
