.class public Lcom/vlingo/core/internal/debug/AutomationLog$WidgetHolder;
.super Ljava/lang/Object;
.source "AutomationLog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/debug/AutomationLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WidgetHolder"
.end annotation


# instance fields
.field public className:Ljava/lang/String;

.field public enm:Ljava/lang/Enum;

.field public widget:Landroid/view/View;


# direct methods
.method public constructor <init>(Ljava/lang/Enum;Ljava/lang/String;Landroid/view/View;)V
    .locals 0
    .param p1, "enm"    # Ljava/lang/Enum;
    .param p2, "className"    # Ljava/lang/String;
    .param p3, "widget"    # Landroid/view/View;

    .prologue
    .line 264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 265
    iput-object p1, p0, Lcom/vlingo/core/internal/debug/AutomationLog$WidgetHolder;->enm:Ljava/lang/Enum;

    .line 266
    iput-object p2, p0, Lcom/vlingo/core/internal/debug/AutomationLog$WidgetHolder;->className:Ljava/lang/String;

    .line 267
    iput-object p3, p0, Lcom/vlingo/core/internal/debug/AutomationLog$WidgetHolder;->widget:Landroid/view/View;

    .line 268
    return-void
.end method
