.class public Lcom/vlingo/core/internal/lmtt/BgLmttService;
.super Lcom/vlingo/core/internal/lmtt/LmttServiceBase;
.source "BgLmttService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/lmtt/BgLmttService$1;,
        Lcom/vlingo/core/internal/lmtt/BgLmttService$VContentObserver;,
        Lcom/vlingo/core/internal/lmtt/BgLmttService$InactivityHandler;
    }
.end annotation


# static fields
.field private static final DEFAULT_INACTIVITY_SHUTDOWN_PERIOD_MINS:I = 0x2760

.field private static final DEFAULT_MUSIC_GENERAL_URI:Landroid/net/Uri;

.field private static final DEFAULT_MUSIC_PLAYLIST_URI:Landroid/net/Uri;

.field private static final MS_PER_MINUTE:J = 0xea60L


# instance fields
.field private mContentObservers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/database/ContentObserver;",
            ">;"
        }
    .end annotation
.end field

.field private mInactivityHandler:Landroid/os/Handler;

.field private volatile mLmttManager:Lcom/vlingo/core/internal/lmtt/LMTTManager;

.field private mService:Landroid/app/Service;

.field private volatile mSyncManager:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->DEFAULT_MUSIC_GENERAL_URI:Landroid/net/Uri;

    .line 40
    sget-object v0, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->DEFAULT_MUSIC_PLAYLIST_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;-><init>()V

    .line 42
    iput-object v1, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mLmttManager:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    .line 43
    iput-object v1, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mSyncManager:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mContentObservers:Ljava/util/List;

    .line 46
    iput-object v1, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mService:Landroid/app/Service;

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/lmtt/BgLmttService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/BgLmttService;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/BgLmttService;->inactivityCheck()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/lmtt/BgLmttService;)Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/BgLmttService;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mSyncManager:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/lmtt/BgLmttService;)Lcom/vlingo/core/internal/lmtt/LMTTManager;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/BgLmttService;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mLmttManager:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    return-object v0
.end method

.method private getRemainingTime()J
    .locals 13

    .prologue
    const-wide/16 v11, 0x0

    .line 307
    const-string/jumbo v7, "lmtt.last_app_start_time"

    invoke-static {v7, v11, v12}, Lcom/vlingo/core/internal/settings/Settings;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    .line 308
    .local v3, "lastAppRequest":J
    const-string/jumbo v7, "lmtt.no_activity_shutdown_period_mins"

    const/16 v8, 0x2760

    invoke-static {v7, v8}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 309
    .local v0, "inactivityShutdownPeriod":I
    int-to-long v7, v0

    const-wide/32 v9, 0xea60

    mul-long v1, v7, v9

    .line 311
    .local v1, "inactivityShutdownPeriodMs":J
    add-long v7, v3, v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long v5, v7, v9

    .line 312
    .local v5, "timeRemainingMs":J
    cmp-long v7, v5, v11

    if-gez v7, :cond_0

    .line 313
    const-wide/16 v5, 0x0

    .line 315
    :cond_0
    return-wide v5
.end method

.method private inactivityCheck()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 280
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/BgLmttService;->getRemainingTime()J

    move-result-wide v0

    .line 281
    .local v0, "remainingTimeMs":J
    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-gtz v3, :cond_1

    .line 286
    const-string/jumbo v3, "lmtt.force_fullupdate_on_start"

    invoke-static {v3, v2}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 287
    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mService:Landroid/app/Service;

    if-eqz v2, :cond_0

    .line 288
    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mService:Landroid/app/Service;

    invoke-virtual {v2}, Landroid/app/Service;->stopSelf()V

    .line 290
    :cond_0
    const/4 v2, 0x0

    .line 294
    :goto_0
    return v2

    .line 293
    :cond_1
    invoke-direct {p0, v0, v1}, Lcom/vlingo/core/internal/lmtt/BgLmttService;->scheduleShutdown(J)V

    goto :goto_0
.end method

.method private registerContentObservers()V
    .locals 9

    .prologue
    .line 224
    const-string/jumbo v6, "Contacts"

    sget-object v7, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_CONTACT_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    sget-object v8, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v6, v7, v8}, Lcom/vlingo/core/internal/lmtt/BgLmttService;->registerObserver(Ljava/lang/String;Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;Landroid/net/Uri;)V

    .line 225
    sget-object v4, Lcom/vlingo/core/internal/lmtt/BgLmttService;->DEFAULT_MUSIC_GENERAL_URI:Landroid/net/Uri;

    .line 226
    .local v4, "musicUri":Landroid/net/Uri;
    sget-object v5, Lcom/vlingo/core/internal/lmtt/BgLmttService;->DEFAULT_MUSIC_PLAYLIST_URI:Landroid/net/Uri;

    .line 227
    .local v5, "playlistUri":Landroid/net/Uri;
    sget-object v6, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v6}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v2

    .line 228
    .local v2, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v3, 0x0

    .line 229
    .local v3, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v2, :cond_0

    .line 231
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object v3, v0

    .line 232
    sget-object v6, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-interface {v3, v6}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v4

    .line 233
    sget-object v6, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->PLAYLIST:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-interface {v3, v6}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 241
    :cond_0
    :goto_0
    const-string/jumbo v6, "Songs"

    sget-object v7, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_MUSIC_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    invoke-direct {p0, v6, v7, v4}, Lcom/vlingo/core/internal/lmtt/BgLmttService;->registerObserver(Ljava/lang/String;Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;Landroid/net/Uri;)V

    .line 242
    const-string/jumbo v6, "Playlists"

    sget-object v7, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_MUSIC_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    invoke-direct {p0, v6, v7, v5}, Lcom/vlingo/core/internal/lmtt/BgLmttService;->registerObserver(Ljava/lang/String;Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;Landroid/net/Uri;)V

    .line 243
    return-void

    .line 234
    :catch_0
    move-exception v1

    .line 235
    .local v1, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/vlingo/core/internal/lmtt/BgLmttService;->DEFAULT_MUSIC_GENERAL_URI:Landroid/net/Uri;

    .line 236
    sget-object v5, Lcom/vlingo/core/internal/lmtt/BgLmttService;->DEFAULT_MUSIC_PLAYLIST_URI:Landroid/net/Uri;

    .line 237
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private registerObserver(Ljava/lang/String;Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;Landroid/net/Uri;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "lmttUpdateType"    # Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    .line 246
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "lmtt.enable."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->getTypeString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 250
    new-instance v0, Lcom/vlingo/core/internal/lmtt/BgLmttService$VContentObserver;

    invoke-direct {v0, p0, p1, p2}, Lcom/vlingo/core/internal/lmtt/BgLmttService$VContentObserver;-><init>(Lcom/vlingo/core/internal/lmtt/BgLmttService;Ljava/lang/String;Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;)V

    .line 251
    .local v0, "contentObserver":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mService:Landroid/app/Service;

    invoke-virtual {v1}, Landroid/app/Service;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p3, v2, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 256
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mContentObservers:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 258
    .end local v0    # "contentObserver":Landroid/database/ContentObserver;
    :cond_0
    return-void
.end method

.method private resetDelayedShutdown()V
    .locals 7

    .prologue
    .line 264
    const-string/jumbo v3, "lmtt.last_app_start_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Ljava/lang/String;J)V

    .line 267
    const-string/jumbo v3, "lmtt.no_activity_shutdown_period_mins"

    const/16 v4, 0x2760

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 268
    .local v0, "inactivityShutdownPeriod":I
    int-to-long v3, v0

    const-wide/32 v5, 0xea60

    mul-long v1, v3, v5

    .line 269
    .local v1, "inactivityShutdownPeriodMs":J
    invoke-direct {p0, v1, v2}, Lcom/vlingo/core/internal/lmtt/BgLmttService;->scheduleShutdown(J)V

    .line 270
    return-void
.end method

.method private scheduleShutdown(J)V
    .locals 2
    .param p1, "delay"    # J

    .prologue
    const/4 v1, 0x1

    .line 302
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mInactivityHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 303
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mInactivityHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 304
    return-void
.end method

.method private unregisterContentObservers()V
    .locals 3

    .prologue
    .line 217
    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mContentObservers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/ContentObserver;

    .line 218
    .local v0, "contactsObserver":Landroid/database/ContentObserver;
    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mService:Landroid/app/Service;

    invoke-virtual {v2}, Landroid/app/Service;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 220
    .end local v0    # "contactsObserver":Landroid/database/ContentObserver;
    :cond_0
    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mContentObservers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 221
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/app/Service;)V
    .locals 3
    .param p1, "service"    # Landroid/app/Service;

    .prologue
    const/4 v2, 0x0

    .line 90
    iput-object p1, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mService:Landroid/app/Service;

    .line 92
    const-string/jumbo v0, "tos_accepted"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    new-instance v0, Lcom/vlingo/core/internal/lmtt/BgLmttService$InactivityHandler;

    invoke-direct {v0, p0, v2}, Lcom/vlingo/core/internal/lmtt/BgLmttService$InactivityHandler;-><init>(Lcom/vlingo/core/internal/lmtt/BgLmttService;Lcom/vlingo/core/internal/lmtt/BgLmttService$1;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mInactivityHandler:Landroid/os/Handler;

    .line 97
    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTManager;

    invoke-direct {v0, v2}, Lcom/vlingo/core/internal/lmtt/LMTTManager;-><init>(Lcom/vlingo/core/internal/lmtt/LMTTManager$Listener;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mLmttManager:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    .line 98
    new-instance v0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    invoke-direct {v0, v2}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;-><init>(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$Listener;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mSyncManager:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    .line 99
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/BgLmttService;->registerContentObservers()V

    .line 106
    :goto_0
    return-void

    .line 104
    :cond_0
    invoke-virtual {p1}, Landroid/app/Service;->stopSelf()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 191
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mLmttManager:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mLmttManager:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmtt/LMTTManager;->destroy()V

    .line 193
    iput-object v2, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mLmttManager:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mSyncManager:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    if-eqz v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mSyncManager:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;->destroy()V

    .line 198
    iput-object v2, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mSyncManager:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    .line 201
    :cond_1
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/BgLmttService;->unregisterContentObservers()V

    .line 203
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mInactivityHandler:Landroid/os/Handler;

    if-eqz v0, :cond_2

    .line 204
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mInactivityHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 205
    iput-object v2, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mInactivityHandler:Landroid/os/Handler;

    .line 208
    :cond_2
    iput-object v2, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mService:Landroid/app/Service;

    .line 209
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/BgLmttService;->destroyInstance()V

    .line 210
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 113
    const-string/jumbo v5, "tos_accepted"

    invoke-static {v5, v7}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-nez v5, :cond_1

    .line 117
    iget-object v5, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mService:Landroid/app/Service;

    invoke-virtual {v5}, Landroid/app/Service;->stopSelf()V

    .line 183
    :cond_0
    :goto_0
    return v8

    .line 124
    :cond_1
    if-eqz p1, :cond_2

    const-string/jumbo v5, "com.vlingo.lmtt.extra.NO_RESET"

    invoke-virtual {p1, v5, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 127
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/BgLmttService;->inactivityCheck()Z

    goto :goto_0

    .line 131
    :cond_3
    const-string/jumbo v5, "com.vlingo.lmtt.action.REINITIALIZE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 135
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/BgLmttService;->inactivityCheck()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 138
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/BgLmttService;->unregisterContentObservers()V

    .line 139
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/BgLmttService;->registerContentObservers()V

    goto :goto_0

    .line 149
    :cond_4
    const-string/jumbo v5, "lmtt.force_fullupdate_on_start"

    invoke-static {v5, v7}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 151
    .local v2, "forceUpdateOnStart":Z
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/BgLmttService;->resetDelayedShutdown()V

    .line 153
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 155
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v5, "com.vlingo.lmtt.action.LANGUAGE_CHANGE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 156
    iget-object v5, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mLmttManager:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    sget-object v6, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_LANGUAGE_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    invoke-virtual {v5, v6, v8, v7}, Lcom/vlingo/core/internal/lmtt/LMTTManager;->fireUpdate(Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;ZZ)V

    goto :goto_0

    .line 157
    :cond_5
    const-string/jumbo v5, "com.vlingo.lmtt.action.SEND_UPDATE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    if-eqz v2, :cond_0

    .line 158
    :cond_6
    if-eqz v2, :cond_7

    .line 161
    const-string/jumbo v5, "lmtt.force_fullupdate_on_start"

    invoke-static {v5, v7}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 164
    :cond_7
    const-string/jumbo v5, "com.vlingo.lmtt.extra.SKIP_DELAY"

    invoke-virtual {p1, v5, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 165
    .local v3, "skipDelay":Z
    const-string/jumbo v5, "com.vlingo.lmtt.extra.CLEAR_LMTT"

    invoke-virtual {p1, v5, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 166
    .local v1, "clearLMTT":Z
    const-string/jumbo v5, "com.vlingo.lmtt.extra.LMTT_TYPE"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 168
    .local v4, "typeString":Ljava/lang/String;
    iget-object v5, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mSyncManager:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    invoke-virtual {v5, v3}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;->fireSync(Z)V

    .line 170
    if-eqz v4, :cond_8

    invoke-static {v4}, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->fromTypeString(Ljava/lang/String;)Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_CONTACT_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    if-ne v5, v6, :cond_9

    .line 173
    :cond_8
    iget-object v5, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mLmttManager:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    sget-object v6, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_CONTACT_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    invoke-virtual {v5, v6, v3, v1}, Lcom/vlingo/core/internal/lmtt/LMTTManager;->fireUpdate(Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;ZZ)V

    .line 176
    :cond_9
    if-eqz v4, :cond_a

    invoke-static {v4}, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->fromTypeString(Ljava/lang/String;)Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_MUSIC_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    if-ne v5, v6, :cond_0

    .line 179
    :cond_a
    iget-object v5, p0, Lcom/vlingo/core/internal/lmtt/BgLmttService;->mLmttManager:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    sget-object v6, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_MUSIC_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    invoke-virtual {v5, v6, v3, v1}, Lcom/vlingo/core/internal/lmtt/LMTTManager;->fireUpdate(Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;ZZ)V

    goto/16 :goto_0
.end method
