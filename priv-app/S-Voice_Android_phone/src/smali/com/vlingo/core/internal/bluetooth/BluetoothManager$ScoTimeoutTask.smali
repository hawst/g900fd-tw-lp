.class Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoTimeoutTask;
.super Ljava/util/TimerTask;
.source "BluetoothManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/bluetooth/BluetoothManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScoTimeoutTask"
.end annotation


# instance fields
.field private mPassFlag:Z

.field private mRetry:Z


# direct methods
.method public constructor <init>(ZZ)V
    .locals 0
    .param p1, "retry"    # Z
    .param p2, "passFlag"    # Z

    .prologue
    .line 1013
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 1014
    iput-boolean p1, p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoTimeoutTask;->mRetry:Z

    .line 1015
    iput-boolean p2, p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoTimeoutTask;->mPassFlag:Z

    .line 1016
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1018
    const-class v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v2

    .line 1021
    :try_start_0
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$300()Ljava/util/TimerTask;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1022
    monitor-exit v2

    .line 1038
    :goto_0
    return-void

    .line 1024
    :cond_0
    const/4 v1, 0x0

    # invokes: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopSCO(Z)V
    invoke-static {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$400(Z)V

    .line 1025
    iget-boolean v1, p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoTimeoutTask;->mRetry:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 1027
    const-wide/16 v3, 0x64

    :try_start_1
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V

    .line 1028
    iget-boolean v1, p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoTimeoutTask;->mPassFlag:Z

    if-eqz v1, :cond_2

    .line 1029
    const/4 v1, 0x0

    # invokes: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startSCO(Z)V
    invoke-static {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$500(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1037
    :cond_1
    :goto_1
    :try_start_2
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 1031
    :cond_2
    :try_start_3
    # invokes: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startSCO()V
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$600()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 1033
    :catch_0
    move-exception v0

    .line 1034
    .local v0, "e":Ljava/lang/Exception;
    :try_start_4
    const-string/jumbo v1, "VLG_EXCEPTION"

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method
