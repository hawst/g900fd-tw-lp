.class public Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;
.super Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;
.source "VoiceDialController.java"


# static fields
.field private static final FIELD_IDS:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->FIELD_IDS:Ljava/util/HashMap;

    .line 39
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->FIELD_IDS:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;->CONTACT:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_DIAL_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v2}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->FIELD_IDS:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;->TYPE:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_DIAL_TYPE_NUM:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v2}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;-><init>()V

    return-void
.end method

.method private determineType()Ljava/lang/String;
    .locals 4

    .prologue
    .line 133
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v1

    .line 134
    .local v1, "phoneList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 135
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->phoneType:Ljava/lang/String;

    invoke-static {v2, v1, v3}, Lcom/vlingo/core/internal/util/DialUtil;->getPhoneTypeMap(Landroid/content/res/Resources;Ljava/util/List;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 136
    .local v0, "contactMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->phoneType:Ljava/lang/String;

    invoke-virtual {p0, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->findKey(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 138
    .end local v0    # "contactMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected checkHistory(Lcom/vlingo/core/internal/contacts/ContactMatch;)Z
    .locals 1
    .param p1, "item"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 167
    const/4 v0, 0x0

    return v0
.end method

.method public confirmRedial(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "contactName"    # Ljava/lang/String;

    .prologue
    .line 202
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->address:Ljava/lang/String;

    .line 203
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->contactSelectedName:Ljava/lang/String;

    .line 204
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_REDIAL_CONFIRMATION:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->state:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    .line 205
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 2
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 191
    invoke-virtual {p0, p2}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->setListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 192
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->state:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_REDIAL_CONFIRMATION:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    if-ne v0, v1, :cond_0

    .line 193
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v1}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 194
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->address:Ljava/lang/String;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->contactSelectedName:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->redial(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    const/4 v0, 0x0

    .line 197
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v0

    goto :goto_0
.end method

.method protected getCapability()Lcom/vlingo/core/internal/contacts/ContactType;
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    return-object v0
.end method

.method protected getContactPrompt()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_who_would_you_like_to_call:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getFieldIds()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->FIELD_IDS:Ljava/util/HashMap;

    return-object v0
.end method

.method protected getNoContactPrompt()Ljava/lang/String;
    .locals 2

    .prologue
    .line 56
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_MATCH_DEMAND_CALL:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    const-string/jumbo v0, "call"

    return-object v0
.end method

.method protected handleNeedMessage()V
    .locals 0

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->promptForConfirmation()V

    .line 65
    return-void
.end method

.method protected promptForConfirmation()V
    .locals 11

    .prologue
    const/4 v8, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 68
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_CONFIRMATION:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    iput-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->state:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    .line 69
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->address:Ljava/lang/String;

    if-nez v6, :cond_0

    .line 70
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_CONTACT_REFINEMENT:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    iput-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->state:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    .line 130
    :goto_0
    return-void

    .line 74
    :cond_0
    const/4 v5, 0x0

    .line 75
    .local v5, "type":Ljava/lang/String;
    const/4 v1, 0x0

    .line 76
    .local v1, "name":Ljava/lang/String;
    const/4 v3, 0x0

    .line 77
    .local v3, "ttsName":Ljava/lang/String;
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    if-eqz v6, :cond_1

    .line 78
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v1, v6, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 79
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->determineType()Ljava/lang/String;

    move-result-object v5

    .line 81
    :cond_1
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 82
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->query:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 89
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->POST_CONFIRMATION:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    iput-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->state:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    goto :goto_0

    .line 92
    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->query:Ljava/lang/String;

    .line 93
    const-string/jumbo v2, "[-()]"

    .line 94
    .local v2, "regularExpression":Ljava/lang/String;
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->query:Ljava/lang/String;

    const-string/jumbo v7, " "

    invoke-virtual {v6, v2, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 96
    .end local v2    # "regularExpression":Ljava/lang/String;
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    .line 98
    .local v0, "displayText":Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 99
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->callMode:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;->VOICEVIDEOCALL:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    if-ne v6, v7, :cond_4

    .line 100
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicevideodial_call_name_type:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v7, v8, [Ljava/lang/Object;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    aput-object v5, v7, v10

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 113
    :goto_1
    if-nez v3, :cond_7

    .line 114
    move-object v4, v0

    .line 126
    .local v4, "ttsText":Ljava/lang/String;
    :goto_2
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v6

    invoke-interface {v6, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    .line 127
    const/4 v6, 0x0

    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_DIAL_AUTODIAL:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v7}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v7

    invoke-virtual {p0, v0, v6, v7}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 128
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->sendAction()V

    .line 129
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->POST_CONFIRMATION:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    iput-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->state:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    goto :goto_0

    .line 102
    .end local v4    # "ttsText":Ljava/lang/String;
    :cond_4
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name_type:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v7, v8, [Ljava/lang/Object;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    aput-object v5, v7, v10

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 106
    :cond_5
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->callMode:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;->VOICEVIDEOCALL:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    if-ne v6, v7, :cond_6

    .line 107
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicevideodial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 109
    :cond_6
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 116
    :cond_7
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->callMode:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;->VOICEVIDEOCALL:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    if-ne v6, v7, :cond_8

    .line 117
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicevideodial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/DialUtil;->getTTSForAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "ttsText":Ljava/lang/String;
    goto :goto_2

    .line 119
    .end local v4    # "ttsText":Ljava/lang/String;
    :cond_8
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/DialUtil;->getTTSForAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "ttsText":Ljava/lang/String;
    goto :goto_2
.end method

.method public redial(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "contactName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 176
    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v3, v4, [Ljava/lang/Object;

    aput-object p2, v3, v5

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 177
    .local v0, "displayText":Ljava/lang/String;
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 178
    move-object v1, v0

    .line 184
    .local v1, "ttsText":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/core/internal/util/PhoneUtil;->turnOnSpeakerphoneIfRequired(Landroid/content/Context;)V

    .line 187
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-static {v2, p1, p0, v3}, Lcom/vlingo/core/internal/util/DialUtil;->dial(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 188
    return-void

    .line 180
    .end local v1    # "ttsText":Ljava/lang/String;
    :cond_0
    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/vlingo/core/internal/util/DialUtil;->getTTSForAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "ttsText":Ljava/lang/String;
    goto :goto_0
.end method

.method protected sendAction()V
    .locals 5

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->turnOnSpeakerIfRequired()V

    .line 157
    move-object v0, p0

    .line 159
    .local v0, "dma":Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->address:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->isVideoCall()Z

    move-result v4

    invoke-static {v1, v2, v0, v3, v4}, Lcom/vlingo/core/internal/util/DialUtil;->dial(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Z)V

    .line 160
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->contactSelectedName:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 161
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->contactSelectedName:Ljava/lang/String;

    sget-object v2, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;->DIAL:Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/VoiceDialController;->sendAcceptedText(Ljava/lang/String;Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;)V

    .line 163
    :cond_0
    return-void
.end method

.method protected turnOnSpeakerIfRequired()V
    .locals 1

    .prologue
    .line 171
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/PhoneUtil;->turnOnSpeakerphoneIfRequired(Landroid/content/Context;)V

    .line 172
    return-void
.end method
