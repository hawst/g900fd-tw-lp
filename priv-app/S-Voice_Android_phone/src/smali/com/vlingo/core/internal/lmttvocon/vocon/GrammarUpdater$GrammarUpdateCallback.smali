.class public interface abstract Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;
.super Ljava/lang/Object;
.source "GrammarUpdater.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GrammarUpdateCallback"
.end annotation


# virtual methods
.method public abstract onCancelled()V
.end method

.method public abstract onError()V
.end method

.method public abstract onFinished(ZLjava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onStarted()V
.end method
