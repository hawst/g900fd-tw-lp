.class Lcom/vlingo/core/internal/contacts/ContactMatch$4;
.super Ljava/lang/Object;
.source "ContactMatch.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/contacts/ContactMatch;->getOldestPhone(Ljava/util/List;)Lcom/vlingo/core/internal/contacts/ContactData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/vlingo/core/internal/contacts/ContactData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/contacts/ContactMatch;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 0

    .prologue
    .line 616
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch$4;->this$0:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/vlingo/core/internal/contacts/ContactData;Lcom/vlingo/core/internal/contacts/ContactData;)I
    .locals 2
    .param p1, "lhs"    # Lcom/vlingo/core/internal/contacts/ContactData;
    .param p2, "rhs"    # Lcom/vlingo/core/internal/contacts/ContactData;

    .prologue
    .line 619
    iget v0, p2, Lcom/vlingo/core/internal/contacts/ContactData;->dataId:I

    iget v1, p1, Lcom/vlingo/core/internal/contacts/ContactData;->dataId:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 616
    check-cast p1, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/core/internal/contacts/ContactMatch$4;->compare(Lcom/vlingo/core/internal/contacts/ContactData;Lcom/vlingo/core/internal/contacts/ContactData;)I

    move-result v0

    return v0
.end method
