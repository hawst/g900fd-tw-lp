.class public Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentIterator;
.super Ljava/lang/Object;
.source "ApplicationContentIterator.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationsIterator:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 20
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 35
    .local v4, "applicationLabels":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static/range {p1 .. p1}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationIdDbUtil;->getDb(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 36
    .local v7, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {v7}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationIdDbUtil;->getComponentNameToIdMap(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/Map;

    move-result-object v9

    .line 37
    .local v9, "idMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v12, Ljava/util/HashSet;

    invoke-direct {v12}, Ljava/util/HashSet;-><init>()V

    .line 38
    .local v12, "newComponentNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v13

    .line 39
    .local v13, "pm":Landroid/content/pm/PackageManager;
    new-instance v14, Landroid/content/Intent;

    const-string/jumbo v17, "android.intent.action.MAIN"

    move-object/from16 v0, v17

    invoke-direct {v14, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 40
    .local v14, "queryIntent":Landroid/content/Intent;
    const-string/jumbo v17, "android.intent.category.LAUNCHER"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    const/16 v16, 0x0

    .line 43
    .local v16, "resolveInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/16 v17, 0x0

    :try_start_0
    move/from16 v0, v17

    invoke-virtual {v13, v14, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v16

    .line 48
    :goto_0
    if-eqz v16, :cond_2

    .line 49
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/content/pm/ResolveInfo;

    .line 50
    .local v15, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v2, v15, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 51
    .local v2, "activityInfo":Landroid/content/pm/ActivityInfo;
    if-eqz v2, :cond_0

    .line 54
    invoke-virtual {v15, v13}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    .line 55
    .local v11, "localizedName":Ljava/lang/String;
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_0

    .line 58
    new-instance v17, Landroid/content/ComponentName;

    iget-object v0, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v18, v0

    iget-object v0, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-direct/range {v17 .. v19}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v6

    .line 59
    .local v6, "componentName":Ljava/lang/String;
    invoke-interface {v9, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_1

    .line 68
    invoke-interface {v12, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 70
    :cond_1
    invoke-interface {v4, v6, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 73
    .end local v2    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .end local v6    # "componentName":Ljava/lang/String;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v11    # "localizedName":Ljava/lang/String;
    .end local v15    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_2
    invoke-interface {v12}, Ljava/util/Set;->isEmpty()Z

    move-result v17

    if-nez v17, :cond_3

    .line 74
    invoke-static {v7, v12}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationIdDbUtil;->insertAll(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Set;)Ljava/util/Map;

    move-result-object v10

    .line 75
    .local v10, "insertedIdMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v9, v10}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 77
    .end local v10    # "insertedIdMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_3
    new-instance v5, Lcom/vlingo/core/internal/util/SparseArrayMap;

    invoke-direct {v5}, Lcom/vlingo/core/internal/util/SparseArrayMap;-><init>()V

    .line 78
    .local v5, "applications":Lcom/vlingo/core/internal/util/SparseArrayMap;, "Lcom/vlingo/core/internal/util/SparseArrayMap<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 79
    .local v3, "applicationLabelEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 80
    .restart local v6    # "componentName":Ljava/lang/String;
    invoke-interface {v9, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v18

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Lcom/vlingo/core/internal/util/SparseArrayMap;->put(ILjava/lang/Object;)V

    .line 81
    invoke-interface {v9, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 83
    .end local v3    # "applicationLabelEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v6    # "componentName":Ljava/lang/String;
    :cond_4
    invoke-interface {v9}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v7, v0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationIdDbUtil;->deleteAll(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Collection;)V

    .line 84
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 86
    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/SparseArrayMap;->iterator()Ljava/util/Iterator;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentIterator;->applicationsIterator:Ljava/util/Iterator;

    .line 87
    return-void

    .line 44
    .end local v5    # "applications":Lcom/vlingo/core/internal/util/SparseArrayMap;, "Lcom/vlingo/core/internal/util/SparseArrayMap<Ljava/lang/String;>;"
    .end local v8    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v17

    goto/16 :goto_0
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentIterator;->applicationsIterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;
    .locals 6

    .prologue
    .line 96
    iget-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentIterator;->applicationsIterator:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 97
    .local v0, "application":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    new-instance v3, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v4, v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->getAliasesForLabel(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v3, v4, v5, v1, v2}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;-><init>(JLjava/lang/String;Ljava/util/Collection;)V

    return-object v3
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentIterator;->next()Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 0

    .prologue
    .line 103
    return-void
.end method
