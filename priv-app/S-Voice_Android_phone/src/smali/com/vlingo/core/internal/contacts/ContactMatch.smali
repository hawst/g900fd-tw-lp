.class public Lcom/vlingo/core/internal/contacts/ContactMatch;
.super Ljava/lang/Object;
.source "ContactMatch.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/contacts/ContactMatch$5;,
        Lcom/vlingo/core/internal/contacts/ContactMatch$ReverseComparator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/vlingo/core/internal/contacts/ContactMatch;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x58080a2bcf94fb27L


# instance fields
.field private addressData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation
.end field

.field private allData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation
.end field

.field private allDataSorted:Z

.field private bestDetailScore:F

.field private birthdayData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation
.end field

.field private dataSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation
.end field

.field private detailsAreSorted:Z

.field private emailData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation
.end field

.field public volatile extraNameUsed:Ljava/lang/String;

.field private extraNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public firstName:Ljava/lang/String;

.field public lastName:Ljava/lang/String;

.field public final lookupKey:Ljava/lang/String;

.field private phoneData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation
.end field

.field public phoneticFirstName:Ljava/lang/String;

.field public phoneticLastName:Ljava/lang/String;

.field public final phoneticName:Ljava/lang/String;

.field public final primaryContactID:J

.field public final primaryDisplayName:Ljava/lang/String;

.field private rawContactID:J

.field public score:F

.field scoreBestMRU:F

.field private socialData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation
.end field

.field public final starred:Z

.field private timesContacted:I

.field private typeMatches:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;Z)V
    .locals 2
    .param p1, "displayName"    # Ljava/lang/String;
    .param p2, "contactID"    # J
    .param p4, "lookupKey"    # Ljava/lang/String;
    .param p5, "starred"    # Z

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    .line 42
    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->scoreBestMRU:F

    .line 45
    iput-boolean v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->typeMatches:Z

    .line 46
    iput v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->timesContacted:I

    .line 47
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->bestDetailScore:F

    .line 54
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->dataSet:Ljava/util/HashSet;

    .line 65
    iput-boolean v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->detailsAreSorted:Z

    .line 66
    iput-boolean v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->allDataSorted:Z

    .line 75
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    .line 77
    iput-wide p2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    .line 78
    iput-object p4, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->lookupKey:Ljava/lang/String;

    .line 79
    iput-boolean p5, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->starred:Z

    .line 80
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V
    .locals 2
    .param p1, "displayName"    # Ljava/lang/String;
    .param p2, "phoneticName"    # Ljava/lang/String;
    .param p3, "contactID"    # J
    .param p5, "lookupKey"    # Ljava/lang/String;
    .param p6, "starred"    # Z

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    .line 42
    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->scoreBestMRU:F

    .line 45
    iput-boolean v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->typeMatches:Z

    .line 46
    iput v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->timesContacted:I

    .line 47
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->bestDetailScore:F

    .line 54
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->dataSet:Ljava/util/HashSet;

    .line 65
    iput-boolean v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->detailsAreSorted:Z

    .line 66
    iput-boolean v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->allDataSorted:Z

    .line 83
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 84
    iput-object p2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    .line 85
    iput-wide p3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    .line 86
    iput-object p5, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->lookupKey:Ljava/lang/String;

    .line 87
    iput-boolean p6, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->starred:Z

    .line 88
    return-void
.end method

.method public static clone(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 120
    .local v0, "c":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 121
    .local v4, "t":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {v4}, Lcom/vlingo/core/internal/contacts/ContactMatch;->clone()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v1

    .line 122
    .local v1, "copy":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 125
    .end local v0    # "c":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v1    # "copy":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "t":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :catch_0
    move-exception v2

    .line 126
    .local v2, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/RuntimeException;

    const-string/jumbo v6, "List cloning unsupported"

    invoke-direct {v5, v6, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 124
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "c":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_0
    return-object v0
.end method

.method private filterPhonesByType([ILjava/util/List;)Ljava/util/List;
    .locals 2
    .param p1, "requestedTypes"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 576
    .local p2, "phonesList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 577
    .local v0, "filteredPhonesList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    if-eqz p1, :cond_0

    array-length v1, p1

    if-lez v1, :cond_0

    .line 578
    new-instance v1, Lcom/vlingo/core/internal/contacts/ContactMatch$1;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/core/internal/contacts/ContactMatch$1;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;[I)V

    invoke-static {p2, v1, v0}, Lcom/vlingo/sdk/util/CollectionUtils;->select(Ljava/lang/Iterable;Lcom/vlingo/sdk/util/Predicate;Ljava/util/Collection;)V

    .line 591
    :cond_0
    return-object v0
.end method

.method private getBestDetailScore()F
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 373
    iget v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->bestDetailScore:F

    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 374
    invoke-virtual {p0, v3}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData(Z)Ljava/util/List;

    move-result-object v0

    .line 375
    .local v0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 376
    const/4 v1, 0x0

    iput v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->bestDetailScore:F

    .line 381
    .end local v0    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :cond_0
    :goto_0
    iget v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->bestDetailScore:F

    return v1

    .line 378
    .restart local v0    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :cond_1
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/ContactData;->getScore()F

    move-result v1

    iput v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->bestDetailScore:F

    goto :goto_0
.end method

.method private getDefaultPhone(Ljava/util/List;)Lcom/vlingo/core/internal/contacts/ContactData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)",
            "Lcom/vlingo/core/internal/contacts/ContactData;"
        }
    .end annotation

    .prologue
    .line 595
    .local p1, "phonesList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactMatch$2;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/contacts/ContactMatch$2;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    invoke-static {p1, v0}, Lcom/vlingo/sdk/util/CollectionUtils;->find(Ljava/lang/Iterable;Lcom/vlingo/sdk/util/Predicate;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    return-object v0
.end method

.method private getOldestPhone(Ljava/util/List;)Lcom/vlingo/core/internal/contacts/ContactData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)",
            "Lcom/vlingo/core/internal/contacts/ContactData;"
        }
    .end annotation

    .prologue
    .line 616
    .local p1, "phonesList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactMatch$4;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/contacts/ContactMatch$4;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 622
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    return-object v0
.end method

.method private selectMobilePhones(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 604
    .local p1, "phonesList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 605
    .local v0, "mobilePhonesList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    new-instance v1, Lcom/vlingo/core/internal/contacts/ContactMatch$3;

    invoke-direct {v1, p0}, Lcom/vlingo/core/internal/contacts/ContactMatch$3;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    invoke-static {p1, v1, v0}, Lcom/vlingo/sdk/util/CollectionUtils;->select(Ljava/lang/Iterable;Lcom/vlingo/sdk/util/Predicate;Ljava/util/Collection;)V

    .line 612
    return-object v0
.end method


# virtual methods
.method public addAddress(Lcom/vlingo/core/internal/contacts/ContactData;)V
    .locals 1
    .param p1, "data"    # Lcom/vlingo/core/internal/contacts/ContactData;

    .prologue
    .line 153
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->dataSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 154
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->addressData:Ljava/util/List;

    if-nez v0, :cond_0

    .line 155
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->addressData:Ljava/util/List;

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->addressData:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->dataSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->allData:Ljava/util/List;

    .line 161
    :cond_1
    return-void
.end method

.method public addBirthday(Lcom/vlingo/core/internal/contacts/ContactData;)V
    .locals 1
    .param p1, "data"    # Lcom/vlingo/core/internal/contacts/ContactData;

    .prologue
    .line 164
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->dataSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 165
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->birthdayData:Ljava/util/List;

    if-nez v0, :cond_0

    .line 166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->birthdayData:Ljava/util/List;

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->birthdayData:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->dataSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->allData:Ljava/util/List;

    .line 172
    :cond_1
    return-void
.end method

.method public addEmail(Lcom/vlingo/core/internal/contacts/ContactData;)V
    .locals 1
    .param p1, "data"    # Lcom/vlingo/core/internal/contacts/ContactData;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->dataSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 143
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->emailData:Ljava/util/List;

    if-nez v0, :cond_0

    .line 144
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->emailData:Ljava/util/List;

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->emailData:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->dataSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 148
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->allData:Ljava/util/List;

    .line 150
    :cond_1
    return-void
.end method

.method addExtraName(Ljava/lang/String;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 501
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-static {v3, p1}, Lcom/vlingo/core/internal/util/StringUtils;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 525
    :cond_0
    :goto_0
    return-void

    .line 503
    :cond_1
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 505
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->extraNames:Ljava/util/List;

    if-nez v3, :cond_2

    .line 506
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->extraNames:Ljava/util/List;

    .line 507
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->extraNames:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 512
    :cond_2
    const/4 v0, 0x0

    .line 513
    .local v0, "found":Z
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->extraNames:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 514
    .local v2, "n":Ljava/lang/String;
    invoke-static {p1, v2}, Lcom/vlingo/core/internal/util/StringUtils;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 515
    const/4 v0, 0x1

    .line 519
    .end local v2    # "n":Ljava/lang/String;
    :cond_4
    if-nez v0, :cond_0

    .line 520
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->extraNames:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addMRUScore(F)V
    .locals 1
    .param p1, "scoreToAdd"    # F

    .prologue
    .line 536
    iget v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->scoreBestMRU:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->scoreBestMRU:F

    .line 537
    return-void
.end method

.method public addNameFields(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "firstNameParam"    # Ljava/lang/String;
    .param p2, "lastNameParam"    # Ljava/lang/String;

    .prologue
    .line 450
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->firstName:Ljava/lang/String;

    .line 451
    iput-object p2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->lastName:Ljava/lang/String;

    .line 452
    return-void
.end method

.method public addPhone(Lcom/vlingo/core/internal/contacts/ContactData;)V
    .locals 1
    .param p1, "data"    # Lcom/vlingo/core/internal/contacts/ContactData;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->dataSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 132
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    if-nez v0, :cond_0

    .line 133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->dataSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 136
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->allData:Ljava/util/List;

    .line 139
    :cond_1
    return-void
.end method

.method public addPhoneticFields(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "phoneticFirstNameParam"    # Ljava/lang/String;
    .param p2, "phoneticLastNameParam"    # Ljava/lang/String;

    .prologue
    .line 455
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticFirstName:Ljava/lang/String;

    .line 456
    iput-object p2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticLastName:Ljava/lang/String;

    .line 457
    return-void
.end method

.method public addScore(F)V
    .locals 1
    .param p1, "scoreToAdd"    # F

    .prologue
    .line 532
    iget v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    .line 533
    return-void
.end method

.method public addSocial(Lcom/vlingo/core/internal/contacts/ContactData;)V
    .locals 1
    .param p1, "data"    # Lcom/vlingo/core/internal/contacts/ContactData;

    .prologue
    .line 175
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->dataSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 176
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->socialData:Ljava/util/List;

    if-nez v0, :cond_0

    .line 177
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->socialData:Ljava/util/List;

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->dataSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 180
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->socialData:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->allData:Ljava/util/List;

    .line 183
    :cond_1
    return-void
.end method

.method public chooseSinglePhone([I)V
    .locals 5
    .param p1, "requestedTypes"    # [I

    .prologue
    const/4 v4, 0x1

    .line 546
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_1

    .line 573
    :cond_0
    :goto_0
    return-void

    .line 550
    :cond_1
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    invoke-direct {p0, p1, v2}, Lcom/vlingo/core/internal/contacts/ContactMatch;->filterPhonesByType([ILjava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 551
    .local v1, "phonesList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-static {v1}, Lcom/vlingo/sdk/util/CollectionUtils;->isEmpty(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 552
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 553
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 554
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 556
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eq v2, v4, :cond_0

    .line 561
    :cond_2
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    invoke-direct {p0, v2}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getDefaultPhone(Ljava/util/List;)Lcom/vlingo/core/internal/contacts/ContactData;

    move-result-object v0

    .line 562
    .local v0, "defaultPhone":Lcom/vlingo/core/internal/contacts/ContactData;
    if-nez v0, :cond_3

    .line 563
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    invoke-direct {p0, v2}, Lcom/vlingo/core/internal/contacts/ContactMatch;->selectMobilePhones(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 564
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_0

    .line 569
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v4, :cond_4

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    move-object v0, v2

    .line 571
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 572
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 569
    :cond_4
    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getOldestPhone(Ljava/util/List;)Lcom/vlingo/core/internal/contacts/ContactData;

    move-result-object v0

    goto :goto_1
.end method

.method public clone()Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 93
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 95
    .local v0, "clone":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->dataSet:Ljava/util/HashSet;

    if-nez v3, :cond_0

    move-object v3, v2

    :goto_0
    iput-object v3, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->dataSet:Ljava/util/HashSet;

    .line 96
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->emailData:Ljava/util/List;

    if-nez v3, :cond_1

    move-object v3, v2

    :goto_1
    iput-object v3, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->emailData:Ljava/util/List;

    .line 97
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    if-nez v3, :cond_2

    move-object v3, v2

    :goto_2
    iput-object v3, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    .line 98
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->addressData:Ljava/util/List;

    if-nez v3, :cond_3

    move-object v3, v2

    :goto_3
    iput-object v3, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->addressData:Ljava/util/List;

    .line 99
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->birthdayData:Ljava/util/List;

    if-nez v3, :cond_4

    move-object v3, v2

    :goto_4
    iput-object v3, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->birthdayData:Ljava/util/List;

    .line 100
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->socialData:Ljava/util/List;

    if-nez v3, :cond_5

    move-object v3, v2

    :goto_5
    iput-object v3, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->socialData:Ljava/util/List;

    .line 101
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->allData:Ljava/util/List;

    if-nez v3, :cond_6

    move-object v3, v2

    :goto_6
    iput-object v3, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->allData:Ljava/util/List;

    .line 102
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->extraNames:Ljava/util/List;

    if-nez v3, :cond_7

    move-object v3, v2

    :goto_7
    iput-object v3, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->extraNames:Ljava/util/List;

    .line 103
    iget v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->timesContacted:I

    iput v3, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->timesContacted:I

    .line 109
    .end local v0    # "clone":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :goto_8
    return-object v0

    .line 95
    .restart local v0    # "clone":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_0
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->dataSet:Ljava/util/HashSet;

    invoke-static {v3}, Lcom/vlingo/core/internal/contacts/ContactData;->clone(Ljava/util/HashSet;)Ljava/util/HashSet;

    move-result-object v3

    goto :goto_0

    .line 96
    :cond_1
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->emailData:Ljava/util/List;

    invoke-static {v3}, Lcom/vlingo/core/internal/contacts/ContactData;->clone(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    goto :goto_1

    .line 97
    :cond_2
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    invoke-static {v3}, Lcom/vlingo/core/internal/contacts/ContactData;->clone(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    goto :goto_2

    .line 98
    :cond_3
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->addressData:Ljava/util/List;

    invoke-static {v3}, Lcom/vlingo/core/internal/contacts/ContactData;->clone(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    goto :goto_3

    .line 99
    :cond_4
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->birthdayData:Ljava/util/List;

    invoke-static {v3}, Lcom/vlingo/core/internal/contacts/ContactData;->clone(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    goto :goto_4

    .line 100
    :cond_5
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->socialData:Ljava/util/List;

    invoke-static {v3}, Lcom/vlingo/core/internal/contacts/ContactData;->clone(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    goto :goto_5

    .line 101
    :cond_6
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->allData:Ljava/util/List;

    invoke-static {v3}, Lcom/vlingo/core/internal/contacts/ContactData;->clone(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    goto :goto_6

    .line 102
    :cond_7
    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->extraNames:Ljava/util/List;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_7

    .line 106
    .end local v0    # "clone":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :catch_0
    move-exception v1

    .line 107
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v1}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    move-object v0, v2

    .line 109
    goto :goto_8
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->clone()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v0

    return-object v0
.end method

.method public compareTo(Lcom/vlingo/core/internal/contacts/ContactMatch;)I
    .locals 4
    .param p1, "another"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x1

    .line 399
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getScore(Z)F

    move-result v2

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getScore(Z)F

    move-result v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 419
    :cond_0
    :goto_0
    return v0

    .line 401
    :cond_1
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getScore(Z)F

    move-result v2

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getScore(Z)F

    move-result v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    move v0, v1

    .line 402
    goto :goto_0

    .line 405
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getBestDetailScore()F

    move-result v2

    invoke-direct {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getBestDetailScore()F

    move-result v3

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_0

    .line 407
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getBestDetailScore()F

    move-result v2

    invoke-direct {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getBestDetailScore()F

    move-result v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3

    move v0, v1

    .line 408
    goto :goto_0

    .line 410
    :cond_3
    iget-boolean v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->starred:Z

    if-eqz v2, :cond_4

    iget-boolean v2, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->starred:Z

    if-eqz v2, :cond_0

    .line 411
    :cond_4
    iget-boolean v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->starred:Z

    if-nez v2, :cond_5

    iget-boolean v2, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->starred:Z

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    .line 413
    :cond_5
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getTimesContacted()I

    move-result v2

    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getTimesContacted()I

    move-result v3

    if-gt v2, v3, :cond_0

    .line 414
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getTimesContacted()I

    move-result v2

    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getTimesContacted()I

    move-result v3

    if-ge v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    .line 416
    :cond_6
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 417
    :cond_7
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    if-nez v0, :cond_8

    iget-object v0, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    if-eqz v0, :cond_8

    move v0, v1

    goto :goto_0

    .line 418
    :cond_8
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    if-nez v0, :cond_9

    iget-object v0, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    if-nez v0, :cond_9

    const/4 v0, 0x0

    goto :goto_0

    .line 419
    :cond_9
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    iget-object v1, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    neg-int v0, v0

    goto :goto_0
.end method

.method public compareTo(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/lang/String;)I
    .locals 1
    .param p1, "another"    # Lcom/vlingo/core/internal/contacts/ContactMatch;
    .param p2, "query"    # Ljava/lang/String;

    .prologue
    .line 430
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 431
    const/4 v0, 0x1

    .line 434
    :goto_0
    return v0

    .line 432
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 433
    const/4 v0, -0x1

    goto :goto_0

    .line 434
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 18
    check-cast p1, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->compareTo(Lcom/vlingo/core/internal/contacts/ContactMatch;)I

    move-result v0

    return v0
.end method

.method public computeMRUScore(Lcom/vlingo/core/internal/contacts/mru/MRU;Lcom/vlingo/core/internal/contacts/ContactType;)V
    .locals 2
    .param p1, "mru"    # Lcom/vlingo/core/internal/contacts/mru/MRU;
    .param p2, "actionType"    # Lcom/vlingo/core/internal/contacts/ContactType;

    .prologue
    .line 303
    iget-wide v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    long-to-int v0, v0

    invoke-virtual {p1, p2, v0}, Lcom/vlingo/core/internal/contacts/mru/MRU;->getNormalizedCount(Lcom/vlingo/core/internal/contacts/ContactType;I)F

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->scoreBestMRU:F

    .line 304
    return-void
.end method

.method public computeMRUScoresForData(Lcom/vlingo/core/internal/contacts/mru/MRU;Lcom/vlingo/core/internal/contacts/ContactType;)V
    .locals 5
    .param p1, "mru"    # Lcom/vlingo/core/internal/contacts/mru/MRU;
    .param p2, "actionType"    # Lcom/vlingo/core/internal/contacts/ContactType;

    .prologue
    .line 312
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v1

    .line 313
    .local v1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 314
    .local v0, "d":Lcom/vlingo/core/internal/contacts/ContactData;
    iget-wide v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    long-to-int v3, v3

    invoke-virtual {v0, p1, p2, v3}, Lcom/vlingo/core/internal/contacts/ContactData;->computeMRUScore(Lcom/vlingo/core/internal/contacts/mru/MRU;Lcom/vlingo/core/internal/contacts/ContactType;I)V

    goto :goto_0

    .line 316
    .end local v0    # "d":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_0
    return-void
.end method

.method public computeScores(I[I)V
    .locals 4
    .param p1, "preferredType"    # I
    .param p2, "requestedTypes"    # [I

    .prologue
    const/4 v3, 0x1

    .line 267
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 268
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 269
    .local v0, "d":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v0, p1, p2}, Lcom/vlingo/core/internal/contacts/ContactData;->computeScore(I[I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 270
    iput-boolean v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->typeMatches:Z

    goto :goto_0

    .line 274
    .end local v0    # "d":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->emailData:Ljava/util/List;

    if-eqz v2, :cond_3

    .line 275
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->emailData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 276
    .restart local v0    # "d":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v0, p1, p2}, Lcom/vlingo/core/internal/contacts/ContactData;->computeScore(I[I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 277
    iput-boolean v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->typeMatches:Z

    goto :goto_1

    .line 281
    .end local v0    # "d":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_3
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->addressData:Ljava/util/List;

    if-eqz v2, :cond_5

    .line 282
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->addressData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_4
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 283
    .restart local v0    # "d":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v0, p1, p2}, Lcom/vlingo/core/internal/contacts/ContactData;->computeScore(I[I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 284
    iput-boolean v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->typeMatches:Z

    goto :goto_2

    .line 288
    .end local v0    # "d":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_5
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->socialData:Ljava/util/List;

    if-eqz v2, :cond_7

    .line 289
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->socialData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_6
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 290
    .restart local v0    # "d":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v0, p1, p2}, Lcom/vlingo/core/internal/contacts/ContactData;->computeScore(I[I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 291
    iput-boolean v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->typeMatches:Z

    goto :goto_3

    .line 295
    .end local v0    # "d":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_7
    return-void
.end method

.method public containsData()Z
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->dataSet:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAddressData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->addressData:Ljava/util/List;

    return-object v0
.end method

.method public getAllData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 213
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData(Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAllData(Z)Ljava/util/List;
    .locals 2
    .param p1, "cache"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->allData:Ljava/util/List;

    .line 189
    .local v0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    if-nez v0, :cond_5

    .line 190
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 191
    .restart local v0    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->emailData:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 192
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->emailData:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 193
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 194
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 195
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->addressData:Ljava/util/List;

    if-eqz v1, :cond_2

    .line 196
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->addressData:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 197
    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->birthdayData:Ljava/util/List;

    if-eqz v1, :cond_3

    .line 198
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->birthdayData:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 199
    :cond_3
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->socialData:Ljava/util/List;

    if-eqz v1, :cond_4

    .line 200
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->socialData:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 201
    :cond_4
    if-eqz p1, :cond_5

    .line 202
    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->allData:Ljava/util/List;

    .line 204
    :cond_5
    iget-boolean v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->detailsAreSorted:Z

    if-eqz v1, :cond_6

    iget-boolean v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->allDataSorted:Z

    if-nez v1, :cond_6

    .line 205
    new-instance v1, Lcom/vlingo/core/internal/contacts/ContactData$ReverseComparator;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/ContactData$ReverseComparator;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 206
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->allData:Ljava/util/List;

    if-eqz v1, :cond_6

    .line 207
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->allDataSorted:Z

    .line 209
    :cond_6
    return-object v0
.end method

.method public getBirthdayData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->birthdayData:Ljava/util/List;

    return-object v0
.end method

.method public getData(Lcom/vlingo/core/internal/contacts/ContactType;)Ljava/util/List;
    .locals 2
    .param p1, "type"    # Lcom/vlingo/core/internal/contacts/ContactType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/contacts/ContactType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 237
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactMatch$5;->$SwitchMap$com$vlingo$core$internal$contacts$ContactType:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 246
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    .line 239
    :pswitch_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 240
    :pswitch_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getEmailData()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 241
    :pswitch_2
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAddressData()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 242
    :pswitch_3
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getBirthdayData()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 243
    :pswitch_4
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAddressData()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 244
    :pswitch_5
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getSocialData()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 237
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public getEmailData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->emailData:Ljava/util/List;

    return-object v0
.end method

.method public getExtraNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 258
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->extraNames:Ljava/util/List;

    return-object v0
.end method

.method public getFirstName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->firstName:Ljava/lang/String;

    return-object v0
.end method

.method public getLastName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->lastName:Ljava/lang/String;

    return-object v0
.end method

.method public getLookupKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->lookupKey:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    return-object v0
.end method

.method public getPhoneticFirstName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticFirstName:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneticLastName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticLastName:Ljava/lang/String;

    return-object v0
.end method

.method public getRawContactID()J
    .locals 2

    .prologue
    .line 442
    iget-wide v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->rawContactID:J

    return-wide v0
.end method

.method public getScore(Z)F
    .locals 2
    .param p1, "includeMRUScore"    # Z

    .prologue
    .line 323
    if-eqz p1, :cond_0

    .line 324
    iget v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    iget v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->scoreBestMRU:F

    add-float/2addr v0, v1

    .line 326
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    goto :goto_0
.end method

.method public getSocialData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 233
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->socialData:Ljava/util/List;

    return-object v0
.end method

.method public getTimesContacted()I
    .locals 1

    .prologue
    .line 250
    iget v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->timesContacted:I

    return v0
.end method

.method public hasExtraNames()Z
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->extraNames:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLookupData(Ljava/util/EnumSet;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 481
    invoke-virtual {p1}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;

    .line 482
    .local v1, "type":Lcom/vlingo/core/internal/contacts/ContactLookupType;
    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactMatch$5;->$SwitchMap$com$vlingo$core$internal$contacts$ContactLookupType:[I

    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/ContactLookupType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 497
    .end local v1    # "type":Lcom/vlingo/core/internal/contacts/ContactLookupType;
    :cond_0
    :goto_0
    return v2

    .line 484
    .restart local v1    # "type":Lcom/vlingo/core/internal/contacts/ContactLookupType;
    :pswitch_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAddressData()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAddressData()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_0

    :cond_1
    move v2, v3

    goto :goto_0

    .line 486
    :pswitch_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getEmailData()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getEmailData()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_0

    :cond_2
    move v2, v3

    goto :goto_0

    .line 488
    :pswitch_2
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getBirthdayData()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getBirthdayData()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_0

    :cond_3
    move v2, v3

    goto :goto_0

    .line 490
    :pswitch_3
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_0

    :cond_4
    move v2, v3

    goto :goto_0

    .line 492
    :pswitch_4
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getSocialData()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getSocialData()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_0

    :cond_5
    move v2, v3

    goto :goto_0

    .line 482
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public hasMatchedType()Z
    .locals 1

    .prologue
    .line 438
    iget-boolean v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->typeMatches:Z

    return v0
.end method

.method public setRawContactID(J)V
    .locals 0
    .param p1, "rawContactID"    # J

    .prologue
    .line 446
    iput-wide p1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->rawContactID:J

    .line 447
    return-void
.end method

.method public setTimesContacted(I)V
    .locals 0
    .param p1, "timesContacted"    # I

    .prologue
    .line 254
    iput p1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->timesContacted:I

    .line 255
    return-void
.end method

.method public sortDetails()V
    .locals 2

    .prologue
    .line 333
    iget-boolean v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->detailsAreSorted:Z

    if-nez v1, :cond_6

    .line 334
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactData$ReverseComparator;

    invoke-direct {v0}, Lcom/vlingo/core/internal/contacts/ContactData$ReverseComparator;-><init>()V

    .line 335
    .local v0, "comp":Lcom/vlingo/core/internal/contacts/ContactData$ReverseComparator;
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 336
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneData:Ljava/util/List;

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 338
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->emailData:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 339
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->emailData:Ljava/util/List;

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 341
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->addressData:Ljava/util/List;

    if-eqz v1, :cond_2

    .line 342
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->addressData:Ljava/util/List;

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 344
    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->birthdayData:Ljava/util/List;

    if-eqz v1, :cond_3

    .line 345
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->birthdayData:Ljava/util/List;

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 347
    :cond_3
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->socialData:Ljava/util/List;

    if-eqz v1, :cond_4

    .line 348
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->socialData:Ljava/util/List;

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 350
    :cond_4
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->allData:Ljava/util/List;

    if-eqz v1, :cond_5

    .line 351
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->allData:Ljava/util/List;

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 353
    :cond_5
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->detailsAreSorted:Z

    .line 355
    .end local v0    # "comp":Lcom/vlingo/core/internal/contacts/ContactData$ReverseComparator;
    :cond_6
    return-void
.end method

.method public toDescriptiveString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 363
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 364
    .local v3, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 365
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData(Z)Ljava/util/List;

    move-result-object v1

    .line 366
    .local v1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 367
    .local v0, "d":Lcom/vlingo/core/internal/contacts/ContactData;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactData;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 369
    .end local v0    # "d":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 359
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "[ContactMatch] name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " contactID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " starred="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->starred:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " lookupKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->lookupKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " score="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getScore(Z)F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " timesContacted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch;->timesContacted:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
