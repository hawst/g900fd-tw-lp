.class public Lcom/vlingo/core/internal/util/SparseArrayMap;
.super Landroid/util/SparseArray;
.source "SparseArrayMap.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/util/SparseArrayMap$1;,
        Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/util/SparseArray",
        "<TT;>;",
        "Ljava/lang/Iterable",
        "<",
        "Ljava/util/Map$Entry",
        "<",
        "Ljava/lang/Integer;",
        "TT;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    .local p0, "this":Lcom/vlingo/core/internal/util/SparseArrayMap;, "Lcom/vlingo/core/internal/util/SparseArrayMap<TT;>;"
    invoke-direct {p0}, Landroid/util/SparseArray;-><init>()V

    .line 17
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "initialCapacity"    # I

    .prologue
    .line 20
    .local p0, "this":Lcom/vlingo/core/internal/util/SparseArrayMap;, "Lcom/vlingo/core/internal/util/SparseArrayMap<TT;>;"
    invoke-direct {p0, p1}, Landroid/util/SparseArray;-><init>(I)V

    .line 21
    return-void
.end method

.method public static joinKeys(Lcom/vlingo/core/internal/util/SparseArrayMap;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "delimiter"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/vlingo/core/internal/util/SparseArrayMap",
            "<TT;>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 36
    .local p0, "list":Lcom/vlingo/core/internal/util/SparseArrayMap;, "Lcom/vlingo/core/internal/util/SparseArrayMap<TT;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 38
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/SparseArrayMap;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 39
    if-lez v0, :cond_0

    .line 40
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    :cond_0
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/SparseArrayMap;->keyAt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 38
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 45
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public containsKey(Ljava/lang/Integer;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/Integer;

    .prologue
    .line 28
    .local p0, "this":Lcom/vlingo/core/internal/util/SparseArrayMap;, "Lcom/vlingo/core/internal/util/SparseArrayMap<TT;>;"
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/SparseArrayMap;->indexOfKey(I)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 32
    .local p0, "this":Lcom/vlingo/core/internal/util/SparseArrayMap;, "Lcom/vlingo/core/internal/util/SparseArrayMap<TT;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/util/SparseArrayMap;->indexOfValue(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 24
    .local p0, "this":Lcom/vlingo/core/internal/util/SparseArrayMap;, "Lcom/vlingo/core/internal/util/SparseArrayMap<TT;>;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/SparseArrayMap;->size()I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Integer;",
            "TT;>;>;"
        }
    .end annotation

    .prologue
    .line 66
    .local p0, "this":Lcom/vlingo/core/internal/util/SparseArrayMap;, "Lcom/vlingo/core/internal/util/SparseArrayMap<TT;>;"
    new-instance v0, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;-><init>(Lcom/vlingo/core/internal/util/SparseArrayMap;Lcom/vlingo/core/internal/util/SparseArrayMap$1;)V

    return-object v0
.end method

.method public keySet()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    .local p0, "this":Lcom/vlingo/core/internal/util/SparseArrayMap;, "Lcom/vlingo/core/internal/util/SparseArrayMap<TT;>;"
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 50
    .local v1, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/SparseArrayMap;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 51
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/SparseArrayMap;->keyAt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 50
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_0
    return-object v1
.end method

.method public values()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 57
    .local p0, "this":Lcom/vlingo/core/internal/util/SparseArrayMap;, "Lcom/vlingo/core/internal/util/SparseArrayMap<TT;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 58
    .local v1, "values":Ljava/util/List;, "Ljava/util/List<TT;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/SparseArrayMap;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 59
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/SparseArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 61
    :cond_0
    return-object v1
.end method
