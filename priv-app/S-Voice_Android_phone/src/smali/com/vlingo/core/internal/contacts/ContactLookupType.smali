.class public final enum Lcom/vlingo/core/internal/contacts/ContactLookupType;
.super Ljava/lang/Enum;
.source "ContactLookupType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/contacts/ContactLookupType;

.field public static final enum ADDRESS:Lcom/vlingo/core/internal/contacts/ContactLookupType;

.field public static final enum BIRTHDAY:Lcom/vlingo/core/internal/contacts/ContactLookupType;

.field public static final enum EMAIL_ADDRESS:Lcom/vlingo/core/internal/contacts/ContactLookupType;

.field public static final enum PHONE_NUMBER:Lcom/vlingo/core/internal/contacts/ContactLookupType;

.field public static final enum SOCIAL_NETWORK:Lcom/vlingo/core/internal/contacts/ContactLookupType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactLookupType;

    const-string/jumbo v1, "PHONE_NUMBER"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/contacts/ContactLookupType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactLookupType;->PHONE_NUMBER:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    .line 10
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactLookupType;

    const-string/jumbo v1, "EMAIL_ADDRESS"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/contacts/ContactLookupType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactLookupType;->EMAIL_ADDRESS:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    .line 11
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactLookupType;

    const-string/jumbo v1, "ADDRESS"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/contacts/ContactLookupType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactLookupType;->ADDRESS:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    .line 12
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactLookupType;

    const-string/jumbo v1, "SOCIAL_NETWORK"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/contacts/ContactLookupType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactLookupType;->SOCIAL_NETWORK:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    .line 13
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactLookupType;

    const-string/jumbo v1, "BIRTHDAY"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/core/internal/contacts/ContactLookupType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactLookupType;->BIRTHDAY:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    .line 8
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vlingo/core/internal/contacts/ContactLookupType;

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->PHONE_NUMBER:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->EMAIL_ADDRESS:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->ADDRESS:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->SOCIAL_NETWORK:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->BIRTHDAY:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactLookupType;->$VALUES:[Lcom/vlingo/core/internal/contacts/ContactLookupType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactLookupType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 8
    const-class v0, Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactLookupType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/contacts/ContactLookupType;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactLookupType;->$VALUES:[Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/contacts/ContactLookupType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/contacts/ContactLookupType;

    return-object v0
.end method
