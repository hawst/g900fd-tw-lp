.class public final Lcom/vlingo/core/internal/contacts/ContactConstants;
.super Ljava/lang/Object;
.source "ContactConstants.java"


# static fields
.field public static final EXTRA_NAME_SCORE_COST:I = -0x2

.field static final HOME_PHONE_BIAS:F = 0.004f

.field static final MOBILE_PHONE_BIAS:F = 0.006f

.field static final PHONE_BIAS:F = 0.003f

.field static final PREFERRED_TYPE_BOOST:F = 0.9f

.field public static final PRUNING_THRESHOLD:F = 10.0f

.field static final REQUESTED_TYPE_BOOST:F = 0.99f

.field public static final SEARCH_SCORE_EXTRA_WORD_COST:I = -0x1

.field public static final SEARCH_SCORE_FULL_MATCH_CORRECT_POS:I = 0x14

.field public static final SEARCH_SCORE_FULL_MATCH_WRONG_POS:I = 0x12

.field public static final SEARCH_SCORE_MISSING_WORD_COST:I = -0x1

.field public static final SEARCH_SCORE_PARTIAL_MATCH_CORRECT_POS:I = 0xa

.field public static final SEARCH_SCORE_PARTIAL_MATCH_WRONG_POS:I = 0x8

.field public static final SOCIAL_TYPE_FACEBOOK:I = 0x7d1

.field public static final THRESHOLD_AUTO_ACTION_REC_CONFIDENCE_WHEN_CONFIDENT:F = 0.1f

.field public static final THRESHOLD_AUTO_ACTION_SCORE_WHEN_CONFIDENT:F = 0.05f

.field static final WORK_PHONE_BIAS:F = 0.005f

.field public static final WRONG_TYPE_MULT:F = 0.1f


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
