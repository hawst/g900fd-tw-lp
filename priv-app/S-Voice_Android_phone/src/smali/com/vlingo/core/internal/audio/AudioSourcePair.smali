.class public Lcom/vlingo/core/internal/audio/AudioSourcePair;
.super Ljava/lang/Object;
.source "AudioSourcePair.java"


# instance fields
.field private fallback:I

.field private preferred:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "preferred"    # I
    .param p2, "fallback"    # I

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput p1, p0, Lcom/vlingo/core/internal/audio/AudioSourcePair;->preferred:I

    .line 16
    iput p2, p0, Lcom/vlingo/core/internal/audio/AudioSourcePair;->fallback:I

    .line 17
    return-void
.end method


# virtual methods
.method public getFallback()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/vlingo/core/internal/audio/AudioSourcePair;->fallback:I

    return v0
.end method

.method public getPreferred()I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lcom/vlingo/core/internal/audio/AudioSourcePair;->preferred:I

    return v0
.end method
