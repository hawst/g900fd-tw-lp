.class public interface abstract Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
.super Ljava/lang/Object;
.source "WidgetActionListener.java"


# static fields
.field public static final ACTION_ACCEPTED_TEXT:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.AcceptedText"

.field public static final ACTION_BODY_CHANGE:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.BodyChange"

.field public static final ACTION_CALL:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.Call"

.field public static final ACTION_CANCEL:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.Cancel"

.field public static final ACTION_CHOICE:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.Choice"

.field public static final ACTION_CONTACT_CHOICE:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.ContactChoice"

.field public static final ACTION_DATA_TRANSFER:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.DataTransfered"

.field public static final ACTION_DEFAULT:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.Default"

.field public static final ACTION_DIRECTREPLY:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.DirectReply"

.field public static final ACTION_EDIT:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.Edit"

.field public static final ACTION_EDIT_TEXT_CLICKED:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.EditTextClicked"

.field public static final ACTION_FORWARD:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.Forward"

.field public static final ACTION_LOCAL_SEARCH_RETRY:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.LocalSearchRetry"

.field public static final ACTION_NEXT:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.Next"

.field public static final ACTION_NO_DATA:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.NoData"

.field public static final ACTION_PREV:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.Prev"

.field public static final ACTION_READOUT:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.Readout"

.field public static final ACTION_RECO:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.PerformReco"

.field public static final ACTION_REPLY:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.Reply"

.field public static final ACTION_SAVE:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.Save"

.field public static final ACTION_SOCIAL_CHANGE:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.SocialChange"

.field public static final ACTION_SUBJECT_CHANGE:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.SubjectChange"

.field public static final ACTION_UPDATE_TIME:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.UpdateTime"

.field public static final ACTION_VIEW:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.View"

.field public static final ACTION_VOL_DOWN:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.VolumeDown"

.field public static final ACTION_VOL_UP:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.VolumeUp"

.field public static final BUNDLE_ACCEPTED_TEXT:Ljava/lang/String; = "accepted_text"

.field public static final BUNDLE_ADDRESS:Ljava/lang/String; = "address"

.field public static final BUNDLE_CHOICE:Ljava/lang/String; = "choice"

.field public static final BUNDLE_DESCRIPTION:Ljava/lang/String; = "description"

.field public static final BUNDLE_EDITED_TEXT:Ljava/lang/String; = "edited_text"

.field public static final BUNDLE_ENABLE:Ljava/lang/String; = "enable"

.field public static final BUNDLE_FROM_ITEM_COUNT:Ljava/lang/String; = "item_count"

.field public static final BUNDLE_FROM_READ_MESSAGES:Ljava/lang/String; = "from_read_messages"

.field public static final BUNDLE_ID:Ljava/lang/String; = "id"

.field public static final BUNDLE_MESSAGE:Ljava/lang/String; = "message"

.field public static final BUNDLE_MESSAGE_TYPE:Ljava/lang/String; = "message_type"

.field public static final BUNDLE_PHONE_CHOICE:Ljava/lang/String; = "phone choice"

.field public static final BUNDLE_SEARCH_LOCATION:Ljava/lang/String; = "location"

.field public static final BUNDLE_SEARCH_TERM:Ljava/lang/String; = "search"

.field public static final BUNDLE_SOCIAL_CHOICE:Ljava/lang/String; = "social choice"

.field public static final BUNDLE_SUBJECT:Ljava/lang/String; = "subject"

.field public static final BUNDLE_TIME:Ljava/lang/String; = "time"

.field public static final BUNDLE_TITLE:Ljava/lang/String; = "title"


# virtual methods
.method public abstract handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation
.end method
