.class public Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;
.super Ljava/lang/Object;
.source "CMAWeatherElement.java"


# instance fields
.field public lastUpdate:Ljava/util/Date;

.field public location:Lcom/vlingo/core/internal/weather/china/CMALocationElement;

.field public nameCity:Ljava/lang/String;

.field public nameCountry:Ljava/lang/String;

.field public nameProvince:Ljava/lang/String;

.field public weatherCurrent:Lcom/vlingo/core/internal/weather/china/WeatherCurrent;

.field public weatherItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/weather/china/WeatherItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    return-void
.end method
