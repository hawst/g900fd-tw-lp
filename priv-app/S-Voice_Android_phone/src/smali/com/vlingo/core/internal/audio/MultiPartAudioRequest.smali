.class public Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;
.super Lcom/vlingo/core/internal/audio/AudioRequest;
.source "MultiPartAudioRequest.java"


# instance fields
.field private currentPart:I

.field private final parts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/audio/AudioRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;-><init>([Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 15
    return-void
.end method

.method public constructor <init>([Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 5
    .param p1, "requests"    # [Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/AudioRequest;-><init>()V

    .line 19
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->parts:Ljava/util/ArrayList;

    .line 20
    const/4 v4, 0x0

    iput v4, p0, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->currentPart:I

    .line 21
    if-eqz p1, :cond_0

    .line 22
    move-object v0, p1

    .local v0, "arr$":[Lcom/vlingo/core/internal/audio/AudioRequest;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 23
    .local v3, "req":Lcom/vlingo/core/internal/audio/AudioRequest;
    invoke-virtual {p0, v3}, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->addRequest(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 22
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 26
    .end local v0    # "arr$":[Lcom/vlingo/core/internal/audio/AudioRequest;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "req":Lcom/vlingo/core/internal/audio/AudioRequest;
    :cond_0
    return-void
.end method

.method public static varargs getMultipartRequest([Lcom/vlingo/core/internal/audio/AudioRequest;)Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;
    .locals 5
    .param p0, "requests"    # [Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 40
    new-instance v3, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;

    invoke-direct {v3}, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;-><init>()V

    .line 41
    .local v3, "multipartReq":Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;
    move-object v0, p0

    .local v0, "arr$":[Lcom/vlingo/core/internal/audio/AudioRequest;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 42
    .local v4, "req":Lcom/vlingo/core/internal/audio/AudioRequest;
    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->addRequest(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 41
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 44
    .end local v4    # "req":Lcom/vlingo/core/internal/audio/AudioRequest;
    :cond_0
    return-object v3
.end method


# virtual methods
.method public addRequest(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "req"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->parts:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30
    return-void
.end method

.method areMorePartsWaiting()Z
    .locals 2

    .prologue
    .line 75
    iget v0, p0, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->currentPart:I

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->parts:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLastRequest()Lcom/vlingo/core/internal/audio/AudioRequest;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->parts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->getRequest(I)Lcom/vlingo/core/internal/audio/AudioRequest;

    move-result-object v0

    return-object v0
.end method

.method public getRequest(I)Lcom/vlingo/core/internal/audio/AudioRequest;
    .locals 1
    .param p1, "which"    # I

    .prologue
    .line 33
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->parts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->parts:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/audio/AudioRequest;

    .line 36
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isOnFirstPart()Z
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->currentPart:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isOnLastPart()Z
    .locals 2

    .prologue
    .line 79
    iget v0, p0, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->currentPart:I

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->parts:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public prepareForPlayback(Landroid/content/Context;Lcom/vlingo/core/internal/audio/AudioPlayer;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "player"    # Lcom/vlingo/core/internal/audio/AudioPlayer;

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->getLastRequest()Lcom/vlingo/core/internal/audio/AudioRequest;

    move-result-object v1

    .line 50
    .local v1, "lastRequest":Lcom/vlingo/core/internal/audio/AudioRequest;
    iget-object v3, p0, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->parts:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/audio/AudioRequest;

    .line 51
    .local v2, "req":Lcom/vlingo/core/internal/audio/AudioRequest;
    if-eq v2, v1, :cond_1

    .line 52
    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/audio/AudioRequest;->setFlag(I)V

    .line 54
    :cond_1
    invoke-virtual {v2, p1, p2}, Lcom/vlingo/core/internal/audio/AudioRequest;->prepareForPlayback(Landroid/content/Context;Lcom/vlingo/core/internal/audio/AudioPlayer;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 55
    const/4 v3, 0x0

    .line 58
    .end local v2    # "req":Lcom/vlingo/core/internal/audio/AudioRequest;
    :goto_0
    return v3

    :cond_2
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public setDataSource(Landroid/content/Context;Landroid/media/MediaPlayer;Lcom/vlingo/core/internal/audio/AudioPlayer;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mediaPlayer"    # Landroid/media/MediaPlayer;
    .param p3, "player"    # Lcom/vlingo/core/internal/audio/AudioPlayer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->areMorePartsWaiting()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->parts:Ljava/util/ArrayList;

    iget v2, p0, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->currentPart:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/audio/AudioRequest;

    .line 65
    .local v0, "req":Lcom/vlingo/core/internal/audio/AudioRequest;
    iget v1, p0, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->currentPart:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->currentPart:I

    .line 66
    invoke-virtual {v0, p1, p2, p3}, Lcom/vlingo/core/internal/audio/AudioRequest;->setDataSource(Landroid/content/Context;Landroid/media/MediaPlayer;Lcom/vlingo/core/internal/audio/AudioPlayer;)V

    .line 68
    .end local v0    # "req":Lcom/vlingo/core/internal/audio/AudioRequest;
    :cond_0
    return-void
.end method
