.class public Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactChoicesController;
.super Lcom/vlingo/core/internal/dialogmanager/Controller;
.source "ShowContactChoicesController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# instance fields
.field private listUtil:Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/Controller;-><init>()V

    .line 25
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactChoicesController;->listUtil:Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 7
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 33
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/Controller;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 35
    const-string/jumbo v0, "ListPosition"

    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    const-string/jumbo v0, "choices"

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    .line 39
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v0

    const-string/jumbo v1, "contact"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactChoicesController;->listUtil:Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowContactChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v6, 0x0

    move-object v1, p2

    move-object v2, p1

    move-object v5, p0

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->showChoiceListWidget(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;Ljava/util/Map;)Z

    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "unused"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactChoicesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 58
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 59
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactChoicesController;->listUtil:Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;

    invoke-virtual {v2, p1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->getIdOfSelection(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 60
    .local v1, "id":Ljava/lang/String;
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ChoiceSelectedEvent;

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/events/ChoiceSelectedEvent;-><init>(Ljava/lang/String;)V

    .line 61
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ChoiceSelectedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactChoicesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactChoicesController;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v2, v0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 66
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ChoiceSelectedEvent;
    .end local v1    # "id":Ljava/lang/String;
    :goto_0
    return-void

    .line 64
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactChoicesController;->throwUnknownActionException(Ljava/lang/String;)V

    goto :goto_0
.end method
