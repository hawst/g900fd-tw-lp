.class Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;
.super Landroid/os/Handler;
.source "TonePlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/audio/TonePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlaybackHandler"
.end annotation


# static fields
.field private static final PLAY:I = 0x1

.field private static final WAIT_FOR_STOP:I = 0x2


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/audio/TonePlayer;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/audio/TonePlayer;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;->this$0:Lcom/vlingo/core/internal/audio/TonePlayer;

    .line 77
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 78
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x2

    .line 82
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 84
    :pswitch_0
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;->this$0:Lcom/vlingo/core/internal/audio/TonePlayer;

    # invokes: Lcom/vlingo/core/internal/audio/TonePlayer;->playInternal()V
    invoke-static {v1}, Lcom/vlingo/core/internal/audio/TonePlayer;->access$000(Lcom/vlingo/core/internal/audio/TonePlayer;)V

    goto :goto_0

    .line 87
    :pswitch_1
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;->this$0:Lcom/vlingo/core/internal/audio/TonePlayer;

    # getter for: Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;
    invoke-static {v1}, Lcom/vlingo/core/internal/audio/TonePlayer;->access$100(Lcom/vlingo/core/internal/audio/TonePlayer;)Landroid/media/AudioTrack;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;->this$0:Lcom/vlingo/core/internal/audio/TonePlayer;

    # getter for: Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;
    invoke-static {v1}, Lcom/vlingo/core/internal/audio/TonePlayer;->access$100(Lcom/vlingo/core/internal/audio/TonePlayer;)Landroid/media/AudioTrack;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v1

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;->this$0:Lcom/vlingo/core/internal/audio/TonePlayer;

    # getter for: Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;
    invoke-static {v1}, Lcom/vlingo/core/internal/audio/TonePlayer;->access$100(Lcom/vlingo/core/internal/audio/TonePlayer;)Landroid/media/AudioTrack;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioTrack;->getState()I

    move-result v1

    if-ne v1, v2, :cond_3

    .line 88
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;->this$0:Lcom/vlingo/core/internal/audio/TonePlayer;

    # getter for: Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;
    invoke-static {v1}, Lcom/vlingo/core/internal/audio/TonePlayer;->access$100(Lcom/vlingo/core/internal/audio/TonePlayer;)Landroid/media/AudioTrack;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioTrack;->getPlaybackHeadPosition()I

    move-result v0

    .line 89
    .local v0, "getPlaybackHeadPos":I
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;->this$0:Lcom/vlingo/core/internal/audio/TonePlayer;

    # getter for: Lcom/vlingo/core/internal/audio/TonePlayer;->totalSamples:I
    invoke-static {v1}, Lcom/vlingo/core/internal/audio/TonePlayer;->access$200(Lcom/vlingo/core/internal/audio/TonePlayer;)I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 93
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;->this$0:Lcom/vlingo/core/internal/audio/TonePlayer;

    # getter for: Lcom/vlingo/core/internal/audio/TonePlayer;->audioTrack:Landroid/media/AudioTrack;
    invoke-static {v1}, Lcom/vlingo/core/internal/audio/TonePlayer;->access$100(Lcom/vlingo/core/internal/audio/TonePlayer;)Landroid/media/AudioTrack;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioTrack;->stop()V

    .line 94
    # getter for: Lcom/vlingo/core/internal/audio/TonePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/audio/TonePlayer;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "[LatencyCheck] audioTrack.stop since audioTrack.getPlaybackHeadPosition() >= totalSamples"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;->this$0:Lcom/vlingo/core/internal/audio/TonePlayer;

    # getter for: Lcom/vlingo/core/internal/audio/TonePlayer;->notificationHandler:Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;
    invoke-static {v1}, Lcom/vlingo/core/internal/audio/TonePlayer;->access$400(Lcom/vlingo/core/internal/audio/TonePlayer;)Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 96
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;->this$0:Lcom/vlingo/core/internal/audio/TonePlayer;

    # getter for: Lcom/vlingo/core/internal/audio/TonePlayer;->notificationHandler:Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;
    invoke-static {v1}, Lcom/vlingo/core/internal/audio/TonePlayer;->access$400(Lcom/vlingo/core/internal/audio/TonePlayer;)Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 102
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;->this$0:Lcom/vlingo/core/internal/audio/TonePlayer;

    # invokes: Lcom/vlingo/core/internal/audio/TonePlayer;->cleanup()V
    invoke-static {v1}, Lcom/vlingo/core/internal/audio/TonePlayer;->access$500(Lcom/vlingo/core/internal/audio/TonePlayer;)V

    goto :goto_0

    .line 104
    :cond_2
    # getter for: Lcom/vlingo/core/internal/audio/TonePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/audio/TonePlayer;->access$300()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "[LatencyCheck] Wait for 10ms more, getPlaybackHeadPos ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    const-wide/16 v1, 0xa

    invoke-virtual {p0, v4, v1, v2}, Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 113
    .end local v0    # "getPlaybackHeadPos":I
    :cond_3
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;->this$0:Lcom/vlingo/core/internal/audio/TonePlayer;

    # getter for: Lcom/vlingo/core/internal/audio/TonePlayer;->notificationHandler:Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;
    invoke-static {v1}, Lcom/vlingo/core/internal/audio/TonePlayer;->access$400(Lcom/vlingo/core/internal/audio/TonePlayer;)Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 114
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;->this$0:Lcom/vlingo/core/internal/audio/TonePlayer;

    # getter for: Lcom/vlingo/core/internal/audio/TonePlayer;->notificationHandler:Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;
    invoke-static {v1}, Lcom/vlingo/core/internal/audio/TonePlayer;->access$400(Lcom/vlingo/core/internal/audio/TonePlayer;)Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/vlingo/core/internal/audio/TonePlayer$NotificationHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 115
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/TonePlayer$PlaybackHandler;->this$0:Lcom/vlingo/core/internal/audio/TonePlayer;

    # invokes: Lcom/vlingo/core/internal/audio/TonePlayer;->cleanup()V
    invoke-static {v1}, Lcom/vlingo/core/internal/audio/TonePlayer;->access$500(Lcom/vlingo/core/internal/audio/TonePlayer;)V

    goto/16 :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
