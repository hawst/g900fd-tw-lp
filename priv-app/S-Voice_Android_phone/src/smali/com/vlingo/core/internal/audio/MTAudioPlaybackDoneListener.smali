.class public abstract Lcom/vlingo/core/internal/audio/MTAudioPlaybackDoneListener;
.super Ljava/lang/Object;
.source "MTAudioPlaybackDoneListener.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method


# virtual methods
.method public final onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 17
    new-instance v0, Lcom/vlingo/core/internal/audio/MTAudioPlaybackDoneListener$1;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/audio/MTAudioPlaybackDoneListener$1;-><init>(Lcom/vlingo/core/internal/audio/MTAudioPlaybackDoneListener;Lcom/vlingo/core/internal/audio/AudioRequest;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 22
    return-void
.end method

.method public final onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 25
    new-instance v0, Lcom/vlingo/core/internal/audio/MTAudioPlaybackDoneListener$2;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/audio/MTAudioPlaybackDoneListener$2;-><init>(Lcom/vlingo/core/internal/audio/MTAudioPlaybackDoneListener;Lcom/vlingo/core/internal/audio/AudioRequest;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 30
    return-void
.end method

.method public abstract onRequestDone(Lcom/vlingo/core/internal/audio/AudioRequest;)V
.end method

.method public final onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 33
    new-instance v0, Lcom/vlingo/core/internal/audio/MTAudioPlaybackDoneListener$3;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/audio/MTAudioPlaybackDoneListener$3;-><init>(Lcom/vlingo/core/internal/audio/MTAudioPlaybackDoneListener;Lcom/vlingo/core/internal/audio/AudioRequest;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 38
    return-void
.end method

.method public final onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 41
    return-void
.end method
