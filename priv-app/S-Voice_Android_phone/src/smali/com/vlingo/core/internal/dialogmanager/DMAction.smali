.class public abstract Lcom/vlingo/core/internal/dialogmanager/DMAction;
.super Ljava/lang/Object;
.source "DMAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    }
.end annotation


# instance fields
.field protected actionHandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

.field private context:Landroid/content/Context;

.field private listener:Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DMAction;->listener:Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionAbort()V

    .line 96
    return-void
.end method

.method public actionHandlerListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;
    .locals 0
    .param p1, "listenerParam"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DMAction;->actionHandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 128
    return-object p0
.end method

.method public context(Landroid/content/Context;)Lcom/vlingo/core/internal/dialogmanager/DMAction;
    .locals 0
    .param p1, "contextParam"    # Landroid/content/Context;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DMAction;->context:Landroid/content/Context;

    .line 120
    return-object p0
.end method

.method protected abstract execute()V
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DMAction;->context:Landroid/content/Context;

    return-object v0
.end method

.method protected getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DMAction;->listener:Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    return-object v0
.end method

.method protected init(Landroid/content/Context;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)V
    .locals 0
    .param p1, "contextParam"    # Landroid/content/Context;
    .param p2, "listenerParam"    # Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    .prologue
    .line 143
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->setContext(Landroid/content/Context;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    .line 144
    invoke-virtual {p0, p2}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->setListener(Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    .line 145
    return-void
.end method

.method public listener(Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;
    .locals 0
    .param p1, "listenerParam"    # Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DMAction;->listener:Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    .line 124
    return-object p0
.end method

.method public queue()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DMAction;->actionHandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DMAction;->actionHandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    invoke-interface {v0, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->execute(Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;)V

    .line 83
    :cond_0
    return-void
.end method

.method public readyToExec()V
    .locals 0

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->execute()V

    .line 90
    return-void
.end method

.method public setActionHandlerListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;
    .locals 1
    .param p1, "listenerParam"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 114
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->actionHandlerListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    return-object v0
.end method

.method public setContext(Landroid/content/Context;)Lcom/vlingo/core/internal/dialogmanager/DMAction;
    .locals 1
    .param p1, "contextParam"    # Landroid/content/Context;

    .prologue
    .line 106
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->context(Landroid/content/Context;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    return-object v0
.end method

.method public setListener(Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;
    .locals 1
    .param p1, "listenerParam"    # Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    .prologue
    .line 110
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->listener(Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-result-object v0

    return-object v0
.end method
