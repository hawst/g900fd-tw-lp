.class public final enum Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;
.super Ljava/lang/Enum;
.source "AcceptedText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TextType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

.field public static final enum DIAL:Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

.field public static final enum MEMO:Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

.field public static final enum SMS:Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 16
    new-instance v0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    const-string/jumbo v1, "SMS"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;->SMS:Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    new-instance v0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    const-string/jumbo v1, "MEMO"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;->MEMO:Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    new-instance v0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    const-string/jumbo v1, "DIAL"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;->DIAL:Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    sget-object v1, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;->SMS:Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;->MEMO:Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;->DIAL:Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;->$VALUES:[Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 16
    const-class v0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;->$VALUES:[Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    return-object v0
.end method
