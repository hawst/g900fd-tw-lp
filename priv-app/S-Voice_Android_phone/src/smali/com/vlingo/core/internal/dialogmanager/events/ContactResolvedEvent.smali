.class public Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;
.super Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;
.source "ContactResolvedEvent.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent",
        "<",
        "Lcom/vlingo/core/internal/contacts/ContactMatch;",
        ">;"
    }
.end annotation


# instance fields
.field private final NAME:Ljava/lang/String;

.field private final detailed:Z

.field private final phoneType:[I

.field private final type:Lcom/vlingo/core/internal/contacts/ContactType;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;-><init>()V

    .line 25
    const-string/jumbo v0, "contact-resolved"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;->NAME:Ljava/lang/String;

    .line 40
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->UNDEFINED:Lcom/vlingo/core/internal/contacts/ContactType;

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;->detailed:Z

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;->phoneType:[I

    .line 43
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/vlingo/core/internal/contacts/ContactType;ZII[I)V
    .locals 1
    .param p2, "type"    # Lcom/vlingo/core/internal/contacts/ContactType;
    .param p3, "detailed"    # Z
    .param p4, "offset"    # I
    .param p5, "totalCount"    # I
    .param p6, "phoneType"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Lcom/vlingo/core/internal/contacts/ContactType;",
            "ZII[I)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-direct {p0, p1, p4, p5}, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;-><init>(Ljava/util/List;II)V

    .line 25
    const-string/jumbo v0, "contact-resolved"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;->NAME:Ljava/lang/String;

    .line 33
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 34
    iput-boolean p3, p0, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;->detailed:Z

    .line 35
    iput-object p6, p0, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;->phoneType:[I

    .line 36
    return-void
.end method

.method private getTypeString(Lcom/vlingo/core/internal/contacts/ContactData;)Ljava/lang/String;
    .locals 2
    .param p1, "data"    # Lcom/vlingo/core/internal/contacts/ContactData;

    .prologue
    .line 108
    const-string/jumbo v0, "other"

    .line 109
    .local v0, "result":Ljava/lang/String;
    iget v1, p1, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    packed-switch v1, :pswitch_data_0

    .line 121
    :goto_0
    return-object v0

    .line 110
    :pswitch_0
    const-string/jumbo v0, "mobile"

    goto :goto_0

    .line 111
    :pswitch_1
    const-string/jumbo v0, "home"

    goto :goto_0

    .line 112
    :pswitch_2
    const-string/jumbo v0, "other"

    goto :goto_0

    .line 113
    :pswitch_3
    const-string/jumbo v0, "work"

    goto :goto_0

    .line 114
    :pswitch_4
    const-string/jumbo v0, "work fax"

    goto :goto_0

    .line 115
    :pswitch_5
    const-string/jumbo v0, "home fax"

    goto :goto_0

    .line 116
    :pswitch_6
    const-string/jumbo v0, "pager"

    goto :goto_0

    .line 117
    :pswitch_7
    const-string/jumbo v0, "custom"

    goto :goto_0

    .line 118
    :pswitch_8
    const-string/jumbo v0, "callback"

    goto :goto_0

    .line 109
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_2
        :pswitch_8
    .end packed-switch
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    const-string/jumbo v0, "contact-resolved"

    return-object v0
.end method

.method public getVLDialogEvent()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    .locals 21

    .prologue
    .line 52
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;->getItems()Ljava/util/List;

    move-result-object v18

    if-nez v18, :cond_0

    const/16 v18, 0x0

    .line 103
    :goto_0
    return-object v18

    .line 54
    :cond_0
    const/4 v8, 0x0

    .line 56
    .local v8, "contactCount":I
    new-instance v5, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    const-string/jumbo v18, "contact-resolved"

    move-object/from16 v0, v18

    invoke-direct {v5, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;-><init>(Ljava/lang/String;)V

    .line 57
    .local v5, "builder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;
    new-instance v11, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;

    const-string/jumbo v18, "contacts"

    move-object/from16 v0, v18

    invoke-direct {v11, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;-><init>(Ljava/lang/String;)V

    .line 58
    .local v11, "contactsBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;->getOffset()I

    move-result v10

    .line 59
    .local v10, "contactId":I
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;->getItems()Ljava/util/List;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;->getOffset()I

    move-result v19

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;->getItems()Ljava/util/List;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v20

    invoke-interface/range {v18 .. v20}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v17

    .line 60
    .local v17, "sendItems":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_1
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_5

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 61
    .local v6, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getData(Lcom/vlingo/core/internal/contacts/ContactType;)Ljava/util/List;

    move-result-object v16

    .line 62
    .local v16, "lsContactData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const/4 v13, 0x0

    .line 63
    .local v13, "hasAddressData":Z
    if-eqz v16, :cond_1

    .line 64
    new-instance v7, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;

    const-string/jumbo v18, "contact"

    move-object/from16 v0, v18

    invoke-direct {v7, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;-><init>(Ljava/lang/String;)V

    .line 66
    .local v7, "contactBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    const-string/jumbo v18, "name"

    iget-object v0, v6, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v7, v0, v1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 67
    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    .line 68
    .local v9, "contactIDStr":Ljava/lang/String;
    const-string/jumbo v18, "id"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0, v9}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 69
    add-int/lit8 v10, v10, 0x1

    .line 70
    const-string/jumbo v18, "score"

    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getScore(Z)F

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v7, v0, v1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 72
    new-instance v4, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;

    const-string/jumbo v18, "addresses"

    move-object/from16 v0, v18

    invoke-direct {v4, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;-><init>(Ljava/lang/String;)V

    .line 73
    .local v4, "addressesBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    const/4 v3, 0x0

    .line 75
    .local v3, "addressId":I
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_3

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 76
    .local v12, "data":Lcom/vlingo/core/internal/contacts/ContactData;
    new-instance v2, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;

    const-string/jumbo v18, "address"

    move-object/from16 v0, v18

    invoke-direct {v2, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;-><init>(Ljava/lang/String;)V

    .line 77
    .local v2, "addressBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;->detailed:Z

    move/from16 v18, v0

    if-eqz v18, :cond_2

    .line 78
    const-string/jumbo v18, "detail"

    iget-object v0, v12, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 80
    :cond_2
    const-string/jumbo v18, "type"

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;->getTypeString(Lcom/vlingo/core/internal/contacts/ContactData;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 81
    const-string/jumbo v18, "id"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, "."

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 82
    add-int/lit8 v3, v3, 0x1

    .line 83
    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventFieldGroup(Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 84
    const-string/jumbo v18, "use_default_phone"

    const/16 v19, 0x1

    invoke-static/range {v18 .. v19}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v18

    if-eqz v18, :cond_4

    iget v0, v12, Lcom/vlingo/core/internal/contacts/ContactData;->isDefault:I

    move/from16 v18, v0

    if-lez v18, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;->phoneType:[I

    move-object/from16 v18, v0

    if-nez v18, :cond_4

    .line 85
    new-instance v4, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;

    .end local v4    # "addressesBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    const-string/jumbo v18, "addresses"

    move-object/from16 v0, v18

    invoke-direct {v4, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;-><init>(Ljava/lang/String;)V

    .line 86
    .restart local v4    # "addressesBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventFieldGroup(Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 87
    const/4 v13, 0x1

    .line 92
    .end local v2    # "addressBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    .end local v12    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_3
    invoke-virtual {v4}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventFieldGroup(Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 93
    invoke-virtual {v7}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventFieldGroup(Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 94
    if-eqz v13, :cond_1

    .line 95
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 90
    .restart local v2    # "addressBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    .restart local v12    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_4
    const/4 v13, 0x1

    .line 91
    goto/16 :goto_2

    .line 100
    .end local v2    # "addressBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    .end local v3    # "addressId":I
    .end local v4    # "addressesBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    .end local v6    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v7    # "contactBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    .end local v9    # "contactIDStr":Ljava/lang/String;
    .end local v12    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v13    # "hasAddressData":Z
    .end local v15    # "i$":Ljava/util/Iterator;
    .end local v16    # "lsContactData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :cond_5
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;->writeTotalCount(Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;I)V

    .line 102
    invoke-virtual {v11}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->eventFieldGroup(Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 103
    invoke-virtual {v5}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;

    move-result-object v18

    goto/16 :goto_0
.end method
