.class public Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;
.super Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;
.source "SMSController.java"


# static fields
.field static final FIELD_IDS:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->FIELD_IDS:Ljava/util/HashMap;

    .line 44
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->FIELD_IDS:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;->CONTACT:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SMS_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v2}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->FIELD_IDS:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;->TYPE:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SMS_TYPE_NUM:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v2}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;-><init>()V

    return-void
.end method

.method private promptForMessage()V
    .locals 3

    .prologue
    .line 91
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->state:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    .line 93
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_speak_msg_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 94
    .local v0, "msg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SMS_MSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v2}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 95
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v1

    const-string/jumbo v2, "car-sms-message"

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 96
    return-void
.end method


# virtual methods
.method protected actionDefault()V
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerStarted()V

    .line 87
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->actionDefault()V

    .line 88
    return-void
.end method

.method public actionFail(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 73
    const-string/jumbo v1, "cancelled"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 74
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_error_sending:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "msg":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v1

    invoke-virtual {v1, v0, v0}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->errorDisplayed(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 80
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 81
    invoke-super {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->actionFail(Ljava/lang/String;)V

    .line 82
    return-void

    .line 77
    .end local v0    # "msg":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_TASK_CANCELLED:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "msg":Ljava/lang/String;
    goto :goto_0
.end method

.method public actionSuccess()V
    .locals 5

    .prologue
    .line 64
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SMS_SENT_CONFIRM_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 65
    .local v0, "msg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 66
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v1

    const-string/jumbo v2, "sms sent"

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageAction(Ljava/lang/String;Ljava/util/List;Z)V

    .line 67
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 68
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->actionSuccess()V

    .line 69
    return-void
.end method

.method protected checkHistory(Lcom/vlingo/core/internal/contacts/ContactMatch;)Z
    .locals 12
    .param p1, "item"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 160
    new-array v4, v9, [Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 161
    .local v4, "items":[Lcom/vlingo/core/internal/contacts/ContactMatch;
    aput-object p1, v4, v8

    .line 162
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v10

    invoke-interface {v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10, v4}, Lcom/vlingo/core/internal/util/SMSUtil;->findMatchingDisplayItemInInboxOutbox(Landroid/content/Context;[Lcom/vlingo/core/internal/contacts/ContactMatch;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v3

    .line 163
    .local v3, "inboxOutboxMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-eqz v3, :cond_2

    .line 166
    sget-object v10, Lcom/vlingo/core/internal/contacts/ContactType;->SMS:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {v3, v10}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getData(Lcom/vlingo/core/internal/contacts/ContactType;)Ljava/util/List;

    move-result-object v0

    .line 167
    .local v0, "cd":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v10

    if-ne v10, v9, :cond_2

    .line 169
    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/vlingo/core/internal/contacts/ContactData;

    iget-object v8, v8, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    iput-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->address:Ljava/lang/String;

    .line 171
    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->message:Ljava/lang/String;

    invoke-static {v8}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 172
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->promptForMessage()V

    .line 177
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v8

    const-string/jumbo v10, "car-sms-message"

    invoke-virtual {v8, v10}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    move v8, v9

    .line 217
    .end local v0    # "cd":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :cond_0
    :goto_1
    return v8

    .line 174
    .restart local v0    # "cd":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->promptForConfirmation()V

    goto :goto_0

    .line 181
    .end local v0    # "cd":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :cond_2
    const/4 v6, 0x0

    .line 183
    .local v6, "mobileItem":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 184
    .local v5, "matchItem":Lcom/vlingo/core/internal/contacts/ContactData;
    iget v10, v5, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_3

    .line 185
    if-nez v6, :cond_4

    .line 186
    move-object v6, v5

    goto :goto_2

    .line 189
    :cond_4
    const/4 v6, 0x0

    .line 194
    .end local v5    # "matchItem":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_5
    if-eqz v6, :cond_7

    .line 198
    iget-object v8, v6, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    iput-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->address:Ljava/lang/String;

    .line 200
    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->message:Ljava/lang/String;

    invoke-static {v8}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 201
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->promptForMessage()V

    :goto_3
    move v8, v9

    .line 206
    goto :goto_1

    .line 203
    :cond_6
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->promptForConfirmation()V

    goto :goto_3

    .line 208
    :cond_7
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v7

    .line 209
    .local v7, "phoneData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_0

    .line 210
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v10

    invoke-interface {v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    iget-object v11, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->phoneType:Ljava/lang/String;

    invoke-static {v10, v7, v11}, Lcom/vlingo/core/internal/util/DialUtil;->getPhoneTypeMap(Landroid/content/res/Resources;Ljava/util/List;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 211
    .local v1, "contactMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    iget-object v10, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->phoneType:Ljava/lang/String;

    invoke-interface {v1, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 212
    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->phoneType:Ljava/lang/String;

    invoke-interface {v1, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/vlingo/core/internal/contacts/ContactData;

    iget-object v8, v8, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    iput-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->address:Ljava/lang/String;

    .line 213
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->promptForConfirmation()V

    move v8, v9

    .line 214
    goto :goto_1
.end method

.method protected getCapability()Lcom/vlingo/core/internal/contacts/ContactType;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactType;->SMS:Lcom/vlingo/core/internal/contacts/ContactType;

    return-object v0
.end method

.method protected getContactPrompt()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_text_who:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getFieldIds()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$FieldID;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->FIELD_IDS:Ljava/util/HashMap;

    return-object v0
.end method

.method protected getNoContactPrompt()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_MATCH_DEMAND_MESSAGE:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    const-string/jumbo v0, "sms"

    return-object v0
.end method

.method protected handleNeedMessage()V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->message:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/PhoneUtil;->phoneInUse(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->promptForConfirmation()V

    .line 105
    :goto_0
    return-void

    .line 103
    :cond_1
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->promptForMessage()V

    goto :goto_0
.end method

.method protected promptForConfirmation()V
    .locals 7

    .prologue
    .line 142
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_CONFIRMATION:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->state:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    .line 144
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_send_confirm:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    .line 145
    .local v1, "msg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SMS_MSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v5}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 146
    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->message:Ljava/lang/String;

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->contactSelectedName:Ljava/lang/String;

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->address:Ljava/lang/String;

    invoke-direct {v2, v4, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    .local v2, "mt":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    new-instance v3, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->contactSelectedName:Ljava/lang/String;

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->address:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    .local v3, "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->addRecipient(Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;)V

    .line 150
    const/4 v0, 0x0

    .line 151
    .local v0, "decor":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeSendButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->cancelButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    .line 153
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ComposeMessage:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {v4, v5, v0, v2, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 154
    return-void
.end method

.method protected sendAction()V
    .locals 6

    .prologue
    .line 113
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_sending_readout:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v2

    .line 114
    .local v2, "sendingSpokenMsg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3, v2, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->message:Ljava/lang/String;

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->contactSelectedName:Ljava/lang/String;

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->address:Ljava/lang/String;

    invoke-direct {v0, v3, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    .local v0, "mt":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->contactSelectedName:Ljava/lang/String;

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->address:Ljava/lang/String;

    invoke-direct {v1, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    .local v1, "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->addRecipient(Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;)V

    .line 119
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ComposeMessage:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5, v0, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 121
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SEND_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v4, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->address:Ljava/lang/String;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;->address(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->message:Ljava/lang/String;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;->message(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;->queue()V

    .line 125
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->message:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 126
    new-instance v3, Lcom/vlingo/core/internal/recognition/acceptedtext/SMSAcceptedText;

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->message:Ljava/lang/String;

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->contactSelectedName:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lcom/vlingo/core/internal/recognition/acceptedtext/SMSAcceptedText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V

    .line 128
    :cond_0
    return-void
.end method
