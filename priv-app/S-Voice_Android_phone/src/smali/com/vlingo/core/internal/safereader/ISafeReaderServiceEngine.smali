.class public interface abstract Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;
.super Ljava/lang/Object;
.source "ISafeReaderServiceEngine.java"


# virtual methods
.method public abstract broadcastStatusUpdate()V
.end method

.method public abstract doesSupportNotifications()Z
.end method

.method public abstract getNotification()Landroid/app/Notification;
.end method

.method public abstract getNotificationId()I
.end method

.method public abstract isSafeReaderOn()Z
.end method

.method public abstract pause()V
.end method

.method public abstract registerAlertHandler(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
.end method

.method public abstract resume()V
.end method

.method public abstract safeReaderDeinit()V
.end method

.method public abstract safeReaderInit(Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;)V
.end method

.method public abstract skipCurrentlyPlayingItem()V
.end method

.method public abstract startSafeReading()V
.end method

.method public abstract stopSafeReading()V
.end method

.method public abstract unregisterAlertHandler(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
.end method

.method public abstract updateNotification()V
.end method
