.class public Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;
.super Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;
.source "DataTable.java"


# static fields
.field static final BASE_PATH:Ljava/lang/String; = "view_data"

.field public static final CLMN_ORIGINAL_ID:Ljava/lang/String; = "original_id"

.field public static COLUMN_FIELDS_MAP:Ljava/util/LinkedHashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final CONTACTS_CONTRACT_CONTENT_URI:Landroid/net/Uri;

.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/data"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/datas"

.field public static final FIELDS_MAP:Ljava/util/LinkedHashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;",
            ">;"
        }
    .end annotation
.end field

.field private static final INDEXES:[Ljava/lang/String;

.field private static final TABLE_CREATE:Ljava/lang/String;

.field public static final TABLE_NAME:Ljava/lang/String; = "data"


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 20
    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sput-object v4, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->CONTACTS_CONTRACT_CONTENT_URI:Landroid/net/Uri;

    .line 28
    new-instance v4, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;

    invoke-direct {v4}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;-><init>()V

    sput-object v4, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->FIELDS_MAP:Ljava/util/LinkedHashMap;

    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 117
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string/jumbo v4, "CREATE TABLE "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "data"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->INTEGER:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " PRIMARY KEY "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " AUTOINCREMENT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    const/4 v2, 0x0

    .line 122
    .local v2, "i":I
    sget-object v4, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->FIELDS_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 123
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    add-int/lit8 v2, v2, 0x1

    sget-object v4, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->FIELDS_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->size()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 125
    const-string/jumbo v4, ", "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 128
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;>;"
    :cond_1
    const-string/jumbo v4, ")"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->TABLE_CREATE:Ljava/lang/String;

    .line 132
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "CREATE INDEX data_mimetype_data1_index ON data (mimetype,data1)"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string/jumbo v6, "CREATE INDEX data_raw_contact_id ON data (raw_contact_id)"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string/jumbo v6, "CREATE INDEX data_version ON data (version)"

    aput-object v6, v4, v5

    sput-object v4, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->INDEXES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/contentprovider/TableBase;-><init>()V

    return-void
.end method

.method public static getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 101
    invoke-static {}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getContentUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0, "authority"    # Ljava/lang/String;

    .prologue
    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "view_data"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getVersionContentUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "get_version"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p0, "database"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 139
    sget-object v4, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->TABLE_CREATE:Ljava/lang/String;

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 140
    sget-object v0, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->INDEXES:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    .line 141
    .local v2, "index":Ljava/lang/String;
    invoke-virtual {p0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 140
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 143
    .end local v2    # "index":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public static onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3
    .param p0, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "oldVersion"    # I
    .param p2, "newVersion"    # I

    .prologue
    .line 147
    const-class v0, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Upgrading database from version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", which will destroy all old data"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    const-string/jumbo v0, "DROP TABLE IF EXISTS data"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 151
    invoke-static {p0}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 152
    return-void
.end method


# virtual methods
.method public getBasePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    const-string/jumbo v0, "view_data"

    return-object v0
.end method

.method public getBaseURI(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p1, "authority"    # Ljava/lang/String;

    .prologue
    .line 176
    invoke-static {p1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getContactsContractUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 181
    sget-object v0, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->CONTACTS_CONTRACT_CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public getFieldsMap()Ljava/util/LinkedHashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186
    sget-object v0, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->FIELDS_MAP:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method public declared-synchronized getRenamedFieldsMap()Ljava/util/LinkedHashMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 191
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->COLUMN_FIELDS_MAP:Ljava/util/LinkedHashMap;

    if-nez v2, :cond_1

    .line 192
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v2, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->COLUMN_FIELDS_MAP:Ljava/util/LinkedHashMap;

    .line 193
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->getFieldsMap()Ljava/util/LinkedHashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 194
    .local v0, "field":Ljava/lang/String;
    sget-object v2, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->COLUMN_FIELDS_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v0, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 191
    .end local v0    # "field":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 196
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    sget-object v2, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->COLUMN_FIELDS_MAP:Ljava/util/LinkedHashMap;

    const-string/jumbo v3, "original_id"

    invoke-virtual {v2, v3}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    sget-object v2, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->COLUMN_FIELDS_MAP:Ljava/util/LinkedHashMap;

    const-string/jumbo v3, "_id"

    const-string/jumbo v4, "original_id"

    invoke-virtual {v2, v3, v4}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    sget-object v2, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->COLUMN_FIELDS_MAP:Ljava/util/LinkedHashMap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v2
.end method

.method public getTable()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    const-string/jumbo v0, "data"

    return-object v0
.end method

.method public getTableID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    const-string/jumbo v0, "_id"

    return-object v0
.end method

.method public getUpdateURIs(Ljava/lang/String;)[Landroid/net/Uri;
    .locals 3
    .param p1, "authority"    # Ljava/lang/String;

    .prologue
    .line 171
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method
