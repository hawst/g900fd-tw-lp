.class public final Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;
.super Ljava/lang/Object;
.source "UserLoggingEngine.java"

# interfaces
.implements Lcom/vlingo/core/facade/logging/IUserLoggingEngine;


# static fields
.field public static final ACTION_ALT_PHRASE_SELECTED:I = 0x65

.field public static final ACTION_CONTACT_CHANGE:I = 0x66

.field public static final ACTION_NONE:I = 0x0

.field public static final ACTION_NOTE_CHANGE:I = 0x68

.field public static final ACTION_NUMBER_CHANGE:I = 0x67

.field public static final ACTION_UNDO:I = 0x69

.field public static final ERROR_LOGGING_ENABLED:Z = true

.field public static final FIELD_LOGGING_ENABLED:Z = true

.field public static final HELP_LOGGING_ENABLED:Z = true

.field public static final LANDING_PAGE_LOGGING_ENABLED:Z = true

.field private static final RECORD_TRANSMIT_THRESHOLD:I = 0x32

.field public static final SETTINGS_LOGGING_ENABLED:Z = true

.field public static final TIMING_LOGGING_ENABLED:Z = true

.field public static final USERLOGGING_ENGINE_ENABLED:Z = true

.field private static smInstance:Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;


# instance fields
.field private mErrorRecordBuilders:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;",
            ">;"
        }
    .end annotation
.end field

.field private mHelpPageRecordBuilders:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;",
            ">;"
        }
    .end annotation
.end field

.field private mLandingPageRecordBuilders:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;",
            ">;"
        }
    .end annotation
.end field

.field private mLogRecordBuilder:Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

.field private mStartTime:J


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    new-instance v0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    invoke-direct {v0}, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mLogRecordBuilder:Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    .line 88
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mLandingPageRecordBuilders:Ljava/util/HashMap;

    .line 89
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mHelpPageRecordBuilders:Ljava/util/HashMap;

    .line 90
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mErrorRecordBuilders:Ljava/util/HashMap;

    .line 96
    return-void
.end method

.method private getErrorRecordBuilder(Ljava/lang/String;)Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;
    .locals 2
    .param p1, "pageId"    # Ljava/lang/String;

    .prologue
    .line 298
    iget-object v1, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mErrorRecordBuilders:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;

    .line 299
    .local v0, "builder":Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;
    if-nez v0, :cond_0

    .line 300
    new-instance v0, Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;

    .end local v0    # "builder":Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;
    invoke-direct {v0, p1}, Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;-><init>(Ljava/lang/String;)V

    .line 301
    .restart local v0    # "builder":Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;
    iget-object v1, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mErrorRecordBuilders:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    :cond_0
    return-object v0
.end method

.method private getHelpPageRecordBuilder(Ljava/lang/String;)Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;
    .locals 2
    .param p1, "pageId"    # Ljava/lang/String;

    .prologue
    .line 289
    iget-object v1, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mHelpPageRecordBuilders:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;

    .line 290
    .local v0, "builder":Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;
    if-nez v0, :cond_0

    .line 291
    new-instance v0, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;

    .end local v0    # "builder":Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;
    invoke-direct {v0, p1}, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;-><init>(Ljava/lang/String;)V

    .line 292
    .restart local v0    # "builder":Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;
    iget-object v1, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mHelpPageRecordBuilders:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    :cond_0
    return-object v0
.end method

.method public static getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->smInstance:Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    invoke-direct {v0}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->smInstance:Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    .line 81
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->smInstance:Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    return-object v0
.end method

.method private getLandingPageRecordBuilder(Ljava/lang/String;)Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    .locals 2
    .param p1, "pageId"    # Ljava/lang/String;

    .prologue
    .line 307
    iget-object v1, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mLandingPageRecordBuilders:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .line 308
    .local v0, "builder":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    if-nez v0, :cond_0

    .line 309
    new-instance v0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .end local v0    # "builder":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;-><init>(Ljava/lang/String;Z)V

    .line 310
    .restart local v0    # "builder":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    iget-object v1, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mLandingPageRecordBuilders:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    :cond_0
    return-object v0
.end method

.method private getRecordSize()I
    .locals 9

    .prologue
    .line 327
    iget-object v3, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mHelpPageRecordBuilders:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    iget-object v4, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mErrorRecordBuilders:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    add-int/2addr v3, v4

    const-wide/high16 v4, 0x4012000000000000L    # 4.5

    iget-object v6, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mLandingPageRecordBuilders:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    int-to-double v6, v6

    mul-double/2addr v4, v6

    double-to-int v4, v4

    add-int v2, v3, v4

    .line 328
    .local v2, "size":I
    iget-object v3, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mLandingPageRecordBuilders:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .line 329
    .local v1, "lpBuilder":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    int-to-double v3, v2

    const-wide/high16 v5, 0x400c000000000000L    # 3.5

    invoke-virtual {v1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->getFieldCount()I

    move-result v7

    int-to-double v7, v7

    mul-double/2addr v5, v7

    add-double/2addr v3, v5

    double-to-int v2, v3

    goto :goto_0

    .line 331
    .end local v1    # "lpBuilder":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    :cond_0
    return v2
.end method

.method private resetBuilders()V
    .locals 1

    .prologue
    .line 316
    new-instance v0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    invoke-direct {v0}, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mLogRecordBuilder:Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    .line 317
    iget-object v0, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mLandingPageRecordBuilders:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 318
    iget-object v0, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mHelpPageRecordBuilders:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 319
    iget-object v0, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mErrorRecordBuilders:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 320
    return-void
.end method

.method private declared-synchronized transmitIfNecessary()V
    .locals 5

    .prologue
    .line 268
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 269
    .local v1, "context":Landroid/content/Context;
    const/4 v0, 0x0

    .line 270
    .local v0, "cn":Landroid/content/ComponentName;
    if-eqz v1, :cond_0

    .line 271
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/vlingo/core/internal/userlogging/UALService;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 272
    .local v2, "intent":Landroid/content/Intent;
    invoke-direct {p0}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getRecordSize()I

    move-result v3

    const/16 v4, 0x32

    if-le v3, v4, :cond_1

    .line 275
    const-string/jumbo v3, "com.vlingo.client.userlogging.skipInitialDelay"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 276
    invoke-virtual {v1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 281
    :goto_0
    if-nez v0, :cond_0

    .line 286
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_0
    monitor-exit p0

    return-void

    .line 279
    .restart local v2    # "intent":Landroid/content/Intent;
    :cond_1
    :try_start_1
    invoke-virtual {v1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 268
    .end local v0    # "cn":Landroid/content/ComponentName;
    .end local v1    # "context":Landroid/content/Context;
    .end local v2    # "intent":Landroid/content/Intent;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method


# virtual methods
.method public declared-synchronized errorDisplayed(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "errorCode"    # Ljava/lang/String;
    .param p2, "errorMsg"    # Ljava/lang/String;

    .prologue
    .line 143
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 145
    .local v1, "errorId":Ljava/lang/String;
    const-string/jumbo v2, "REC106"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "REC107"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_1

    .line 148
    :cond_0
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ";siglev="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getInstance()Lcom/vlingo/sdk/internal/net/ConnectionManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getGsmSignal()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 149
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ";contype="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getInstance()Lcom/vlingo/sdk/internal/net/ConnectionManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getCurrentConnectionType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 150
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ";wifi="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getInstance()Lcom/vlingo/sdk/internal/net/ConnectionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_2

    const-string/jumbo v2, "true"

    :goto_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 151
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ";netsvc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getInstance()Lcom/vlingo/sdk/internal/net/ConnectionManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getNetworkTypeName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 152
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ";nettype="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getInstance()Lcom/vlingo/sdk/internal/net/ConnectionManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getCurrentConnectionType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 155
    :cond_1
    :goto_1
    :try_start_2
    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getErrorRecordBuilder(Ljava/lang/String;)Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;

    move-result-object v0

    .line 156
    .local v0, "builder":Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;
    invoke-virtual {v0}, Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;->errorDisplayed()Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;

    .line 158
    invoke-direct {p0}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->transmitIfNecessary()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 160
    monitor-exit p0

    return-void

    .line 150
    .end local v0    # "builder":Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;
    :cond_2
    :try_start_3
    const-string/jumbo v2, "false"
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 143
    .end local v1    # "errorId":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 153
    .restart local v1    # "errorId":Ljava/lang/String;
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method declared-synchronized flushUserLogRecord()Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;
    .locals 8

    .prologue
    .line 239
    monitor-enter p0

    const/4 v5, 0x0

    .line 241
    .local v5, "setupFinished":Z
    :try_start_0
    iget-object v6, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mLogRecordBuilder:Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    invoke-virtual {v6, v5}, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->setupStarted(Z)Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    .line 242
    iget-object v6, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mLogRecordBuilder:Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    invoke-virtual {v6, v5}, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->setupFinished(Z)Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    .line 244
    iget-object v6, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mErrorRecordBuilders:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;

    .line 245
    .local v0, "erb":Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;
    iget-object v6, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mLogRecordBuilder:Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    invoke-virtual {v0}, Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;->build()Lcom/vlingo/sdk/services/userlogging/VLErrorRecord;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->addErrorRecord(Lcom/vlingo/sdk/services/userlogging/VLErrorRecord;)Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 239
    .end local v0    # "erb":Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 248
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    iget-object v6, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mHelpPageRecordBuilders:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;

    .line 249
    .local v1, "hprb":Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;
    iget-object v6, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mLogRecordBuilder:Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    invoke-virtual {v1}, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;->build()Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->addHelpPageRecord(Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;)Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    goto :goto_1

    .line 252
    .end local v1    # "hprb":Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;
    :cond_1
    iget-object v6, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mLandingPageRecordBuilders:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .line 253
    .local v4, "lprb":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    iget-object v6, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mLogRecordBuilder:Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    invoke-virtual {v4}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->build()Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->addLandingPageRecord(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;)Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    goto :goto_2

    .line 255
    .end local v4    # "lprb":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    :cond_2
    iget-object v6, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mLogRecordBuilder:Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    invoke-virtual {v6}, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->build()Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;

    move-result-object v3

    .line 257
    .local v3, "logRecord":Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;
    invoke-direct {p0}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->resetBuilders()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 259
    monitor-exit p0

    return-object v3
.end method

.method public declared-synchronized helpPageViewed(Ljava/lang/String;)V
    .locals 3
    .param p1, "pageId"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x80

    .line 124
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v2, :cond_0

    .line 125
    const/4 v1, 0x0

    const/16 v2, 0x80

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 128
    :cond_0
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getHelpPageRecordBuilder(Ljava/lang/String;)Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;

    move-result-object v0

    .line 129
    .local v0, "builder":Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;
    invoke-virtual {v0}, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;->pageViewed()Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;

    .line 131
    invoke-direct {p0}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->transmitIfNecessary()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    monitor-exit p0

    return-void

    .line 124
    .end local v0    # "builder":Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized landingPageAction(Ljava/lang/String;Ljava/util/List;Z)V
    .locals 3
    .param p1, "pageId"    # Ljava/lang/String;
    .param p3, "autoAction"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 183
    .local p2, "textFieldData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;>;"
    monitor-enter p0

    if-eqz p3, :cond_0

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "AutoAction:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .end local p1    # "pageId":Ljava/lang/String;
    :cond_0
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getLandingPageRecordBuilder(Ljava/lang/String;)Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    move-result-object v0

    .line 184
    .local v0, "builder":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    invoke-virtual {v0, p2}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->actionClicked(Ljava/util/List;)Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .line 186
    invoke-direct {p0}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->transmitIfNecessary()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    monitor-exit p0

    return-void

    .line 183
    .end local v0    # "builder":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized landingPageActionEvent(Ljava/lang/String;I)V
    .locals 2
    .param p1, "pageId"    # Ljava/lang/String;
    .param p2, "actionType"    # I

    .prologue
    .line 195
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getLandingPageRecordBuilder(Ljava/lang/String;)Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    move-result-object v0

    .line 197
    .local v0, "builder":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    packed-switch p2, :pswitch_data_0

    .line 215
    :goto_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->transmitIfNecessary()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 217
    monitor-exit p0

    return-void

    .line 199
    :pswitch_0
    :try_start_1
    invoke-virtual {v0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->alterPhrasePicked()Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 195
    .end local v0    # "builder":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 202
    .restart local v0    # "builder":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    :pswitch_1
    :try_start_2
    invoke-virtual {v0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->incrContactChange()Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    goto :goto_0

    .line 205
    :pswitch_2
    invoke-virtual {v0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->incrPhoneChange()Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    goto :goto_0

    .line 208
    :pswitch_3
    invoke-virtual {v0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->incrNoteChanged()Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    goto :goto_0

    .line 211
    :pswitch_4
    invoke-virtual {v0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->incrUndoCount()Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 197
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public declared-synchronized landingPageCanceled(Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .param p1, "pageId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 224
    .local p2, "textFieldData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;>;"
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getLandingPageRecordBuilder(Ljava/lang/String;)Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    move-result-object v0

    .line 225
    .local v0, "builder":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    invoke-virtual {v0, p2}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->backClicked(Ljava/util/List;)Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .line 227
    invoke-direct {p0}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->transmitIfNecessary()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229
    monitor-exit p0

    return-void

    .line 224
    .end local v0    # "builder":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized landingPageViewed(Ljava/lang/String;)V
    .locals 7
    .param p1, "pageId"    # Ljava/lang/String;

    .prologue
    .line 168
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getLandingPageRecordBuilder(Ljava/lang/String;)Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    move-result-object v0

    .line 169
    .local v0, "builder":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    invoke-virtual {v0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->pageViewed()Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .line 170
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mStartTime:J

    sub-long v1, v3, v5

    .line 171
    .local v1, "launchTime":J
    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->addLaunchTime(J)Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .line 173
    invoke-direct {p0}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->transmitIfNecessary()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    monitor-exit p0

    return-void

    .line 168
    .end local v0    # "builder":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    .end local v1    # "launchTime":J
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized settingsChanged()V
    .locals 2

    .prologue
    .line 113
    monitor-enter p0

    :try_start_0
    const-string/jumbo v0, ""

    .line 114
    .local v0, "settings":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mLogRecordBuilder:Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    invoke-virtual {v1, v0}, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->settings(Ljava/lang/String;)Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    .line 115
    invoke-direct {p0}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->transmitIfNecessary()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    monitor-exit p0

    return-void

    .line 113
    .end local v0    # "settings":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized timeApplicationStart()V
    .locals 2

    .prologue
    .line 101
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->mStartTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    monitor-exit p0

    return-void

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
