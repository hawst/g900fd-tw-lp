.class public final Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;
.super Ljava/lang/Object;
.source "SafeReaderBackgroundHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;
.implements Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;


# static fields
.field private static final FOCUS_DEFAULT_BACKGROUND:I = 0x0

.field private static final FOCUS_FOREGROUND:I = 0x1

.field private static isInProcess:Z

.field private static mFocus:I

.field private static mInstance:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;


# instance fields
.field private alerts:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;"
        }
    .end annotation
.end field

.field private isSilentMode:Z

.field private listener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    sput-boolean v0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->isInProcess:Z

    .line 38
    sput v0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->mFocus:I

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->isSilentMode:Z

    .line 55
    iput-object p0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->listener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .line 56
    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->listener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    invoke-static {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->registerSafeReaderListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 57
    return-void
.end method

.method public static getInstance()Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->mInstance:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;

    invoke-direct {v0}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->mInstance:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;

    .line 49
    const/4 v0, 0x0

    sput v0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->mFocus:I

    .line 51
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->mInstance:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;

    return-object v0
.end method

.method private readAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)V
    .locals 11
    .param p1, "alert"    # Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .prologue
    const/4 v10, 0x1

    .line 168
    sput-boolean v10, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->isInProcess:Z

    .line 172
    invoke-virtual {p1}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getType()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "MMS"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {p1}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getType()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "SMS"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    move-object v1, p1

    .line 173
    check-cast v1, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 174
    .local v1, "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v5

    .line 175
    .local v5, "sender":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 176
    .local v0, "address":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getMessageText()Ljava/lang/String;

    move-result-object v3

    .line 178
    .local v3, "msgTxt":Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    move-object v2, v0

    .line 179
    .local v2, "messageSender":Ljava/lang/String;
    :goto_0
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 180
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_FROM_BODY_NO_PROMPT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v2, v8, v9

    aput-object v3, v8, v10

    invoke-interface {v6, v7, v8}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 181
    .local v4, "prompt":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/core/internal/audio/TTSRequest;->getMessageReadback(Ljava/lang/String;)Lcom/vlingo/core/internal/audio/TTSRequest;

    move-result-object v6

    invoke-static {v6, p0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->play(Lcom/vlingo/core/internal/audio/TTSRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 186
    .end local v0    # "address":Ljava/lang/String;
    .end local v1    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v2    # "messageSender":Ljava/lang/String;
    .end local v3    # "msgTxt":Ljava/lang/String;
    .end local v4    # "prompt":Ljava/lang/String;
    .end local v5    # "sender":Ljava/lang/String;
    :cond_1
    return-void

    .restart local v0    # "address":Ljava/lang/String;
    .restart local v1    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .restart local v3    # "msgTxt":Ljava/lang/String;
    .restart local v5    # "sender":Ljava/lang/String;
    :cond_2
    move-object v2, v5

    .line 178
    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized getForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p1, "foregroundListener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 66
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    sput v0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->mFocus:I

    .line 67
    invoke-static {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->unregisterSafeReaderListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 68
    invoke-static {p1}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->registerSafeReaderListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 69
    iput-object p1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->listener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    monitor-exit p0

    return-void

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public handleAlert(Ljava/util/LinkedList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 108
    .local p1, "alertList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<+Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    const/4 v1, 0x0

    .line 109
    .local v1, "focusIsForeground":Z
    monitor-enter p0

    .line 110
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->isFocusForeground()Z

    move-result v1

    .line 111
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    if-eqz v1, :cond_0

    .line 125
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->isSilentMode()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 163
    :cond_1
    :goto_0
    return-void

    .line 111
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 131
    :cond_2
    if-eqz p1, :cond_1

    .line 135
    iget-object v3, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->alerts:Ljava/util/Queue;

    if-nez v3, :cond_3

    .line 139
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    iput-object v3, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->alerts:Ljava/util/Queue;

    .line 142
    :cond_3
    invoke-virtual {p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 143
    .local v0, "alert":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    iget-object v3, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->alerts:Ljava/util/Queue;

    invoke-interface {v3, v0}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 146
    iget-object v3, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->alerts:Ljava/util/Queue;

    invoke-interface {v3, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 150
    .end local v0    # "alert":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    :cond_5
    const/4 v0, 0x0

    .line 151
    .restart local v0    # "alert":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/core/internal/util/PhoneUtil;->phoneInUse(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-boolean v3, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->isInProcess:Z

    if-nez v3, :cond_1

    .line 154
    iget-object v3, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->alerts:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "alert":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 155
    .restart local v0    # "alert":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    if-eqz v0, :cond_1

    .line 156
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->readAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)V

    goto :goto_0
.end method

.method public handleAlert(Ljava/util/LinkedList;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    .locals 0
    .param p2, "fieldId"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;",
            ")V"
        }
    .end annotation

    .prologue
    .line 244
    .local p1, "alertList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<+Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->handleAlert(Ljava/util/LinkedList;)V

    .line 245
    return-void
.end method

.method public isFocusForeground()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 80
    sget v1, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->mFocus:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSilentMode()Z
    .locals 1

    .prologue
    .line 232
    iget-boolean v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->isSilentMode:Z

    return v0
.end method

.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 221
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->isInProcess:Z

    .line 222
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 223
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 198
    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->alerts:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->alerts:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->readAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)V

    .line 205
    :goto_0
    return-void

    .line 202
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->isInProcess:Z

    .line 203
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    goto :goto_0
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 212
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->isInProcess:Z

    .line 213
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 214
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 192
    return-void
.end method

.method public readoutDelay()J
    .locals 3

    .prologue
    .line 227
    const-string/jumbo v0, "safereader.delay"

    const-wide/16 v1, 0x0

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->listener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    invoke-static {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->unregisterSafeReaderListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 61
    iput-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->listener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .line 62
    sput-object v1, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->mInstance:Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;

    .line 63
    return-void
.end method

.method public declared-synchronized releaseForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p1, "foregroundListener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 73
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    sput v0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->mFocus:I

    .line 74
    invoke-static {p1}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->unregisterSafeReaderListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 75
    invoke-static {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->registerSafeReaderListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 76
    iput-object p0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->listener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    monitor-exit p0

    return-void

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setSilentMode(Z)V
    .locals 0
    .param p1, "silentMode"    # Z

    .prologue
    .line 237
    iput-boolean p1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderBackgroundHandler;->isSilentMode:Z

    .line 238
    return-void
.end method
