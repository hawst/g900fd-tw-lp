.class public Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;
.super Landroid/app/Service;
.source "VoconLmttService.java"


# static fields
.field public static final ACTION_APP_ACTIVITY:Ljava/lang/String; = "com.vlingo.lmttvocon.action.APP_ACTIVITY"

.field public static final ACTION_SEND_UPDATE:Ljava/lang/String; = "com.vlingo.lmttvocon.action.SEND_UPDATE"

.field private static final DEFAULT_INACTIVITY_SHUTDOWN_TIMEOUT_MINS:J = 0x2760L

.field public static final EXTRA_BOOT_START:Ljava/lang/String; = "com.vlingo.lmttvocon.extra.BOOT_START"

.field public static final EXTRA_CLEAR_LMTT:Ljava/lang/String; = "com.vlingo.lmttvocon.extra.CLEAR_LMTT"

.field public static final EXTRA_LMTT_TYPE:Ljava/lang/String; = "com.vlingo.lmttvocon.extra.LMTT_TYPE"

.field private static final MS_PER_MINUTE:J = 0xea60L

.field private static final SUPPORTED_UPLOAD_TYPES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/core/internal/lmttvocon/UploadType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private languageChangeReceiver:Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;

.field private manager:Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;

.field private pendingInactivityShutdown:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 32
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x7

    new-array v1, v1, [Lcom/vlingo/core/internal/lmttvocon/UploadType;

    const/4 v2, 0x0

    sget-object v3, Lcom/vlingo/core/internal/lmttvocon/UploadType;->CONTACT:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/vlingo/core/internal/lmttvocon/UploadType;->SONG:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lcom/vlingo/core/internal/lmttvocon/UploadType;->PLAYLIST:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, Lcom/vlingo/core/internal/lmttvocon/UploadType;->ARTIST:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, Lcom/vlingo/core/internal/lmttvocon/UploadType;->ALBUM:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    sget-object v3, Lcom/vlingo/core/internal/lmttvocon/UploadType;->APPLICATION:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/vlingo/core/internal/lmttvocon/UploadType;->APPLICATION:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->SUPPORTED_UPLOAD_TYPES:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 35
    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->manager:Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;

    .line 36
    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->pendingInactivityShutdown:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;

    .line 37
    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->languageChangeReceiver:Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;

    return-void
.end method

.method private checkForInactivity()V
    .locals 5

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->getTimeoutBeforeStopServiceDueToInactivity()J

    move-result-wide v0

    .line 172
    .local v0, "timeoutBeforeStopServiceDueToInactivity":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 175
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->stopSelf()V

    .line 177
    :cond_0
    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->pendingInactivityShutdown:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;

    if-nez v2, :cond_1

    .line 180
    new-instance v2, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService$2;

    invoke-direct {v4, p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService$2;-><init>(Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;)V

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;-><init>(Landroid/os/Handler;Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback$Callback;J)V

    iput-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->pendingInactivityShutdown:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;

    .line 192
    :cond_1
    return-void
.end method

.method private getTimeoutBeforeStopServiceDueToInactivity()J
    .locals 13

    .prologue
    const-wide/16 v8, 0x0

    .line 195
    const-string/jumbo v10, "lmttvocon.last_app_activity_time"

    invoke-static {v10, v8, v9}, Lcom/vlingo/core/internal/settings/Settings;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 196
    .local v4, "lastAppActivityTime":J
    const-string/jumbo v10, "lmttvocon.no_activity_shutdown_timeout"

    const-wide/16 v11, 0x2760

    invoke-static {v10, v11, v12}, Lcom/vlingo/core/internal/settings/Settings;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 197
    .local v0, "inactivityShutdownTimeoutMins":J
    const-wide/32 v10, 0xea60

    mul-long v2, v0, v10

    .line 198
    .local v2, "inactivityShutdownTimeoutMs":J
    cmp-long v10, v4, v8

    if-nez v10, :cond_0

    .line 202
    .end local v2    # "inactivityShutdownTimeoutMs":J
    :goto_0
    return-wide v2

    .line 201
    .restart local v2    # "inactivityShutdownTimeoutMs":J
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v4

    sub-long v6, v2, v10

    .line 202
    .local v6, "result":J
    cmp-long v10, v6, v8

    if-lez v10, :cond_1

    .end local v6    # "result":J
    :goto_1
    move-wide v2, v6

    goto :goto_0

    .restart local v6    # "result":J
    :cond_1
    move-wide v6, v8

    goto :goto_1
.end method

.method private handleAction(Landroid/content/Intent;)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 99
    if-eqz p1, :cond_2

    .line 100
    const-string/jumbo v0, "com.vlingo.lmttvocon.extra.BOOT_START"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    :cond_0
    const-string/jumbo v0, "com.vlingo.lmttvocon.action.SEND_UPDATE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->handleSendUpdateAction(Landroid/content/Intent;)I

    move-result v0

    .line 110
    :goto_0
    return v0

    .line 106
    :cond_1
    const-string/jumbo v0, "com.vlingo.lmttvocon.action.APP_ACTIVITY"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 107
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->handleAppActivityAction(Landroid/content/Intent;)I

    move-result v0

    goto :goto_0

    .line 110
    :cond_2
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->handleUnknownAction(Landroid/content/Intent;)I

    move-result v0

    goto :goto_0
.end method

.method private handleAppActivityAction(Landroid/content/Intent;)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 146
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->pendingInactivityShutdown:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->pendingInactivityShutdown:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->reset()V

    .line 149
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->updateLastAppActivityTime()V

    .line 150
    const-string/jumbo v0, "lmttvocon.force_start_from_scratch"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 151
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->manager:Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;

    if-eqz v0, :cond_1

    .line 154
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->manager:Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->uploadAll(Z)V

    .line 155
    const-string/jumbo v0, "lmttvocon.force_start_from_scratch"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 158
    :cond_1
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->manager:Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;

    if-eqz v0, :cond_2

    .line 159
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->manager:Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->resetUploadDelays()V

    .line 161
    :cond_2
    return v2
.end method

.method private handleSendUpdateAction(Landroid/content/Intent;)I
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 114
    const-string/jumbo v3, "com.vlingo.lmttvocon.extra.CLEAR_LMTT"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 115
    .local v0, "startFromScratch":Z
    const-string/jumbo v3, "com.vlingo.lmttvocon.extra.LMTT_TYPE"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 116
    .local v2, "uploadTypeString":Ljava/lang/String;
    const/4 v1, 0x0

    .line 117
    .local v1, "uploadType":Lcom/vlingo/core/internal/lmttvocon/UploadType;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 119
    :try_start_0
    invoke-static {v2}, Lcom/vlingo/core/internal/lmttvocon/UploadType;->valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/lmttvocon/UploadType;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 125
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->manager:Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;

    if-eqz v3, :cond_1

    .line 126
    if-eqz v1, :cond_2

    .line 129
    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->manager:Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;

    invoke-virtual {v3, v1, v0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->upload(Lcom/vlingo/core/internal/lmttvocon/UploadType;Z)V

    .line 135
    :goto_1
    const-string/jumbo v3, "lmttvocon.force_start_from_scratch"

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 140
    :cond_1
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->handleAppActivityAction(Landroid/content/Intent;)I

    move-result v3

    return v3

    .line 133
    :cond_2
    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->manager:Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;

    invoke-virtual {v3, v0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->uploadAll(Z)V

    goto :goto_1

    .line 120
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method private handleUnknownAction(Landroid/content/Intent;)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 167
    const/4 v0, 0x1

    return v0
.end method

.method private updateLastAppActivityTime()V
    .locals 3

    .prologue
    .line 207
    const-string/jumbo v0, "lmttvocon.last_app_activity_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Ljava/lang/String;J)V

    .line 208
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 95
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 42
    invoke-static {}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttServiceHelper;->isVoconLmttEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    new-instance v0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;

    sget-object v1, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->SUPPORTED_UPLOAD_TYPES:Ljava/util/Set;

    invoke-direct {v0, p0, v1}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;-><init>(Landroid/content/Context;Ljava/util/Set;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->manager:Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;

    .line 46
    new-instance v0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService$1;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService$1;-><init>(Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->languageChangeReceiver:Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;

    .line 54
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->languageChangeReceiver:Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;

    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;->register(Landroid/content/Context;)V

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->stopSelf()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 79
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->pendingInactivityShutdown:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->pendingInactivityShutdown:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;->cancel()V

    .line 81
    iput-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->pendingInactivityShutdown:Lcom/vlingo/core/internal/lmttvocon/util/PendingCallback;

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->languageChangeReceiver:Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;

    if-eqz v0, :cond_1

    .line 84
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->languageChangeReceiver:Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;

    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;->unregister(Landroid/content/Context;)V

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->manager:Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;

    if-eqz v0, :cond_2

    .line 87
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->manager:Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->release()V

    .line 88
    iput-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->manager:Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;

    .line 90
    :cond_2
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 91
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 64
    invoke-static {}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttServiceHelper;->isVoconLmttEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->stopSelf()V

    .line 68
    const/4 v0, 0x1

    .line 72
    :goto_0
    return v0

    .line 70
    :cond_0
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->handleAction(Landroid/content/Intent;)I

    move-result v0

    .line 71
    .local v0, "result":I
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttService;->checkForInactivity()V

    goto :goto_0
.end method
