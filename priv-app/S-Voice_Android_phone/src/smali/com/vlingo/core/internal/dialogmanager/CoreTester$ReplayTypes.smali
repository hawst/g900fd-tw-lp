.class final enum Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;
.super Ljava/lang/Enum;
.source "CoreTester.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/CoreTester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "ReplayTypes"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

.field public static final enum DELAY:Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

.field public static final enum INTENT:Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

.field public static final enum QUIT:Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

.field public static final enum WIDGET_ACTIVATED:Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

.field public static final enum XML_RESPONSE:Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 42
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

    const-string/jumbo v1, "XML_RESPONSE"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;->XML_RESPONSE:Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

    .line 43
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

    const-string/jumbo v1, "INTENT"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;->INTENT:Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

    .line 44
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

    const-string/jumbo v1, "DELAY"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;->DELAY:Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

    .line 45
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

    const-string/jumbo v1, "WIDGET_ACTIVATED"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;->WIDGET_ACTIVATED:Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

    .line 46
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

    const-string/jumbo v1, "QUIT"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;->QUIT:Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

    .line 41
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;->XML_RESPONSE:Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;->INTENT:Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;->DELAY:Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;->WIDGET_ACTIVATED:Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;->QUIT:Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 41
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;

    return-object v0
.end method
