.class public Lcom/vlingo/core/internal/audio/MTAudioPlaybackListener;
.super Ljava/lang/Object;
.source "MTAudioPlaybackListener.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# instance fields
.field final listener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/MTAudioPlaybackListener;->listener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .line 15
    return-void
.end method


# virtual methods
.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 18
    new-instance v0, Lcom/vlingo/core/internal/audio/MTAudioPlaybackListener$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/vlingo/core/internal/audio/MTAudioPlaybackListener$1;-><init>(Lcom/vlingo/core/internal/audio/MTAudioPlaybackListener;Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 23
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 26
    new-instance v0, Lcom/vlingo/core/internal/audio/MTAudioPlaybackListener$2;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/audio/MTAudioPlaybackListener$2;-><init>(Lcom/vlingo/core/internal/audio/MTAudioPlaybackListener;Lcom/vlingo/core/internal/audio/AudioRequest;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 31
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 34
    new-instance v0, Lcom/vlingo/core/internal/audio/MTAudioPlaybackListener$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/vlingo/core/internal/audio/MTAudioPlaybackListener$3;-><init>(Lcom/vlingo/core/internal/audio/MTAudioPlaybackListener;Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 39
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 42
    new-instance v0, Lcom/vlingo/core/internal/audio/MTAudioPlaybackListener$4;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/audio/MTAudioPlaybackListener$4;-><init>(Lcom/vlingo/core/internal/audio/MTAudioPlaybackListener;Lcom/vlingo/core/internal/audio/AudioRequest;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 47
    return-void
.end method
