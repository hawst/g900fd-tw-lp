.class Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;
.super Ljava/lang/Object;
.source "WeatherLookupHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->showSuccess()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 28

    .prologue
    .line 147
    const/16 v20, 0x0

    .line 148
    .local v20, "msg":Ljava/lang/String;
    const/16 v22, 0x0

    .line 149
    .local v22, "spokenMsg":Ljava/lang/String;
    const/16 v19, 0x0

    .line 150
    .local v19, "isDatePlusSeven":Z
    const/16 v23, 0x0

    .line 151
    .local v23, "today":Z
    const/16 v24, 0x0

    .line 152
    .local v24, "tomorrow":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getDate()Ljava/lang/String;

    move-result-object v11

    .line 153
    .local v11, "dateStr":Ljava/lang/String;
    new-instance v15, Ljava/util/Date;

    invoke-direct {v15}, Ljava/util/Date;-><init>()V

    .line 154
    .local v15, "currDate":Ljava/util/Date;
    new-instance v18, Ljava/text/SimpleDateFormat;

    sget-object v2, Lcom/vlingo/core/internal/schedule/DateUtil;->CANONICAL_DATE_FORMAT:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 155
    .local v18, "format":Ljava/text/SimpleDateFormat;
    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    .line 156
    .local v6, "todaysDate":Ljava/lang/String;
    new-instance v25, Ljava/util/Date;

    invoke-direct/range {v25 .. v25}, Ljava/util/Date;-><init>()V

    .line 157
    .local v25, "tomorrowDate":Ljava/util/Date;
    new-instance v14, Ljava/util/GregorianCalendar;

    invoke-direct {v14}, Ljava/util/GregorianCalendar;-><init>()V

    .line 158
    .local v14, "calendar":Ljava/util/GregorianCalendar;
    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V

    .line 159
    const/4 v2, 0x5

    const/4 v7, 0x1

    invoke-virtual {v14, v2, v7}, Ljava/util/GregorianCalendar;->add(II)V

    .line 160
    invoke-virtual {v14}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v26

    invoke-virtual/range {v25 .. v27}, Ljava/util/Date;->setTime(J)V

    .line 161
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->isDefaultLocation()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 162
    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v2, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$100(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    .line 163
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    move-object/from16 v0, v20

    move-object/from16 v1, v20

    invoke-interface {v2, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->actionSuccess()V

    .line 236
    :goto_0
    return-void

    .line 168
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$300(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;
    invoke-static {v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getWeatherElement()Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v7

    invoke-static {v2, v7, v11}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getWeatherInfoUtil(Landroid/content/Context;Lcom/vlingo/core/internal/weather/WeatherElement;Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    move-result-object v4

    .line 170
    .local v4, "weatherInfoUtil":Lcom/vlingo/core/internal/weather/WeatherInfoUtil;
    invoke-static {v11}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 172
    :try_start_0
    invoke-static {v11}, Lcom/vlingo/core/internal/schedule/DateUtil;->getDateFromCanonicalString(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v10

    .line 173
    .local v10, "date":Ljava/util/Date;
    invoke-static {v15, v10}, Lcom/vlingo/core/internal/schedule/DateUtil;->isSameOrFurtherDate(Ljava/util/Date;Ljava/util/Date;)Z

    move-result v23

    .line 174
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    move-result-object v2

    move/from16 v0, v23

    invoke-virtual {v2, v0}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->setToday(Z)V

    .line 175
    invoke-virtual/range {v25 .. v25}, Ljava/util/Date;->getDate()I

    move-result v2

    invoke-virtual {v10}, Ljava/util/Date;->getDate()I

    move-result v7

    if-ne v2, v7, :cond_5

    const/16 v24, 0x1

    .line 176
    :goto_1
    invoke-virtual {v4}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getWeatherDate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v15}, Ljava/util/Date;->getDate()I

    move-result v2

    invoke-virtual {v10}, Ljava/util/Date;->getDate()I
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-eq v2, v7, :cond_6

    const/16 v19, 0x1

    .line 182
    .end local v10    # "date":Ljava/util/Date;
    :cond_1
    :goto_2
    invoke-static {v11}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 184
    :try_start_1
    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_today:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v2, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$500(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 185
    .local v5, "todayStr":Ljava/lang/String;
    const/4 v3, 0x0

    .line 186
    .local v3, "strContextToday":Ljava/lang/Integer;
    const/16 v21, 0x0

    .line 187
    .local v21, "oneDaySpokenMsg":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 188
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;->TODAY:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getOneDaySpokenMsgInternal(Ljava/lang/Integer;Lcom/vlingo/core/internal/weather/WeatherInfoUtil;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;)Ljava/lang/String;
    invoke-static/range {v2 .. v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$600(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;Ljava/lang/Integer;Lcom/vlingo/core/internal/weather/WeatherInfoUtil;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;)Ljava/lang/String;

    move-result-object v21

    .line 189
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_with_locatin:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/16 v26, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    move-object/from16 v27, v0

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;
    invoke-static/range {v27 .. v27}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getLocationCity()Ljava/lang/String;

    move-result-object v27

    aput-object v27, v9, v26

    const/16 v26, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    move-object/from16 v27, v0

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;
    invoke-static/range {v27 .. v27}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getLocationState()Ljava/lang/String;

    move-result-object v27

    aput-object v27, v9, v26

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v7, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$700(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-eqz v21, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/16 v26, 0x0

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v9, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$800(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_3
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v20

    .line 195
    .end local v3    # "strContextToday":Ljava/lang/Integer;
    .end local v5    # "todayStr":Ljava/lang/String;
    .end local v21    # "oneDaySpokenMsg":Ljava/lang/String;
    :goto_4
    move-object/from16 v22, v20

    .line 223
    :goto_5
    if-eqz v22, :cond_3

    .line 224
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$1200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-interface {v2, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    .line 226
    :cond_3
    if-eqz v19, :cond_4

    if-nez v22, :cond_4

    .line 227
    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_plus_seven:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v2, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$1300(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    .line 228
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$1400(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-interface {v2, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    .line 230
    :cond_4
    invoke-static {v11}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 231
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$1500(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowWeatherDailyWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    move-object/from16 v26, v0

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;
    invoke-static/range {v26 .. v26}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-interface {v2, v7, v9, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 235
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->actionSuccess()V

    goto/16 :goto_0

    .line 175
    .restart local v10    # "date":Ljava/util/Date;
    :cond_5
    const/16 v24, 0x0

    goto/16 :goto_1

    .line 176
    :cond_6
    const/16 v19, 0x0

    goto/16 :goto_2

    .line 178
    .end local v10    # "date":Ljava/util/Date;
    :catch_0
    move-exception v16

    .line 179
    .local v16, "e":Ljava/text/ParseException;
    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$400()Ljava/lang/String;

    move-result-object v2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "DateParseError "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v16 .. v16}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 189
    .end local v16    # "e":Ljava/text/ParseException;
    .restart local v3    # "strContextToday":Ljava/lang/Integer;
    .restart local v5    # "todayStr":Ljava/lang/String;
    .restart local v21    # "oneDaySpokenMsg":Ljava/lang/String;
    :cond_7
    :try_start_2
    const-string/jumbo v2, ""
    :try_end_2
    .catch Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_3

    .line 191
    .end local v3    # "strContextToday":Ljava/lang/Integer;
    .end local v5    # "todayStr":Ljava/lang/String;
    .end local v21    # "oneDaySpokenMsg":Ljava/lang/String;
    :catch_1
    move-exception v16

    .line 193
    .local v16, "e":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;->printStackTrace()V

    goto/16 :goto_4

    .line 201
    .end local v16    # "e":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    :cond_8
    if-eqz v23, :cond_9

    .line 202
    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_today:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v2, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$900(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 206
    .local v10, "date":Ljava/lang/String;
    :goto_7
    if-eqz v23, :cond_a

    sget-object v12, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;->TODAY:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    .line 209
    .local v12, "dateType":Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;
    :goto_8
    const/4 v13, 0x0

    .line 211
    .local v13, "generalMsg":Ljava/lang/String;
    :try_start_3
    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_with_locatin:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    move-object/from16 v26, v0

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;
    invoke-static/range {v26 .. v26}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getLocationCity()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v7, v9

    const/4 v9, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    move-object/from16 v26, v0

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;
    invoke-static/range {v26 .. v26}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getLocationState()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v7, v9

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v2, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$1100(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_3
    .catch Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v13

    .line 216
    :goto_9
    const/4 v8, 0x0

    .line 217
    .local v8, "strContext":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    move-object v9, v4

    invoke-virtual/range {v7 .. v13}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getOneDaySpokenMsg(Ljava/lang/Integer;Lcom/vlingo/core/internal/weather/WeatherInfoUtil;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_5

    .line 204
    .end local v8    # "strContext":Ljava/lang/Integer;
    .end local v10    # "date":Ljava/lang/String;
    .end local v12    # "dateType":Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;
    .end local v13    # "generalMsg":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->strDatetoDOW(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, v11}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$1000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .restart local v10    # "date":Ljava/lang/String;
    goto :goto_7

    .line 206
    :cond_a
    if-eqz v24, :cond_b

    sget-object v12, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;->TOMORROW:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    goto :goto_8

    :cond_b
    sget-object v12, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;->OTHER:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    goto :goto_8

    .line 212
    .restart local v12    # "dateType":Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;
    .restart local v13    # "generalMsg":Ljava/lang/String;
    :catch_2
    move-exception v17

    .line 214
    .local v17, "e1":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;->printStackTrace()V

    goto :goto_9

    .line 233
    .end local v10    # "date":Ljava/lang/String;
    .end local v12    # "dateType":Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;
    .end local v13    # "generalMsg":Ljava/lang/String;
    .end local v17    # "e1":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$1600(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowWeatherWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    move-object/from16 v26, v0

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->adaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;
    invoke-static/range {v26 .. v26}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$1;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-interface {v2, v7, v9, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto/16 :goto_6
.end method
