.class public interface abstract Lcom/vlingo/core/internal/contacts/IContactDBUtil;
.super Ljava/lang/Object;
.source "IContactDBUtil.java"


# virtual methods
.method public abstract getContactDataByEmail(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactData;
.end method

.method public abstract getContactMatchByPhoneNumber(Ljava/lang/String;Landroid/content/Context;)Lcom/vlingo/core/internal/contacts/ContactMatch;
.end method

.method public abstract getContactMatches(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;Z)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;Z)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getExactContactMatch(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactMatch;
.end method

.method public abstract isAddressBookEmpty(Landroid/content/Context;)Z
.end method

.method public abstract isProviderStatusNormal(Landroid/content/ContentResolver;)Z
.end method
