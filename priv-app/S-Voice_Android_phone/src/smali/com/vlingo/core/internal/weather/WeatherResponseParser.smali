.class public Lcom/vlingo/core/internal/weather/WeatherResponseParser;
.super Ljava/lang/Object;
.source "WeatherResponseParser.java"

# interfaces
.implements Lcom/vlingo/core/internal/weather/WeatherElementNames;


# static fields
.field private static final K_WEATHER_XML:Ljava/lang/String; = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><WeatherResponse Location=\"?????, ?????\" Provider=\"KweatherContentProvider\"><CurrentCondition><CurrentCondition BadFeel=\"0.0\" DewPt=\"0.0\" FineDustConcentration=\"0.0\" Humidity=\"52.0\" ObservationTime=\"2012-09-14 17:54:00\" Pressure=\"1020.7\" RealFeel=\"27.3\" TempC=\"0.0\" TempF=\"0.0\" Temperature=\"26.7\" Vis=\"0.0\" Visibility=\"-1\" WeatherCode=\"2\" WeatherText=\"NA\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></CurrentCondition><Forecasts><Forecast ForecastDate=\"2012-09-14\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"-1\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"><DaytimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"25.5\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"16.4\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"1\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></DaytimeForecast><NighttimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"25.5\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"16.4\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"1\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></NighttimeForecast></Forecast><Forecast ForecastDate=\"2012-09-15\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"-1\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"><DaytimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"22.8\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"16.1\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"5\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></DaytimeForecast><NighttimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"22.8\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"16.1\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"5\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></NighttimeForecast></Forecast><Forecast ForecastDate=\"2012-09-16\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"-1\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"><DaytimeForecast><DayNighttimeForecast  IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"18.9\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"12.2\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"2\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></DaytimeForecast><NighttimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"18.9\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"12.2\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"2\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></NighttimeForecast></Forecast><Forecast ForecastDate=\"2012-09-17\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"-1\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"><DaytimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"22.8\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"12.2\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"2\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></DaytimeForecast><NighttimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"22.8\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"12.2\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"2\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></NighttimeForecast></Forecast><Forecast ForecastDate=\"2012-09-18\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"-1\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"><DaytimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"23.9\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"15.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"10\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></DaytimeForecast><NighttimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"23.9\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"15.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"10\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></NighttimeForecast></Forecast><Forecast ForecastDate=\"2012-09-19\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"-1\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"><DaytimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"23.3\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"17.2\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"10\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></DaytimeForecast><NighttimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"23.3\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"17.2\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"10\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></NighttimeForecast></Forecast><Forecast ForecastDate=\"2012-09-20\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"-1\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"><DaytimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"21.6\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"13.9\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"2\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></DaytimeForecast><NighttimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"21.6\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"13.9\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"2\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></NighttimeForecast></Forecast></Forecasts><MoonPhases/><WeatherLocation><Location City=\"?????\" Country=\"?????\"/></WeatherLocation></WeatherResponse>"

.field private static final TAG:Ljava/lang/String;

.field private static final XML:Ljava/lang/String; = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><WeatherResponse><CurrentCondition><CurrentCondition CloudCover=\"0%\" DewPt=\"6.0\" Humidity=\"11.0\" ObservationTime=\"2012-01-26 07:53:14\" Precip=\"0.0\" Pressure=\"102.0\" RealFeel=\"13.0\" Sunrise=\"6:54 AM\" Sunset=\"5:17 PM\" TempC=\"0.0\" TempF=\"0.0\" Temperature=\"11.0\" UVIndex=\"Low\" Visibility=\"16\" WeatherCode=\"1\" WeatherText=\"Sunny\" WindDir=\"N\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></CurrentCondition><Forecasts><Forecast ForecastDate=\"2012-01-26\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"-1\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"><DaytimeForecast><DayNighttimeForecast IceAmount=\"0.0\" LongText=\"Very warm with sunshine\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"29.0\" RealFeelLow=\"13.0\" ShortText=\"Very warm with sunshine\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"28.0\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"12.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"1\" WindDir=\"N\" WindGusts=\"4.0\" WindSpeed=\"1.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></DaytimeForecast><NighttimeForecast><DayNighttimeForecast IceAmount=\"0.0\" LongText=\"Mainly clear\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"29.0\" RealFeelLow=\"13.0\" ShortText=\"Mainly clear\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"28.0\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"12.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"34\" WindDir=\"N\" WindGusts=\"3.0\" WindSpeed=\"1.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></NighttimeForecast></Forecast><Forecast ForecastDate=\"2012-01-27\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"-1\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"><DaytimeForecast><DayNighttimeForecast IceAmount=\"0.0\" LongText=\"Mostly sunny\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"27.0\" RealFeelLow=\"13.0\" ShortText=\"Mostly sunny\" SnowAmount=\"0.0\" TStormProb=\"3.0\" TempMax=\"24.0\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"11.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"2\" WindDir=\"N\" WindGusts=\"6.0\" WindSpeed=\"3.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></DaytimeForecast><NighttimeForecast><DayNighttimeForecast IceAmount=\"0.0\" LongText=\"Clear\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"27.0\" RealFeelLow=\"13.0\" ShortText=\"Clear\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"24.0\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"11.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"33\" WindDir=\"N\" WindGusts=\"3.0\" WindSpeed=\"1.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></NighttimeForecast></Forecast><Forecast ForecastDate=\"2012-01-28\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"-1\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"><DaytimeForecast><DayNighttimeForecast IceAmount=\"0.0\" LongText=\"Mostly sunny and very warm\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"28.0\" RealFeelLow=\"12.0\" ShortText=\"Mostly sunny and very warm\" SnowAmount=\"0.0\" TStormProb=\"1.0\" TempMax=\"28.0\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"10.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"2\" WindDir=\"NNW\" WindGusts=\"4.0\" WindSpeed=\"3.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></DaytimeForecast><NighttimeForecast><DayNighttimeForecast IceAmount=\"0.0\" LongText=\"Mostly clear\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"28.0\" RealFeelLow=\"12.0\" ShortText=\"Mostly clear\" SnowAmount=\"0.0\" TStormProb=\"1.0\" TempMax=\"28.0\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"10.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"34\" WindDir=\"NNE\" WindGusts=\"1.0\" WindSpeed=\"1.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></NighttimeForecast></Forecast><Forecast ForecastDate=\"2012-01-29\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"-1\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"><DaytimeForecast><DayNighttimeForecast IceAmount=\"0.0\" LongText=\"Plenty of sunshine\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"26.0\" RealFeelLow=\"12.0\" ShortText=\"Plenty of sunshine\" SnowAmount=\"0.0\" TStormProb=\"1.0\" TempMax=\"25.0\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"11.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"1\" WindDir=\"N\" WindGusts=\"1.0\" WindSpeed=\"1.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></DaytimeForecast><NighttimeForecast><DayNighttimeForecast IceAmount=\"0.0\" LongText=\"Partly cloudy\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"26.0\" RealFeelLow=\"12.0\" ShortText=\"Partly cloudy\" SnowAmount=\"0.0\" TStormProb=\"1.0\" TempMax=\"25.0\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"11.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"35\" WindDir=\"SW\" WindGusts=\"3.0\" WindSpeed=\"1.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></NighttimeForecast></Forecast><Forecast ForecastDate=\"2012-01-30\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"-1\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"><DaytimeForecast><DayNighttimeForecast IceAmount=\"0.0\" LongText=\"Sunny to partly cloudy and nice\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"23.0\" RealFeelLow=\"9.0\" ShortText=\"Mostly sunny and nice\" SnowAmount=\"0.0\" TStormProb=\"1.0\" TempMax=\"22.0\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"9.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"2\" WindDir=\"N\" WindGusts=\"1.0\" WindSpeed=\"1.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></DaytimeForecast><NighttimeForecast><DayNighttimeForecast IceAmount=\"0.0\" LongText=\"Partly cloudy\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"23.0\" RealFeelLow=\"9.0\" ShortText=\"Partly cloudy\" SnowAmount=\"0.0\" TStormProb=\"1.0\" TempMax=\"22.0\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"9.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"35\" WindDir=\"NE\" WindGusts=\"3.0\" WindSpeed=\"3.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></NighttimeForecast></Forecast><Forecast ForecastDate=\"2012-01-31\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"-1\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"><DaytimeForecast><DayNighttimeForecast IceAmount=\"0.0\" LongText=\"Sunny to partly cloudy and pleasant\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"22.0\" RealFeelLow=\"11.0\" ShortText=\"Mostly sunny and pleasant\" SnowAmount=\"0.0\" TStormProb=\"2.0\" TempMax=\"22.0\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"10.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"2\" WindDir=\"NE\" WindGusts=\"3.0\" WindSpeed=\"3.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></DaytimeForecast><NighttimeForecast><DayNighttimeForecast IceAmount=\"0.0\" LongText=\"Mainly clear\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"22.0\" RealFeelLow=\"11.0\" ShortText=\"Mainly clear\" SnowAmount=\"0.0\" TStormProb=\"2.0\" TempMax=\"22.0\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"10.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"34\" WindDir=\"NNE\" WindGusts=\"6.0\" WindSpeed=\"4.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></NighttimeForecast></Forecast><Forecast ForecastDate=\"2012-02-01\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"-1\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"><DaytimeForecast><DayNighttimeForecast IceAmount=\"0.0\" LongText=\"Mostly sunny\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"25.0\" RealFeelLow=\"11.0\" ShortText=\"Mostly sunny\" SnowAmount=\"0.0\" TStormProb=\"2.0\" TempMax=\"25.0\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"11.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"2\" WindDir=\"NNE\" WindGusts=\"6.0\" WindSpeed=\"4.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></DaytimeForecast><NighttimeForecast><DayNighttimeForecast IceAmount=\"0.0\" LongText=\"Clear to partly cloudy\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"25.0\" RealFeelLow=\"11.0\" ShortText=\"Clear to partly cloudy\" SnowAmount=\"0.0\" TStormProb=\"2.0\" TempMax=\"25.0\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"11.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"34\" WindDir=\"NNE\" WindGusts=\"12.0\" WindSpeed=\"6.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></NighttimeForecast></Forecast></Forecasts><Images><Images MapSpace=\"http://vortex.accuweather.com/adc2004/mapspace/usradar/480x480/CAS.png\" Radar=\"&#10;&#9;&#9;&#9;&#9;&#9;&#9;http://sirocco.accuweather.com/nx_mosaic_108x81c/re/inmreCAS.gif&#10;&#9;&#9;&#9;&#9;&#9;\" Url=\"http://www.accuweather.com/m/en-us/US/CA/Los Angeles/radar.aspx?p=samsungmobile&amp;cityId=347625\"/></Images><MoonPhases><MoonPhase Date=\"2012-01-26\" Day=\"2\" Text=\"Waxing Crescent\"/><MoonPhase Date=\"2012-01-27\" Day=\"3\" Text=\"Waxing Crescent\"/><MoonPhase Date=\"2012-01-28\" Day=\"4\" Text=\"Waxing Crescent\"/><MoonPhase Date=\"2012-01-29\" Day=\"5\" Text=\"Waxing Crescent\"/><MoonPhase Date=\"2012-01-30\" Day=\"6\" Text=\"Waxing Crescent\"/><MoonPhase Date=\"2012-01-31\" Day=\"7\" Text=\"First\"/><MoonPhase Date=\"2012-02-01\" Day=\"8\" Text=\"Waxing Gibbous\"/><MoonPhase Date=\"2012-02-02\" Day=\"9\" Text=\"Waxing Gibbous\"/><MoonPhase Date=\"2012-02-03\" Day=\"10\" Text=\"Waxing Gibbous\"/><MoonPhase Date=\"2012-02-04\" Day=\"11\" Text=\"Waxing Gibbous\"/><MoonPhase Date=\"2012-02-05\" Day=\"12\" Text=\"Waxing Gibbous\"/><MoonPhase Date=\"2012-02-06\" Day=\"13\" Text=\"Waxing Gibbous\"/><MoonPhase Date=\"2012-02-07\" Day=\"14\" Text=\"Full\"/><MoonPhase Date=\"2012-02-08\" Day=\"15\" Text=\"Waning Gibbous\"/><MoonPhase Date=\"2012-02-09\" Day=\"16\" Text=\"Waning Gibbous\"/><MoonPhase Date=\"2012-02-10\" Day=\"17\" Text=\"Waning Gibbous\"/><MoonPhase Date=\"2012-02-11\" Day=\"18\" Text=\"Waning Gibbous\"/><MoonPhase Date=\"2012-02-12\" Day=\"19\" Text=\"Waning Gibbous\"/><MoonPhase Date=\"2012-02-13\" Day=\"20\" Text=\"Waning Gibbous\"/><MoonPhase Date=\"2012-02-14\" Day=\"21\" Text=\"Last\"/><MoonPhase Date=\"2012-02-15\" Day=\"22\" Text=\"Waning Crescent\"/><MoonPhase Date=\"2012-02-16\" Day=\"23\" Text=\"Waning Crescent\"/><MoonPhase Date=\"2012-02-17\" Day=\"24\" Text=\"Waning Crescent\"/><MoonPhase Date=\"2012-02-18\" Day=\"25\" Text=\"Waning Crescent\"/><MoonPhase Date=\"2012-02-19\" Day=\"26\" Text=\"Waning Crescent\"/><MoonPhase Date=\"2012-02-20\" Day=\"27\" Text=\"Waning Crescent\"/><MoonPhase Date=\"2012-02-21\" Day=\"1\" Text=\"New\"/><MoonPhase Date=\"2012-02-22\" Day=\"1\" Text=\"New\"/><MoonPhase Date=\"2012-02-23\" Day=\"1\" Text=\"New\"/><MoonPhase Date=\"2012-02-24\" Day=\"1\" Text=\"New\"/><MoonPhase Date=\"2012-02-25\" Day=\"2\" Text=\"Waxing Crescent\"/><MoonPhase Date=\"2012-02-26\" Day=\"3\" Text=\"Waxing Crescent\"/></MoonPhases></WeatherResponse>"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/vlingo/core/internal/weather/WeatherResponseParser;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createCurrentConditionsElement(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/RootElement;)V
    .locals 5
    .param p0, "response"    # Lcom/vlingo/core/internal/weather/WeatherElement;
    .param p1, "root"    # Landroid/sax/RootElement;

    .prologue
    .line 214
    new-instance v3, Lcom/vlingo/core/internal/weather/WeatherElement;

    const-string/jumbo v4, "CurrentCondition"

    invoke-direct {v3, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;-><init>(Ljava/lang/String;)V

    .line 215
    .local v3, "wConditions":Lcom/vlingo/core/internal/weather/WeatherElement;
    const-string/jumbo v4, "CurrentCondition"

    invoke-virtual {p1, v4}, Landroid/sax/RootElement;->getChild(Ljava/lang/String;)Landroid/sax/Element;

    move-result-object v1

    .line 216
    .local v1, "conditions":Landroid/sax/Element;
    invoke-static {v3, v1}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->setAttribute(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/Element;)V

    .line 217
    new-instance v4, Lcom/vlingo/core/internal/weather/WeatherResponseParser$15;

    invoke-direct {v4, p0, v3}, Lcom/vlingo/core/internal/weather/WeatherResponseParser$15;-><init>(Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;)V

    invoke-virtual {v1, v4}, Landroid/sax/Element;->setEndElementListener(Landroid/sax/EndElementListener;)V

    .line 224
    const-string/jumbo v4, "CurrentCondition"

    invoke-virtual {v1, v4}, Landroid/sax/Element;->getChild(Ljava/lang/String;)Landroid/sax/Element;

    move-result-object v0

    .line 225
    .local v0, "condition":Landroid/sax/Element;
    new-instance v2, Lcom/vlingo/core/internal/weather/WeatherElement;

    const-string/jumbo v4, "CurrentCondition"

    invoke-direct {v2, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;-><init>(Ljava/lang/String;)V

    .line 226
    .local v2, "wCondition":Lcom/vlingo/core/internal/weather/WeatherElement;
    invoke-static {v2, v0}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->setAttribute(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/Element;)V

    .line 227
    new-instance v4, Lcom/vlingo/core/internal/weather/WeatherResponseParser$16;

    invoke-direct {v4, v3, v2}, Lcom/vlingo/core/internal/weather/WeatherResponseParser$16;-><init>(Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;)V

    invoke-virtual {v0, v4}, Landroid/sax/Element;->setEndElementListener(Landroid/sax/EndElementListener;)V

    .line 233
    return-void
.end method

.method private static createForecastElement(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/RootElement;)V
    .locals 13
    .param p0, "response"    # Lcom/vlingo/core/internal/weather/WeatherElement;
    .param p1, "root"    # Landroid/sax/RootElement;

    .prologue
    .line 152
    new-instance v8, Lcom/vlingo/core/internal/weather/WeatherElement;

    const-string/jumbo v12, "Forecasts"

    invoke-direct {v8, v12}, Lcom/vlingo/core/internal/weather/WeatherElement;-><init>(Ljava/lang/String;)V

    .line 153
    .local v8, "wForcasts":Lcom/vlingo/core/internal/weather/WeatherElement;
    const-string/jumbo v12, "Forecasts"

    invoke-virtual {p1, v12}, Landroid/sax/RootElement;->getChild(Ljava/lang/String;)Landroid/sax/Element;

    move-result-object v3

    .line 154
    .local v3, "forecasts":Landroid/sax/Element;
    invoke-static {v8, v3}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->setAttribute(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/Element;)V

    .line 155
    new-instance v12, Lcom/vlingo/core/internal/weather/WeatherResponseParser$9;

    invoke-direct {v12, p0, v8}, Lcom/vlingo/core/internal/weather/WeatherResponseParser$9;-><init>(Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;)V

    invoke-virtual {v3, v12}, Landroid/sax/Element;->setEndElementListener(Landroid/sax/EndElementListener;)V

    .line 162
    new-instance v9, Lcom/vlingo/core/internal/weather/WeatherElement;

    const-string/jumbo v12, "Forecast"

    invoke-direct {v9, v12}, Lcom/vlingo/core/internal/weather/WeatherElement;-><init>(Ljava/lang/String;)V

    .line 163
    .local v9, "wForecast":Lcom/vlingo/core/internal/weather/WeatherElement;
    const-string/jumbo v12, "Forecast"

    invoke-virtual {v3, v12}, Landroid/sax/Element;->getChild(Ljava/lang/String;)Landroid/sax/Element;

    move-result-object v2

    .line 164
    .local v2, "forecast":Landroid/sax/Element;
    invoke-static {v9, v2}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->setAttribute(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/Element;)V

    .line 165
    new-instance v12, Lcom/vlingo/core/internal/weather/WeatherResponseParser$10;

    invoke-direct {v12, v8, v9}, Lcom/vlingo/core/internal/weather/WeatherResponseParser$10;-><init>(Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;)V

    invoke-virtual {v2, v12}, Landroid/sax/Element;->setEndElementListener(Landroid/sax/EndElementListener;)V

    .line 172
    new-instance v7, Lcom/vlingo/core/internal/weather/WeatherElement;

    const-string/jumbo v12, "DaytimeForecast"

    invoke-direct {v7, v12}, Lcom/vlingo/core/internal/weather/WeatherElement;-><init>(Ljava/lang/String;)V

    .line 173
    .local v7, "wDaytimeForecast":Lcom/vlingo/core/internal/weather/WeatherElement;
    const-string/jumbo v12, "DaytimeForecast"

    invoke-virtual {v2, v12}, Landroid/sax/Element;->getChild(Ljava/lang/String;)Landroid/sax/Element;

    move-result-object v1

    .line 174
    .local v1, "daytimeForecast":Landroid/sax/Element;
    invoke-static {v7, v1}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->setAttribute(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/Element;)V

    .line 175
    new-instance v12, Lcom/vlingo/core/internal/weather/WeatherResponseParser$11;

    invoke-direct {v12, v9, v7}, Lcom/vlingo/core/internal/weather/WeatherResponseParser$11;-><init>(Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;)V

    invoke-virtual {v1, v12}, Landroid/sax/Element;->setEndElementListener(Landroid/sax/EndElementListener;)V

    .line 182
    new-instance v6, Lcom/vlingo/core/internal/weather/WeatherElement;

    const-string/jumbo v12, "DayNighttimeForecast"

    invoke-direct {v6, v12}, Lcom/vlingo/core/internal/weather/WeatherElement;-><init>(Ljava/lang/String;)V

    .line 183
    .local v6, "wDayDayNittimeForecast":Lcom/vlingo/core/internal/weather/WeatherElement;
    const-string/jumbo v12, "DayNighttimeForecast"

    invoke-virtual {v1, v12}, Landroid/sax/Element;->getChild(Ljava/lang/String;)Landroid/sax/Element;

    move-result-object v0

    .line 184
    .local v0, "dayDayNittimeForecast":Landroid/sax/Element;
    invoke-static {v6, v0}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->setAttribute(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/Element;)V

    .line 185
    new-instance v12, Lcom/vlingo/core/internal/weather/WeatherResponseParser$12;

    invoke-direct {v12, v7, v6}, Lcom/vlingo/core/internal/weather/WeatherResponseParser$12;-><init>(Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;)V

    invoke-virtual {v0, v12}, Landroid/sax/Element;->setEndElementListener(Landroid/sax/EndElementListener;)V

    .line 192
    new-instance v11, Lcom/vlingo/core/internal/weather/WeatherElement;

    const-string/jumbo v12, "NighttimeForecast"

    invoke-direct {v11, v12}, Lcom/vlingo/core/internal/weather/WeatherElement;-><init>(Ljava/lang/String;)V

    .line 193
    .local v11, "wNighttimeForecast":Lcom/vlingo/core/internal/weather/WeatherElement;
    const-string/jumbo v12, "NighttimeForecast"

    invoke-virtual {v2, v12}, Landroid/sax/Element;->getChild(Ljava/lang/String;)Landroid/sax/Element;

    move-result-object v5

    .line 194
    .local v5, "nighttimeForecast":Landroid/sax/Element;
    invoke-static {v11, v5}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->setAttribute(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/Element;)V

    .line 195
    new-instance v12, Lcom/vlingo/core/internal/weather/WeatherResponseParser$13;

    invoke-direct {v12, v9, v11}, Lcom/vlingo/core/internal/weather/WeatherResponseParser$13;-><init>(Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;)V

    invoke-virtual {v5, v12}, Landroid/sax/Element;->setEndElementListener(Landroid/sax/EndElementListener;)V

    .line 202
    new-instance v10, Lcom/vlingo/core/internal/weather/WeatherElement;

    const-string/jumbo v12, "DayNighttimeForecast"

    invoke-direct {v10, v12}, Lcom/vlingo/core/internal/weather/WeatherElement;-><init>(Ljava/lang/String;)V

    .line 203
    .local v10, "wNightDayNittimeForecast":Lcom/vlingo/core/internal/weather/WeatherElement;
    const-string/jumbo v12, "DayNighttimeForecast"

    invoke-virtual {v5, v12}, Landroid/sax/Element;->getChild(Ljava/lang/String;)Landroid/sax/Element;

    move-result-object v4

    .line 204
    .local v4, "nightDayNittimeForecast":Landroid/sax/Element;
    invoke-static {v10, v4}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->setAttribute(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/Element;)V

    .line 205
    new-instance v12, Lcom/vlingo/core/internal/weather/WeatherResponseParser$14;

    invoke-direct {v12, v11, v10}, Lcom/vlingo/core/internal/weather/WeatherResponseParser$14;-><init>(Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;)V

    invoke-virtual {v4, v12}, Landroid/sax/Element;->setEndElementListener(Landroid/sax/EndElementListener;)V

    .line 211
    return-void
.end method

.method private static createImagesElement(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/RootElement;)V
    .locals 5
    .param p0, "response"    # Lcom/vlingo/core/internal/weather/WeatherElement;
    .param p1, "root"    # Landroid/sax/RootElement;

    .prologue
    .line 130
    new-instance v3, Lcom/vlingo/core/internal/weather/WeatherElement;

    const-string/jumbo v4, "Images"

    invoke-direct {v3, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;-><init>(Ljava/lang/String;)V

    .line 131
    .local v3, "wImages":Lcom/vlingo/core/internal/weather/WeatherElement;
    const-string/jumbo v4, "Images"

    invoke-virtual {p1, v4}, Landroid/sax/RootElement;->getChild(Ljava/lang/String;)Landroid/sax/Element;

    move-result-object v1

    .line 132
    .local v1, "images":Landroid/sax/Element;
    invoke-static {v3, v1}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->setAttribute(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/Element;)V

    .line 133
    new-instance v4, Lcom/vlingo/core/internal/weather/WeatherResponseParser$7;

    invoke-direct {v4, p0, v3}, Lcom/vlingo/core/internal/weather/WeatherResponseParser$7;-><init>(Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;)V

    invoke-virtual {v1, v4}, Landroid/sax/Element;->setEndElementListener(Landroid/sax/EndElementListener;)V

    .line 140
    new-instance v2, Lcom/vlingo/core/internal/weather/WeatherElement;

    const-string/jumbo v4, "Images"

    invoke-direct {v2, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;-><init>(Ljava/lang/String;)V

    .line 141
    .local v2, "wImage":Lcom/vlingo/core/internal/weather/WeatherElement;
    const-string/jumbo v4, "Images"

    invoke-virtual {v1, v4}, Landroid/sax/Element;->getChild(Ljava/lang/String;)Landroid/sax/Element;

    move-result-object v0

    .line 142
    .local v0, "image":Landroid/sax/Element;
    invoke-static {v2, v0}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->setAttribute(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/Element;)V

    .line 143
    new-instance v4, Lcom/vlingo/core/internal/weather/WeatherResponseParser$8;

    invoke-direct {v4, v3, v2}, Lcom/vlingo/core/internal/weather/WeatherResponseParser$8;-><init>(Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;)V

    invoke-virtual {v0, v4}, Landroid/sax/Element;->setEndElementListener(Landroid/sax/EndElementListener;)V

    .line 149
    return-void
.end method

.method private static createMoonPhasesElement(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/RootElement;)V
    .locals 5
    .param p0, "response"    # Lcom/vlingo/core/internal/weather/WeatherElement;
    .param p1, "root"    # Landroid/sax/RootElement;

    .prologue
    .line 108
    new-instance v3, Lcom/vlingo/core/internal/weather/WeatherElement;

    const-string/jumbo v4, "MoonPhases"

    invoke-direct {v3, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;-><init>(Ljava/lang/String;)V

    .line 109
    .local v3, "wMoonPhases":Lcom/vlingo/core/internal/weather/WeatherElement;
    const-string/jumbo v4, "MoonPhases"

    invoke-virtual {p1, v4}, Landroid/sax/RootElement;->getChild(Ljava/lang/String;)Landroid/sax/Element;

    move-result-object v1

    .line 110
    .local v1, "moonPhases":Landroid/sax/Element;
    invoke-static {v3, v1}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->setAttribute(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/Element;)V

    .line 111
    new-instance v4, Lcom/vlingo/core/internal/weather/WeatherResponseParser$5;

    invoke-direct {v4, p0, v3}, Lcom/vlingo/core/internal/weather/WeatherResponseParser$5;-><init>(Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;)V

    invoke-virtual {v1, v4}, Landroid/sax/Element;->setEndElementListener(Landroid/sax/EndElementListener;)V

    .line 118
    new-instance v2, Lcom/vlingo/core/internal/weather/WeatherElement;

    const-string/jumbo v4, "MoonPhase"

    invoke-direct {v2, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;-><init>(Ljava/lang/String;)V

    .line 119
    .local v2, "wMoonPhase":Lcom/vlingo/core/internal/weather/WeatherElement;
    const-string/jumbo v4, "MoonPhase"

    invoke-virtual {v1, v4}, Landroid/sax/Element;->getChild(Ljava/lang/String;)Landroid/sax/Element;

    move-result-object v0

    .line 120
    .local v0, "moonPhase":Landroid/sax/Element;
    invoke-static {v2, v0}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->setAttribute(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/Element;)V

    .line 121
    new-instance v4, Lcom/vlingo/core/internal/weather/WeatherResponseParser$6;

    invoke-direct {v4, v3, v2}, Lcom/vlingo/core/internal/weather/WeatherResponseParser$6;-><init>(Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;)V

    invoke-virtual {v0, v4}, Landroid/sax/Element;->setEndElementListener(Landroid/sax/EndElementListener;)V

    .line 127
    return-void
.end method

.method private static createUnitsElement(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/RootElement;)V
    .locals 5
    .param p0, "response"    # Lcom/vlingo/core/internal/weather/WeatherElement;
    .param p1, "root"    # Landroid/sax/RootElement;

    .prologue
    .line 86
    new-instance v3, Lcom/vlingo/core/internal/weather/WeatherElement;

    const-string/jumbo v4, "Units"

    invoke-direct {v3, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;-><init>(Ljava/lang/String;)V

    .line 87
    .local v3, "wUnits":Lcom/vlingo/core/internal/weather/WeatherElement;
    const-string/jumbo v4, "Units"

    invoke-virtual {p1, v4}, Landroid/sax/RootElement;->getChild(Ljava/lang/String;)Landroid/sax/Element;

    move-result-object v1

    .line 88
    .local v1, "units":Landroid/sax/Element;
    invoke-static {v3, v1}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->setAttribute(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/Element;)V

    .line 89
    new-instance v4, Lcom/vlingo/core/internal/weather/WeatherResponseParser$3;

    invoke-direct {v4, p0, v3}, Lcom/vlingo/core/internal/weather/WeatherResponseParser$3;-><init>(Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;)V

    invoke-virtual {v1, v4}, Landroid/sax/Element;->setEndElementListener(Landroid/sax/EndElementListener;)V

    .line 96
    new-instance v2, Lcom/vlingo/core/internal/weather/WeatherElement;

    const-string/jumbo v4, "Units"

    invoke-direct {v2, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;-><init>(Ljava/lang/String;)V

    .line 97
    .local v2, "wUnit":Lcom/vlingo/core/internal/weather/WeatherElement;
    const-string/jumbo v4, "Units"

    invoke-virtual {v1, v4}, Landroid/sax/Element;->getChild(Ljava/lang/String;)Landroid/sax/Element;

    move-result-object v0

    .line 98
    .local v0, "unit":Landroid/sax/Element;
    invoke-static {v2, v0}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->setAttribute(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/Element;)V

    .line 99
    new-instance v4, Lcom/vlingo/core/internal/weather/WeatherResponseParser$4;

    invoke-direct {v4, v3, v2}, Lcom/vlingo/core/internal/weather/WeatherResponseParser$4;-><init>(Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;)V

    invoke-virtual {v0, v4}, Landroid/sax/Element;->setEndElementListener(Landroid/sax/EndElementListener;)V

    .line 105
    return-void
.end method

.method private static createWeatherLocationElement(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/RootElement;)V
    .locals 5
    .param p0, "response"    # Lcom/vlingo/core/internal/weather/WeatherElement;
    .param p1, "root"    # Landroid/sax/RootElement;

    .prologue
    .line 64
    new-instance v2, Lcom/vlingo/core/internal/weather/WeatherElement;

    const-string/jumbo v4, "WeatherLocation"

    invoke-direct {v2, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;-><init>(Ljava/lang/String;)V

    .line 65
    .local v2, "wWeatherLocation":Lcom/vlingo/core/internal/weather/WeatherElement;
    const-string/jumbo v4, "WeatherLocation"

    invoke-virtual {p1, v4}, Landroid/sax/RootElement;->getChild(Ljava/lang/String;)Landroid/sax/Element;

    move-result-object v3

    .line 66
    .local v3, "weatherLocation":Landroid/sax/Element;
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->setAttribute(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/Element;)V

    .line 67
    new-instance v4, Lcom/vlingo/core/internal/weather/WeatherResponseParser$1;

    invoke-direct {v4, p0, v2}, Lcom/vlingo/core/internal/weather/WeatherResponseParser$1;-><init>(Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;)V

    invoke-virtual {v3, v4}, Landroid/sax/Element;->setEndElementListener(Landroid/sax/EndElementListener;)V

    .line 74
    new-instance v1, Lcom/vlingo/core/internal/weather/WeatherElement;

    const-string/jumbo v4, "Location"

    invoke-direct {v1, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;-><init>(Ljava/lang/String;)V

    .line 75
    .local v1, "wLocation":Lcom/vlingo/core/internal/weather/WeatherElement;
    const-string/jumbo v4, "Location"

    invoke-virtual {v3, v4}, Landroid/sax/Element;->getChild(Ljava/lang/String;)Landroid/sax/Element;

    move-result-object v0

    .line 76
    .local v0, "location":Landroid/sax/Element;
    invoke-static {v1, v0}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->setAttribute(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/Element;)V

    .line 77
    new-instance v4, Lcom/vlingo/core/internal/weather/WeatherResponseParser$2;

    invoke-direct {v4, v2, v1}, Lcom/vlingo/core/internal/weather/WeatherResponseParser$2;-><init>(Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;)V

    invoke-virtual {v0, v4}, Landroid/sax/Element;->setEndElementListener(Landroid/sax/EndElementListener;)V

    .line 83
    return-void
.end method

.method private static getErrorMessage(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 12
    .param p0, "inputStream"    # Ljava/io/InputStream;

    .prologue
    .line 270
    const-string/jumbo v1, "Unexpected Server Error"

    .line 273
    .local v1, "errorMessage":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v3

    .line 274
    .local v3, "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v3}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v8

    .line 275
    .local v8, "xmlParser":Ljavax/xml/parsers/DocumentBuilder;
    invoke-virtual {v8, p0}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 276
    .local v0, "doc":Lorg/w3c/dom/Document;
    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v6

    .line 278
    .local v6, "root":Lorg/w3c/dom/Element;
    const-string/jumbo v9, "Error"

    invoke-interface {v6, v9}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 279
    .local v2, "errors":Lorg/w3c/dom/NodeList;
    if-eqz v2, :cond_1

    .line 280
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v9

    if-lez v9, :cond_2

    .line 282
    const-string/jumbo v9, "Message"

    invoke-interface {v6, v9}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v5

    .line 284
    .local v5, "messages":Lorg/w3c/dom/NodeList;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {v5}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v9

    if-ge v4, v9, :cond_0

    .line 285
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v5, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v10

    invoke-interface {v10}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 284
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 287
    :cond_0
    sget-object v9, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->TAG:Ljava/lang/String;

    const-string/jumbo v10, "LSP1 Server error"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v0    # "doc":Lorg/w3c/dom/Document;
    .end local v2    # "errors":Lorg/w3c/dom/NodeList;
    .end local v3    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v4    # "i":I
    .end local v5    # "messages":Lorg/w3c/dom/NodeList;
    .end local v6    # "root":Lorg/w3c/dom/Element;
    .end local v8    # "xmlParser":Ljavax/xml/parsers/DocumentBuilder;
    :cond_1
    :goto_1
    move-object v9, v1

    .line 300
    :goto_2
    return-object v9

    .line 290
    .restart local v0    # "doc":Lorg/w3c/dom/Document;
    .restart local v2    # "errors":Lorg/w3c/dom/NodeList;
    .restart local v3    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v6    # "root":Lorg/w3c/dom/Element;
    .restart local v8    # "xmlParser":Ljavax/xml/parsers/DocumentBuilder;
    :cond_2
    new-instance v7, Lcom/vlingo/core/internal/util/ServerErrorUtil;

    invoke-direct {v7, v6}, Lcom/vlingo/core/internal/util/ServerErrorUtil;-><init>(Lorg/w3c/dom/Element;)V

    .line 291
    .local v7, "serverErrorUtil":Lcom/vlingo/core/internal/util/ServerErrorUtil;
    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ServerErrorUtil;->hasServerError()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 292
    sget-object v9, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Server error Code=\'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ServerErrorUtil;->getCode()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "\'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " Code="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ServerErrorUtil;->getCode()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 294
    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ServerErrorUtil;->logIt()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    const/4 v9, 0x0

    goto :goto_2

    .line 299
    .end local v0    # "doc":Lorg/w3c/dom/Document;
    .end local v2    # "errors":Lorg/w3c/dom/NodeList;
    .end local v3    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v6    # "root":Lorg/w3c/dom/Element;
    .end local v7    # "serverErrorUtil":Lcom/vlingo/core/internal/util/ServerErrorUtil;
    .end local v8    # "xmlParser":Ljavax/xml/parsers/DocumentBuilder;
    :catch_0
    move-exception v9

    goto :goto_1
.end method

.method public static getSample()Lcom/vlingo/core/internal/weather/WeatherElement;
    .locals 3

    .prologue
    .line 44
    new-instance v0, Lcom/vlingo/core/internal/weather/WeatherElement;

    const-string/jumbo v1, "WeatherResponse"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/weather/WeatherElement;-><init>(Ljava/lang/String;)V

    .line 45
    .local v0, "response":Lcom/vlingo/core/internal/weather/WeatherElement;
    const-string/jumbo v1, "<?xml version=\"1.0\" encoding=\"UTF-8\"?><WeatherResponse Location=\"?????, ?????\" Provider=\"KweatherContentProvider\"><CurrentCondition><CurrentCondition BadFeel=\"0.0\" DewPt=\"0.0\" FineDustConcentration=\"0.0\" Humidity=\"52.0\" ObservationTime=\"2012-09-14 17:54:00\" Pressure=\"1020.7\" RealFeel=\"27.3\" TempC=\"0.0\" TempF=\"0.0\" Temperature=\"26.7\" Vis=\"0.0\" Visibility=\"-1\" WeatherCode=\"2\" WeatherText=\"NA\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></CurrentCondition><Forecasts><Forecast ForecastDate=\"2012-09-14\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"-1\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"><DaytimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"25.5\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"16.4\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"1\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></DaytimeForecast><NighttimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"25.5\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"16.4\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"1\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></NighttimeForecast></Forecast><Forecast ForecastDate=\"2012-09-15\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"-1\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"><DaytimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"22.8\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"16.1\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"5\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></DaytimeForecast><NighttimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"22.8\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"16.1\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"5\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></NighttimeForecast></Forecast><Forecast ForecastDate=\"2012-09-16\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"-1\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"><DaytimeForecast><DayNighttimeForecast  IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"18.9\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"12.2\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"2\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></DaytimeForecast><NighttimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"18.9\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"12.2\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"2\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></NighttimeForecast></Forecast><Forecast ForecastDate=\"2012-09-17\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"-1\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"><DaytimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"22.8\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"12.2\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"2\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></DaytimeForecast><NighttimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"22.8\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"12.2\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"2\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></NighttimeForecast></Forecast><Forecast ForecastDate=\"2012-09-18\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"-1\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"><DaytimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"23.9\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"15.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"10\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></DaytimeForecast><NighttimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"23.9\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"15.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"10\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></NighttimeForecast></Forecast><Forecast ForecastDate=\"2012-09-19\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"-1\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"><DaytimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"23.3\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"17.2\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"10\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></DaytimeForecast><NighttimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"23.3\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"17.2\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"10\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></NighttimeForecast></Forecast><Forecast ForecastDate=\"2012-09-20\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"-1\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"><DaytimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"21.6\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"13.9\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"2\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></DaytimeForecast><NighttimeForecast><DayNighttimeForecast IceAmount=\"0.0\" PrecipAmount=\"0.0\" RainAmount=\"0.0\" RealFeelHigh=\"0.0\" RealFeelLow=\"0.0\" SnowAmount=\"0.0\" TStormProb=\"0.0\" TempMax=\"21.6\" TempMaxC=\"0.0\" TempMaxF=\"0.0\" TempMin=\"13.9\" TempMinC=\"0.0\" TempMinF=\"0.0\" WeatherCode=\"2\" WindGusts=\"0.0\" WindSpeed=\"0.0\" WindSpeedKmph=\"0.0\" WindSpeedMiles=\"0.0\"/></NighttimeForecast></Forecast></Forecasts><MoonPhases/><WeatherLocation><Location City=\"?????\" Country=\"?????\"/></WeatherLocation></WeatherResponse>"

    invoke-static {v0}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->setWeatherParser(Lcom/vlingo/core/internal/weather/WeatherElement;)Landroid/sax/RootElement;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->parse(Ljava/lang/String;Landroid/sax/RootElement;)V

    .line 46
    return-object v0
.end method

.method public static parse(Ljava/io/InputStream;)Lcom/vlingo/core/internal/weather/WeatherElement;
    .locals 2
    .param p0, "is"    # Ljava/io/InputStream;

    .prologue
    .line 38
    new-instance v0, Lcom/vlingo/core/internal/weather/WeatherElement;

    const-string/jumbo v1, "WeatherResponse"

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/weather/WeatherElement;-><init>(Ljava/lang/String;)V

    .line 39
    .local v0, "response":Lcom/vlingo/core/internal/weather/WeatherElement;
    invoke-static {v0}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->setWeatherParser(Lcom/vlingo/core/internal/weather/WeatherElement;)Landroid/sax/RootElement;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->parse(Ljava/io/InputStream;Landroid/sax/RootElement;Lcom/vlingo/core/internal/weather/WeatherElement;)V

    .line 40
    return-object v0
.end method

.method private static parse(Ljava/io/InputStream;Landroid/sax/RootElement;Lcom/vlingo/core/internal/weather/WeatherElement;)V
    .locals 5
    .param p0, "is"    # Ljava/io/InputStream;
    .param p1, "root"    # Landroid/sax/RootElement;
    .param p2, "response"    # Lcom/vlingo/core/internal/weather/WeatherElement;

    .prologue
    .line 237
    :try_start_0
    sget-object v2, Landroid/util/Xml$Encoding;->UTF_8:Landroid/util/Xml$Encoding;

    invoke-virtual {p1}, Landroid/sax/RootElement;->getContentHandler()Lorg/xml/sax/ContentHandler;

    move-result-object v3

    invoke-static {p0, v2, v3}, Landroid/util/Xml;->parse(Ljava/io/InputStream;Landroid/util/Xml$Encoding;Lorg/xml/sax/ContentHandler;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 248
    :goto_0
    return-void

    .line 238
    :catch_0
    move-exception v1

    .line 240
    .local v1, "exp":Ljava/lang/Exception;
    :try_start_1
    invoke-static {p0}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->getErrorMessage(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    .line 241
    .local v0, "errorMessage":Ljava/lang/String;
    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/weather/WeatherElement;->setErrorMessage(Ljava/lang/String;)V

    .line 242
    sget-object v2, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "WEATHER_XML_PARSE_EXCEPTION "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    sget-object v2, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "XML "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    sget-object v2, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 246
    .end local v0    # "errorMessage":Ljava/lang/String;
    :catch_1
    move-exception v2

    goto :goto_0
.end method

.method private static parse(Ljava/lang/String;Landroid/sax/RootElement;)V
    .locals 4
    .param p0, "xmlParam"    # Ljava/lang/String;
    .param p1, "root"    # Landroid/sax/RootElement;

    .prologue
    .line 252
    :try_start_0
    invoke-virtual {p1}, Landroid/sax/RootElement;->getContentHandler()Lorg/xml/sax/ContentHandler;

    move-result-object v1

    invoke-static {p0, v1}, Landroid/util/Xml;->parse(Ljava/lang/String;Lorg/xml/sax/ContentHandler;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 261
    :goto_0
    return-void

    .line 253
    :catch_0
    move-exception v0

    .line 255
    .local v0, "exp":Ljava/lang/Exception;
    :try_start_1
    sget-object v1, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "WEATHER_XML_PARSE_EXCEPTION "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    sget-object v1, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "XML "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 259
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method private static setAttribute(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/Element;)V
    .locals 1
    .param p0, "weatherElement"    # Lcom/vlingo/core/internal/weather/WeatherElement;
    .param p1, "element"    # Landroid/sax/Element;

    .prologue
    .line 265
    new-instance v0, Lcom/vlingo/core/internal/weather/WeatherAttributeListener;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/weather/WeatherAttributeListener;-><init>(Lcom/vlingo/core/internal/weather/WeatherElement;)V

    invoke-virtual {p1, v0}, Landroid/sax/Element;->setStartElementListener(Landroid/sax/StartElementListener;)V

    .line 267
    return-void
.end method

.method private static setWeatherParser(Lcom/vlingo/core/internal/weather/WeatherElement;)Landroid/sax/RootElement;
    .locals 2
    .param p0, "response"    # Lcom/vlingo/core/internal/weather/WeatherElement;

    .prologue
    .line 50
    new-instance v0, Landroid/sax/RootElement;

    const-string/jumbo v1, "WeatherResponse"

    invoke-direct {v0, v1}, Landroid/sax/RootElement;-><init>(Ljava/lang/String;)V

    .line 51
    .local v0, "root":Landroid/sax/RootElement;
    invoke-static {p0, v0}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->setAttribute(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/Element;)V

    .line 53
    invoke-static {p0, v0}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->createCurrentConditionsElement(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/RootElement;)V

    .line 54
    invoke-static {p0, v0}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->createForecastElement(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/RootElement;)V

    .line 55
    invoke-static {p0, v0}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->createImagesElement(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/RootElement;)V

    .line 56
    invoke-static {p0, v0}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->createMoonPhasesElement(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/RootElement;)V

    .line 57
    invoke-static {p0, v0}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->createUnitsElement(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/RootElement;)V

    .line 58
    invoke-static {p0, v0}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->createWeatherLocationElement(Lcom/vlingo/core/internal/weather/WeatherElement;Landroid/sax/RootElement;)V

    .line 60
    return-object v0
.end method
