.class public Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
.super Ljava/lang/Object;
.source "DialogFieldID.java"


# instance fields
.field private fieldId:Ljava/lang/String;

.field private timeNoSpeechInMillisecond:I

.field private timeWithSpeechInMillisecond:I


# direct methods
.method public constructor <init>(Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;I)V
    .locals 1
    .param p1, "fieldId"    # Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;
    .param p2, "endpointTimeWithSpeechMilliseconds"    # I

    .prologue
    .line 39
    invoke-interface {p1}, Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;-><init>(Ljava/lang/String;I)V

    .line 40
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .param p1, "fieldId"    # Ljava/lang/String;
    .param p2, "endpointTimeWithSpeechMilliseconds"    # I

    .prologue
    .line 35
    const-string/jumbo v0, "endpoint.time_withoutspeech"

    const/16 v1, 0x1388

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;-><init>(Ljava/lang/String;II)V

    .line 36
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p1, "fieldId"    # Ljava/lang/String;
    .param p2, "endpointTimeWithSpeechMilliseconds"    # I
    .param p3, "endpointTimeNoSpeechMilliseconds"    # I

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->fieldId:Ljava/lang/String;

    .line 30
    iput p2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->timeWithSpeechInMillisecond:I

    .line 31
    iput p3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->timeNoSpeechInMillisecond:I

    .line 32
    return-void
.end method

.method public static buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .locals 2
    .param p0, "fieldId"    # Ljava/lang/String;

    .prologue
    .line 80
    invoke-static {p0}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    .line 81
    .local v0, "toReturn":Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    if-eqz v0, :cond_0

    .line 86
    .end local v0    # "toReturn":Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    :goto_0
    return-object v0

    .restart local v0    # "toReturn":Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    :cond_0
    sget-object v1, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;->DEFAULT:Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    invoke-static {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    goto :goto_0
.end method

.method public static buildFromString(Ljava/lang/String;Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .locals 2
    .param p0, "fieldId"    # Ljava/lang/String;
    .param p1, "endpointTimeWithSpeech"    # Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    .prologue
    .line 71
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;->getEndpointTimeWithSpeechMilliseconds()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method private setFieldId(Ljava/lang/String;)V
    .locals 0
    .param p1, "fieldId"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->fieldId:Ljava/lang/String;

    .line 62
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 96
    if-ne p0, p1, :cond_0

    .line 97
    const/4 v0, 0x1

    .line 100
    .end local p1    # "obj":Ljava/lang/Object;
    :goto_0
    return v0

    .line 98
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 99
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 100
    :cond_2
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->fieldId:Ljava/lang/String;

    check-cast p1, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->getFieldId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/StringUtils;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public getFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->fieldId:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeNoSpeechInMillisecond()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->timeNoSpeechInMillisecond:I

    return v0
.end method

.method public getTimeWithSpeechInMilliseconds()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->timeWithSpeechInMillisecond:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->fieldId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->fieldId:Ljava/lang/String;

    return-object v0
.end method
