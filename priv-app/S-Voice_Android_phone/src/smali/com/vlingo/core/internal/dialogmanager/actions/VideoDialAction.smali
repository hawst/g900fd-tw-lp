.class public Lcom/vlingo/core/internal/dialogmanager/actions/VideoDialAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "VideoDialAction.java"


# instance fields
.field private address:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method


# virtual methods
.method public address(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/VideoDialAction;
    .locals 0
    .param p1, "addressParam"    # Ljava/lang/String;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/VideoDialAction;->address:Ljava/lang/String;

    .line 24
    return-object p0
.end method

.method protected execute()V
    .locals 2

    .prologue
    .line 29
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/VideoDialAction;->address:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/VideoDialAction;->address:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/DialUtil;->getVideoDialIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 31
    .local v0, "i":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 35
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/VideoDialAction;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 36
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/VideoDialAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 40
    .end local v0    # "i":Landroid/content/Intent;
    :goto_0
    return-void

    .line 38
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/VideoDialAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    goto :goto_0
.end method
