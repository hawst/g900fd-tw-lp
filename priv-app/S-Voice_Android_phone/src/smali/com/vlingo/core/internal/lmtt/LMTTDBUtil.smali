.class public final Lcom/vlingo/core/internal/lmtt/LMTTDBUtil;
.super Ljava/lang/Object;
.source "LMTTDBUtil.java"


# static fields
.field public static final COLUMN_HASHCODE:Ljava/lang/String; = "HASHCODE"

.field public static final COLUMN_ID:Ljava/lang/String; = "ID"

.field public static final COLUMN_TYPE:Ljava/lang/String; = "TYPE"

.field public static final LMTT_DB:Ljava/lang/String; = "LMTTv2"

.field public static final LMTT_TABLE:Ljava/lang/String; = "LMTT"

.field private static final TAG:Ljava/lang/String;

.field public static final WHERE_ID:Ljava/lang/String; = "ID=?"

.field public static final WHERE_TYPE:Ljava/lang/String; = "TYPE=?"

.field public static final WHERE_TYPE_ID:Ljava/lang/String; = "TYPE=? AND ID=?"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/vlingo/core/internal/lmtt/LMTTDBUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTDBUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clearLMTTTable(Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;)V
    .locals 6
    .param p0, "lmttItemType"    # Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    .prologue
    .line 65
    const/4 v0, 0x0

    .line 66
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x1

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->ordinal()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 68
    .local v2, "whereArgs":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/LMTTDBUtil;->openDB()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 69
    if-eqz v0, :cond_0

    .line 70
    const-string/jumbo v3, "LMTT"

    const-string/jumbo v4, "TYPE=?"

    invoke-virtual {v0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    :cond_0
    if-eqz v0, :cond_1

    .line 82
    :try_start_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 88
    :cond_1
    :goto_0
    return-void

    .line 83
    :catch_0
    move-exception v1

    .line 84
    .local v1, "ex":Ljava/lang/Exception;
    const-string/jumbo v3, "VLG_EXCEPTION"

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 73
    .end local v1    # "ex":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 80
    if-eqz v0, :cond_1

    .line 82
    :try_start_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 83
    :catch_2
    move-exception v1

    .line 84
    .restart local v1    # "ex":Ljava/lang/Exception;
    const-string/jumbo v3, "VLG_EXCEPTION"

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 80
    .end local v1    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_2

    .line 82
    :try_start_3
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 85
    :cond_2
    :goto_1
    throw v3

    .line 83
    :catch_3
    move-exception v1

    .line 84
    .restart local v1    # "ex":Ljava/lang/Exception;
    const-string/jumbo v4, "VLG_EXCEPTION"

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static deleteItem(Lcom/vlingo/core/internal/lmtt/LMTTItem;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p0, "item"    # Lcom/vlingo/core/internal/lmtt/LMTTItem;
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 214
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/vlingo/core/internal/lmtt/LMTTItem;->type:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v3, p0, Lcom/vlingo/core/internal/lmtt/LMTTItem;->uid:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 215
    .local v1, "whereArgs":[Ljava/lang/String;
    const-string/jumbo v2, "LMTT"

    const-string/jumbo v3, "TYPE=? AND ID=?"

    invoke-virtual {p1, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 216
    .local v0, "rowsAffected":I
    if-gtz v0, :cond_0

    .line 217
    sget-object v2, Lcom/vlingo/core/internal/lmtt/LMTTDBUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "DBUdb.delete returned "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    :cond_0
    return-void
.end method

.method public static getSongGenres(Landroid/content/ContentResolver;I)Ljava/lang/String;
    .locals 18
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "audioId"    # I

    .prologue
    .line 235
    sget-object v2, Landroid/provider/MediaStore$Audio$Genres;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 236
    .local v2, "genresUri":Landroid/net/Uri;
    const-string/jumbo v11, "members"

    .line 237
    .local v11, "contentdir":Ljava/lang/String;
    const-string/jumbo v9, "_id"

    .line 238
    .local v9, "audioGenreId":Ljava/lang/String;
    const-string/jumbo v15, "name"

    .line 239
    .local v15, "genreName":Ljava/lang/String;
    const-string/jumbo v17, "_id"

    .line 241
    .local v17, "mediaId":Ljava/lang/String;
    const/4 v10, 0x0

    .line 244
    .local v10, "c":Landroid/database/Cursor;
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 247
    .local v13, "genreIdMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x2

    :try_start_0
    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v9, v3, v1

    const/4 v1, 0x1

    aput-object v15, v3, v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v1, p0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 248
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_1

    .line 249
    const/4 v1, 0x0

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v13, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 252
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_0

    .line 253
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 254
    const/4 v10, 0x0

    :cond_0
    throw v1

    .line 252
    :cond_1
    if-eqz v10, :cond_2

    .line 253
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 254
    const/4 v10, 0x0

    .line 258
    :cond_2
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 260
    .local v14, "genreList":Ljava/lang/StringBuilder;
    invoke-virtual {v13}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 262
    .local v12, "genreId":Ljava/lang/String;
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 265
    .local v4, "uri":Landroid/net/Uri;
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v17, v5, v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "=?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v1, 0x1

    new-array v7, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v7, v1

    const/4 v8, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 266
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_4

    .line 267
    invoke-virtual {v13, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v3, 0x2c

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 270
    :cond_4
    if-eqz v10, :cond_3

    .line 271
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 272
    const/4 v10, 0x0

    goto :goto_1

    .line 270
    .end local v4    # "uri":Landroid/net/Uri;
    :catchall_1
    move-exception v1

    if-eqz v10, :cond_5

    .line 271
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 272
    const/4 v10, 0x0

    :cond_5
    throw v1

    .line 277
    .end local v12    # "genreId":Ljava/lang/String;
    :cond_6
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_7

    .line 278
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 279
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 282
    :goto_2
    return-object v1

    :cond_7
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public static getSynchedItems(Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;)Ljava/util/HashMap;
    .locals 15
    .param p0, "lmttItemType"    # Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    const/4 v0, 0x0

    .line 98
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v8, 0x0

    .line 99
    .local v8, "cur":Landroid/database/Cursor;
    const/4 v13, 0x0

    .line 101
    .local v13, "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/LMTTDBUtil;->openDB()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 102
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_c
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    .local v4, "whereArgs":[Ljava/lang/String;
    if-nez v0, :cond_2

    .line 141
    if-eqz v8, :cond_0

    .line 143
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 148
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 150
    :try_start_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    move-object v14, v13

    .line 156
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v13    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .local v14, "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :goto_2
    return-object v14

    .line 144
    .end local v14    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v13    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :catch_0
    move-exception v9

    .line 145
    .local v9, "ex":Ljava/lang/Exception;
    const-string/jumbo v1, "VLG_EXCEPTION"

    invoke-static {v9}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 151
    .end local v9    # "ex":Ljava/lang/Exception;
    :catch_1
    move-exception v9

    .line 152
    .restart local v9    # "ex":Ljava/lang/Exception;
    const-string/jumbo v1, "VLG_EXCEPTION"

    invoke-static {v9}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 107
    .end local v9    # "ex":Ljava/lang/Exception;
    :cond_2
    :try_start_3
    const-string/jumbo v1, "LMTT"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v5, "ID"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v5, "HASHCODE"

    aput-object v5, v2, v3

    const-string/jumbo v3, "TYPE=?"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_c
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v8

    .line 113
    if-nez v8, :cond_5

    .line 141
    if-eqz v8, :cond_3

    .line 143
    :try_start_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 148
    :cond_3
    :goto_3
    if-eqz v0, :cond_4

    .line 150
    :try_start_5
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    :cond_4
    :goto_4
    move-object v14, v13

    .line 153
    .end local v13    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .restart local v14    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    goto :goto_2

    .line 144
    .end local v14    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .restart local v13    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :catch_2
    move-exception v9

    .line 145
    .restart local v9    # "ex":Ljava/lang/Exception;
    const-string/jumbo v1, "VLG_EXCEPTION"

    invoke-static {v9}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 151
    .end local v9    # "ex":Ljava/lang/Exception;
    :catch_3
    move-exception v9

    .line 152
    .restart local v9    # "ex":Ljava/lang/Exception;
    const-string/jumbo v1, "VLG_EXCEPTION"

    invoke-static {v9}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 117
    .end local v9    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_6
    new-instance v14, Ljava/util/HashMap;

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-direct {v14, v1}, Ljava/util/HashMap;-><init>(I)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_c
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 119
    .end local v13    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .restart local v14    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :try_start_7
    const-string/jumbo v1, "ID"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 120
    .local v11, "idColumn":I
    const-string/jumbo v1, "HASHCODE"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 122
    .local v10, "hashColumn":I
    const/4 v12, 0x0

    .line 123
    .local v12, "num":I
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 125
    :cond_6
    invoke-interface {v8, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v14, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    add-int/lit8 v12, v12, 0x1

    .line 127
    rem-int/lit8 v1, v12, 0xa
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    if-nez v1, :cond_7

    .line 129
    const-wide/16 v1, 0xa

    :try_start_8
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 134
    :cond_7
    :goto_5
    :try_start_9
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_5
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result v1

    if-nez v1, :cond_6

    .line 141
    :cond_8
    if-eqz v8, :cond_9

    .line 143
    :try_start_a
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_7

    .line 148
    :cond_9
    :goto_6
    if-eqz v0, :cond_e

    .line 150
    :try_start_b
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_8

    move-object v13, v14

    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v10    # "hashColumn":I
    .end local v11    # "idColumn":I
    .end local v12    # "num":I
    .end local v14    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .restart local v13    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :cond_a
    :goto_7
    move-object v14, v13

    .line 156
    .end local v13    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .restart local v14    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    goto/16 :goto_2

    .line 130
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v10    # "hashColumn":I
    .restart local v11    # "idColumn":I
    .restart local v12    # "num":I
    :catch_4
    move-exception v9

    .line 131
    .restart local v9    # "ex":Ljava/lang/Exception;
    :try_start_c
    const-string/jumbo v1, "VLG_EXCEPTION"

    invoke-static {v9}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto :goto_5

    .line 137
    .end local v9    # "ex":Ljava/lang/Exception;
    .end local v10    # "hashColumn":I
    .end local v11    # "idColumn":I
    .end local v12    # "num":I
    :catch_5
    move-exception v9

    move-object v13, v14

    .line 138
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v14    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .restart local v9    # "ex":Ljava/lang/Exception;
    .restart local v13    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :goto_8
    :try_start_d
    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTDBUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 141
    if-eqz v8, :cond_b

    .line 143
    :try_start_e
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_9

    .line 148
    :cond_b
    :goto_9
    if-eqz v0, :cond_a

    .line 150
    :try_start_f
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_6

    goto :goto_7

    .line 151
    :catch_6
    move-exception v9

    .line 152
    const-string/jumbo v1, "VLG_EXCEPTION"

    invoke-static {v9}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 144
    .end local v9    # "ex":Ljava/lang/Exception;
    .end local v13    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v10    # "hashColumn":I
    .restart local v11    # "idColumn":I
    .restart local v12    # "num":I
    .restart local v14    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :catch_7
    move-exception v9

    .line 145
    .restart local v9    # "ex":Ljava/lang/Exception;
    const-string/jumbo v1, "VLG_EXCEPTION"

    invoke-static {v9}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 151
    .end local v9    # "ex":Ljava/lang/Exception;
    :catch_8
    move-exception v9

    .line 152
    .restart local v9    # "ex":Ljava/lang/Exception;
    const-string/jumbo v1, "VLG_EXCEPTION"

    invoke-static {v9}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v13, v14

    .line 153
    .end local v14    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .restart local v13    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    goto :goto_7

    .line 144
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v10    # "hashColumn":I
    .end local v11    # "idColumn":I
    .end local v12    # "num":I
    :catch_9
    move-exception v9

    .line 145
    const-string/jumbo v1, "VLG_EXCEPTION"

    invoke-static {v9}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 141
    .end local v9    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    :goto_a
    if-eqz v8, :cond_c

    .line 143
    :try_start_10
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_a

    .line 148
    :cond_c
    :goto_b
    if-eqz v0, :cond_d

    .line 150
    :try_start_11
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_b

    .line 153
    :cond_d
    :goto_c
    throw v1

    .line 144
    :catch_a
    move-exception v9

    .line 145
    .restart local v9    # "ex":Ljava/lang/Exception;
    const-string/jumbo v2, "VLG_EXCEPTION"

    invoke-static {v9}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    .line 151
    .end local v9    # "ex":Ljava/lang/Exception;
    :catch_b
    move-exception v9

    .line 152
    .restart local v9    # "ex":Ljava/lang/Exception;
    const-string/jumbo v2, "VLG_EXCEPTION"

    invoke-static {v9}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c

    .line 141
    .end local v9    # "ex":Ljava/lang/Exception;
    .end local v13    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v14    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :catchall_1
    move-exception v1

    move-object v13, v14

    .end local v14    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .restart local v13    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    goto :goto_a

    .line 137
    .end local v4    # "whereArgs":[Ljava/lang/String;
    :catch_c
    move-exception v9

    goto/16 :goto_8

    .end local v13    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v10    # "hashColumn":I
    .restart local v11    # "idColumn":I
    .restart local v12    # "num":I
    .restart local v14    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :cond_e
    move-object v13, v14

    .end local v14    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .restart local v13    # "table":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    goto/16 :goto_7
.end method

.method public static insertItem(Lcom/vlingo/core/internal/lmtt/LMTTItem;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 7
    .param p0, "item"    # Lcom/vlingo/core/internal/lmtt/LMTTItem;
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 201
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 202
    .local v3, "values":Landroid/content/ContentValues;
    iget-object v4, p0, Lcom/vlingo/core/internal/lmtt/LMTTItem;->type:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->ordinal()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 203
    .local v2, "typeValue":Ljava/lang/String;
    const-string/jumbo v4, "TYPE"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    const-string/jumbo v4, "ID"

    iget-wide v5, p0, Lcom/vlingo/core/internal/lmtt/LMTTItem;->uid:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 205
    const-string/jumbo v4, "HASHCODE"

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 206
    const-string/jumbo v4, "LMTT"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 207
    .local v0, "retValue":J
    const-wide/16 v4, -0x1

    cmp-long v4, v0, v4

    if-nez v4, :cond_0

    .line 208
    sget-object v4, Lcom/vlingo/core/internal/lmtt/LMTTDBUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "DBU db.insert returned "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    :cond_0
    return-void
.end method

.method private static openDB()Landroid/database/sqlite/SQLiteDatabase;
    .locals 6

    .prologue
    .line 45
    const/4 v1, 0x0

    .line 47
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 48
    .local v0, "context":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 49
    const-string/jumbo v3, "LMTTv2"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Context;->openOrCreateDatabase(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 52
    const-string/jumbo v3, "CREATE TABLE IF NOT EXISTS LMTT (TYPE INT, ID INT, HASHCODE INT)"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    .end local v0    # "context":Landroid/content/Context;
    :cond_0
    :goto_0
    return-object v1

    .line 54
    :catch_0
    move-exception v2

    .line 55
    .local v2, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/vlingo/core/internal/lmtt/LMTTDBUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "still unable to open db "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static updateItem(Lcom/vlingo/core/internal/lmtt/LMTTItem;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6
    .param p0, "item"    # Lcom/vlingo/core/internal/lmtt/LMTTItem;
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 223
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 224
    .local v1, "values":Landroid/content/ContentValues;
    const-string/jumbo v3, "HASHCODE"

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 225
    const/4 v3, 0x2

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/vlingo/core/internal/lmtt/LMTTItem;->type:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->ordinal()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/vlingo/core/internal/lmtt/LMTTItem;->uid:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 226
    .local v2, "whereArgs":[Ljava/lang/String;
    const-string/jumbo v3, "LMTT"

    const-string/jumbo v4, "TYPE=? AND ID=?"

    invoke-virtual {p1, v3, v1, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 227
    .local v0, "rowsAffected":I
    if-gtz v0, :cond_0

    .line 228
    sget-object v3, Lcom/vlingo/core/internal/lmtt/LMTTDBUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "DBU db.update returned "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    :cond_0
    return-void
.end method

.method public static updateSynchedItems(Ljava/util/List;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/lmtt/LMTTItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 166
    .local p0, "changedItems":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/lmtt/LMTTItem;>;"
    const/4 v0, 0x0

    .line 168
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/LMTTDBUtil;->openDB()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 169
    if-eqz v0, :cond_4

    .line 170
    const/4 v4, 0x0

    .line 171
    .local v4, "num":I
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/lmtt/LMTTItem;

    .line 172
    .local v3, "item":Lcom/vlingo/core/internal/lmtt/LMTTItem;
    iget-object v5, v3, Lcom/vlingo/core/internal/lmtt/LMTTItem;->changeType:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    invoke-virtual {v5, v3, v0}, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->dbUpdate(Lcom/vlingo/core/internal/lmtt/LMTTItem;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 173
    add-int/lit8 v4, v4, 0x1

    .line 174
    rem-int/lit8 v5, v4, 0xa
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v5, :cond_0

    .line 176
    const-wide/16 v5, 0xa

    :try_start_1
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 177
    :catch_0
    move-exception v1

    .line 178
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string/jumbo v5, "VLG_EXCEPTION"

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 185
    .end local v1    # "ex":Ljava/lang/Exception;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "item":Lcom/vlingo/core/internal/lmtt/LMTTItem;
    .end local v4    # "num":I
    :catch_1
    move-exception v1

    .line 186
    .restart local v1    # "ex":Ljava/lang/Exception;
    :try_start_3
    sget-object v5, Lcom/vlingo/core/internal/lmtt/LMTTDBUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 189
    if-eqz v0, :cond_1

    .line 191
    :try_start_4
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 197
    .end local v1    # "ex":Ljava/lang/Exception;
    :cond_1
    :goto_1
    const/4 v5, 0x0

    :cond_2
    :goto_2
    return v5

    .line 182
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v4    # "num":I
    :cond_3
    const/4 v5, 0x1

    .line 189
    if-eqz v0, :cond_2

    .line 191
    :try_start_5
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2

    .line 192
    :catch_2
    move-exception v1

    .line 193
    .restart local v1    # "ex":Ljava/lang/Exception;
    const-string/jumbo v6, "VLG_EXCEPTION"

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 189
    .end local v1    # "ex":Ljava/lang/Exception;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "num":I
    :cond_4
    if-eqz v0, :cond_1

    .line 191
    :try_start_6
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_1

    .line 192
    :catch_3
    move-exception v1

    .line 193
    .restart local v1    # "ex":Ljava/lang/Exception;
    const-string/jumbo v5, "VLG_EXCEPTION"

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 192
    :catch_4
    move-exception v1

    .line 193
    const-string/jumbo v5, "VLG_EXCEPTION"

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 189
    .end local v1    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    if-eqz v0, :cond_5

    .line 191
    :try_start_7
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    .line 194
    :cond_5
    :goto_3
    throw v5

    .line 192
    :catch_5
    move-exception v1

    .line 193
    .restart local v1    # "ex":Ljava/lang/Exception;
    const-string/jumbo v6, "VLG_EXCEPTION"

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method
