.class public Lcom/vlingo/core/internal/util/CoreServerInfoUtils;
.super Ljava/lang/Object;
.source "CoreServerInfoUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/util/CoreServerInfoUtils$1;,
        Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;
    }
.end annotation


# static fields
.field private static final SCHEME_HTTP:Ljava/lang/String; = "http://"

.field private static final SCHEME_HTTPS:Ljava/lang/String; = "https://"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAsrUri(Lcom/vlingo/core/internal/util/CoreServerInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "serverInfo"    # Lcom/vlingo/core/internal/util/CoreServerInfo;

    .prologue
    .line 24
    sget-object v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->ASR:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils;->getUri(Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;Lcom/vlingo/core/internal/util/CoreServerInfo;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getHelloUri(Lcom/vlingo/core/internal/util/CoreServerInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "serverInfo"    # Lcom/vlingo/core/internal/util/CoreServerInfo;

    .prologue
    .line 44
    sget-object v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->HELLO:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils;->getUri(Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;Lcom/vlingo/core/internal/util/CoreServerInfo;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLMTTUri(Lcom/vlingo/core/internal/util/CoreServerInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "serverInfo"    # Lcom/vlingo/core/internal/util/CoreServerInfo;

    .prologue
    .line 40
    sget-object v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->LMTT:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils;->getUri(Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;Lcom/vlingo/core/internal/util/CoreServerInfo;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLogUri(Lcom/vlingo/core/internal/util/CoreServerInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "serverInfo"    # Lcom/vlingo/core/internal/util/CoreServerInfo;

    .prologue
    .line 36
    sget-object v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->LOG:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils;->getUri(Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;Lcom/vlingo/core/internal/util/CoreServerInfo;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getTtsUri(Lcom/vlingo/core/internal/util/CoreServerInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "serverInfo"    # Lcom/vlingo/core/internal/util/CoreServerInfo;

    .prologue
    .line 28
    sget-object v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->TTS:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils;->getUri(Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;Lcom/vlingo/core/internal/util/CoreServerInfo;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getUri(Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;Lcom/vlingo/core/internal/util/CoreServerInfo;)Ljava/lang/String;
    .locals 8
    .param p0, "type"    # Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;
    .param p1, "serverInfo"    # Lcom/vlingo/core/internal/util/CoreServerInfo;

    .prologue
    const/4 v5, 0x1

    .line 49
    const-string/jumbo v4, "http://"

    .line 50
    .local v4, "scheme":Ljava/lang/String;
    const/4 v1, 0x0

    .line 51
    .local v1, "host":Ljava/lang/String;
    const/4 v2, 0x0

    .line 52
    .local v2, "key":Ljava/lang/String;
    sget-object v6, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$1;->$SwitchMap$com$vlingo$core$internal$util$CoreServerInfoUtils$ServerType:[I

    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 80
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils;->isUserInRolloutGroup()Z

    move-result v0

    .line 81
    .local v0, "allowHttps":Z
    if-eqz v0, :cond_1

    invoke-static {v2, v5}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 82
    .local v5, "usingHttps":Z
    :goto_1
    if-eqz v5, :cond_0

    .line 83
    const-string/jumbo v4, "https://"

    .line 86
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 87
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 55
    .end local v0    # "allowHttps":Z
    .end local v3    # "sb":Ljava/lang/StringBuilder;
    .end local v5    # "usingHttps":Z
    :pswitch_0
    const-string/jumbo v2, "https.asr_enabled"

    .line 56
    invoke-interface {p1}, Lcom/vlingo/core/internal/util/CoreServerInfo;->getASRHost()Ljava/lang/String;

    move-result-object v1

    .line 57
    goto :goto_0

    .line 59
    :pswitch_1
    const-string/jumbo v2, "https.tts_enabled"

    .line 60
    invoke-interface {p1}, Lcom/vlingo/core/internal/util/CoreServerInfo;->getTTSHost()Ljava/lang/String;

    move-result-object v1

    .line 61
    goto :goto_0

    .line 63
    :pswitch_2
    const-string/jumbo v2, "https.vcs_enabled"

    .line 64
    invoke-interface {p1}, Lcom/vlingo/core/internal/util/CoreServerInfo;->getVCSHost()Ljava/lang/String;

    move-result-object v1

    .line 65
    goto :goto_0

    .line 67
    :pswitch_3
    const-string/jumbo v2, "https.log_enabled"

    .line 68
    invoke-interface {p1}, Lcom/vlingo/core/internal/util/CoreServerInfo;->getLogHost()Ljava/lang/String;

    move-result-object v1

    .line 69
    goto :goto_0

    .line 71
    :pswitch_4
    const-string/jumbo v2, "https.lmtt_enabled"

    .line 72
    invoke-interface {p1}, Lcom/vlingo/core/internal/util/CoreServerInfo;->getLMTTHost()Ljava/lang/String;

    move-result-object v1

    .line 73
    goto :goto_0

    .line 75
    :pswitch_5
    const-string/jumbo v2, "https.hello_enabled"

    .line 76
    invoke-interface {p1}, Lcom/vlingo/core/internal/util/CoreServerInfo;->getHelloHost()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 81
    .restart local v0    # "allowHttps":Z
    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    .line 52
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static getVcsUri(Lcom/vlingo/core/internal/util/CoreServerInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "serverInfo"    # Lcom/vlingo/core/internal/util/CoreServerInfo;

    .prologue
    .line 32
    sget-object v0, Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;->VCS:Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils;->getUri(Lcom/vlingo/core/internal/util/CoreServerInfoUtils$ServerType;Lcom/vlingo/core/internal/util/CoreServerInfo;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static isUserInRolloutGroup()Z
    .locals 6

    .prologue
    const/16 v5, 0x64

    .line 99
    const-string/jumbo v2, "https.rollout_groupid"

    const/4 v3, -0x1

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 100
    .local v0, "rolloutGroupId":I
    if-gez v0, :cond_0

    .line 101
    new-instance v2, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/util/Random;-><init>(J)V

    invoke-virtual {v2, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 102
    const-string/jumbo v2, "https.rollout_groupid"

    invoke-static {v2, v0}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 105
    :cond_0
    const-string/jumbo v2, "https.rollout_percentage"

    invoke-static {v2, v5}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 110
    .local v1, "rolloutPercentage":I
    if-ge v0, v1, :cond_1

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
