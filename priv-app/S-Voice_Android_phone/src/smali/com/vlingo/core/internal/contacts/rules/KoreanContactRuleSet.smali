.class public Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;
.super Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;
.source "KoreanContactRuleSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$1;,
        Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;
    }
.end annotation


# static fields
.field public static final COMMON_TITLE_PATTERN:Ljava/lang/String; = "\uad50\uc218|\uc0ac\uc6d0|\ub300\ub9ac|\uacfc\uc7a5|\ucc28\uc7a5|\ubd80\uc7a5|\uc120\uc784|\ucc45\uc784|\uc218\uc11d|\uc774\uc0ac|\uc0c1\ubb34|\uc804\ubb34|\ubd80\uc0ac\uc7a5|\uc0ac\uc7a5|\ub300\ud45c\uc774\uc0ac\ubd80\ud68c\uc7a5|\ud68c\uc7a5|\uc9c0\uc0ac\uc7a5|\uac10\uc0ac|\uace0\ubb38|\uad50\uc218\ub2d8|\uc790\ubb38|\uc8fc\uc784|\uc0ac\uc5c5\ubd80\uc7a5|\ubcf8\ubd80\uc7a5|\ubd80\ubcf8\ubd80\uc7a5|\uad00\uc7a5|\uad6d\uc7a5|\uc18c\uc7a5|\uc9c0\uc810\uc7a5|\uc2e4\uc7a5|\ud300\uc7a5|\uacc4\uc7a5|\uc804\ubb38|\ud504\ub85c|\uac10\ub3c5|\ucf54\uce58|\uc0ac\uc6d0\ub2d8|\ub300\ub9ac\ub2d8|\uacfc\uc7a5\ub2d8|\ucc28\uc7a5\ub2d8|\ubd80\uc7a5\ub2d8|\uc120\uc784\ub2d8|\ucc45\uc784\ub2d8|\uc218\uc11d\ub2d8|\uc774\uc0ac\ub2d8|\uc0c1\ubb34\ub2d8|\uc804\ubb34\ub2d8|\ubd80\uc0ac\uc7a5\ub2d8|\uc0ac\uc7a5\ub2d8|\ubd80\ud68c\uc7a5\ub2d8|\ud68c\uc7a5\ub2d8|\uc9c0\uc0ac\uc7a5\ub2d8|\uac10\uc0ac\ub2d8|\uace0\ubb38\ub2d8|\uc790\ubb38\ub2d8|\uc0ac\uc5c5\ubd80\uc7a5\ub2d8|\ubcf8\ubd80\uc7a5\ub2d8|\ubd80\ubcf8\ubd80\uc7a5\ub2d8|\uad00\uc7a5\ub2d8|\uad6d\uc7a5\ub2d8|\uc18c\uc7a5\ub2d8|\uc9c0\uc810\uc7a5\ub2d8|\uc2e4\uc7a5\ub2d8|\ud300\uc7a5\ub2d8|\uacc4\uc7a5\ub2d8|\uc8fc\uc784\ub2d8|\uc804\ubb38\ub2d8|\ud504\ub85c\ub2d8|\uac10\ub3c5\ub2d8|\ucf54\uce58\ub2d8|\uc120\uc0dd\ub2d8|\ub300\ud45c|\ud615|\uc5b8\ub2c8|\ub204\ub098|\uc624\ube60"

.field public static final FAMILIARITY_LIST:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final FAMILIARITY_PATTERN:Ljava/lang/String; = "\uc774"

.field public static final HONORIFIC_PATTERN:Ljava/lang/String; = "\uc591|\uad70|\ub2d8|\uc528|\ub204\ub098|\ud615|\uc120\ubc30"

.field public static final MULTI_POST_PROPOSITION_PATTERN:Ljava/lang/String; = "\uc73c\ub85c\ubd80\ud130|\uc73c\ub85c|\ub85c\ubd80\ud130|\uc5d0\uac8c\ub3c4|\uc5d0\uac8c\uc11c|\ud55c\ud14c\uc11c|\ud55c\ud14c\ub3c4|\ub354\ub7ec|\ud558\uace0|\uc5d0\uac8c|\uc5d0\uc11c"

.field public static final POST_POSITION_PATTERN_REGEXP:Ljava/util/regex/Pattern;

.field public static final POST_PROPOSITION_PATTERN:Ljava/lang/String; = "\uacfc|\uc744|\uc774|\ub85c|\ub791|\ub97c|\uac00|\uc640|\uc758|\uc5d0|\ub3c4|\uc778|\uaed8|\uc57c"

.field public static final TRIALING_EXTRAS:Ljava/util/regex/Pattern;

.field private static commonTitles:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 36
    const-string/jumbo v0, "^(.{2,}?)(\\s*(\uad50\uc218|\uc0ac\uc6d0|\ub300\ub9ac|\uacfc\uc7a5|\ucc28\uc7a5|\ubd80\uc7a5|\uc120\uc784|\ucc45\uc784|\uc218\uc11d|\uc774\uc0ac|\uc0c1\ubb34|\uc804\ubb34|\ubd80\uc0ac\uc7a5|\uc0ac\uc7a5|\ub300\ud45c\uc774\uc0ac\ubd80\ud68c\uc7a5|\ud68c\uc7a5|\uc9c0\uc0ac\uc7a5|\uac10\uc0ac|\uace0\ubb38|\uad50\uc218\ub2d8|\uc790\ubb38|\uc8fc\uc784|\uc0ac\uc5c5\ubd80\uc7a5|\ubcf8\ubd80\uc7a5|\ubd80\ubcf8\ubd80\uc7a5|\uad00\uc7a5|\uad6d\uc7a5|\uc18c\uc7a5|\uc9c0\uc810\uc7a5|\uc2e4\uc7a5|\ud300\uc7a5|\uacc4\uc7a5|\uc804\ubb38|\ud504\ub85c|\uac10\ub3c5|\ucf54\uce58|\uc0ac\uc6d0\ub2d8|\ub300\ub9ac\ub2d8|\uacfc\uc7a5\ub2d8|\ucc28\uc7a5\ub2d8|\ubd80\uc7a5\ub2d8|\uc120\uc784\ub2d8|\ucc45\uc784\ub2d8|\uc218\uc11d\ub2d8|\uc774\uc0ac\ub2d8|\uc0c1\ubb34\ub2d8|\uc804\ubb34\ub2d8|\ubd80\uc0ac\uc7a5\ub2d8|\uc0ac\uc7a5\ub2d8|\ubd80\ud68c\uc7a5\ub2d8|\ud68c\uc7a5\ub2d8|\uc9c0\uc0ac\uc7a5\ub2d8|\uac10\uc0ac\ub2d8|\uace0\ubb38\ub2d8|\uc790\ubb38\ub2d8|\uc0ac\uc5c5\ubd80\uc7a5\ub2d8|\ubcf8\ubd80\uc7a5\ub2d8|\ubd80\ubcf8\ubd80\uc7a5\ub2d8|\uad00\uc7a5\ub2d8|\uad6d\uc7a5\ub2d8|\uc18c\uc7a5\ub2d8|\uc9c0\uc810\uc7a5\ub2d8|\uc2e4\uc7a5\ub2d8|\ud300\uc7a5\ub2d8|\uacc4\uc7a5\ub2d8|\uc8fc\uc784\ub2d8|\uc804\ubb38\ub2d8|\ud504\ub85c\ub2d8|\uac10\ub3c5\ub2d8|\ucf54\uce58\ub2d8|\uc120\uc0dd\ub2d8|\ub300\ud45c|\ud615|\uc5b8\ub2c8|\ub204\ub098|\uc624\ube60)*(\uc774|\uc591|\uad70|\ub2d8|\uc528|\ub204\ub098|\ud615|\uc120\ubc30)?(\uacfc|\uc744|\uc774|\ub85c|\ub791|\ub97c|\uac00|\uc640|\uc758|\uc5d0|\ub3c4|\uc778|\uaed8|\uc57c|\uc73c\ub85c\ubd80\ud130|\uc73c\ub85c|\ub85c\ubd80\ud130|\uc5d0\uac8c\ub3c4|\uc5d0\uac8c\uc11c|\ud55c\ud14c\uc11c|\ud55c\ud14c\ub3c4|\ub354\ub7ec|\ud558\uace0|\uc5d0\uac8c|\uc5d0\uc11c)?)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->TRIALING_EXTRAS:Ljava/util/regex/Pattern;

    .line 40
    const-string/jumbo v0, "(\uacfc|\uc744|\uc774|\ub85c|\ub791|\ub97c|\uac00|\uc640|\uc758|\uc5d0|\ub3c4|\uc778|\uaed8|\uc57c)?$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->POST_POSITION_PATTERN_REGEXP:Ljava/util/regex/Pattern;

    .line 48
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x44

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "\uc0ac\uc6d0"

    aput-object v2, v1, v4

    const-string/jumbo v2, "\ub300\ub9ac"

    aput-object v2, v1, v5

    const-string/jumbo v2, "\uacfc\uc7a5"

    aput-object v2, v1, v6

    const-string/jumbo v2, "\ucc28\uc7a5"

    aput-object v2, v1, v7

    const-string/jumbo v2, "\ubd80\uc7a5"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string/jumbo v3, "\uc120\uc784"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "\ucc45\uc784"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string/jumbo v3, "\uc218\uc11d"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string/jumbo v3, "\uc774\uc0ac"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string/jumbo v3, "\uc0c1\ubb34"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string/jumbo v3, "\uc804\ubb34"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string/jumbo v3, "\ubd80\uc0ac\uc7a5"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string/jumbo v3, "\uc0ac\uc7a5"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string/jumbo v3, "\ubd80\ud68c\uc7a5"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string/jumbo v3, "\ud68c\uc7a5"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string/jumbo v3, "\uc9c0\uc0ac\uc7a5"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string/jumbo v3, "\uac10\uc0ac"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string/jumbo v3, "\uace0\ubb38"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string/jumbo v3, "\uc790\ubb38"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string/jumbo v3, "\uc8fc\uc784"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string/jumbo v3, "\uc0ac\uc5c5\ubd80\uc7a5"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string/jumbo v3, "\ubcf8\ubd80\uc7a5"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-string/jumbo v3, "\ubd80\ubcf8\ubd80\uc7a5"

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string/jumbo v3, "\uad00\uc7a5"

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-string/jumbo v3, "\uad6d\uc7a5"

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const-string/jumbo v3, "\uc18c\uc7a5"

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const-string/jumbo v3, "\uc9c0\uc810\uc7a5"

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    const-string/jumbo v3, "\uc2e4\uc7a5"

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    const-string/jumbo v3, "\ud300\uc7a5"

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    const-string/jumbo v3, "\uacc4\uc7a5"

    aput-object v3, v1, v2

    const/16 v2, 0x1e

    const-string/jumbo v3, "\uc804\ubb38"

    aput-object v3, v1, v2

    const/16 v2, 0x1f

    const-string/jumbo v3, "\ud504\ub85c"

    aput-object v3, v1, v2

    const/16 v2, 0x20

    const-string/jumbo v3, "\uac10\ub3c5"

    aput-object v3, v1, v2

    const/16 v2, 0x21

    const-string/jumbo v3, "\ucf54\uce58"

    aput-object v3, v1, v2

    const/16 v2, 0x22

    const-string/jumbo v3, "\uc0ac\uc6d0\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x23

    const-string/jumbo v3, "\ub300\ub9ac\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x24

    const-string/jumbo v3, "\uacfc\uc7a5\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x25

    const-string/jumbo v3, "\ucc28\uc7a5\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x26

    const-string/jumbo v3, "\ubd80\uc7a5\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x27

    const-string/jumbo v3, "\uc120\uc784\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x28

    const-string/jumbo v3, "\ucc45\uc784\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x29

    const-string/jumbo v3, "\uc218\uc11d\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x2a

    const-string/jumbo v3, "\uc774\uc0ac\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x2b

    const-string/jumbo v3, "\uc0c1\ubb34\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x2c

    const-string/jumbo v3, "\uc804\ubb34\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x2d

    const-string/jumbo v3, "\ubd80\uc0ac\uc7a5\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x2e

    const-string/jumbo v3, "\uc0ac\uc7a5\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x2f

    const-string/jumbo v3, "\ubd80\ud68c\uc7a5\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x30

    const-string/jumbo v3, "\ud68c\uc7a5\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x31

    const-string/jumbo v3, "\uc9c0\uc0ac\uc7a5\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x32

    const-string/jumbo v3, "\uac10\uc0ac\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x33

    const-string/jumbo v3, "\uace0\ubb38\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x34

    const-string/jumbo v3, "\uc790\ubb38\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x35

    const-string/jumbo v3, "\uc0ac\uc5c5\ubd80\uc7a5\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x36

    const-string/jumbo v3, "\ubcf8\ubd80\uc7a5\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x37

    const-string/jumbo v3, "\ubd80\ubcf8\ubd80\uc7a5\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x38

    const-string/jumbo v3, "\uad00\uc7a5\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x39

    const-string/jumbo v3, "\uad6d\uc7a5\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x3a

    const-string/jumbo v3, "\uc18c\uc7a5\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x3b

    const-string/jumbo v3, "\uc9c0\uc810\uc7a5\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x3c

    const-string/jumbo v3, "\uc2e4\uc7a5\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x3d

    const-string/jumbo v3, "\ud300\uc7a5\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x3e

    const-string/jumbo v3, "\uacc4\uc7a5\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x3f

    const-string/jumbo v3, "\uc8fc\uc784\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x40

    const-string/jumbo v3, "\uc804\ubb38\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x41

    const-string/jumbo v3, "\ud504\ub85c\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x42

    const-string/jumbo v3, "\uac10\ub3c5\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0x43

    const-string/jumbo v3, "\ucf54\uce58\ub2d8"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->commonTitles:Ljava/util/HashSet;

    .line 121
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x11

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "\uacfc"

    aput-object v2, v1, v4

    const-string/jumbo v2, "\ub791"

    aput-object v2, v1, v5

    const-string/jumbo v2, "\uc774"

    aput-object v2, v1, v6

    const-string/jumbo v2, "\ub97c"

    aput-object v2, v1, v7

    const-string/jumbo v2, "\uc744"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string/jumbo v3, "\uac00"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "\uc528"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string/jumbo v3, "\uc640"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string/jumbo v3, "\uc758"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string/jumbo v3, "\uc5d0"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string/jumbo v3, "\uc591"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string/jumbo v3, "\ub85c"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string/jumbo v3, "\uad70"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string/jumbo v3, "\ub3c4"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string/jumbo v3, "\ub2d8"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string/jumbo v3, "\uc544"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string/jumbo v3, "\uc57c"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->FAMILIARITY_LIST:Ljava/util/HashSet;

    .line 140
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 156
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;-><init>()V

    .line 158
    return-void
.end method

.method private createContactPattern(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "contactPatternName"    # Ljava/lang/String;

    .prologue
    .line 488
    sget-object v0, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->SHOULD:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    sget-object v1, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->SHOULD:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    sget-object v2, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->SHOULD:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->createContactPattern(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private createContactPattern(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;)Ljava/lang/String;
    .locals 3
    .param p1, "contactPatternName"    # Ljava/lang/String;
    .param p2, "title"    # Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;
    .param p3, "familyHonorfix"    # Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;
    .param p4, "postposition"    # Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    .prologue
    .line 436
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 437
    .local v0, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "^("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")?(\\s*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 439
    sget-object v1, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$1;->$SwitchMap$com$vlingo$core$internal$contacts$rules$KoreanContactRuleSet$QueryRelation:[I

    invoke-virtual {p2}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 453
    :goto_0
    sget-object v1, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$1;->$SwitchMap$com$vlingo$core$internal$contacts$rules$KoreanContactRuleSet$QueryRelation:[I

    invoke-virtual {p3}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 467
    :goto_1
    sget-object v1, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$1;->$SwitchMap$com$vlingo$core$internal$contacts$rules$KoreanContactRuleSet$QueryRelation:[I

    invoke-virtual {p4}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_2

    .line 481
    :goto_2
    const-string/jumbo v1, ")$"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 483
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 441
    :pswitch_0
    const-string/jumbo v1, "(\uad50\uc218|\uc0ac\uc6d0|\ub300\ub9ac|\uacfc\uc7a5|\ucc28\uc7a5|\ubd80\uc7a5|\uc120\uc784|\ucc45\uc784|\uc218\uc11d|\uc774\uc0ac|\uc0c1\ubb34|\uc804\ubb34|\ubd80\uc0ac\uc7a5|\uc0ac\uc7a5|\ub300\ud45c\uc774\uc0ac\ubd80\ud68c\uc7a5|\ud68c\uc7a5|\uc9c0\uc0ac\uc7a5|\uac10\uc0ac|\uace0\ubb38|\uad50\uc218\ub2d8|\uc790\ubb38|\uc8fc\uc784|\uc0ac\uc5c5\ubd80\uc7a5|\ubcf8\ubd80\uc7a5|\ubd80\ubcf8\ubd80\uc7a5|\uad00\uc7a5|\uad6d\uc7a5|\uc18c\uc7a5|\uc9c0\uc810\uc7a5|\uc2e4\uc7a5|\ud300\uc7a5|\uacc4\uc7a5|\uc804\ubb38|\ud504\ub85c|\uac10\ub3c5|\ucf54\uce58|\uc0ac\uc6d0\ub2d8|\ub300\ub9ac\ub2d8|\uacfc\uc7a5\ub2d8|\ucc28\uc7a5\ub2d8|\ubd80\uc7a5\ub2d8|\uc120\uc784\ub2d8|\ucc45\uc784\ub2d8|\uc218\uc11d\ub2d8|\uc774\uc0ac\ub2d8|\uc0c1\ubb34\ub2d8|\uc804\ubb34\ub2d8|\ubd80\uc0ac\uc7a5\ub2d8|\uc0ac\uc7a5\ub2d8|\ubd80\ud68c\uc7a5\ub2d8|\ud68c\uc7a5\ub2d8|\uc9c0\uc0ac\uc7a5\ub2d8|\uac10\uc0ac\ub2d8|\uace0\ubb38\ub2d8|\uc790\ubb38\ub2d8|\uc0ac\uc5c5\ubd80\uc7a5\ub2d8|\ubcf8\ubd80\uc7a5\ub2d8|\ubd80\ubcf8\ubd80\uc7a5\ub2d8|\uad00\uc7a5\ub2d8|\uad6d\uc7a5\ub2d8|\uc18c\uc7a5\ub2d8|\uc9c0\uc810\uc7a5\ub2d8|\uc2e4\uc7a5\ub2d8|\ud300\uc7a5\ub2d8|\uacc4\uc7a5\ub2d8|\uc8fc\uc784\ub2d8|\uc804\ubb38\ub2d8|\ud504\ub85c\ub2d8|\uac10\ub3c5\ub2d8|\ucf54\uce58\ub2d8|\uc120\uc0dd\ub2d8|\ub300\ud45c|\ud615|\uc5b8\ub2c8|\ub204\ub098|\uc624\ube60)+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 444
    :pswitch_1
    const-string/jumbo v1, "(\uad50\uc218|\uc0ac\uc6d0|\ub300\ub9ac|\uacfc\uc7a5|\ucc28\uc7a5|\ubd80\uc7a5|\uc120\uc784|\ucc45\uc784|\uc218\uc11d|\uc774\uc0ac|\uc0c1\ubb34|\uc804\ubb34|\ubd80\uc0ac\uc7a5|\uc0ac\uc7a5|\ub300\ud45c\uc774\uc0ac\ubd80\ud68c\uc7a5|\ud68c\uc7a5|\uc9c0\uc0ac\uc7a5|\uac10\uc0ac|\uace0\ubb38|\uad50\uc218\ub2d8|\uc790\ubb38|\uc8fc\uc784|\uc0ac\uc5c5\ubd80\uc7a5|\ubcf8\ubd80\uc7a5|\ubd80\ubcf8\ubd80\uc7a5|\uad00\uc7a5|\uad6d\uc7a5|\uc18c\uc7a5|\uc9c0\uc810\uc7a5|\uc2e4\uc7a5|\ud300\uc7a5|\uacc4\uc7a5|\uc804\ubb38|\ud504\ub85c|\uac10\ub3c5|\ucf54\uce58|\uc0ac\uc6d0\ub2d8|\ub300\ub9ac\ub2d8|\uacfc\uc7a5\ub2d8|\ucc28\uc7a5\ub2d8|\ubd80\uc7a5\ub2d8|\uc120\uc784\ub2d8|\ucc45\uc784\ub2d8|\uc218\uc11d\ub2d8|\uc774\uc0ac\ub2d8|\uc0c1\ubb34\ub2d8|\uc804\ubb34\ub2d8|\ubd80\uc0ac\uc7a5\ub2d8|\uc0ac\uc7a5\ub2d8|\ubd80\ud68c\uc7a5\ub2d8|\ud68c\uc7a5\ub2d8|\uc9c0\uc0ac\uc7a5\ub2d8|\uac10\uc0ac\ub2d8|\uace0\ubb38\ub2d8|\uc790\ubb38\ub2d8|\uc0ac\uc5c5\ubd80\uc7a5\ub2d8|\ubcf8\ubd80\uc7a5\ub2d8|\ubd80\ubcf8\ubd80\uc7a5\ub2d8|\uad00\uc7a5\ub2d8|\uad6d\uc7a5\ub2d8|\uc18c\uc7a5\ub2d8|\uc9c0\uc810\uc7a5\ub2d8|\uc2e4\uc7a5\ub2d8|\ud300\uc7a5\ub2d8|\uacc4\uc7a5\ub2d8|\uc8fc\uc784\ub2d8|\uc804\ubb38\ub2d8|\ud504\ub85c\ub2d8|\uac10\ub3c5\ub2d8|\ucf54\uce58\ub2d8|\uc120\uc0dd\ub2d8|\ub300\ud45c|\ud615|\uc5b8\ub2c8|\ub204\ub098|\uc624\ube60)*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 447
    :pswitch_2
    const-string/jumbo v1, "()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 455
    :pswitch_3
    const-string/jumbo v1, "(\uc774|\uc591|\uad70|\ub2d8|\uc528|\ub204\ub098|\ud615|\uc120\ubc30){1}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 458
    :pswitch_4
    const-string/jumbo v1, "(\uc774|\uc591|\uad70|\ub2d8|\uc528|\ub204\ub098|\ud615|\uc120\ubc30)?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 461
    :pswitch_5
    const-string/jumbo v1, "()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 469
    :pswitch_6
    const-string/jumbo v1, "(\uacfc|\uc744|\uc774|\ub85c|\ub791|\ub97c|\uac00|\uc640|\uc758|\uc5d0|\ub3c4|\uc778|\uaed8|\uc57c|\uc73c\ub85c\ubd80\ud130|\uc73c\ub85c|\ub85c\ubd80\ud130|\uc5d0\uac8c\ub3c4|\uc5d0\uac8c\uc11c|\ud55c\ud14c\uc11c|\ud55c\ud14c\ub3c4|\ub354\ub7ec|\ud558\uace0|\uc5d0\uac8c|\uc5d0\uc11c){1}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 472
    :pswitch_7
    const-string/jumbo v1, "(\uacfc|\uc744|\uc774|\ub85c|\ub791|\ub97c|\uac00|\uc640|\uc758|\uc5d0|\ub3c4|\uc778|\uaed8|\uc57c|\uc73c\ub85c\ubd80\ud130|\uc73c\ub85c|\ub85c\ubd80\ud130|\uc5d0\uac8c\ub3c4|\uc5d0\uac8c\uc11c|\ud55c\ud14c\uc11c|\ud55c\ud14c\ub3c4|\ub354\ub7ec|\ud558\uace0|\uc5d0\uac8c|\uc5d0\uc11c)?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 475
    :pswitch_8
    const-string/jumbo v1, "()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 439
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 453
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 467
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static getCommonTitles()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 153
    sget-object v0, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->commonTitles:Ljava/util/HashSet;

    return-object v0
.end method

.method private queryToWhereClause(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 4
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "firstNameOnly"    # Z

    .prologue
    .line 376
    const-string/jumbo v1, "vnd.android.cursor.item/name"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    if-eqz p2, :cond_0

    const-string/jumbo v0, "data2"

    :goto_0
    aput-object v0, v2, v3

    invoke-static {p1, v1, v2}, Lcom/vlingo/core/internal/contacts/ContactDBQueryUtil;->queryToWhereClauseWithTwoWayNormalization(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "data1"

    goto :goto_0
.end method

.method private queryToWhereClauseWithPhonetic(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 6
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "firstNameOnly"    # Z

    .prologue
    .line 419
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 420
    .local v0, "encoderClasses":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Class<+Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;>;>;"
    const-class v2, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 421
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 422
    .local v1, "queryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 423
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClause(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "data1"

    aput-object v5, v3, v4

    invoke-static {v1, v0, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;->queryToWhereClauseWithEncoding(Ljava/util/List;Ljava/util/List;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private queryToWhereClauseWithoutNormalization(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 398
    const-string/jumbo v0, "vnd.android.cursor.item/name"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "data1"

    aput-object v3, v1, v2

    invoke-static {p1, v0, v1}, Lcom/vlingo/core/internal/contacts/ContactDBQueryUtil;->queryToWhereClauseWithoutNormalization(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private rulesFor4charQuery(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "strippedQuerry"    # Ljava/lang/String;
    .param p4, "familiarityHonorific"    # Ljava/lang/String;
    .param p5, "titles"    # Ljava/lang/String;
    .param p6, "postProposition"    # Ljava/lang/String;
    .param p7, "strippedExtras"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactRule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 322
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 331
    .local v10, "rules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactRule;>;"
    invoke-static/range {p7 .. p7}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static/range {p4 .. p4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object/from16 v0, p7

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    invoke-static/range {p6 .. p6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    move-object/from16 v0, p7

    move-object/from16 v1, p6

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 336
    :cond_1
    move-object/from16 v9, p3

    .line 338
    .local v9, "oneCharStripuery":Ljava/lang/String;
    invoke-static/range {p4 .. p4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    move-object/from16 v0, p7

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 340
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, ""

    move-object/from16 v0, p7

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 344
    :goto_0
    new-instance v2, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v3, "Stripped name"

    const/4 v4, 0x0

    invoke-direct {p0, v9, v4}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClauseWithPhonetic(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    const/16 v6, 0x64

    invoke-direct {v5, v6}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346
    new-instance v11, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v12, "Full Match No Space with company"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClauseWithPhonetic(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v13

    new-instance v2, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;

    const/16 v3, 0x64

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    sget-object v5, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    sget-object v6, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    move-object/from16 v0, p2

    invoke-direct {p0, v0, v4, v5, v6}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->createContactPattern(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-direct {v11, v12, v13, v2}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 352
    new-instance v11, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v12, "Stripped name"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClauseWithPhonetic(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v13

    new-instance v2, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;

    const/16 v3, 0x64

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, ".*"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->SHOULD:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    sget-object v6, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    sget-object v7, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    invoke-direct {p0, v4, v5, v6, v7}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->createContactPattern(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-direct {v11, v12, v13, v2}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 357
    .end local v9    # "oneCharStripuery":Ljava/lang/String;
    :cond_2
    new-instance v11, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v12, "Full Match No Space with company"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClause(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v13

    new-instance v2, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;

    const/16 v3, 0x50

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    sget-object v5, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    sget-object v6, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v4, v5, v6}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->createContactPattern(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-direct {v11, v12, v13, v2}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 362
    new-instance v2, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v3, "Stripped name"

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v4}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClause(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    const/16 v6, 0x50

    invoke-direct {v5, v6}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 366
    new-instance v11, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v12, "Stripped name"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClauseWithPhonetic(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v13

    new-instance v2, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;

    const/16 v3, 0x50

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, ".*"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->createContactPattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-direct {v11, v12, v13, v2}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367
    new-instance v11, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v12, "Stripped name"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClauseWithPhonetic(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v13

    new-instance v2, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;

    const/16 v3, 0x50

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, ".*"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->createContactPattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-direct {v11, v12, v13, v2}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 368
    new-instance v11, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v12, "Stripped name"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClauseWithPhonetic(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v13

    new-instance v2, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;

    const/16 v3, 0x50

    move-object/from16 v0, p3

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->createContactPattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-direct {v11, v12, v13, v2}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 372
    return-object v10

    .line 342
    .restart local v9    # "oneCharStripuery":Ljava/lang/String;
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ""

    move-object/from16 v0, p7

    move-object/from16 v1, p6

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_0
.end method


# virtual methods
.method public canProcess(Ljava/lang/String;)Z
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 144
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isKorean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public generateRules(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactRule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->generateRulesInternal(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 166
    .local v0, "rules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactRule;>;"
    const-string/jumbo v1, "\uac15"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 167
    const-string/jumbo v1, "\uac15"

    const-string/jumbo v2, "\uac10"

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->generateRulesInternal(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 172
    :cond_0
    :goto_0
    return-object v0

    .line 168
    :cond_1
    const-string/jumbo v1, "\uac10"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 169
    const-string/jumbo v1, "\uac10"

    const-string/jumbo v2, "\uac15"

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->generateRulesInternal(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public generateRulesInternal(Ljava/lang/String;)Ljava/util/List;
    .locals 23
    .param p1, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactRule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 176
    invoke-static/range {p1 .. p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 177
    const/16 v17, 0x0

    .line 314
    :cond_0
    :goto_0
    return-object v17

    .line 180
    :cond_1
    move-object/from16 v15, p1

    .line 181
    .local v15, "origQuery":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    .line 184
    .local v11, "context":Landroid/content/Context;
    const-string/jumbo v3, "\\s+"

    const-string/jumbo v4, ""

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 186
    const/16 v18, 0x0

    .local v18, "strippedQuerry":Ljava/lang/String;
    const/4 v10, 0x0

    .local v10, "strippedExtras":Ljava/lang/String;
    const/16 v19, 0x0

    .local v19, "title":Ljava/lang/String;
    const/4 v12, 0x0

    .local v12, "familiarityHonorific":Ljava/lang/String;
    const/16 v16, 0x0

    .line 188
    .local v16, "postProposition":Ljava/lang/String;
    sget-object v3, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->TRIALING_EXTRAS:Ljava/util/regex/Pattern;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v14

    .line 189
    .local v14, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v14}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 190
    const/4 v3, 0x1

    invoke-virtual {v14, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v18

    .line 191
    const/4 v3, 0x2

    invoke-virtual {v14, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v10

    .line 193
    const/4 v3, 0x3

    invoke-virtual {v14, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v19

    .line 194
    if-nez v19, :cond_2

    .line 195
    sget-object v3, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->POST_POSITION_PATTERN_REGEXP:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v10}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v13

    .line 196
    .local v13, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v13}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 197
    const/4 v3, 0x0

    invoke-virtual {v13, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v19

    .line 200
    .end local v13    # "m":Ljava/util/regex/Matcher;
    :cond_2
    const/4 v3, 0x4

    invoke-virtual {v14, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v12

    .line 201
    const/4 v3, 0x5

    invoke-virtual {v14, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v16

    .line 202
    if-nez v16, :cond_3

    if-eqz v12, :cond_3

    const-string/jumbo v3, "(\uacfc|\uc744|\uc774|\ub85c|\ub791|\ub97c|\uac00|\uc640|\uc758|\uc5d0|\ub3c4|\uc778|\uaed8|\uc57c)"

    invoke-static {v3, v12}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 204
    move-object/from16 v16, v12

    .line 208
    :cond_3
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 219
    .local v17, "rules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactRule;>;"
    new-instance v3, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v4, "Full Match No Space"

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClause(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    const/16 v7, 0x64

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v3, v4, v5, v6}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    new-instance v20, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v21, "Full Match Allow Space"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClause(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v22

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;

    const/16 v4, 0x64

    if-nez v19, :cond_5

    sget-object v5, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    move-object v7, v5

    :goto_1
    if-nez v12, :cond_6

    sget-object v5, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    move-object v6, v5

    :goto_2
    if-nez v16, :cond_7

    sget-object v5, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    :goto_3
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v7, v6, v5}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->createContactPattern(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;)Ljava/lang/String;

    move-result-object v5

    if-nez v19, :cond_8

    const-string/jumbo v6, ""

    :goto_4
    if-nez v12, :cond_9

    const-string/jumbo v7, ""

    :goto_5
    if-nez v16, :cond_a

    const-string/jumbo v8, ""

    :goto_6
    const/4 v9, 0x0

    invoke-direct/range {v3 .. v9}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233
    new-instance v20, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v21, "Full Match No Space with company"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClause(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v22

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;

    const/16 v4, 0x64

    sget-object v5, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    sget-object v6, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    sget-object v7, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5, v6, v7}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->createContactPattern(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v3 .. v9}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 238
    new-instance v3, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v4, "Full Match"

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v5}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClauseWithPhonetic(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    const/16 v7, 0x64

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v3, v4, v5, v6}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 239
    new-instance v20, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v21, "Full Match  with company"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClause(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v22

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;

    const/16 v4, 0x64

    sget-object v5, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    sget-object v6, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    sget-object v7, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v5, v6, v7}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->createContactPattern(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v3 .. v9}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 246
    :cond_4
    new-instance v3, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v4, "Full Match No Space"

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClauseWithPhonetic(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    const/16 v7, 0x5a

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v3, v4, v5, v6}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 252
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_b

    .line 256
    new-instance v3, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v4, "Full Match /w 1"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClause(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/vlingo/core/internal/contacts/scoring/KoreanOneCharacterContactScore;

    invoke-direct {v6}, Lcom/vlingo/core/internal/contacts/scoring/KoreanOneCharacterContactScore;-><init>()V

    invoke-direct {v3, v4, v5, v6}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 309
    :goto_7
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x2

    if-le v3, v4, :cond_0

    .line 310
    new-instance v3, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v4, "Partial Match"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClauseWithoutNormalization(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    const/16 v7, 0x32

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v3, v4, v5, v6}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 220
    :cond_5
    sget-object v5, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    move-object v7, v5

    goto/16 :goto_1

    :cond_6
    sget-object v5, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    move-object v6, v5

    goto/16 :goto_2

    :cond_7
    sget-object v5, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    goto/16 :goto_3

    :cond_8
    move-object/from16 v6, v19

    goto/16 :goto_4

    :cond_9
    move-object v7, v12

    goto/16 :goto_5

    :cond_a
    move-object/from16 v8, v16

    goto/16 :goto_6

    .line 259
    :cond_b
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_e

    .line 266
    const-string/jumbo v3, "(\uc774|\uc591|\uad70|\ub2d8|\uc528|\ub204\ub098|\ud615|\uc120\ubc30)"

    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 267
    new-instance v20, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v21, "Match BC 1"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClause(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v22

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;

    const/16 v4, 0x5a

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, ".*"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->createContactPattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-direct/range {v3 .. v9}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 277
    :goto_8
    new-instance v20, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v21, "Match BC 2"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClause(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v22

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;

    const/16 v4, 0x50

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, ".*"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->createContactPattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v3 .. v9}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    .line 268
    :cond_c
    const-string/jumbo v3, "(\uacfc|\uc744|\uc774|\ub85c|\ub791|\ub97c|\uac00|\uc640|\uc758|\uc5d0|\ub3c4|\uc778|\uaed8|\uc57c)"

    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 269
    new-instance v3, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v4, "Match BC 1"

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClause(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    const/16 v7, 0x50

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v3, v4, v5, v6}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 270
    new-instance v20, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v21, "Full Match No Space with company"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClause(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v22

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;

    const/16 v4, 0x50

    sget-object v5, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    sget-object v6, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    sget-object v7, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5, v6, v7}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->createContactPattern(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v3 .. v9}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    .line 275
    :cond_d
    new-instance v20, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v21, "Match BC 1"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClause(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v22

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;

    const/16 v4, 0x5a

    invoke-direct/range {p0 .. p1}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->createContactPattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-direct/range {v3 .. v9}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    .line 281
    :cond_e
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_10

    .line 284
    new-instance v3, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v4, "Full Match no space /w 3"

    invoke-direct/range {p0 .. p1}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClauseWithoutNormalization(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    const/16 v7, 0x64

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v3, v4, v5, v6}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285
    new-instance v20, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v21, "Full Match No Space with company"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClauseWithoutNormalization(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;

    const/16 v4, 0x64

    sget-object v5, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    sget-object v6, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    sget-object v7, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5, v6, v7}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->createContactPattern(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v3 .. v9}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 290
    invoke-static {v10}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 291
    new-instance v3, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v4, "Match BC 2"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClauseWithoutNormalization(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    const/16 v7, 0x5a

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v3, v4, v5, v6}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 292
    new-instance v20, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v21, "Full Match No Space with company"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClauseWithoutNormalization(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;

    const/16 v4, 0x5a

    sget-object v5, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    sget-object v6, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    sget-object v7, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5, v6, v7}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->createContactPattern(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v3 .. v9}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 296
    new-instance v20, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v21, "Match ABC 2"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClauseWithoutNormalization(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;

    const/16 v4, 0x5a

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, ".*"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->createContactPattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string/jumbo v7, ""

    const-string/jumbo v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v3 .. v9}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 298
    new-instance v20, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v21, "Match ABCT 2"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClauseWithoutNormalization(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;

    const/16 v4, 0x5a

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, ".*"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->SHOULD:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    sget-object v7, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    sget-object v8, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;->MUST_NOT:Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6, v7, v8}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->createContactPattern(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet$QueryRelation;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string/jumbo v7, ""

    const-string/jumbo v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v3 .. v9}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 301
    :cond_f
    new-instance v20, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v21, "Match ABC T"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClause(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v22

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;

    const/16 v4, 0x50

    invoke-direct/range {p0 .. p1}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->createContactPattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v3 .. v9}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 302
    new-instance v20, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v21, "Match XABC"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClause(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v22

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;

    const/16 v4, 0x50

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, ".*"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->createContactPattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v3 .. v9}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    :cond_10
    move-object/from16 v3, p0

    move-object v4, v11

    move-object v5, v15

    move-object/from16 v6, v18

    move-object v7, v12

    move-object/from16 v8, v19

    move-object/from16 v9, v16

    .line 305
    invoke-direct/range {v3 .. v10}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->rulesFor4charQuery(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_7
.end method

.method public getExitCriteria()Lcom/vlingo/core/internal/contacts/ContactExitCriteria;
    .locals 3

    .prologue
    .line 160
    new-instance v0, Lcom/vlingo/core/internal/contacts/EFIGSContactExitCriteria;

    const/16 v1, 0x28

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/contacts/EFIGSContactExitCriteria;-><init>(II)V

    return-object v0
.end method

.method protected queryFromListToWhereClause(Ljava/util/List;Z)Ljava/lang/String;
    .locals 8
    .param p2, "firstNameOnly"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p1, "nameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 381
    const-string/jumbo v4, ""

    .line 382
    .local v4, "query":Ljava/lang/String;
    const-string/jumbo v3, " OR "

    .line 383
    .local v3, "or":Ljava/lang/String;
    const/4 v1, 0x0

    .line 385
    .local v1, "index":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 386
    .local v2, "name":Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-eq v1, v5, :cond_0

    .line 387
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-direct {p0, v2, v7}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClause(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " OR "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 391
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 389
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-direct {p0, v2, v7}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClause(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 394
    .end local v2    # "name":Ljava/lang/String;
    :cond_1
    return-object v4
.end method

.method protected queryFromListToWhereClauseWithPhonetic(Ljava/util/List;Z)Ljava/lang/String;
    .locals 6
    .param p2, "firstNameOnly"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 427
    .local p1, "nameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 428
    .local v0, "encoderClasses":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Class<+Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncoder;>;>;"
    const-class v2, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 429
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 430
    .local v1, "queryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 431
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1, p2}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryFromListToWhereClause(Ljava/util/List;Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "data1"

    aput-object v5, v3, v4

    invoke-static {v1, v0, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;->queryToWhereClauseWithEncoding(Ljava/util/List;Ljava/util/List;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method protected queryFromListToWhereClauseWithoutNormalization(Ljava/util/List;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 402
    .local p1, "nameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v4, ""

    .line 403
    .local v4, "query":Ljava/lang/String;
    const-string/jumbo v3, " OR "

    .line 404
    .local v3, "or":Ljava/lang/String;
    const/4 v1, 0x0

    .line 406
    .local v1, "index":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 407
    .local v2, "name":Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-eq v1, v5, :cond_0

    .line 408
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-direct {p0, v2}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClauseWithoutNormalization(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " OR "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 412
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 410
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-direct {p0, v2}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->queryToWhereClauseWithoutNormalization(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 415
    .end local v2    # "name":Ljava/lang/String;
    :cond_1
    return-object v4
.end method

.method public skipExtraData()Z
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    return v0
.end method
