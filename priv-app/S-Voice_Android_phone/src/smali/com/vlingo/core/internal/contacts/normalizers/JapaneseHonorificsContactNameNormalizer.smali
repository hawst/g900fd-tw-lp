.class public Lcom/vlingo/core/internal/contacts/normalizers/JapaneseHonorificsContactNameNormalizer;
.super Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;
.source "JapaneseHonorificsContactNameNormalizer.java"


# static fields
.field private static final JAPANESE_HONORIFICS:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-string/jumbo v0, "(%?;?[\\w]+)(\u3055\u3093|\u30b5\u30f3|\u3061\u3083\u3093|\u30c1\u30e3\u30f3|\u541b|\u304f\u3093|\u30af\u30f3|\u69d8|\u3055\u307e|\u30b5\u30de)(\\b\\s?;?%?)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/normalizers/JapaneseHonorificsContactNameNormalizer;->JAPANESE_HONORIFICS:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;-><init>()V

    return-void
.end method


# virtual methods
.method public canNormalize(Ljava/lang/String;)Z
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    sget-object v1, Lcom/vlingo/core/internal/contacts/normalizers/JapaneseHonorificsContactNameNormalizer;->JAPANESE_HONORIFICS:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 13
    .local v0, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    return v1
.end method

.method public normalize(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 18
    sget-object v1, Lcom/vlingo/core/internal/contacts/normalizers/JapaneseHonorificsContactNameNormalizer;->JAPANESE_HONORIFICS:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 19
    .local v0, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 20
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 22
    .end local p1    # "name":Ljava/lang/String;
    :cond_0
    return-object p1
.end method
