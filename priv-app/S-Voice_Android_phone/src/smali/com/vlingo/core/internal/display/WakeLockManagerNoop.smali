.class public Lcom/vlingo/core/internal/display/WakeLockManagerNoop;
.super Ljava/lang/Object;
.source "WakeLockManagerNoop.java"

# interfaces
.implements Lcom/vlingo/core/internal/display/WakeLockManager;


# static fields
.field private static instance:Lcom/vlingo/core/internal/display/WakeLockManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/vlingo/core/internal/display/WakeLockManager;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/vlingo/core/internal/display/WakeLockManagerNoop;->instance:Lcom/vlingo/core/internal/display/WakeLockManager;

    if-nez v0, :cond_0

    .line 9
    new-instance v0, Lcom/vlingo/core/internal/display/WakeLockManagerNoop;

    invoke-direct {v0}, Lcom/vlingo/core/internal/display/WakeLockManagerNoop;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/display/WakeLockManagerNoop;->instance:Lcom/vlingo/core/internal/display/WakeLockManager;

    .line 11
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/display/WakeLockManagerNoop;->instance:Lcom/vlingo/core/internal/display/WakeLockManager;

    return-object v0
.end method


# virtual methods
.method public acquireWakeLock()V
    .locals 0

    .prologue
    .line 17
    return-void
.end method

.method public releaseWakeLock()V
    .locals 0

    .prologue
    .line 22
    return-void
.end method
