.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/NavigateHomeKoreanHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "NavigateHomeKoreanHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public actionSuccess()V
    .locals 0

    .prologue
    .line 55
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->actionSuccess()V

    .line 59
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 8
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v7, 0x0

    .line 26
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 27
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getHomeAddress()Ljava/lang/String;

    move-result-object v0

    .line 29
    .local v0, "addr":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v3

    const-string/jumbo v4, "navigate"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 31
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 32
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/NavigateHomeKoreanHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_navigate_home:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    .line 33
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v4, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/NavigateHomeKoreanHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    const-string/jumbo v4, "android.intent.action.VIEW"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "http://maps.google.com/maps?saddr=&daddr="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    const-string/jumbo v6, "+"

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->argument(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v3

    const-string/jumbo v4, "com.google.android.apps.maps,com.google.android.maps.MapsActivity"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->className(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->broadcast(Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    .line 44
    :goto_0
    return v7

    .line 40
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_nav_home_prompt_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    .line 41
    .local v1, "shownText":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_nav_home_prompt_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v2

    .line 42
    .local v2, "spokenText":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/NavigateHomeKoreanHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v4

    const/4 v3, 0x0

    check-cast v3, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    invoke-virtual {v4, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/NavigateHomeKoreanHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishDialog()V

    .line 50
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/NavigateHomeKoreanHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 51
    return-void
.end method
