.class public abstract Lcom/vlingo/core/internal/util/UrlUtils;
.super Ljava/lang/Object;
.source "UrlUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static extractValueForKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "urlKey"    # Ljava/lang/String;
    .param p1, "urlString"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    .line 33
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 34
    .local v1, "valueIndex":I
    if-eq v1, v4, :cond_1

    .line 35
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v1, v2

    .line 36
    const-string/jumbo v2, "&"

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 37
    .local v0, "valueEndIndex":I
    if-ne v0, v4, :cond_0

    .line 39
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 41
    :cond_0
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/core/internal/util/UrlUtils;->urlDecode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 43
    .end local v0    # "valueEndIndex":I
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static fetchContet(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p0, "address"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 95
    .local v1, "url":Ljava/net/URL;
    invoke-virtual {v1}, Ljava/net/URL;->getContent()Ljava/lang/Object;

    move-result-object v0

    .line 96
    .local v0, "content":Ljava/lang/Object;
    return-object v0
.end method

.method public static fetchDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p0, "address"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    invoke-static {p0}, Lcom/vlingo/core/internal/util/UrlUtils;->fetchContet(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/InputStream;

    .line 100
    .local v1, "is":Ljava/io/InputStream;
    const-string/jumbo v2, "src"

    invoke-static {v1, v2}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 101
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 102
    return-object v0
.end method

.method public static urlDecode(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "decodeStr"    # Ljava/lang/String;

    .prologue
    const/16 v8, 0x2b

    const/16 v7, 0x20

    .line 60
    if-nez p0, :cond_0

    .line 61
    const/4 v5, 0x0

    .line 89
    :goto_0
    return-object v5

    .line 64
    :cond_0
    const/4 v4, 0x0

    .line 66
    .local v4, "less":I
    const/4 v0, 0x0

    .line 68
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    if-ge v3, v5, :cond_3

    .line 69
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 71
    .local v2, "c":C
    const/16 v5, 0x25

    if-ne v2, v5, :cond_2

    .line 73
    add-int/lit8 v5, v3, 0x1

    add-int/lit8 v6, v3, 0x3

    :try_start_0
    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x10

    invoke-static {v5, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    int-to-char v2, v5

    .line 75
    if-nez v0, :cond_1

    .line 76
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1, p0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .end local v0    # "buf":Ljava/lang/StringBuffer;
    .local v1, "buf":Ljava/lang/StringBuffer;
    move-object v0, v1

    .line 78
    .end local v1    # "buf":Ljava/lang/StringBuffer;
    .restart local v0    # "buf":Ljava/lang/StringBuffer;
    :cond_1
    sub-int v5, v3, v4

    sub-int v6, v3, v4

    add-int/lit8 v6, v6, 0x3

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 79
    sub-int v5, v3, v4

    invoke-virtual {v0, v5, v2}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    add-int/lit8 v4, v4, 0x2

    .line 68
    :cond_2
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 86
    .end local v2    # "c":C
    :cond_3
    if-nez v0, :cond_4

    .line 87
    invoke-virtual {p0, v8, v7}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 89
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v8, v7}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 81
    .restart local v2    # "c":C
    :catch_0
    move-exception v5

    goto :goto_2
.end method

.method public static urlEncode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 47
    if-eqz p0, :cond_0

    .line 49
    :try_start_0
    const-string/jumbo v0, "UTF-8"

    invoke-static {p0, v0}, Lcom/vlingo/core/internal/util/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    .line 55
    :cond_0
    :goto_0
    return-object p0

    .line 51
    :catch_0
    move-exception v0

    goto :goto_0
.end method
