.class public Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;
.super Landroid/app/Service;
.source "VlingoApplicationService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/vlservice/VlingoApplicationService$ServiceHandler;
    }
.end annotation


# static fields
.field public static final ACTION_ACTIVITY_STATE_CHANGED:Ljava/lang/String; = "com.vlingo.client.app.action.ACTIVITY_STATE_CHANGED"

.field public static final ACTION_APPLICATION_STATE_CHANGED:Ljava/lang/String; = "com.vlingo.client.app.action.APPLICATION_STATE_CHANGED"

.field public static final ACTION_CLOSE_APPLICATION:Ljava/lang/String; = "com.vlingo.client.app.action.CLOSE_APPLICATION"

.field public static final EXTRA_FINISH_ACTIVITY:Ljava/lang/String; = "com.vlingo.core.internal.vlservice.FINISH_ACTIVITY"

.field public static final EXTRA_STATE:Ljava/lang/String; = "com.vlingo.client.app.extra.STATE"

.field private static final ITERATIONS:I = 0x1e

.field private static final MSG_ACTIVITY_STATE:I = 0x1

.field private static final MSG_CLOSE_APP:I = 0x2

.field private static final RESUME_WAIT:I = 0x32

.field public static final STATE_HIDDEN:I = 0x0

.field public static final STATE_SHOWN:I = 0x1

.field private static final VLINGO_PKG_PREFIX:Ljava/lang/String; = "com.vlingo"

.field private static volatile mLastState:I


# instance fields
.field private volatile mServiceHandler:Lcom/vlingo/core/internal/vlservice/VlingoApplicationService$ServiceHandler;

.field private volatile mServiceLooper:Landroid/os/Looper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const/4 v0, -0x1

    sput v0, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->mLastState:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 81
    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 53
    sget v0, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->mLastState:I

    return v0
.end method

.method static synthetic access$100(I)V
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 53
    invoke-static {p0}, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->setLastState(I)V

    return-void
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 53
    invoke-static {}, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->isAppInForegroundInternal()Z

    move-result v0

    return v0
.end method

.method public static isAppInForeground()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 247
    sget v1, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->mLastState:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isAppInForegroundInternal()Z
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 268
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    .line 270
    .local v4, "processId":I
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-string/jumbo v7, "activity"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 271
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v3

    .line 272
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    if-nez v3, :cond_1

    .line 287
    :cond_0
    :goto_0
    return v5

    .line 274
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 276
    .local v2, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 278
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 280
    .local v1, "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget v6, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v4, v6, :cond_2

    .line 281
    invoke-static {v1}, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->isForeground(Landroid/app/ActivityManager$RunningAppProcessInfo;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 282
    const/4 v5, 0x1

    goto :goto_0
.end method

.method private static isForeground(Landroid/app/ActivityManager$RunningAppProcessInfo;)Z
    .locals 2
    .param p0, "info"    # Landroid/app/ActivityManager$RunningAppProcessInfo;

    .prologue
    .line 251
    iget v0, p0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_0

    .line 252
    const/4 v0, 0x1

    .line 254
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static setLastState(I)V
    .locals 0
    .param p0, "lastState"    # I

    .prologue
    .line 214
    sput p0, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->mLastState:I

    .line 215
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 241
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 203
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 206
    const/4 v1, -0x1

    invoke-static {v1}, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->setLastState(I)V

    .line 207
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "VAS Worker"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 208
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 209
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->mServiceLooper:Landroid/os/Looper;

    .line 210
    new-instance v1, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService$ServiceHandler;

    iget-object v2, p0, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->mServiceLooper:Landroid/os/Looper;

    invoke-direct {v1, p0, v2}, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService$ServiceHandler;-><init>(Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->mServiceHandler:Lcom/vlingo/core/internal/vlservice/VlingoApplicationService$ServiceHandler;

    .line 211
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->mServiceLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 238
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 221
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 222
    .local v0, "action":Ljava/lang/String;
    const/4 v2, 0x1

    .line 224
    .local v2, "msgType":I
    const-string/jumbo v3, "com.vlingo.client.app.action.CLOSE_APPLICATION"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 225
    const/4 v2, 0x2

    .line 228
    :cond_0
    iget-object v3, p0, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->mServiceHandler:Lcom/vlingo/core/internal/vlservice/VlingoApplicationService$ServiceHandler;

    invoke-virtual {v3, v2}, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService$ServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 229
    .local v1, "msg":Landroid/os/Message;
    iput p3, v1, Landroid/os/Message;->arg1:I

    .line 230
    iput-object p1, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 231
    iget-object v3, p0, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->mServiceHandler:Lcom/vlingo/core/internal/vlservice/VlingoApplicationService$ServiceHandler;

    invoke-virtual {v3, v1}, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .line 233
    const/4 v3, 0x3

    return v3
.end method
