.class public Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentIterator;
.super Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;
.source "ContactContentIterator.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator",
        "<",
        "Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 10
    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x4

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "contact_id"

    aput-object v1, v3, v0

    const/4 v0, 0x1

    const-string/jumbo v1, "data2"

    aput-object v1, v3, v0

    const/4 v0, 0x2

    const-string/jumbo v1, "data5"

    aput-object v1, v3, v0

    const/4 v0, 0x3

    const-string/jumbo v1, "data3"

    aput-object v1, v3, v0

    const-string/jumbo v4, "mimetype=\'vnd.android.cursor.item/name\' AND in_visible_group=1"

    move-object v0, p0

    move-object v1, p1

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    return-void
.end method


# virtual methods
.method public bridge synthetic hasNext()Z
    .locals 1

    .prologue
    .line 8
    invoke-super {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method protected parseItem(Landroid/database/Cursor;)Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;
    .locals 11
    .param p1, "cursorParam"    # Landroid/database/Cursor;

    .prologue
    .line 24
    const-string/jumbo v0, "contact_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 25
    .local v8, "idColumnIndex":I
    const-string/jumbo v0, "data2"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 26
    .local v7, "firstNameColumnIndex":I
    const-string/jumbo v0, "data5"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 27
    .local v10, "middleNameColumnIndex":I
    const-string/jumbo v0, "data3"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 32
    .local v9, "lastNameColumnIndex":I
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 33
    .local v6, "contactId":Ljava/lang/Long;
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 34
    .local v3, "firstName":Ljava/lang/String;
    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 35
    .local v4, "lastName":Ljava/lang/String;
    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 40
    .local v5, "middleName":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 41
    :cond_0
    new-instance v0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic parseItem(Landroid/database/Cursor;)Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
    .locals 1
    .param p1, "x0"    # Landroid/database/Cursor;

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentIterator;->parseItem(Landroid/database/Cursor;)Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic remove()V
    .locals 0

    .prologue
    .line 8
    invoke-super {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->remove()V

    return-void
.end method
