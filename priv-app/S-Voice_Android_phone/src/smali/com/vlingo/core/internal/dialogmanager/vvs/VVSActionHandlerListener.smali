.class public interface abstract Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
.super Ljava/lang/Object;
.source "VVSActionHandlerListener.java"


# virtual methods
.method public abstract asyncHandlerDone()V
.end method

.method public abstract asyncHandlerStarted()V
.end method

.method public abstract endpointReco()V
.end method

.method public abstract execute(Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;)V
.end method

.method public abstract finishDialog()V
.end method

.method public abstract finishTurn()V
.end method

.method public abstract getActivityContext()Landroid/content/Context;
.end method

.method public abstract getFieldId()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
.end method

.method public abstract getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;
.end method

.method public abstract interruptTurn()V
.end method

.method public abstract queueEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V
.end method

.method public abstract sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V
.end method

.method public abstract setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
.end method

.method public abstract showUserText(Ljava/lang/String;)V
.end method

.method public abstract showUserText(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V
.end method

.method public abstract showVlingoText(Ljava/lang/String;)V
.end method

.method public abstract showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            "TT;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")V"
        }
    .end annotation
.end method

.method public abstract startReco()Z
.end method

.method public abstract storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V
.end method

.method public abstract tts(Ljava/lang/String;)V
.end method

.method public abstract tts(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
.end method

.method public abstract ttsAnyway(Ljava/lang/String;)V
.end method

.method public abstract ttsAnyway(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
.end method

.method public abstract userCancel()V
.end method
