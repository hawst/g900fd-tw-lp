.class public final Lcom/vlingo/core/internal/audio/TTSRequest;
.super Lcom/vlingo/core/internal/audio/AudioRequest;
.source "TTSRequest.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static final TYPE_MESSAGE_READBACK:I = 0x1

.field public static final TYPE_PROMPT:I = 0x0

.field public static final TYPE_RESULT:I = 0x2


# instance fields
.field public final allowLocalTTS:Z

.field public final allowNetworkTTS:Z

.field public final body:Ljava/lang/String;

.field public isCacheable:Z

.field public final isPersistentCache:Z

.field private lastString:Ljava/lang/String;

.field private final ttsAnyway:Z

.field private ttsFilePath:Ljava/lang/String;

.field public final type:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/vlingo/core/internal/audio/TTSRequest;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/audio/TTSRequest;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "ttsAnyway"    # Z

    .prologue
    const/4 v1, 0x1

    .line 42
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/AudioRequest;-><init>()V

    .line 30
    iput-boolean v1, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->isCacheable:Z

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->isPersistentCache:Z

    .line 33
    iput-boolean v1, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->allowLocalTTS:Z

    .line 34
    iput-boolean v1, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->allowNetworkTTS:Z

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->lastString:Ljava/lang/String;

    .line 44
    if-nez p1, :cond_0

    .line 45
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "text string cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_0
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->cleanTts(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->body:Ljava/lang/String;

    .line 48
    iput p2, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->type:I

    .line 49
    iput-boolean p3, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->ttsAnyway:Z

    .line 50
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->isTtsCachingOn()Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->isCacheable:Z

    .line 51
    return-void
.end method

.method public static getMessageReadback(Ljava/lang/String;)Lcom/vlingo/core/internal/audio/TTSRequest;
    .locals 3
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 206
    new-instance v0, Lcom/vlingo/core/internal/audio/TTSRequest;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/vlingo/core/internal/audio/TTSRequest;-><init>(Ljava/lang/String;IZ)V

    .line 207
    .local v0, "req":Lcom/vlingo/core/internal/audio/TTSRequest;
    return-object v0
.end method

.method public static getPrompt(Ljava/lang/String;)Lcom/vlingo/core/internal/audio/TTSRequest;
    .locals 2
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 183
    new-instance v0, Lcom/vlingo/core/internal/audio/TTSRequest;

    invoke-direct {v0, p0, v1, v1}, Lcom/vlingo/core/internal/audio/TTSRequest;-><init>(Ljava/lang/String;IZ)V

    .line 184
    .local v0, "req":Lcom/vlingo/core/internal/audio/TTSRequest;
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/audio/TTSRequest;->setFlag(I)V

    .line 185
    return-object v0
.end method

.method public static getPrompt(Ljava/lang/String;Z)Lcom/vlingo/core/internal/audio/TTSRequest;
    .locals 2
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "ttsAnyway"    # Z

    .prologue
    .line 195
    new-instance v0, Lcom/vlingo/core/internal/audio/TTSRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/vlingo/core/internal/audio/TTSRequest;-><init>(Ljava/lang/String;IZ)V

    .line 196
    .local v0, "req":Lcom/vlingo/core/internal/audio/TTSRequest;
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/audio/TTSRequest;->setFlag(I)V

    .line 197
    return-object v0
.end method

.method public static getResult(Ljava/lang/String;)Lcom/vlingo/core/internal/audio/TTSRequest;
    .locals 3
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 216
    new-instance v0, Lcom/vlingo/core/internal/audio/TTSRequest;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/vlingo/core/internal/audio/TTSRequest;-><init>(Ljava/lang/String;IZ)V

    .line 217
    .local v0, "req":Lcom/vlingo/core/internal/audio/TTSRequest;
    return-object v0
.end method


# virtual methods
.method protected clearCachedFile()V
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->isCacheable:Z

    if-nez v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->ttsFilePath:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/TTSCache;->deleteCachedRequest(Ljava/lang/String;)Z

    .line 163
    :cond_0
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 84
    instance-of v1, p1, Lcom/vlingo/core/internal/audio/TTSRequest;

    if-nez v1, :cond_0

    .line 85
    const/4 v1, 0x0

    .line 88
    :goto_0
    return v1

    :cond_0
    move-object v0, p1

    .line 87
    check-cast v0, Lcom/vlingo/core/internal/audio/TTSRequest;

    .line 88
    .local v0, "that":Lcom/vlingo/core/internal/audio/TTSRequest;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/TTSRequest;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/TTSRequest;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method getCacheKey()Ljava/lang/String;
    .locals 2

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/TTSRequest;->hashCode()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getTextToSpeak()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->body:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/TTSRequest;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public isTtsAnyway()Z
    .locals 1

    .prologue
    .line 221
    iget-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->ttsAnyway:Z

    return v0
.end method

.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 155
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/audio/AudioRequest;->onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V

    .line 156
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/TTSRequest;->clearCachedFile()V

    .line 157
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 143
    invoke-super {p0, p1}, Lcom/vlingo/core/internal/audio/AudioRequest;->onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 144
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/TTSRequest;->clearCachedFile()V

    .line 145
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 149
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/audio/AudioRequest;->onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V

    .line 150
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/TTSRequest;->clearCachedFile()V

    .line 151
    return-void
.end method

.method public onSetDataSourceFailed()V
    .locals 0

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/TTSRequest;->clearCachedFile()V

    .line 139
    return-void
.end method

.method public prepareForPlayback(Landroid/content/Context;Lcom/vlingo/core/internal/audio/AudioPlayer;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "player"    # Lcom/vlingo/core/internal/audio/AudioPlayer;

    .prologue
    .line 98
    invoke-virtual {p2}, Lcom/vlingo/core/internal/audio/AudioPlayer;->getTTSEngine()Lcom/vlingo/core/internal/audio/TTSEngine;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/audio/TTSRequest;->prepareForPlayback(Landroid/content/Context;Lcom/vlingo/core/internal/audio/TTSEngine;)Z

    move-result v0

    return v0
.end method

.method public prepareForPlayback(Landroid/content/Context;Lcom/vlingo/core/internal/audio/TTSEngine;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ttsEngine"    # Lcom/vlingo/core/internal/audio/TTSEngine;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 103
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    iget v2, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->type:I

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->ttsAnyway:Z

    if-nez v2, :cond_1

    .line 104
    sget-object v0, Lcom/vlingo/core/internal/audio/TTSRequest;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "voice prompts off for Prompt, prepareForPlayback return false"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 117
    :cond_0
    :goto_0
    return v0

    .line 108
    :cond_1
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->ttsFilePath:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 112
    invoke-virtual {p2, p1, p0}, Lcom/vlingo/core/internal/audio/TTSEngine;->getFilePathForTTSRequest(Landroid/content/Context;Lcom/vlingo/core/internal/audio/TTSRequest;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->ttsFilePath:Ljava/lang/String;

    .line 117
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->ttsFilePath:Ljava/lang/String;

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public setDataSource(Landroid/content/Context;Landroid/media/MediaPlayer;Lcom/vlingo/core/internal/audio/AudioPlayer;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mediaPlayer"    # Landroid/media/MediaPlayer;
    .param p3, "player"    # Lcom/vlingo/core/internal/audio/AudioPlayer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 122
    iget-object v3, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->ttsFilePath:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 123
    const/4 v1, 0x0

    .line 125
    .local v1, "in":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v3, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->ttsFilePath:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    .end local v1    # "in":Ljava/io/FileInputStream;
    .local v2, "in":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-virtual {p2, v3}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 131
    if-eqz v2, :cond_0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 134
    .end local v2    # "in":Ljava/io/FileInputStream;
    :cond_0
    :goto_0
    return-void

    .line 127
    .restart local v1    # "in":Ljava/io/FileInputStream;
    :catch_0
    move-exception v0

    .line 128
    .local v0, "ex":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    const-class v3, Lcom/vlingo/core/internal/audio/TTSRequest;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "problem in getting TTS file: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 131
    if-eqz v1, :cond_0

    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    :catch_1
    move-exception v3

    goto :goto_0

    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    :goto_2
    if-eqz v1, :cond_1

    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    :cond_1
    :goto_3
    throw v3

    .end local v1    # "in":Ljava/io/FileInputStream;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    :catch_2
    move-exception v3

    goto :goto_0

    .end local v2    # "in":Ljava/io/FileInputStream;
    .restart local v1    # "in":Ljava/io/FileInputStream;
    :catch_3
    move-exception v4

    goto :goto_3

    .end local v1    # "in":Ljava/io/FileInputStream;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "in":Ljava/io/FileInputStream;
    .restart local v1    # "in":Ljava/io/FileInputStream;
    goto :goto_2

    .line 127
    .end local v1    # "in":Ljava/io/FileInputStream;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "in":Ljava/io/FileInputStream;
    .restart local v1    # "in":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 64
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 65
    .local v1, "sb":Ljava/lang/StringBuilder;
    iget v2, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->type:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 67
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->body:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 68
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->body:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    :cond_0
    iget-boolean v2, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->isCacheable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 71
    iget v2, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->flags:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 73
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 74
    .local v0, "ret":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->lastString:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->lastString:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 78
    :cond_1
    iput-object v0, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->lastString:Ljava/lang/String;

    .line 79
    return-object v0
.end method
