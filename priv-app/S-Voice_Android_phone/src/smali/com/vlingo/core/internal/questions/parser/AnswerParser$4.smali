.class Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;
.super Ljava/lang/Object;
.source "AnswerParser.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/questions/parser/AnswerParser;->newElement(Ljava/lang/String;)Lcom/vlingo/core/internal/questions/parser/ResponseElement;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/questions/parser/AnswerParser;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)V
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;->this$0:Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 292
    iget-object v1, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;->this$0:Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    # getter for: Lcom/vlingo/core/internal/questions/parser/AnswerParser;->mCurrentSubsection:Lcom/vlingo/core/internal/questions/parser/SubsectionElement;
    invoke-static {v1}, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->access$300(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)Lcom/vlingo/core/internal/questions/parser/SubsectionElement;

    move-result-object v1

    iget-object v1, v1, Lcom/vlingo/core/internal/questions/parser/SubsectionElement;->attributes:Ljava/util/Map;

    const-string/jumbo v2, "Width"

    iget-object v3, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;->this$0:Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    # getter for: Lcom/vlingo/core/internal/questions/parser/AnswerParser;->mCurrentImage:Lcom/vlingo/core/internal/questions/parser/ImageElement;
    invoke-static {v3}, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->access$400(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)Lcom/vlingo/core/internal/questions/parser/ImageElement;

    move-result-object v3

    const-string/jumbo v4, "Width"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/questions/parser/ImageElement;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    iget-object v1, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;->this$0:Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    # getter for: Lcom/vlingo/core/internal/questions/parser/AnswerParser;->mCurrentSubsection:Lcom/vlingo/core/internal/questions/parser/SubsectionElement;
    invoke-static {v1}, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->access$300(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)Lcom/vlingo/core/internal/questions/parser/SubsectionElement;

    move-result-object v1

    iget-object v1, v1, Lcom/vlingo/core/internal/questions/parser/SubsectionElement;->attributes:Ljava/util/Map;

    const-string/jumbo v2, "Height"

    iget-object v3, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;->this$0:Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    # getter for: Lcom/vlingo/core/internal/questions/parser/AnswerParser;->mCurrentImage:Lcom/vlingo/core/internal/questions/parser/ImageElement;
    invoke-static {v3}, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->access$400(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)Lcom/vlingo/core/internal/questions/parser/ImageElement;

    move-result-object v3

    const-string/jumbo v4, "Height"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/questions/parser/ImageElement;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    iget-object v1, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;->this$0:Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    # getter for: Lcom/vlingo/core/internal/questions/parser/AnswerParser;->mCurrentSubsection:Lcom/vlingo/core/internal/questions/parser/SubsectionElement;
    invoke-static {v1}, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->access$300(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)Lcom/vlingo/core/internal/questions/parser/SubsectionElement;

    move-result-object v1

    iget-object v1, v1, Lcom/vlingo/core/internal/questions/parser/SubsectionElement;->attributes:Ljava/util/Map;

    const-string/jumbo v2, "Src"

    iget-object v3, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;->this$0:Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    # getter for: Lcom/vlingo/core/internal/questions/parser/AnswerParser;->mCurrentImage:Lcom/vlingo/core/internal/questions/parser/ImageElement;
    invoke-static {v3}, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->access$400(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)Lcom/vlingo/core/internal/questions/parser/ImageElement;

    move-result-object v3

    const-string/jumbo v4, "Src"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/questions/parser/ImageElement;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    new-instance v0, Lcom/vlingo/core/internal/questions/DownloadableImage;

    iget-object v1, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;->this$0:Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    # getter for: Lcom/vlingo/core/internal/questions/parser/AnswerParser;->mCurrentImage:Lcom/vlingo/core/internal/questions/parser/ImageElement;
    invoke-static {v1}, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->access$400(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)Lcom/vlingo/core/internal/questions/parser/ImageElement;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/questions/parser/ImageElement;->getURL()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/questions/DownloadableImage;-><init>(Ljava/lang/String;)V

    .line 298
    .local v0, "image":Lcom/vlingo/core/internal/questions/DownloadableImage;
    iget-object v1, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;->this$0:Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    # getter for: Lcom/vlingo/core/internal/questions/parser/AnswerParser;->mCurrentSubsection:Lcom/vlingo/core/internal/questions/parser/SubsectionElement;
    invoke-static {v1}, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->access$300(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)Lcom/vlingo/core/internal/questions/parser/SubsectionElement;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/questions/parser/SubsectionElement;->setImage(Lcom/vlingo/core/internal/questions/DownloadableImage;)V

    .line 304
    new-instance v1, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4$1;

    invoke-direct {v1, p0, v0}, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4$1;-><init>(Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;Lcom/vlingo/core/internal/questions/DownloadableImage;)V

    invoke-virtual {v1}, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4$1;->start()V

    .line 317
    return-void
.end method
