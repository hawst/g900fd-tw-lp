.class public Lcom/vlingo/core/internal/contacts/EFIGSContactExitCriteria;
.super Lcom/vlingo/core/internal/contacts/ContactExitCriteria;
.source "EFIGSContactExitCriteria.java"


# instance fields
.field high:I

.field final maxDiff:I

.field final rawMin:I


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1, "rawMin"    # I
    .param p2, "maxDiff"    # I

    .prologue
    .line 11
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/contacts/ContactExitCriteria;-><init>(Z)V

    .line 8
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/core/internal/contacts/EFIGSContactExitCriteria;->high:I

    .line 12
    iput p1, p0, Lcom/vlingo/core/internal/contacts/EFIGSContactExitCriteria;->rawMin:I

    .line 13
    iput p2, p0, Lcom/vlingo/core/internal/contacts/EFIGSContactExitCriteria;->maxDiff:I

    .line 14
    return-void
.end method


# virtual methods
.method public keepMatch(I)Z
    .locals 2
    .param p1, "score"    # I

    .prologue
    .line 28
    iget v0, p0, Lcom/vlingo/core/internal/contacts/EFIGSContactExitCriteria;->high:I

    sub-int/2addr v0, p1

    iget v1, p0, Lcom/vlingo/core/internal/contacts/EFIGSContactExitCriteria;->maxDiff:I

    if-gt v0, v1, :cond_0

    iget v0, p0, Lcom/vlingo/core/internal/contacts/EFIGSContactExitCriteria;->rawMin:I

    if-ge p1, v0, :cond_1

    .line 29
    :cond_0
    const/4 v0, 0x0

    .line 32
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public tallyScore(I)Z
    .locals 1
    .param p1, "score"    # I

    .prologue
    .line 21
    iget v0, p0, Lcom/vlingo/core/internal/contacts/EFIGSContactExitCriteria;->high:I

    if-ge v0, p1, :cond_0

    .line 22
    iput p1, p0, Lcom/vlingo/core/internal/contacts/EFIGSContactExitCriteria;->high:I

    .line 24
    :cond_0
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/contacts/EFIGSContactExitCriteria;->keepMatch(I)Z

    move-result v0

    return v0
.end method
