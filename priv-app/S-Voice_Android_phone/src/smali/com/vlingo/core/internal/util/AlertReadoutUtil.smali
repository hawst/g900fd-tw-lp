.class public final Lcom/vlingo/core/internal/util/AlertReadoutUtil;
.super Ljava/lang/Object;
.source "AlertReadoutUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/util/AlertReadoutUtil$MessageReadoutTypeComparator;,
        Lcom/vlingo/core/internal/util/AlertReadoutUtil$SearchType;
    }
.end annotation


# static fields
.field public static final EXPANDED_LIST_SIZE:I = 0x6

.field public static final SHORT_LIST_SIZE:I = 0x3


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createSMSMMSSenderQueueMap(Ljava/util/LinkedList;)Ljava/util/HashMap;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<+Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 47
    .local v5, "senderQueueMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 72
    :cond_0
    return-object v5

    .line 51
    :cond_1
    invoke-virtual {p0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 52
    .local v0, "alert":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-static {v0}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_3
    move-object v2, v0

    .line 53
    check-cast v2, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 54
    .local v2, "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 55
    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 56
    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 57
    .local v4, "senderEntry":Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;
    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessagesFromSender()Ljava/util/LinkedList;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 58
    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 60
    .end local v4    # "senderEntry":Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;
    :cond_4
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 61
    .local v3, "messages":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 62
    new-instance v4, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6, v3}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;-><init>(Ljava/lang/String;Ljava/util/LinkedList;)V

    .line 63
    .restart local v4    # "senderEntry":Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;
    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->setDisplayName(Ljava/lang/String;)V

    .line 64
    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static getMessageQueueByContactName(Ljava/util/HashMap;Ljava/lang/String;Landroid/content/Context;)Ljava/util/HashMap;
    .locals 12
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 236
    .local p0, "senderQueue":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 241
    .local v1, "filteredQueue":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    if-eqz p0, :cond_4

    invoke-virtual {p0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_4

    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 242
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->getInstance()Lcom/vlingo/core/internal/contacts/ContactDBUtil;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/contacts/ContactType;->MESSAGE:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {v10}, Lcom/vlingo/core/internal/contacts/ContactType;->getLookupTypes()Ljava/util/EnumSet;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v9, p2, p1, v10, v11}, Lcom/vlingo/core/internal/contacts/ContactDBUtil;->getContactMatches(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;Z)Ljava/util/List;

    move-result-object v5

    .line 245
    .local v5, "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->cleanPhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 247
    .local v0, "cleanedName":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    .line 248
    .local v7, "senderEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    const/4 v8, 0x0

    .line 249
    .local v8, "senderName":Ljava/lang/String;
    const/4 v6, 0x0

    .line 250
    .local v6, "senderAddress":Ljava/lang/String;
    if-eqz v7, :cond_1

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 251
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getDisplayName()Ljava/lang/String;

    move-result-object v8

    .line 252
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getAddress()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/vlingo/core/internal/util/StringUtils;->cleanPhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 256
    :cond_1
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 258
    .local v4, "m":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-eqz v8, :cond_2

    iget-object v9, v4, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 259
    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v1, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 262
    .end local v4    # "m":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_3
    if-eqz v6, :cond_0

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 263
    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v1, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 268
    .end local v0    # "cleanedName":Ljava/lang/String;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v6    # "senderAddress":Ljava/lang/String;
    .end local v7    # "senderEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    .end local v8    # "senderName":Ljava/lang/String;
    :cond_4
    return-object v1
.end method

.method public static getMessageTypeFromAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;Z)Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    .locals 7
    .param p0, "alert"    # Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    .param p1, "showMessageBody"    # Z

    .prologue
    .line 186
    invoke-static {p0}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 187
    const/4 v2, 0x0

    .line 224
    :goto_0
    return-object v2

    :cond_0
    move-object v1, p0

    .line 190
    check-cast v1, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 192
    .local v1, "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    if-eqz p1, :cond_5

    .line 195
    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getBody()Ljava/lang/String;

    move-result-object v0

    .line 196
    .local v0, "body":Ljava/lang/String;
    invoke-static {p0}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSubject()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 197
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 198
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSubject()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 204
    :cond_1
    :goto_1
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 205
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_message_empty_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 211
    :cond_2
    :goto_2
    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderName()Ljava/lang/String;

    move-result-object v3

    .line 213
    .local v3, "name":Ljava/lang/String;
    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    const-string/jumbo v5, ""

    invoke-direct {v2, v0, v3, v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    .local v2, "mt":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAttachments()I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setAttachments(I)V

    .line 215
    invoke-static {p0}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 216
    const-string/jumbo v5, "SMS"

    invoke-virtual {v2, v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setType(Ljava/lang/String;)V

    .line 220
    :cond_3
    :goto_3
    new-instance v4, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v3, v5}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    .local v4, "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    invoke-virtual {v2, v4}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->addRecipient(Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;)V

    .line 222
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getTimeStamp()J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setTimeStamp(J)V

    .line 223
    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAttachments()I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setAttachments(I)V

    goto/16 :goto_0

    .line 200
    .end local v2    # "mt":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    :cond_4
    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSubject()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 209
    .end local v0    # "body":Ljava/lang/String;
    :cond_5
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_safereader_hidden_message_body:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "body":Ljava/lang/String;
    goto :goto_2

    .line 217
    .restart local v2    # "mt":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    .restart local v3    # "name":Ljava/lang/String;
    :cond_6
    invoke-static {p0}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 218
    const-string/jumbo v5, "MMS"

    invoke-virtual {v2, v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setType(Ljava/lang/String;)V

    goto :goto_3
.end method

.method public static getTotalMessagesFromSenderQueue(Ljava/util/HashMap;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 82
    .local p0, "senderQueue":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    const/4 v2, 0x0

    .line 83
    .local v2, "total":I
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 84
    invoke-virtual {p0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 85
    .local v1, "senderEntry":Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;
    if-eqz v1, :cond_0

    .line 86
    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessageCount()I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    .line 90
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "senderEntry":Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;
    :cond_1
    return v2
.end method

.method public static getUnreadCount(Ljava/util/LinkedList;I)I
    .locals 3
    .param p1, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .local p0, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    const/4 v0, -0x1

    .line 283
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    if-gez p1, :cond_1

    .line 287
    :cond_0
    :goto_0
    return v0

    .line 285
    :cond_1
    invoke-virtual {p0}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_2

    invoke-virtual {p0}, Ljava/util/LinkedList;->size()I

    move-result v1

    sub-int/2addr v1, p1

    if-gtz v1, :cond_0

    .line 287
    :cond_2
    invoke-virtual {p0}, Ljava/util/LinkedList;->size()I

    move-result v0

    sub-int/2addr v0, p1

    goto :goto_0
.end method

.method public static hasNext(Ljava/util/LinkedList;I)Z
    .locals 2
    .param p1, "currentPosition"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<*>;I)Z"
        }
    .end annotation

    .prologue
    .line 172
    .local p0, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<*>;"
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-ltz p1, :cond_0

    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z
    .locals 2
    .param p0, "alert"    # Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .prologue
    .line 142
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getType()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 143
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getType()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "MMS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 145
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z
    .locals 2
    .param p0, "alert"    # Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .prologue
    .line 129
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getType()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 130
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getType()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "SMS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 132
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z
    .locals 2
    .param p0, "alert"    # Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .prologue
    const/4 v0, 0x0

    .line 116
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getType()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 117
    invoke-static {p0}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 119
    :cond_1
    return v0
.end method

.method public static sortMessageReadoutList(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    .local p0, "senderList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 102
    new-instance v0, Lcom/vlingo/core/internal/util/AlertReadoutUtil$MessageReadoutTypeComparator;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/AlertReadoutUtil$MessageReadoutTypeComparator;-><init>()V

    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 105
    .end local p0    # "senderList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    :goto_0
    return-object p0

    .restart local p0    # "senderList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    :cond_0
    new-instance p0, Ljava/util/LinkedList;

    .end local p0    # "senderList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    invoke-direct {p0}, Ljava/util/LinkedList;-><init>()V

    goto :goto_0
.end method

.method public static trimListToMaxDisplay(Ljava/util/LinkedList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 155
    .local p0, "untrimmedList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<*>;"
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 156
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMaxDisplayNumber()I

    move-result v0

    if-lez v0, :cond_0

    .line 157
    :goto_0
    invoke-virtual {p0}, Ljava/util/LinkedList;->size()I

    move-result v0

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMaxDisplayNumber()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 158
    invoke-virtual {p0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    goto :goto_0

    .line 162
    :cond_0
    return-void
.end method
