.class public Lcom/vlingo/core/internal/contacts/rules/KoreanSamelessContactRuleSet;
.super Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;
.source "KoreanSamelessContactRuleSet.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;-><init>()V

    .line 32
    return-void
.end method


# virtual methods
.method public canProcess(Ljava/lang/String;)Z
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isKorean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public generateRules(Ljava/lang/String;)Ljava/util/List;
    .locals 13
    .param p1, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactRule;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v12, 0x64

    const/4 v11, 0x0

    .line 39
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 42
    .local v3, "rules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactRule;>;"
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 43
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v6, "\\s+"

    const-string/jumbo v7, ""

    invoke-virtual {p1, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 45
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 46
    .local v2, "namesToCheck":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    move-object v5, p1

    .line 48
    .local v5, "strippedQuerry":Ljava/lang/String;
    sget-object v6, Lcom/vlingo/core/internal/contacts/rules/KoreanSamelessContactRuleSet;->TRIALING_EXTRAS:Ljava/util/regex/Pattern;

    invoke-virtual {v6, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 49
    .local v1, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 50
    const/4 v6, 0x1

    invoke-virtual {v1, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    .line 51
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    :cond_0
    invoke-static {v0, v2}, Lcom/vlingo/core/internal/util/CallLogUtil;->findSimilarNameFromCallLog(Landroid/content/Context;Ljava/util/List;)Ljava/util/Map;

    move-result-object v4

    .line 57
    .local v4, "similarNameInCallList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    if-eqz v4, :cond_1

    invoke-interface {v4, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 58
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v8, "CallLog Partial Match /w 2"

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    invoke-virtual {p0, v6}, Lcom/vlingo/core/internal/contacts/rules/KoreanSamelessContactRuleSet;->queryFromListToWhereClauseWithoutNormalization(Ljava/util/List;)Ljava/lang/String;

    move-result-object v6

    new-instance v9, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    const/16 v10, 0x5a

    invoke-direct {v9, v10}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v7, v8, v6, v9}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x3

    if-le v6, v7, :cond_3

    .line 61
    if-eqz v4, :cond_2

    invoke-interface {v4, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 62
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v8, "CallLog Full Match /w 3+"

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    invoke-virtual {p0, v6, v11}, Lcom/vlingo/core/internal/contacts/rules/KoreanSamelessContactRuleSet;->queryFromListToWhereClauseWithPhonetic(Ljava/util/List;Z)Ljava/lang/String;

    move-result-object v6

    new-instance v9, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v9, v12}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v7, v8, v6, v9}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v8, "CallLog Full Match with wildcards /w 3+"

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    invoke-virtual {p0, v6, v11}, Lcom/vlingo/core/internal/contacts/rules/KoreanSamelessContactRuleSet;->queryFromListToWhereClause(Ljava/util/List;Z)Ljava/lang/String;

    move-result-object v6

    new-instance v9, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v9, v12}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v7, v8, v6, v9}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    :cond_2
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 67
    if-eqz v4, :cond_3

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    .line 68
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v8, "CallLog First Name /w 3+"

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    invoke-virtual {p0, v6, v11}, Lcom/vlingo/core/internal/contacts/rules/KoreanSamelessContactRuleSet;->queryFromListToWhereClause(Ljava/util/List;Z)Ljava/lang/String;

    move-result-object v6

    new-instance v9, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v9, v12}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v7, v8, v6, v9}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    :cond_3
    return-object v3
.end method

.method public getExitCriteria()Lcom/vlingo/core/internal/contacts/ContactExitCriteria;
    .locals 3

    .prologue
    .line 34
    new-instance v0, Lcom/vlingo/core/internal/contacts/EFIGSContactExitCriteria;

    const/16 v1, 0x28

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/contacts/EFIGSContactExitCriteria;-><init>(II)V

    return-object v0
.end method

.method public skipExtraData()Z
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    return v0
.end method
