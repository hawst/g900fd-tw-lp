.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowContactChoicesHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "ShowContactChoicesHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 2
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 21
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 22
    const-class v1, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactChoicesController;

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowContactChoicesHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactChoicesController;

    .line 23
    .local v0, "controller":Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactChoicesController;
    invoke-virtual {v0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowContactChoicesController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v1

    return v1
.end method
