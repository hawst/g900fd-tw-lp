.class public interface abstract Lcom/vlingo/core/internal/dialogmanager/features/MemoFeatures$Error;
.super Ljava/lang/Object;
.source "MemoFeatures.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/features/MemoFeatures;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Error"
.end annotation


# static fields
.field public static final ERROR:Ljava/lang/String; = "memo.error"

.field public static final MISSING:Ljava/lang/String; = "memo.missing"
