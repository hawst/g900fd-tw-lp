.class public Lcom/vlingo/core/internal/dialogmanager/events/ScheduleResolvedEvent;
.super Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;
.source "ScheduleResolvedEvent.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent",
        "<",
        "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
        ">;"
    }
.end annotation


# instance fields
.field private final NAME:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;-><init>()V

    .line 21
    const-string/jumbo v0, "appointment-resolved"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ScheduleResolvedEvent;->NAME:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public constructor <init>(Ljava/util/List;II)V
    .locals 1
    .param p2, "offset"    # I
    .param p3, "totalCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 24
    .local p1, "appointments":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;-><init>(Ljava/util/List;II)V

    .line 21
    const-string/jumbo v0, "appointment-resolved"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ScheduleResolvedEvent;->NAME:Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    const-string/jumbo v0, "appointment-resolved"

    return-object v0
.end method

.method public getVLDialogEvent()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    .locals 18

    .prologue
    .line 38
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/dialogmanager/events/ScheduleResolvedEvent;->getItems()Ljava/util/List;

    move-result-object v13

    if-nez v13, :cond_0

    const/4 v13, 0x0

    .line 83
    :goto_0
    return-object v13

    .line 40
    :cond_0
    new-instance v9, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    const-string/jumbo v13, "appointment-resolved"

    invoke-direct {v9, v13}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;-><init>(Ljava/lang/String;)V

    .line 41
    .local v9, "builder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/vlingo/core/internal/dialogmanager/events/ScheduleResolvedEvent;->writeTotalCount(Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;)V

    .line 42
    new-instance v3, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;

    const-string/jumbo v13, "appointments"

    invoke-direct {v3, v13}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;-><init>(Ljava/lang/String;)V

    .line 43
    .local v3, "appointmentsBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/dialogmanager/events/ScheduleResolvedEvent;->getOffset()I

    move-result v12

    .line 45
    .local v12, "id":I
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/dialogmanager/events/ScheduleResolvedEvent;->getItems()Ljava/util/List;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 46
    .local v1, "appointment":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    new-instance v2, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;

    const-string/jumbo v13, "appointment"

    invoke-direct {v2, v13}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;-><init>(Ljava/lang/String;)V

    .line 48
    .local v2, "appointmentBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    const-string/jumbo v13, "id"

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 49
    add-int/lit8 v12, v12, 0x1

    .line 50
    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_1

    .line 51
    const-string/jumbo v13, "title"

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 53
    :cond_1
    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBeginTime()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_2

    .line 54
    const-string/jumbo v13, "time"

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBeginTime()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 56
    :cond_2
    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBeginDate()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_3

    .line 57
    const-string/jumbo v13, "date"

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBeginDate()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 59
    :cond_3
    const-string/jumbo v13, "all.day"

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getAllDay()Z

    move-result v14

    invoke-static {v14}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 60
    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getDuration()J

    move-result-wide v13

    const-wide/16 v15, 0x0

    cmp-long v13, v13, v15

    if-lez v13, :cond_4

    .line 61
    const-string/jumbo v13, "duration"

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getDuration()J

    move-result-wide v14

    const-wide/32 v16, 0xea60

    div-long v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 63
    :cond_4
    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getLocation()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_5

    .line 64
    const-string/jumbo v13, "location"

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getLocation()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 66
    :cond_5
    const-string/jumbo v13, "readonly"

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->isReadOnly()Z

    move-result v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 67
    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getAttendeeNames()Ljava/util/List;

    move-result-object v7

    .line 68
    .local v7, "attendees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 69
    .local v6, "attendeeId":I
    if-eqz v7, :cond_7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v13

    if-lez v13, :cond_7

    .line 70
    new-instance v8, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;

    const-string/jumbo v13, "invitees"

    invoke-direct {v8, v13}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;-><init>(Ljava/lang/String;)V

    .line 71
    .local v8, "attendeesBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_6

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 72
    .local v4, "attendee":Ljava/lang/String;
    new-instance v5, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;

    const-string/jumbo v13, "invitee"

    invoke-direct {v5, v13}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;-><init>(Ljava/lang/String;)V

    .line 73
    .local v5, "attendeeBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    const-string/jumbo v13, "id"

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v5, v13, v14}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 74
    add-int/lit8 v6, v6, 0x1

    .line 75
    const-string/jumbo v13, "name"

    invoke-virtual {v5, v13, v4}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 76
    invoke-virtual {v5}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;

    move-result-object v13

    invoke-virtual {v8, v13}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventFieldGroup(Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    goto :goto_2

    .line 78
    .end local v4    # "attendee":Ljava/lang/String;
    .end local v5    # "attendeeBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    :cond_6
    invoke-virtual {v8}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;

    move-result-object v13

    invoke-virtual {v2, v13}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventFieldGroup(Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 80
    .end local v8    # "attendeesBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    .end local v11    # "i$":Ljava/util/Iterator;
    :cond_7
    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;

    move-result-object v13

    invoke-virtual {v3, v13}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventFieldGroup(Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    goto/16 :goto_1

    .line 82
    .end local v1    # "appointment":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .end local v2    # "appointmentBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    .end local v6    # "attendeeId":I
    .end local v7    # "attendees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_8
    invoke-virtual {v3}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;

    move-result-object v13

    invoke-virtual {v9, v13}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->eventFieldGroup(Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 83
    invoke-virtual {v9}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;

    move-result-object v13

    goto/16 :goto_0
.end method
