.class public final Lcom/vlingo/core/internal/debug/AutomationLog;
.super Ljava/lang/Object;
.source "AutomationLog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/debug/AutomationLog$WidgetHolder;
    }
.end annotation


# static fields
.field static final AUTOMATABLE:Z = true

.field public static final BUTTON_CENTER:I = 0x67

.field public static final BUTTON_LEFT:I = 0x65

.field public static final BUTTON_RIGHT:I = 0x66

.field public static final EVENT_APP_CREATE:I = 0x5

.field public static final EVENT_APP_DESTROY:I = 0x6

.field public static final EVENT_APP_PAUSE:I = 0x3

.field public static final EVENT_APP_RESUME:I = 0x4

.field public static final EVENT_APP_START:I = 0x7

.field public static final EVENT_APP_STOP:I = 0x8

.field public static final EVENT_DIALOG_IDLE:I = 0xc

.field public static final EVENT_END_RECORDING:I = 0x2

.field public static final EVENT_FAILED_RECORDING:I = 0xb

.field public static final EVENT_FINISHING_UTT:I = 0x10

.field public static final EVENT_LOCATION_PAUSED:I = 0xa

.field public static final EVENT_LOCATION_RESUMED:I = 0x9

.field public static final EVENT_PERFOMING_AUDIO_FILE_UTT:I = 0xe

.field public static final EVENT_PERFOMING_AUDIO_UTT:I = 0xf

.field public static final EVENT_PERFOMING_TEXT_UTT:I = 0xd

.field public static final EVENT_START_RECORDING:I = 0x1

.field public static final EVENT_TTS_START:I = 0x11

.field public static final EVENT_TTS_STOP:I = 0x12

.field public static final MIC_BTN_IDLE:I = 0xd

.field public static final MIC_BTN_IDLE_DESCR:Ljava/lang/String; = "ZEP_MIC_BTN_IDLE"

.field public static final MIC_BTN_LISTEN:I = 0xe

.field public static final MIC_BTN_LISTENING_DESCR:Ljava/lang/String; = "ZEP_MIC_BTN_LISTENING"

.field public static final MIC_BTN_THINKING:I = 0xf

.field public static final MIC_BTN_THINKING_DESCR:Ljava/lang/String; = "ZEP_MIC_BTN_THINKING"

.field private static PREFIX:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-string/jumbo v0, "ZEP"

    sput-object v0, Lcom/vlingo/core/internal/debug/AutomationLog;->PREFIX:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    return-void
.end method

.method public static addWidget(Ljava/lang/Enum;Landroid/view/View;)V
    .locals 0
    .param p0, "enm"    # Ljava/lang/Enum;
    .param p1, "widget"    # Landroid/view/View;

    .prologue
    .line 287
    return-void
.end method

.method private static alog(Ljava/lang/String;)V
    .locals 0
    .param p0, "txt"    # Ljava/lang/String;

    .prologue
    .line 69
    return-void
.end method

.method public static appdata(Ljava/lang/String;)V
    .locals 0
    .param p0, "json"    # Ljava/lang/String;

    .prologue
    .line 146
    return-void
.end method

.method public static builtView(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 413
    return-void
.end method

.method public static cleanWidgetList()V
    .locals 0

    .prologue
    .line 279
    return-void
.end method

.method public static dialog(Ljava/lang/String;)V
    .locals 0
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 86
    return-void
.end method

.method public static dialogAndTts(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "dialogMsg"    # Ljava/lang/String;
    .param p1, "ttsMsg"    # Ljava/lang/String;

    .prologue
    .line 76
    return-void
.end method

.method public static dialogKey(Ljava/lang/String;)V
    .locals 0
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 256
    return-void
.end method

.method public static event(I)V
    .locals 0
    .param p0, "e"    # I

    .prologue
    .line 214
    return-void
.end method

.method public static getResult()Lorg/json/JSONObject;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 422
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getWidgetList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/debug/AutomationLog$WidgetHolder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 295
    const/4 v0, 0x0

    return-object v0
.end method

.method private static logKeyValue(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 236
    return-void
.end method

.method public static micButton(I)V
    .locals 0
    .param p0, "micButtonState"    # I

    .prologue
    .line 116
    return-void
.end method

.method public static notifyNewUtt(Ljava/lang/String;)V
    .locals 0
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 384
    return-void
.end method

.method public static notifyStartNewTest()V
    .locals 0

    .prologue
    .line 372
    return-void
.end method

.method public static receipt(Ljava/lang/String;)V
    .locals 0
    .param p0, "reciept"    # Ljava/lang/String;

    .prologue
    .line 246
    return-void
.end method

.method public static tabs(Lorg/json/JSONObject;)V
    .locals 0
    .param p0, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 136
    return-void
.end method

.method public static transcription(Ljava/lang/String;)V
    .locals 0
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 96
    return-void
.end method

.method public static tts(Ljava/lang/String;)V
    .locals 0
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 224
    return-void
.end method

.method public static ttsKey(Ljava/lang/String;)V
    .locals 0
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 251
    return-void
.end method
