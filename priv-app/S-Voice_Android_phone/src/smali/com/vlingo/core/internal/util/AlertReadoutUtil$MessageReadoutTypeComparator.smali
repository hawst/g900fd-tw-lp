.class Lcom/vlingo/core/internal/util/AlertReadoutUtil$MessageReadoutTypeComparator;
.super Ljava/lang/Object;
.source "AlertReadoutUtil.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/AlertReadoutUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MessageReadoutTypeComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;)I
    .locals 4
    .param p1, "lhs"    # Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;
    .param p2, "rhs"    # Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .prologue
    .line 278
    invoke-virtual {p2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMostRecentTimestamp()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMostRecentTimestamp()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 273
    check-cast p1, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/core/internal/util/AlertReadoutUtil$MessageReadoutTypeComparator;->compare(Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;)I

    move-result v0

    return v0
.end method
