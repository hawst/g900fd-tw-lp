.class public final Lcom/vlingo/core/internal/debug/AutomationTestsProxy;
.super Ljava/lang/Object;
.source "AutomationTestsProxy.java"


# static fields
.field private static instance:Lcom/vlingo/core/internal/debug/AutomationTestsProxy;


# instance fields
.field private externalListener:Lcom/vlingo/core/internal/debug/IAutomationTestsListener;

.field private settings:Lcom/vlingo/core/internal/debug/AutomationTestsSettings;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method

.method public static getInstance()Lcom/vlingo/core/internal/debug/AutomationTestsProxy;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/vlingo/core/internal/debug/AutomationTestsProxy;->instance:Lcom/vlingo/core/internal/debug/AutomationTestsProxy;

    if-nez v0, :cond_0

    .line 26
    new-instance v0, Lcom/vlingo/core/internal/debug/AutomationTestsProxy;

    invoke-direct {v0}, Lcom/vlingo/core/internal/debug/AutomationTestsProxy;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/debug/AutomationTestsProxy;->instance:Lcom/vlingo/core/internal/debug/AutomationTestsProxy;

    .line 28
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/debug/AutomationTestsProxy;->instance:Lcom/vlingo/core/internal/debug/AutomationTestsProxy;

    return-object v0
.end method

.method private parseAudioType(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;
    .locals 3
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 124
    const-string/jumbo v0, "PCM_8k"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    sget-object v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;->PCM_8KHZ_16BIT:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    .line 128
    :goto_0
    return-object v0

    .line 127
    :cond_0
    const-string/jumbo v0, "PCM_16k"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 128
    sget-object v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;->PCM_16KHZ_16BIT:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    goto :goto_0

    .line 130
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No such type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method addAudioFileUtt(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "audioType"    # Ljava/lang/String;

    .prologue
    .line 99
    if-eqz p1, :cond_0

    const-string/jumbo v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 103
    invoke-static {}, Lcom/vlingo/core/internal/debug/UttsProvider;->getInstance()Lcom/vlingo/core/internal/debug/UttsProvider;

    move-result-object v0

    new-instance v1, Lcom/vlingo/core/internal/debug/actions/AudioFileUtt;

    invoke-direct {p0, p2}, Lcom/vlingo/core/internal/debug/AutomationTestsProxy;->parseAudioType(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Lcom/vlingo/core/internal/debug/actions/AudioFileUtt;-><init>(Ljava/lang/String;Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;)V

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/debug/UttsProvider;->addUtt(Lcom/vlingo/core/internal/debug/actions/Utt;)V

    .line 109
    :cond_0
    return-void
.end method

.method addTextUtt(Ljava/lang/String;)V
    .locals 2
    .param p1, "utt"    # Ljava/lang/String;

    .prologue
    .line 114
    invoke-static {}, Lcom/vlingo/core/internal/debug/UttsProvider;->getInstance()Lcom/vlingo/core/internal/debug/UttsProvider;

    move-result-object v0

    new-instance v1, Lcom/vlingo/core/internal/debug/actions/TextUtt;

    invoke-direct {v1, p1}, Lcom/vlingo/core/internal/debug/actions/TextUtt;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/debug/UttsProvider;->addUtt(Lcom/vlingo/core/internal/debug/actions/Utt;)V

    .line 115
    return-void
.end method

.method changeLanguage(Ljava/lang/String;)V
    .locals 1
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/AutomationTestsProxy;->externalListener:Lcom/vlingo/core/internal/debug/IAutomationTestsListener;

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/AutomationTestsProxy;->externalListener:Lcom/vlingo/core/internal/debug/IAutomationTestsListener;

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/debug/IAutomationTestsListener;->onLanguageChanged(Ljava/lang/String;)V

    .line 79
    :cond_0
    return-void
.end method

.method clearUtts()V
    .locals 1

    .prologue
    .line 120
    invoke-static {}, Lcom/vlingo/core/internal/debug/UttsProvider;->getInstance()Lcom/vlingo/core/internal/debug/UttsProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/debug/UttsProvider;->clearUtts()V

    .line 121
    return-void
.end method

.method disableAutomation()V
    .locals 1

    .prologue
    .line 61
    invoke-static {}, Lcom/vlingo/core/internal/debug/TestWrapper;->unsetExecuteFromAutomationTests()V

    .line 62
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/AutomationTestsProxy;->externalListener:Lcom/vlingo/core/internal/debug/IAutomationTestsListener;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/AutomationTestsProxy;->externalListener:Lcom/vlingo/core/internal/debug/IAutomationTestsListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/debug/IAutomationTestsListener;->onDisabled()V

    .line 68
    :cond_0
    return-void
.end method

.method enableAutomation(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "kpiActive"    # Z
    .param p2, "language"    # Ljava/lang/String;
    .param p3, "audioFileDir"    # Ljava/lang/String;

    .prologue
    .line 40
    new-instance v0, Lcom/vlingo/core/internal/debug/AutomationTestsSettings;

    invoke-direct {v0, p1}, Lcom/vlingo/core/internal/debug/AutomationTestsSettings;-><init>(Z)V

    iput-object v0, p0, Lcom/vlingo/core/internal/debug/AutomationTestsProxy;->settings:Lcom/vlingo/core/internal/debug/AutomationTestsSettings;

    .line 41
    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/AutomationTestsProxy;->clearUtts()V

    .line 42
    invoke-virtual {p0, p3}, Lcom/vlingo/core/internal/debug/AutomationTestsProxy;->setFileDir(Ljava/lang/String;)V

    .line 43
    invoke-static {}, Lcom/vlingo/core/internal/debug/TestWrapper;->setExecuteFromAutomationTests()V

    .line 44
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/AutomationTestsProxy;->externalListener:Lcom/vlingo/core/internal/debug/IAutomationTestsListener;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/AutomationTestsProxy;->externalListener:Lcom/vlingo/core/internal/debug/IAutomationTestsListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/debug/IAutomationTestsListener;->onEnabled()V

    .line 46
    if-eqz p2, :cond_0

    const-string/jumbo v0, ""

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/AutomationTestsProxy;->externalListener:Lcom/vlingo/core/internal/debug/IAutomationTestsListener;

    invoke-interface {v0, p2}, Lcom/vlingo/core/internal/debug/IAutomationTestsListener;->onLanguageChanged(Ljava/lang/String;)V

    .line 56
    :cond_0
    return-void
.end method

.method public getSettings()Lcom/vlingo/core/internal/debug/AutomationTestsSettings;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/AutomationTestsProxy;->settings:Lcom/vlingo/core/internal/debug/AutomationTestsSettings;

    return-object v0
.end method

.method public resetClient()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/AutomationTestsProxy;->externalListener:Lcom/vlingo/core/internal/debug/IAutomationTestsListener;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/AutomationTestsProxy;->externalListener:Lcom/vlingo/core/internal/debug/IAutomationTestsListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/debug/IAutomationTestsListener;->onResetClient()V

    .line 94
    :cond_0
    return-void
.end method

.method setFileDir(Ljava/lang/String;)V
    .locals 1
    .param p1, "fileDir"    # Ljava/lang/String;

    .prologue
    .line 82
    invoke-static {}, Lcom/vlingo/core/internal/debug/UttsProvider;->getInstance()Lcom/vlingo/core/internal/debug/UttsProvider;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/debug/UttsProvider;->setFileDir(Ljava/lang/String;)Z

    .line 83
    return-void
.end method

.method public setListener(Lcom/vlingo/core/internal/debug/IAutomationTestsListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/vlingo/core/internal/debug/IAutomationTestsListener;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/vlingo/core/internal/debug/AutomationTestsProxy;->externalListener:Lcom/vlingo/core/internal/debug/IAutomationTestsListener;

    .line 35
    return-void
.end method
