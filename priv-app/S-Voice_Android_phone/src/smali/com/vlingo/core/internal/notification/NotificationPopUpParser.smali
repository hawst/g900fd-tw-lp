.class Lcom/vlingo/core/internal/notification/NotificationPopUpParser;
.super Lcom/vlingo/core/internal/xml/SimpleXmlParser;
.source "NotificationPopUpParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/notification/NotificationPopUpParser$1;
    }
.end annotation


# instance fields
.field private final ATTR_ACTION_REQUIRED:I

.field private final ATTR_EXIT_ON_DECLINE:I

.field private final ATTR_LNKRES:I

.field private final ATTR_NAME:I

.field private final ATTR_STRRES:I

.field private final ATTR_VAL:I

.field private final ELEM_ALERT_DIALOG:I

.field private final ELEM_LINK:I

.field private final ELEM_LNKRES:I

.field private final ELEM_LOCALE:I

.field private final ELEM_LOCALIZED:I

.field private final ELEM_NEGATIVE_BUTTON:I

.field private final ELEM_NOTIFICATION_TEXT:I

.field private final ELEM_POSITIVE_BUTTON:I

.field private final ELEM_STRRES:I

.field private final ELEM_TITLE:I

.field private final ELEM_TYPE:I

.field private final ELEM_VERSION:I

.field private actionRequired:Ljava/lang/Boolean;

.field private exitOnDecline:Ljava/lang/Boolean;

.field private factory:Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;

.field private final hrefBase:Ljava/lang/String;

.field private i:I

.field private links:[Ljava/lang/String;

.field private lnkResources:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private localizedResources:Z

.field private name:Ljava/lang/String;

.field private negativeButton:Ljava/lang/String;

.field private notificationText:Ljava/lang/String;

.field private final notificationType:Ljava/lang/String;

.field private notifications:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/notification/NotificationPopUp;",
            ">;"
        }
    .end annotation
.end field

.field private positiveButton:Ljava/lang/String;

.field private size:I

.field private strResources:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private title:Ljava/lang/String;

.field private final tosType:Ljava/lang/String;

.field private type:Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

.field private version:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)V
    .locals 1
    .param p1, "factory"    # Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/vlingo/core/internal/xml/SimpleXmlParser;-><init>()V

    .line 41
    const-string/jumbo v0, "TOS"

    iput-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->tosType:Ljava/lang/String;

    .line 42
    const-string/jumbo v0, "Notification"

    iput-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->notificationType:Ljava/lang/String;

    .line 51
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->strResources:Ljava/util/HashMap;

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->localizedResources:Z

    .line 54
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->lnkResources:Ljava/util/HashMap;

    .line 59
    const-string/jumbo v0, "<a href=\"%s\">%s</a>"

    iput-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->hrefBase:Ljava/lang/String;

    .line 65
    iput-object p1, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->factory:Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->notifications:Ljava/util/ArrayList;

    .line 68
    const-string/jumbo v0, "AlertDialog"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_ALERT_DIALOG:I

    .line 69
    const-string/jumbo v0, "type"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_TYPE:I

    .line 70
    const-string/jumbo v0, "version"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_VERSION:I

    .line 71
    const-string/jumbo v0, "Localized"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_LOCALIZED:I

    .line 72
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_LOCALE:I

    .line 73
    const-string/jumbo v0, "StrRes"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_STRRES:I

    .line 74
    const-string/jumbo v0, "LnkRes"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_LNKRES:I

    .line 75
    const-string/jumbo v0, "title"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_TITLE:I

    .line 76
    const-string/jumbo v0, "notificationText"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_NOTIFICATION_TEXT:I

    .line 77
    const-string/jumbo v0, "positiveButtonText"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_POSITIVE_BUTTON:I

    .line 78
    const-string/jumbo v0, "negativeButtonText"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_NEGATIVE_BUTTON:I

    .line 79
    const-string/jumbo v0, "link"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_LINK:I

    .line 81
    const-string/jumbo v0, "exitOnDecline"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ATTR_EXIT_ON_DECLINE:I

    .line 82
    const-string/jumbo v0, "actionRequired"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ATTR_ACTION_REQUIRED:I

    .line 83
    const-string/jumbo v0, "name"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ATTR_NAME:I

    .line 84
    const-string/jumbo v0, "value"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ATTR_VAL:I

    .line 85
    const-string/jumbo v0, "strRes"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ATTR_STRRES:I

    .line 86
    const-string/jumbo v0, "lnkRes"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ATTR_LNKRES:I

    .line 87
    return-void
.end method


# virtual methods
.method public beginElement(ILcom/vlingo/core/internal/xml/XmlAttributes;[CI)V
    .locals 11
    .param p1, "elementType"    # I
    .param p2, "attributes"    # Lcom/vlingo/core/internal/xml/XmlAttributes;
    .param p3, "cData"    # [C
    .param p4, "elementEndPosition"    # I

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v7, 0x0

    .line 98
    iget v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_ALERT_DIALOG:I

    if-ne v6, p1, :cond_1

    .line 101
    iput-object v7, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->type:Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

    .line 102
    iput-object v7, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->version:Ljava/lang/Integer;

    .line 103
    iput-object v7, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->title:Ljava/lang/String;

    .line 104
    iput-object v7, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->notificationText:Ljava/lang/String;

    .line 105
    iput-object v7, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->positiveButton:Ljava/lang/String;

    .line 106
    iput-object v7, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->negativeButton:Ljava/lang/String;

    .line 107
    iget v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ATTR_EXIT_ON_DECLINE:I

    invoke-virtual {p2, v6}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "true"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->exitOnDecline:Ljava/lang/Boolean;

    .line 108
    iget v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ATTR_ACTION_REQUIRED:I

    invoke-virtual {p2, v6}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "true"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->actionRequired:Ljava/lang/Boolean;

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    iget v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_TYPE:I

    if-ne v6, p1, :cond_3

    .line 114
    invoke-static {p3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v4

    .line 115
    .local v4, "s":Ljava/lang/String;
    const-string/jumbo v6, "TOS"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 116
    sget-object v6, Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;->TOS:Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

    iput-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->type:Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

    goto :goto_0

    .line 117
    :cond_2
    const-string/jumbo v6, "Notification"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 118
    sget-object v6, Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;->ALERT:Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

    iput-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->type:Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

    goto :goto_0

    .line 122
    .end local v4    # "s":Ljava/lang/String;
    :cond_3
    iget v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_VERSION:I

    if-ne v6, p1, :cond_4

    .line 125
    invoke-static {p3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->version:Ljava/lang/Integer;

    goto :goto_0

    .line 128
    :cond_4
    iget v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_LOCALIZED:I

    if-ne v6, p1, :cond_5

    .line 129
    new-instance v6, Ljava/util/LinkedHashMap;

    invoke-direct {v6}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->strResources:Ljava/util/HashMap;

    goto :goto_0

    .line 130
    :cond_5
    iget v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_LOCALE:I

    if-ne v6, p1, :cond_6

    .line 131
    iput-boolean v10, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->localizedResources:Z

    goto :goto_0

    .line 134
    :cond_6
    iget v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_STRRES:I

    if-ne v6, p1, :cond_7

    iget-boolean v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->localizedResources:Z

    if-eqz v6, :cond_7

    .line 137
    iget v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ATTR_NAME:I

    invoke-virtual {p2, v6}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v3

    .line 138
    .local v3, "nameAttribute":Ljava/lang/String;
    invoke-static {p3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v5

    .line 139
    .local v5, "value":Ljava/lang/String;
    iget-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->strResources:Ljava/util/HashMap;

    invoke-virtual {v6, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 142
    .end local v3    # "nameAttribute":Ljava/lang/String;
    .end local v5    # "value":Ljava/lang/String;
    :cond_7
    iget v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_LNKRES:I

    if-ne v6, p1, :cond_8

    iget-boolean v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->localizedResources:Z

    if-eqz v6, :cond_8

    .line 145
    iget v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ATTR_NAME:I

    invoke-virtual {p2, v6}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->name:Ljava/lang/String;

    .line 146
    iget v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ATTR_VAL:I

    invoke-virtual {p2, v6}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->size:I

    .line 147
    iput v9, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->i:I

    .line 148
    iget v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->size:I

    new-array v6, v6, [Ljava/lang/String;

    iput-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->links:[Ljava/lang/String;

    goto/16 :goto_0

    .line 151
    :cond_8
    iget v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_LINK:I

    if-ne v6, p1, :cond_9

    iget-boolean v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->localizedResources:Z

    if-eqz v6, :cond_9

    .line 154
    new-instance v0, Ljava/util/Formatter;

    invoke-direct {v0}, Ljava/util/Formatter;-><init>()V

    .line 155
    .local v0, "formatter":Ljava/util/Formatter;
    const-string/jumbo v6, "<a href=\"%s\">%s</a>"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    iget v8, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ATTR_VAL:I

    invoke-virtual {p2, v8}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    iget v8, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ATTR_NAME:I

    invoke-virtual {p2, v8}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {v0, v6, v7}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v1

    .line 159
    .local v1, "href":Ljava/lang/String;
    iget v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->i:I

    iget v7, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->size:I

    if-ge v6, v7, :cond_0

    .line 160
    iget-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->links:[Ljava/lang/String;

    iget v7, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->i:I

    aput-object v1, v6, v7

    .line 161
    iget v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->i:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->i:I

    goto/16 :goto_0

    .line 163
    .end local v0    # "formatter":Ljava/util/Formatter;
    .end local v1    # "href":Ljava/lang/String;
    :cond_9
    iget v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_TITLE:I

    if-ne v6, p1, :cond_a

    .line 166
    iget-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->strResources:Ljava/util/HashMap;

    iget v7, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ATTR_STRRES:I

    invoke-virtual {p2, v7}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iput-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->title:Ljava/lang/String;

    goto/16 :goto_0

    .line 169
    :cond_a
    iget v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_NOTIFICATION_TEXT:I

    if-ne v6, p1, :cond_b

    .line 172
    iget-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->strResources:Ljava/util/HashMap;

    iget v7, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ATTR_STRRES:I

    invoke-virtual {p2, v7}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iput-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->notificationText:Ljava/lang/String;

    .line 173
    iget-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->lnkResources:Ljava/util/HashMap;

    iget v7, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ATTR_LNKRES:I

    invoke-virtual {p2, v7}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 174
    .local v2, "linkRes":[Ljava/lang/String;
    if-eqz v2, :cond_0

    array-length v6, v2

    if-lez v6, :cond_0

    .line 175
    new-instance v0, Ljava/util/Formatter;

    invoke-direct {v0}, Ljava/util/Formatter;-><init>()V

    .line 176
    .restart local v0    # "formatter":Ljava/util/Formatter;
    iget-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->notificationText:Ljava/lang/String;

    check-cast v2, [Ljava/lang/Object;

    .end local v2    # "linkRes":[Ljava/lang/String;
    invoke-virtual {v0, v6, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->notificationText:Ljava/lang/String;

    goto/16 :goto_0

    .line 180
    .end local v0    # "formatter":Ljava/util/Formatter;
    :cond_b
    iget v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_POSITIVE_BUTTON:I

    if-ne v6, p1, :cond_c

    .line 183
    iget-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->strResources:Ljava/util/HashMap;

    iget v7, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ATTR_STRRES:I

    invoke-virtual {p2, v7}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iput-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->positiveButton:Ljava/lang/String;

    goto/16 :goto_0

    .line 186
    :cond_c
    iget v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_NEGATIVE_BUTTON:I

    if-ne v6, p1, :cond_0

    .line 189
    iget-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->strResources:Ljava/util/HashMap;

    iget v7, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ATTR_STRRES:I

    invoke-virtual {p2, v7}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iput-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->negativeButton:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public endDocument()V
    .locals 2

    .prologue
    .line 220
    invoke-super {p0}, Lcom/vlingo/core/internal/xml/SimpleXmlParser;->endDocument()V

    .line 223
    iget-object v1, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->notifications:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/notification/NotificationPopUp;

    goto :goto_0

    .line 227
    :cond_0
    return-void
.end method

.method public endElement(II)V
    .locals 10
    .param p1, "elementType"    # I
    .param p2, "elementStartPosition"    # I

    .prologue
    .line 197
    iget v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_ALERT_DIALOG:I

    if-ne v0, p1, :cond_1

    .line 198
    iget-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->notificationText:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    sget-object v0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser$1;->$SwitchMap$com$vlingo$core$internal$notification$NotificationPopUp$Type:[I

    iget-object v1, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->type:Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 201
    :pswitch_0
    iget-object v9, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->notifications:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->factory:Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;

    sget-object v1, Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;->TOS:Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

    iget-object v2, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->version:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->title:Ljava/lang/String;

    iget-object v4, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->notificationText:Ljava/lang/String;

    iget-object v5, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->negativeButton:Ljava/lang/String;

    iget-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->positiveButton:Ljava/lang/String;

    iget-object v7, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->exitOnDecline:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    iget-object v8, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->actionRequired:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    invoke-virtual/range {v0 .. v8}, Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;->getNotification(Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/vlingo/core/internal/notification/NotificationPopUp;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 204
    :pswitch_1
    iget-object v9, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->notifications:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->factory:Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;

    sget-object v1, Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;->ALERT:Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

    iget-object v2, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->version:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->title:Ljava/lang/String;

    iget-object v4, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->notificationText:Ljava/lang/String;

    iget-object v5, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->negativeButton:Ljava/lang/String;

    iget-object v6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->positiveButton:Ljava/lang/String;

    iget-object v7, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->exitOnDecline:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    iget-object v8, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->actionRequired:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    invoke-virtual/range {v0 .. v8}, Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;->getNotification(Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/vlingo/core/internal/notification/NotificationPopUp;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 211
    :cond_1
    iget v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_LOCALE:I

    if-ne v0, p1, :cond_2

    .line 212
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->localizedResources:Z

    goto :goto_0

    .line 213
    :cond_2
    iget v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->ELEM_LNKRES:I

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->localizedResources:Z

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->lnkResources:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->links:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 199
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getFirstNotification()Lcom/vlingo/core/internal/notification/NotificationPopUp;
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->notifications:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->notifications:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/notification/NotificationPopUp;

    .line 236
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNotifications()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/notification/NotificationPopUp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 230
    iget-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUpParser;->notifications:Ljava/util/ArrayList;

    return-object v0
.end method

.method public onParseBegin([C)V
    .locals 0
    .param p1, "xml"    # [C

    .prologue
    .line 91
    invoke-super {p0, p1}, Lcom/vlingo/core/internal/xml/SimpleXmlParser;->onParseBegin([C)V

    .line 94
    return-void
.end method
