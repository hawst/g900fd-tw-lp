.class public Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
.super Ljava/lang/Object;
.source "MessageType.java"


# instance fields
.field private address:Ljava/lang/String;

.field private attachments:I

.field private contactName:Ljava/lang/String;

.field private id:J

.field private message:Ljava/lang/String;

.field private recipients:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;",
            ">;"
        }
    .end annotation
.end field

.field private subject:Ljava/lang/String;

.field private timeStamp:J

.field private type:Ljava/lang/String;

.field private unreadCount:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "contactName"    # Ljava/lang/String;
    .param p3, "address"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->attachments:I

    .line 31
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->id:J

    .line 34
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->timeStamp:J

    .line 38
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->message:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->contactName:Ljava/lang/String;

    .line 40
    iput-object p3, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->address:Ljava/lang/String;

    .line 42
    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->recipients:Ljava/util/List;

    .line 43
    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->subject:Ljava/lang/String;

    .line 44
    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->type:Ljava/lang/String;

    .line 45
    return-void
.end method


# virtual methods
.method public addRecipient(Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;)V
    .locals 5
    .param p1, "recipient"    # Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    .prologue
    .line 76
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->recipients:Ljava/util/List;

    if-nez v3, :cond_1

    .line 77
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->recipients:Ljava/util/List;

    .line 78
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->recipients:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    const/4 v0, 0x0

    .line 81
    .local v0, "exist":Z
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->recipients:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    .line 82
    .local v2, "recipientType":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 83
    const/4 v0, 0x1

    .line 87
    .end local v2    # "recipientType":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    :cond_3
    if-nez v0, :cond_0

    .line 88
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->recipients:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 137
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    .line 158
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 141
    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    .line 143
    .local v0, "messageObj":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->address:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->address:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 146
    :cond_2
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->contactName:Ljava/lang/String;

    if-nez v2, :cond_8

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->contactName:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 149
    :cond_3
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->recipients:Ljava/util/List;

    if-nez v2, :cond_9

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->recipients:Ljava/util/List;

    if-nez v2, :cond_0

    .line 152
    :cond_4
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->type:Ljava/lang/String;

    if-nez v2, :cond_a

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->type:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 155
    :cond_5
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->message:Ljava/lang/String;

    if-nez v2, :cond_b

    iget-object v2, v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->message:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 158
    :cond_6
    const/4 v1, 0x1

    goto :goto_0

    .line 143
    :cond_7
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->address:Ljava/lang/String;

    iget-object v3, v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->address:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0

    .line 146
    :cond_8
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->contactName:Ljava/lang/String;

    iget-object v3, v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->contactName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    .line 149
    :cond_9
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->recipients:Ljava/util/List;

    iget-object v3, v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->recipients:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    goto :goto_0

    .line 152
    :cond_a
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->type:Ljava/lang/String;

    iget-object v3, v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->type:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_0

    .line 155
    :cond_b
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->message:Ljava/lang/String;

    iget-object v3, v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    goto :goto_0
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getAttachments()I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->attachments:I

    return v0
.end method

.method public getContactName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->contactName:Ljava/lang/String;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 123
    iget-wide v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->id:J

    return-wide v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getRecipients()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->recipients:Ljava/util/List;

    return-object v0
.end method

.method public getSubject()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->subject:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeStamp()J
    .locals 2

    .prologue
    .line 127
    iget-wide v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->timeStamp:J

    return-wide v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getUnreadCount()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->unreadCount:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 163
    const/4 v0, 0x1

    .line 164
    .local v0, "hash":I
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->address:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 165
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->address:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit8 v0, v1, 0x11

    .line 167
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->contactName:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 168
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->contactName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 170
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->type:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 171
    mul-int/lit8 v1, v0, 0xd

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->type:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 173
    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->message:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 174
    mul-int/lit8 v1, v0, 0x11

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->message:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 176
    :cond_3
    return v0
.end method

.method public isSimple()Z
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x1

    return v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->address:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public setAttachments(I)V
    .locals 0
    .param p1, "attachments"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->attachments:I

    .line 24
    return-void
.end method

.method public setContactName(Ljava/lang/String;)V
    .locals 0
    .param p1, "contactName"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->contactName:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public setId(J)V
    .locals 0
    .param p1, "id"    # J

    .prologue
    .line 119
    iput-wide p1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->id:J

    .line 120
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->message:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 0
    .param p1, "subj"    # Ljava/lang/String;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->subject:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public setTimeStamp(J)V
    .locals 0
    .param p1, "timeStamp"    # J

    .prologue
    .line 131
    iput-wide p1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->timeStamp:J

    .line 132
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->type:Ljava/lang/String;

    .line 108
    return-void
.end method

.method public setUnreadCount(I)V
    .locals 0
    .param p1, "unreadcount"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->unreadCount:I

    .line 52
    return-void
.end method
