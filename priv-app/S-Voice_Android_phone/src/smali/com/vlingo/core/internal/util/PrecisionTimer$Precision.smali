.class public final enum Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;
.super Ljava/lang/Enum;
.source "PrecisionTimer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/PrecisionTimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Precision"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

.field public static final enum HOURS:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

.field public static final enum MICROSECONDS:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

.field public static final enum MILLISECONDS:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

.field public static final enum MINUTES:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

.field public static final enum NANOSECONDS:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

.field public static final enum SECONDS:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 14
    new-instance v0, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    const-string/jumbo v1, "NANOSECONDS"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->NANOSECONDS:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    new-instance v0, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    const-string/jumbo v1, "MICROSECONDS"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->MICROSECONDS:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    new-instance v0, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    const-string/jumbo v1, "MILLISECONDS"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->MILLISECONDS:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    new-instance v0, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    const-string/jumbo v1, "SECONDS"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->SECONDS:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    new-instance v0, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    const-string/jumbo v1, "MINUTES"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->MINUTES:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    new-instance v0, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    const-string/jumbo v1, "HOURS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->HOURS:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    sget-object v1, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->NANOSECONDS:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->MICROSECONDS:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->MILLISECONDS:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->SECONDS:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->MINUTES:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->HOURS:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->$VALUES:[Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->$VALUES:[Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    return-object v0
.end method
