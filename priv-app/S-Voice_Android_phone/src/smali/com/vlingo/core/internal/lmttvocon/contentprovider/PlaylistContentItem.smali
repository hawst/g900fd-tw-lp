.class public Lcom/vlingo/core/internal/lmttvocon/contentprovider/PlaylistContentItem;
.super Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
.source "PlaylistContentItem.java"


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;)V
    .locals 0
    .param p1, "id"    # J
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;-><init>(J)V

    .line 13
    iput-object p3, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/PlaylistContentItem;->name:Ljava/lang/String;

    .line 14
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 32
    if-nez p1, :cond_1

    .line 39
    :cond_0
    :goto_0
    return v1

    .line 35
    :cond_1
    instance-of v2, p1, Lcom/vlingo/core/internal/lmttvocon/contentprovider/PlaylistContentItem;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 36
    check-cast v0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/PlaylistContentItem;

    .line 37
    .local v0, "playlist":Lcom/vlingo/core/internal/lmttvocon/contentprovider/PlaylistContentItem;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/PlaylistContentItem;->getId()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/PlaylistContentItem;->getId()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/PlaylistContentItem;->name:Ljava/lang/String;

    if-nez v2, :cond_2

    iget-object v2, v0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/PlaylistContentItem;->name:Ljava/lang/String;

    if-nez v2, :cond_0

    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/PlaylistContentItem;->name:Ljava/lang/String;

    iget-object v3, v0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/PlaylistContentItem;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public getChecksum()I
    .locals 4

    .prologue
    .line 18
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/PlaylistContentItem;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/PlaylistContentItem;->name:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/PlaylistContentItem;->makeChecksum([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/PlaylistContentItem;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getOrthographies()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/PlaylistContentItem;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/PlaylistContentItem;->name:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/PlaylistContentItem;->getChecksum()I

    move-result v0

    return v0
.end method
