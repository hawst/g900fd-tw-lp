.class public final Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;
.super Ljava/lang/Object;
.source "VVSDispatcher.java"


# static fields
.field private static final HANDLERS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;",
            ">;>;"
        }
    .end annotation
.end field

.field static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 87
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->TAG:Ljava/lang/String;

    .line 89
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->HANDLERS:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/sdk/recognition/VLActionEvaluator;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)Z
    .locals 9
    .param p0, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p1, "evaluator"    # Lcom/vlingo/sdk/recognition/VLActionEvaluator;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .param p3, "turn"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .prologue
    const/4 v5, 0x0

    .line 250
    const-string/jumbo v6, "test_resolve_contact"

    invoke-static {v6, v5}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 251
    const-string/jumbo v6, "in VVSDispatcher"

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/SettingsTestResolveContactHelper;->saveTimeResolveContact(Ljava/lang/String;Ljava/lang/Object;)V

    .line 253
    :cond_0
    invoke-virtual {p1, p0}, Lcom/vlingo/sdk/recognition/VLActionEvaluator;->evaluateAction(Lcom/vlingo/sdk/recognition/VLAction;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 299
    :cond_1
    :goto_0
    return v5

    .line 259
    :cond_2
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->HANDLERS:Ljava/util/Map;

    invoke-interface {p0}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Class;

    .line 260
    .local v3, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;>;"
    if-eqz v3, :cond_3

    .line 261
    const/4 v2, 0x0

    .line 263
    .local v2, "handler":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
    :try_start_0
    invoke-virtual {v3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "handler":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
    check-cast v2, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 271
    .restart local v2    # "handler":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
    if-eqz v2, :cond_1

    .line 273
    :try_start_1
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "handler Class: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    invoke-virtual {v2, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->setListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 275
    invoke-virtual {v2, p3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->setTurn(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 276
    invoke-virtual {v2, p0, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    :try_end_1
    .catch Ljava/security/InvalidParameterException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v5

    goto :goto_0

    .line 264
    .end local v2    # "handler":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
    :catch_0
    move-exception v0

    .line 265
    .local v0, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 267
    .end local v0    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v0

    .line 268
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 278
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    .restart local v2    # "handler":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
    :catch_2
    move-exception v0

    .line 279
    .local v0, "e":Ljava/security/InvalidParameterException;
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;

    invoke-virtual {v0}, Ljava/security/InvalidParameterException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;-><init>(Ljava/lang/String;)V

    .line 280
    .local v1, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;
    invoke-interface {p2, v1, p3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    goto :goto_0

    .line 285
    .end local v0    # "e":Ljava/security/InvalidParameterException;
    .end local v1    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;
    .end local v2    # "handler":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Action "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {p0}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " not handled"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 297
    .local v4, "msg":Ljava/lang/String;
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->TAG:Ljava/lang/String;

    invoke-static {v6, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static processActionList(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)I
    .locals 5
    .param p1, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .param p2, "turn"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogTurn;",
            ")I"
        }
    .end annotation

    .prologue
    .line 220
    .local p0, "actionList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/VLAction;>;"
    const/4 v1, 0x0

    .line 223
    .local v1, "asyncHandlerCount":I
    if-eqz p0, :cond_1

    .line 224
    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/vvs/PhoenixActionEvaluator;

    invoke-direct {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/PhoenixActionEvaluator;-><init>()V

    .line 225
    .local v2, "evaluator":Lcom/vlingo/core/internal/dialogmanager/vvs/PhoenixActionEvaluator;
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/recognition/VLAction;

    .line 226
    .local v0, "action":Lcom/vlingo/sdk/recognition/VLAction;
    if-eqz v0, :cond_0

    .line 229
    invoke-static {v0, v2, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/sdk/recognition/VLActionEvaluator;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 230
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 238
    .end local v0    # "action":Lcom/vlingo/sdk/recognition/VLAction;
    .end local v2    # "evaluator":Lcom/vlingo/core/internal/dialogmanager/vvs/PhoenixActionEvaluator;
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    return v1
.end method

.method public static registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V
    .locals 1
    .param p0, "key"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112
    .local p1, "handler":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;>;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Ljava/lang/String;Ljava/lang/Class;)V

    .line 113
    return-void
.end method

.method public static registerHandler(Ljava/lang/String;Ljava/lang/Class;)V
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "handler":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;>;"
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->HANDLERS:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    return-void
.end method

.method public static registerVersionSpecificHandlers()V
    .locals 3

    .prologue
    .line 199
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 200
    .local v0, "ver":I
    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    .line 201
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_SET_ALARM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v2, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetAlarmHandler;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 203
    :cond_0
    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 204
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->RESOLVE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v2, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 205
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_APPOINTMENTS:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v2, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowAppointmentsHandler;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 206
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_APPOINTMENT_CHOICES:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v2, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowAppointmentChoicesHandler;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 207
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CREATE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v2, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 208
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_DELETE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v2, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/delete/ShowDeleteAppointmentHandler;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 209
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_EDIT_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v2, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/edit/ShowModifyAppointmentHandler;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 212
    :cond_1
    return-void
.end method

.method public static setupStandardMappings()V
    .locals 2

    .prologue
    .line 137
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->ADDRESS_BOOK:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 138
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->ANSWER_QUESTION:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 139
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->CALENDAR_READBACK:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/CheckScheduleHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 140
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->CALL_CONTACT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CallContactHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 141
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->CANCEL:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/CancelActionHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 142
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->CHANGE_VOLUME:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/VolumeHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 143
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->CONTACT_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 144
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->DATE_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/DateLookupHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 145
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->DEFAULT_WEB_SEARCH:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 146
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->DIALOG_CANCEL:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/DialogCancelHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 147
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->DIAL_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/VoiceDialPageHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 148
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->EVENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/EventHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 149
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->EXECUTE_UNKNOWN_DEF_SEARCH:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ExecuteUnkownDefSearchHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 150
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->INTENT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 151
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->LISTEN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ListenHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 152
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->LP_ACTION:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/LPActionHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 153
    const-string/jumbo v0, "ListPosition"

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ListPositionActionHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Ljava/lang/String;Ljava/lang/Class;)V

    .line 154
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->MAP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/MapHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 155
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->NAVIGATE_HOME:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/NavigateHomeHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 156
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->NAVIGATE_HOME_KOREAN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/NavigateHomeKoreanHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 157
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SAMSUNG_NAVIGATE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/NavHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 158
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->PLAY_MEDIA:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/PlayMediaHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 159
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->POPULATE_TEXTBOX:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ControllerPassThruHanlder;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 160
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->RESOLVE_CONTACT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 161
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SAFEREADER_REPLY:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SafeReaderReplyHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 162
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SEARCH_WEB_PROMPT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SearchWebPromptHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 163
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SEND_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SendMessageHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 164
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SET_CONFIG:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SetConfigHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 165
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SET_PARAMS:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SetParamsHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 166
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SET_TURN_PARAMS:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SetTurnParamsHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 167
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SETTING_CHANGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/SettingChangeHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 168
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CALL:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallWidgetHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 169
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CALL_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowCallMessageWidgetHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 170
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_COMPOSE_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 171
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CONTACT_CHOICES:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowContactChoicesHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 172
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_CONTACT_TYPE_CHOICES:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ShowContactTypeChoicesHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 173
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_FORWARD_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeForwardMessageHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 174
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ShowServerMessageHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 175
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_0PEN_APP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowOpenAppWidgetHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 176
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_REPLY_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeReplyMessageHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 177
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_SET_TIMER:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetTimerHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 178
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_SYSTEM_TURN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ShowSystemTurnHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 179
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_USER_TURN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ShowUserTurnHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 180
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SMS_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/SMSPageHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 181
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SPEAK_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SpeakMessageHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 182
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->UPDATE_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ControllerPassThruHanlder;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 183
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_LOCAL_SEARCH:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 184
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->WEATHER_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 185
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->ONE_ROUND_WEATHER_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/OneRoundWeatherHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 186
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->WEB_SEARCH_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/EngineWebSearchHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 187
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_WEB_SEARCH_BUTTON:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/WebSearchButtonHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 188
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerVersionSpecificHandlers()V

    .line 189
    return-void
.end method

.method public static unregisterHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;)V
    .locals 1
    .param p0, "key"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->unregisterHandler(Ljava/lang/String;)V

    .line 126
    return-void
.end method

.method public static unregisterHandler(Ljava/lang/String;)V
    .locals 1
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 116
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->HANDLERS:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    return-void
.end method
