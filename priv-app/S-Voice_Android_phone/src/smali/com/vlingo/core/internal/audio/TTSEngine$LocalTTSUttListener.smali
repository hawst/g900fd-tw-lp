.class Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSUttListener;
.super Ljava/lang/Object;
.source "TTSEngine.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/audio/TTSEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LocalTTSUttListener"
.end annotation


# instance fields
.field volatile isComplete:Z

.field final synthetic this$0:Lcom/vlingo/core/internal/audio/TTSEngine;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/audio/TTSEngine;)V
    .locals 1

    .prologue
    .line 335
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSUttListener;->this$0:Lcom/vlingo/core/internal/audio/TTSEngine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSUttListener;->isComplete:Z

    return-void
.end method


# virtual methods
.method declared-synchronized isComplete()Z
    .locals 1

    .prologue
    .line 346
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSUttListener;->isComplete:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onUtteranceCompleted(Ljava/lang/String;)V
    .locals 2
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    .line 340
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSUttListener;->this$0:Lcom/vlingo/core/internal/audio/TTSEngine;

    # getter for: Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/TTSEngine;->access$200(Lcom/vlingo/core/internal/audio/TTSEngine;)Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSUttListener;->this$0:Lcom/vlingo/core/internal/audio/TTSEngine;

    # getter for: Lcom/vlingo/core/internal/audio/TTSEngine;->localTTSEngine:Lcom/vlingo/core/internal/audio/TTSLocalEngine;
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/TTSEngine;->access$200(Lcom/vlingo/core/internal/audio/TTSEngine;)Lcom/vlingo/core/internal/audio/TTSLocalEngine;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/audio/TTSLocalEngine;->setOnUtteranceCompletedListener(Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;)I

    .line 343
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSUttListener;->isComplete:Z

    .line 344
    return-void
.end method
