.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSAudioPlaybackListener;
.super Ljava/lang/Object;
.source "PhraseSpotter.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PSAudioPlaybackListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)V
    .locals 0

    .prologue
    .line 435
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSAudioPlaybackListener;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 468
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSAudioPlaybackListener;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    const/4 v1, 0x0

    # setter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->audioPlaying:Z
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$1302(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Z)Z

    .line 469
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSAudioPlaybackListener;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->updatePhraseSpotterState()V
    invoke-static {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$1200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)V

    .line 470
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 450
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSAudioPlaybackListener;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    const/4 v1, 0x0

    # setter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->audioPlaying:Z
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$1302(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Z)Z

    .line 451
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSAudioPlaybackListener;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->updatePhraseSpotterState()V
    invoke-static {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$1200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)V

    .line 452
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 459
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSAudioPlaybackListener;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    const/4 v1, 0x0

    # setter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->audioPlaying:Z
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$1302(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Z)Z

    .line 460
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSAudioPlaybackListener;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->updatePhraseSpotterState()V
    invoke-static {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$1200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)V

    .line 461
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 441
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSAudioPlaybackListener;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    const/4 v1, 0x1

    # setter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->audioPlaying:Z
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$1302(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Z)Z

    .line 442
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSAudioPlaybackListener;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->updatePhraseSpotterState()V
    invoke-static {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$1200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)V

    .line 443
    return-void
.end method
