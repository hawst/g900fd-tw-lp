.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
.super Ljava/lang/Object;
.source "PhraseSpotterControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpottingState;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StartingState;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IdleState;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;
    }
.end annotation


# static fields
.field private static volatile smInstancesCounter:I


# instance fields
.field private mAudioLogger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;

.field private volatile mCurrentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

.field private mDialogFlowTaskRegulator:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;

.field private final mIdleState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

.field private mInstanceSerialNumber:I

.field private mListener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

.field private mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

.field private volatile mRecordThread:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;

.field private volatile mRestartWhenIdle:Z

.field private mSpotterThreadCount:I

.field private final mSpottingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

.field private final mStartingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

.field private final mStateTransitionLock:Ljava/lang/Object;

.field private final mStoppingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

.field mVlRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

.field private resumeControlOnPhraseSpotterStopped:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    sput v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->smInstancesCounter:I

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mVlRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    .line 68
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStateTransitionLock:Ljava/lang/Object;

    .line 70
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;

    invoke-direct {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mAudioLogger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;

    .line 118
    sget v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->smInstancesCounter:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->smInstancesCounter:I

    .line 119
    sget v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->smInstancesCounter:I

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mInstanceSerialNumber:I

    .line 121
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IdleState;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IdleState;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mIdleState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    .line 122
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpottingState;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpottingState;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mSpottingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    .line 123
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStoppingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    .line 124
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StartingState;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StartingState;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStartingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    .line 127
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mIdleState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mCurrentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    .line 128
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRestartWhenIdle:Z

    return v0
.end method

.method static synthetic access$002(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRestartWhenIdle:Z

    return p1
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStateTransitionLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mIdleState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->resumeControlOnPhraseSpotterStopped:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
    .param p1, "x1"    # Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->resumeControlOnPhraseSpotterStopped:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mAudioLogger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mCurrentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mSpottingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    return-object v0
.end method

.method static synthetic access$202(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
    .param p1, "x1"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    return-object p1
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mListener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    return-object v0
.end method

.method static synthetic access$302(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
    .param p1, "x1"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mListener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    return-object p1
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStartingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
    .param p1, "x1"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V

    return-void
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStoppingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;

    return-object v0
.end method

.method static synthetic access$702(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
    .param p1, "x1"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;

    return-object p1
.end method

.method static synthetic access$800(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 49
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mInstanceSerialNumber:I

    return v0
.end method

.method static synthetic access$904(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .prologue
    .line 49
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mSpotterThreadCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mSpotterThreadCount:I

    return v0
.end method

.method public static getTag()Ljava/lang/String;
    .locals 2

    .prologue
    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "PhraseSpotterControl:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V
    .locals 4
    .param p1, "state"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    .prologue
    .line 196
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStateTransitionLock:Ljava/lang/Object;

    monitor-enter v1

    .line 197
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setState() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mCurrentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " ==> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mCurrentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->onExit()V

    .line 199
    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mIdleState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    .end local p1    # "state":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    :cond_0
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mCurrentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    .line 200
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mCurrentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->onEnter()V

    .line 201
    monitor-exit v1

    .line 202
    return-void

    .line 201
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public getDialogFlowTaskRegulator()Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mDialogFlowTaskRegulator:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;

    return-object v0
.end method

.method isInIdleState()Z
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mCurrentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mIdleState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isInSpottingState()Z
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mCurrentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mSpottingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isInStartingState()Z
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mCurrentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStartingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isInStoppingState()Z
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mCurrentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStoppingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isListening()Z
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mCurrentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mSpottingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mCurrentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStartingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDialogFlowTaskRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V
    .locals 0
    .param p1, "dialogFlowTaskRegulator"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mDialogFlowTaskRegulator:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;

    .line 82
    return-void
.end method

.method public setRecognitionContext(Lcom/vlingo/sdk/recognition/VLRecognitionContext;)V
    .locals 0
    .param p1, "srContext"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mVlRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    .line 132
    return-void
.end method

.method start(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V
    .locals 1
    .param p1, "params"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .param p2, "listener"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    .prologue
    .line 157
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mCurrentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    invoke-virtual {v0, p1, p2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->start(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V

    .line 158
    return-void
.end method

.method stop()V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mCurrentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->stop()V

    .line 164
    return-void
.end method

.method stop(Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)V
    .locals 3
    .param p1, "resumeControl"    # Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .prologue
    .line 170
    const/4 v0, 0x1

    .line 171
    .local v0, "resumeControlImmediately":Z
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStateTransitionLock:Ljava/lang/Object;

    monitor-enter v2

    .line 172
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->isInIdleState()Z

    move-result v1

    if-nez v1, :cond_1

    .line 173
    const/4 v0, 0x0

    .line 174
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->resumeControlOnPhraseSpotterStopped:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    if-eqz v1, :cond_0

    .line 177
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->resumeControlOnPhraseSpotterStopped:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/ResumeControl;->resume()V

    .line 179
    :cond_0
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->resumeControlOnPhraseSpotterStopped:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .line 180
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mCurrentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->stop()V

    .line 182
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    if-eqz v0, :cond_2

    .line 184
    invoke-interface {p1}, Lcom/vlingo/core/internal/dialogmanager/ResumeControl;->resume()V

    .line 186
    :cond_2
    return-void

    .line 182
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
