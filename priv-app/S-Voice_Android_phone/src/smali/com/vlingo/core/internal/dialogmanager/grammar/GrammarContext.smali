.class public final Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
.super Ljava/lang/Object;
.source "GrammarContext.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$1;,
        Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;
    }
.end annotation


# instance fields
.field private generalSpeakables:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private listItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    # getter for: Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;->generalSpeakables:Ljava/util/List;
    invoke-static {p1}, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;->access$000(Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;->generalSpeakables:Ljava/util/List;

    .line 33
    # getter for: Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;->listItems:Ljava/util/List;
    invoke-static {p1}, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;->access$100(Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;->listItems:Ljava/util/List;

    .line 34
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$1;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;-><init>(Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;)V

    return-void
.end method


# virtual methods
.method public addGeneralSpeakable(Ljava/lang/String;)V
    .locals 2
    .param p1, "generalSpeakable"    # Ljava/lang/String;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;->generalSpeakables:Ljava/util/List;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;->generalSpeakables:Ljava/util/List;

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;->generalSpeakables:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    return-void
.end method

.method public addListItem(Ljava/lang/String;)V
    .locals 1
    .param p1, "item"    # Ljava/lang/String;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;->listItems:Ljava/util/List;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;->listItems:Ljava/util/List;

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;->listItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    return-void
.end method

.method public getGeneralSpeakables()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;->generalSpeakables:Ljava/util/List;

    return-object v0
.end method

.method public getListItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;->listItems:Ljava/util/List;

    return-object v0
.end method
