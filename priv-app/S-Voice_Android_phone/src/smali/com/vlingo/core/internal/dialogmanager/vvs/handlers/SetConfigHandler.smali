.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SetConfigHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "SetConfigHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 4
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v3, 0x0

    .line 28
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 29
    const-string/jumbo v2, "dynamic_config_disabled"

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 32
    .local v1, "isDynamicConfigDisabled":Z
    if-nez v1, :cond_0

    .line 33
    const-string/jumbo v2, "Config"

    invoke-interface {p1, v2}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "configXML":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 35
    new-instance v2, Lcom/vlingo/core/internal/vlservice/VLConfigParser;

    invoke-direct {v2}, Lcom/vlingo/core/internal/vlservice/VLConfigParser;-><init>()V

    invoke-static {v0, v2, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VLConfigImporter;->importSettings(Ljava/lang/String;Lcom/vlingo/core/internal/vlservice/VLConfigParser;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 36
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/VLSdk;->doesUseEDM()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 37
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/VLSdk;->getEmbeddedDialogManager()Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;->updateDynamicFeaturesConfig()V

    .line 41
    .end local v0    # "configXML":Ljava/lang/String;
    :cond_0
    return v3
.end method
