.class public Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentChoicesController;
.super Lcom/vlingo/core/internal/dialogmanager/Controller;
.source "ShowAppointmentChoicesController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# instance fields
.field private listUtil:Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;"
        }
    .end annotation
.end field

.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/Controller;-><init>()V

    .line 27
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentChoicesController;->listUtil:Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 7
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 35
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/Controller;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 36
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentChoicesController;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 37
    const-string/jumbo v0, "ListPosition"

    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    const-string/jumbo v0, "choices"

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    .line 41
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v0

    const-string/jumbo v1, "appointment"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentChoicesController;->listUtil:Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CALENDAR_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowEventChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v6, 0x0

    move-object v1, p2

    move-object v2, p1

    move-object v5, p0

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->showChoiceListWidget(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;Ljava/util/Map;)Z

    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, -0x1

    .line 59
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 61
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTADDMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->getValue()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentChoicesController;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getFieldId()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->getFieldId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 62
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 63
    .local v0, "calendarIntent":Landroid/content/Intent;
    const-string/jumbo v3, "com.android.calendar"

    const-string/jumbo v4, "com.android.calendar.EventInfoActivity"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    const-string/jumbo v3, "uri"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 66
    const-string/jumbo v3, "title"

    const-string/jumbo v4, "title"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 67
    const-string/jumbo v3, "beginTime"

    const-string/jumbo v4, "beginTime"

    invoke-virtual {p1, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 68
    const-string/jumbo v3, "endTime"

    const-string/jumbo v4, "endTime"

    invoke-virtual {p1, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 69
    const/high16 v3, 0x10000000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 70
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentChoicesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 71
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentChoicesController;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->buildFromString(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 81
    .end local v0    # "calendarIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 73
    :cond_0
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentChoicesController;->listUtil:Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;

    invoke-virtual {v3, p1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->getIdOfSelection(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    .line 74
    .local v2, "id":Ljava/lang/String;
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/events/ChoiceSelectedEvent;

    invoke-direct {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/events/ChoiceSelectedEvent;-><init>(Ljava/lang/String;)V

    .line 75
    .local v1, "event":Lcom/vlingo/core/internal/dialogmanager/events/ChoiceSelectedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentChoicesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentChoicesController;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v3, v1, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    goto :goto_0

    .line 79
    .end local v1    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ChoiceSelectedEvent;
    .end local v2    # "id":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/ShowAppointmentChoicesController;->throwUnknownActionException(Ljava/lang/String;)V

    goto :goto_0
.end method
