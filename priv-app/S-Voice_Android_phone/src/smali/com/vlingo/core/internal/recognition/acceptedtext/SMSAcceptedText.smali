.class public Lcom/vlingo/core/internal/recognition/acceptedtext/SMSAcceptedText;
.super Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;
.source "SMSAcceptedText.java"


# instance fields
.field private contact:Ljava/lang/String;

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "dialogGuid"    # Ljava/lang/String;
    .param p2, "dialogTurn"    # I
    .param p3, "gUttId"    # Ljava/lang/String;
    .param p4, "text"    # Ljava/lang/String;
    .param p5, "contact"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 47
    iput-object p4, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SMSAcceptedText;->text:Ljava/lang/String;

    .line 48
    iput-object p5, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SMSAcceptedText;->contact:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "contact"    # Ljava/lang/String;

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lcom/vlingo/core/internal/recognition/acceptedtext/SMSAcceptedText;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "gUttId"    # Ljava/lang/String;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "contact"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;-><init>(Ljava/lang/String;)V

    .line 33
    iput-object p2, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SMSAcceptedText;->text:Ljava/lang/String;

    .line 34
    iput-object p3, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SMSAcceptedText;->contact:Ljava/lang/String;

    .line 35
    return-void
.end method


# virtual methods
.method protected getAcceptedTextXML()Ljava/lang/String;
    .locals 2

    .prologue
    .line 53
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 54
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "<AcceptedText pt=\"sms:def\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 55
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SMSAcceptedText;->contact:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 56
    const-string/jumbo v1, "<Tag u=\"contact\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 57
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SMSAcceptedText;->contact:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 58
    const-string/jumbo v1, "</Tag>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 60
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SMSAcceptedText;->text:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 61
    const-string/jumbo v1, "<Tag u=\"text\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 62
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SMSAcceptedText;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 63
    const-string/jumbo v1, "</Tag>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 65
    :cond_1
    const-string/jumbo v1, "</AcceptedText>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 66
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "SMSAcceptedText ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SMSAcceptedText;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", contact="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/SMSAcceptedText;->contact:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
