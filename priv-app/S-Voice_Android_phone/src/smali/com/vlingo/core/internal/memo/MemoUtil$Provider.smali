.class final Lcom/vlingo/core/internal/memo/MemoUtil$Provider;
.super Ljava/lang/Object;
.source "MemoUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/memo/MemoUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Provider"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/memo/MemoUtil$Provider$Query;
    }
.end annotation


# instance fields
.field public query:Lcom/vlingo/core/internal/memo/MemoUtil$Provider$Query;

.field public uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/vlingo/core/internal/memo/MemoUtil$Provider$Query;)V
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "query"    # Lcom/vlingo/core/internal/memo/MemoUtil$Provider$Query;

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/memo/MemoUtil$Provider;->uri:Landroid/net/Uri;

    .line 167
    iput-object p2, p0, Lcom/vlingo/core/internal/memo/MemoUtil$Provider;->query:Lcom/vlingo/core/internal/memo/MemoUtil$Provider$Query;

    .line 168
    return-void
.end method


# virtual methods
.method public values()Landroid/content/ContentValues;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/vlingo/core/internal/memo/MemoUtil$Provider;->query:Lcom/vlingo/core/internal/memo/MemoUtil$Provider$Query;

    invoke-interface {v0}, Lcom/vlingo/core/internal/memo/MemoUtil$Provider$Query;->build()Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method
