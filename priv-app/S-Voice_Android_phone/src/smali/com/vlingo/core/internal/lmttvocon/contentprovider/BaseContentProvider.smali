.class abstract Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseContentProvider;
.super Ljava/lang/Object;
.source "BaseContentProvider.java"

# interfaces
.implements Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentProvider;


# instance fields
.field private final uploadContext:Lcom/vlingo/core/internal/lmttvocon/UploadContext;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/lmttvocon/UploadContext;)V
    .locals 0
    .param p1, "uploadContext"    # Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseContentProvider;->uploadContext:Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    .line 10
    return-void
.end method


# virtual methods
.method protected final getUploadContext()Lcom/vlingo/core/internal/lmttvocon/UploadContext;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseContentProvider;->uploadContext:Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    return-object v0
.end method
