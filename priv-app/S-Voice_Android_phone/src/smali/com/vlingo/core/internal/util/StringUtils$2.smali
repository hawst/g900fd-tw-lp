.class final Lcom/vlingo/core/internal/util/StringUtils$2;
.super Ljava/util/HashSet;
.source "StringUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/StringUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashSet",
        "<",
        "Ljava/lang/Character$UnicodeBlock;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    .line 73
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_COMPATIBILITY:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$2;->add(Ljava/lang/Object;)Z

    .line 74
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_COMPATIBILITY_FORMS:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$2;->add(Ljava/lang/Object;)Z

    .line 75
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_COMPATIBILITY_IDEOGRAPHS:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$2;->add(Ljava/lang/Object;)Z

    .line 76
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$2;->add(Ljava/lang/Object;)Z

    .line 77
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_RADICALS_SUPPLEMENT:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$2;->add(Ljava/lang/Object;)Z

    .line 78
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_SYMBOLS_AND_PUNCTUATION:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$2;->add(Ljava/lang/Object;)Z

    .line 79
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$2;->add(Ljava/lang/Object;)Z

    .line 80
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$2;->add(Ljava/lang/Object;)Z

    .line 81
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$2;->add(Ljava/lang/Object;)Z

    .line 82
    return-void
.end method
