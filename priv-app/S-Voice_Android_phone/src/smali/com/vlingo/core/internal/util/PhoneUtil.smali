.class public Lcom/vlingo/core/internal/util/PhoneUtil;
.super Ljava/lang/Object;
.source "PhoneUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/util/PhoneUtil$Status;
    }
.end annotation


# static fields
.field public static final DEFAULT_AUDIO_STREAM:I = 0x3

.field public static final KEY_STREAM_VOLUME:Ljava/lang/String; = "com.nuance.androidcore.internal.streamvolume."

.field private static final MUTED_STREAMS:Landroid/util/SparseBooleanArray;

.field private static audioManager:Landroid/media/AudioManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/util/PhoneUtil;->MUTED_STREAMS:Landroid/util/SparseBooleanArray;

    .line 39
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/util/PhoneUtil;->audioManager:Landroid/media/AudioManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static decrementCurrentStreamVolume()Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, -0x1

    invoke-static {v0}, Lcom/vlingo/core/internal/util/PhoneUtil;->incrementCurrentStreamVolume(I)Z

    move-result v0

    return v0
.end method

.method private static declared-synchronized getAudioManager()Landroid/media/AudioManager;
    .locals 3

    .prologue
    .line 55
    const-class v1, Lcom/vlingo/core/internal/util/PhoneUtil;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/util/PhoneUtil;->audioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    .line 56
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v2, "audio"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    sput-object v0, Lcom/vlingo/core/internal/util/PhoneUtil;->audioManager:Landroid/media/AudioManager;

    .line 58
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/util/PhoneUtil;->audioManager:Landroid/media/AudioManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getCurrentStream()I
    .locals 3

    .prologue
    .line 140
    const-class v2, Lcom/vlingo/core/internal/util/PhoneUtil;

    monitor-enter v2

    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v0

    .line 141
    .local v0, "bt":Z
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    .line 142
    :cond_0
    const/4 v1, 0x6

    .line 145
    :goto_0
    monitor-exit v2

    return v1

    :cond_1
    const/4 v1, 0x3

    goto :goto_0

    .line 140
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static getCurrentStreamMaxVolume()I
    .locals 2

    .prologue
    .line 66
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getCurrentStream()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    return v0
.end method

.method public static getCurrentStreamVolume()I
    .locals 2

    .prologue
    .line 62
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getCurrentStream()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    return v0
.end method

.method public static incrementCurrentStreamVolume()Z
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/core/internal/util/PhoneUtil;->incrementCurrentStreamVolume(I)Z

    move-result v0

    return v0
.end method

.method private static incrementCurrentStreamVolume(I)Z
    .locals 2
    .param p0, "sign"    # I

    .prologue
    .line 70
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getCurrentStream()I

    move-result v0

    .line 71
    .local v0, "stream":I
    invoke-static {v0, p0}, Lcom/vlingo/core/internal/util/PhoneUtil;->incrementStreamVolume(II)Z

    move-result v1

    return v1
.end method

.method public static incrementStreamVolume(II)Z
    .locals 8
    .param p0, "stream"    # I
    .param p1, "sign"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 91
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v0

    .line 92
    .local v0, "am":Landroid/media/AudioManager;
    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    .line 93
    .local v1, "currentVolume":I
    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v3

    .line 98
    .local v3, "maxVolume":I
    if-ne p1, v6, :cond_1

    if-lt v1, v3, :cond_1

    .line 115
    :cond_0
    :goto_0
    return v5

    .line 101
    :cond_1
    const/4 v7, -0x1

    if-ne p1, v7, :cond_2

    if-lez v1, :cond_0

    .line 106
    :cond_2
    const/16 v5, 0xb

    if-lt v3, v5, :cond_4

    div-int/lit8 v5, v3, 0xb

    :goto_1
    mul-int v2, v5, p1

    .line 107
    .local v2, "delta":I
    add-int v5, v1, v2

    if-le v5, v3, :cond_5

    move v4, v3

    .line 108
    .local v4, "newVolume":I
    :goto_2
    if-gez v4, :cond_3

    .line 109
    const/4 v4, 0x0

    .line 111
    :cond_3
    invoke-virtual {v0, p0, v4, v6}, Landroid/media/AudioManager;->setStreamVolume(III)V

    move v5, v6

    .line 115
    goto :goto_0

    .end local v2    # "delta":I
    .end local v4    # "newVolume":I
    :cond_4
    move v5, v6

    .line 106
    goto :goto_1

    .line 107
    .restart local v2    # "delta":I
    :cond_5
    add-int v4, v1, v2

    goto :goto_2
.end method

.method public static declared-synchronized isCurrentStreamMuted()Z
    .locals 3

    .prologue
    .line 179
    const-class v2, Lcom/vlingo/core/internal/util/PhoneUtil;

    monitor-enter v2

    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getCurrentStream()I

    move-result v0

    .line 180
    .local v0, "stream":I
    sget-object v1, Lcom/vlingo/core/internal/util/PhoneUtil;->MUTED_STREAMS:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseBooleanArray;->get(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    monitor-exit v2

    return v1

    .line 179
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static declared-synchronized muteCurrentStream()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 163
    const-class v2, Lcom/vlingo/core/internal/util/PhoneUtil;

    monitor-enter v2

    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getCurrentStream()I

    move-result v0

    .line 164
    .local v0, "stream":I
    const/4 v3, 0x1

    invoke-static {v0, v3}, Lcom/vlingo/core/internal/util/PhoneUtil;->muteStream(IZ)Lcom/vlingo/core/internal/util/PhoneUtil$Status;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/util/PhoneUtil$Status;->SUCCESS:Lcom/vlingo/core/internal/util/PhoneUtil$Status;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v3, v4, :cond_0

    :goto_0
    monitor-exit v2

    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 163
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private static declared-synchronized muteStream(IZ)Lcom/vlingo/core/internal/util/PhoneUtil$Status;
    .locals 6
    .param p0, "stream"    # I
    .param p1, "checkCurrentState"    # Z

    .prologue
    .line 184
    const-class v4, Lcom/vlingo/core/internal/util/PhoneUtil;

    monitor-enter v4

    const/4 v1, 0x1

    .line 185
    .local v1, "muteState":Z
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v2

    .line 186
    .local v2, "volume":I
    if-eqz p1, :cond_0

    if-nez v2, :cond_0

    .line 189
    sget-object v3, Lcom/vlingo/core/internal/util/PhoneUtil$Status;->ALREADY_IN_STATE:Lcom/vlingo/core/internal/util/PhoneUtil$Status;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    :goto_0
    monitor-exit v4

    return-object v3

    .line 191
    :cond_0
    if-eqz v2, :cond_1

    .line 192
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "com.nuance.androidcore.internal.streamvolume."

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 194
    :cond_1
    sget-object v3, Lcom/vlingo/core/internal/util/PhoneUtil;->MUTED_STREAMS:Landroid/util/SparseBooleanArray;

    const/4 v5, 0x1

    invoke-virtual {v3, p0, v5}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 195
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v0

    .line 196
    .local v0, "am":Landroid/media/AudioManager;
    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, p0, v3, v5}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 200
    sget-object v3, Lcom/vlingo/core/internal/util/PhoneUtil$Status;->SUCCESS:Lcom/vlingo/core/internal/util/PhoneUtil$Status;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 184
    .end local v0    # "am":Landroid/media/AudioManager;
    .end local v2    # "volume":I
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method

.method public static declared-synchronized onPause()V
    .locals 4

    .prologue
    .line 267
    const-class v2, Lcom/vlingo/core/internal/util/PhoneUtil;

    monitor-enter v2

    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    sget-object v1, Lcom/vlingo/core/internal/util/PhoneUtil;->MUTED_STREAMS:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 268
    sget-object v1, Lcom/vlingo/core/internal/util/PhoneUtil;->MUTED_STREAMS:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 269
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/PhoneUtil;->unmuteStream(IZ)Lcom/vlingo/core/internal/util/PhoneUtil$Status;

    .line 267
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 274
    :cond_1
    const-class v3, Lcom/vlingo/core/internal/util/PhoneUtil;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 275
    const/4 v1, 0x0

    :try_start_1
    sput-object v1, Lcom/vlingo/core/internal/util/PhoneUtil;->audioManager:Landroid/media/AudioManager;

    .line 276
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 277
    monitor-exit v2

    return-void

    .line 276
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 267
    :catchall_1
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static declared-synchronized onResume()V
    .locals 3

    .prologue
    .line 283
    const-class v2, Lcom/vlingo/core/internal/util/PhoneUtil;

    monitor-enter v2

    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    sget-object v1, Lcom/vlingo/core/internal/util/PhoneUtil;->MUTED_STREAMS:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 284
    sget-object v1, Lcom/vlingo/core/internal/util/PhoneUtil;->MUTED_STREAMS:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 285
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/PhoneUtil;->muteStream(IZ)Lcom/vlingo/core/internal/util/PhoneUtil$Status;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 288
    :cond_1
    monitor-exit v2

    return-void

    .line 283
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static phoneInUse(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 150
    if-nez p0, :cond_1

    .line 155
    :cond_0
    :goto_0
    return v1

    .line 154
    :cond_1
    const-string/jumbo v2, "phone"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 155
    .local v0, "mgr":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static setCurrentStreamVolume(I)Z
    .locals 2
    .param p0, "vol"    # I

    .prologue
    .line 75
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getCurrentStream()I

    move-result v0

    .line 76
    .local v0, "stream":I
    invoke-static {p0, v0}, Lcom/vlingo/core/internal/util/PhoneUtil;->setStreamVolume(II)Z

    move-result v1

    return v1
.end method

.method private static setStreamVolume(II)Z
    .locals 3
    .param p0, "stream"    # I
    .param p1, "volume"    # I

    .prologue
    const/4 v2, 0x1

    .line 80
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getCurrentStreamMaxVolume()I

    move-result v1

    .line 81
    .local v1, "max":I
    if-le p1, v1, :cond_0

    .line 82
    const/4 v2, 0x0

    .line 87
    :goto_0
    return v2

    .line 85
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v0

    .line 86
    .local v0, "am":Landroid/media/AudioManager;
    invoke-virtual {v0, p0, p1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_0
.end method

.method public static turnOnSpeakerphoneIfRequired(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    const-string/jumbo v1, "car_auto_start_speakerphone"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 47
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v0

    .line 48
    .local v0, "am":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v1

    if-nez v1, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xf

    if-gt v1, v2, :cond_0

    .line 49
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 52
    .end local v0    # "am":Landroid/media/AudioManager;
    :cond_0
    return-void
.end method

.method public static declared-synchronized unmuteCurrentStream()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 172
    const-class v3, Lcom/vlingo/core/internal/util/PhoneUtil;

    monitor-enter v3

    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getCurrentStream()I

    move-result v1

    .line 173
    .local v1, "stream":I
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/util/PhoneUtil;->unmuteStream(IZ)Lcom/vlingo/core/internal/util/PhoneUtil$Status;

    move-result-object v2

    sget-object v4, Lcom/vlingo/core/internal/util/PhoneUtil$Status;->SUCCESS:Lcom/vlingo/core/internal/util/PhoneUtil$Status;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v2, v4, :cond_0

    .line 174
    .local v0, "ret":Z
    :goto_0
    monitor-exit v3

    return v0

    .line 173
    .end local v0    # "ret":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 172
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method private static declared-synchronized unmuteStream(IZ)Lcom/vlingo/core/internal/util/PhoneUtil$Status;
    .locals 8
    .param p0, "stream"    # I
    .param p1, "checkCurrentState"    # Z

    .prologue
    .line 204
    const-class v6, Lcom/vlingo/core/internal/util/PhoneUtil;

    monitor-enter v6

    const/4 v4, 0x0

    .line 205
    .local v4, "unmuteState":Z
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v0

    .line 206
    .local v0, "am":Landroid/media/AudioManager;
    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v2

    .line 207
    .local v2, "currentVolume":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "com.nuance.androidcore.internal.streamvolume."

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x5

    invoke-static {v5, v7}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 209
    .local v3, "targetVolume":I
    if-eqz p1, :cond_0

    if-eqz v2, :cond_0

    .line 212
    sget-object v5, Lcom/vlingo/core/internal/util/PhoneUtil$Status;->ALREADY_IN_STATE:Lcom/vlingo/core/internal/util/PhoneUtil$Status;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 259
    :goto_0
    monitor-exit v6

    return-object v5

    .line 218
    :cond_0
    const/4 v1, 0x0

    .line 219
    .local v1, "count":I
    :goto_1
    if-nez v2, :cond_1

    const/16 v5, 0x64

    if-ge v1, v5, :cond_1

    .line 220
    const/4 v5, 0x0

    :try_start_1
    invoke-virtual {v0, p0, v5}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    .line 222
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 224
    :cond_1
    if-lez v2, :cond_2

    .line 225
    sget-object v5, Lcom/vlingo/core/internal/util/PhoneUtil;->MUTED_STREAMS:Landroid/util/SparseBooleanArray;

    const/4 v7, 0x0

    invoke-virtual {v5, p0, v7}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 226
    sget-object v5, Lcom/vlingo/core/internal/util/PhoneUtil$Status;->SUCCESS:Lcom/vlingo/core/internal/util/PhoneUtil$Status;

    goto :goto_0

    .line 232
    :cond_2
    if-nez v2, :cond_4

    .line 235
    invoke-static {p0, v3}, Lcom/vlingo/core/internal/util/PhoneUtil;->setStreamVolume(II)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 243
    :cond_3
    const/4 v5, 0x0

    invoke-virtual {v0, p0, v5}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    .line 249
    :cond_4
    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v2

    .line 250
    if-lez v2, :cond_5

    .line 251
    const/4 v5, 0x0

    invoke-virtual {v0, p0, v5}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    .line 254
    sget-object v5, Lcom/vlingo/core/internal/util/PhoneUtil;->MUTED_STREAMS:Landroid/util/SparseBooleanArray;

    const/4 v7, 0x0

    invoke-virtual {v5, p0, v7}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 257
    :cond_5
    invoke-static {p0, v3}, Lcom/vlingo/core/internal/util/PhoneUtil;->setStreamVolume(II)Z

    .line 258
    sget-object v5, Lcom/vlingo/core/internal/util/PhoneUtil;->MUTED_STREAMS:Landroid/util/SparseBooleanArray;

    const/4 v7, 0x0

    invoke-virtual {v5, p0, v7}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 259
    sget-object v5, Lcom/vlingo/core/internal/util/PhoneUtil$Status;->SUCCESS:Lcom/vlingo/core/internal/util/PhoneUtil$Status;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 204
    .end local v0    # "am":Landroid/media/AudioManager;
    .end local v1    # "count":I
    .end local v2    # "currentVolume":I
    .end local v3    # "targetVolume":I
    :catchall_0
    move-exception v5

    monitor-exit v6

    throw v5
.end method
