.class Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$MyResultDispatcher;
.super Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;
.source "VlingoVoiceRecognitionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyResultDispatcher"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$MyResultDispatcher;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    invoke-direct {p0}, Lcom/vlingo/core/internal/recognition/AndroidResultDispatcher;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;
    .param p2, "x1"    # Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$1;

    .prologue
    .line 206
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$MyResultDispatcher;-><init>(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)V

    return-void
.end method


# virtual methods
.method public handleResults(Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;)Z
    .locals 9
    .param p1, "results"    # Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;

    .prologue
    const/4 v8, 0x1

    .line 209
    iget-object v5, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$MyResultDispatcher;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    monitor-enter v5

    .line 210
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 211
    .local v2, "finalResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$MyResultDispatcher;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_state:I
    invoke-static {v4}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$200(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)I

    move-result v4

    const/4 v6, 0x2

    if-eq v4, v6, :cond_0

    .line 214
    iget-object v4, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$MyResultDispatcher;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # invokes: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->endRecognition()V
    invoke-static {v4}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$300(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)V

    .line 215
    monitor-exit v5

    .line 257
    :goto_0
    return v8

    .line 218
    :cond_0
    iget-object v4, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$MyResultDispatcher;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->serviceCallbackListener:Landroid/speech/RecognitionService$Callback;
    invoke-static {v4}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$400(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)Landroid/speech/RecognitionService$Callback;

    move-result-object v4

    if-nez v4, :cond_1

    .line 221
    iget-object v4, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$MyResultDispatcher;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # invokes: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->endRecognition()V
    invoke-static {v4}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$300(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)V

    .line 222
    monitor-exit v5

    goto :goto_0

    .line 258
    .end local v2    # "finalResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 226
    .restart local v2    # "finalResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    if-nez p1, :cond_2

    .line 229
    :try_start_1
    iget-object v4, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$MyResultDispatcher;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->serviceCallbackListener:Landroid/speech/RecognitionService$Callback;
    invoke-static {v4}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$400(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)Landroid/speech/RecognitionService$Callback;

    move-result-object v4

    const/4 v6, 0x7

    invoke-virtual {v4, v6}, Landroid/speech/RecognitionService$Callback;->error(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 256
    :goto_1
    :try_start_2
    iget-object v4, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$MyResultDispatcher;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # invokes: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->endRecognition()V
    invoke-static {v4}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$300(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)V

    .line 257
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 230
    :cond_2
    :try_start_3
    invoke-virtual {p1}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;->hasError()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 234
    iget-object v4, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$MyResultDispatcher;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->serviceCallbackListener:Landroid/speech/RecognitionService$Callback;
    invoke-static {v4}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$400(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)Landroid/speech/RecognitionService$Callback;

    move-result-object v4

    const/4 v6, 0x7

    invoke-virtual {v4, v6}, Landroid/speech/RecognitionService$Callback;->error(I)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 252
    :catch_0
    move-exception v1

    .line 253
    .local v1, "ex":Landroid/os/RemoteException;
    :try_start_4
    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$500()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "VLG_EXCEPTION"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 236
    .end local v1    # "ex":Landroid/os/RemoteException;
    :cond_3
    :try_start_5
    invoke-virtual {p1}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionResults;->getRecognitionResultPhrase()Ljava/lang/String;

    move-result-object v3

    .line 237
    .local v3, "phrase":Ljava/lang/String;
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_4

    .line 238
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 240
    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_5

    .line 243
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 244
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v4, "results_recognition"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 245
    iget-object v4, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$MyResultDispatcher;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->serviceCallbackListener:Landroid/speech/RecognitionService$Callback;
    invoke-static {v4}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$400(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)Landroid/speech/RecognitionService$Callback;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/speech/RecognitionService$Callback;->results(Landroid/os/Bundle;)V

    goto :goto_1

    .line 249
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_5
    iget-object v4, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$MyResultDispatcher;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->serviceCallbackListener:Landroid/speech/RecognitionService$Callback;
    invoke-static {v4}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$400(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)Landroid/speech/RecognitionService$Callback;

    move-result-object v4

    const/4 v6, 0x7

    invoke-virtual {v4, v6}, Landroid/speech/RecognitionService$Callback;->error(I)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method

.method public notifyWorking()V
    .locals 0

    .prologue
    .line 265
    return-void
.end method

.method public onRmsChanged(I)V
    .locals 4
    .param p1, "rmsValue"    # I

    .prologue
    .line 273
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$MyResultDispatcher;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->serviceCallbackListener:Landroid/speech/RecognitionService$Callback;
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$400(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)Landroid/speech/RecognitionService$Callback;

    move-result-object v1

    add-int/lit8 v2, p1, -0x1e

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/speech/RecognitionService$Callback;->rmsChanged(F)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    :goto_0
    return-void

    .line 274
    :catch_0
    move-exception v0

    .line 275
    .local v0, "ex":Landroid/os/RemoteException;
    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$500()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "VLG_EXCEPTION"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
