.class public interface abstract Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;
.super Ljava/lang/Object;
.source "VLPhraseSpotter.java"

# interfaces
.implements Lcom/vlingo/core/internal/CoreAdapter;


# virtual methods
.method public abstract destroy()V
.end method

.method public abstract getDeltaD()I
.end method

.method public abstract getSpottedPhraseScore()F
.end method

.method public abstract init(I)V
.end method

.method public abstract processShortArray([SII)Ljava/lang/String;
.end method

.method public abstract useSeamlessFeature(Ljava/lang/String;)Z
.end method
