.class public Lcom/vlingo/core/internal/contacts/ContactData;
.super Ljava/lang/Object;
.source "ContactData.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/contacts/ContactData$1;,
        Lcom/vlingo/core/internal/contacts/ContactData$ReverseComparator;,
        Lcom/vlingo/core/internal/contacts/ContactData$Kind;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/vlingo/core/internal/contacts/ContactData;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x36afaeb45a470828L


# instance fields
.field public final address:Ljava/lang/String;

.field public final contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

.field public final dataId:I

.field public final isDefault:I

.field public final kind:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

.field public final label:Ljava/lang/String;

.field private scoreBoost:F

.field private scoreMRU:F

.field public final type:I


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V
    .locals 1
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;
    .param p2, "kind"    # Lcom/vlingo/core/internal/contacts/ContactData$Kind;
    .param p3, "address"    # Ljava/lang/String;
    .param p4, "type"    # I
    .param p5, "isDefault"    # I

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactData;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 58
    iput-object p2, p0, Lcom/vlingo/core/internal/contacts/ContactData;->kind:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    .line 59
    if-nez p3, :cond_0

    const-string/jumbo p3, ""

    .end local p3    # "address":Ljava/lang/String;
    :cond_0
    iput-object p3, p0, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    .line 60
    iput p4, p0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    .line 61
    iput p5, p0, Lcom/vlingo/core/internal/contacts/ContactData;->isDefault:I

    .line 62
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->label:Ljava/lang/String;

    .line 63
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->dataId:I

    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->scoreMRU:F

    .line 65
    iget v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->scoreMRU:F

    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->scoreBoost:F

    .line 66
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;III)V
    .locals 1
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;
    .param p2, "kind"    # Lcom/vlingo/core/internal/contacts/ContactData$Kind;
    .param p3, "address"    # Ljava/lang/String;
    .param p4, "type"    # I
    .param p5, "isDefault"    # I
    .param p6, "dataId"    # I

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactData;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 70
    iput-object p2, p0, Lcom/vlingo/core/internal/contacts/ContactData;->kind:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    .line 71
    if-nez p3, :cond_0

    const-string/jumbo p3, ""

    .end local p3    # "address":Ljava/lang/String;
    :cond_0
    iput-object p3, p0, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    .line 72
    iput p4, p0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    .line 73
    iput p5, p0, Lcom/vlingo/core/internal/contacts/ContactData;->isDefault:I

    .line 74
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->label:Ljava/lang/String;

    .line 75
    iput p6, p0, Lcom/vlingo/core/internal/contacts/ContactData;->dataId:I

    .line 76
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->scoreMRU:F

    .line 77
    iget v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->scoreMRU:F

    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->scoreBoost:F

    .line 78
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;
    .param p2, "kind"    # Lcom/vlingo/core/internal/contacts/ContactData$Kind;
    .param p3, "address"    # Ljava/lang/String;
    .param p4, "label"    # Ljava/lang/String;
    .param p5, "isDefault"    # I

    .prologue
    const/4 v0, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactData;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 82
    iput-object p2, p0, Lcom/vlingo/core/internal/contacts/ContactData;->kind:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    .line 83
    if-nez p3, :cond_0

    const-string/jumbo p3, ""

    .end local p3    # "address":Ljava/lang/String;
    :cond_0
    iput-object p3, p0, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    .line 84
    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    .line 85
    iput p5, p0, Lcom/vlingo/core/internal/contacts/ContactData;->isDefault:I

    .line 86
    if-nez p4, :cond_1

    const-string/jumbo p4, ""

    .end local p4    # "label":Ljava/lang/String;
    :cond_1
    iput-object p4, p0, Lcom/vlingo/core/internal/contacts/ContactData;->label:Ljava/lang/String;

    .line 87
    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->dataId:I

    .line 88
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->scoreMRU:F

    .line 89
    iget v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->scoreMRU:F

    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->scoreBoost:F

    .line 90
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 1
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;
    .param p2, "kind"    # Lcom/vlingo/core/internal/contacts/ContactData$Kind;
    .param p3, "address"    # Ljava/lang/String;
    .param p4, "label"    # Ljava/lang/String;
    .param p5, "isDefault"    # I
    .param p6, "dataId"    # I

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactData;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 94
    iput-object p2, p0, Lcom/vlingo/core/internal/contacts/ContactData;->kind:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    .line 95
    if-nez p3, :cond_0

    const-string/jumbo p3, ""

    .end local p3    # "address":Ljava/lang/String;
    :cond_0
    iput-object p3, p0, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    .line 96
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    .line 97
    iput p5, p0, Lcom/vlingo/core/internal/contacts/ContactData;->isDefault:I

    .line 98
    if-nez p4, :cond_1

    const-string/jumbo p4, ""

    .end local p4    # "label":Ljava/lang/String;
    :cond_1
    iput-object p4, p0, Lcom/vlingo/core/internal/contacts/ContactData;->label:Ljava/lang/String;

    .line 99
    iput p6, p0, Lcom/vlingo/core/internal/contacts/ContactData;->dataId:I

    .line 100
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->scoreMRU:F

    .line 101
    iget v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->scoreMRU:F

    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->scoreBoost:F

    .line 102
    return-void
.end method

.method public static clone(Ljava/util/HashSet;)Ljava/util/HashSet;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)",
            "Ljava/util/HashSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 334
    .local p0, "dataSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 335
    .local v0, "c":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-virtual {p0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 336
    .local v4, "t":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v4}, Lcom/vlingo/core/internal/contacts/ContactData;->clone()Lcom/vlingo/core/internal/contacts/ContactData;

    move-result-object v1

    .line 337
    .local v1, "copy":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 340
    .end local v0    # "c":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .end local v1    # "copy":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "t":Lcom/vlingo/core/internal/contacts/ContactData;
    :catch_0
    move-exception v2

    .line 341
    .local v2, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/RuntimeException;

    const-string/jumbo v6, "List cloning unsupported"

    invoke-direct {v5, v6, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 339
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "c":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_0
    return-object v0
.end method

.method public static clone(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 321
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 322
    .local v0, "c":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 323
    .local v4, "t":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v4}, Lcom/vlingo/core/internal/contacts/ContactData;->clone()Lcom/vlingo/core/internal/contacts/ContactData;

    move-result-object v1

    .line 324
    .local v1, "copy":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 327
    .end local v0    # "c":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .end local v1    # "copy":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "t":Lcom/vlingo/core/internal/contacts/ContactData;
    :catch_0
    move-exception v2

    .line 328
    .local v2, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/RuntimeException;

    const-string/jumbo v6, "List cloning unsupported"

    invoke-direct {v5, v6, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 326
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "c":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_0
    return-object v0
.end method

.method public static clone(Ljava/util/Map;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 348
    .local p0, "dataSet":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 349
    .local v0, "c":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 350
    .local v3, "t":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 353
    .end local v0    # "c":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "t":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :catch_0
    move-exception v1

    .line 354
    .local v1, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/RuntimeException;

    const-string/jumbo v5, "List cloning unsupported"

    invoke-direct {v4, v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 352
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "c":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    return-object v0
.end method


# virtual methods
.method public clone()Lcom/vlingo/core/internal/contacts/ContactData;
    .locals 2

    .prologue
    .line 107
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    :goto_0
    return-object v0

    .line 109
    :catch_0
    move-exception v1

    .line 110
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v1}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    .line 112
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactData;->clone()Lcom/vlingo/core/internal/contacts/ContactData;

    move-result-object v0

    return-object v0
.end method

.method public compareTo(Lcom/vlingo/core/internal/contacts/ContactData;)I
    .locals 2
    .param p1, "d2"    # Lcom/vlingo/core/internal/contacts/ContactData;

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactData;->getScore()F

    move-result v0

    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactData;->getScore()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 163
    const/4 v0, 0x1

    .line 166
    :goto_0
    return v0

    .line 164
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactData;->getScore()F

    move-result v0

    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactData;->getScore()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 165
    const/4 v0, -0x1

    goto :goto_0

    .line 166
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 19
    check-cast p1, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/contacts/ContactData;->compareTo(Lcom/vlingo/core/internal/contacts/ContactData;)I

    move-result v0

    return v0
.end method

.method computeMRUScore(Lcom/vlingo/core/internal/contacts/mru/MRU;Lcom/vlingo/core/internal/contacts/ContactType;I)V
    .locals 1
    .param p1, "mru"    # Lcom/vlingo/core/internal/contacts/mru/MRU;
    .param p2, "contactType"    # Lcom/vlingo/core/internal/contacts/ContactType;
    .param p3, "contactID"    # I

    .prologue
    .line 129
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-virtual {p1, p2, p3, v0}, Lcom/vlingo/core/internal/contacts/mru/MRU;->getNormalizedCount(Lcom/vlingo/core/internal/contacts/ContactType;ILjava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->scoreMRU:F

    .line 130
    return-void
.end method

.method public computeScore(I[I)Z
    .locals 9
    .param p1, "preferredType"    # I
    .param p2, "requestedTypes"    # [I

    .prologue
    const/4 v5, 0x1

    const v8, 0x3f7d70a4    # 0.99f

    .line 244
    const/4 v1, 0x0

    .line 245
    .local v1, "boostAmount":F
    iget v6, p0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    if-ne v6, p1, :cond_0

    .line 246
    const v6, 0x3f666666    # 0.9f

    add-float/2addr v1, v6

    .line 248
    :cond_0
    iget-object v6, p0, Lcom/vlingo/core/internal/contacts/ContactData;->kind:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    sget-object v7, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Email:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    if-ne v6, v7, :cond_1

    .line 249
    if-eqz p2, :cond_1

    .line 250
    move-object v0, p2

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget v4, v0, v2

    .line 251
    .local v4, "requestedType":I
    iget v6, p0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    if-ne v4, v6, :cond_5

    .line 252
    add-float/2addr v1, v8

    .line 258
    .end local v0    # "arr$":[I
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "requestedType":I
    :cond_1
    iget-object v6, p0, Lcom/vlingo/core/internal/contacts/ContactData;->kind:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    sget-object v7, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Address:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    if-ne v6, v7, :cond_2

    .line 259
    if-eqz p2, :cond_2

    .line 260
    move-object v0, p2

    .restart local v0    # "arr$":[I
    array-length v3, v0

    .restart local v3    # "len$":I
    const/4 v2, 0x0

    .restart local v2    # "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget v4, v0, v2

    .line 261
    .restart local v4    # "requestedType":I
    iget v6, p0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    if-ne v4, v6, :cond_6

    .line 262
    add-float/2addr v1, v8

    .line 268
    .end local v0    # "arr$":[I
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "requestedType":I
    :cond_2
    iget-object v6, p0, Lcom/vlingo/core/internal/contacts/ContactData;->kind:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    sget-object v7, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Phone:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    if-ne v6, v7, :cond_4

    .line 269
    if-eqz p2, :cond_3

    .line 270
    move-object v0, p2

    .restart local v0    # "arr$":[I
    array-length v3, v0

    .restart local v3    # "len$":I
    const/4 v2, 0x0

    .restart local v2    # "i$":I
    :goto_2
    if-ge v2, v3, :cond_3

    aget v4, v0, v2

    .line 271
    .restart local v4    # "requestedType":I
    iget v6, p0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    if-ne v4, v6, :cond_7

    .line 272
    add-float/2addr v1, v8

    .line 277
    .end local v0    # "arr$":[I
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "requestedType":I
    :cond_3
    iget v6, p0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_8

    .line 278
    const v6, 0x3bc49ba6    # 0.006f

    add-float/2addr v1, v6

    .line 287
    :cond_4
    :goto_3
    iput v1, p0, Lcom/vlingo/core/internal/contacts/ContactData;->scoreBoost:F

    .line 288
    cmpl-float v6, v1, v8

    if-ltz v6, :cond_b

    :goto_4
    return v5

    .line 250
    .restart local v0    # "arr$":[I
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    .restart local v4    # "requestedType":I
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 260
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 270
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 279
    .end local v0    # "arr$":[I
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "requestedType":I
    :cond_8
    iget v6, p0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    const/4 v7, 0x3

    if-ne v6, v7, :cond_9

    .line 280
    const v6, 0x3ba3d70a    # 0.005f

    add-float/2addr v1, v6

    goto :goto_3

    .line 281
    :cond_9
    iget v6, p0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    if-ne v6, v5, :cond_a

    .line 282
    const v6, 0x3b83126f    # 0.004f

    add-float/2addr v1, v6

    goto :goto_3

    .line 284
    :cond_a
    const v6, 0x3b449ba6    # 0.003f

    add-float/2addr v1, v6

    goto :goto_3

    .line 288
    :cond_b
    const/4 v5, 0x0

    goto :goto_4
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 204
    if-ne p0, p1, :cond_1

    .line 206
    const/4 v3, 0x1

    .line 229
    :cond_0
    :goto_0
    return v3

    .line 209
    :cond_1
    instance-of v4, p1, Lcom/vlingo/core/internal/contacts/ContactData;

    if-eqz v4, :cond_0

    move-object v2, p1

    .line 213
    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 215
    .local v2, "other":Lcom/vlingo/core/internal/contacts/ContactData;
    iget v4, v2, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    iget v5, p0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    if-ne v4, v5, :cond_0

    iget-object v4, v2, Lcom/vlingo/core/internal/contacts/ContactData;->kind:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    iget-object v5, p0, Lcom/vlingo/core/internal/contacts/ContactData;->kind:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    if-ne v4, v5, :cond_0

    .line 218
    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactData;->label:Ljava/lang/String;

    iget-object v5, v2, Lcom/vlingo/core/internal/contacts/ContactData;->label:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 222
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactData;->getNormalizedAddress()Ljava/lang/String;

    move-result-object v0

    .line 223
    .local v0, "a1":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/vlingo/core/internal/contacts/ContactData;->getNormalizedAddress()Ljava/lang/String;

    move-result-object v1

    .line 229
    .local v1, "a2":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_0
.end method

.method public getAcceptedTextTagStringForMessaging()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->kind:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->getAcceptedText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFormattedType(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 292
    sget-object v0, Lcom/vlingo/core/internal/contacts/ContactData$1;->$SwitchMap$com$vlingo$core$internal$contacts$ContactData$Kind:[I

    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactData;->kind:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 305
    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    .line 294
    :pswitch_0
    iget v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    const-string/jumbo v1, ""

    invoke-static {p1, v0, v1}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 297
    :pswitch_1
    iget v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    const-string/jumbo v1, ""

    invoke-static {p1, v0, v1}, Landroid/provider/ContactsContract$CommonDataKinds$StructuredPostal;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 300
    :pswitch_2
    iget v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    const-string/jumbo v1, ""

    invoke-static {p1, v0, v1}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 292
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getMRUScore()F
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->scoreMRU:F

    return v0
.end method

.method public getNormalizedAddress()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 181
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    .line 183
    .local v0, "addr":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactData;->isPhoneNumber()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 184
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->cleanPhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 186
    const-string/jumbo v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 187
    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 190
    :cond_0
    const-string/jumbo v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 191
    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 199
    :cond_1
    :goto_0
    return-object v0

    .line 195
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 196
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getScore()F
    .locals 2

    .prologue
    .line 116
    iget v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->scoreMRU:F

    iget v1, p0, Lcom/vlingo/core/internal/contacts/ContactData;->scoreBoost:F

    add-float/2addr v0, v1

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 316
    iget v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 235
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactData;->kind:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->hashCode()I

    move-result v1

    .line 236
    .local v1, "hash":I
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactData;->getNormalizedAddress()Ljava/lang/String;

    move-result-object v0

    .line 237
    .local v0, "addr":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    xor-int/2addr v1, v2

    .line 238
    iget v2, p0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    mul-int/lit16 v2, v2, 0x223

    xor-int/2addr v1, v2

    .line 239
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactData;->label:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    xor-int/2addr v1, v2

    .line 240
    return v1
.end method

.method public isAddress()Z
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->kind:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Address:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBirthday()Z
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->kind:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Birthday:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmail()Z
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->kind:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Email:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFacebook()Z
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->kind:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Facebook:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPhoneNumber()Z
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->kind:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Phone:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setScore(F)V
    .locals 1
    .param p1, "score"    # F

    .prologue
    .line 120
    iput p1, p0, Lcom/vlingo/core/internal/contacts/ContactData;->scoreBoost:F

    .line 121
    iget v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->scoreBoost:F

    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactData;->scoreMRU:F

    .line 122
    return-void
.end method

.method setScoreBoost(F)V
    .locals 0
    .param p1, "boost"    # F

    .prologue
    .line 125
    iput p1, p0, Lcom/vlingo/core/internal/contacts/ContactData;->scoreBoost:F

    .line 126
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 312
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "[ContactData] type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactData;->kind:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " address="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " score="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactData;->getScore()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
