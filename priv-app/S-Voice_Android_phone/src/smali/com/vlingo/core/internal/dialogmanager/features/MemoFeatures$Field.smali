.class public interface abstract Lcom/vlingo/core/internal/dialogmanager/features/MemoFeatures$Field;
.super Ljava/lang/Object;
.source "MemoFeatures.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/features/MemoFeatures;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Field"
.end annotation


# static fields
.field public static final BODY:Ljava/lang/String; = "memo.body"

.field public static final ID:Ljava/lang/String; = "memo.id"

.field public static final TITLE:Ljava/lang/String; = "memo.title"
