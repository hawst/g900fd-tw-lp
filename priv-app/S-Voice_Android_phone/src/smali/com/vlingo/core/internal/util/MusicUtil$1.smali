.class synthetic Lcom/vlingo/core/internal/util/MusicUtil$1;
.super Ljava/lang/Object;
.source "MusicUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/MusicUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$vlingo$core$internal$util$MusicUtil$MusicType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 51
    invoke-static {}, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->values()[Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/vlingo/core/internal/util/MusicUtil$1;->$SwitchMap$com$vlingo$core$internal$util$MusicUtil$MusicType:[I

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/util/MusicUtil$1;->$SwitchMap$com$vlingo$core$internal$util$MusicUtil$MusicType:[I

    sget-object v1, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->PLAYLIST:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_0
    :try_start_1
    sget-object v0, Lcom/vlingo/core/internal/util/MusicUtil$1;->$SwitchMap$com$vlingo$core$internal$util$MusicUtil$MusicType:[I

    sget-object v1, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->TITLE:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_1
    :try_start_2
    sget-object v0, Lcom/vlingo/core/internal/util/MusicUtil$1;->$SwitchMap$com$vlingo$core$internal$util$MusicUtil$MusicType:[I

    sget-object v1, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->ARTIST:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    :try_start_3
    sget-object v0, Lcom/vlingo/core/internal/util/MusicUtil$1;->$SwitchMap$com$vlingo$core$internal$util$MusicUtil$MusicType:[I

    sget-object v1, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->ALBUM:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_3
    return-void

    :catch_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_0
.end method
