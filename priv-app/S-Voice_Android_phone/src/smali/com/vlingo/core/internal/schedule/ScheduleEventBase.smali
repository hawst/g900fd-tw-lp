.class public abstract Lcom/vlingo/core/internal/schedule/ScheduleEventBase;
.super Lcom/vlingo/core/internal/schedule/EventBase;
.source "ScheduleEventBase.java"


# instance fields
.field protected location:Ljava/lang/String;

.field protected organizer:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/core/internal/schedule/EventBase;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Lcom/vlingo/core/internal/schedule/EventBase;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleEventBase;->clone()Lcom/vlingo/core/internal/schedule/ScheduleEventBase;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/vlingo/core/internal/schedule/ScheduleEventBase;
    .locals 1

    .prologue
    .line 23
    invoke-super {p0}, Lcom/vlingo/core/internal/schedule/EventBase;->clone()Lcom/vlingo/core/internal/schedule/EventBase;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/schedule/ScheduleEventBase;

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleEventBase;->clone()Lcom/vlingo/core/internal/schedule/ScheduleEventBase;

    move-result-object v0

    return-object v0
.end method

.method public getEnd()J
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEventBase;->end:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getEndDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEventBase;->end:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getDateStr()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEndTime()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEventBase;->end:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTimeStr(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEventBase;->location:Ljava/lang/String;

    return-object v0
.end method

.method public getOrganizer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEventBase;->organizer:Ljava/lang/String;

    return-object v0
.end method

.method protected normalizeToNow()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-super {p0}, Lcom/vlingo/core/internal/schedule/EventBase;->normalizeToNow()V

    .line 51
    new-instance v0, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleEventBase;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/schedule/DateUtil;->endOfGivenDay(J)J

    move-result-wide v1

    invoke-direct {v0, p0, v1, v2}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;-><init>(Lcom/vlingo/core/internal/schedule/EventBase;J)V

    iput-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleEventBase;->end:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    .line 52
    return-void
.end method

.method protected abstract setEndTime()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleEventBase;->location:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public setOrganizer(Ljava/lang/String;)V
    .locals 0
    .param p1, "organizer"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleEventBase;->organizer:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/vlingo/core/internal/schedule/EventBase;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " loc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleEventBase;->getLocation()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " org="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleEventBase;->getOrganizer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " end="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleEventBase;->getEnd()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
