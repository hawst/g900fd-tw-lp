.class public Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;
.super Landroid/util/Pair;
.source "GrammarAction.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/Pair",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final checksumDelta:I

.field private final contentItem:Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;

.field private final type:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionType;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;I)V
    .locals 2
    .param p1, "contentItem"    # Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
    .param p2, "oldChecksum"    # I

    .prologue
    .line 24
    invoke-virtual {p1}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getChecksum()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 25
    iput-object p1, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->contentItem:Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;

    .line 26
    sget-object v0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionType;->MODIFY:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionType;

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->type:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionType;

    .line 27
    invoke-virtual {p1}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getChecksum()I

    move-result v0

    sub-int/2addr v0, p2

    iput v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->checksumDelta:I

    .line 28
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;Z)V
    .locals 2
    .param p1, "contentItem"    # Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
    .param p2, "addTrueRemoveFalse"    # Z

    .prologue
    .line 12
    invoke-virtual {p1}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getChecksum()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 13
    iput-object p1, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->contentItem:Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;

    .line 14
    if-eqz p2, :cond_0

    .line 15
    sget-object v0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionType;->ADD:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionType;

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->type:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionType;

    .line 16
    invoke-virtual {p1}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getChecksum()I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->checksumDelta:I

    .line 21
    :goto_0
    return-void

    .line 18
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionType;->REMOVE:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionType;

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->type:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionType;

    .line 19
    invoke-virtual {p1}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getChecksum()I

    move-result v0

    neg-int v0, v0

    iput v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->checksumDelta:I

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 52
    instance-of v2, p1, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 53
    check-cast v0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;

    .line 54
    .local v0, "grammarAction":Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;
    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->contentItem:Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;

    iget-object v3, v0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->contentItem:Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->type:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionType;

    iget-object v3, v0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->type:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionType;

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->checksumDelta:I

    iget v3, v0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->checksumDelta:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 56
    .end local v0    # "grammarAction":Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;
    :cond_0
    return v1
.end method

.method public getChecksum()I
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getChecksumDelta()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->checksumDelta:I

    return v0
.end method

.method public getContentItem()Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->contentItem:Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getType()Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionType;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->type:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionType;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->contentItem:Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->type:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionType;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->checksumDelta:I

    add-int/2addr v0, v1

    return v0
.end method
