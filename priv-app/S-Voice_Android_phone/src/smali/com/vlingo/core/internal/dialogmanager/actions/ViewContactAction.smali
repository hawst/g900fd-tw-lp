.class public Lcom/vlingo/core/internal/dialogmanager/actions/ViewContactAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "ViewContactAction.java"


# instance fields
.field private contact:Lcom/vlingo/core/internal/contacts/ContactMatch;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method


# virtual methods
.method public contact(Lcom/vlingo/core/internal/contacts/ContactMatch;)Lcom/vlingo/core/internal/dialogmanager/actions/ViewContactAction;
    .locals 0
    .param p1, "contactMatch"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ViewContactAction;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 25
    return-object p0
.end method

.method protected execute()V
    .locals 5

    .prologue
    .line 30
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ViewContactAction;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    if-eqz v2, :cond_0

    .line 31
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 32
    .local v0, "intent":Landroid/content/Intent;
    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ViewContactAction;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-wide v3, v3, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 33
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 35
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ViewContactAction;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ViewContactAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 45
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "uri":Landroid/net/Uri;
    :goto_1
    return-void

    .line 43
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ViewContactAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    goto :goto_1

    .line 37
    .restart local v0    # "intent":Landroid/content/Intent;
    .restart local v1    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v2

    goto :goto_0
.end method
