.class Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
.super Ljava/lang/Object;
.source "CallLogUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/CallLogUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CallLogData"
.end annotation


# instance fields
.field private name:Ljava/lang/String;

.field private score:D


# direct methods
.method public constructor <init>(Ljava/lang/String;D)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "score"    # D

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;->name:Ljava/lang/String;

    .line 35
    iput-wide p2, p0, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;->score:D

    .line 36
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getScore()D
    .locals 2

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;->score:D

    return-wide v0
.end method
