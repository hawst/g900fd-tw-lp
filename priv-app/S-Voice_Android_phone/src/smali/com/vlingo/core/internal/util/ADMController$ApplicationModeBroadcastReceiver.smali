.class Lcom/vlingo/core/internal/util/ADMController$ApplicationModeBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ADMController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/ADMController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ApplicationModeBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/util/ADMController;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/util/ADMController;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/vlingo/core/internal/util/ADMController$ApplicationModeBroadcastReceiver;->this$0:Lcom/vlingo/core/internal/util/ADMController;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/util/ADMController;Lcom/vlingo/core/internal/util/ADMController$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/util/ADMController;
    .param p2, "x1"    # Lcom/vlingo/core/internal/util/ADMController$1;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/util/ADMController$ApplicationModeBroadcastReceiver;-><init>(Lcom/vlingo/core/internal/util/ADMController;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 40
    if-eqz p2, :cond_1

    .line 43
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 44
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v1, "com.vlingo.client.app.action.VLINGO_APP_START"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 45
    iget-object v1, p0, Lcom/vlingo/core/internal/util/ADMController$ApplicationModeBroadcastReceiver;->this$0:Lcom/vlingo/core/internal/util/ADMController;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/vlingo/core/internal/util/ADMController;->onBoot:Z

    .line 47
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/util/ADMController$ApplicationModeBroadcastReceiver;->this$0:Lcom/vlingo/core/internal/util/ADMController;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ADMController;->refreshFeatureStates()V

    .line 49
    .end local v0    # "action":Ljava/lang/String;
    :cond_1
    return-void
.end method
