.class public Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;
.super Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
.source "ContactContentItem.java"


# instance fields
.field private final firstName:Ljava/lang/String;

.field private final lastName:Ljava/lang/String;

.field private final middleName:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # J
    .param p3, "firstName"    # Ljava/lang/String;
    .param p4, "lastName"    # Ljava/lang/String;
    .param p5, "middleName"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;-><init>(J)V

    .line 15
    iput-object p3, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->firstName:Ljava/lang/String;

    .line 16
    iput-object p4, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->lastName:Ljava/lang/String;

    .line 17
    iput-object p5, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->middleName:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 54
    if-nez p1, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v1

    .line 57
    :cond_1
    instance-of v2, p1, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 58
    check-cast v0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;

    .line 59
    .local v0, "contact":Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->getId()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->getId()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->firstName:Ljava/lang/String;

    if-nez v2, :cond_2

    iget-object v2, v0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->firstName:Ljava/lang/String;

    if-nez v2, :cond_0

    :goto_1
    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->lastName:Ljava/lang/String;

    if-nez v2, :cond_3

    iget-object v2, v0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->lastName:Ljava/lang/String;

    if-nez v2, :cond_0

    :goto_2
    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->middleName:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, v0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->middleName:Ljava/lang/String;

    if-nez v2, :cond_0

    :goto_3
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->firstName:Ljava/lang/String;

    iget-object v3, v0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->firstName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->lastName:Ljava/lang/String;

    iget-object v3, v0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->lastName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->middleName:Ljava/lang/String;

    iget-object v3, v0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->middleName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_3
.end method

.method public getChecksum()I
    .locals 4

    .prologue
    .line 49
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->firstName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->lastName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->middleName:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->makeChecksum([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getFirstName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->firstName:Ljava/lang/String;

    return-object v0
.end method

.method public getLastName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->lastName:Ljava/lang/String;

    return-object v0
.end method

.method public getMiddleName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->middleName:Ljava/lang/String;

    return-object v0
.end method

.method public getOrthographies()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 35
    .local v0, "result":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->firstName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 36
    iget-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->firstName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 38
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->lastName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 39
    iget-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->lastName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 41
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->middleName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 42
    iget-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->middleName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 44
    :cond_2
    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContactContentItem;->getChecksum()I

    move-result v0

    return v0
.end method
