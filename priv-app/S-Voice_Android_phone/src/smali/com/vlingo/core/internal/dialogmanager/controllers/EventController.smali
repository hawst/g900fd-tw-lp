.class public Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;
.super Lcom/vlingo/core/internal/dialogmanager/StateController;
.source "EventController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# instance fields
.field private maps:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private rules:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;",
            ">;"
        }
    .end annotation
.end field

.field private scheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 44
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/StateController;-><init>()V

    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->rules:Ljava/util/Map;

    .line 46
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->rules:Ljava/util/Map;

    const-string/jumbo v1, "title"

    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MAIN_EVENT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v3}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->getFieldId()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_EVENT_SAY_TITLE:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->rules:Ljava/util/Map;

    const-string/jumbo v1, "time"

    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;

    const-string/jumbo v3, ""

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->rules:Ljava/util/Map;

    const-string/jumbo v1, "Confirm"

    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_EVENT_TITLE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v3}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->getFieldId()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_event_save_confirm:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->maps:Ljava/util/Map;

    .line 51
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->maps:Ljava/util/Map;

    const-string/jumbo v1, "Value"

    const-string/jumbo v2, "title"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    return-void
.end method

.method private save()V
    .locals 2

    .prologue
    .line 159
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SCHEDULE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/ScheduleAppointmentAction;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/actions/ScheduleAppointmentAction;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->scheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/ScheduleAppointmentAction;->event(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Lcom/vlingo/core/internal/dialogmanager/actions/ScheduleAppointmentAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/actions/ScheduleAppointmentAction;->queue()V

    .line 160
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->sendAcceptedText()V

    .line 161
    return-void
.end method

.method private sendAcceptedText()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 163
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->scheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    if-eqz v0, :cond_1

    .line 164
    const/4 v7, 0x0

    .line 165
    .local v7, "attendees":Ljava/lang/String;
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->scheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getAttendeeNames()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->scheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getAttendeeNames()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    .line 169
    :cond_0
    new-instance v0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->scheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->scheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBeginDate()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->scheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBeginTime()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    move-object v6, v4

    move-object v8, v4

    invoke-direct/range {v0 .. v8}, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V

    .line 172
    .end local v7    # "attendees":Ljava/lang/String;
    :cond_1
    return-void
.end method


# virtual methods
.method public actionSuccess()V
    .locals 4

    .prologue
    .line 66
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/StateController;->actionSuccess()V

    .line 67
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_event_saved:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    .line 68
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v0

    const-string/jumbo v1, "event saved"

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageAction(Ljava/lang/String;Ljava/util/List;Z)V

    .line 69
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 13
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v12, 0x0

    .line 104
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/StateController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 106
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v7

    const-string/jumbo v8, "event"

    invoke-virtual {v7, v8}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 108
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "LPAction"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 143
    :cond_0
    :goto_0
    return v12

    .line 115
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->isAllSlotsFilled()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 116
    new-instance v7, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-direct {v7}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;-><init>()V

    iput-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->scheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 117
    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->scheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->rules:Ljava/util/Map;

    const-string/jumbo v9, "title"

    invoke-interface {v7, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setTitle(Ljava/lang/String;)V

    .line 119
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 120
    .local v0, "d1":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Date;->getHours()I

    move-result v3

    .line 121
    .local v3, "hour":I
    add-int/lit8 v7, v3, 0x1

    invoke-virtual {v0, v7}, Ljava/util/Date;->setHours(I)V

    .line 122
    invoke-virtual {v0, v12}, Ljava/util/Date;->setMinutes(I)V

    .line 123
    invoke-virtual {v0, v12}, Ljava/util/Date;->setSeconds(I)V

    .line 124
    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->rules:Ljava/util/Map;

    const-string/jumbo v8, "time"

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 125
    .local v4, "st":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 126
    new-instance v2, Ljava/text/SimpleDateFormat;

    sget-object v7, Lcom/vlingo/core/internal/schedule/DateUtil;->CANONICAL_TIME_FORMAT:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 127
    .local v2, "formatter":Ljava/text/SimpleDateFormat;
    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    .line 132
    .end local v2    # "formatter":Ljava/text/SimpleDateFormat;
    :goto_1
    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->scheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setBegin(J)V

    .line 133
    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->scheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-static {v4}, Lcom/vlingo/core/internal/schedule/DateUtil;->getMillisFromTimeString(Ljava/lang/String;)J

    move-result-wide v8

    sget-wide v10, Lcom/vlingo/core/internal/schedule/DateUtil;->ONE_HOUR:J

    add-long/2addr v8, v10

    invoke-virtual {v7, v8, v9}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setEnd(J)V

    .line 134
    const/4 v1, 0x0

    .line 135
    .local v1, "decos":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    const-string/jumbo v7, "confirm"

    invoke-static {p1, v7, v12, v12}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 136
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeConfirmButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v1

    .line 138
    :cond_2
    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ScheduleEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->scheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-interface {p2, v7, v1, v8, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 139
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->hasConfirm()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 140
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->executeConfirm()V

    goto/16 :goto_0

    .line 129
    .end local v1    # "decos":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    :cond_3
    invoke-static {v4}, Lcom/vlingo/core/internal/schedule/DateUtil;->getMillisFromTimeString(Ljava/lang/String;)J

    move-result-wide v5

    .line 130
    .local v5, "startTime":J
    invoke-virtual {v0, v5, v6}, Ljava/util/Date;->setTime(J)V

    goto :goto_1
.end method

.method public getRuleMappings()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->maps:Ljava/util/Map;

    return-object v0
.end method

.method public getRules()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->rules:Ljava/util/Map;

    return-object v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "used"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 74
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.Default"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 76
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->save()V

    .line 96
    .end local p2    # "used":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-void

    .line 77
    .restart local p2    # "used":Ljava/lang/Object;
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.Cancel"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 78
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 79
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_TASK_CANCELLED:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 80
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->reset()V

    goto :goto_0

    .line 81
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.View"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 84
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 85
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SCHEDULE_VIEW:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;

    check-cast p2, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .end local p2    # "used":Ljava/lang/Object;
    invoke-virtual {v0, p2}, Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;->scheduleEvent(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;->queue()V

    goto :goto_0

    .line 88
    .restart local p2    # "used":Ljava/lang/Object;
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.Edit"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 92
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SCHEDULE_EDIT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;

    check-cast p2, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .end local p2    # "used":Ljava/lang/Object;
    invoke-virtual {v0, p2}, Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;->scheduleEvent(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;->queue()V

    goto :goto_0
.end method

.method public handleLPAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 3
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v1, 0x0

    .line 148
    const-string/jumbo v2, "Action"

    invoke-static {p1, v2, v1}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 149
    .local v0, "actionValue":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 150
    const-string/jumbo v2, "send"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "save"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 151
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/EventController;->save()V

    .line 152
    const/4 v1, 0x1

    .line 155
    :cond_1
    return v1
.end method
