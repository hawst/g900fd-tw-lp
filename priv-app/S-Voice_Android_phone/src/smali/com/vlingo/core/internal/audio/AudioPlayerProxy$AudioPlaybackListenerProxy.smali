.class final Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;
.super Ljava/lang/Object;
.source "AudioPlayerProxy.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/audio/AudioPlayerProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AudioPlaybackListenerProxy"
.end annotation


# instance fields
.field requestListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

.field final synthetic this$0:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/audio/AudioPlayerProxy;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 0
    .param p2, "requestListener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    .line 208
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 209
    iput-object p2, p0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;->requestListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .line 210
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/audio/AudioPlayerProxy;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;Lcom/vlingo/core/internal/audio/AudioPlayerProxy$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/audio/AudioPlayerProxy;
    .param p2, "x1"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;
    .param p3, "x2"    # Lcom/vlingo/core/internal/audio/AudioPlayerProxy$1;

    .prologue
    .line 204
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;-><init>(Lcom/vlingo/core/internal/audio/AudioPlayerProxy;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    return-void
.end method


# virtual methods
.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 5
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 253
    # getter for: Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onRequestCancelled "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", reason= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;->requestListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    if-eqz v2, :cond_0

    .line 255
    # getter for: Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "requestListener "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;->requestListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;->requestListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-interface {v2, p1, p2}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V

    .line 258
    :cond_0
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    # getter for: Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->externalListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->access$200(Lcom/vlingo/core/internal/audio/AudioPlayerProxy;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .line 259
    .local v0, "externalListener":Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;
    # getter for: Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "externalListener "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    invoke-interface {v0, p1, p2}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V

    goto :goto_0

    .line 262
    .end local v0    # "externalListener":Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;
    :cond_1
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 5
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 227
    # getter for: Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onRequestDidPlay "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;->requestListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    if-eqz v2, :cond_0

    .line 229
    # getter for: Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "requestListener "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;->requestListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;->requestListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-interface {v2, p1}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 232
    :cond_0
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    # getter for: Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->externalListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->access$200(Lcom/vlingo/core/internal/audio/AudioPlayerProxy;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .line 233
    .local v0, "externalListener":Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;
    # getter for: Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "externalListener "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    goto :goto_0

    .line 236
    .end local v0    # "externalListener":Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;
    :cond_1
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 5
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 240
    # getter for: Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onRequestIgnored "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", reason= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;->requestListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    if-eqz v2, :cond_0

    .line 242
    # getter for: Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "requestListener "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;->requestListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;->requestListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-interface {v2, p1, p2}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V

    .line 245
    :cond_0
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    # getter for: Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->externalListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->access$200(Lcom/vlingo/core/internal/audio/AudioPlayerProxy;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .line 246
    .local v0, "externalListener":Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;
    # getter for: Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "externalListener "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    invoke-interface {v0, p1, p2}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V

    goto :goto_0

    .line 249
    .end local v0    # "externalListener":Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;
    :cond_1
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 5
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 214
    # getter for: Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onRequestWillPlay "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;->requestListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    if-eqz v2, :cond_0

    .line 216
    # getter for: Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "requestListener "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;->requestListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;->requestListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-interface {v2, p1}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 219
    :cond_0
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    # getter for: Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->externalListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->access$200(Lcom/vlingo/core/internal/audio/AudioPlayerProxy;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .line 220
    .local v0, "externalListener":Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;
    # getter for: Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "externalListener "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    goto :goto_0

    .line 223
    .end local v0    # "externalListener":Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;
    :cond_1
    return-void
.end method
