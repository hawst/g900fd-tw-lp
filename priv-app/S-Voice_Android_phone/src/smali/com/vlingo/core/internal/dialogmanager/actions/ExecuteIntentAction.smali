.class public Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "ExecuteIntentAction.java"


# instance fields
.field private className:Ljava/lang/String;

.field private extras:Ljava/lang/String;

.field private intent:Landroid/content/Intent;

.field private intentArgument:Ljava/lang/String;

.field private intentName:Ljava/lang/String;

.field private isBroadcast:Z

.field private isService:Z

.field private typeName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method

.method private buildIntent()Landroid/content/Intent;
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 67
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intentName:Ljava/lang/String;

    if-nez v9, :cond_1

    .line 68
    const/4 v3, 0x0

    .line 98
    :cond_0
    :goto_0
    return-object v3

    .line 70
    :cond_1
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 71
    .local v3, "intentToLaunch":Landroid/content/Intent;
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intentName:Ljava/lang/String;

    invoke-virtual {v3, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intentArgument:Ljava/lang/String;

    if-eqz v9, :cond_2

    .line 73
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intentArgument:Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v3, v9}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 75
    :cond_2
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->typeName:Ljava/lang/String;

    invoke-static {v9}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 76
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->typeName:Ljava/lang/String;

    invoke-virtual {v3, v9}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    :cond_3
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->extras:Ljava/lang/String;

    if-eqz v9, :cond_7

    .line 79
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->extras:Ljava/lang/String;

    const-string/jumbo v10, ";"

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v4, :cond_7

    aget-object v1, v0, v2

    .line 80
    .local v1, "extra":Ljava/lang/String;
    const-string/jumbo v9, ","

    invoke-virtual {v1, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 81
    .local v6, "nameAndValue":[Ljava/lang/String;
    array-length v9, v6

    const/4 v10, 0x2

    if-ne v9, v10, :cond_4

    .line 82
    aget-object v5, v6, v11

    .line 83
    .local v5, "name":Ljava/lang/String;
    aget-object v8, v6, v12

    .line 84
    .local v8, "value":Ljava/lang/String;
    const-string/jumbo v9, "true"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 85
    invoke-virtual {v3, v5, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 79
    .end local v5    # "name":Ljava/lang/String;
    .end local v8    # "value":Ljava/lang/String;
    :cond_4
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 86
    .restart local v5    # "name":Ljava/lang/String;
    .restart local v8    # "value":Ljava/lang/String;
    :cond_5
    const-string/jumbo v9, "false"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 87
    invoke-virtual {v3, v5, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_2

    .line 89
    :cond_6
    invoke-virtual {v3, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    .line 94
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "extra":Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "nameAndValue":[Ljava/lang/String;
    .end local v8    # "value":Ljava/lang/String;
    :cond_7
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->className:Ljava/lang/String;

    invoke-static {v9}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->className:Ljava/lang/String;

    const-string/jumbo v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 95
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->className:Ljava/lang/String;

    const-string/jumbo v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 96
    .local v7, "names":[Ljava/lang/String;
    aget-object v9, v7, v11

    aget-object v10, v7, v12

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0
.end method


# virtual methods
.method public argument(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;
    .locals 0
    .param p1, "argument"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intentArgument:Ljava/lang/String;

    .line 38
    return-object p0
.end method

.method public broadcast(Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;
    .locals 0
    .param p1, "isBroadcastParam"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->isBroadcast:Z

    .line 53
    return-object p0
.end method

.method public className(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;
    .locals 0
    .param p1, "classNameParam"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->className:Ljava/lang/String;

    .line 48
    return-object p0
.end method

.method protected execute()V
    .locals 4

    .prologue
    .line 114
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent:Landroid/content/Intent;

    if-eqz v2, :cond_2

    .line 117
    :try_start_0
    iget-boolean v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->isService:Z

    if-eqz v2, :cond_0

    .line 118
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent:Landroid/content/Intent;

    invoke-virtual {v2, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 124
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 142
    :goto_1
    return-void

    .line 119
    :cond_0
    iget-boolean v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->isBroadcast:Z

    if-eqz v2, :cond_1

    .line 120
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent:Landroid/content/Intent;

    invoke-virtual {v2, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 125
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_1

    .line 122
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent:Landroid/content/Intent;

    invoke-virtual {v2, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 128
    :cond_2
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intentName:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 129
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->buildIntent()Landroid/content/Intent;

    move-result-object v1

    .line 130
    .local v1, "newIntent":Landroid/content/Intent;
    iget-boolean v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->isBroadcast:Z

    if-eqz v2, :cond_3

    .line 131
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 138
    :goto_2
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    goto :goto_1

    .line 132
    :cond_3
    iget-boolean v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->isService:Z

    if-eqz v2, :cond_4

    .line 133
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_2

    .line 135
    :cond_4
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 136
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    .line 140
    .end local v1    # "newIntent":Landroid/content/Intent;
    :cond_5
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v3, "No intent name"

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public extra(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;
    .locals 0
    .param p1, "extra"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->extras:Ljava/lang/String;

    .line 43
    return-object p0
.end method

.method public intent(Landroid/content/Intent;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;
    .locals 0
    .param p1, "intentParam"    # Landroid/content/Intent;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent:Landroid/content/Intent;

    .line 28
    return-object p0
.end method

.method public isAvailable()Z
    .locals 2

    .prologue
    .line 102
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent:Landroid/content/Intent;

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->buildIntent()Landroid/content/Intent;

    move-result-object v0

    .line 103
    .local v0, "intentToCheck":Landroid/content/Intent;
    :goto_0
    iget-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->isBroadcast:Z

    if-eqz v1, :cond_1

    .line 104
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/vlingo/sdk/internal/util/PackageUtil;->canHandleBroadcastIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    .line 108
    :goto_1
    return v1

    .line 102
    .end local v0    # "intentToCheck":Landroid/content/Intent;
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent:Landroid/content/Intent;

    goto :goto_0

    .line 105
    .restart local v0    # "intentToCheck":Landroid/content/Intent;
    :cond_1
    iget-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->isService:Z

    if-eqz v1, :cond_2

    .line 106
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/vlingo/sdk/internal/util/PackageUtil;->canHandleServiceIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    goto :goto_1

    .line 108
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/vlingo/sdk/internal/util/PackageUtil;->canHandleIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    goto :goto_1
.end method

.method public name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intentName:Ljava/lang/String;

    .line 33
    return-object p0
.end method

.method public service(Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;
    .locals 0
    .param p1, "isServiceParam"    # Z

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->isService:Z

    .line 58
    return-object p0
.end method

.method public type(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;
    .locals 0
    .param p1, "typeNameParam"    # Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->typeName:Ljava/lang/String;

    .line 63
    return-object p0
.end method
