.class public interface abstract Lcom/vlingo/core/internal/audio/IAudioPlaybackService;
.super Ljava/lang/Object;
.source "IAudioPlaybackService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;
    }
.end annotation


# virtual methods
.method public abstract isPlaying()Z
.end method

.method public abstract pause()V
.end method

.method public abstract play(Lcom/vlingo/core/internal/audio/AudioRequest;)V
.end method

.method public abstract play(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
.end method

.method public abstract resume()V
.end method

.method public abstract stop()V
.end method
