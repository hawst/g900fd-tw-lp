.class public final Lcom/vlingo/core/internal/dialogmanager/RecoContext;
.super Ljava/lang/Object;
.source "RecoContext.java"


# static fields
.field private static final CXT_AUTO_PUNCTUATION:Z = true

.field private static final CXT_CAPITALIZATION:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

.field private static final CXT_CURRENT_TEXT:Ljava/lang/String;

.field private static final CXT_CURSOR_POSITION:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;->DEFAULT:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/RecoContext;->CXT_CAPITALIZATION:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    .line 28
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/RecoContext;->CXT_CURRENT_TEXT:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addEvents(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;Ljava/util/List;)V
    .locals 4
    .param p0, "contextBuilder"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/DialogEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 163
    .local p1, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/DialogEvent;>;"
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 171
    :cond_0
    return-void

    .line 165
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;

    .line 166
    .local v1, "event":Lcom/vlingo/core/internal/dialogmanager/DialogEvent;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;->getVLDialogEvent()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;

    move-result-object v0

    .line 167
    .local v0, "dlEvent":Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    if-eqz v0, :cond_2

    .line 168
    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->addEvent(Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;)Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    goto :goto_0
.end method

.method public static addMetaProperties(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;Ljava/util/Map;)V
    .locals 4
    .param p0, "builder"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 76
    .local p1, "metaProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez p0, :cond_1

    .line 85
    :cond_0
    return-void

    .line 80
    :cond_1
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 81
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 82
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p0, v2, v3}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->addMetaKVPair(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    goto :goto_0
.end method

.method public static addReco(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    .locals 8
    .param p0, "builder"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;
    .param p1, "fieldID"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 34
    if-nez p0, :cond_0

    .line 61
    :goto_0
    return-void

    .line 41
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->getFieldId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->fieldID(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .line 42
    const-string/jumbo v0, "max_audio_time"

    const v1, 0x9c40

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->maxAudioTime(I)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .line 43
    const-string/jumbo v0, "auto_endpointing"

    invoke-static {v0, v6}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->autoEndpointing(Z)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .line 44
    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->getTimeWithSpeechInMilliseconds()I

    move-result v0

    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->getTimeNoSpeechInMillisecond()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->autoEndPointingTimeouts(II)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .line 46
    const-string/jumbo v0, "endpoint.speechdetect_threshold"

    const/high16 v1, 0x41300000    # 11.0f

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v0

    const-string/jumbo v1, "endpoint.speechdetect_voice_duration"

    const v2, 0x3da3d70a    # 0.08f

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v1

    const-string/jumbo v2, "endpoint.speechdetect_voice_portion"

    const v3, 0x3ca3d70a    # 0.02f

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v2

    const-string/jumbo v3, "endpoint.speechdetect_min_voice_level"

    const/high16 v4, 0x42640000    # 57.0f

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->speechDetectorParams(FFFF)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .line 50
    invoke-virtual {p0, v6}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->autoPunctuation(Z)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .line 51
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/RecoContext;->CXT_CAPITALIZATION:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->capitalizationMode(Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .line 52
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/RecoContext;->CXT_CURRENT_TEXT:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->currentText(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .line 53
    invoke-virtual {p0, v5}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->cursorPosition(I)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .line 54
    const-string/jumbo v0, "profanity_filter"

    invoke-static {v0, v6}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->profanityFilter(Z)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .line 55
    const-string/jumbo v0, "Vlingo"

    invoke-virtual {p0, v0, v7, v7}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->appInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .line 56
    const-string/jumbo v0, "dm.username"

    const-string/jumbo v1, ""

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->setUsername(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    .line 57
    const-string/jumbo v0, "speex.complexity"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    const-string/jumbo v1, "speex.quality"

    const/16 v2, 0x8

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v1

    const-string/jumbo v2, "speex.variable_bitrate"

    invoke-static {v2, v5}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v2

    const-string/jumbo v3, "speex.voice_activity_detection"

    invoke-static {v3, v5}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->speexParams(IIII)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    goto/16 :goto_0
.end method

.method public static addUserProperties(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;Ljava/util/Map;)V
    .locals 4
    .param p0, "builder"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p1, "userProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez p0, :cond_1

    .line 73
    :cond_0
    return-void

    .line 68
    :cond_1
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 69
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 70
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p0, v2, v3}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->addDMHeaderKVPair(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    goto :goto_0
.end method

.method public static init(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;Ljava/lang/String;I[B)V
    .locals 2
    .param p0, "builder"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;
    .param p1, "dialogGUID"    # Ljava/lang/String;
    .param p2, "dialogTurn"    # I
    .param p3, "dialogState"    # [B

    .prologue
    .line 151
    if-nez p0, :cond_1

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->language(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .line 153
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->setDialogGUID(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    .line 154
    invoke-virtual {p0, p2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->setTurnNumber(I)Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    .line 155
    invoke-virtual {p0, p3}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->setState([B)Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    .line 157
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getDialogOriginator()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 158
    const-string/jumbo v0, "dialogOriginator"

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getDialogOriginator()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->addDMHeaderKVPair(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    goto :goto_0
.end method

.method public static setFileSource(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;Ljava/lang/String;Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;)V
    .locals 1
    .param p0, "builder"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;
    .param p1, "audioFile"    # Ljava/lang/String;
    .param p2, "format"    # Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    .prologue
    .line 131
    if-nez p0, :cond_1

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    if-eqz p1, :cond_0

    .line 142
    invoke-static {p1, p2}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getFileSource(Ljava/lang/String;Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;)Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->audioSourceInfo(Lcom/vlingo/sdk/recognition/AudioSourceInfo;)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    goto :goto_0
.end method

.method public static setRecoSource(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
    .locals 1
    .param p0, "builder"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;
    .param p1, "micStream"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    .line 88
    if-nez p0, :cond_1

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    if-eqz p1, :cond_0

    .line 99
    invoke-virtual {p1}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->is16KHz()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 100
    sget-object v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;->PCM_16KHZ_16BIT:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    invoke-static {p1, v0}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getStreamSource(Ljava/io/InputStream;Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;)Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->audioSourceInfo(Lcom/vlingo/sdk/recognition/AudioSourceInfo;)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    goto :goto_0

    .line 102
    :cond_2
    invoke-virtual {p1}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->is8KHz()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    sget-object v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;->PCM_8KHZ_16BIT:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    invoke-static {p1, v0}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getStreamSource(Ljava/io/InputStream;Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;)Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->audioSourceInfo(Lcom/vlingo/sdk/recognition/AudioSourceInfo;)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    goto :goto_0
.end method

.method public static setTextSource(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;Ljava/lang/String;)V
    .locals 1
    .param p0, "builder"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 112
    if-nez p0, :cond_1

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    if-eqz p1, :cond_0

    .line 123
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getStringSource(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->audioSourceInfo(Lcom/vlingo/sdk/recognition/AudioSourceInfo;)Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    goto :goto_0
.end method
