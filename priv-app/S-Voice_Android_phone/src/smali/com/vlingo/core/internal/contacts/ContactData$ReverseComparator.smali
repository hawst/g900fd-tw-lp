.class public Lcom/vlingo/core/internal/contacts/ContactData$ReverseComparator;
.super Ljava/lang/Object;
.source "ContactData.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/contacts/ContactData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ReverseComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/vlingo/core/internal/contacts/ContactData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/vlingo/core/internal/contacts/ContactData;Lcom/vlingo/core/internal/contacts/ContactData;)I
    .locals 1
    .param p1, "cd1"    # Lcom/vlingo/core/internal/contacts/ContactData;
    .param p2, "cd2"    # Lcom/vlingo/core/internal/contacts/ContactData;

    .prologue
    .line 172
    invoke-virtual {p2, p1}, Lcom/vlingo/core/internal/contacts/ContactData;->compareTo(Lcom/vlingo/core/internal/contacts/ContactData;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 169
    check-cast p1, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/core/internal/contacts/ContactData$ReverseComparator;->compare(Lcom/vlingo/core/internal/contacts/ContactData;Lcom/vlingo/core/internal/contacts/ContactData;)I

    move-result v0

    return v0
.end method
