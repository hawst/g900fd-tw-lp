.class Lcom/vlingo/core/internal/audio/MicrophoneStream$1;
.super Ljava/lang/Object;
.source "MicrophoneStream.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/audio/MicrophoneStream;->startRecordingJBNew(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/audio/MicrophoneStream;

.field final synthetic val$audioSessionId:I


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/audio/MicrophoneStream;I)V
    .locals 0

    .prologue
    .line 323
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream$1;->this$0:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    iput p2, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream$1;->val$audioSessionId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 327
    const/4 v1, 0x1

    invoke-static {v1}, Landroid/media/MediaSyncEvent;->createEvent(I)Landroid/media/MediaSyncEvent;

    move-result-object v0

    .line 328
    .local v0, "mse":Landroid/media/MediaSyncEvent;
    iget v1, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream$1;->val$audioSessionId:I

    invoke-virtual {v0, v1}, Landroid/media/MediaSyncEvent;->setAudioSessionId(I)Landroid/media/MediaSyncEvent;

    .line 329
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/MicrophoneStream$1;->this$0:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    # getter for: Lcom/vlingo/core/internal/audio/MicrophoneStream;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v1}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->access$000(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Landroid/media/AudioRecord;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/media/AudioRecord;->startRecording(Landroid/media/MediaSyncEvent;)V

    .line 330
    return-void
.end method
