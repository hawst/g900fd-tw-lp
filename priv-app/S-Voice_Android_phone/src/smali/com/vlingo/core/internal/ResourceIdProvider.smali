.class public interface abstract Lcom/vlingo/core/internal/ResourceIdProvider;
.super Ljava/lang/Object;
.source "ResourceIdProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/ResourceIdProvider$raw;,
        Lcom/vlingo/core/internal/ResourceIdProvider$array;,
        Lcom/vlingo/core/internal/ResourceIdProvider$string;,
        Lcom/vlingo/core/internal/ResourceIdProvider$layout;,
        Lcom/vlingo/core/internal/ResourceIdProvider$id;,
        Lcom/vlingo/core/internal/ResourceIdProvider$drawable;
    }
.end annotation


# virtual methods
.method public abstract getDrawable(Lcom/vlingo/core/internal/ResourceIdProvider$drawable;)Landroid/graphics/drawable/Drawable;
.end method

.method public abstract getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$array;)I
.end method

.method public abstract getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$drawable;)I
.end method

.method public abstract getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$id;)I
.end method

.method public abstract getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$layout;)I
.end method

.method public abstract getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$raw;)I
.end method

.method public abstract getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$string;)I
.end method

.method public abstract getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;
.end method

.method public varargs abstract getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
.end method

.method public abstract getStringArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;)[Ljava/lang/String;
.end method
