.class public interface abstract Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;
.super Ljava/lang/Object;
.source "UploadedChecksumProvider.java"


# virtual methods
.method public abstract clearCurrentChecksums(Lcom/vlingo/core/internal/lmttvocon/UploadType;)V
.end method

.method public abstract getCurrentChecksums(Lcom/vlingo/core/internal/lmttvocon/UploadType;)Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/lmttvocon/UploadType;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract updateChecksums(Lcom/vlingo/core/internal/lmttvocon/UploadType;Ljava/util/Iterator;Ljava/util/Iterator;Ljava/util/Iterator;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/lmttvocon/UploadType;",
            "Ljava/util/Iterator",
            "<+",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;>;",
            "Ljava/util/Iterator",
            "<+",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;>;",
            "Ljava/util/Iterator",
            "<+",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation
.end method
