.class public interface abstract Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;
.super Ljava/lang/Object;
.source "ClientSuppliedValuesInterface.java"

# interfaces
.implements Lcom/vlingo/sdk/util/AllowsLocation;


# virtual methods
.method public abstract disableAppCarMode()V
.end method

.method public abstract disablePhoneDrivingMode()V
.end method

.method public abstract enableAppCarMode()V
.end method

.method public abstract enablePhoneDrivingMode()V
.end method

.method public abstract getADMController()Lcom/vlingo/core/internal/util/ADMController;
.end method

.method public abstract getAssets()Landroid/content/res/AssetManager;
.end method

.method public abstract getCarModePackageName()Ljava/lang/String;
.end method

.method public abstract getClientAudioValues()Lcom/vlingo/core/facade/audio/ClientAudioValues;
.end method

.method public abstract getConfiguration()Landroid/content/res/Configuration;
.end method

.method public abstract getContactProviderName()Ljava/lang/String;
.end method

.method public abstract getContentResolver()Landroid/content/ContentResolver;
.end method

.method public abstract getDefaultFieldId()Ljava/lang/String;
.end method

.method public abstract getDrivingModeWidgetMax()I
.end method

.method public abstract getFmRadioApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;
.end method

.method public abstract getForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
.end method

.method public abstract getHomeAddress()Ljava/lang/String;
.end method

.method public abstract getLaunchingClass()Ljava/lang/Class;
.end method

.method public abstract getMaxDisplayNumber()I
.end method

.method public abstract getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;
.end method

.method public abstract getMmsBodyText(Landroid/database/Cursor;)Ljava/lang/String;
.end method

.method public abstract getMmsSubject(Landroid/database/Cursor;)Ljava/lang/String;
.end method

.method public abstract getRegularWidgetMax()I
.end method

.method public abstract getRelativePriority()B
.end method

.method public abstract getSafeReaderHandler(ZLjava/util/LinkedList;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;)",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;"
        }
    .end annotation
.end method

.method public abstract getSettingsProviderName()Ljava/lang/String;
.end method

.method public abstract getWakeLockManager()Lcom/vlingo/core/internal/display/WakeLockManager;
.end method

.method public abstract isAppCarModeEnabled()Z
.end method

.method public abstract isBlockingMode()Z
.end method

.method public abstract isCurrentModelNotExactlySynchronizedWithPlayingTts()Z
.end method

.method public abstract isDSPSeamlessWakeupEnabled()Z
.end method

.method public abstract isEmbeddedBrowserUsedForSearchResults()Z
.end method

.method public abstract isEyesFree()Z
.end method

.method public abstract isGearProviderExistOnPhone()Z
.end method

.method public abstract isIUXComplete()Z
.end method

.method public abstract isInDmFlowChanging()Z
.end method

.method public abstract isLanguageChangeAllowed()Z
.end method

.method public abstract isMdsoApplication()Z
.end method

.method public abstract isMessageReadbackFlowEnabled()Z
.end method

.method public abstract isMessagingLocked()Z
.end method

.method public abstract isMiniTabletDevice()Z
.end method

.method public abstract isPhoneDrivingModeEnabled()Z
.end method

.method public abstract isReadMessageBodyEnabled()Z
.end method

.method public abstract isSeamless()Z
.end method

.method public abstract isTalkbackOn()Z
.end method

.method public abstract isTutorialMode()Z
.end method

.method public abstract isVideoCallingSupported()Z
.end method

.method public abstract isViewCoverOpened()Z
.end method

.method public abstract releaseForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
.end method

.method public abstract setInDmFlowChanging(Z)V
.end method

.method public abstract shouldIncomingMessagesReadout()Z
.end method

.method public abstract shouldSetWideBandSpeechToUse16khzVoice()Z
.end method

.method public abstract showViewCoverUi()V
.end method

.method public abstract supportsSVoiceAssociatedServiceOnly()Z
.end method

.method public abstract updateCurrentLocale(Landroid/content/res/Resources;)V
.end method

.method public abstract useGoogleSearch()Z
.end method
