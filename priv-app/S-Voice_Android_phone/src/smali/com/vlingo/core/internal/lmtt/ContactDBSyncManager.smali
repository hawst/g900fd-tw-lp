.class Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;
.super Ljava/lang/Object;
.source "ContactDBSyncManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$1;,
        Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;,
        Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;,
        Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$Listener;
    }
.end annotation


# instance fields
.field private mLastReceivedRequestId:I

.field private mSyncHandler:Landroid/os/Handler;

.field private mWorkerThread:Landroid/os/HandlerThread;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$Listener;)V
    .locals 3
    .param p1, "listener"    # Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$Listener;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "SyncWorker"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;->mWorkerThread:Landroid/os/HandlerThread;

    .line 31
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;->mWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 32
    new-instance v0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;

    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;->mWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, p1, v2}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;-><init>(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;Landroid/os/Looper;Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$Listener;Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$1;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;->mSyncHandler:Landroid/os/Handler;

    .line 33
    return-void
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    .prologue
    .line 16
    iget v0, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;->mLastReceivedRequestId:I

    return v0
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;->mSyncHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method destroy()V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;->mWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 53
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 54
    return-void
.end method

.method fireSync(Z)V
    .locals 5
    .param p1, "skipDelay"    # Z

    .prologue
    .line 41
    iget v3, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;->mLastReceivedRequestId:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;->mLastReceivedRequestId:I

    .line 42
    new-instance v2, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;-><init>(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$1;)V

    .line 43
    .local v2, "newTask":Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;
    iget-object v3, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 44
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 45
    .local v0, "data":Landroid/os/Bundle;
    const-string/jumbo v3, "skip_delay"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 46
    const-string/jumbo v3, "request_id"

    iget v4, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;->mLastReceivedRequestId:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 47
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 48
    iget-object v3, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;->mSyncHandler:Landroid/os/Handler;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 49
    return-void
.end method
