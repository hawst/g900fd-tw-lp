.class public Lcom/vlingo/core/internal/messages/MessageSenderSummaries;
.super Ljava/lang/Object;
.source "MessageSenderSummaries.java"


# instance fields
.field private summaries:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/MessageSenderSummary;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->summaries:Ljava/util/LinkedList;

    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/util/Queue;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 41
    .local p1, "alertQueue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    return-void
.end method

.method public constructor <init>(Ljava/util/Queue;Z)V
    .locals 5
    .param p2, "collapse"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p1, "alertQueue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    iput-object v4, p0, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->summaries:Ljava/util/LinkedList;

    .line 23
    invoke-interface {p1}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 25
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 26
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 27
    .local v0, "alert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    const/4 v2, 0x0

    .line 28
    .local v2, "matched":Lcom/vlingo/core/internal/messages/MessageSenderSummary;
    if-eqz p2, :cond_0

    .line 29
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->getMatchedSender(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Lcom/vlingo/core/internal/messages/MessageSenderSummary;

    move-result-object v2

    .line 31
    :cond_0
    if-eqz v2, :cond_1

    .line 32
    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/MessageSenderSummary;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v2, v4}, Lcom/vlingo/core/internal/messages/MessageSenderSummary;->setCount(I)V

    goto :goto_0

    .line 34
    :cond_1
    new-instance v3, Lcom/vlingo/core/internal/messages/MessageSenderSummary;

    const/4 v4, 0x1

    invoke-direct {v3, v0, v4}, Lcom/vlingo/core/internal/messages/MessageSenderSummary;-><init>(Lcom/vlingo/core/internal/messages/SMSMMSAlert;I)V

    .line 35
    .local v3, "newLineItem":Lcom/vlingo/core/internal/messages/MessageSenderSummary;
    iget-object v4, p0, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->summaries:Ljava/util/LinkedList;

    invoke-virtual {v4, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 38
    .end local v0    # "alert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v2    # "matched":Lcom/vlingo/core/internal/messages/MessageSenderSummary;
    .end local v3    # "newLineItem":Lcom/vlingo/core/internal/messages/MessageSenderSummary;
    :cond_2
    return-void
.end method

.method private getMatchedSender(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Lcom/vlingo/core/internal/messages/MessageSenderSummary;
    .locals 4
    .param p1, "candidateAlert"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .prologue
    .line 53
    iget-object v2, p0, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->summaries:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 55
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/internal/messages/MessageSenderSummary;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 56
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/messages/MessageSenderSummary;

    .line 57
    .local v1, "safeReaderSenderSummary":Lcom/vlingo/core/internal/messages/MessageSenderSummary;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/MessageSenderSummary;->getAlert()Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 66
    .end local v1    # "safeReaderSenderSummary":Lcom/vlingo/core/internal/messages/MessageSenderSummary;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getSummaries()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/MessageSenderSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->summaries:Ljava/util/LinkedList;

    return-object v0
.end method
