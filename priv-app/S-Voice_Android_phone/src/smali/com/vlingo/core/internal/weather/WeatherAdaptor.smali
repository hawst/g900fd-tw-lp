.class public Lcom/vlingo/core/internal/weather/WeatherAdaptor;
.super Ljava/lang/Object;
.source "WeatherAdaptor.java"

# interfaces
.implements Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/weather/WeatherAdaptor$1;,
        Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;
    }
.end annotation


# static fields
.field private static final PROVIDER:Ljava/lang/String; = "Provider"

.field public static final WEATHERNEWS_CONTENT_PROVIDER:Ljava/lang/String; = "WeathernewsContentProvider"


# instance fields
.field private currentRequest:Ljava/lang/String;

.field private date:Ljava/lang/String;

.field private failReason:Ljava/lang/String;

.field private isRequestComplete:Z

.field private isRequestFailed:Z

.field private isToday:Z

.field private location:Ljava/lang/String;

.field private lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

.field private spokenDate:Ljava/lang/String;

.field private weatherElement:Lcom/vlingo/core/internal/weather/WeatherElement;

.field private widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-boolean v1, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->isRequestComplete:Z

    .line 20
    iput-boolean v1, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->isRequestFailed:Z

    .line 21
    iput-boolean v1, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->isToday:Z

    .line 22
    iput-object v2, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->failReason:Ljava/lang/String;

    .line 27
    iput-object v2, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .line 37
    iput-object v2, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/WeatherElement;

    .line 38
    new-instance v0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    invoke-direct {v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    .line 39
    iput-boolean v1, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->isRequestComplete:Z

    .line 40
    iput-object v2, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->currentRequest:Ljava/lang/String;

    .line 41
    return-void
.end method

.method private sendWeatherRequest(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "req"    # Ljava/lang/String;
    .param p2, "day"    # Ljava/lang/String;
    .param p3, "force"    # Z

    .prologue
    .line 69
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->sendWeatherRequest(Ljava/lang/String;Ljava/lang/String;ZI)V

    .line 70
    return-void
.end method

.method private sendWeatherRequest(Ljava/lang/String;Ljava/lang/String;ZI)V
    .locals 1
    .param p1, "req"    # Ljava/lang/String;
    .param p2, "day"    # Ljava/lang/String;
    .param p3, "force"    # Z
    .param p4, "version"    # I

    .prologue
    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->isRequestComplete:Z

    .line 75
    if-nez p1, :cond_0

    .line 76
    const-string/jumbo p1, ""

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->currentRequest:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p3, :cond_2

    .line 80
    :cond_1
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->currentRequest:Ljava/lang/String;

    .line 81
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    invoke-virtual {v0, p1, p2, p0, p4}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sendWeatherRequest(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;I)V

    .line 83
    :cond_2
    return-void
.end method

.method private declared-synchronized setWeatherElement(Lcom/vlingo/core/internal/weather/WeatherElement;)V
    .locals 5
    .param p1, "weatherElement"    # Lcom/vlingo/core/internal/weather/WeatherElement;

    .prologue
    .line 132
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/WeatherElement;

    .line 133
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v0

    .line 134
    .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v3, "WeatherText"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 135
    .local v2, "wt":Ljava/lang/String;
    const-string/jumbo v3, "Temperature"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    .local v1, "temp":Ljava/lang/String;
    monitor-exit p0

    return-void

    .line 132
    .end local v0    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "temp":Ljava/lang/String;
    .end local v2    # "wt":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method


# virtual methods
.method public getCurrentRequest()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->currentRequest:Ljava/lang/String;

    return-object v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->date:Ljava/lang/String;

    return-object v0
.end method

.method public getFailReason()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->failReason:Ljava/lang/String;

    return-object v0
.end method

.method public getFullLocation()Ljava/lang/String;
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/WeatherElement;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v1, "Location"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->location:Ljava/lang/String;

    return-object v0
.end method

.method public getLocationCity()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
        }
    .end annotation

    .prologue
    .line 258
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/WeatherElement;

    const-string/jumbo v2, "WeatherLocation"

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v1

    const-string/jumbo v2, "Location"

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v1

    const-string/jumbo v2, "City"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 262
    .local v0, "city":Ljava/lang/String;
    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    .line 263
    :cond_0
    return-object v0
.end method

.method public getLocationState()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
        }
    .end annotation

    .prologue
    .line 248
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/WeatherElement;

    const-string/jumbo v2, "WeatherLocation"

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v1

    const-string/jumbo v2, "Location"

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v1

    const-string/jumbo v2, "State"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 253
    .local v0, "locationState":Ljava/lang/String;
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getLocationCity()Ljava/lang/String;

    move-result-object v0

    .line 254
    :cond_0
    return-object v0
.end method

.method public getProvider()Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;
    .locals 3

    .prologue
    .line 234
    const-string/jumbo v0, "WeathernewsContentProvider"

    iget-object v1, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/WeatherElement;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v1

    const-string/jumbo v2, "Provider"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    sget-object v0, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;->WEATHERNEWS:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    .line 236
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;->ACCUWEATHER:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    goto :goto_0
.end method

.method public getSpokenDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->spokenDate:Ljava/lang/String;

    return-object v0
.end method

.method public getWeatherElement()Lcom/vlingo/core/internal/weather/WeatherElement;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/WeatherElement;

    return-object v0
.end method

.method public getWidgetListener()Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    return-object v0
.end method

.method public isDefaultLocation()Z
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/WeatherElement;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/weather/WeatherElement;->isDefaultLocation()Z

    move-result v0

    return v0
.end method

.method public isRequestComplete()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->isRequestComplete:Z

    return v0
.end method

.method public isRequestFailed()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->isRequestFailed:Z

    return v0
.end method

.method public isToday()Z
    .locals 1

    .prologue
    .line 271
    iget-boolean v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->isToday:Z

    return v0
.end method

.method public onRequestComplete(ZLjava/lang/Object;)V
    .locals 1
    .param p1, "success"    # Z
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 156
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->isRequestComplete:Z

    .line 157
    if-eqz p1, :cond_0

    .line 160
    instance-of v0, p2, Lcom/vlingo/core/internal/weather/WeatherElement;

    if-eqz v0, :cond_0

    .line 161
    check-cast p2, Lcom/vlingo/core/internal/weather/WeatherElement;

    .end local p2    # "item":Ljava/lang/Object;
    invoke-direct {p0, p2}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->setWeatherElement(Lcom/vlingo/core/internal/weather/WeatherElement;)V

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    if-eqz v0, :cond_1

    .line 169
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onResponseReceived()V

    .line 171
    :cond_1
    return-void
.end method

.method public onRequestFailed(Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;)V
    .locals 2
    .param p1, "failureType"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;

    .prologue
    .line 186
    sget-object v0, Lcom/vlingo/core/internal/weather/WeatherAdaptor$1;->$SwitchMap$com$vlingo$core$internal$localsearch$LocalSearchRequestListener$LocalSearchFailureType:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 198
    :goto_0
    return-void

    .line 188
    :pswitch_0
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_bad_response:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0

    .line 191
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_no_results:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0

    .line 194
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_bad_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0

    .line 186
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onRequestFailed(Ljava/lang/String;)V
    .locals 1
    .param p1, "failReasonParam"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 175
    iput-boolean v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->isRequestComplete:Z

    .line 176
    iput-boolean v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->isRequestFailed:Z

    .line 177
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->failReason:Ljava/lang/String;

    .line 179
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed()V

    .line 182
    :cond_0
    return-void
.end method

.method public onRequestScheduled()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 202
    iput-boolean v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->isRequestComplete:Z

    .line 203
    iput-boolean v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->isRequestFailed:Z

    .line 205
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestScheduled()V

    .line 208
    :cond_0
    return-void
.end method

.method public resetWeatherElement()V
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/WeatherElement;

    .line 87
    return-void
.end method

.method public runWithoutRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/weather/WeatherElement;)V
    .locals 1
    .param p1, "dateParam"    # Ljava/lang/String;
    .param p2, "locationParam"    # Ljava/lang/String;
    .param p3, "spokenDateParam"    # Ljava/lang/String;
    .param p4, "weatherElementParam"    # Lcom/vlingo/core/internal/weather/WeatherElement;

    .prologue
    .line 281
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->date:Ljava/lang/String;

    .line 282
    iput-object p2, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->location:Ljava/lang/String;

    .line 284
    iput-object p3, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->spokenDate:Ljava/lang/String;

    .line 285
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p4}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->onRequestComplete(ZLjava/lang/Object;)V

    .line 286
    return-void
.end method

.method public sendWeatherRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "dateParam"    # Ljava/lang/String;
    .param p2, "spokenDateParam"    # Ljava/lang/String;
    .param p3, "locationParam"    # Ljava/lang/String;

    .prologue
    .line 45
    const-string/jumbo v0, "1"

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->sendWeatherRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method public sendWeatherRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "dateParam"    # Ljava/lang/String;
    .param p2, "spokenDateParam"    # Ljava/lang/String;
    .param p3, "locationParam"    # Ljava/lang/String;
    .param p4, "days"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 49
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->date:Ljava/lang/String;

    .line 50
    iput-object p2, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->spokenDate:Ljava/lang/String;

    .line 51
    iput-object p3, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->location:Ljava/lang/String;

    .line 52
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->useWeatherVpLocation()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 53
    const/4 v1, 0x2

    invoke-direct {p0, p3, p4, v2, v1}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->sendWeatherRequest(Ljava/lang/String;Ljava/lang/String;ZI)V

    .line 66
    :goto_0
    return-void

    .line 55
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 56
    .local v0, "searchStr":Ljava/lang/StringBuilder;
    if-eqz p1, :cond_2

    .line 57
    const-string/jumbo v1, "what is the weather "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    :goto_1
    if-eqz p3, :cond_1

    .line 62
    const-string/jumbo v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p4, v2}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->sendWeatherRequest(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 59
    :cond_2
    const-string/jumbo v1, "what is the weather today"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public setToday(Z)V
    .locals 0
    .param p1, "today"    # Z

    .prologue
    .line 276
    iput-boolean p1, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->isToday:Z

    .line 277
    return-void
.end method

.method public setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V
    .locals 0
    .param p1, "widgetListener"    # Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .line 152
    return-void
.end method
