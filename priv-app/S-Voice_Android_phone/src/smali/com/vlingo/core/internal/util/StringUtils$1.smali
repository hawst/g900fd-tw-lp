.class final Lcom/vlingo/core/internal/util/StringUtils$1;
.super Ljava/util/HashSet;
.source "StringUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/StringUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashSet",
        "<",
        "Ljava/lang/Character$UnicodeBlock;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    .line 56
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_COMPATIBILITY:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$1;->add(Ljava/lang/Object;)Z

    .line 57
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_COMPATIBILITY_FORMS:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$1;->add(Ljava/lang/Object;)Z

    .line 58
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_COMPATIBILITY_IDEOGRAPHS:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$1;->add(Ljava/lang/Object;)Z

    .line 59
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$1;->add(Ljava/lang/Object;)Z

    .line 60
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_RADICALS_SUPPLEMENT:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$1;->add(Ljava/lang/Object;)Z

    .line 61
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_SYMBOLS_AND_PUNCTUATION:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$1;->add(Ljava/lang/Object;)Z

    .line 62
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$1;->add(Ljava/lang/Object;)Z

    .line 63
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$1;->add(Ljava/lang/Object;)Z

    .line 64
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$1;->add(Ljava/lang/Object;)Z

    .line 65
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->KANGXI_RADICALS:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$1;->add(Ljava/lang/Object;)Z

    .line 66
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->IDEOGRAPHIC_DESCRIPTION_CHARACTERS:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$1;->add(Ljava/lang/Object;)Z

    .line 68
    return-void
.end method
