.class public Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;
.super Ljava/lang/Object;
.source "ScheduleUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/schedule/ScheduleUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ScheduleCalendar"
.end annotation


# instance fields
.field private ID:J

.field private accessLevel:I

.field private accountName:Ljava/lang/String;

.field private accountType:Ljava/lang/String;

.field private displayName:Ljava/lang/String;

.field private enabled:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1569
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1570
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->ID:J

    .line 1571
    iput-object v2, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->displayName:Ljava/lang/String;

    .line 1572
    iput-object v2, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->accountName:Ljava/lang/String;

    .line 1573
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->enabled:Z

    .line 1574
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->accessLevel:I

    .line 1575
    iput-object v2, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->accountType:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method getAccessLevel()I
    .locals 1

    .prologue
    .line 1599
    iget v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->accessLevel:I

    return v0
.end method

.method getAccountName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1590
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->accountName:Ljava/lang/String;

    return-object v0
.end method

.method getAccountType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1602
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->accountType:Ljava/lang/String;

    return-object v0
.end method

.method getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1584
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method getID()J
    .locals 2

    .prologue
    .line 1578
    iget-wide v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->ID:J

    return-wide v0
.end method

.method isEnabled()Z
    .locals 1

    .prologue
    .line 1596
    iget-boolean v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->enabled:Z

    return v0
.end method

.method setAccessLevel(I)V
    .locals 0
    .param p1, "level"    # I

    .prologue
    .line 1611
    iput p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->accessLevel:I

    .line 1612
    return-void
.end method

.method setAccountName(Ljava/lang/String;)V
    .locals 0
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 1593
    iput-object p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->accountName:Ljava/lang/String;

    .line 1594
    return-void
.end method

.method setAccountType(Ljava/lang/String;)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 1614
    iput-object p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->accountType:Ljava/lang/String;

    .line 1615
    return-void
.end method

.method setDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1, "displayName"    # Ljava/lang/String;

    .prologue
    .line 1587
    iput-object p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->displayName:Ljava/lang/String;

    .line 1588
    return-void
.end method

.method setEnabled(I)V
    .locals 1
    .param p1, "enabled"    # I

    .prologue
    .line 1608
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->enabled:Z

    .line 1609
    return-void

    .line 1608
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method setEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 1605
    iput-boolean p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->enabled:Z

    .line 1606
    return-void
.end method

.method setID(J)V
    .locals 0
    .param p1, "iD"    # J

    .prologue
    .line 1581
    iput-wide p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->ID:J

    .line 1582
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1618
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->ID:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->accountName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->enabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
