.class public Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "OpenAppAction.java"


# instance fields
.field private appInfo:Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method


# virtual methods
.method public appInfo(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;
    .locals 0
    .param p1, "appInfoParam"    # Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;->appInfo:Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .line 22
    return-object p0
.end method

.method protected execute()V
    .locals 4

    .prologue
    .line 27
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;->appInfo:Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    if-eqz v2, :cond_0

    .line 32
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;->appInfo:Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->getLaunchIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 33
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    :goto_0
    return-void

    .line 34
    :catch_0
    move-exception v0

    .line 35
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v3, "Activity could not be found."

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0

    .line 38
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_APPMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    .line 39
    .local v1, "noMatchMsg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0
.end method
