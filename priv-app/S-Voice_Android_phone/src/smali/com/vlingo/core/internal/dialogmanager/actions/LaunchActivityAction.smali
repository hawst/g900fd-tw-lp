.class public Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "LaunchActivityAction.java"


# instance fields
.field private action:Ljava/lang/String;

.field private activityName:Ljava/lang/String;

.field private appName:Ljava/lang/String;

.field private extras:Ljava/lang/String;

.field private packageName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method


# virtual methods
.method public action(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;
    .locals 0
    .param p1, "actionParam"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->action:Ljava/lang/String;

    .line 45
    return-object p0
.end method

.method public activity(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;
    .locals 0
    .param p1, "activityNameParam"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->activityName:Ljava/lang/String;

    .line 30
    return-object p0
.end method

.method public app(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;
    .locals 0
    .param p1, "appNameParam"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->appName:Ljava/lang/String;

    .line 40
    return-object p0
.end method

.method public enclosingPackage(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;
    .locals 0
    .param p1, "packageNameParam"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->packageName:Ljava/lang/String;

    .line 25
    return-object p0
.end method

.method protected execute()V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 50
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 51
    .local v4, "intent":Landroid/content/Intent;
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->action:Ljava/lang/String;

    if-nez v9, :cond_1

    .line 52
    const-string/jumbo v9, "android.intent.action.MAIN"

    invoke-virtual {v4, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    const-string/jumbo v9, "android.intent.category.LAUNCHER"

    invoke-virtual {v4, v9}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 57
    :goto_0
    new-instance v9, Landroid/content/ComponentName;

    iget-object v10, p0, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->packageName:Ljava/lang/String;

    iget-object v11, p0, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->activityName:Ljava/lang/String;

    invoke-direct {v9, v10, v11}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v9}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 58
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->extras:Ljava/lang/String;

    if-eqz v9, :cond_4

    .line 59
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->extras:Ljava/lang/String;

    const-string/jumbo v10, ";"

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v5, :cond_4

    aget-object v2, v0, v3

    .line 60
    .local v2, "extra":Ljava/lang/String;
    const-string/jumbo v9, ","

    invoke-virtual {v2, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 61
    .local v7, "nameAndValue":[Ljava/lang/String;
    array-length v9, v7

    const/4 v10, 0x2

    if-ne v9, v10, :cond_0

    .line 62
    aget-object v6, v7, v12

    .line 63
    .local v6, "name":Ljava/lang/String;
    aget-object v8, v7, v13

    .line 64
    .local v8, "value":Ljava/lang/String;
    const-string/jumbo v9, "true"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 65
    invoke-virtual {v4, v6, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 59
    .end local v6    # "name":Ljava/lang/String;
    .end local v8    # "value":Ljava/lang/String;
    :cond_0
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 55
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "extra":Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v5    # "len$":I
    .end local v7    # "nameAndValue":[Ljava/lang/String;
    :cond_1
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->action:Ljava/lang/String;

    invoke-virtual {v4, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 66
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v2    # "extra":Ljava/lang/String;
    .restart local v3    # "i$":I
    .restart local v5    # "len$":I
    .restart local v6    # "name":Ljava/lang/String;
    .restart local v7    # "nameAndValue":[Ljava/lang/String;
    .restart local v8    # "value":Ljava/lang/String;
    :cond_2
    const-string/jumbo v9, "false"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 67
    invoke-virtual {v4, v6, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_2

    .line 69
    :cond_3
    invoke-virtual {v4, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    .line 75
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "extra":Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "name":Ljava/lang/String;
    .end local v7    # "nameAndValue":[Ljava/lang/String;
    .end local v8    # "value":Ljava/lang/String;
    :cond_4
    const/high16 v9, 0x10210000

    :try_start_0
    invoke-virtual {v4, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 76
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 77
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "launch "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->appName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v9

    invoke-interface {v9}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 83
    :goto_3
    return-void

    .line 78
    :catch_0
    move-exception v1

    .line 79
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    :try_start_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Cannot start intent "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v9

    invoke-interface {v9}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    goto :goto_3

    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    :catchall_0
    move-exception v9

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v10

    invoke-interface {v10}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    throw v9
.end method

.method public extra(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;
    .locals 0
    .param p1, "extra"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->extras:Ljava/lang/String;

    .line 35
    return-object p0
.end method
