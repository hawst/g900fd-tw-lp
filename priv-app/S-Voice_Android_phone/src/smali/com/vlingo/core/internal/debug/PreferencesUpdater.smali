.class public Lcom/vlingo/core/internal/debug/PreferencesUpdater;
.super Ljava/util/HashMap;
.source "PreferencesUpdater.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Lcom/vlingo/core/internal/debug/PreferenceUpdateAction;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    return-void
.end method


# virtual methods
.method public register(Ljava/lang/String;Lcom/vlingo/core/internal/debug/PreferenceUpdateAction;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/vlingo/core/internal/debug/PreferenceUpdateAction;

    .prologue
    .line 15
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/core/internal/debug/PreferencesUpdater;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    return-void
.end method

.method public unregister(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/debug/PreferencesUpdater;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/debug/PreferencesUpdater;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    :cond_0
    return-void
.end method

.method public unregister(Ljava/lang/String;Lcom/vlingo/core/internal/debug/PreferenceUpdateAction;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/vlingo/core/internal/debug/PreferenceUpdateAction;

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/debug/PreferencesUpdater;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/debug/PreferencesUpdater;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p2, :cond_0

    .line 25
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/debug/PreferencesUpdater;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    :cond_0
    return-void
.end method
