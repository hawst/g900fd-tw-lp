.class public Lcom/vlingo/core/internal/xml/XmlAttributes;
.super Ljava/lang/Object;
.source "XmlAttributes.java"


# static fields
.field public static final ATTRIBUTE_UNDEF:B


# instance fields
.field private attributeList:Ljava/util/Vector;

.field private xmlAttributes:Lcom/vlingo/core/internal/util/ToIntHashtable;


# direct methods
.method public constructor <init>(ILcom/vlingo/core/internal/util/ToIntHashtable;)V
    .locals 1
    .param p1, "initialCapacity"    # I
    .param p2, "xmlAttributes"    # Lcom/vlingo/core/internal/util/ToIntHashtable;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p2, p0, Lcom/vlingo/core/internal/xml/XmlAttributes;->xmlAttributes:Lcom/vlingo/core/internal/util/ToIntHashtable;

    .line 39
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0, p1}, Ljava/util/Vector;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/core/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    .line 40
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/core/internal/util/ToIntHashtable;)V
    .locals 1
    .param p1, "xmlAttributes"    # Lcom/vlingo/core/internal/util/ToIntHashtable;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/vlingo/core/internal/xml/XmlAttributes;->xmlAttributes:Lcom/vlingo/core/internal/util/ToIntHashtable;

    .line 47
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    .line 48
    return-void
.end method


# virtual methods
.method public add(BLjava/lang/String;)V
    .locals 2
    .param p1, "type"    # B
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/vlingo/core/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    new-instance v1, Lcom/vlingo/core/internal/xml/XmlAttribute;

    invoke-direct {v1, p1, p2}, Lcom/vlingo/core/internal/xml/XmlAttribute;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 56
    return-void
.end method

.method public getAttribute(I)Lcom/vlingo/core/internal/xml/XmlAttribute;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 117
    iget-object v0, p0, Lcom/vlingo/core/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/xml/XmlAttribute;

    return-object v0
.end method

.method public getAttributeType([CII)B
    .locals 2
    .param p1, "data"    # [C
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 62
    invoke-static {p1, p2, p3}, Ljava/lang/String;->valueOf([CII)Ljava/lang/String;

    move-result-object v0

    .line 64
    .local v0, "name":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/core/internal/xml/XmlAttributes;->xmlAttributes:Lcom/vlingo/core/internal/util/ToIntHashtable;

    invoke-interface {v1, v0}, Lcom/vlingo/core/internal/util/ToIntHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 65
    iget-object v1, p0, Lcom/vlingo/core/internal/xml/XmlAttributes;->xmlAttributes:Lcom/vlingo/core/internal/util/ToIntHashtable;

    invoke-interface {v1, v0}, Lcom/vlingo/core/internal/util/ToIntHashtable;->get(Ljava/lang/Object;)I

    move-result v1

    int-to-byte v1, v1

    .line 67
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/vlingo/core/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public getType(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 81
    iget-object v1, p0, Lcom/vlingo/core/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 82
    iget-object v1, p0, Lcom/vlingo/core/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/xml/XmlAttribute;

    .line 83
    .local v0, "attribute":Lcom/vlingo/core/internal/xml/XmlAttribute;
    if-eqz v0, :cond_0

    .line 84
    invoke-virtual {v0}, Lcom/vlingo/core/internal/xml/XmlAttribute;->getType()I

    move-result v1

    .line 87
    .end local v0    # "attribute":Lcom/vlingo/core/internal/xml/XmlAttribute;
    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getValue(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 94
    iget-object v1, p0, Lcom/vlingo/core/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 95
    iget-object v1, p0, Lcom/vlingo/core/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/xml/XmlAttribute;

    .line 96
    .local v0, "attribute":Lcom/vlingo/core/internal/xml/XmlAttribute;
    if-eqz v0, :cond_0

    .line 97
    invoke-virtual {v0}, Lcom/vlingo/core/internal/xml/XmlAttribute;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 100
    .end local v0    # "attribute":Lcom/vlingo/core/internal/xml/XmlAttribute;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public lookup(I)Ljava/lang/String;
    .locals 3
    .param p1, "key"    # I

    .prologue
    .line 105
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vlingo/core/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 106
    iget-object v2, p0, Lcom/vlingo/core/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/xml/XmlAttribute;

    .line 107
    .local v0, "attribute":Lcom/vlingo/core/internal/xml/XmlAttribute;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/xml/XmlAttribute;->getType()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 108
    invoke-virtual {v0}, Lcom/vlingo/core/internal/xml/XmlAttribute;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 110
    .end local v0    # "attribute":Lcom/vlingo/core/internal/xml/XmlAttribute;
    :goto_1
    return-object v2

    .line 105
    .restart local v0    # "attribute":Lcom/vlingo/core/internal/xml/XmlAttribute;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 110
    .end local v0    # "attribute":Lcom/vlingo/core/internal/xml/XmlAttribute;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method
