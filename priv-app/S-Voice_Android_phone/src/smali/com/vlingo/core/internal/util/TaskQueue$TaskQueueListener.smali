.class public interface abstract Lcom/vlingo/core/internal/util/TaskQueue$TaskQueueListener;
.super Ljava/lang/Object;
.source "TaskQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/TaskQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TaskQueueListener"
.end annotation


# virtual methods
.method public abstract onQueueCancelled()V
.end method

.method public abstract onQueueDone()V
.end method

.method public abstract onTaskStarting(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V
.end method
