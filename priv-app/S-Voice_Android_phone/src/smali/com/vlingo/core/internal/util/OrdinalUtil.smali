.class public Lcom/vlingo/core/internal/util/OrdinalUtil;
.super Ljava/lang/Object;
.source "OrdinalUtil.java"


# static fields
.field public static final ALL:Ljava/lang/String; = "all"

.field public static final BOTTOM:Ljava/lang/String; = "bottom"

.field public static final LAST:Ljava/lang/String; = "last"

.field public static final LEFT:Ljava/lang/String; = "left"

.field public static final MIDDLE:Ljava/lang/String; = "middle"

.field public static final NEXT:Ljava/lang/String; = "next"

.field public static final PATTERN:Ljava/util/regex/Pattern;

.field public static final RIGHT:Ljava/lang/String; = "right"

.field public static final S_PATTERN:Ljava/lang/String; = "^(\\d{1,2})(st|nd|rd|th)$"

.field public static final TOP:Ljava/lang/String; = "top"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-string/jumbo v0, "^(\\d{1,2})(st|nd|rd|th)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/OrdinalUtil;->PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clearListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V
    .locals 2
    .param p0, "vvsAHlistener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 187
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->LIST_CONTROL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 188
    return-void
.end method

.method public static clearOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V
    .locals 2
    .param p0, "vvsAHlistener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 83
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p0, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 84
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA_COUNT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p0, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 85
    invoke-static {p0}, Lcom/vlingo/core/internal/util/OrdinalUtil;->clearListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 86
    return-void
.end method

.method public static getElement(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/lang/String;)Ljava/lang/Object;
    .locals 6
    .param p0, "vvsAHlistener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .param p1, "ordVal"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 108
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p0, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 109
    .local v2, "elements":Ljava/util/List;, "Ljava/util/List<TT;>;"
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA_COUNT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p0, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 111
    .local v1, "displayedElemented":Ljava/lang/Integer;
    if-nez v2, :cond_1

    .line 124
    :cond_0
    :goto_0
    return-object v3

    .line 115
    :cond_1
    if-nez v1, :cond_2

    .line 116
    const-string/jumbo v4, "widget_display_max"

    const/4 v5, -0x1

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 117
    .local v0, "defaultDisplayMaxSize":I
    if-lez v0, :cond_0

    .line 118
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v0, :cond_3

    .end local v0    # "defaultDisplayMaxSize":I
    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 124
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3, p1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getOrdinalElement(Ljava/util/List;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    goto :goto_0

    .line 118
    .restart local v0    # "defaultDisplayMaxSize":I
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_1
.end method

.method public static getElementFromList(Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "ordVal"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 171
    .local p0, "elements":Ljava/util/List;, "Ljava/util/List<TT;>;"
    if-nez p0, :cond_0

    .line 172
    const/4 v0, 0x0

    .line 174
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {p0, v0, p1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getOrdinalElement(Ljava/util/List;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public static getElements(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .param p0, "vvsAHlistener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .param p1, "ordVal"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 134
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p0, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 135
    .local v2, "elements":Ljava/util/List;, "Ljava/util/List<TT;>;"
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA_COUNT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p0, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 137
    .local v1, "displayedElemented":Ljava/lang/Integer;
    const/4 v3, 0x0

    .line 139
    .local v3, "retVal":Ljava/util/List;, "Ljava/util/List<TT;>;"
    if-nez v2, :cond_1

    .line 161
    :cond_0
    :goto_0
    return-object v5

    .line 144
    :cond_1
    if-nez v1, :cond_2

    .line 145
    const-string/jumbo v6, "widget_display_max"

    const/4 v7, -0x1

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 146
    .local v0, "defaultDisplayMaxSize":I
    if-lez v0, :cond_0

    .line 147
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v0, :cond_3

    .end local v0    # "defaultDisplayMaxSize":I
    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 153
    :cond_2
    const-string/jumbo v5, "all"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 154
    const/4 v5, 0x0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-interface {v2, v5, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    :goto_2
    move-object v5, v3

    .line 161
    goto :goto_0

    .line 147
    .restart local v0    # "defaultDisplayMaxSize":I
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_1

    .line 156
    .end local v0    # "defaultDisplayMaxSize":I
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v2, v5, p1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getOrdinalElement(Ljava/util/List;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 157
    .local v4, "val":Ljava/lang/Object;, "TT;"
    new-instance v3, Ljava/util/ArrayList;

    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 158
    .restart local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public static getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;
    .locals 2
    .param p0, "vvsAHlistener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 191
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->LIST_CONTROL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/util/ListControlData;

    .line 192
    .local v0, "listControlData":Lcom/vlingo/core/internal/util/ListControlData;
    return-object v0
.end method

.method public static getOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/util/List;
    .locals 1
    .param p0, "vvsAHlistener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 97
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private static getOrdinalElement(Ljava/util/List;ILjava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "numEls"    # I
    .param p2, "ordVal"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;I",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 40
    .local p0, "elements":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-static {p2, p1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->ordinalToInt(Ljava/lang/String;I)I

    move-result v0

    .line 41
    .local v0, "index":I
    if-ltz v0, :cond_0

    if-ge v0, p1, :cond_0

    .line 42
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 44
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static ordinalToInt(Ljava/lang/String;I)I
    .locals 5
    .param p0, "ordVal"    # Ljava/lang/String;
    .param p1, "numEls"    # I

    .prologue
    const/4 v4, 0x1

    .line 48
    const-string/jumbo v3, "top"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 49
    const/4 v3, 0x0

    .line 64
    :goto_0
    return v3

    .line 50
    :cond_0
    const-string/jumbo v3, "last"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string/jumbo v3, "bottom"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 51
    :cond_1
    add-int/lit8 v3, p1, -0x1

    goto :goto_0

    .line 52
    :cond_2
    const-string/jumbo v3, "middle"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    rem-int/lit8 v3, p1, 0x2

    if-ne v3, v4, :cond_3

    .line 53
    add-int/lit8 v3, p1, -0x1

    div-int/lit8 v3, v3, 0x2

    goto :goto_0

    .line 55
    :cond_3
    sget-object v3, Lcom/vlingo/core/internal/util/OrdinalUtil;->PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v3, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 56
    .local v0, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 57
    invoke-virtual {v0, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 58
    .local v1, "match":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 59
    .local v2, "val":I
    add-int/lit8 v3, v2, -0x1

    goto :goto_0

    .line 61
    .end local v1    # "match":Ljava/lang/String;
    .end local v2    # "val":I
    :cond_4
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v4, "NO MATCH"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 64
    const/4 v3, -0x1

    goto :goto_0
.end method

.method public static storeListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/util/ListControlData;)V
    .locals 1
    .param p0, "vvsAHlistener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .param p1, "data"    # Lcom/vlingo/core/internal/util/ListControlData;

    .prologue
    .line 183
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->LIST_CONTROL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p0, v0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 184
    return-void
.end method

.method private static storeListControlDataForOrdinals(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/util/List;)V
    .locals 2
    .param p0, "vvsAHlistener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 89
    .local p1, "elements":Ljava/util/List;, "Ljava/util/List<TT;>;"
    if-eqz p1, :cond_0

    .line 90
    new-instance v0, Lcom/vlingo/core/internal/util/ListControlData;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/util/ListControlData;-><init>(I)V

    invoke-static {p0, v0}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/util/ListControlData;)V

    .line 94
    :goto_0
    return-void

    .line 92
    :cond_0
    invoke-static {p0}, Lcom/vlingo/core/internal/util/OrdinalUtil;->clearListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    goto :goto_0
.end method

.method public static storeOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;I)V
    .locals 2
    .param p0, "vvsAHlistener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .param p1, "numEls"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 79
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA_COUNT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 80
    return-void
.end method

.method public static storeOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/util/List;)V
    .locals 1
    .param p0, "vvsAHlistener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p1, "elements":Ljava/util/List;, "Ljava/util/List<TT;>;"
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p0, v0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 75
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeListControlDataForOrdinals(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/util/List;)V

    .line 76
    return-void
.end method

.method public static storeOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/util/List;I)V
    .locals 2
    .param p0, "vvsAHlistener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .param p2, "numEls"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;",
            "Ljava/util/List",
            "<TT;>;I)V"
        }
    .end annotation

    .prologue
    .line 68
    .local p1, "elements":Ljava/util/List;, "Ljava/util/List<TT;>;"
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p0, v0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 69
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA_COUNT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 70
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeListControlDataForOrdinals(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/util/List;)V

    .line 71
    return-void
.end method
