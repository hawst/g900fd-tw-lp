.class public abstract Lcom/vlingo/core/internal/notification/NotificationPopUp;
.super Ljava/lang/Object;
.source "NotificationPopUp.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;
    }
.end annotation


# instance fields
.field private actionRequired:Z

.field private exitOnDecline:Z

.field private negativeButton:Ljava/lang/String;

.field private notificationText:Ljava/lang/String;

.field private positiveButton:Ljava/lang/String;

.field private title:Ljava/lang/String;

.field private type:Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

.field private version:I


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0
    .param p1, "type"    # Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;
    .param p2, "version"    # I
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "notificationText"    # Ljava/lang/String;
    .param p5, "negativeButton"    # Ljava/lang/String;
    .param p6, "positiveButton"    # Ljava/lang/String;
    .param p7, "exitOnDecline"    # Z
    .param p8, "actionRequired"    # Z

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/vlingo/core/internal/notification/NotificationPopUp;->type:Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

    .line 20
    iput p2, p0, Lcom/vlingo/core/internal/notification/NotificationPopUp;->version:I

    .line 21
    iput-object p3, p0, Lcom/vlingo/core/internal/notification/NotificationPopUp;->title:Ljava/lang/String;

    .line 22
    iput-object p4, p0, Lcom/vlingo/core/internal/notification/NotificationPopUp;->notificationText:Ljava/lang/String;

    .line 23
    iput-object p6, p0, Lcom/vlingo/core/internal/notification/NotificationPopUp;->positiveButton:Ljava/lang/String;

    .line 24
    iput-object p5, p0, Lcom/vlingo/core/internal/notification/NotificationPopUp;->negativeButton:Ljava/lang/String;

    .line 25
    iput-boolean p7, p0, Lcom/vlingo/core/internal/notification/NotificationPopUp;->exitOnDecline:Z

    .line 26
    iput-boolean p8, p0, Lcom/vlingo/core/internal/notification/NotificationPopUp;->actionRequired:Z

    .line 28
    return-void
.end method


# virtual methods
.method public abstract accept()V
.end method

.method public actionRequired()Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUp;->actionRequired:Z

    return v0
.end method

.method public abstract decline()V
.end method

.method public exitOnDecline()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUp;->exitOnDecline:Z

    return v0
.end method

.method public getNegativeButton()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUp;->negativeButton:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUp;->negativeButton:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public getNotificationText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUp;->notificationText:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUp;->notificationText:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public getPositiveButton()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUp;->positiveButton:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUp;->positiveButton:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUp;->title:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUp;->title:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public getType()Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUp;->type:Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/vlingo/core/internal/notification/NotificationPopUp;->version:I

    return v0
.end method

.method public abstract isAccepted()Z
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 72
    .local v0, "builder":Ljava/lang/StringBuilder;
    :try_start_0
    const-string/jumbo v2, "Type="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getType()Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 73
    const-string/jumbo v2, "; Action required="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->actionRequired()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 74
    const-string/jumbo v2, "; Exit on decline="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->exitOnDecline()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 75
    const-string/jumbo v2, "; Version="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getVersion()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 76
    const-string/jumbo v2, "; Title="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    const-string/jumbo v2, "; Notification text="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getNotificationText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    const-string/jumbo v2, "; Negative button text="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getNegativeButton()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    const-string/jumbo v2, "; Positive button text="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getPositiveButton()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 80
    :catch_0
    move-exception v1

    .line 83
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
