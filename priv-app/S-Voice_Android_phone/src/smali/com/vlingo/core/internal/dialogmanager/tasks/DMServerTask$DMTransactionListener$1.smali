.class Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener$1;
.super Ljava/lang/Object;
.source "DMServerTask.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/TonePlayer$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->onStateChanged(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;)V
    .locals 0

    .prologue
    .line 539
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener$1;->this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStarted(I)V
    .locals 0
    .param p1, "audioSessionId"    # I

    .prologue
    .line 551
    return-void
.end method

.method public onStopped()V
    .locals 2

    .prologue
    .line 542
    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Finished playing"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener$1;->this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    const/4 v1, 0x1

    # invokes: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->notifyIfDone(Z)V
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$1400(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;Z)V

    .line 544
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener$1;->this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->access$1500(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;)Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 545
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener$1;->this$1:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->recoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;->access$1500(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$DMTransactionListener;)Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onRecoToneStopped(Z)V

    .line 547
    :cond_0
    return-void
.end method
