.class final Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;
.super Ljava/lang/Object;
.source "ContactDBSyncManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SyncTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;->this$0:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;
    .param p2, "x1"    # Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$1;

    .prologue
    .line 176
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;-><init>(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;)V

    return-void
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;->runTask()V

    return-void
.end method

.method private end()V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;->this$0:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    # getter for: Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;->mSyncHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;->access$500(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 200
    return-void
.end method

.method private runTask()V
    .locals 3

    .prologue
    .line 187
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 188
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/vlingo/core/internal/contacts/ContactDBSyncUtil;->syncContacts(Landroid/content/Context;)V

    .line 189
    const-string/jumbo v1, "lmtt.sync_status_contact_initial"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 192
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;->end()V

    .line 193
    return-void
.end method
