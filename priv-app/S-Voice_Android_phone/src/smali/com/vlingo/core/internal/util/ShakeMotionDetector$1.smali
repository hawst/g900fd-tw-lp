.class Lcom/vlingo/core/internal/util/ShakeMotionDetector$1;
.super Ljava/lang/Object;
.source "ShakeMotionDetector.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/ShakeMotionDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/util/ShakeMotionDetector;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/util/ShakeMotionDetector;)V
    .locals 0

    .prologue
    .line 23
    iput-object p1, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector$1;->this$0:Lcom/vlingo/core/internal/util/ShakeMotionDetector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 59
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 9
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v8, 0x3

    .line 25
    iget-object v4, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v5, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector$1;->this$0:Lcom/vlingo/core/internal/util/ShakeMotionDetector;

    # getter for: Lcom/vlingo/core/internal/util/ShakeMotionDetector;->s_Accelerometer:Landroid/hardware/Sensor;
    invoke-static {v5}, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->access$000(Lcom/vlingo/core/internal/util/ShakeMotionDetector;)Landroid/hardware/Sensor;

    move-result-object v5

    if-eq v4, v5, :cond_1

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 29
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 30
    .local v0, "curTime":J
    iget-object v4, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector$1;->this$0:Lcom/vlingo/core/internal/util/ShakeMotionDetector;

    # getter for: Lcom/vlingo/core/internal/util/ShakeMotionDetector;->m_LastSensorTime:J
    invoke-static {v4}, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->access$100(Lcom/vlingo/core/internal/util/ShakeMotionDetector;)J

    move-result-wide v4

    const-wide/16 v6, 0xfa

    add-long/2addr v4, v6

    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    .line 31
    iget-object v4, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector$1;->this$0:Lcom/vlingo/core/internal/util/ShakeMotionDetector;

    # getter for: Lcom/vlingo/core/internal/util/ShakeMotionDetector;->mSensorManager:Landroid/hardware/SensorManager;
    invoke-static {v4}, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->access$200(Lcom/vlingo/core/internal/util/ShakeMotionDetector;)Landroid/hardware/SensorManager;

    move-result-object v5

    monitor-enter v5

    .line 32
    :try_start_0
    iget-object v4, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector$1;->this$0:Lcom/vlingo/core/internal/util/ShakeMotionDetector;

    # setter for: Lcom/vlingo/core/internal/util/ShakeMotionDetector;->m_LastSensorTime:J
    invoke-static {v4, v0, v1}, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->access$102(Lcom/vlingo/core/internal/util/ShakeMotionDetector;J)J

    .line 33
    iget-object v4, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector$1;->this$0:Lcom/vlingo/core/internal/util/ShakeMotionDetector;

    # getter for: Lcom/vlingo/core/internal/util/ShakeMotionDetector;->m_LastValues:[F
    invoke-static {v4}, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->access$300(Lcom/vlingo/core/internal/util/ShakeMotionDetector;)[F

    move-result-object v4

    if-nez v4, :cond_4

    .line 34
    iget-object v4, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector$1;->this$0:Lcom/vlingo/core/internal/util/ShakeMotionDetector;

    const/4 v6, 0x3

    new-array v6, v6, [F

    # setter for: Lcom/vlingo/core/internal/util/ShakeMotionDetector;->m_LastValues:[F
    invoke-static {v4, v6}, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->access$302(Lcom/vlingo/core/internal/util/ShakeMotionDetector;[F)[F

    .line 49
    :cond_2
    :goto_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v8, :cond_7

    .line 50
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    array-length v4, v4

    if-ge v2, v4, :cond_3

    .line 51
    iget-object v4, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector$1;->this$0:Lcom/vlingo/core/internal/util/ShakeMotionDetector;

    # getter for: Lcom/vlingo/core/internal/util/ShakeMotionDetector;->m_LastValues:[F
    invoke-static {v4}, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->access$300(Lcom/vlingo/core/internal/util/ShakeMotionDetector;)[F

    move-result-object v4

    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v6, v6, v2

    aput v6, v4, v2

    .line 49
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 38
    .end local v2    # "i":I
    :cond_4
    const/4 v3, 0x0

    .line 39
    .local v3, "speed":F
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    if-ge v2, v8, :cond_6

    .line 40
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    array-length v4, v4

    if-ge v2, v4, :cond_5

    .line 41
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v4, v4, v2

    iget-object v6, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector$1;->this$0:Lcom/vlingo/core/internal/util/ShakeMotionDetector;

    # getter for: Lcom/vlingo/core/internal/util/ShakeMotionDetector;->m_LastValues:[F
    invoke-static {v6}, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->access$300(Lcom/vlingo/core/internal/util/ShakeMotionDetector;)[F

    move-result-object v6

    aget v6, v6, v2

    sub-float/2addr v4, v6

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    add-float/2addr v3, v4

    .line 39
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 44
    :cond_6
    const/high16 v4, 0x41800000    # 16.0f

    cmpl-float v4, v3, v4

    if-lez v4, :cond_2

    .line 45
    iget-object v4, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector$1;->this$0:Lcom/vlingo/core/internal/util/ShakeMotionDetector;

    # getter for: Lcom/vlingo/core/internal/util/ShakeMotionDetector;->listener:Lcom/vlingo/core/internal/util/ShakeMotionDetector$ShakeMotionDetectorListener;
    invoke-static {v4}, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->access$400(Lcom/vlingo/core/internal/util/ShakeMotionDetector;)Lcom/vlingo/core/internal/util/ShakeMotionDetector$ShakeMotionDetectorListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/util/ShakeMotionDetector$ShakeMotionDetectorListener;->onShakeDetected()V

    .line 46
    iget-object v4, p0, Lcom/vlingo/core/internal/util/ShakeMotionDetector$1;->this$0:Lcom/vlingo/core/internal/util/ShakeMotionDetector;

    const-wide/32 v6, 0x7fffffff

    # += operator for: Lcom/vlingo/core/internal/util/ShakeMotionDetector;->m_LastSensorTime:J
    invoke-static {v4, v6, v7}, Lcom/vlingo/core/internal/util/ShakeMotionDetector;->access$114(Lcom/vlingo/core/internal/util/ShakeMotionDetector;J)J

    goto :goto_1

    .line 54
    .end local v2    # "i":I
    .end local v3    # "speed":F
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .restart local v2    # "i":I
    :cond_7
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
