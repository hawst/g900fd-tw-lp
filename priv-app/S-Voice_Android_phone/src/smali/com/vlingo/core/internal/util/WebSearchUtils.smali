.class public Lcom/vlingo/core/internal/util/WebSearchUtils;
.super Ljava/lang/Object;
.source "WebSearchUtils.java"


# static fields
.field public static final GOOGLE_NOW_ACTION:Ljava/lang/String; = "android.intent.action.WEB_SEARCH"

.field public static final GOOGLE_NOW_ACTIVITY:Ljava/lang/String; = "com.google.android.googlequicksearchbox.SearchActivity"

.field public static final GOOGLE_NOW_APP:Ljava/lang/String; = "Google Search"

.field public static final GOOGLE_NOW_PACKAGE:Ljava/lang/String; = "com.google.android.googlequicksearchbox"

.field public static final GOOGLE_NOW_QUERY:Ljava/lang/String; = "query"

.field private static final WEB_SEARCH_URL_BAIDU_DEFAULT:Ljava/lang/String;

.field private static final WEB_SEARCH_URL_BAIDU_HOME_DEFAULT:Ljava/lang/String;

.field private static final WEB_SEARCH_URL_BING_DEFAULT:Ljava/lang/String;

.field private static final WEB_SEARCH_URL_BING_HOME_DEFAULT:Ljava/lang/String;

.field private static final WEB_SEARCH_URL_DAUM_DEFAULT:Ljava/lang/String;

.field private static final WEB_SEARCH_URL_DAUM_HOME_DEFAULT:Ljava/lang/String;

.field private static final WEB_SEARCH_URL_DEFAULT:Ljava/lang/String;

.field private static final WEB_SEARCH_URL_GOOGLE_DEFAULT:Ljava/lang/String;

.field private static final WEB_SEARCH_URL_GOOGLE_HOME_DEFAULT:Ljava/lang/String;

.field private static final WEB_SEARCH_URL_HOME_DEFAULT:Ljava/lang/String;

.field private static final WEB_SEARCH_URL_NAVER_DEFAULT:Ljava/lang/String;

.field private static final WEB_SEARCH_URL_NAVER_HOME_DEFAULT:Ljava/lang/String;

.field private static final WEB_SEARCH_URL_YAHOO_DEFAULT:Ljava/lang/String;

.field private static final WEB_SEARCH_URL_YAHOO_HOME_DEFAULT:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 15
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_HOME_DEFAULT:Ljava/lang/String;

    .line 17
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_DEFAULT:Ljava/lang/String;

    .line 20
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_BING_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_BING_HOME_DEFAULT:Ljava/lang/String;

    .line 22
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_BING_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_BING_DEFAULT:Ljava/lang/String;

    .line 25
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_YAHOO_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_YAHOO_HOME_DEFAULT:Ljava/lang/String;

    .line 27
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_YAHOO_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_YAHOO_DEFAULT:Ljava/lang/String;

    .line 30
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_BAIDU_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_BAIDU_HOME_DEFAULT:Ljava/lang/String;

    .line 32
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_BAIDU_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_BAIDU_DEFAULT:Ljava/lang/String;

    .line 35
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->mcc:I

    const/16 v1, 0x136

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_GOOGLE_HOME_DEFAULT_US:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    sput-object v0, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_GOOGLE_HOME_DEFAULT:Ljava/lang/String;

    .line 40
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_GOOGLE_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_GOOGLE_DEFAULT:Ljava/lang/String;

    .line 43
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_NAVER_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_NAVER_HOME_DEFAULT:Ljava/lang/String;

    .line 45
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_NAVER_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_NAVER_DEFAULT:Ljava/lang/String;

    .line 48
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_DAUM_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_DAUM_HOME_DEFAULT:Ljava/lang/String;

    .line 50
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_DAUM_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_DAUM_DEFAULT:Ljava/lang/String;

    return-void

    .line 35
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_GOOGLE_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefaultSearchString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "query"    # Ljava/lang/String;
    .param p1, "engine"    # Ljava/lang/String;

    .prologue
    .line 69
    invoke-static {}, Lcom/vlingo/core/internal/util/WebSearchUtils;->getDefaultWebSearchEnginName()Ljava/lang/String;

    move-result-object v1

    .line 71
    .local v1, "defaultWebSearchEngineName":Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 72
    const-string/jumbo v5, "web_search_engine"

    invoke-static {v5, v1}, Lcom/vlingo/core/internal/settings/Settings;->getEnum(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 77
    :cond_0
    const-string/jumbo v5, "Yahoo"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 78
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 79
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_YAHOO_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 80
    .local v0, "defaultValue":Ljava/lang/String;
    const-string/jumbo v5, "web_search_yahoo_default"

    invoke-static {v5, v0}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 143
    .end local v0    # "defaultValue":Ljava/lang/String;
    .local v2, "url":Ljava/lang/String;
    :goto_0
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 144
    const-string/jumbo v5, "{query}"

    invoke-static {p0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 147
    :cond_1
    return-object v2

    .line 83
    .end local v2    # "url":Ljava/lang/String;
    :cond_2
    const-string/jumbo v5, "web_search_yahoo_query"

    sget-object v6, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_YAHOO_DEFAULT:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "url":Ljava/lang/String;
    goto :goto_0

    .line 86
    .end local v2    # "url":Ljava/lang/String;
    :cond_3
    const-string/jumbo v5, "Bing"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 87
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 88
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_BING_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 89
    .restart local v0    # "defaultValue":Ljava/lang/String;
    const-string/jumbo v5, "web_search_bing_default"

    invoke-static {v5, v0}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "url":Ljava/lang/String;
    goto :goto_0

    .line 92
    .end local v0    # "defaultValue":Ljava/lang/String;
    .end local v2    # "url":Ljava/lang/String;
    :cond_4
    const-string/jumbo v5, "web_search_bing_query"

    sget-object v6, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_BING_DEFAULT:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "url":Ljava/lang/String;
    goto :goto_0

    .line 95
    .end local v2    # "url":Ljava/lang/String;
    :cond_5
    const-string/jumbo v5, "Baidu"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 96
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 97
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_BAIDU_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 98
    .restart local v0    # "defaultValue":Ljava/lang/String;
    const-string/jumbo v5, "web_search_biadu_default"

    invoke-static {v5, v0}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "url":Ljava/lang/String;
    goto :goto_0

    .line 101
    .end local v0    # "defaultValue":Ljava/lang/String;
    .end local v2    # "url":Ljava/lang/String;
    :cond_6
    const-string/jumbo v5, "web_search_biadu_query"

    sget-object v6, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_BAIDU_DEFAULT:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "url":Ljava/lang/String;
    goto :goto_0

    .line 104
    .end local v2    # "url":Ljava/lang/String;
    :cond_7
    const-string/jumbo v5, "Google"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z

    move-result v5

    if-nez v5, :cond_9

    .line 106
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 107
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_GOOGLE_HOME_DEFAULT_US:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 108
    .restart local v0    # "defaultValue":Ljava/lang/String;
    const-string/jumbo v5, "web_search_google_default"

    invoke-static {v5, v0}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "url":Ljava/lang/String;
    goto/16 :goto_0

    .line 111
    .end local v0    # "defaultValue":Ljava/lang/String;
    .end local v2    # "url":Ljava/lang/String;
    :cond_8
    const-string/jumbo v5, "web_search_google_query"

    sget-object v6, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_GOOGLE_DEFAULT:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "url":Ljava/lang/String;
    goto/16 :goto_0

    .line 114
    .end local v2    # "url":Ljava/lang/String;
    :cond_9
    const-string/jumbo v5, "Naver"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z

    move-result v5

    if-nez v5, :cond_b

    .line 116
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 117
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_NAVER_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 118
    .restart local v0    # "defaultValue":Ljava/lang/String;
    const-string/jumbo v5, "web_search_naver_default"

    invoke-static {v5, v0}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "url":Ljava/lang/String;
    goto/16 :goto_0

    .line 121
    .end local v0    # "defaultValue":Ljava/lang/String;
    .end local v2    # "url":Ljava/lang/String;
    :cond_a
    const-string/jumbo v5, "web_search_naver_query"

    sget-object v6, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_NAVER_DEFAULT:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "url":Ljava/lang/String;
    goto/16 :goto_0

    .line 124
    .end local v2    # "url":Ljava/lang/String;
    :cond_b
    const-string/jumbo v5, "Daum"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_d

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z

    move-result v5

    if-nez v5, :cond_d

    .line 125
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 126
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_DAUM_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 127
    .restart local v0    # "defaultValue":Ljava/lang/String;
    const-string/jumbo v5, "web_search_daum_default"

    invoke-static {v5, v0}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "url":Ljava/lang/String;
    goto/16 :goto_0

    .line 130
    .end local v0    # "defaultValue":Ljava/lang/String;
    .end local v2    # "url":Ljava/lang/String;
    :cond_c
    const-string/jumbo v5, "web_search_daum_query"

    sget-object v6, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_DAUM_DEFAULT:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "url":Ljava/lang/String;
    goto/16 :goto_0

    .line 134
    .end local v2    # "url":Ljava/lang/String;
    :cond_d
    sget-object v4, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_HOME_DEFAULT:Ljava/lang/String;

    .line 135
    .local v4, "urlHomeDefault":Ljava/lang/String;
    sget-object v3, Lcom/vlingo/core/internal/util/WebSearchUtils;->WEB_SEARCH_URL_DEFAULT:Ljava/lang/String;

    .line 136
    .local v3, "urlDefault":Ljava/lang/String;
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 137
    const-string/jumbo v5, "web_search_home_url"

    invoke-static {v5, v4}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "url":Ljava/lang/String;
    goto/16 :goto_0

    .line 140
    .end local v2    # "url":Ljava/lang/String;
    :cond_e
    const-string/jumbo v5, "web_search_url"

    invoke-static {v5, v3}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "url":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method public static getDefaultWebSearchEnginName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 151
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_NAME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getEngineFromParseType(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "parseType"    # Ljava/lang/String;

    .prologue
    .line 186
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 187
    const-string/jumbo v0, "wsearch:"

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 188
    const-string/jumbo v0, "def"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    :goto_0
    return-object p0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static final googleNowSearch(Ljava/lang/String;)V
    .locals 5
    .param p0, "question"    # Ljava/lang/String;

    .prologue
    .line 155
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 156
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 157
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "android.intent.action.WEB_SEARCH"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 158
    new-instance v2, Landroid/content/ComponentName;

    const-string/jumbo v3, "com.google.android.googlequicksearchbox"

    const-string/jumbo v4, "com.google.android.googlequicksearchbox.SearchActivity"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 159
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 160
    const-string/jumbo v2, "query"

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 161
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 178
    return-void
.end method

.method public static isDefaultGoogleSearch()Z
    .locals 2

    .prologue
    .line 181
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Google"

    invoke-static {}, Lcom/vlingo/core/internal/util/WebSearchUtils;->getDefaultWebSearchEnginName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
