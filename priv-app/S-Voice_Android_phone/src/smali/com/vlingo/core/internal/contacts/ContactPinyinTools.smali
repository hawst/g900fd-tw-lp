.class public Lcom/vlingo/core/internal/contacts/ContactPinyinTools;
.super Ljava/lang/Object;
.source "ContactPinyinTools.java"


# static fields
.field private static final PINYIN_DATA_FILE:Ljava/lang/String; = "ChinesePinyinTableWithTones.txt"


# instance fields
.field private context:Landroid/content/Context;

.field private pinyinFullTranscriptionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Character;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Character;",
            ">;>;"
        }
    .end annotation
.end field

.field private pinyinTranscriptionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Character;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Character;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->context:Landroid/content/Context;

    .line 35
    return-void
.end method

.method private initPinyinTranscriptionMap(Z)V
    .locals 12
    .param p1, "completeMap"    # Z

    .prologue
    .line 38
    const/4 v4, 0x0

    .line 39
    .local v4, "is":Ljava/io/InputStream;
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    iput-object v10, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinTranscriptionMap:Ljava/util/HashMap;

    .line 40
    if-eqz p1, :cond_0

    .line 41
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    iput-object v10, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinFullTranscriptionMap:Ljava/util/HashMap;

    .line 44
    :cond_0
    :try_start_0
    iget-object v10, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->context:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v10

    const-string/jumbo v11, "ChinesePinyinTableWithTones.txt"

    invoke-virtual {v10, v11}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 49
    :goto_0
    if-eqz v4, :cond_6

    .line 50
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    iput-object v10, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinTranscriptionMap:Ljava/util/HashMap;

    .line 51
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/InputStreamReader;

    invoke-direct {v10, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 54
    .local v1, "br":Ljava/io/BufferedReader;
    :cond_1
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .local v6, "line":Ljava/lang/String;
    if-eqz v6, :cond_6

    .line 55
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v6, v10}, Ljava/lang/String;->charAt(I)C

    move-result v10

    invoke-static {v10}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v9

    .line 56
    .local v9, "replacement":Ljava/lang/Character;
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 57
    .local v8, "pinyinTranscriptionList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Character;>;"
    if-eqz p1, :cond_2

    .line 58
    invoke-virtual {v6}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .local v0, "arr$":[C
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v5, :cond_2

    aget-char v2, v0, v3

    .line 59
    .local v2, "c":C
    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v10

    invoke-interface {v8, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 58
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 62
    .end local v0    # "arr$":[C
    .end local v2    # "c":C
    .end local v3    # "i$":I
    .end local v5    # "len$":I
    :cond_2
    if-eqz p1, :cond_3

    .line 63
    iget-object v10, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinFullTranscriptionMap:Ljava/util/HashMap;

    invoke-virtual {v10, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 64
    iget-object v10, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinFullTranscriptionMap:Ljava/util/HashMap;

    invoke-virtual {v10, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Set;

    invoke-interface {v10, v8}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 70
    :cond_3
    :goto_2
    invoke-virtual {v6}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .restart local v0    # "arr$":[C
    array-length v5, v0

    .restart local v5    # "len$":I
    const/4 v3, 0x0

    .restart local v3    # "i$":I
    :goto_3
    if-ge v3, v5, :cond_1

    aget-char v10, v0, v3

    invoke-static {v10}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    .line 71
    .local v2, "c":Ljava/lang/Character;
    iget-object v10, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinTranscriptionMap:Ljava/util/HashMap;

    invoke-virtual {v10, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Set;

    .line 72
    .local v7, "mapCharReplaceList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Character;>;"
    if-nez v7, :cond_4

    .line 73
    new-instance v7, Ljava/util/HashSet;

    .end local v7    # "mapCharReplaceList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Character;>;"
    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 74
    .restart local v7    # "mapCharReplaceList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Character;>;"
    iget-object v10, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinTranscriptionMap:Ljava/util/HashMap;

    invoke-virtual {v10, v2, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    :cond_4
    invoke-interface {v7, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 70
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 66
    .end local v0    # "arr$":[C
    .end local v2    # "c":Ljava/lang/Character;
    .end local v3    # "i$":I
    .end local v5    # "len$":I
    .end local v7    # "mapCharReplaceList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Character;>;"
    :cond_5
    iget-object v10, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinFullTranscriptionMap:Ljava/util/HashMap;

    invoke-virtual {v10, v9, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 81
    .end local v6    # "line":Ljava/lang/String;
    .end local v8    # "pinyinTranscriptionList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Character;>;"
    .end local v9    # "replacement":Ljava/lang/Character;
    :catch_0
    move-exception v10

    .line 89
    .end local v1    # "br":Ljava/io/BufferedReader;
    :cond_6
    return-void

    .line 45
    :catch_1
    move-exception v10

    goto/16 :goto_0
.end method


# virtual methods
.method public destroyPinyinTranscriptionList()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 133
    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinTranscriptionMap:Ljava/util/HashMap;

    .line 134
    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinFullTranscriptionMap:Ljava/util/HashMap;

    .line 135
    return-void
.end method

.method public findAllPhonetic(C)Ljava/util/List;
    .locals 6
    .param p1, "symbol"    # C
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(C)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinFullTranscriptionMap:Ljava/util/HashMap;

    if-nez v4, :cond_0

    .line 113
    const/4 v4, 0x1

    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->initPinyinTranscriptionMap(Z)V

    .line 115
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 116
    .local v2, "replacementList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Character;>;"
    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinTranscriptionMap:Ljava/util/HashMap;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinTranscriptionMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 117
    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinTranscriptionMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v2, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 119
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 121
    .local v3, "samePhoneticsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Character;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    .line 122
    .local v0, "c":Ljava/lang/Character;
    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinFullTranscriptionMap:Ljava/util/HashMap;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinFullTranscriptionMap:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 123
    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinFullTranscriptionMap:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 126
    .end local v0    # "c":Ljava/lang/Character;
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 127
    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    :cond_4
    return-object v3
.end method

.method public findNormalizedCharacterList(C)Ljava/util/List;
    .locals 3
    .param p1, "symbol"    # C
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(C)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation

    .prologue
    .line 98
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinTranscriptionMap:Ljava/util/HashMap;

    if-nez v1, :cond_0

    .line 99
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->initPinyinTranscriptionMap(Z)V

    .line 101
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 102
    .local v0, "samePhoneticsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Character;>;"
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinTranscriptionMap:Ljava/util/HashMap;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinTranscriptionMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 103
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactPinyinTools;->pinyinTranscriptionMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 105
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 106
    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    :cond_2
    return-object v0
.end method
