.class Lcom/vlingo/core/internal/questions/parser/AnswerParser$4$1;
.super Ljava/lang/Thread;
.source "AnswerParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;

.field private url:Ljava/lang/String;

.field final synthetic val$image:Lcom/vlingo/core/internal/questions/DownloadableImage;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;Lcom/vlingo/core/internal/questions/DownloadableImage;)V
    .locals 1

    .prologue
    .line 304
    iput-object p1, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4$1;->this$1:Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;

    iput-object p2, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4$1;->val$image:Lcom/vlingo/core/internal/questions/DownloadableImage;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 305
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4$1;->val$image:Lcom/vlingo/core/internal/questions/DownloadableImage;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/questions/DownloadableImage;->getURL()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4$1;->url:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 309
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4$1;->this$1:Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;

    iget-object v0, v0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;->this$0:Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    # getter for: Lcom/vlingo/core/internal/questions/parser/AnswerParser;->imageQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v0}, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->access$500(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 310
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4$1;->val$image:Lcom/vlingo/core/internal/questions/DownloadableImage;

    iget-object v1, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4$1;->this$1:Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;

    iget-object v1, v1, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;->this$0:Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    iget-object v2, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4$1;->url:Ljava/lang/String;

    # invokes: Lcom/vlingo/core/internal/questions/parser/AnswerParser;->loadImage(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->access$600(Lcom/vlingo/core/internal/questions/parser/AnswerParser;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/questions/DownloadableImage;->setDrawable(Landroid/graphics/drawable/Drawable;)Lcom/vlingo/core/internal/questions/DownloadableImage;

    .line 311
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4$1;->this$1:Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;

    iget-object v0, v0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;->this$0:Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    # getter for: Lcom/vlingo/core/internal/questions/parser/AnswerParser;->imageQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v0}, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->access$500(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    .line 312
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4$1;->this$1:Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;

    iget-object v0, v0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;->this$0:Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    # getter for: Lcom/vlingo/core/internal/questions/parser/AnswerParser;->imageQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v0}, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->access$500(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v1

    monitor-enter v1

    .line 313
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4$1;->this$1:Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;

    iget-object v0, v0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;->this$0:Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    # getter for: Lcom/vlingo/core/internal/questions/parser/AnswerParser;->imageQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v0}, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->access$500(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 314
    monitor-exit v1

    .line 315
    return-void

    .line 314
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
