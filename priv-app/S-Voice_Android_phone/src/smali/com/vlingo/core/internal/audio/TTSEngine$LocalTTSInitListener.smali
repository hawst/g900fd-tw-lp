.class Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;
.super Ljava/lang/Object;
.source "TTSEngine.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/audio/TTSEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LocalTTSInitListener"
.end annotation


# instance fields
.field volatile isInitialized:Z

.field volatile isSuccess:Z

.field final synthetic this$0:Lcom/vlingo/core/internal/audio/TTSEngine;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/audio/TTSEngine;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 307
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;->this$0:Lcom/vlingo/core/internal/audio/TTSEngine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 308
    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;->isInitialized:Z

    .line 309
    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;->isSuccess:Z

    return-void
.end method


# virtual methods
.method declared-synchronized isInitialized()Z
    .locals 1

    .prologue
    .line 318
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;->isInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized isSuccess()Z
    .locals 1

    .prologue
    .line 321
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;->isSuccess:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onInit(I)V
    .locals 1
    .param p1, "status"    # I

    .prologue
    const/4 v0, 0x1

    .line 312
    if-nez p1, :cond_0

    .line 313
    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;->isSuccess:Z

    .line 315
    :cond_0
    iput-boolean v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$LocalTTSInitListener;->isInitialized:Z

    .line 316
    return-void
.end method
