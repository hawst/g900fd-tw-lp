.class public Lcom/vlingo/core/internal/lmttvocon/checksumprovider/DbUploadedChecksumProvider;
.super Ljava/lang/Object;
.source "DbUploadedChecksumProvider.java"

# interfaces
.implements Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;


# static fields
.field private static final COLUMN_CHECKSUM:Ljava/lang/String; = "checksum"

.field private static final COLUMN_ID:Ljava/lang/String; = "id"


# instance fields
.field private final context:Lcom/vlingo/core/internal/lmttvocon/UploadContext;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/lmttvocon/UploadContext;)V
    .locals 0
    .param p1, "context"    # Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/DbUploadedChecksumProvider;->context:Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    .line 28
    return-void
.end method


# virtual methods
.method protected checkIfTableExists(Landroid/database/sqlite/SQLiteDatabase;Lcom/vlingo/core/internal/lmttvocon/UploadType;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "uploadType"    # Lcom/vlingo/core/internal/lmttvocon/UploadType;

    .prologue
    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "CREATE TABLE IF NOT EXISTS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p2}, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/DbUploadedChecksumProvider;->getTableName(Lcom/vlingo/core/internal/lmttvocon/UploadType;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " INTEGER, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "checksum"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " INTEGER)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 141
    return-void
.end method

.method public clearCurrentChecksums(Lcom/vlingo/core/internal/lmttvocon/UploadType;)V
    .locals 4
    .param p1, "uploadType"    # Lcom/vlingo/core/internal/lmttvocon/UploadType;

    .prologue
    .line 37
    const/4 v0, 0x0

    .line 39
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/DbUploadedChecksumProvider;->openDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 40
    invoke-virtual {p0, v0, p1}, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/DbUploadedChecksumProvider;->checkIfTableExists(Landroid/database/sqlite/SQLiteDatabase;Lcom/vlingo/core/internal/lmttvocon/UploadType;)V

    .line 41
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/DbUploadedChecksumProvider;->getTableName(Lcom/vlingo/core/internal/lmttvocon/UploadType;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    if-eqz v0, :cond_0

    .line 44
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 47
    :cond_0
    return-void

    .line 43
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    .line 44
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_1
    throw v1
.end method

.method public getCurrentChecksums(Lcom/vlingo/core/internal/lmttvocon/UploadType;)Landroid/util/Pair;
    .locals 18
    .param p1, "uploadType"    # Lcom/vlingo/core/internal/lmttvocon/UploadType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/lmttvocon/UploadType;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 54
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 55
    .local v13, "currentChecksums":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 56
    .local v12, "currentChecksum":Ljava/lang/Integer;
    const/4 v2, 0x0

    .line 58
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/DbUploadedChecksumProvider;->openDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 59
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/DbUploadedChecksumProvider;->checkIfTableExists(Landroid/database/sqlite/SQLiteDatabase;Lcom/vlingo/core/internal/lmttvocon/UploadType;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 60
    const/4 v14, 0x0

    .line 62
    .local v14, "cursor":Landroid/database/Cursor;
    :try_start_1
    invoke-virtual/range {p0 .. p1}, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/DbUploadedChecksumProvider;->getTableName(Lcom/vlingo/core/internal/lmttvocon/UploadType;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string/jumbo v6, "checksum"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string/jumbo v9, "id"

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 63
    if-eqz v14, :cond_0

    .line 64
    const-string/jumbo v3, "id"

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 65
    .local v17, "idColumnIndex":I
    const-string/jumbo v3, "checksum"

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 66
    .local v11, "checksumColumnIndex":I
    :goto_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 67
    move/from16 v0, v17

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v15

    .line 68
    .local v15, "id":J
    invoke-interface {v14, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 69
    .local v10, "checksum":I
    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v13, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v3, v10

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v12

    .line 71
    goto :goto_0

    .line 74
    .end local v10    # "checksum":I
    .end local v11    # "checksumColumnIndex":I
    .end local v15    # "id":J
    .end local v17    # "idColumnIndex":I
    :cond_0
    if-eqz v14, :cond_1

    .line 75
    :try_start_2
    invoke-interface {v14}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 79
    :cond_1
    if-eqz v2, :cond_2

    .line 80
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 85
    :cond_2
    new-instance v3, Landroid/util/Pair;

    invoke-direct {v3, v12, v13}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v3

    .line 74
    :catchall_0
    move-exception v3

    if-eqz v14, :cond_3

    .line 75
    :try_start_3
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 79
    .end local v14    # "cursor":Landroid/database/Cursor;
    :catchall_1
    move-exception v3

    if-eqz v2, :cond_4

    .line 80
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_4
    throw v3
.end method

.method protected getTableName(Lcom/vlingo/core/internal/lmttvocon/UploadType;)Ljava/lang/String;
    .locals 1
    .param p1, "uploadType"    # Lcom/vlingo/core/internal/lmttvocon/UploadType;

    .prologue
    .line 144
    invoke-virtual {p1}, Lcom/vlingo/core/internal/lmttvocon/UploadType;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected openDb()Landroid/database/sqlite/SQLiteDatabase;
    .locals 4

    .prologue
    .line 148
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/DbUploadedChecksumProvider;->context:Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/UploadContext;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "LMTTforVOCONv1"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->openOrCreateDatabase(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public updateChecksums(Lcom/vlingo/core/internal/lmttvocon/UploadType;Ljava/util/Iterator;Ljava/util/Iterator;Ljava/util/Iterator;)V
    .locals 11
    .param p1, "uploadType"    # Lcom/vlingo/core/internal/lmttvocon/UploadType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/lmttvocon/UploadType;",
            "Ljava/util/Iterator",
            "<+",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;>;",
            "Ljava/util/Iterator",
            "<+",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;>;",
            "Ljava/util/Iterator",
            "<+",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 95
    .local p2, "removedChecksums":Ljava/util/Iterator;, "Ljava/util/Iterator<+Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Integer;>;>;"
    .local p3, "modifiedChecksums":Ljava/util/Iterator;, "Ljava/util/Iterator<+Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Integer;>;>;"
    .local p4, "addedChecksums":Ljava/util/Iterator;, "Ljava/util/Iterator<+Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Integer;>;>;"
    const/4 v2, 0x0

    .line 97
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/DbUploadedChecksumProvider;->openDb()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 99
    :try_start_1
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 100
    if-eqz p2, :cond_1

    .line 101
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 102
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    .line 103
    .local v3, "deletedChecksum":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Integer;>;"
    iget-object v4, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Long;

    .line 104
    .local v4, "deletedId":Ljava/lang/Long;
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/DbUploadedChecksumProvider;->getTableName(Lcom/vlingo/core/internal/lmttvocon/UploadType;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v2, v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 126
    .end local v3    # "deletedChecksum":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .end local v4    # "deletedId":Ljava/lang/Long;
    :catch_0
    move-exception v6

    .line 130
    :try_start_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 133
    :goto_1
    if-eqz v2, :cond_0

    .line 134
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 137
    :cond_0
    return-void

    .line 107
    :cond_1
    if-eqz p3, :cond_3

    .line 108
    :goto_2
    :try_start_3
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 109
    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/util/Pair;

    .line 110
    .local v5, "modifiedChecksum":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Integer;>;"
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 111
    .local v1, "cv":Landroid/content/ContentValues;
    const-string/jumbo v7, "id"

    iget-object v6, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v1, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 112
    const-string/jumbo v7, "checksum"

    iget-object v6, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v1, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 113
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/DbUploadedChecksumProvider;->getTableName(Lcom/vlingo/core/internal/lmttvocon/UploadType;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-object v10, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v2, v6, v1, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 130
    .end local v1    # "cv":Landroid/content/ContentValues;
    .end local v5    # "modifiedChecksum":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :catchall_0
    move-exception v6

    :try_start_4
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 133
    :catchall_1
    move-exception v6

    if-eqz v2, :cond_2

    .line 134
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_2
    throw v6

    .line 116
    :cond_3
    if-eqz p4, :cond_4

    .line 117
    :goto_3
    :try_start_5
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 118
    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 119
    .local v0, "addedChecksum":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Integer;>;"
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 120
    .restart local v1    # "cv":Landroid/content/ContentValues;
    const-string/jumbo v7, "id"

    iget-object v6, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v1, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 121
    const-string/jumbo v7, "checksum"

    iget-object v6, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v1, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 122
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/DbUploadedChecksumProvider;->getTableName(Lcom/vlingo/core/internal/lmttvocon/UploadType;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_3

    .line 125
    .end local v0    # "addedChecksum":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .end local v1    # "cv":Landroid/content/ContentValues;
    :cond_4
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 130
    :try_start_6
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_1
.end method
