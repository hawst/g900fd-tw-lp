.class final enum Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;
.super Ljava/lang/Enum;
.source "VDSMSBaseController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "CallMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

.field public static final enum VOICECALL:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

.field public static final enum VOICEVIDEOCALL:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 67
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    const-string/jumbo v1, "VOICECALL"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;->VOICECALL:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    const-string/jumbo v1, "VOICEVIDEOCALL"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;->VOICEVIDEOCALL:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    .line 66
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;->VOICECALL:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;->VOICEVIDEOCALL:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 66
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$CallMode;

    return-object v0
.end method
