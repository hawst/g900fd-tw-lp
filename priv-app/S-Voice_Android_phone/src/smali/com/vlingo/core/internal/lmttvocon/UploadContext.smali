.class public final Lcom/vlingo/core/internal/lmttvocon/UploadContext;
.super Ljava/lang/Object;
.source "UploadContext.java"


# static fields
.field public static final LMTT_FOR_VOCON_DB_NAME:Ljava/lang/String; = "LMTTforVOCONv1"

.field public static final LOG_PREFIX:Ljava/lang/String; = "VoLmSe: "


# instance fields
.field private final context:Landroid/content/Context;

.field private final mainHandler:Landroid/os/Handler;

.field private final workerHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "VoconLmttManager"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 22
    .local v0, "workerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 23
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/UploadContext;->workerHandler:Landroid/os/Handler;

    .line 24
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/UploadContext;->mainHandler:Landroid/os/Handler;

    .line 25
    iput-object p1, p0, Lcom/vlingo/core/internal/lmttvocon/UploadContext;->context:Landroid/content/Context;

    .line 26
    return-void
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/UploadContext;->context:Landroid/content/Context;

    return-object v0
.end method

.method public getMainHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/UploadContext;->mainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public getWorkerHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/UploadContext;->workerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/UploadContext;->workerHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 44
    return-void
.end method
