.class public Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;
.super Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;
.source "VoconLmttTask.java"

# interfaces
.implements Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;


# instance fields
.field private volatile cancelled:Z

.field private final contentProvider:Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentProvider;

.field protected grammarActionIterator:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;

.field private grammarUpdateControl:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateControl;

.field private final grammarUpdater:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater;

.field private final lifeCycleSync:Ljava/lang/Object;

.field protected startFromScratchActual:Z

.field private final uploadedChecksumProvider:Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/lmttvocon/UploadType;ZLcom/vlingo/core/internal/lmttvocon/contentprovider/ContentProvider;Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater;)V
    .locals 1
    .param p1, "uploadType"    # Lcom/vlingo/core/internal/lmttvocon/UploadType;
    .param p2, "startFromScratch"    # Z
    .param p3, "contentProviderParam"    # Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentProvider;
    .param p4, "uploadedChecksumProviderParam"    # Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;
    .param p5, "grammarUpdaterParam"    # Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;-><init>(Lcom/vlingo/core/internal/lmttvocon/UploadType;Z)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->grammarUpdateControl:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateControl;

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->cancelled:Z

    .line 23
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->lifeCycleSync:Ljava/lang/Object;

    .line 35
    iput-object p3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->contentProvider:Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentProvider;

    .line 36
    iput-object p5, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->grammarUpdater:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater;

    .line 37
    iput-object p4, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->uploadedChecksumProvider:Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;

    .line 38
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .prologue
    .line 82
    iget-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->lifeCycleSync:Ljava/lang/Object;

    monitor-enter v1

    .line 83
    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->cancelled:Z

    if-nez v0, :cond_0

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->cancelled:Z

    .line 85
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->grammarUpdateControl:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateControl;

    if-eqz v0, :cond_1

    .line 86
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->grammarUpdateControl:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateControl;

    invoke-interface {v0}, Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateControl;->cancel()V

    .line 91
    :cond_0
    :goto_0
    monitor-exit v1

    .line 92
    return-void

    .line 88
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->onCancelled()V

    goto :goto_0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected createGrammarActionsIterator(Ljava/util/Iterator;Landroid/util/Pair;)Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<+",
            "Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;",
            ">;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;>;)",
            "Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "contentIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;>;"
    .local p2, "uploadedChecksumInfo":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Integer;>;>;"
    new-instance v1, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;

    iget-object v0, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/util/Map;

    invoke-direct {v1, p1, v0}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;-><init>(Ljava/util/Iterator;Ljava/util/Map;)V

    return-object v1
.end method

.method public onCancelled()V
    .locals 0

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->notifyCompleted()V

    .line 134
    return-void
.end method

.method public onError()V
    .locals 0

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->notifyCompleted()V

    .line 141
    return-void
.end method

.method public onFinished(ZLjava/util/Collection;)V
    .locals 9
    .param p1, "forcedToStartFromScratch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "failedActions":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;>;"
    const/4 v7, 0x0

    .line 108
    if-nez p1, :cond_0

    iget-boolean v4, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->startFromScratchActual:Z

    if-eqz v4, :cond_2

    .line 109
    :cond_0
    iget-object v4, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->uploadedChecksumProvider:Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->getUploadType()Lcom/vlingo/core/internal/lmttvocon/UploadType;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;->clearCurrentChecksums(Lcom/vlingo/core/internal/lmttvocon/UploadType;)V

    .line 110
    iget-object v4, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->grammarActionIterator:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->getAllFromScratchGrammarActions()Ljava/util/Collection;

    move-result-object v1

    .line 111
    .local v1, "allActions":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;>;"
    if-eqz p2, :cond_1

    .line 112
    invoke-interface {v1, p2}, Ljava/util/Collection;->removeAll(Ljava/util/Collection;)Z

    .line 114
    :cond_1
    iget-object v4, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->uploadedChecksumProvider:Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->getUploadType()Lcom/vlingo/core/internal/lmttvocon/UploadType;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->grammarActionIterator:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->getAllFromScratchGrammarActions()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    invoke-interface {v4, v5, v7, v7, v6}, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;->updateChecksums(Lcom/vlingo/core/internal/lmttvocon/UploadType;Ljava/util/Iterator;Ljava/util/Iterator;Ljava/util/Iterator;)V

    .line 126
    .end local v1    # "allActions":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;>;"
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->notifyCompleted()V

    .line 127
    return-void

    .line 116
    :cond_2
    iget-object v4, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->grammarActionIterator:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->getRemovedGrammarActions()Ljava/util/Collection;

    move-result-object v3

    .line 117
    .local v3, "removedActions":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;>;"
    iget-object v4, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->grammarActionIterator:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->getAddedGrammarActions()Ljava/util/Collection;

    move-result-object v0

    .line 118
    .local v0, "addedActions":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;>;"
    iget-object v4, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->grammarActionIterator:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->getModifiedGrammarActions()Ljava/util/Collection;

    move-result-object v2

    .line 119
    .local v2, "modifiedActions":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;>;"
    if-eqz p2, :cond_3

    .line 120
    invoke-interface {v3, p2}, Ljava/util/Collection;->removeAll(Ljava/util/Collection;)Z

    .line 121
    invoke-interface {v0, p2}, Ljava/util/Collection;->removeAll(Ljava/util/Collection;)Z

    .line 122
    invoke-interface {v2, p2}, Ljava/util/Collection;->removeAll(Ljava/util/Collection;)Z

    .line 124
    :cond_3
    iget-object v4, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->uploadedChecksumProvider:Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->getUploadType()Lcom/vlingo/core/internal/lmttvocon/UploadType;

    move-result-object v5

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    invoke-interface {v4, v5, v6, v7, v8}, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;->updateChecksums(Lcom/vlingo/core/internal/lmttvocon/UploadType;Ljava/util/Iterator;Ljava/util/Iterator;Ljava/util/Iterator;)V

    goto :goto_0
.end method

.method public onStarted()V
    .locals 0

    .prologue
    .line 98
    return-void
.end method

.method public run()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 47
    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->contentProvider:Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentProvider;

    if-nez v3, :cond_5

    move-object v0, v4

    .line 48
    .local v0, "contentIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;>;"
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->isStartFromScratch()Z

    move-result v3

    if-nez v3, :cond_0

    if-nez v0, :cond_6

    :cond_0
    move v3, v6

    :goto_1
    iput-boolean v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->startFromScratchActual:Z

    .line 49
    iget-boolean v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->startFromScratchActual:Z

    if-eqz v3, :cond_7

    move v2, v5

    .line 50
    .local v2, "voconCurrentChecksum":I
    :goto_2
    iget-boolean v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->startFromScratchActual:Z

    if-eqz v3, :cond_8

    new-instance v1, Landroid/util/Pair;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 52
    .local v1, "uploadedChecksumInfo":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Integer;>;>;"
    :goto_3
    iget-boolean v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->startFromScratchActual:Z

    if-nez v3, :cond_1

    iget-object v3, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, v2, :cond_2

    .line 55
    :cond_1
    iput-boolean v6, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->startFromScratchActual:Z

    .line 56
    new-instance v1, Landroid/util/Pair;

    .end local v1    # "uploadedChecksumInfo":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Integer;>;>;"
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 58
    .restart local v1    # "uploadedChecksumInfo":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Integer;>;>;"
    :cond_2
    iget-object v4, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->lifeCycleSync:Ljava/lang/Object;

    monitor-enter v4

    .line 59
    :try_start_0
    iget-boolean v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->cancelled:Z

    if-nez v3, :cond_4

    .line 60
    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->createGrammarActionsIterator(Ljava/util/Iterator;Landroid/util/Pair;)Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->grammarActionIterator:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;

    .line 61
    iget-boolean v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->startFromScratchActual:Z

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->grammarActionIterator:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 62
    :cond_3
    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->grammarUpdater:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->getUploadType()Lcom/vlingo/core/internal/lmttvocon/UploadType;

    move-result-object v5

    iget-boolean v6, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->startFromScratchActual:Z

    iget-object v7, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->grammarActionIterator:Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;

    invoke-interface {v3, v5, p0, v6, v7}, Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater;->updateGrammar(Lcom/vlingo/core/internal/lmttvocon/UploadType;Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;ZLcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;)Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateControl;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->grammarUpdateControl:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateControl;

    .line 70
    :cond_4
    :goto_4
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    return-void

    .line 47
    .end local v0    # "contentIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;>;"
    .end local v1    # "uploadedChecksumInfo":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Integer;>;>;"
    .end local v2    # "voconCurrentChecksum":I
    :cond_5
    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->contentProvider:Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentProvider;

    invoke-interface {v3}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentProvider;->getContentIterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0

    .restart local v0    # "contentIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;>;"
    :cond_6
    move v3, v5

    .line 48
    goto :goto_1

    .line 49
    :cond_7
    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->grammarUpdater:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->getUploadType()Lcom/vlingo/core/internal/lmttvocon/UploadType;

    move-result-object v7

    invoke-interface {v3, v7}, Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater;->getCurrentChecksum(Lcom/vlingo/core/internal/lmttvocon/UploadType;)I

    move-result v2

    goto :goto_2

    .line 50
    .restart local v2    # "voconCurrentChecksum":I
    :cond_8
    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->uploadedChecksumProvider:Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->getUploadType()Lcom/vlingo/core/internal/lmttvocon/UploadType;

    move-result-object v7

    invoke-interface {v3, v7}, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;->getCurrentChecksums(Lcom/vlingo/core/internal/lmttvocon/UploadType;)Landroid/util/Pair;

    move-result-object v1

    goto :goto_3

    .line 67
    .restart local v1    # "uploadedChecksumInfo":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Integer;>;>;"
    :cond_9
    :try_start_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;->notifyCompleted()V

    goto :goto_4

    .line 70
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method
