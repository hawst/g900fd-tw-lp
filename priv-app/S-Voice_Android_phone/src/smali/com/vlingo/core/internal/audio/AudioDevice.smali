.class public Lcom/vlingo/core/internal/audio/AudioDevice;
.super Ljava/lang/Object;
.source "AudioDevice.java"


# static fields
.field public static final BT_AMR_PADDING_MILLIS:I = 0x4b0

.field private static final DEFAULT_AUDIO_DEVICE_NAME:Ljava/lang/String; = "Android"

.field private static final DEFAULT_BLUETOOTH_DEVICE_NAME:Ljava/lang/String; = "Unknown"

.field private static instance:Lcom/vlingo/core/internal/audio/AudioDevice;


# instance fields
.field private volatile currentBluetoothHeadsetName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/audio/AudioDevice;->instance:Lcom/vlingo/core/internal/audio/AudioDevice;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string/jumbo v0, "Unknown"

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/AudioDevice;->currentBluetoothHeadsetName:Ljava/lang/String;

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/vlingo/core/internal/audio/AudioDevice;
    .locals 2

    .prologue
    .line 50
    const-class v1, Lcom/vlingo/core/internal/audio/AudioDevice;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioDevice;->instance:Lcom/vlingo/core/internal/audio/AudioDevice;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Lcom/vlingo/core/internal/audio/AudioDevice;

    invoke-direct {v0}, Lcom/vlingo/core/internal/audio/AudioDevice;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/audio/AudioDevice;->instance:Lcom/vlingo/core/internal/audio/AudioDevice;

    .line 52
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioDevice;->instance:Lcom/vlingo/core/internal/audio/AudioDevice;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 50
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public getAudioDeviceName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/AudioDevice;->isAudioBluetooth()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Android/BT/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioDevice;->currentBluetoothHeadsetName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 60
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "Android"

    goto :goto_0
.end method

.method public isAudioBluetooth()Z
    .locals 3

    .prologue
    .line 67
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 68
    .local v1, "context":Landroid/content/Context;
    const-string/jumbo v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 69
    .local v0, "am":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v2

    return v2
.end method

.method public isAudioHeadset()Z
    .locals 3

    .prologue
    .line 79
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 80
    .local v1, "context":Landroid/content/Context;
    const-string/jumbo v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 81
    .local v0, "am":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v2

    return v2
.end method

.method public padAudioWithSilenceIfBT([BII)[B
    .locals 1
    .param p1, "audio"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/AudioDevice;->isAudioBluetooth()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    const/16 v0, 0x4b0

    invoke-static {p1, p2, p3, v0}, Lcom/vlingo/core/internal/audio/AMRUtil;->addPaddingToAMR([BIII)[B

    move-result-object p1

    .line 46
    .end local p1    # "audio":[B
    :cond_0
    return-object p1
.end method

.method public resetCurrentBluetoothDeviceName()V
    .locals 1

    .prologue
    .line 95
    const-string/jumbo v0, "Unknown"

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/AudioDevice;->currentBluetoothHeadsetName:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public setCurrentBluetoothDeviceName(Landroid/bluetooth/BluetoothDevice;)V
    .locals 2
    .param p1, "bd"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 86
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 87
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/AudioDevice;->resetCurrentBluetoothDeviceName()V

    .line 92
    :goto_0
    return-void

    .line 89
    :cond_1
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    .line 90
    .local v0, "name":Ljava/lang/String;
    iput-object v0, p0, Lcom/vlingo/core/internal/audio/AudioDevice;->currentBluetoothHeadsetName:Ljava/lang/String;

    goto :goto_0
.end method
