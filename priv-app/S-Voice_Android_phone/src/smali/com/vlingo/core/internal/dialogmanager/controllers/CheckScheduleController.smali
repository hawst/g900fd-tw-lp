.class public Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;
.super Lcom/vlingo/core/internal/dialogmanager/Controller;
.source "CheckScheduleController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# instance fields
.field private events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/Controller;-><init>()V

    return-void
.end method

.method private extractNonCanonicalScheduleQuery(Lcom/vlingo/sdk/recognition/VLAction;)Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;
    .locals 9
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 91
    new-instance v2, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;

    invoke-direct {v2}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;-><init>()V

    .line 93
    .local v2, "criteria":Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;
    const-string/jumbo v7, "date"

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 94
    .local v0, "beginDate":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 95
    const/4 v7, 0x1

    invoke-static {v0, v7}, Lcom/vlingo/core/internal/schedule/DateUtil;->getMillisFromString(Ljava/lang/String;Z)J

    move-result-wide v5

    .line 96
    .local v5, "startMillis":J
    invoke-static {v5, v6}, Lcom/vlingo/core/internal/schedule/DateUtil;->endOfGivenDay(J)J

    move-result-wide v3

    .line 97
    .local v3, "endMillis":J
    invoke-virtual {v2, v5, v6}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->setBegin(J)V

    .line 98
    invoke-virtual {v2, v3, v4}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->setEnd(J)V

    .line 100
    .end local v3    # "endMillis":J
    .end local v5    # "startMillis":J
    :cond_0
    invoke-static {p1}, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->getCount(Lcom/vlingo/sdk/recognition/VLAction;)I

    move-result v1

    .line 101
    .local v1, "count":I
    invoke-virtual {v2, v1}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->setCount(I)V

    .line 102
    return-object v2
.end method


# virtual methods
.method public clone()Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 38
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/Controller;->clone()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;

    .line 39
    .local v0, "clone":Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;->events:Ljava/util/List;

    invoke-static {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->clone(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;->events:Ljava/util/List;

    .line 40
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;->clone()Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;->clone()Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;

    move-result-object v0

    return-object v0
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 12
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 61
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/Controller;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 63
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v6

    const-string/jumbo v7, "schedule"

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 65
    iput-object v10, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;->events:Ljava/util/List;

    .line 66
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;->extractNonCanonicalScheduleQuery(Lcom/vlingo/sdk/recognition/VLAction;)Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;

    move-result-object v0

    .line 69
    .local v0, "criteria":Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v0}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getScheduledEvents(Landroid/content/Context;Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;->events:Ljava/util/List;

    .line 71
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;->events:Ljava/util/List;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;->events:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 72
    :cond_0
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SCHEDULE_NO_EVENTS:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v7, v9, [Ljava/lang/Object;

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 73
    .local v3, "noMoreEvents":Ljava/lang/String;
    invoke-virtual {p0, v3, v3, v9, v10}, Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 86
    .end local v3    # "noMoreEvents":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;->reset()V

    .line 87
    return v9

    .line 75
    :cond_1
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventYouHaveToday:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v7, v11, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;->events:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 76
    .local v4, "systemTurn":Ljava/lang/String;
    invoke-virtual {p0, v4, v4, v9, v10}, Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 77
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;->events:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 79
    .local v1, "event":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventTitleTimeDetailBrief:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBeginTime()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 82
    .local v5, "tts":Ljava/lang/String;
    invoke-interface {p2, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    goto :goto_1

    .line 84
    .end local v1    # "event":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .end local v5    # "tts":Ljava/lang/String;
    :cond_2
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;->events:Ljava/util/List;

    invoke-static {p2, p1, v6, p0}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->displayScheduleEventList(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/sdk/recognition/VLAction;Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto :goto_0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 107
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 44
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 45
    .local v1, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "CheckScheduleController{"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    const-string/jumbo v3, "List<ScheduleEvent>{"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/CheckScheduleController;->events:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 48
    .local v2, "se":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 50
    .end local v2    # "se":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    :cond_0
    const-string/jumbo v3, "}"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    const-string/jumbo v3, "}"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
