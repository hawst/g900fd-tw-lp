.class public Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;
.super Lcom/vlingo/core/internal/debug/PreferenceBuilder;
.source "EditTextPreferenceBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/core/internal/debug/PreferenceBuilder",
        "<",
        "Landroid/preference/EditTextPreference;",
        ">;"
    }
.end annotation


# static fields
.field private static showAsSummaryAction:Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder$1;

    invoke-direct {v0}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder$1;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->showAsSummaryAction:Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;-><init>()V

    .line 10
    sget-object v0, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->showAsSummaryAction:Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->setShowAsSummary(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)V

    .line 11
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "setting"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;-><init>(Ljava/lang/String;)V

    .line 15
    sget-object v0, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->showAsSummaryAction:Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->setShowAsSummary(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)V

    .line 16
    return-void
.end method


# virtual methods
.method public register(Landroid/preference/PreferenceActivity;)Landroid/preference/EditTextPreference;
    .locals 3
    .param p1, "activity"    # Landroid/preference/PreferenceActivity;

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->attach(Landroid/preference/PreferenceActivity;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/EditTextPreference;

    .line 21
    .local v1, "pref":Landroid/preference/EditTextPreference;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->hasValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 22
    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 23
    :cond_0
    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->getDefault()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v0, v2

    .line 24
    .local v0, "initialValue":Ljava/lang/String;
    :goto_0
    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 25
    invoke-virtual {p0}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->isShowAsSummary()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 26
    invoke-virtual {v1, v0}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 27
    :cond_1
    return-object v1

    .line 23
    .end local v0    # "initialValue":Ljava/lang/String;
    :cond_2
    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic register(Landroid/preference/PreferenceActivity;)Landroid/preference/Preference;
    .locals 1
    .param p1, "x0"    # Landroid/preference/PreferenceActivity;

    .prologue
    .line 7
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->register(Landroid/preference/PreferenceActivity;)Landroid/preference/EditTextPreference;

    move-result-object v0

    return-object v0
.end method
