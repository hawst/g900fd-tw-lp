.class public Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;
.super Ljava/lang/Object;
.source "VDSMSBaseController.java"

# interfaces
.implements Lcom/vlingo/core/internal/contacts/ContactMatchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "LocalContactMatchListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;


# direct methods
.method protected constructor <init>(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)V
    .locals 0

    .prologue
    .line 675
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAutoAction(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 3
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 679
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iput-object p1, v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 680
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iget-object v1, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    iput-object v1, v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelectedName:Ljava/lang/String;

    .line 681
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->checkMessageType(Lcom/vlingo/core/internal/contacts/ContactMatch;Z)Z

    move-result v1

    # setter for: Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->executed:Z
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->access$002(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;Z)Z

    .line 682
    return-void
.end method

.method public onContactMatchResultsUpdated(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 685
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    return-void
.end method

.method public onContactMatchingFailed()V
    .locals 1

    .prologue
    .line 748
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->access$1000(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 749
    return-void
.end method

.method public onContactMatchingFinished(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 689
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->access$100(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 690
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->executed:Z
    invoke-static {v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->access$000(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 691
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->access$200(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 741
    :goto_0
    return-void

    .line 695
    :cond_0
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iget-object v5, v5, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->ordinal:Ljava/lang/String;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->access$300(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iget-object v6, v6, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->ordinal:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElement(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 696
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->access$400(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iget-object v6, v6, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->ordinal:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElement(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 697
    .local v2, "item":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iput-object v2, v5, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 698
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iget-object v6, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    iput-object v6, v5, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelectedName:Ljava/lang/String;

    .line 699
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    invoke-virtual {v6, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->checkMessageType(Lcom/vlingo/core/internal/contacts/ContactMatch;)Z

    move-result v6

    # setter for: Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->executed:Z
    invoke-static {v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->access$002(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;Z)Z

    .line 740
    .end local v2    # "item":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :goto_1
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->access$900(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    goto :goto_0

    .line 701
    :cond_1
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->access$500(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/core/internal/util/OrdinalUtil;->clearOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 702
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-ne v5, v8, :cond_3

    .line 709
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iget-object v5, v5, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactMatcher:Lcom/vlingo/core/internal/contacts/ContactMatcher;

    invoke-virtual {v5, p1}, Lcom/vlingo/core/internal/contacts/ContactMatcher;->determinePerformAction(Ljava/util/List;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 710
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    invoke-virtual {v5, p1}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->showCallContact(Ljava/util/List;)V

    goto :goto_1

    .line 713
    :cond_2
    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 714
    .restart local v2    # "item":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iput-object v2, v5, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelected:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 715
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iget-object v6, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    iput-object v6, v5, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelectedName:Ljava/lang/String;

    .line 716
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    invoke-virtual {v6, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->checkMessageType(Lcom/vlingo/core/internal/contacts/ContactMatch;)Z

    move-result v6

    # setter for: Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->executed:Z
    invoke-static {v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->access$002(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;Z)Z

    goto :goto_1

    .line 718
    .end local v2    # "item":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_3
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iget-object v5, v5, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactMatcher:Lcom/vlingo/core/internal/contacts/ContactMatcher;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/contacts/ContactMatcher;->getNumContacts()I

    move-result v5

    if-le v5, v8, :cond_4

    .line 721
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iget-object v6, v6, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactMatcher:Lcom/vlingo/core/internal/contacts/ContactMatcher;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/contacts/ContactMatcher;->getSortedContacts()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->promptForRefineContact(Ljava/util/List;)V

    .line 722
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->access$600(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iget-object v6, v6, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactMatcher:Lcom/vlingo/core/internal/contacts/ContactMatcher;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/contacts/ContactMatcher;->getSortedContacts()Ljava/util/List;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/util/List;)V

    goto :goto_1

    .line 726
    :cond_4
    :try_start_0
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iget-object v5, v5, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->query:Ljava/lang/String;

    const-string/jumbo v6, "\\D"

    const-string/jumbo v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 727
    .local v4, "numericValue":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    .line 728
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iget-object v6, v6, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->query:Ljava/lang/String;

    iput-object v6, v5, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->address:Ljava/lang/String;

    .line 729
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->handleNeedMessage()V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 730
    .end local v4    # "numericValue":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 733
    .local v0, "e":Ljava/lang/NumberFormatException;
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iget-object v5, v5, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelectedName:Ljava/lang/String;

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iget-object v1, v5, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->query:Ljava/lang/String;

    .line 734
    .local v1, "filter":Ljava/lang/String;
    :goto_2
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contacts_no_match_openquote:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v1, v6, v9

    # invokes: Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->access$700(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 735
    .local v3, "noMatchString":Ljava/lang/String;
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->access$800(Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5, v3, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->handleNeedContact()V

    goto/16 :goto_1

    .line 733
    .end local v1    # "filter":Ljava/lang/String;
    .end local v3    # "noMatchString":Ljava/lang/String;
    :cond_5
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;

    iget-object v1, v5, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;->contactSelectedName:Ljava/lang/String;

    goto :goto_2
.end method
