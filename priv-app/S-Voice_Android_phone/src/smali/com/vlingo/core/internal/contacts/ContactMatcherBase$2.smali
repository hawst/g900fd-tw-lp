.class Lcom/vlingo/core/internal/contacts/ContactMatcherBase$2;
.super Ljava/lang/Object;
.source "ContactMatcherBase.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->onAsyncSortingFinished(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/contacts/ContactMatcherBase;

.field final synthetic val$sortedContacts:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/contacts/ContactMatcherBase;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase$2;->this$0:Lcom/vlingo/core/internal/contacts/ContactMatcherBase;

    iput-object p2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase$2;->val$sortedContacts:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 267
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase$2;->this$0:Lcom/vlingo/core/internal/contacts/ContactMatcherBase;

    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase$2;->val$sortedContacts:Ljava/util/List;

    # invokes: Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->attemptAutoAction(Ljava/util/List;)V
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->access$000(Lcom/vlingo/core/internal/contacts/ContactMatcherBase;Ljava/util/List;)V

    .line 268
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase$2;->this$0:Lcom/vlingo/core/internal/contacts/ContactMatcherBase;

    iget-object v0, v0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mListener:Lcom/vlingo/core/internal/contacts/ContactMatchListener;

    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase$2;->this$0:Lcom/vlingo/core/internal/contacts/ContactMatcherBase;

    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase$2;->val$sortedContacts:Ljava/util/List;

    # invokes: Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->getSortedSublist(Ljava/util/List;)Ljava/util/List;
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->access$100(Lcom/vlingo/core/internal/contacts/ContactMatcherBase;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/contacts/ContactMatchListener;->onContactMatchingFinished(Ljava/util/List;)V

    .line 269
    return-void
.end method
