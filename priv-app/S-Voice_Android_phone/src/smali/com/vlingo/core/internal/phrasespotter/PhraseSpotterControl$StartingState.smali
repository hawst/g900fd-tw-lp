.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StartingState;
.super Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
.source "PhraseSpotterControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "StartingState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StartingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V

    return-void
.end method


# virtual methods
.method public onEnter()V
    .locals 5

    .prologue
    .line 295
    invoke-super {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->onEnter()V

    .line 296
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "  starting RecordThread"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StartingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    new-instance v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;

    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StartingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "SpotterRecordThread#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StartingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mInstanceSerialNumber:I
    invoke-static {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$800(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StartingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # ++operator for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mSpotterThreadCount:I
    invoke-static {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$904(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Ljava/lang/String;)V

    # setter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$702(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;

    .line 298
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StartingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;
    invoke-static {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$700(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->start()V

    .line 301
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StartingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mListener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;->onPhraseSpotterStarted()V

    .line 302
    return-void
.end method

.method public start(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V
    .locals 0
    .param p1, "phraseSpotterParams"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .param p2, "listener"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    .prologue
    .line 284
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->start(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V

    .line 285
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 289
    invoke-super {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->stop()V

    .line 290
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StartingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StartingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStoppingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    invoke-static {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v1

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V

    .line 291
    return-void
.end method
