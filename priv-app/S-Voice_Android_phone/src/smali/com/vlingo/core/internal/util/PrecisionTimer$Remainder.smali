.class Lcom/vlingo/core/internal/util/PrecisionTimer$Remainder;
.super Ljava/lang/Object;
.source "PrecisionTimer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/PrecisionTimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Remainder"
.end annotation


# instance fields
.field private value:J


# direct methods
.method public constructor <init>(J)V
    .locals 0
    .param p1, "value"    # J

    .prologue
    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    iput-wide p1, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Remainder;->value:J

    .line 162
    return-void
.end method


# virtual methods
.method public dividedBy(J)J
    .locals 4
    .param p1, "divisor"    # J

    .prologue
    .line 169
    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Remainder;->value:J

    div-long v0, v2, p1

    .line 170
    .local v0, "dividend":J
    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Remainder;->value:J

    rem-long/2addr v2, p1

    iput-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Remainder;->value:J

    .line 171
    return-wide v0
.end method

.method public value()J
    .locals 2

    .prologue
    .line 165
    iget-wide v0, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Remainder;->value:J

    return-wide v0
.end method
