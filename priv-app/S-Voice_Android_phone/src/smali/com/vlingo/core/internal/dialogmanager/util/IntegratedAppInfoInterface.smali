.class public interface abstract Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;
.super Ljava/lang/Object;
.source "IntegratedAppInfoInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;
    }
.end annotation


# virtual methods
.method public abstract getAppType()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;
.end method

.method public abstract getContentProviderUri()Landroid/net/Uri;
.end method

.method public abstract getExecName()Ljava/lang/String;
.end method

.method public abstract getExecPackage()Ljava/lang/String;
.end method

.method public abstract getIntentNameCreate()Ljava/lang/String;
.end method

.method public abstract getIntentNameDelete()Ljava/lang/String;
.end method

.method public abstract getIntentNameStart()Ljava/lang/String;
.end method

.method public abstract getUpdateContentProviderUri()Landroid/net/Uri;
.end method

.method public abstract isBroadcast()Z
.end method

.method public abstract isSNote()Z
.end method

.method public abstract setAppType(Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;)V
.end method

.method public abstract setBroadcast(Z)V
.end method

.method public abstract setContentProviderUri(Landroid/net/Uri;)V
.end method

.method public abstract setExecName(Ljava/lang/String;)V
.end method

.method public abstract setExecPackage(Ljava/lang/String;)V
.end method

.method public abstract setIntentNameCreate(Ljava/lang/String;)V
.end method
