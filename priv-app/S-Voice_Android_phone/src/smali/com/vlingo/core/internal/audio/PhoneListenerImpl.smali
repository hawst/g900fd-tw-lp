.class public Lcom/vlingo/core/internal/audio/PhoneListenerImpl;
.super Landroid/telephony/PhoneStateListener;
.source "PhoneListenerImpl.java"


# static fields
.field private static final LOG:Lcom/vlingo/core/internal/logging/Logger;


# instance fields
.field private final listeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/vlingo/core/internal/audio/PhoneStateListenerCallback;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;->LOG:Lcom/vlingo/core/internal/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    .line 21
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-void
.end method


# virtual methods
.method public addListener(Lcom/vlingo/core/internal/audio/PhoneStateListenerCallback;)V
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/core/internal/audio/PhoneStateListenerCallback;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 53
    :cond_0
    return-void
.end method

.method public clearListeners()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 73
    :cond_0
    return-void
.end method

.method public notifyCallStateChanged(ILjava/lang/String;)V
    .locals 3
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    .line 62
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/audio/PhoneStateListenerCallback;

    .line 63
    .local v1, "listener":Lcom/vlingo/core/internal/audio/PhoneStateListenerCallback;
    if-eqz v1, :cond_0

    .line 64
    invoke-interface {v1, p1, p2}, Lcom/vlingo/core/internal/audio/PhoneStateListenerCallback;->onCallStateChanged(ILjava/lang/String;)V

    goto :goto_0

    .line 67
    .end local v1    # "listener":Lcom/vlingo/core/internal/audio/PhoneStateListenerCallback;
    :cond_1
    return-void
.end method

.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 3
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    .line 26
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    .line 28
    .local v1, "dialogFlow":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;->notifyCallStateChanged(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    packed-switch p1, :pswitch_data_0

    .line 45
    .end local v1    # "dialogFlow":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    :goto_0
    return-void

    .line 29
    :catch_0
    move-exception v2

    .line 32
    .local v2, "ise":Ljava/lang/IllegalStateException;
    goto :goto_0

    .line 37
    .end local v2    # "ise":Ljava/lang/IllegalStateException;
    .restart local v1    # "dialogFlow":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    :pswitch_0
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v0

    .line 38
    .local v0, "bluetoothOn":Z
    if-eqz v0, :cond_0

    .line 39
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopScoOnIdle()V

    .line 42
    :cond_0
    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    goto :goto_0

    .line 34
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public removeListener(Lcom/vlingo/core/internal/audio/PhoneStateListenerCallback;)V
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/core/internal/audio/PhoneStateListenerCallback;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 59
    :cond_0
    return-void
.end method
