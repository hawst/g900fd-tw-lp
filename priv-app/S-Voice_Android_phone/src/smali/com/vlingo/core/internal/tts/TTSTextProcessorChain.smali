.class public final Lcom/vlingo/core/internal/tts/TTSTextProcessorChain;
.super Ljava/lang/Object;
.source "TTSTextProcessorChain.java"


# static fields
.field private static final CHAIN:[Lcom/vlingo/core/internal/tts/TTSTextProcessor;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 7
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/core/internal/tts/TTSTextProcessor;

    const/4 v1, 0x0

    new-instance v2, Lcom/vlingo/core/internal/tts/ContactTextProcessor;

    invoke-direct {v2}, Lcom/vlingo/core/internal/tts/ContactTextProcessor;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/vlingo/core/internal/tts/XMLEscapeProcessor;

    invoke-direct {v2}, Lcom/vlingo/core/internal/tts/XMLEscapeProcessor;-><init>()V

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/tts/TTSTextProcessorChain;->CHAIN:[Lcom/vlingo/core/internal/tts/TTSTextProcessor;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static process(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 15
    sget-object v0, Lcom/vlingo/core/internal/tts/TTSTextProcessorChain;->CHAIN:[Lcom/vlingo/core/internal/tts/TTSTextProcessor;

    .local v0, "arr$":[Lcom/vlingo/core/internal/tts/TTSTextProcessor;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 16
    .local v3, "textProcessor":Lcom/vlingo/core/internal/tts/TTSTextProcessor;
    invoke-interface {v3, p0, p1, p2}, Lcom/vlingo/core/internal/tts/TTSTextProcessor;->process(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/lang/String;

    move-result-object p1

    .line 15
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 18
    .end local v3    # "textProcessor":Lcom/vlingo/core/internal/tts/TTSTextProcessor;
    :cond_0
    return-object p1
.end method
