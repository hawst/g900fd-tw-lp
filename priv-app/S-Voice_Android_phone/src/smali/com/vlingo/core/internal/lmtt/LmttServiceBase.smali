.class public abstract Lcom/vlingo/core/internal/lmtt/LmttServiceBase;
.super Ljava/lang/Object;
.source "LmttServiceBase.java"


# static fields
.field public static final ACTION_REINITIALIZE:Ljava/lang/String; = "com.vlingo.lmtt.action.REINITIALIZE"

.field public static final ACTION_SEND_LANGUAGE_CHANGE:Ljava/lang/String; = "com.vlingo.lmtt.action.LANGUAGE_CHANGE"

.field public static final ACTION_SEND_UPDATE:Ljava/lang/String; = "com.vlingo.lmtt.action.SEND_UPDATE"

.field private static final DEFAULT_INACTIVITY_SHUTDOWN_PERIOD_MINS:I = 0x2760

.field public static final EXTRA_CLEAR_LMTT:Ljava/lang/String; = "com.vlingo.lmtt.extra.CLEAR_LMTT"

.field public static final EXTRA_LMTT_TYPE:Ljava/lang/String; = "com.vlingo.lmtt.extra.LMTT_TYPE"

.field public static final EXTRA_NO_RESET:Ljava/lang/String; = "com.vlingo.lmtt.extra.NO_RESET"

.field public static final EXTRA_SKIP_INITIAL_DELAY:Ljava/lang/String; = "com.vlingo.lmtt.extra.SKIP_DELAY"

.field private static final MS_PER_MINUTE:J = 0xea60L

.field public static final USE_FG_SERVICE:Z = true

.field private static smInstance:Lcom/vlingo/core/internal/lmtt/LmttServiceBase;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static destroyInstance()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;->smInstance:Lcom/vlingo/core/internal/lmtt/LmttServiceBase;

    .line 51
    return-void
.end method

.method public static getInstance()Lcom/vlingo/core/internal/lmtt/LmttServiceBase;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;->smInstance:Lcom/vlingo/core/internal/lmtt/LmttServiceBase;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/vlingo/core/internal/lmtt/FgLmttService;

    invoke-direct {v0}, Lcom/vlingo/core/internal/lmtt/FgLmttService;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;->smInstance:Lcom/vlingo/core/internal/lmtt/LmttServiceBase;

    .line 46
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;->smInstance:Lcom/vlingo/core/internal/lmtt/LmttServiceBase;

    return-object v0
.end method

.method private static isInactive()Z
    .locals 11

    .prologue
    .line 131
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 132
    .local v0, "currentTime":J
    const-string/jumbo v7, "lmtt.no_activity_shutdown_period_mins"

    const/16 v8, 0x2760

    invoke-static {v7, v8}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 133
    .local v4, "maxInactivityPeriodMins":I
    int-to-long v7, v4

    const-wide/32 v9, 0xea60

    mul-long v5, v7, v9

    .line 134
    .local v5, "maxInactivityPeriodMs":J
    const-string/jumbo v7, "lmtt.last_app_start_time"

    const-wide/16 v8, 0x0

    invoke-static {v7, v8, v9}, Lcom/vlingo/core/internal/settings/Settings;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 135
    .local v2, "lastActivityTime":J
    add-long v7, v2, v5

    cmp-long v7, v0, v7

    if-lez v7, :cond_0

    const/4 v7, 0x1

    :goto_0
    return v7

    :cond_0
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public static sendHeartBeat(Z)V
    .locals 3
    .param p0, "isAppLaunch"    # Z

    .prologue
    .line 121
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.vlingo.lmtt.action.CONNECTED_APP_HEART_BIT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 122
    .local v0, "intent":Landroid/content/Intent;
    if-eqz p0, :cond_0

    .line 123
    const-string/jumbo v1, "com.vlingo.lmtt.extra.APP_LAUNCHED"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 125
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 126
    return-void
.end method

.method public static startLmttService(Ljava/lang/String;ZLandroid/os/Bundle;)V
    .locals 5
    .param p0, "action"    # Ljava/lang/String;
    .param p1, "resetInactivity"    # Z
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 60
    const-string/jumbo v2, "tos_accepted"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v2

    if-nez v2, :cond_0

    .line 70
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 71
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/vlingo/core/facade/lmtt/LmttService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 72
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, p0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    if-eqz p2, :cond_2

    .line 74
    invoke-virtual {v1, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 78
    :cond_2
    if-eqz p1, :cond_5

    .line 79
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;->isInactive()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 82
    const-string/jumbo v2, "com.vlingo.lmtt.action.SEND_UPDATE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 83
    const-string/jumbo v2, "com.vlingo.lmtt.extra.SKIP_DELAY"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 85
    :cond_3
    const-string/jumbo v2, "lmtt.last_app_start_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Ljava/lang/String;J)V

    .line 91
    :cond_4
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 107
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 86
    :cond_5
    const-string/jumbo v2, "com.vlingo.lmtt.action.SEND_UPDATE"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {}, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;->isInactive()Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_0
.end method

.method public static stopLmttService()V
    .locals 3

    .prologue
    .line 113
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 114
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/vlingo/core/facade/lmtt/LmttService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 115
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 116
    return-void
.end method


# virtual methods
.method public abstract onCreate(Landroid/app/Service;)V
.end method

.method public abstract onDestroy()V
.end method

.method public abstract onStartCommand(Landroid/content/Intent;II)I
.end method
