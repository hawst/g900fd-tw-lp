.class Lcom/vlingo/core/internal/audio/AudioPlayer$LanguageChangeListener;
.super Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;
.source "AudioPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/audio/AudioPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LanguageChangeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/audio/AudioPlayer;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$LanguageChangeListener;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    invoke-direct {p0}, Lcom/vlingo/core/internal/settings/LanguageChangeReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onLanguageChanged(Ljava/lang/String;)V
    .locals 1
    .param p1, "newLanguage"    # Ljava/lang/String;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$LanguageChangeListener;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->ttsEngine:Lcom/vlingo/core/internal/audio/TTSEngine;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$LanguageChangeListener;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->ttsEngine:Lcom/vlingo/core/internal/audio/TTSEngine;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/TTSEngine;->onApplicationLanguageChanged()V

    .line 114
    :cond_0
    return-void
.end method
