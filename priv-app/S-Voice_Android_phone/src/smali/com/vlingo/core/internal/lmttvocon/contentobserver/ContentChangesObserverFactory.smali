.class public final Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserverFactory;
.super Ljava/lang/Object;
.source "ContentChangesObserverFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserverFactory$1;
    }
.end annotation


# static fields
.field private static overriddenObservers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/internal/lmttvocon/UploadType;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 15
    new-instance v0, Ljava/util/HashMap;

    invoke-static {}, Lcom/vlingo/core/internal/lmttvocon/UploadType;->values()[Lcom/vlingo/core/internal/lmttvocon/UploadType;

    move-result-object v1

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserverFactory;->overriddenObservers:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method

.method public static create(Lcom/vlingo/core/internal/lmttvocon/UploadContext;Lcom/vlingo/core/internal/lmttvocon/UploadType;)Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;
    .locals 7
    .param p0, "uploadContext"    # Lcom/vlingo/core/internal/lmttvocon/UploadContext;
    .param p1, "uploadType"    # Lcom/vlingo/core/internal/lmttvocon/UploadType;

    .prologue
    const/4 v4, 0x0

    .line 22
    sget-object v3, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserverFactory;->overriddenObservers:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 23
    sget-object v3, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserverFactory;->overriddenObservers:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    .line 24
    .local v2, "observerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;>;"
    if-eqz v2, :cond_0

    .line 26
    const/4 v3, 0x1

    :try_start_0
    new-array v3, v3, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    aput-object v6, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 27
    .local v0, "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<+Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;>;"
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v3, v5

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    .end local v0    # "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<+Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;>;"
    .end local v2    # "observerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;>;"
    :goto_0
    return-object v3

    .line 28
    .restart local v2    # "observerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;>;"
    :catch_0
    move-exception v1

    .local v1, "e":Ljava/lang/Exception;
    move-object v3, v4

    .line 31
    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    move-object v3, v4

    .line 36
    goto :goto_0

    .line 39
    .end local v2    # "observerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;>;"
    :cond_1
    sget-object v3, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserverFactory$1;->$SwitchMap$com$vlingo$core$internal$lmttvocon$UploadType:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/lmttvocon/UploadType;->ordinal()I

    move-result v5

    aget v3, v3, v5

    packed-switch v3, :pswitch_data_0

    move-object v3, v4

    .line 67
    goto :goto_0

    .line 43
    :pswitch_0
    new-instance v3, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContactContentChangesObserver;

    invoke-direct {v3, p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContactContentChangesObserver;-><init>(Lcom/vlingo/core/internal/lmttvocon/UploadContext;)V

    goto :goto_0

    .line 47
    :pswitch_1
    new-instance v3, Lcom/vlingo/core/internal/lmttvocon/contentobserver/SongContentChangesObserver;

    invoke-direct {v3, p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/SongContentChangesObserver;-><init>(Lcom/vlingo/core/internal/lmttvocon/UploadContext;)V

    goto :goto_0

    .line 51
    :pswitch_2
    new-instance v3, Lcom/vlingo/core/internal/lmttvocon/contentobserver/AlbumContentChangesObserver;

    invoke-direct {v3, p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/AlbumContentChangesObserver;-><init>(Lcom/vlingo/core/internal/lmttvocon/UploadContext;)V

    goto :goto_0

    .line 55
    :pswitch_3
    new-instance v3, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ArtistContentChangesObserver;

    invoke-direct {v3, p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ArtistContentChangesObserver;-><init>(Lcom/vlingo/core/internal/lmttvocon/UploadContext;)V

    goto :goto_0

    .line 59
    :pswitch_4
    new-instance v3, Lcom/vlingo/core/internal/lmttvocon/contentobserver/PlaylistContentChangesObserver;

    invoke-direct {v3, p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/PlaylistContentChangesObserver;-><init>(Lcom/vlingo/core/internal/lmttvocon/UploadContext;)V

    goto :goto_0

    .line 63
    :pswitch_5
    new-instance v3, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ApplicationContentChangesObserver;

    invoke-direct {v3, p0}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ApplicationContentChangesObserver;-><init>(Lcom/vlingo/core/internal/lmttvocon/UploadContext;)V

    goto :goto_0

    .line 39
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static override(Lcom/vlingo/core/internal/lmttvocon/UploadType;Ljava/lang/Class;)V
    .locals 1
    .param p0, "type"    # Lcom/vlingo/core/internal/lmttvocon/UploadType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/lmttvocon/UploadType;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 72
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;>;"
    sget-object v0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserverFactory;->overriddenObservers:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    return-void
.end method

.method public static removeOverride(Lcom/vlingo/core/internal/lmttvocon/UploadType;)V
    .locals 1
    .param p0, "type"    # Lcom/vlingo/core/internal/lmttvocon/UploadType;

    .prologue
    .line 76
    sget-object v0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserverFactory;->overriddenObservers:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    return-void
.end method
