.class public final Lcom/vlingo/core/internal/settings/LanguageDefaulter;
.super Ljava/lang/Object;
.source "LanguageDefaulter.java"


# static fields
.field private static final COMMONWEATH_COUNTRIES:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "AU"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "ZA"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "NZ"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "IE"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "IN"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/settings/LanguageDefaulter;->COMMONWEATH_COUNTRIES:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkForCommonwealthCountry(Ljava/lang/String;)Z
    .locals 5
    .param p0, "country"    # Ljava/lang/String;

    .prologue
    .line 135
    sget-object v0, Lcom/vlingo/core/internal/settings/LanguageDefaulter;->COMMONWEATH_COUNTRIES:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 136
    .local v1, "c":Ljava/lang/String;
    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 137
    const/4 v4, 0x1

    .line 139
    .end local v1    # "c":Ljava/lang/String;
    :goto_1
    return v4

    .line 135
    .restart local v1    # "c":Ljava/lang/String;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 139
    .end local v1    # "c":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private static chooseBestLanguage(Ljava/util/Set;Ljava/util/Locale;)Ljava/lang/String;
    .locals 4
    .param p1, "locale"    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Locale;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 117
    .local p0, "localeMatches":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "es"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 120
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 121
    .local v1, "match":Ljava/lang/String;
    const-string/jumbo v2, "v-es-LA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "v-es-NA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 131
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "match":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object v1

    .line 126
    :cond_2
    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/core/internal/settings/LanguageDefaulter;->checkForCommonwealthCountry(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 127
    const-string/jumbo v1, "en-GB"

    goto :goto_0

    .line 131
    :cond_3
    invoke-interface {p0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v2, v2, v3

    check-cast v2, Ljava/lang/String;

    move-object v1, v2

    goto :goto_0
.end method

.method private static getCountrySubString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "fullAvailableLocale"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x5

    .line 109
    invoke-static {p0}, Lcom/vlingo/core/internal/settings/LanguageDefaulter;->isVlingoSpecialLanguage(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    const/4 v0, 0x7

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 112
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getDefaultLanguage(Landroid/content/Context;)Ljava/lang/String;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    const/4 v4, 0x0

    .line 36
    .local v4, "index":I
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSupportedLanguages()[Ljava/lang/CharSequence;

    move-result-object v9

    .line 37
    .local v9, "supportedLanguages":[Ljava/lang/CharSequence;
    array-length v10, v9

    new-array v1, v10, [Ljava/lang/String;

    .line 38
    .local v1, "availableLanguages":[Ljava/lang/String;
    move-object v0, v9

    .local v0, "arr$":[Ljava/lang/CharSequence;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    move v5, v4

    .end local v4    # "index":I
    .local v5, "index":I
    :goto_0
    if-ge v3, v7, :cond_0

    aget-object v6, v0, v3

    .line 39
    .local v6, "lang":Ljava/lang/CharSequence;
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "index":I
    .restart local v4    # "index":I
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v5

    .line 38
    add-int/lit8 v3, v3, 0x1

    move v5, v4

    .end local v4    # "index":I
    .restart local v5    # "index":I
    goto :goto_0

    .line 42
    .end local v6    # "lang":Ljava/lang/CharSequence;
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 43
    .local v2, "c":Landroid/content/res/Configuration;
    if-eqz v2, :cond_1

    .line 44
    iget-object v8, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 45
    .local v8, "locale":Ljava/util/Locale;
    invoke-static {v8, v1}, Lcom/vlingo/core/internal/settings/LanguageDefaulter;->getDefaultLanguage(Ljava/util/Locale;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 50
    .end local v8    # "locale":Ljava/util/Locale;
    :goto_1
    return-object v10

    :cond_1
    const-string/jumbo v10, "en-US"

    goto :goto_1
.end method

.method private static getDefaultLanguage(Ljava/util/Locale;[Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "locale"    # Ljava/util/Locale;
    .param p1, "availableLanguages"    # [Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    .line 54
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "-"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 59
    .local v6, "localeStr":Ljava/lang/String;
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v3, v0, v2

    .line 60
    .local v3, "lang":Ljava/lang/String;
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 92
    .end local v3    # "lang":Ljava/lang/String;
    .end local v6    # "localeStr":Ljava/lang/String;
    :goto_1
    return-object v6

    .line 59
    .restart local v3    # "lang":Ljava/lang/String;
    .restart local v6    # "localeStr":Ljava/lang/String;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 67
    .end local v3    # "lang":Ljava/lang/String;
    :cond_1
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 68
    .local v7, "matchedLanguages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    move-object v0, p1

    array-length v5, v0

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v5, :cond_3

    aget-object v1, v0, v2

    .line 69
    .local v1, "fullAvailableLocale":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/settings/LanguageDefaulter;->getLanguageSubString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 70
    .local v4, "languageSubstring":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const-string/jumbo v8, "pt-BR"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 74
    invoke-interface {v7, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 68
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 80
    .end local v1    # "fullAvailableLocale":Ljava/lang/String;
    .end local v4    # "languageSubstring":Ljava/lang/String;
    :cond_3
    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v8

    if-ne v8, v10, :cond_4

    .line 81
    invoke-interface {v7}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v8

    const/4 v9, 0x0

    aget-object v8, v8, v9

    check-cast v8, Ljava/lang/String;

    move-object v6, v8

    goto :goto_1

    .line 85
    :cond_4
    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v8

    if-le v8, v10, :cond_5

    .line 86
    invoke-static {v7, p0}, Lcom/vlingo/core/internal/settings/LanguageDefaulter;->chooseBestLanguage(Ljava/util/Set;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 92
    :cond_5
    const-string/jumbo v6, "en-US"

    goto :goto_1
.end method

.method private static getLanguageSubString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "fullAvailableLocale"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x2

    .line 101
    invoke-static {p0}, Lcom/vlingo/core/internal/settings/LanguageDefaulter;->isVlingoSpecialLanguage(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    const/4 v0, 0x4

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 104
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static isVlingoSpecialLanguage(Ljava/lang/String;)Z
    .locals 2
    .param p0, "fullAvailableLocale"    # Ljava/lang/String;

    .prologue
    .line 97
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "v-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
