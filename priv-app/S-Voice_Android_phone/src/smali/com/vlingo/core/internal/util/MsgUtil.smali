.class public interface abstract Lcom/vlingo/core/internal/util/MsgUtil;
.super Ljava/lang/Object;
.source "MsgUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/CoreAdapter;


# virtual methods
.method public abstract countAttachmentMMS(Landroid/database/Cursor;)I
.end method

.method public abstract getMmsQueryCursor(J)Landroid/database/Cursor;
.end method

.method public abstract getMmsTextByCursor(Landroid/database/Cursor;)Ljava/lang/String;
.end method

.method public abstract sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
.end method
