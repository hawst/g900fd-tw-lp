.class final Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarUpdateControlImpl;
.super Ljava/lang/Object;
.source "VoconGrammarUpdater.java"

# interfaces
.implements Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateControl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "GrammarUpdateControlImpl"
.end annotation


# instance fields
.field private final voconSlotUpdateControl:Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerControl;


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerControl;)V
    .locals 0
    .param p1, "voconSlotUpdateControl"    # Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerControl;

    .prologue
    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    iput-object p1, p0, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarUpdateControlImpl;->voconSlotUpdateControl:Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerControl;

    .line 196
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerControl;Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerControl;
    .param p2, "x1"    # Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$1;

    .prologue
    .line 191
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarUpdateControlImpl;-><init>(Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerControl;)V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarUpdateControlImpl;->voconSlotUpdateControl:Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerControl;

    invoke-interface {v0}, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerControl;->cancel()V

    .line 201
    return-void
.end method
