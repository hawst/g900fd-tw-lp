.class public Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderReplyController;
.super Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;
.source "SafeReaderReplyController.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/dialogmanager/types/MessageType;)V
    .locals 1
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .param p3, "mt"    # Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    .prologue
    .line 15
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/controllers/SMSController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 16
    invoke-virtual {p0, p2}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderReplyController;->setListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 17
    invoke-virtual {p3}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getContactName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderReplyController;->contactSelectedName:Ljava/lang/String;

    .line 18
    invoke-virtual {p3}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderReplyController;->address:Ljava/lang/String;

    .line 19
    invoke-virtual {p3}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderReplyController;->message:Ljava/lang/String;

    .line 20
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderReplyController;->handleNeedMessage()V

    .line 21
    return-void
.end method
