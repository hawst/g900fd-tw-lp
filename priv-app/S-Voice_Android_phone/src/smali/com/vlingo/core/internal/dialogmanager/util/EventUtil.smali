.class public final Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;
.super Ljava/lang/Object;
.source "EventUtil.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static extractScheduleEvent(Lcom/vlingo/sdk/recognition/VLAction;Ljava/util/List;)Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .locals 10
    .param p0, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;"
        }
    .end annotation

    .prologue
    .local p1, "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/4 v9, 0x0

    .line 37
    new-instance v4, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-direct {v4}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;-><init>()V

    .line 38
    .local v4, "event":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    const-string/jumbo v7, "title"

    invoke-static {p0, v7, v9}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setTitle(Ljava/lang/String;)V

    .line 39
    const-string/jumbo v7, "location"

    invoke-static {p0, v7, v9}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setLocation(Ljava/lang/String;)V

    .line 42
    :try_start_0
    const-string/jumbo v7, "begin"

    const/4 v8, 0x0

    invoke-static {p0, v7, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setBegin(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/InvalidParameterException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    .line 50
    :goto_0
    const-string/jumbo v7, "duration"

    invoke-static {p0, v7, v9, v9}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamInt(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;IZ)I

    move-result v2

    .line 51
    .local v2, "duration":I
    if-lez v2, :cond_0

    .line 52
    mul-int/lit8 v7, v2, 0x3c

    mul-int/lit16 v7, v7, 0x3e8

    invoke-virtual {v4, v7}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setDuration(I)V

    .line 54
    :cond_0
    const-string/jumbo v7, "invitees"

    const-string/jumbo v8, "id"

    invoke-static {p0, v7, v8, v9}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamStringList(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v1

    .line 55
    .local v1, "contactIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v1, :cond_2

    .line 56
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 57
    .local v0, "contactId":Ljava/lang/String;
    invoke-static {p1, v0}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getContactDataFromId(Ljava/util/List;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactData;

    move-result-object v6

    .line 58
    .local v6, "participant":Lcom/vlingo/core/internal/contacts/ContactData;
    if-eqz v6, :cond_1

    .line 59
    invoke-virtual {v4, v6}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->addContactData(Lcom/vlingo/core/internal/contacts/ContactData;)V

    goto :goto_1

    .line 43
    .end local v0    # "contactId":Ljava/lang/String;
    .end local v1    # "contactIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "duration":I
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "participant":Lcom/vlingo/core/internal/contacts/ContactData;
    :catch_0
    move-exception v3

    .line 45
    .local v3, "e":Ljava/security/InvalidParameterException;
    invoke-virtual {v3}, Ljava/security/InvalidParameterException;->printStackTrace()V

    goto :goto_0

    .line 46
    .end local v3    # "e":Ljava/security/InvalidParameterException;
    :catch_1
    move-exception v3

    .line 48
    .local v3, "e":Ljava/text/ParseException;
    invoke-virtual {v3}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0

    .line 63
    .end local v3    # "e":Ljava/text/ParseException;
    .restart local v1    # "contactIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "duration":I
    :cond_2
    return-object v4
.end method

.method public static extractScheduleQuery(Lcom/vlingo/sdk/recognition/VLAction;Ljava/util/List;)Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;
    .locals 5
    .param p0, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)",
            "Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;"
        }
    .end annotation

    .prologue
    .line 256
    .local p1, "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    new-instance v1, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;

    invoke-direct {v1}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;-><init>()V

    .line 258
    .local v1, "query":Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;
    :try_start_0
    const-string/jumbo v2, "title"

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->setTitle(Ljava/lang/String;)V

    .line 259
    const-string/jumbo v2, "location"

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->setLocation(Ljava/lang/String;)V

    .line 260
    const-string/jumbo v2, "invitee"

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->setAttendee(Ljava/lang/String;)V

    .line 261
    const-string/jumbo v2, "range.start"

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->setBegin(Ljava/lang/String;)V

    .line 262
    const-string/jumbo v2, "range.end"

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->setEnd(Ljava/lang/String;)V

    .line 263
    const-string/jumbo v2, "match.type"

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->setType(Ljava/lang/String;)V

    .line 264
    invoke-static {p0}, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->getCount(Lcom/vlingo/sdk/recognition/VLAction;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->setCount(I)V

    .line 265
    const-string/jumbo v2, "include.all.day"

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {p0, v2, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->setIncludeAllDay(Z)V
    :try_end_0
    .catch Ljava/security/InvalidParameterException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    .line 273
    :goto_0
    return-object v1

    .line 266
    :catch_0
    move-exception v0

    .line 268
    .local v0, "e":Ljava/security/InvalidParameterException;
    invoke-virtual {v0}, Ljava/security/InvalidParameterException;->printStackTrace()V

    goto :goto_0

    .line 269
    .end local v0    # "e":Ljava/security/InvalidParameterException;
    :catch_1
    move-exception v0

    .line 271
    .local v0, "e":Ljava/text/ParseException;
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0
.end method

.method public static extractTask(Lcom/vlingo/sdk/recognition/VLAction;)Lcom/vlingo/core/internal/schedule/ScheduleTask;
    .locals 4
    .param p0, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 77
    new-instance v1, Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-direct {v1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;-><init>()V

    .line 79
    .local v1, "task":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    :try_start_0
    const-string/jumbo v2, "title"

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setTitle(Ljava/lang/String;)V

    .line 80
    const-string/jumbo v2, "date"

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setBegin(Ljava/lang/String;)V

    .line 81
    const-string/jumbo v2, "reminder"

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setReminderDateTime(Ljava/lang/String;)V

    .line 82
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->normalize(Z)V
    :try_end_0
    .catch Ljava/security/InvalidParameterException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    .line 88
    :goto_0
    return-object v1

    .line 83
    :catch_0
    move-exception v0

    .line 84
    .local v0, "e1":Ljava/security/InvalidParameterException;
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "Unable to normalize date and time"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 85
    .end local v0    # "e1":Ljava/security/InvalidParameterException;
    :catch_1
    move-exception v0

    .line 86
    .local v0, "e1":Ljava/text/ParseException;
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "Unable to normalize date and time"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static extractTaskQuery(Lcom/vlingo/sdk/recognition/VLAction;)Lcom/vlingo/core/internal/schedule/TaskQueryObject;
    .locals 5
    .param p0, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 277
    new-instance v1, Lcom/vlingo/core/internal/schedule/TaskQueryObject;

    invoke-direct {v1}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;-><init>()V

    .line 279
    .local v1, "query":Lcom/vlingo/core/internal/schedule/TaskQueryObject;
    :try_start_0
    const-string/jumbo v2, "range.start"

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->setBegin(Ljava/lang/String;)V

    .line 280
    const-string/jumbo v2, "range.end"

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->setEnd(Ljava/lang/String;)V

    .line 281
    const-string/jumbo v2, "match.undated"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {p0, v2, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 282
    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->matchUndatedItems()V

    .line 284
    :cond_0
    const-string/jumbo v2, "match.completed"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {p0, v2, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 285
    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->matchCompletedItems()V

    .line 287
    :cond_1
    const-string/jumbo v2, "title"

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->setTitle(Ljava/lang/String;)V

    .line 288
    const-string/jumbo v2, "count"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {p0, v2, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamInt(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;IZ)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->setCount(I)V

    .line 289
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->normalize(Z)V
    :try_end_0
    .catch Ljava/security/InvalidParameterException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    .line 295
    :goto_0
    return-object v1

    .line 290
    :catch_0
    move-exception v0

    .line 291
    .local v0, "e":Ljava/security/InvalidParameterException;
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "Unable to normalize date and time"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 292
    .end local v0    # "e":Ljava/security/InvalidParameterException;
    :catch_1
    move-exception v0

    .line 293
    .local v0, "e":Ljava/text/ParseException;
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "Unable to normalize date and time"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getCount(Lcom/vlingo/sdk/recognition/VLAction;)I
    .locals 5
    .param p0, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v4, 0x0

    .line 231
    const-string/jumbo v3, "count"

    invoke-static {p0, v3, v4, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamInt(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;IZ)I

    move-result v1

    .line 234
    .local v1, "toReturn":I
    if-eqz v1, :cond_0

    move v2, v1

    .line 251
    .end local v1    # "toReturn":I
    .local v2, "toReturn":I
    :goto_0
    return v2

    .line 239
    .end local v2    # "toReturn":I
    .restart local v1    # "toReturn":I
    :cond_0
    const-string/jumbo v3, "limit"

    invoke-static {p0, v3, v4, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamInt(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;IZ)I

    move-result v1

    .line 240
    if-eqz v1, :cond_1

    move v2, v1

    .line 241
    .end local v1    # "toReturn":I
    .restart local v2    # "toReturn":I
    goto :goto_0

    .line 245
    .end local v2    # "toReturn":I
    .restart local v1    # "toReturn":I
    :cond_1
    const-string/jumbo v3, "multi.widget.client.capped"

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 246
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getMultiWidgetItemsInitialMax()I

    move-result v0

    .line 247
    .local v0, "settingsInitialCount":I
    if-ge v0, v1, :cond_2

    .line 248
    move v1, v0

    .end local v0    # "settingsInitialCount":I
    :cond_2
    move v2, v1

    .line 251
    .end local v1    # "toReturn":I
    .restart local v2    # "toReturn":I
    goto :goto_0
.end method

.method private static isDeleteKeyword(Ljava/lang/String;)Z
    .locals 1
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 314
    if-eqz p0, :cond_0

    const-string/jumbo v0, "#NULL#"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static mergeChanges(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/schedule/ScheduleEvent;Ljava/util/List;)Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .locals 16
    .param p0, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p1, "origEvent"    # Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;"
        }
    .end annotation

    .prologue
    .line 93
    .local p2, "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->clone()Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    move-result-object v8

    .line 95
    .local v8, "event":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    const-string/jumbo v13, "title"

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->mergeValues(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setTitle(Ljava/lang/String;)V

    .line 97
    const-string/jumbo v13, "begin"

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 98
    .local v1, "begin":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->isDeleteKeyword(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 99
    const-wide/16 v13, 0x0

    invoke-virtual {v8, v13, v14}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setBegin(J)V

    .line 116
    :cond_0
    :goto_0
    const-wide/16 v13, 0x0

    invoke-virtual {v8, v13, v14}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setEnd(J)V

    .line 119
    const-string/jumbo v13, "duration"

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 120
    .local v4, "durFromAction":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_1

    .line 121
    invoke-static {v4}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 122
    .local v10, "iDurFromAction":I
    const v13, 0xea60

    mul-int/2addr v10, v13

    .line 123
    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 125
    .end local v10    # "iDurFromAction":I
    :cond_1
    const-string/jumbo v13, "duration"

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getDuration()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->mergeValues(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 126
    .local v6, "durationStr":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 127
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getDuration()J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    .line 133
    :goto_1
    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_2

    .line 135
    :try_start_0
    invoke-static {v6}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 136
    .local v5, "duration":I
    invoke-virtual {v8, v5}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setDuration(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_2

    .line 142
    .end local v5    # "duration":I
    :cond_2
    :goto_2
    const-string/jumbo v13, "location"

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getLocation()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->mergeValues(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setLocation(Ljava/lang/String;)V

    .line 144
    const-string/jumbo v13, "invitees"

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v11

    .line 145
    .local v11, "invitees":Ljava/lang/String;
    invoke-static {v11}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_7

    .line 146
    const-string/jumbo v13, "invitees"

    const-string/jumbo v14, "id"

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v13, v14, v15}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamStringList(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v3

    .line 147
    .local v3, "contactIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v3, :cond_7

    .line 148
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 149
    .local v2, "contactId":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getContactDataFromId(Ljava/util/List;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactData;

    move-result-object v12

    .line 150
    .local v12, "participant":Lcom/vlingo/core/internal/contacts/ContactData;
    if-eqz v12, :cond_3

    .line 151
    invoke-virtual {v8, v12}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->addContactData(Lcom/vlingo/core/internal/contacts/ContactData;)V

    goto :goto_3

    .line 102
    .end local v2    # "contactId":Ljava/lang/String;
    .end local v3    # "contactIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "durFromAction":Ljava/lang/String;
    .end local v6    # "durationStr":Ljava/lang/String;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v11    # "invitees":Ljava/lang/String;
    .end local v12    # "participant":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_4
    if-eqz v1, :cond_0

    .line 104
    :try_start_1
    invoke-virtual {v8, v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setBegin(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/security/InvalidParameterException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 105
    :catch_0
    move-exception v7

    .line 107
    .local v7, "e":Ljava/security/InvalidParameterException;
    invoke-virtual {v7}, Ljava/security/InvalidParameterException;->printStackTrace()V

    goto/16 :goto_0

    .line 108
    .end local v7    # "e":Ljava/security/InvalidParameterException;
    :catch_1
    move-exception v7

    .line 110
    .local v7, "e":Ljava/text/ParseException;
    invoke-virtual {v7}, Ljava/text/ParseException;->printStackTrace()V

    goto/16 :goto_0

    .line 128
    .end local v7    # "e":Ljava/text/ParseException;
    .restart local v4    # "durFromAction":Ljava/lang/String;
    .restart local v6    # "durationStr":Ljava/lang/String;
    :cond_5
    invoke-static {v4}, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->isDeleteKeyword(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 129
    const/4 v6, 0x0

    goto :goto_1

    .line 131
    :cond_6
    move-object v6, v4

    goto :goto_1

    .line 138
    :catch_2
    move-exception v7

    .line 139
    .local v7, "e":Ljava/lang/NumberFormatException;
    sget-object v13, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Invalid duration string, not an int "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 157
    .end local v7    # "e":Ljava/lang/NumberFormatException;
    .restart local v11    # "invitees":Ljava/lang/String;
    :cond_7
    return-object v8
.end method

.method public static mergeChanges(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/schedule/ScheduleTask;)Lcom/vlingo/core/internal/schedule/ScheduleTask;
    .locals 8
    .param p0, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p1, "origTask"    # Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .prologue
    const/4 v7, 0x0

    .line 180
    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->clone()Lcom/vlingo/core/internal/schedule/EventBase;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .line 182
    .local v4, "task":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    const-string/jumbo v5, "title"

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->mergeValues(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setTitle(Ljava/lang/String;)V

    .line 184
    const-string/jumbo v5, "date"

    invoke-static {p0, v5, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 185
    .local v0, "begin":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->isDeleteKeyword(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 186
    const-wide/16 v5, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setBegin(J)V

    .line 202
    :cond_0
    :goto_0
    const-string/jumbo v5, "reminder"

    invoke-static {p0, v5, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 203
    .local v3, "reminder":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->isDeleteKeyword(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 204
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setReminderDateTime(Ljava/lang/String;)V

    .line 212
    :cond_1
    :goto_1
    const-string/jumbo v5, "complete"

    invoke-static {p0, v5, v7, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v1

    .line 213
    .local v1, "complete":Z
    if-eqz v1, :cond_2

    .line 214
    invoke-virtual {v4, v1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setCompleted(Z)V

    .line 218
    :cond_2
    const/4 v5, 0x1

    :try_start_0
    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->normalize(Z)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_2

    .line 222
    :goto_2
    return-object v4

    .line 189
    .end local v1    # "complete":Z
    .end local v3    # "reminder":Ljava/lang/String;
    :cond_3
    if-eqz v0, :cond_0

    .line 191
    :try_start_1
    invoke-virtual {v4, v0}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setBegin(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/security/InvalidParameterException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 192
    :catch_0
    move-exception v2

    .line 194
    .local v2, "e":Ljava/security/InvalidParameterException;
    invoke-virtual {v2}, Ljava/security/InvalidParameterException;->printStackTrace()V

    goto :goto_0

    .line 195
    .end local v2    # "e":Ljava/security/InvalidParameterException;
    :catch_1
    move-exception v2

    .line 197
    .local v2, "e":Ljava/text/ParseException;
    invoke-virtual {v2}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0

    .line 207
    .end local v2    # "e":Ljava/text/ParseException;
    .restart local v3    # "reminder":Ljava/lang/String;
    :cond_4
    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 208
    invoke-virtual {v4, v3}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setReminderDateTime(Ljava/lang/String;)V

    goto :goto_1

    .line 219
    .restart local v1    # "complete":Z
    :catch_2
    move-exception v2

    .line 220
    .restart local v2    # "e":Ljava/text/ParseException;
    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "Unable to normalize date and time"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public static mergeChanges(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/util/Alarm;)Lcom/vlingo/core/internal/util/Alarm;
    .locals 5
    .param p0, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p1, "origAlarm"    # Lcom/vlingo/core/internal/util/Alarm;

    .prologue
    .line 161
    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->clone()Lcom/vlingo/core/internal/util/Alarm;

    move-result-object v0

    .line 163
    .local v0, "alarm":Lcom/vlingo/core/internal/util/Alarm;
    const-string/jumbo v2, "time"

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->getTimeCanonical()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->mergeValues(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/util/Alarm;->setTimeFromCanonical(Ljava/lang/String;)V

    .line 164
    const-string/jumbo v3, "set"

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/Alarm;->getAlarmStatus()Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    move-result-object v2

    sget-object v4, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->INACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    if-eq v2, v4, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v3, v2}, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->mergeValues(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 165
    .local v1, "isActive":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->isDeleteKeyword(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 166
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "Didn\'t expect NULL keyword for \"set\". Setting value to false"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    sget-object v2, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->INACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/util/Alarm;->setAlarmState(Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;)V

    .line 176
    :goto_1
    return-object v0

    .line 164
    .end local v1    # "isActive":Ljava/lang/String;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 170
    .restart local v1    # "isActive":Ljava/lang/String;
    :cond_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 171
    sget-object v2, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->ACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/util/Alarm;->setAlarmState(Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;)V

    goto :goto_1

    .line 173
    :cond_2
    sget-object v2, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->INACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/util/Alarm;->setAlarmState(Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;)V

    goto :goto_1
.end method

.method private static mergeValues(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "origName"    # Ljava/lang/String;

    .prologue
    .line 299
    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 300
    .local v0, "newName":Ljava/lang/String;
    invoke-static {v0, p2}, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->mergeValues(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static mergeValues(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "newName"    # Ljava/lang/String;
    .param p1, "origName"    # Ljava/lang/String;

    .prologue
    .line 304
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    .end local p1    # "origName":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 307
    .restart local p1    # "origName":Ljava/lang/String;
    :cond_0
    invoke-static {p0}, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->isDeleteKeyword(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 308
    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    move-object p1, p0

    .line 310
    goto :goto_0
.end method
