.class public final Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;
.super Ljava/lang/Object;
.source "CoreSpotter.java"

# interfaces
.implements Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;


# static fields
.field private static smInstance:Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;


# instance fields
.field private mDeltaD:I

.field private mSampleRate:I

.field private mSpotter:Lcom/vlingo/sdk/recognition/spotter/VLSpotter;

.field private mStarted:Z

.field private mVLSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

.field private wakeUpExternalStorage:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->smInstance:Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;

    return-void
.end method

.method private constructor <init>(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;)V
    .locals 0
    .param p1, "csParams"    # Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->updateParams(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;)V

    .line 48
    return-void
.end method

.method public static declared-synchronized getInstance(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;)Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;
    .locals 3
    .param p0, "csParams"    # Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 34
    const-class v1, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->smInstance:Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;

    if-nez v0, :cond_1

    .line 35
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;-><init>(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;)V

    sput-object v0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->smInstance:Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;

    .line 43
    :cond_0
    :goto_0
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->smInstance:Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 39
    :cond_1
    :try_start_1
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->smInstance:Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;

    iget-object v0, v0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->mVLSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->smInstance:Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->updateParams(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private init()V
    .locals 5

    .prologue
    .line 64
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isCarMode()Z

    move-result v1

    .line 65
    .local v1, "isCarMode":Z
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v0

    .line 67
    .local v0, "isBluetoothOn":Z
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/VLSdk;->getSpotter()Lcom/vlingo/sdk/recognition/spotter/VLSpotter;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->mSpotter:Lcom/vlingo/sdk/recognition/spotter/VLSpotter;

    .line 68
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->mSpotter:Lcom/vlingo/sdk/recognition/spotter/VLSpotter;

    iget-object v3, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->mVLSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->wakeUpExternalStorage:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v1, v0}, Lcom/vlingo/sdk/recognition/spotter/VLSpotter;->startSpotter(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;Ljava/lang/String;ZZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->mStarted:Z

    .line 69
    iget-boolean v2, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->mStarted:Z

    if-nez v2, :cond_0

    .line 70
    const-string/jumbo v2, "CoreSpotter VLG_EXCEPTION"

    const-string/jumbo v3, "startSpotter failed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    :cond_0
    return-void
.end method

.method private processData([SII)Ljava/lang/String;
    .locals 4
    .param p1, "audioData"    # [S
    .param p2, "offset"    # I
    .param p3, "size"    # I

    .prologue
    .line 101
    mul-int/lit8 v1, p3, 0x2

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 102
    .local v0, "bb":Ljava/nio/ByteBuffer;
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 103
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Ljava/nio/ShortBuffer;->put([SII)Ljava/nio/ShortBuffer;

    .line 104
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->mSpotter:Lcom/vlingo/sdk/recognition/spotter/VLSpotter;

    iget v2, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->mSampleRate:I

    int-to-long v2, v2

    invoke-interface {v1, v0, v2, v3}, Lcom/vlingo/sdk/recognition/spotter/VLSpotter;->phrasespotPipe(Ljava/nio/ByteBuffer;J)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private updateParams(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;)V
    .locals 7
    .param p1, "csParams"    # Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    .prologue
    .line 130
    invoke-virtual {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getCGFilename()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 131
    invoke-virtual {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getCGFilename()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;->getCompiledFileSource(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;

    move-result-object v6

    .line 139
    .local v6, "gs":Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;
    :goto_0
    new-instance v0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;

    invoke-direct {v0, v6}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;-><init>(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;)V

    .line 140
    .local v0, "builder":Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;
    invoke-virtual {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getBeam()F

    move-result v1

    invoke-virtual {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getAbsbeam()F

    move-result v2

    invoke-virtual {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getAoffset()F

    move-result v3

    invoke-virtual {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getDelay()F

    move-result v4

    invoke-virtual {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->spotterParams(FFFFLjava/lang/String;)Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;

    .line 141
    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->build()Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->mVLSpotterContext:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    .line 142
    invoke-virtual {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getDeltaD()I

    move-result v1

    iput v1, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->mDeltaD:I

    .line 143
    invoke-virtual {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getWakeUpExternalStorage()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->wakeUpExternalStorage:Ljava/lang/String;

    .line 144
    return-void

    .line 133
    .end local v0    # "builder":Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;
    .end local v6    # "gs":Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getGrammarSpec()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 134
    invoke-virtual {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getGrammarSpec()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getWordList()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getPronunList()[Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;->getGrammarSpecSource(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;

    move-result-object v6

    .restart local v6    # "gs":Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;
    goto :goto_0

    .line 137
    .end local v6    # "gs":Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "Invalid parameters"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->mStarted:Z

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->mSpotter:Lcom/vlingo/sdk/recognition/spotter/VLSpotter;

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/spotter/VLSpotter;->stopSpotter()V

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->mSpotter:Lcom/vlingo/sdk/recognition/spotter/VLSpotter;

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/spotter/VLSpotter;->destroy()V

    .line 82
    return-void
.end method

.method public getDeltaD()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->mDeltaD:I

    return v0
.end method

.method public getSpottedPhraseScore()F
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->mSpotter:Lcom/vlingo/sdk/recognition/spotter/VLSpotter;

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/spotter/VLSpotter;->getLastScore()F

    move-result v0

    return v0
.end method

.method public init(I)V
    .locals 0
    .param p1, "sampleRate"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->mSampleRate:I

    .line 60
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->init()V

    .line 61
    return-void
.end method

.method public processShortArray([SII)Ljava/lang/String;
    .locals 3
    .param p1, "audioData"    # [S
    .param p2, "offset"    # I
    .param p3, "size"    # I

    .prologue
    .line 86
    const/4 v1, 0x0

    .line 87
    .local v1, "spottedPhrase":Ljava/lang/String;
    iget-boolean v2, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->mStarted:Z

    if-eqz v2, :cond_0

    .line 89
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->processData([SII)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 97
    :cond_0
    :goto_0
    return-object v1

    .line 90
    :catch_0
    move-exception v0

    .line 93
    .local v0, "ise":Ljava/lang/IllegalStateException;
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->init()V

    .line 94
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->processData([SII)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public useSeamlessFeature(Ljava/lang/String;)Z
    .locals 1
    .param p1, "spottedPhrase"    # Ljava/lang/String;

    .prologue
    .line 109
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isSeamless()Z

    move-result v0

    return v0
.end method
