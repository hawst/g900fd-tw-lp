.class public final Lcom/vlingo/core/internal/audio/AudioPlayerProxy;
.super Lcom/vlingo/core/internal/util/ServiceProxyBase;
.source "AudioPlayerProxy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/audio/AudioPlayerProxy$1;,
        Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/core/internal/util/ServiceProxyBase",
        "<",
        "Lcom/vlingo/core/internal/audio/IAudioPlaybackService;",
        "Lcom/vlingo/core/internal/audio/AudioPlaybackService;",
        ">;"
    }
.end annotation


# static fields
.field private static final PLAY:I = 0x15

.field private static final STOP:I = 0x16

.field private static final TAG:Ljava/lang/String;

.field private static audioStream:I

.field private static instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;


# instance fields
.field private final externalListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->TAG:Ljava/lang/String;

    .line 23
    const/4 v0, 0x3

    sput v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->audioStream:I

    .line 25
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/util/ServiceProxyBase;-><init>(Landroid/content/Context;)V

    .line 145
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->externalListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 146
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/audio/AudioPlayerProxy;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->externalListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method public static declared-synchronized addListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 4
    .param p0, "listener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    .line 120
    const-class v1, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    if-eqz v0, :cond_1

    .line 121
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->externalListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 122
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->externalListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 125
    :cond_1
    :try_start_1
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "addListener: instance is null, listener: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized deinit()V
    .locals 2

    .prologue
    .line 63
    const-class v1, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    if-eqz v0, :cond_0

    .line 64
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->release()V

    .line 65
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    :cond_0
    monitor-exit v1

    return-void

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized init(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    const-class v1, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    monitor-enter v1

    const/4 v0, 0x0

    .line 53
    .local v0, "connectService":Z
    :try_start_0
    invoke-static {p0, v0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->init(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    monitor-exit v1

    return-void

    .line 52
    :catchall_0
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method public static declared-synchronized init(Landroid/content/Context;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "connectService"    # Z

    .prologue
    .line 40
    const-class v1, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    .line 42
    if-eqz p1, :cond_0

    .line 43
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->connect()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    :cond_0
    monitor-exit v1

    return-void

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized play(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 3
    .param p0, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 74
    const-class v1, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    if-eqz v0, :cond_0

    .line 75
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->playInternal(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    :cond_0
    monitor-exit v1

    return-void

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized play(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 2
    .param p0, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p1, "listener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    .line 80
    const-class v1, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    if-eqz v0, :cond_0

    .line 81
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->playInternal(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    :cond_0
    monitor-exit v1

    return-void

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized play(Lcom/vlingo/core/internal/audio/TTSRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 3
    .param p0, "request"    # Lcom/vlingo/core/internal/audio/TTSRequest;
    .param p1, "listener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    .line 86
    const-class v1, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/vlingo/core/internal/audio/TTSRequest;->type:I

    if-nez v0, :cond_1

    .line 87
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "voice prompts off for Prompt, play no-op"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 90
    :cond_1
    :try_start_1
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    if-eqz v0, :cond_0

    .line 91
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->playInternal(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized playAnyway(Lcom/vlingo/core/internal/audio/TTSRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 2
    .param p0, "request"    # Lcom/vlingo/core/internal/audio/TTSRequest;
    .param p1, "listener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    .line 96
    const-class v1, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    if-eqz v0, :cond_0

    .line 97
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->playInternal(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    :cond_0
    monitor-exit v1

    return-void

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private playInternal(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "listener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    .line 149
    new-instance v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p2, v1}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;-><init>(Lcom/vlingo/core/internal/audio/AudioPlayerProxy;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;Lcom/vlingo/core/internal/audio/AudioPlayerProxy$1;)V

    .line 150
    .local v0, "proxyListener":Lcom/vlingo/core/internal/audio/AudioPlayerProxy$AudioPlaybackListenerProxy;
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->usingBluetooth()V

    .line 151
    sget v1, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->audioStream:I

    invoke-virtual {p1, v1}, Lcom/vlingo/core/internal/audio/AudioRequest;->setStream(I)V

    .line 152
    iput-object v0, p1, Lcom/vlingo/core/internal/audio/AudioRequest;->listener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .line 153
    const/16 v1, 0x15

    invoke-virtual {p0, v1, p1}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->execute(ILjava/lang/Object;)V

    .line 154
    return-void
.end method

.method public static declared-synchronized playTone(I)V
    .locals 3
    .param p0, "toneResId"    # I

    .prologue
    .line 102
    const-class v1, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    if-eqz v0, :cond_0

    .line 103
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->playToneInternal(ILcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    :cond_0
    monitor-exit v1

    return-void

    .line 102
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized playTone(ILcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 2
    .param p0, "toneResId"    # I
    .param p1, "listener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    .line 108
    const-class v1, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    if-eqz v0, :cond_0

    .line 109
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->playToneInternal(ILcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    :cond_0
    monitor-exit v1

    return-void

    .line 108
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private playToneInternal(ILcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 1
    .param p1, "toneResId"    # I
    .param p2, "listener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    .line 157
    invoke-static {p1}, Lcom/vlingo/core/internal/audio/ToneAudioRequest;->getRequest(I)Lcom/vlingo/core/internal/audio/ToneAudioRequest;

    move-result-object v0

    .line 158
    .local v0, "request":Lcom/vlingo/core/internal/audio/AudioRequest;
    invoke-direct {p0, v0, p2}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->playInternal(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 159
    return-void
.end method

.method public static declared-synchronized releaseService()V
    .locals 2

    .prologue
    .line 136
    const-class v1, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    if-eqz v0, :cond_0

    .line 137
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    invoke-direct {v0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->releaseServiceInternal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    :cond_0
    monitor-exit v1

    return-void

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private releaseServiceInternal()V
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->execute(I)V

    .line 167
    return-void
.end method

.method public static declared-synchronized removeListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 2
    .param p0, "listener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    .line 130
    const-class v1, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    if-eqz v0, :cond_0

    .line 131
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->externalListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    :cond_0
    monitor-exit v1

    return-void

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized stop()V
    .locals 2

    .prologue
    .line 114
    const-class v1, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    if-eqz v0, :cond_0

    .line 115
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->instance:Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    invoke-direct {v0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->stopInternal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    :cond_0
    monitor-exit v1

    return-void

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private stopInternal()V
    .locals 1

    .prologue
    .line 162
    const/16 v0, 0x16

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->execute(I)V

    .line 163
    return-void
.end method

.method public static declared-synchronized usingBluetooth()V
    .locals 2

    .prologue
    .line 70
    const-class v1, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getCurrentStream()I

    move-result v0

    sput v0, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->audioStream:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    monitor-exit v1

    return-void

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected allowsDelayDisconnect()Z
    .locals 1

    .prologue
    .line 268
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isAppCarModeEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getInterfaceClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/vlingo/core/internal/audio/IAudioPlaybackService;",
            ">;"
        }
    .end annotation

    .prologue
    .line 196
    const-class v0, Lcom/vlingo/core/internal/audio/IAudioPlaybackService;

    return-object v0
.end method

.method protected getMessageName(Landroid/os/Message;)Ljava/lang/String;
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 172
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 178
    const-string/jumbo v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 174
    :pswitch_0
    const-string/jumbo v0, "STOP"

    goto :goto_0

    .line 176
    :pswitch_1
    const-string/jumbo v0, "PLAY"

    goto :goto_0

    .line 172
    nop

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected getServiceClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/vlingo/core/internal/audio/AudioPlaybackService;",
            ">;"
        }
    .end annotation

    .prologue
    .line 201
    const-class v0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;

    return-object v0
.end method

.method protected declared-synchronized handleMessageForService(Landroid/os/Message;Lcom/vlingo/core/internal/audio/IAudioPlaybackService;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;
    .param p2, "serviceParam"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService;

    .prologue
    .line 183
    monitor-enter p0

    :try_start_0
    iget v1, p1, Landroid/os/Message;->what:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v1, :pswitch_data_0

    .line 192
    :goto_0
    monitor-exit p0

    return-void

    .line 185
    :pswitch_0
    :try_start_1
    invoke-interface {p2}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService;->stop()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 183
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 188
    :pswitch_1
    :try_start_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/vlingo/core/internal/audio/AudioRequest;

    .line 189
    .local v0, "request":Lcom/vlingo/core/internal/audio/AudioRequest;
    iget-object v1, v0, Lcom/vlingo/core/internal/audio/AudioRequest;->listener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-interface {p2, v0, v1}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService;->play(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 183
    nop

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic handleMessageForService(Landroid/os/Message;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Message;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 17
    check-cast p2, Lcom/vlingo/core/internal/audio/IAudioPlaybackService;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->handleMessageForService(Landroid/os/Message;Lcom/vlingo/core/internal/audio/IAudioPlaybackService;)V

    return-void
.end method
