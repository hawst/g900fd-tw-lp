.class public Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater;
.super Ljava/lang/Object;
.source "VoconGrammarUpdater.java"

# interfaces
.implements Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$1;,
        Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarUpdateControlImpl;,
        Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$VoconSlotUpdateCallbackImpl;,
        Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarSlotAction;
    }
.end annotation


# static fields
.field private static final KEY_LMTT_VOCON_UPLOADED_CHECKSUM_PREFIX:Ljava/lang/String; = "com.vlingo.core.internal.lmttvocon.vocon.uploaded_checksum_for_"


# instance fields
.field private final strategy:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdaterStrategy;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdaterStrategy;)V
    .locals 0
    .param p1, "strategy"    # Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdaterStrategy;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater;->strategy:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdaterStrategy;

    .line 34
    return-void
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/lmttvocon/UploadType;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmttvocon/UploadType;

    .prologue
    .line 25
    invoke-static {p0}, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater;->getUploadedChecksumKey(Lcom/vlingo/core/internal/lmttvocon/UploadType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getUploadedChecksumKey(Lcom/vlingo/core/internal/lmttvocon/UploadType;)Ljava/lang/String;
    .locals 2
    .param p0, "uploadType"    # Lcom/vlingo/core/internal/lmttvocon/UploadType;

    .prologue
    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "com.vlingo.core.internal.lmttvocon.vocon.uploaded_checksum_for_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCurrentChecksum(Lcom/vlingo/core/internal/lmttvocon/UploadType;)I
    .locals 2
    .param p1, "uploadType"    # Lcom/vlingo/core/internal/lmttvocon/UploadType;

    .prologue
    .line 108
    invoke-static {p1}, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater;->getUploadedChecksumKey(Lcom/vlingo/core/internal/lmttvocon/UploadType;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method protected getSdkVoconSlotUpdater()Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainer;
    .locals 1

    .prologue
    .line 38
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/VLSdk;->getEmbeddedTrainer()Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainer;

    move-result-object v0

    return-object v0
.end method

.method public updateGrammar(Lcom/vlingo/core/internal/lmttvocon/UploadType;Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;ZLcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;)Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateControl;
    .locals 22
    .param p1, "uploadType"    # Lcom/vlingo/core/internal/lmttvocon/UploadType;
    .param p2, "callback"    # Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;
    .param p3, "startFromScratch"    # Z
    .param p4, "grammarActionIterator"    # Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;

    .prologue
    .line 46
    if-eqz p3, :cond_1

    const/4 v13, 0x0

    .line 47
    .local v13, "checksum":I
    :goto_0
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .local v15, "addedSlotTerminals":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;>;"
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 49
    .local v19, "removedSlotTerminals":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;>;"
    :cond_0
    :goto_1
    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 50
    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->next()Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;

    move-result-object v3

    .line 51
    .local v3, "grammarAction":Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->getChecksumDelta()I

    move-result v4

    add-int/2addr v13, v4

    .line 52
    invoke-virtual {v3}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->getContentItem()Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;

    move-result-object v16

    .line 53
    .local v16, "contentItem":Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
    sget-object v4, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$1;->$SwitchMap$com$vlingo$core$internal$lmttvocon$grammar$GrammarActionType:[I

    invoke-virtual {v3}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->getType()Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_1

    .line 56
    :pswitch_0
    new-instance v2, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarSlotAction;

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getId()J

    move-result-wide v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarSlotAction;-><init>(Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;JLjava/lang/String;Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$1;)V

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 46
    .end local v3    # "grammarAction":Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;
    .end local v13    # "checksum":I
    .end local v15    # "addedSlotTerminals":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;>;"
    .end local v16    # "contentItem":Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
    .end local v19    # "removedSlotTerminals":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;>;"
    :cond_1
    invoke-virtual/range {p0 .. p1}, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater;->getCurrentChecksum(Lcom/vlingo/core/internal/lmttvocon/UploadType;)I

    move-result v13

    goto :goto_0

    .line 61
    .restart local v3    # "grammarAction":Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;
    .restart local v13    # "checksum":I
    .restart local v15    # "addedSlotTerminals":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;>;"
    .restart local v16    # "contentItem":Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
    .restart local v19    # "removedSlotTerminals":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;>;"
    :pswitch_1
    new-instance v2, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarSlotAction;

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getId()J

    move-result-wide v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarSlotAction;-><init>(Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;JLjava/lang/String;Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$1;)V

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    :pswitch_2
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getOrthographies()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 67
    .local v6, "orthography":Ljava/lang/String;
    new-instance v2, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarSlotAction;

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getId()J

    move-result-wide v4

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarSlotAction;-><init>(Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;JLjava/lang/String;Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$1;)V

    .line 69
    .local v2, "grammarSlotAction":Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarSlotAction;
    invoke-interface {v15, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 74
    .end local v2    # "grammarSlotAction":Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarSlotAction;
    .end local v3    # "grammarAction":Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;
    .end local v6    # "orthography":Ljava/lang/String;
    .end local v16    # "contentItem":Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
    .end local v17    # "i$":Ljava/util/Iterator;
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater;->strategy:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdaterStrategy;

    if-eqz v4, :cond_5

    if-nez p3, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater;->strategy:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdaterStrategy;

    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->getAllFromScratchGrammarActions()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v5

    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->getAddedGrammarActions()Ljava/util/Collection;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Collection;->size()I

    move-result v7

    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->getModifiedGrammarActions()Ljava/util/Collection;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Collection;->size()I

    move-result v9

    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->getRemovedGrammarActions()Ljava/util/Collection;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Collection;->size()I

    move-result v10

    invoke-interface {v4, v5, v7, v9, v10}, Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdaterStrategy;->startFromScratch(IIII)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_3
    const/16 v20, 0x1

    .line 79
    .local v20, "startFromScratchActual":Z
    :goto_3
    if-eqz v20, :cond_7

    if-nez p3, :cond_7

    .line 82
    const/4 v13, 0x0

    .line 83
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v19

    .line 84
    invoke-interface {v15}, Ljava/util/List;->clear()V

    .line 85
    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarActionIterator;->getAllFromScratchGrammarActions()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_4
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;

    .line 86
    .local v8, "action":Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;
    invoke-virtual {v8}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->getContentItem()Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;

    move-result-object v16

    .line 87
    .restart local v16    # "contentItem":Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getOrthographies()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 88
    .restart local v6    # "orthography":Ljava/lang/String;
    new-instance v7, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarSlotAction;

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;->getId()J

    move-result-wide v9

    const/4 v12, 0x0

    move-object v11, v6

    invoke-direct/range {v7 .. v12}, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarSlotAction;-><init>(Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;JLjava/lang/String;Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$1;)V

    invoke-interface {v15, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 74
    .end local v6    # "orthography":Ljava/lang/String;
    .end local v8    # "action":Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;
    .end local v16    # "contentItem":Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
    .end local v18    # "i$":Ljava/util/Iterator;
    .end local v20    # "startFromScratchActual":Z
    :cond_4
    const/16 v20, 0x0

    goto :goto_3

    :cond_5
    move/from16 v20, p3

    goto :goto_3

    .line 90
    .restart local v8    # "action":Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;
    .restart local v16    # "contentItem":Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
    .restart local v18    # "i$":Ljava/util/Iterator;
    .restart local v20    # "startFromScratchActual":Z
    :cond_6
    invoke-virtual {v8}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->getChecksumDelta()I

    move-result v4

    add-int/2addr v13, v4

    .line 91
    goto :goto_4

    .line 93
    .end local v8    # "action":Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;
    .end local v16    # "contentItem":Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
    .end local v18    # "i$":Ljava/util/Iterator;
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater;->getSdkVoconSlotUpdater()Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainer;

    move-result-object v4

    new-instance v9, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$VoconSlotUpdateCallbackImpl;

    const/4 v14, 0x0

    move-object/from16 v10, p1

    move/from16 v11, v20

    move-object/from16 v12, p2

    invoke-direct/range {v9 .. v14}, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$VoconSlotUpdateCallbackImpl;-><init>(Lcom/vlingo/core/internal/lmttvocon/UploadType;ZLcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;ILcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$1;)V

    new-instance v5, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/lmttvocon/UploadType;->getSlotName()Ljava/lang/String;

    move-result-object v7

    move/from16 v0, v20

    move-object/from16 v1, v19

    invoke-direct {v5, v7, v0, v15, v1}, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;-><init>(Ljava/lang/String;ZLjava/util/Collection;Ljava/util/Collection;)V

    invoke-interface {v4, v9, v5}, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainer;->updateSlots(Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerCallback;Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;)Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerControl;

    move-result-object v21

    .line 96
    .local v21, "voconSlotUpdateControl":Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerControl;
    if-eqz v21, :cond_8

    .line 97
    new-instance v4, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarUpdateControlImpl;

    const/4 v5, 0x0

    move-object/from16 v0, v21

    invoke-direct {v4, v0, v5}, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarUpdateControlImpl;-><init>(Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerControl;Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$1;)V

    .line 102
    :goto_6
    return-object v4

    .line 101
    :cond_8
    invoke-interface/range {p2 .. p2}, Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;->onError()V

    .line 102
    const/4 v4, 0x0

    goto :goto_6

    .line 53
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
