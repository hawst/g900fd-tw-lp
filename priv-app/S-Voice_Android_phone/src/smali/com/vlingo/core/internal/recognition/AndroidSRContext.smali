.class public Lcom/vlingo/core/internal/recognition/AndroidSRContext;
.super Lcom/vlingo/sdk/internal/recognizer/SRContext;
.source "AndroidSRContext.java"


# instance fields
.field private m_Custom6Context:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/SRContext;-><init>()V

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidSRContext;->m_Custom6Context:Ljava/lang/String;

    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "fieldID"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/SRContext;-><init>()V

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/recognition/AndroidSRContext;->m_Custom6Context:Ljava/lang/String;

    .line 22
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/recognition/AndroidSRContext;->setFieldID(Ljava/lang/String;)V

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/recognition/AndroidSRContext;->customFlag:Z

    .line 24
    return-void
.end method


# virtual methods
.method public getCustomParam(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 51
    const-string/jumbo v3, "Custom2"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 52
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 53
    .local v0, "ctx":Landroid/content/Context;
    const-string/jumbo v3, "wifi"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 54
    .local v2, "wifiMgr":Landroid/net/wifi/WifiManager;
    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    .line 55
    .local v1, "info":Landroid/net/wifi/WifiInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v3

    if-ltz v3, :cond_0

    .line 56
    const-string/jumbo v3, "Wifi"

    .line 73
    .end local v0    # "ctx":Landroid/content/Context;
    .end local v1    # "info":Landroid/net/wifi/WifiInfo;
    .end local v2    # "wifiMgr":Landroid/net/wifi/WifiManager;
    :goto_0
    return-object v3

    .line 58
    .restart local v0    # "ctx":Landroid/content/Context;
    .restart local v1    # "info":Landroid/net/wifi/WifiInfo;
    .restart local v2    # "wifiMgr":Landroid/net/wifi/WifiManager;
    :cond_0
    const-string/jumbo v3, "Carrier"

    goto :goto_0

    .line 60
    .end local v0    # "ctx":Landroid/content/Context;
    .end local v1    # "info":Landroid/net/wifi/WifiInfo;
    .end local v2    # "wifiMgr":Landroid/net/wifi/WifiManager;
    :cond_1
    const-string/jumbo v3, "Custom4"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/vlingo/core/internal/recognition/AndroidSRContext;->customFlag:Z

    if-eqz v3, :cond_2

    .line 61
    const-string/jumbo v3, "RecoStartedByWakeUpWord"

    goto :goto_0

    .line 63
    :cond_2
    const-string/jumbo v3, "Custom5"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 65
    const-string/jumbo v3, "audiofilelog_enabled"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 67
    const-string/jumbo v3, "AudioFileLoggingEnabled"

    goto :goto_0

    .line 70
    :cond_3
    iget-object v3, p0, Lcom/vlingo/core/internal/recognition/AndroidSRContext;->m_Custom6Context:Ljava/lang/String;

    if-eqz v3, :cond_4

    const-string/jumbo v3, "Custom6"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 71
    iget-object v3, p0, Lcom/vlingo/core/internal/recognition/AndroidSRContext;->m_Custom6Context:Ljava/lang/String;

    goto :goto_0

    .line 73
    :cond_4
    const-string/jumbo v3, ""

    goto :goto_0
.end method

.method public getFieldType()Ljava/lang/String;
    .locals 3

    .prologue
    .line 41
    const-string/jumbo v0, "<xml><taboofilter>"

    .line 42
    .local v0, "fType":Ljava/lang/String;
    const-string/jumbo v1, "profanity_filter"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "on"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 46
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</taboofilter></xml>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 47
    return-object v0

    .line 45
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "off"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public setCustom6Context(Ljava/lang/String;)V
    .locals 0
    .param p1, "custom6Context"    # Ljava/lang/String;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/vlingo/core/internal/recognition/AndroidSRContext;->m_Custom6Context:Ljava/lang/String;

    .line 79
    return-void
.end method

.method public setRecoStartedByPhraseSpotter(Z)V
    .locals 0
    .param p1, "recoStartedByPhraseSpotter"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/vlingo/core/internal/recognition/AndroidSRContext;->customFlag:Z

    .line 38
    return-void
.end method
