.class final Lcom/vlingo/core/internal/util/StringUtils$4;
.super Ljava/util/HashSet;
.source "StringUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/StringUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashSet",
        "<",
        "Ljava/lang/Character$UnicodeBlock;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    .line 96
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->BASIC_LATIN:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$4;->add(Ljava/lang/Object;)Z

    .line 97
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->LATIN_1_SUPPLEMENT:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$4;->add(Ljava/lang/Object;)Z

    .line 98
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->LATIN_EXTENDED_A:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$4;->add(Ljava/lang/Object;)Z

    .line 99
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->LATIN_EXTENDED_B:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/StringUtils$4;->add(Ljava/lang/Object;)Z

    .line 100
    return-void
.end method
