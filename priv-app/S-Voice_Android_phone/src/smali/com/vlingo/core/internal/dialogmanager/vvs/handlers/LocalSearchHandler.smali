.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "LocalSearchHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
.implements Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;


# instance fields
.field private adaptor:Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

.field private handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 35
    new-instance v0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    invoke-direct {v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->adaptor:Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    .line 37
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->handler:Landroid/os/Handler;

    .line 40
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getAdaptor()Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V

    .line 41
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 31
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 31
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 31
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    return-object v0
.end method

.method private showSuccess()V
    .locals 2

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler$2;

    invoke-direct {v1, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler$2;-><init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 148
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v0

    const-string/jumbo v1, "local search"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 149
    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 9
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v6, 0x0

    .line 48
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 49
    const-string/jumbo v7, "Location"

    invoke-static {p1, v7, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 50
    .local v1, "location":Ljava/lang/String;
    const-string/jumbo v7, "Search"

    invoke-static {p1, v7, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    .line 51
    .local v5, "search":Ljava/lang/String;
    const-string/jumbo v7, "Region"

    invoke-static {p1, v7, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 52
    .local v4, "region":Ljava/lang/String;
    const-string/jumbo v7, "Keyterms"

    invoke-static {p1, v7, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 53
    .local v0, "keyterms":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 54
    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_local_search_blank_request_message_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v8, v6, [Ljava/lang/Object;

    invoke-static {v7, v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 55
    .local v2, "noRequestMessageShown":Ljava/lang/String;
    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_local_search_blank_request_message_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v8, v6, [Ljava/lang/Object;

    invoke-static {v7, v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 56
    .local v3, "noRequestMessageSpoken":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v7

    invoke-interface {v7, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .end local v2    # "noRequestMessageShown":Ljava/lang/String;
    .end local v3    # "noRequestMessageSpoken":Ljava/lang/String;
    :goto_0
    return v6

    .line 59
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getAdaptor()Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->setKeyTerms(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getAdaptor()Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->resetNumberOfRetries()V

    .line 61
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getAdaptor()Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    move-result-object v6

    invoke-virtual {v6, v5, v1, v4}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->executeSearch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const/4 v6, 0x1

    goto :goto_0
.end method

.method public getAdaptor()Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->adaptor:Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    return-object v0
.end method

.method public getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 69
    const-string/jumbo v0, "com.vlingo.core.internal.dialogmanager.AcceptedText"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    instance-of v0, p2, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;

    if-eqz v0, :cond_0

    .line 71
    check-cast p2, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;

    .end local p2    # "object":Ljava/lang/Object;
    invoke-virtual {p0, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 74
    .restart local p2    # "object":Ljava/lang/Object;
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->throwUnknownActionException(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onRequestFailed()V
    .locals 2

    .prologue
    .line 110
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->showFailure()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 117
    :goto_0
    return-void

    .line 111
    :catch_0
    move-exception v0

    .line 115
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    throw v0
.end method

.method public onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V
    .locals 2
    .param p1, "prompt"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler$1;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler$1;-><init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 127
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 128
    return-void
.end method

.method public onRequestScheduled()V
    .locals 0

    .prologue
    .line 102
    return-void
.end method

.method public onResponseReceived()V
    .locals 2

    .prologue
    .line 83
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getAdaptor()Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 84
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->showSuccess()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 96
    :goto_1
    return-void

    .line 88
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->showFailure()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 90
    :catch_0
    move-exception v0

    .line 94
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    throw v0
.end method

.method protected setAdaptor(Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;)V
    .locals 0
    .param p1, "adaptor"    # Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->adaptor:Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    .line 179
    return-void
.end method

.method protected setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->handler:Landroid/os/Handler;

    .line 171
    return-void
.end method

.method protected showFailure()V
    .locals 2

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler$3;

    invoke-direct {v1, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler$3;-><init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 163
    return-void
.end method
