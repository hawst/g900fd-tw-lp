.class public Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;
.super Lcom/vlingo/core/internal/util/BoundedSizeMap;
.source "LocalSearchListingCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/core/internal/util/BoundedSizeMap",
        "<",
        "Ljava/lang/String;",
        "Lcom/vlingo/core/internal/localsearch/LocalSearchListing;",
        ">;"
    }
.end annotation


# static fields
.field static final CACHE_SIZE:I = 0x1e

.field private static final serialVersionUID:J = -0x62815940b39cb2b9L


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    const/16 v0, 0x1e

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/util/BoundedSizeMap;-><init>(I)V

    .line 21
    return-void
.end method


# virtual methods
.method public add(Lcom/vlingo/core/internal/localsearch/LocalSearchListing;)V
    .locals 1
    .param p1, "bi"    # Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    .prologue
    .line 24
    invoke-virtual {p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getListingID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    return-void
.end method

.method public get(Ljava/lang/Object;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    .locals 1
    .param p1, "listingID"    # Ljava/lang/Object;

    .prologue
    .line 29
    invoke-super {p0, p1}, Lcom/vlingo/core/internal/util/BoundedSizeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    .line 32
    .local v0, "bi":Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    if-eqz v0, :cond_0

    .line 34
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    check-cast p1, Ljava/lang/String;

    .end local p1    # "listingID":Ljava/lang/Object;
    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    :cond_0
    return-object v0
.end method

.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;->get(Ljava/lang/Object;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-result-object v0

    return-object v0
.end method
