.class public Lcom/vlingo/core/internal/weather/WeatherInfoUtil;
.super Ljava/lang/Object;
.source "WeatherInfoUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;
    }
.end annotation


# static fields
.field private static sdf:Ljava/text/SimpleDateFormat;

.field private static sdf2:Ljava/text/SimpleDateFormat;


# instance fields
.field private date_val:Ljava/lang/String;

.field private mCurrentDate:Ljava/lang/String;

.field private mCurrentTemperature:Ljava/lang/String;

.field private mLangApplication:Ljava/lang/String;

.field private mLongText:Ljava/lang/String;

.field private mMax:Ljava/lang/String;

.field private mMin:Ljava/lang/String;

.field private mWeatherCode:Ljava/lang/String;

.field private mWeatherElement:Lcom/vlingo/core/internal/weather/WeatherElement;

.field private showCurrentWeather:Z

.field private sunRise:Ljava/lang/String;

.field private sunSet:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 41
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "EEE, d MMM"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->sdf:Ljava/text/SimpleDateFormat;

    .line 42
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->sdf2:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mMax:Ljava/lang/String;

    .line 28
    iput-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mMin:Ljava/lang/String;

    .line 33
    iput-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mCurrentDate:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/core/internal/weather/WeatherElement;)V
    .locals 1
    .param p1, "wetherElement"    # Lcom/vlingo/core/internal/weather/WeatherElement;

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mMax:Ljava/lang/String;

    .line 28
    iput-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mMin:Ljava/lang/String;

    .line 33
    iput-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mCurrentDate:Ljava/lang/String;

    .line 47
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mWeatherElement:Lcom/vlingo/core/internal/weather/WeatherElement;

    .line 48
    invoke-direct {p0}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getWeatherInfoFromResponse()V

    .line 49
    return-void
.end method

.method private getWeatherInfoFromResponse()V
    .locals 4

    .prologue
    .line 178
    iget-object v2, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mWeatherElement:Lcom/vlingo/core/internal/weather/WeatherElement;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 180
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string/jumbo v3, "Temperature"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 181
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mCurrentTemperature:Ljava/lang/String;

    .line 183
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string/jumbo v3, "WeatherText"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 184
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mLongText:Ljava/lang/String;

    .line 186
    :cond_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string/jumbo v3, "WeatherCode"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 187
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mWeatherCode:Ljava/lang/String;

    goto :goto_0

    .line 190
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_3
    return-void
.end method

.method public static getWeatherInfoUtil(Landroid/content/Context;Lcom/vlingo/core/internal/weather/WeatherElement;Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherInfoUtil;
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "weatherDetails"    # Lcom/vlingo/core/internal/weather/WeatherElement;
    .param p2, "adapterDate"    # Ljava/lang/String;

    .prologue
    .line 295
    const/4 v11, 0x0

    .line 296
    .local v11, "mWeatherInfoUtil":Lcom/vlingo/core/internal/weather/WeatherInfoUtil;
    const/4 v9, 0x0

    .local v9, "index":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v14

    if-ge v9, v14, :cond_8

    .line 297
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v2

    .line 298
    .local v2, "child":Lcom/vlingo/core/internal/weather/WeatherElement;
    const-string/jumbo v14, "CurrentCondition"

    invoke-virtual {v2}, Lcom/vlingo/core/internal/weather/WeatherElement;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 301
    const-string/jumbo v14, "CurrentCondition"

    const/4 v15, 0x0

    invoke-virtual {v2, v15}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v15

    invoke-virtual {v15}, Lcom/vlingo/core/internal/weather/WeatherElement;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 302
    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    .line 303
    .local v3, "currentCondition":Lcom/vlingo/core/internal/weather/WeatherElement;
    new-instance v11, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    .end local v11    # "mWeatherInfoUtil":Lcom/vlingo/core/internal/weather/WeatherInfoUtil;
    invoke-direct {v11, v3}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;-><init>(Lcom/vlingo/core/internal/weather/WeatherElement;)V

    .line 304
    .restart local v11    # "mWeatherInfoUtil":Lcom/vlingo/core/internal/weather/WeatherInfoUtil;
    if-eqz p2, :cond_1

    .line 306
    :try_start_0
    new-instance v14, Ljava/util/Date;

    invoke-direct {v14}, Ljava/util/Date;-><init>()V

    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/schedule/DateUtil;->getDateFromCanonicalString(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/vlingo/core/internal/schedule/DateUtil;->isSameOrFurtherDate(Ljava/util/Date;Ljava/util/Date;)Z

    move-result v14

    invoke-virtual {v11, v14}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->setShowCurrentWeather(Z)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 314
    :goto_1
    new-instance v13, Ljava/text/SimpleDateFormat;

    const-string/jumbo v14, "yyyyMMdd "

    sget-object v15, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v13, v14, v15}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 316
    .local v13, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v13, v15}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v14

    const-string/jumbo v16, "Sunrise"

    move-object/from16 v0, v16

    invoke-interface {v14, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v14}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->setSunRise(Ljava/lang/String;)V

    .line 317
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v13, v15}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v14

    const-string/jumbo v16, "Sunset"

    move-object/from16 v0, v16

    invoke-interface {v14, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v14}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->setSunSet(Ljava/lang/String;)V

    .line 296
    .end local v3    # "currentCondition":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v13    # "simpleDateFormat":Ljava/text/SimpleDateFormat;
    :cond_0
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 307
    .restart local v3    # "currentCondition":Lcom/vlingo/core/internal/weather/WeatherElement;
    :catch_0
    move-exception v7

    .line 308
    .local v7, "e":Ljava/text/ParseException;
    const-string/jumbo v14, "WeatherInfoUtil"

    invoke-virtual {v7}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 311
    .end local v7    # "e":Ljava/text/ParseException;
    :cond_1
    const/4 v14, 0x1

    invoke-virtual {v11, v14}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->setShowCurrentWeather(Z)V

    goto :goto_1

    .line 322
    .end local v3    # "currentCondition":Lcom/vlingo/core/internal/weather/WeatherElement;
    :cond_2
    const-string/jumbo v14, "Forecasts"

    invoke-virtual {v2}, Lcom/vlingo/core/internal/weather/WeatherElement;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 325
    move-object v8, v2

    .line 328
    .local v8, "forecasts":Lcom/vlingo/core/internal/weather/WeatherElement;
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v8}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v14

    invoke-direct {v1, v14}, Ljava/util/ArrayList;-><init>(I)V

    .line 330
    .local v1, "arrayDate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v12, 0x0

    .line 331
    .local v12, "minDate":Ljava/util/Date;
    const/4 v5, 0x0

    .line 333
    .local v5, "dateFound":Z
    const/4 v10, 0x0

    .local v10, "index1":I
    :goto_3
    invoke-virtual {v8}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v14

    if-ge v10, v14, :cond_4

    .line 334
    invoke-virtual {v8, v10}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v14

    invoke-virtual {v14}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v14

    const-string/jumbo v15, "ForecastDate"

    invoke-interface {v14, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 335
    .local v4, "date":Ljava/lang/String;
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 338
    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 340
    :try_start_1
    sget-object v14, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->sdf2:Ljava/text/SimpleDateFormat;

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v12

    .line 341
    const/4 v5, 0x1

    .line 333
    :cond_3
    :goto_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 349
    .end local v4    # "date":Ljava/lang/String;
    :cond_4
    if-nez v5, :cond_7

    .line 351
    :try_start_2
    sget-object v15, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->sdf2:Ljava/text/SimpleDateFormat;

    const/4 v14, 0x0

    invoke-virtual {v1, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-virtual {v15, v14}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_2
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v12

    .line 354
    :goto_5
    const/4 v10, 0x1

    :goto_6
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-ge v10, v14, :cond_6

    .line 356
    if-eqz v12, :cond_5

    :try_start_3
    sget-object v15, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->sdf2:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-virtual {v15, v14}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 357
    sget-object v15, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->sdf2:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-virtual {v15, v14}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_3
    .catch Ljava/text/ParseException; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v12

    .line 354
    :cond_5
    :goto_7
    add-int/lit8 v10, v10, 0x1

    goto :goto_6

    .line 362
    :cond_6
    if-eqz p2, :cond_7

    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/schedule/DateUtil;->isToday(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_7

    .line 363
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/schedule/DateUtil;->diffInDays(Ljava/lang/String;)I

    move-result v6

    .line 364
    .local v6, "days":I
    if-lez v6, :cond_7

    invoke-virtual {v8}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v14

    if-ge v6, v14, :cond_7

    .line 365
    invoke-static {v12, v6}, Lcom/vlingo/core/internal/schedule/DateUtil;->plusDays(Ljava/util/Date;I)Ljava/util/Date;

    move-result-object v12

    .line 369
    .end local v6    # "days":I
    :cond_7
    if-eqz v11, :cond_0

    if-eqz v12, :cond_0

    .line 370
    sget-object v14, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->sdf2:Ljava/text/SimpleDateFormat;

    invoke-virtual {v14, v12}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v8, v14}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->setWeatherForOneDay(Lcom/vlingo/core/internal/weather/WeatherElement;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 373
    .end local v1    # "arrayDate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "child":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v5    # "dateFound":Z
    .end local v8    # "forecasts":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v10    # "index1":I
    .end local v12    # "minDate":Ljava/util/Date;
    :cond_8
    return-object v11

    .line 359
    .restart local v1    # "arrayDate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v2    # "child":Lcom/vlingo/core/internal/weather/WeatherElement;
    .restart local v5    # "dateFound":Z
    .restart local v8    # "forecasts":Lcom/vlingo/core/internal/weather/WeatherElement;
    .restart local v10    # "index1":I
    .restart local v12    # "minDate":Ljava/util/Date;
    :catch_1
    move-exception v14

    goto :goto_7

    .line 352
    :catch_2
    move-exception v14

    goto :goto_5

    .line 343
    .restart local v4    # "date":Ljava/lang/String;
    :catch_3
    move-exception v14

    goto :goto_4
.end method

.method private tempToIntValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "temp"    # Ljava/lang/String;

    .prologue
    .line 194
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 195
    .local v0, "t":Ljava/lang/Double;
    if-eqz v0, :cond_0

    .line 196
    invoke-virtual {v0}, Ljava/lang/Double;->longValue()J

    move-result-wide v2

    long-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 197
    .local v1, "ti":Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object p1

    .line 199
    .end local v1    # "ti":Ljava/lang/Integer;
    :cond_0
    return-object p1
.end method


# virtual methods
.method public doesShowCurrentWeather()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->showCurrentWeather:Z

    return v0
.end method

.method protected extractWeatherCode(Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;)Ljava/lang/String;
    .locals 3
    .param p1, "current"    # Lcom/vlingo/core/internal/weather/WeatherElement;
    .param p2, "day"    # Lcom/vlingo/core/internal/weather/WeatherElement;

    .prologue
    .line 164
    const/4 v0, 0x0

    .line 165
    .local v0, "weatherCode":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->doesShowCurrentWeather()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 166
    invoke-virtual {p1}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v1

    const-string/jumbo v2, "WeatherCode"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "weatherCode":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 168
    .restart local v0    # "weatherCode":Ljava/lang/String;
    const-string/jumbo v1, "-1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 169
    invoke-virtual {p2}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v1

    const-string/jumbo v2, "WeatherCode"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "weatherCode":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 174
    .restart local v0    # "weatherCode":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 172
    :cond_1
    invoke-virtual {p2}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v1

    const-string/jumbo v2, "WeatherCode"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "weatherCode":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "weatherCode":Ljava/lang/String;
    goto :goto_0
.end method

.method public getCurrentTemp()I
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mCurrentTemperature:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public getCurrentTempForTTS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mCurrentTemperature:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->tempToIntValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentWeatherCode()I
    .locals 5

    .prologue
    .line 211
    const/4 v1, -0x1

    .line 213
    .local v1, "toReturn":I
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mWeatherCode:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 217
    :goto_0
    return v1

    .line 214
    :catch_0
    move-exception v0

    .line 215
    .local v0, "ex":Ljava/lang/NumberFormatException;
    const-class v2, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "unparse-able integer found for weather code \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mWeatherCode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getDateForOneDayWidget()Ljava/lang/String;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->date_val:Ljava/lang/String;

    return-object v0
.end method

.method public getMaximumTempOneDayWeather()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mMax:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->tempToIntValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMinimumTempOneDayWeather()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mMin:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->tempToIntValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSunRise()Ljava/lang/String;
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->sunRise:Ljava/lang/String;

    return-object v0
.end method

.method public getSunSet()Ljava/lang/String;
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->sunSet:Ljava/lang/String;

    return-object v0
.end method

.method public getWeatherDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mCurrentDate:Ljava/lang/String;

    return-object v0
.end method

.method public getWeatherString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mLongText:Ljava/lang/String;

    return-object v0
.end method

.method public setShowCurrentWeather(Z)V
    .locals 0
    .param p1, "today"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->showCurrentWeather:Z

    .line 60
    return-void
.end method

.method public setSunRise(Ljava/lang/String;)V
    .locals 0
    .param p1, "sunRise"    # Ljava/lang/String;

    .prologue
    .line 381
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->sunRise:Ljava/lang/String;

    .line 382
    return-void
.end method

.method public setSunSet(Ljava/lang/String;)V
    .locals 0
    .param p1, "sunSet"    # Ljava/lang/String;

    .prologue
    .line 389
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->sunSet:Ljava/lang/String;

    .line 390
    return-void
.end method

.method public setWeatherCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "weatherCode"    # Ljava/lang/String;

    .prologue
    .line 233
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mWeatherCode:Ljava/lang/String;

    .line 234
    return-void
.end method

.method public setWeatherForOneDay(Lcom/vlingo/core/internal/weather/WeatherElement;Ljava/lang/String;)V
    .locals 13
    .param p1, "forecasts"    # Lcom/vlingo/core/internal/weather/WeatherElement;
    .param p2, "currentDate"    # Ljava/lang/String;

    .prologue
    .line 94
    const-string/jumbo v0, "EEE, d MMM"

    .line 97
    .local v0, "dateFormat":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mLangApplication:Ljava/lang/String;

    .line 98
    iget-object v8, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mLangApplication:Ljava/lang/String;

    const-string/jumbo v9, "-"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 101
    .local v6, "token":[Ljava/lang/String;
    iget-object v8, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mLangApplication:Ljava/lang/String;

    const-string/jumbo v9, "ko-KR"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 105
    const-string/jumbo v0, "M dd E"

    .line 108
    :cond_0
    new-instance v8, Ljava/text/SimpleDateFormat;

    new-instance v9, Ljava/util/Locale;

    const/4 v10, 0x0

    aget-object v10, v6, v10

    const/4 v11, 0x1

    aget-object v11, v6, v11

    invoke-direct {v9, v10, v11}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v8, v0, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v8, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->sdf:Ljava/text/SimpleDateFormat;

    .line 109
    new-instance v8, Ljava/text/SimpleDateFormat;

    const-string/jumbo v9, "yyyy-MM-dd"

    new-instance v10, Ljava/util/Locale;

    const/4 v11, 0x0

    aget-object v11, v6, v11

    const/4 v12, 0x1

    aget-object v12, v6, v12

    invoke-direct {v10, v11, v12}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v8, v9, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v8, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->sdf2:Ljava/text/SimpleDateFormat;

    .line 113
    :try_start_0
    const-string/jumbo v8, "ForecastDate"

    invoke-virtual {p1, v8, p2}, Lcom/vlingo/core/internal/weather/WeatherElement;->findChildWithAttr(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v4

    .line 115
    .local v4, "forecast":Lcom/vlingo/core/internal/weather/WeatherElement;
    invoke-virtual {v4}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v8

    const-string/jumbo v9, "ForecastDate"

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 116
    .local v1, "dateStr":Ljava/lang/String;
    iput-object v1, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mCurrentDate:Ljava/lang/String;
    :try_end_0
    .catch Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    const/4 v2, 0x0

    .line 120
    .local v2, "day":Lcom/vlingo/core/internal/weather/WeatherElement;
    :try_start_1
    const-string/jumbo v8, "DaytimeForecast"

    invoke-virtual {v4, v8}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v8

    const-string/jumbo v9, "DayNighttimeForecast"

    invoke-virtual {v8, v9}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;
    :try_end_1
    .catch Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v2

    .line 122
    :goto_0
    const/4 v5, 0x0

    .line 125
    .local v5, "night":Lcom/vlingo/core/internal/weather/WeatherElement;
    :try_start_2
    const-string/jumbo v8, "NighttimeForecast"

    invoke-virtual {v4, v8}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v8

    const-string/jumbo v9, "DayNighttimeForecast"

    invoke-virtual {v8, v9}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;
    :try_end_2
    .catch Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v5

    .line 129
    :goto_1
    if-eqz v2, :cond_2

    .line 130
    :try_start_3
    invoke-virtual {v2}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v8

    const-string/jumbo v9, "TempMax"

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mMax:Ljava/lang/String;

    .line 131
    invoke-virtual {v2}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v8

    const-string/jumbo v9, "ShortText"

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mLongText:Ljava/lang/String;

    .line 132
    iget-object v8, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mWeatherElement:Lcom/vlingo/core/internal/weather/WeatherElement;

    invoke-virtual {p0, v8, v2}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->extractWeatherCode(Lcom/vlingo/core/internal/weather/WeatherElement;Lcom/vlingo/core/internal/weather/WeatherElement;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mWeatherCode:Ljava/lang/String;

    .line 142
    :cond_1
    :goto_2
    if-eqz v5, :cond_3

    .line 143
    invoke-virtual {v5}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v8

    const-string/jumbo v9, "TempMin"

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mMin:Ljava/lang/String;

    .line 161
    .end local v1    # "dateStr":Ljava/lang/String;
    .end local v2    # "day":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v4    # "forecast":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v5    # "night":Lcom/vlingo/core/internal/weather/WeatherElement;
    :goto_3
    return-void

    .line 134
    .restart local v1    # "dateStr":Ljava/lang/String;
    .restart local v2    # "day":Lcom/vlingo/core/internal/weather/WeatherElement;
    .restart local v4    # "forecast":Lcom/vlingo/core/internal/weather/WeatherElement;
    .restart local v5    # "night":Lcom/vlingo/core/internal/weather/WeatherElement;
    :cond_2
    invoke-virtual {v4}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v8

    const-string/jumbo v9, "TempMaxC"

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mMax:Ljava/lang/String;

    .line 135
    const-string/jumbo v8, ""

    iput-object v8, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mLongText:Ljava/lang/String;

    .line 136
    invoke-virtual {v4}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v8

    const-string/jumbo v9, "WeatherCode"

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 137
    .local v7, "weatherCodeTmp":Ljava/lang/String;
    const-string/jumbo v8, "-1"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 138
    invoke-virtual {v4}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v8

    const-string/jumbo v9, "WeatherCode"

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mWeatherCode:Ljava/lang/String;
    :try_end_3
    .catch Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    .line 157
    .end local v1    # "dateStr":Ljava/lang/String;
    .end local v2    # "day":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v4    # "forecast":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v5    # "night":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v7    # "weatherCodeTmp":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 159
    .local v3, "e1":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;->printStackTrace()V

    goto :goto_3

    .line 145
    .end local v3    # "e1":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    .restart local v1    # "dateStr":Ljava/lang/String;
    .restart local v2    # "day":Lcom/vlingo/core/internal/weather/WeatherElement;
    .restart local v4    # "forecast":Lcom/vlingo/core/internal/weather/WeatherElement;
    .restart local v5    # "night":Lcom/vlingo/core/internal/weather/WeatherElement;
    :cond_3
    :try_start_4
    invoke-virtual {v4}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v8

    const-string/jumbo v9, "TempMinC"

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->mMin:Ljava/lang/String;
    :try_end_4
    .catch Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_3

    .line 126
    :catch_1
    move-exception v8

    goto/16 :goto_1

    .line 121
    .end local v5    # "night":Lcom/vlingo/core/internal/weather/WeatherElement;
    :catch_2
    move-exception v8

    goto/16 :goto_0
.end method
