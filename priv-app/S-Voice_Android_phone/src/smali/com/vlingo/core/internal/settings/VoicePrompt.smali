.class public Lcom/vlingo/core/internal/settings/VoicePrompt;
.super Ljava/lang/Object;
.source "VoicePrompt.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/settings/VoicePrompt$ConfirmationDelegate;,
        Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;
    }
.end annotation


# instance fields
.field private confirmationDelegate:Lcom/vlingo/core/internal/settings/VoicePrompt$ConfirmationDelegate;

.field private listeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private manually:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/settings/VoicePrompt;->manually:Z

    .line 191
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/settings/VoicePrompt;->listeners:Ljava/util/Set;

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/settings/VoicePrompt;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/settings/VoicePrompt;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->notifyListeners(Z)V

    return-void
.end method

.method private confirmSetting(Z)V
    .locals 2
    .param p1, "setting"    # Z

    .prologue
    .line 133
    new-instance v0, Lcom/vlingo/core/internal/settings/VoicePrompt$1;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/settings/VoicePrompt$1;-><init>(Lcom/vlingo/core/internal/settings/VoicePrompt;Z)V

    .line 141
    .local v0, "onConfirm":Ljava/lang/Runnable;
    invoke-direct {p0}, Lcom/vlingo/core/internal/settings/VoicePrompt;->shouldAsk()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/vlingo/core/internal/settings/VoicePrompt;->manually:Z

    if-eqz v1, :cond_0

    .line 142
    invoke-direct {p0}, Lcom/vlingo/core/internal/settings/VoicePrompt;->getConfirmationDelegate()Lcom/vlingo/core/internal/settings/VoicePrompt$ConfirmationDelegate;

    move-result-object v1

    invoke-interface {v1, p0, v0}, Lcom/vlingo/core/internal/settings/VoicePrompt$ConfirmationDelegate;->confirm(Lcom/vlingo/core/internal/settings/VoicePrompt;Ljava/lang/Runnable;)V

    .line 145
    :goto_0
    return-void

    .line 144
    :cond_0
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method private getConfirmationDelegate()Lcom/vlingo/core/internal/settings/VoicePrompt$ConfirmationDelegate;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/VoicePrompt;->confirmationDelegate:Lcom/vlingo/core/internal/settings/VoicePrompt$ConfirmationDelegate;

    if-nez v0, :cond_0

    new-instance v0, Lcom/vlingo/core/internal/settings/VoicePrompt$2;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/settings/VoicePrompt$2;-><init>(Lcom/vlingo/core/internal/settings/VoicePrompt;)V

    :goto_0
    iput-object v0, p0, Lcom/vlingo/core/internal/settings/VoicePrompt;->confirmationDelegate:Lcom/vlingo/core/internal/settings/VoicePrompt$ConfirmationDelegate;

    .line 175
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/VoicePrompt;->confirmationDelegate:Lcom/vlingo/core/internal/settings/VoicePrompt$ConfirmationDelegate;

    return-object v0

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/VoicePrompt;->confirmationDelegate:Lcom/vlingo/core/internal/settings/VoicePrompt$ConfirmationDelegate;

    goto :goto_0
.end method

.method public static isEnabled()Z
    .locals 1

    .prologue
    .line 61
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isOn()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isAppCarModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isOn()Z
    .locals 2

    .prologue
    .line 70
    const-string/jumbo v0, "use_voice_prompt"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private notifyListeners(Z)V
    .locals 3
    .param p1, "setting"    # Z

    .prologue
    .line 180
    iget-object v2, p0, Lcom/vlingo/core/internal/settings/VoicePrompt;->listeners:Ljava/util/Set;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/vlingo/core/internal/settings/VoicePrompt;->listeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    if-eqz v2, :cond_1

    .line 181
    iget-object v2, p0, Lcom/vlingo/core/internal/settings/VoicePrompt;->listeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    .line 182
    .local v1, "listener":Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;
    if-eqz v1, :cond_0

    .line 183
    invoke-interface {v1, p1}, Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;->onChanged(Z)V

    goto :goto_0

    .line 187
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;
    :cond_1
    return-void
.end method

.method public static shouldAsk(Z)V
    .locals 1
    .param p0, "setting"    # Z

    .prologue
    .line 160
    const-string/jumbo v0, "use_voice_prompt_confirm_with_user"

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 161
    return-void
.end method

.method private shouldAsk()Z
    .locals 2

    .prologue
    .line 155
    const-string/jumbo v0, "use_voice_prompt_confirm_with_user"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public addListener(Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;)Lcom/vlingo/core/internal/settings/VoicePrompt;
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    .prologue
    .line 89
    if-eqz p1, :cond_0

    .line 90
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/VoicePrompt;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 91
    :cond_0
    return-object p0
.end method

.method public isManually()Z
    .locals 1

    .prologue
    .line 195
    iget-boolean v0, p0, Lcom/vlingo/core/internal/settings/VoicePrompt;->manually:Z

    return v0
.end method

.method public off()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/settings/VoicePrompt;->confirmSetting(Z)V

    return-void
.end method

.method public on()V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/settings/VoicePrompt;->confirmSetting(Z)V

    return-void
.end method

.method public registerDelegate(Lcom/vlingo/core/internal/settings/VoicePrompt$ConfirmationDelegate;)Lcom/vlingo/core/internal/settings/VoicePrompt;
    .locals 0
    .param p1, "delegate"    # Lcom/vlingo/core/internal/settings/VoicePrompt$ConfirmationDelegate;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/vlingo/core/internal/settings/VoicePrompt;->confirmationDelegate:Lcom/vlingo/core/internal/settings/VoicePrompt$ConfirmationDelegate;

    .line 111
    return-object p0
.end method

.method public removeListener(Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;)Lcom/vlingo/core/internal/settings/VoicePrompt;
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/VoicePrompt;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 98
    return-object p0
.end method

.method public setManually(Z)V
    .locals 0
    .param p1, "manually"    # Z

    .prologue
    .line 199
    iput-boolean p1, p0, Lcom/vlingo/core/internal/settings/VoicePrompt;->manually:Z

    .line 200
    return-void
.end method

.method public unregisterDelegate(Lcom/vlingo/core/internal/settings/VoicePrompt$ConfirmationDelegate;)Lcom/vlingo/core/internal/settings/VoicePrompt;
    .locals 1
    .param p1, "delegate"    # Lcom/vlingo/core/internal/settings/VoicePrompt$ConfirmationDelegate;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/VoicePrompt;->confirmationDelegate:Lcom/vlingo/core/internal/settings/VoicePrompt$ConfirmationDelegate;

    if-ne v0, p1, :cond_0

    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/settings/VoicePrompt;->confirmationDelegate:Lcom/vlingo/core/internal/settings/VoicePrompt$ConfirmationDelegate;

    .line 118
    :cond_0
    return-object p0
.end method
