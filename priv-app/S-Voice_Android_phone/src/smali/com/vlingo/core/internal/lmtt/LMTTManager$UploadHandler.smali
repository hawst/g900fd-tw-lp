.class final Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;
.super Landroid/os/Handler;
.source "LMTTManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/lmtt/LMTTManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "UploadHandler"
.end annotation


# static fields
.field private static final HANDLE_END_TASK:I = 0x3

.field private static final HANDLE_NEW_TASK:I = 0x1

.field private static final MAKE_TASK_READY:I = 0x2

.field private static final PARAM_REQUEST_ID:Ljava/lang/String; = "request_id"

.field private static final PARAM_SKIP_DELAY:Ljava/lang/String; = "skip_delay"

.field private static final WAIT_TIME_MS:J = 0x3a98L


# instance fields
.field private mLastHandledRequestId:I

.field private mListener:Lcom/vlingo/core/internal/lmtt/LMTTManager$Listener;

.field private mReadyList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;",
            ">;"
        }
    .end annotation
.end field

.field private mRunningTask:Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;

.field private mWaitingList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/vlingo/core/internal/lmtt/LMTTManager;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/lmtt/LMTTManager;Landroid/os/Looper;Lcom/vlingo/core/internal/lmtt/LMTTManager$Listener;)V
    .locals 1
    .param p2, "looper"    # Landroid/os/Looper;
    .param p3, "listener"    # Lcom/vlingo/core/internal/lmtt/LMTTManager$Listener;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    .line 148
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->mWaitingList:Ljava/util/List;

    .line 143
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->mReadyList:Ljava/util/List;

    .line 149
    iput-object p3, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->mListener:Lcom/vlingo/core/internal/lmtt/LMTTManager$Listener;

    .line 150
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/lmtt/LMTTManager;Landroid/os/Looper;Lcom/vlingo/core/internal/lmtt/LMTTManager$Listener;Lcom/vlingo/core/internal/lmtt/LMTTManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/lmtt/LMTTManager;
    .param p2, "x1"    # Landroid/os/Looper;
    .param p3, "x2"    # Lcom/vlingo/core/internal/lmtt/LMTTManager$Listener;
    .param p4, "x3"    # Lcom/vlingo/core/internal/lmtt/LMTTManager$1;

    .prologue
    .line 130
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;-><init>(Lcom/vlingo/core/internal/lmtt/LMTTManager;Landroid/os/Looper;Lcom/vlingo/core/internal/lmtt/LMTTManager$Listener;)V

    return-void
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;)Lcom/vlingo/core/internal/lmtt/LMTTManager$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;

    .prologue
    .line 130
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->mListener:Lcom/vlingo/core/internal/lmtt/LMTTManager$Listener;

    return-object v0
.end method

.method private findTaskInList(Ljava/util/List;Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;)Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;
    .locals 3
    .param p2, "updateType"    # Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;",
            ">;",
            "Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;",
            ")",
            "Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;"
        }
    .end annotation

    .prologue
    .line 274
    .local p1, "taskList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;

    .line 275
    .local v1, "task":Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;
    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;
    invoke-static {v1}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->access$200(Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;)Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    move-result-object v2

    if-ne v2, p2, :cond_0

    .line 279
    .end local v1    # "task":Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private handleNewTask(Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;ZI)V
    .locals 6
    .param p1, "newTask"    # Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;
    .param p2, "skipDelay"    # Z
    .param p3, "requestId"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 178
    iput p3, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->mLastHandledRequestId:I

    .line 180
    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->mReadyList:Ljava/util/List;

    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;
    invoke-static {p1}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->access$200(Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;)Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->findTaskInList(Ljava/util/List;Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;)Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;

    move-result-object v0

    .line 181
    .local v0, "existingTask":Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;
    if-eqz v0, :cond_1

    .line 186
    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mClearLMTT:Z
    invoke-static {p1}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->access$300(Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 190
    # setter for: Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mClearLMTT:Z
    invoke-static {v0, v4}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->access$302(Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;Z)Z

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 193
    :cond_1
    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->mWaitingList:Ljava/util/List;

    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;
    invoke-static {p1}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->access$200(Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;)Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->findTaskInList(Ljava/util/List;Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;)Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;

    move-result-object v0

    .line 194
    if-eqz v0, :cond_2

    .line 199
    invoke-virtual {p0, v5, v0}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->removeMessages(ILjava/lang/Object;)V

    .line 200
    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->mWaitingList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 201
    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mClearLMTT:Z
    invoke-static {v0}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->access$300(Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 204
    # setter for: Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mClearLMTT:Z
    invoke-static {p1, v4}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->access$302(Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;Z)Z

    .line 211
    :cond_2
    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->mWaitingList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    invoke-virtual {p0, v5, p1}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 213
    .local v1, "msg":Landroid/os/Message;
    if-eqz p2, :cond_3

    const-wide/16 v2, 0x0

    :goto_1
    invoke-virtual {p0, v1, v2, v3}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_3
    const-wide/16 v2, 0x3a98

    goto :goto_1
.end method

.method private isLmttEnabledForType(Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;)Z
    .locals 3
    .param p1, "updateType"    # Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    .prologue
    .line 283
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "lmtt.enable."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->getTypeString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 284
    .local v0, "settingKey":Ljava/lang/String;
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method private makeReady(Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;)V
    .locals 1
    .param p1, "waitingTask"    # Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;

    .prologue
    .line 226
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->mWaitingList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->mReadyList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->runNextTask()V

    .line 230
    :cond_0
    return-void
.end method

.method private notifyComplete()V
    .locals 2

    .prologue
    .line 263
    iget v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->mLastHandledRequestId:I

    .line 264
    .local v0, "lastHandledRequestId":I
    new-instance v1, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler$1;

    invoke-direct {v1, p0, v0}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler$1;-><init>(Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;I)V

    invoke-static {v1}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 271
    return-void
.end method

.method private runNextTask()V
    .locals 3

    .prologue
    .line 239
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->mRunningTask:Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;

    if-nez v1, :cond_0

    .line 240
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->mReadyList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 241
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->mReadyList:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;

    .line 243
    .local v0, "nextTask":Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;
    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->mLmttUpdateType:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;
    invoke-static {v0}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->access$200(Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;)Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->isLmttEnabledForType(Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 247
    iput-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->mRunningTask:Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;

    .line 248
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->mRunningTask:Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;

    # invokes: Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->runTask()V
    invoke-static {v1}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;->access$400(Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;)V

    .line 260
    .end local v0    # "nextTask":Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;
    :cond_0
    :goto_0
    return-void

    .line 253
    .restart local v0    # "nextTask":Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;
    :cond_1
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->runNextTask()V

    goto :goto_0

    .line 255
    .end local v0    # "nextTask":Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;
    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->mWaitingList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 257
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->notifyComplete()V

    goto :goto_0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 155
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 172
    :goto_0
    return-void

    .line 157
    :pswitch_0
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;

    .line 158
    .local v3, "task":Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 159
    .local v0, "data":Landroid/os/Bundle;
    const-string/jumbo v4, "skip_delay"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 160
    .local v2, "skipDelay":Z
    const-string/jumbo v4, "request_id"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 161
    .local v1, "requestId":I
    invoke-direct {p0, v3, v2, v1}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->handleNewTask(Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;ZI)V

    goto :goto_0

    .line 164
    .end local v0    # "data":Landroid/os/Bundle;
    .end local v1    # "requestId":I
    .end local v2    # "skipDelay":Z
    .end local v3    # "task":Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;
    :pswitch_1
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;

    .line 165
    .restart local v3    # "task":Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;
    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->makeReady(Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;)V

    goto :goto_0

    .line 168
    .end local v3    # "task":Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;
    :pswitch_2
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->mRunningTask:Lcom/vlingo/core/internal/lmtt/LMTTManager$UpdateTask;

    .line 169
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/LMTTManager$UploadHandler;->runNextTask()V

    goto :goto_0

    .line 155
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
