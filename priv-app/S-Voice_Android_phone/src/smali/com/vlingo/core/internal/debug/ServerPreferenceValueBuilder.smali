.class public Lcom/vlingo/core/internal/debug/ServerPreferenceValueBuilder;
.super Landroid/preference/DialogPreference;
.source "ServerPreferenceValueBuilder.java"


# instance fields
.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    return-void
.end method


# virtual methods
.method protected getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/ServerPreferenceValueBuilder;->value:Ljava/lang/String;

    return-object v0
.end method

.method protected setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/vlingo/core/internal/debug/ServerPreferenceValueBuilder;->value:Ljava/lang/String;

    .line 27
    return-void
.end method

.method protected updateSpinner()V
    .locals 0

    .prologue
    .line 19
    return-void
.end method
