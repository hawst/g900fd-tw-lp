.class Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask$1;
.super Landroid/os/Handler;
.source "BluetoothManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;)V
    .locals 0

    .prologue
    .line 1062
    iput-object p1, p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask$1;->this$0:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x0

    const-wide/16 v7, 0x7d0

    const/4 v6, 0x1

    .line 1065
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 1151
    :cond_0
    :goto_0
    return-void

    .line 1067
    :pswitch_0
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$100()Landroid/bluetooth/BluetoothHeadset;

    move-result-object v3

    if-nez v3, :cond_2

    .line 1068
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$700()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "[SVoiceBT, LatencyCheck] BT ScoWorkerTask:handleMessage() mBluetoothHeadset is NULL"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1070
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mOnScoConnectedTask:Ljava/lang/Runnable;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$800()Ljava/lang/Runnable;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1073
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$300()Ljava/util/TimerTask;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1074
    # invokes: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->cancelScoTimeoutTask()V
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$900()V

    .line 1076
    :cond_1
    new-instance v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoTimeoutTask;

    invoke-direct {v3, v6, v5}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoTimeoutTask;-><init>(ZZ)V

    # setter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;
    invoke-static {v3}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$302(Ljava/util/TimerTask;)Ljava/util/TimerTask;

    .line 1078
    const/4 v2, 0x0

    .line 1079
    .local v2, "timer":Ljava/util/Timer;
    invoke-static {}, Lcom/vlingo/core/internal/util/TimerSingleton;->getTimer()Ljava/util/Timer;

    move-result-object v2

    .line 1080
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$300()Ljava/util/TimerTask;

    move-result-object v3

    invoke-virtual {v2, v3, v7, v8}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0

    .line 1087
    .end local v2    # "timer":Ljava/util/Timer;
    :cond_2
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$200()Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    if-nez v3, :cond_4

    .line 1088
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->getBTdevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    # setter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v3}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$202(Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    .line 1089
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$200()Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    if-nez v3, :cond_5

    .line 1092
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mOnScoConnectedTask:Ljava/lang/Runnable;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$800()Ljava/lang/Runnable;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1097
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$300()Ljava/util/TimerTask;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 1098
    # invokes: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->cancelScoTimeoutTask()V
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$900()V

    .line 1100
    :cond_3
    new-instance v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoTimeoutTask;

    invoke-direct {v3, v6, v5}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoTimeoutTask;-><init>(ZZ)V

    # setter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;
    invoke-static {v3}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$302(Ljava/util/TimerTask;)Ljava/util/TimerTask;

    .line 1102
    const/4 v2, 0x0

    .line 1103
    .restart local v2    # "timer":Ljava/util/Timer;
    invoke-static {}, Lcom/vlingo/core/internal/util/TimerSingleton;->getTimer()Ljava/util/Timer;

    move-result-object v2

    .line 1104
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$300()Ljava/util/TimerTask;

    move-result-object v3

    invoke-virtual {v2, v3, v7, v8}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0

    .line 1110
    .end local v2    # "timer":Ljava/util/Timer;
    :cond_4
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$700()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "[SVoiceBT] mBluetoothDevice\'s deviceName:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$200()Landroid/bluetooth/BluetoothDevice;

    move-result-object v5

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", deviceType:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$200()Landroid/bluetooth/BluetoothDevice;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->getSamsungHandsfreeDeviceType(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1113
    :cond_5
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$300()Ljava/util/TimerTask;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 1114
    # invokes: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->cancelScoTimeoutTask()V
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$900()V

    .line 1117
    :cond_6
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v3

    if-nez v3, :cond_9

    .line 1119
    invoke-static {v6}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->enableWideBandSpeech(Z)V

    .line 1120
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$100()Landroid/bluetooth/BluetoothHeadset;

    move-result-object v3

    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$200()Landroid/bluetooth/BluetoothDevice;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/bluetooth/BluetoothHeadset;->startVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    .line 1121
    .local v0, "result":Z
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$700()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "[SVoiceBT, LatencyCheck] BT ScoWorkerTask:handleMessage() startVoiceRecognition() result = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1122
    if-nez v0, :cond_8

    .line 1125
    const/4 v1, 0x1

    .line 1126
    .local v1, "retry":Z
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v4, "sco_retry_flag"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1127
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v4, "sco_retry_flag"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 1129
    :cond_7
    new-instance v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoTimeoutTask;

    invoke-direct {v3, v1, v6}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoTimeoutTask;-><init>(ZZ)V

    # setter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;
    invoke-static {v3}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$302(Ljava/util/TimerTask;)Ljava/util/TimerTask;

    .line 1131
    const/4 v2, 0x0

    .line 1132
    .restart local v2    # "timer":Ljava/util/Timer;
    invoke-static {}, Lcom/vlingo/core/internal/util/TimerSingleton;->getTimer()Ljava/util/Timer;

    move-result-object v2

    .line 1133
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$300()Ljava/util/TimerTask;

    move-result-object v3

    invoke-virtual {v2, v3, v7, v8}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto/16 :goto_0

    .line 1138
    .end local v1    # "retry":Z
    .end local v2    # "timer":Ljava/util/Timer;
    :cond_8
    # setter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mIsStartSCOinProgress:Z
    invoke-static {v6}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$1002(Z)Z

    .line 1139
    sget-object v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;->BT_BUTTON:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    # setter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mAppCloseType:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;
    invoke-static {v3}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$1102(Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;)Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    goto/16 :goto_0

    .line 1143
    .end local v0    # "result":Z
    :cond_9
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$700()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "[SVoiceBT, LatencyCheck] BT ScoWorkerTask:handleMessage() Audio already ON - startVoiceRecognition() will NOT be attempted"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1144
    # invokes: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->notifyScoStateToListeners(Z)V
    invoke-static {v6}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$1200(Z)V

    .line 1145
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$300()Ljava/util/TimerTask;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1146
    # invokes: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->cancelScoTimeoutTask()V
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$900()V

    goto/16 :goto_0

    .line 1065
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
