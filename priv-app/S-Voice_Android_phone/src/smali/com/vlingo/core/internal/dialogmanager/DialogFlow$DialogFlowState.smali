.class public final enum Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;
.super Ljava/lang/Enum;
.source "DialogFlow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DialogFlowState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

.field public static final enum BUSY:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

.field public static final enum IDLE:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 155
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    const-string/jumbo v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->IDLE:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    const-string/jumbo v1, "BUSY"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->BUSY:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->IDLE:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->BUSY:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 155
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 155
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;
    .locals 1

    .prologue
    .line 155
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    return-object v0
.end method
