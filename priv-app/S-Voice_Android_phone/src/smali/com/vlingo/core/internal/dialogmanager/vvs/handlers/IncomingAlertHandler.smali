.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/IncomingAlertHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "IncomingAlertHandler.java"


# instance fields
.field alert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

.field alerts:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;"
        }
    .end annotation
.end field

.field private isSilentMode:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0
    .param p1, "isSilentMode"    # Z

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 28
    iput-boolean p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/IncomingAlertHandler;->isSilentMode:Z

    .line 29
    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 6
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 39
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 40
    .local v3, "messageAlerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/IncomingAlertHandler;->alerts:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 41
    .local v0, "alertItem":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 42
    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .end local v0    # "alertItem":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 46
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->shouldIncomingMessagesReadout()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 47
    const-class v4, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;

    invoke-virtual {p0, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/IncomingAlertHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v1

    .local v1, "controller":Lcom/vlingo/core/internal/dialogmanager/Controller;
    move-object v4, v1

    .line 48
    check-cast v4, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/IncomingAlertHandler;->isSilentMode()Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->setSilentMode(Z)V

    move-object v4, v1

    .line 49
    check-cast v4, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->createSMSMMSSenderQueueMap(Ljava/util/LinkedList;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->setSenderQueue(Ljava/util/HashMap;)V

    .line 50
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ACTIVE_CONTROLLER:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v4, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 51
    invoke-virtual {v1, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/Controller;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 53
    .end local v1    # "controller":Lcom/vlingo/core/internal/dialogmanager/Controller;
    :cond_2
    const/4 v4, 0x0

    return v4
.end method

.method public init(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)V
    .locals 0
    .param p1, "safeReaderAlert"    # Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/IncomingAlertHandler;->alert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 62
    return-void
.end method

.method public init(Ljava/util/Queue;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57
    .local p1, "alertQueue":Ljava/util/Queue;, "Ljava/util/Queue<+Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/IncomingAlertHandler;->alerts:Ljava/util/Queue;

    .line 58
    return-void
.end method

.method public isSilentMode()Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/IncomingAlertHandler;->isSilentMode:Z

    return v0
.end method

.method public setSilentMode(Z)V
    .locals 0
    .param p1, "silentMode"    # Z

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/IncomingAlertHandler;->isSilentMode:Z

    .line 70
    return-void
.end method
