.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;
.super Ljava/lang/Object;
.source "PhraseSpotterControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AudioLoggerWrapper"
.end annotation


# instance fields
.field private logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dumpToFile()V
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;->hasTarget()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;->target()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->dumpToFile()V

    :cond_0
    return-void
.end method

.method public hasTarget()Z
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;->logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public markP()V
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;->hasTarget()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;->target()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->markP()V

    :cond_0
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;->logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    .line 103
    return-void
.end method

.method public setLogger(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;)V
    .locals 0
    .param p1, "logger"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;->logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    .line 109
    return-void
.end method

.method public setScore(F)V
    .locals 1
    .param p1, "score"    # F

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;->hasTarget()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;->target()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->setScore(F)V

    :cond_0
    return-void
.end method

.method public target()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;->logger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    return-object v0
.end method

.method public writeData([SII)V
    .locals 1
    .param p1, "data"    # [S
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;->hasTarget()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;->target()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->writeData([SII)V

    :cond_0
    return-void
.end method
