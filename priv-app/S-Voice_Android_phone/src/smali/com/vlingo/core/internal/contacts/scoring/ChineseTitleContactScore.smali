.class public Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;
.super Lcom/vlingo/core/internal/contacts/scoring/ContactScore;
.source "ChineseTitleContactScore.java"


# instance fields
.field private baseScore:I

.field private regexQuery:Ljava/lang/String;

.field private samePhonetics:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "baseScore"    # I
    .param p2, "regexQuery"    # Ljava/lang/String;
    .param p3, "samePhonetics"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/scoring/ContactScore;-><init>()V

    .line 17
    iput p1, p0, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;->baseScore:I

    .line 18
    iput-object p2, p0, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;->regexQuery:Ljava/lang/String;

    .line 19
    iput-object p3, p0, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;->samePhonetics:Ljava/lang/String;

    .line 20
    return-void
.end method


# virtual methods
.method public getScore(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactMatch;)I
    .locals 12
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "match"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    .line 24
    const/4 v6, 0x0

    .line 25
    .local v6, "score":I
    iget-object v8, p0, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;->regexQuery:Ljava/lang/String;

    invoke-static {v8}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v8

    iget-object v9, p2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 26
    .local v1, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 27
    invoke-virtual {v1, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    .line 28
    .local v2, "name":Ljava/lang/String;
    move-object v4, v2

    .line 29
    .local v4, "nameWithoutTitle":Ljava/lang/String;
    const-string/jumbo v7, ""

    .line 31
    .local v7, "title":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v8

    if-le v8, v10, :cond_0

    .line 32
    invoke-virtual {v1, v11}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_2

    const-string/jumbo v7, ""

    .line 33
    :goto_0
    iget-object v8, p2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    const/4 v9, 0x0

    iget-object v10, p2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v11

    sub-int/2addr v10, v11

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 35
    :cond_0
    invoke-virtual {v4, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    invoke-virtual {v4, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 37
    const/4 v6, 0x0

    .line 62
    .end local v2    # "name":Ljava/lang/String;
    .end local v4    # "nameWithoutTitle":Ljava/lang/String;
    .end local v7    # "title":Ljava/lang/String;
    :cond_1
    :goto_1
    return v6

    .line 32
    .restart local v2    # "name":Ljava/lang/String;
    .restart local v4    # "nameWithoutTitle":Ljava/lang/String;
    .restart local v7    # "title":Ljava/lang/String;
    :cond_2
    invoke-virtual {v1, v11}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 39
    :cond_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, ".*"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;->samePhonetics:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "$"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 40
    .local v3, "nameMatcher":Ljava/util/regex/Matcher;
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    .line 43
    .local v0, "firstName":Z
    if-eqz v0, :cond_5

    iget v6, p0, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;->baseScore:I

    .line 44
    :goto_2
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 45
    add-int/lit8 v6, v6, -0x1

    .line 48
    :cond_4
    iget-object v8, p2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v9

    sub-int/2addr v8, v9

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    sub-int v5, v8, v9

    .line 49
    .local v5, "prefixLength":I
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_6

    .line 50
    add-int/lit8 v6, v6, -0xa

    .line 51
    mul-int/lit8 v8, v5, 0xa

    sub-int/2addr v6, v8

    .line 52
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    mul-int/lit8 v8, v8, 0x2

    sub-int/2addr v6, v8

    goto :goto_1

    .line 43
    .end local v5    # "prefixLength":I
    :cond_5
    iget v8, p0, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;->baseScore:I

    add-int/lit8 v6, v8, -0x32

    goto :goto_2

    .line 54
    .restart local v5    # "prefixLength":I
    :cond_6
    mul-int/lit8 v8, v5, 0x2

    sub-int/2addr v6, v8

    goto :goto_1
.end method
