.class public final enum Lcom/vlingo/core/internal/dialogmanager/FieldIds;
.super Ljava/lang/Enum;
.source "FieldIds.java"

# interfaces
.implements Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/dialogmanager/FieldIds;",
        ">;",
        "Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DEFAULT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_ALARMADDMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_ALARMADDTIME:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_ALARMDELETECHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_ALARMDELETEMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_ALARMEDITCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_ALARMEDITMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_ALARMLOOKUPMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_DIAL_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_DIAL_MAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_EVENTADDINVITEE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_EVENTADDINVITEETYPE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_EVENTADDMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_EVENTADDTIME:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_EVENTDELETECHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_EVENTDELETEMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_EVENTEDITCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_EVENTEDITINVITEE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_EVENTEDITINVITEETYPE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_EVENTEDITMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_EVENTLOOKUPMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_MAIN_SMS:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_SMS_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_SMS_MSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_SMS_MSG_APPEND:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_SMS_MSG_REWRITE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_SMS_TYPE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_TASKADDMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_TASKADDTITLE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_TASKDELETECHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_TASKDELETEMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_TASKEDITCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_TASKEDITMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum DM_TASKLOOKUPMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_CONFIRM:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_CONTACTLOOKUP:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_CONTACTLOOKUP_CHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_DIAL_AUTODIAL:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_DIAL_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_DIAL_TYPE_NUM:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_EVENT_TITLE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_MAIN_EVENT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_MAIN_LAUNCHAPP:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_MAIN_MEMO:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_MAIN_NAV:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_MAIN_SMS:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_MEMO_DELETECHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_MEMO_DELETEPROMPT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_MEMO_LOOKUPCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_MEMO_MSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_MUSIC_PLAYALBUMCHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_MUSIC_PLAYARTISTCHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_MUSIC_PLAYLISTCHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_MUSIC_PLAYTITLECHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_READBACK_MSGCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_READBACK_SENDERCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_SAFEREADER_NEWMSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_SAFEREADER_POSTMSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_SAFEREADER_READBACKMSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_SMS_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_SMS_MSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_SMS_TYPE_NUM:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_SOCIAL_CHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_SOCIAL_STATUS:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_SOCIAL_STATUS_MSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_CAR_TASK_TITLE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

.field public static final enum VP_MAIN_WSEARCH_PROMPT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;


# instance fields
.field private value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 12
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DEFAULT"

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getDefaultFieldId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v4, v2}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 13
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_CONFIRM"

    const-string/jumbo v2, "vp_car_confirm"

    invoke-direct {v0, v1, v5, v2}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_CONFIRM:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 14
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_CONTACTLOOKUP"

    const-string/jumbo v2, "vp_car_contactlookup"

    invoke-direct {v0, v1, v6, v2}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_CONTACTLOOKUP:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 15
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_MAIN_NAV"

    const-string/jumbo v2, "vp_car_main_nav"

    invoke-direct {v0, v1, v7, v2}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MAIN_NAV:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 16
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_CONTACTLOOKUP_CHOOSE"

    const-string/jumbo v2, "vp_car_contactlookup_choose"

    invoke-direct {v0, v1, v8, v2}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_CONTACTLOOKUP_CHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 17
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_MAIN_LAUNCHAPP"

    const/4 v2, 0x5

    const-string/jumbo v3, "vp_car_main_launchapp"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MAIN_LAUNCHAPP:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 18
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_MUSIC_PLAYALBUMCHOICE"

    const/4 v2, 0x6

    const-string/jumbo v3, "vp_car_music_playalbumchoice"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYALBUMCHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 19
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_MUSIC_PLAYARTISTCHOICE"

    const/4 v2, 0x7

    const-string/jumbo v3, "vp_car_music_playartistchoice"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYARTISTCHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 20
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_MUSIC_PLAYTITLECHOICE"

    const/16 v2, 0x8

    const-string/jumbo v3, "vp_car_music_playtitlechoice"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYTITLECHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 21
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_MUSIC_PLAYLISTCHOICE"

    const/16 v2, 0x9

    const-string/jumbo v3, "vp_car_music_playlistchoice"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYLISTCHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 22
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_SOCIAL_CHOICE"

    const/16 v2, 0xa

    const-string/jumbo v3, "vp_car_social_choice"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SOCIAL_CHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 23
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_SOCIAL_STATUS"

    const/16 v2, 0xb

    const-string/jumbo v3, "vp_car_social_status"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SOCIAL_STATUS:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 24
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_SOCIAL_STATUS_MSG"

    const/16 v2, 0xc

    const-string/jumbo v3, "vp_car_social_status_msg"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SOCIAL_STATUS_MSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 25
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_MEMO_DELETEPROMPT"

    const/16 v2, 0xd

    const-string/jumbo v3, "vp_car_memo_deleteprompt"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MEMO_DELETEPROMPT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 26
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_SAFEREADER_NEWMSG"

    const/16 v2, 0xe

    const-string/jumbo v3, "vp_car_safereader_newmsg"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SAFEREADER_NEWMSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 27
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_SAFEREADER_POSTMSG"

    const/16 v2, 0xf

    const-string/jumbo v3, "vp_car_safereader_postmsg"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SAFEREADER_POSTMSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 28
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_READBACK_SENDERCHOOSE"

    const/16 v2, 0x10

    const-string/jumbo v3, "vp_car_readback_senderchoose"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_READBACK_SENDERCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 29
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_READBACK_MSGCHOOSE"

    const/16 v2, 0x11

    const-string/jumbo v3, "vp_car_readback_msgchoose"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_READBACK_MSGCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 30
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_SAFEREADER_READBACKMSG"

    const/16 v2, 0x12

    const-string/jumbo v3, "vp_car_safereader_readbackmsg"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SAFEREADER_READBACKMSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 31
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_DIAL_AUTODIAL"

    const/16 v2, 0x13

    const-string/jumbo v3, "vp_car_dial_autodial"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_DIAL_AUTODIAL:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 32
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_MAIN_SMS"

    const/16 v2, 0x14

    const-string/jumbo v3, "vp_car_main_sms"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MAIN_SMS:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 33
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_SMS_MSG"

    const/16 v2, 0x15

    const-string/jumbo v3, "vp_car_sms_msg"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SMS_MSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 34
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_SMS_CONTACT"

    const/16 v2, 0x16

    const-string/jumbo v3, "vp_car_sms_contact"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SMS_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 35
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_SMS_TYPE_NUM"

    const/16 v2, 0x17

    const-string/jumbo v3, "vp_car_sms_type_num"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SMS_TYPE_NUM:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 36
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_DIAL_CONTACT"

    const/16 v2, 0x18

    const-string/jumbo v3, "vp_car_dial_contact"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_DIAL_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 37
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_DIAL_TYPE_NUM"

    const/16 v2, 0x19

    const-string/jumbo v3, "vp_car_dial_type_num"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_DIAL_TYPE_NUM:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 38
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_MEMO_MSG"

    const/16 v2, 0x1a

    const-string/jumbo v3, "vp_car_memo_msg"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MEMO_MSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 39
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_MAIN_MEMO"

    const/16 v2, 0x1b

    const-string/jumbo v3, "vp_car_main_memo"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MAIN_MEMO:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 40
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_MAIN_WSEARCH_PROMPT"

    const/16 v2, 0x1c

    const-string/jumbo v3, "vp_main_wsearch_prompt"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_MAIN_WSEARCH_PROMPT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 41
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_MAIN_SMS"

    const/16 v2, 0x1d

    const-string/jumbo v3, "dm_main_sms"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_MAIN_SMS:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 42
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_EVENTADDMAIN"

    const/16 v2, 0x1e

    const-string/jumbo v3, "dm_eventaddmain"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTADDMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 43
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_TASK_TITLE"

    const/16 v2, 0x1f

    const-string/jumbo v3, "vp_car_task_title"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_TASK_TITLE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 44
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_MEMO_DELETECHOOSE"

    const/16 v2, 0x20

    const-string/jumbo v3, "vp_car_memo_deletechoose"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MEMO_DELETECHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 45
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_MEMO_LOOKUPCHOOSE"

    const/16 v2, 0x21

    const-string/jumbo v3, "vp_car_memo_lookupchoose"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MEMO_LOOKUPCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 46
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_MAIN_EVENT"

    const/16 v2, 0x22

    const-string/jumbo v3, "vp_car_main_event"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MAIN_EVENT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 47
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "VP_CAR_EVENT_TITLE"

    const/16 v2, 0x23

    const-string/jumbo v3, "vp_car_event_title"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_EVENT_TITLE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 48
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_ALARMADDTIME"

    const/16 v2, 0x24

    const-string/jumbo v3, "dm_alarmaddtime"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_ALARMADDTIME:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 49
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_SMS_CONTACT"

    const/16 v2, 0x25

    const-string/jumbo v3, "dm_sms_contact"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_SMS_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 50
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_ALARMADDMAIN"

    const/16 v2, 0x26

    const-string/jumbo v3, "dm_alarmaddmain"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_ALARMADDMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 51
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_ALARMDELETECHOOSE"

    const/16 v2, 0x27

    const-string/jumbo v3, "dm_alarmdeletechoose"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_ALARMDELETECHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 52
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_ALARMDELETEMAIN"

    const/16 v2, 0x28

    const-string/jumbo v3, "dm_alarmdeletemain"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_ALARMDELETEMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 53
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_ALARMEDITCHOOSE"

    const/16 v2, 0x29

    const-string/jumbo v3, "dm_alarmeditchoose"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_ALARMEDITCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 54
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_ALARMEDITMAIN"

    const/16 v2, 0x2a

    const-string/jumbo v3, "dm_alarmeditmain"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_ALARMEDITMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 55
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_ALARMLOOKUPMAIN"

    const/16 v2, 0x2b

    const-string/jumbo v3, "dm_alarmlookupmain"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_ALARMLOOKUPMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 56
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_EVENTADDINVITEE"

    const/16 v2, 0x2c

    const-string/jumbo v3, "dm_eventaddinvitee"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTADDINVITEE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 57
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_EVENTADDINVITEETYPE"

    const/16 v2, 0x2d

    const-string/jumbo v3, "dm_eventaddinviteetype"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTADDINVITEETYPE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 58
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_EVENTADDTIME"

    const/16 v2, 0x2e

    const-string/jumbo v3, "dm_eventaddtime"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTADDTIME:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 59
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_EVENTDELETECHOOSE"

    const/16 v2, 0x2f

    const-string/jumbo v3, "dm_eventdeletechoose"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTDELETECHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 60
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_EVENTDELETEMAIN"

    const/16 v2, 0x30

    const-string/jumbo v3, "dm_eventdeletemain"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTDELETEMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 61
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_EVENTEDITCHOOSE"

    const/16 v2, 0x31

    const-string/jumbo v3, "dm_eventeditchoose"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTEDITCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 62
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_EVENTEDITINVITEE"

    const/16 v2, 0x32

    const-string/jumbo v3, "dm_eventeditinvitee"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTEDITINVITEE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 63
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_EVENTEDITINVITEETYPE"

    const/16 v2, 0x33

    const-string/jumbo v3, "dm_eventeditinviteetype"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTEDITINVITEETYPE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 64
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_EVENTEDITMAIN"

    const/16 v2, 0x34

    const-string/jumbo v3, "dm_eventeditmain"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTEDITMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 65
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_EVENTLOOKUPMAIN"

    const/16 v2, 0x35

    const-string/jumbo v3, "dm_eventlookupmain"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTLOOKUPMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 66
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_SMS_TYPE"

    const/16 v2, 0x36

    const-string/jumbo v3, "dm_sms_type"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_SMS_TYPE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 67
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_TASKADDMAIN"

    const/16 v2, 0x37

    const-string/jumbo v3, "dm_taskaddmain"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_TASKADDMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 68
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_TASKADDTITLE"

    const/16 v2, 0x38

    const-string/jumbo v3, "dm_taskaddtitle"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_TASKADDTITLE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 69
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_TASKDELETECHOOSE"

    const/16 v2, 0x39

    const-string/jumbo v3, "dm_taskdeletechoose"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_TASKDELETECHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 70
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_TASKDELETEMAIN"

    const/16 v2, 0x3a

    const-string/jumbo v3, "dm_taskdeletemain"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_TASKDELETEMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 71
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_TASKEDITCHOOSE"

    const/16 v2, 0x3b

    const-string/jumbo v3, "dm_taskeditchoose"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_TASKEDITCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 72
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_TASKEDITMAIN"

    const/16 v2, 0x3c

    const-string/jumbo v3, "dm_taskeditmain"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_TASKEDITMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 73
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_TASKLOOKUPMAIN"

    const/16 v2, 0x3d

    const-string/jumbo v3, "dm_tasklookupmain"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_TASKLOOKUPMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 74
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_SMS_MSG"

    const/16 v2, 0x3e

    const-string/jumbo v3, "dm_sms_msg"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_SMS_MSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 75
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_SMS_MSG_APPEND"

    const/16 v2, 0x3f

    const-string/jumbo v3, "dm_sms_msg_append"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_SMS_MSG_APPEND:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 76
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_SMS_MSG_REWRITE"

    const/16 v2, 0x40

    const-string/jumbo v3, "dm_sms_msg_rewrite"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_SMS_MSG_REWRITE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 77
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_DIAL_CONTACT"

    const/16 v2, 0x41

    const-string/jumbo v3, "dm_dial_contact"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_DIAL_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 78
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    const-string/jumbo v1, "DM_DIAL_MAIN"

    const/16 v2, 0x42

    const-string/jumbo v3, "dm_dial_main"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_DIAL_MAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .line 11
    const/16 v0, 0x43

    new-array v0, v0, [Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_CONFIRM:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_CONTACTLOOKUP:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MAIN_NAV:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_CONTACTLOOKUP_CHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MAIN_LAUNCHAPP:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYALBUMCHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYARTISTCHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYTITLECHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYLISTCHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SOCIAL_CHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SOCIAL_STATUS:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SOCIAL_STATUS_MSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MEMO_DELETEPROMPT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SAFEREADER_NEWMSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SAFEREADER_POSTMSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_READBACK_SENDERCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_READBACK_MSGCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SAFEREADER_READBACKMSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_DIAL_AUTODIAL:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MAIN_SMS:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SMS_MSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SMS_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SMS_TYPE_NUM:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_DIAL_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_DIAL_TYPE_NUM:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MEMO_MSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MAIN_MEMO:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_MAIN_WSEARCH_PROMPT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_MAIN_SMS:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTADDMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_TASK_TITLE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MEMO_DELETECHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MEMO_LOOKUPCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MAIN_EVENT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_EVENT_TITLE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_ALARMADDTIME:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_SMS_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_ALARMADDMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_ALARMDELETECHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_ALARMDELETEMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_ALARMEDITCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_ALARMEDITMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_ALARMLOOKUPMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTADDINVITEE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTADDINVITEETYPE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTADDTIME:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTDELETECHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTDELETEMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTEDITCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTEDITINVITEE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTEDITINVITEETYPE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTEDITMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTLOOKUPMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_SMS_TYPE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_TASKADDMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_TASKADDTITLE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_TASKDELETECHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_TASKDELETEMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_TASKEDITCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_TASKEDITMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_TASKLOOKUPMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_SMS_MSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_SMS_MSG_APPEND:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_SMS_MSG_REWRITE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_DIAL_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_DIAL_MAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 81
    iput-object p3, p0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->value:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public static from(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/FieldIds;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 91
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/FieldIds;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 95
    :goto_0
    return-object v0

    .line 92
    :catch_0
    move-exception v0

    .line 95
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    goto :goto_0
.end method

.method public static generateFieldIdsMap()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;",
            "Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 103
    .local v0, "smFieldIds":Ljava/util/Map;, "Ljava/util/Map<Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;>;"
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 104
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->MEDIUM_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    :goto_0
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_CONTACTLOOKUP:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MAIN_NAV:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_CONTACTLOOKUP_CHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MAIN_LAUNCHAPP:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYALBUMCHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYARTISTCHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYTITLECHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYLISTCHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SOCIAL_STATUS:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MEMO_DELETEPROMPT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->SHORT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SAFEREADER_NEWMSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SAFEREADER_POSTMSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->SHORT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 126
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_DIAL_AUTODIAL:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->MEDIUM_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    :goto_1
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MAIN_SMS:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SMS_MSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_SMS_MSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_SMS_MSG_APPEND:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_SMS_MSG_REWRITE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SMS_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_SMS_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SMS_TYPE_NUM:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 144
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_DIAL_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->MEDIUM_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_DIAL_TYPE_NUM:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->MEDIUM_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_DIAL_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->MEDIUM_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_DIAL_MAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->MEDIUM_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    :goto_2
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MEMO_MSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MAIN_MEMO:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_MAIN_SMS:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTADDMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_TASK_TITLE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MEMO_DELETECHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MEMO_LOOKUPCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MAIN_EVENT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_EVENT_TITLE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_ALARMADDTIME:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->SHORT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_ALARMADDMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_ALARMDELETECHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_ALARMDELETEMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_ALARMEDITCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_ALARMEDITMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_ALARMLOOKUPMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTADDINVITEE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTADDINVITEETYPE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTADDTIME:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTDELETECHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTDELETEMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTEDITCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTEDITINVITEE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTEDITINVITEETYPE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTEDITMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_EVENTLOOKUPMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_SMS_TYPE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_TASKADDMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_TASKADDTITLE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_TASKDELETECHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_TASKDELETEMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_TASKEDITCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_TASKEDITMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_TASKLOOKUPMAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    return-object v0

    .line 106
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v1

    if-nez v1, :cond_1

    .line 107
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 109
    :cond_1
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 128
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v1

    if-nez v1, :cond_3

    .line 129
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_DIAL_AUTODIAL:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->SHORT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 131
    :cond_3
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_DIAL_AUTODIAL:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 149
    :cond_4
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v1

    if-nez v1, :cond_5

    .line 150
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_DIAL_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_DIAL_TYPE_NUM:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_DIAL_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_DIAL_MAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 155
    :cond_5
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_DIAL_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_DIAL_TYPE_NUM:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_DIAL_CONTACT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DM_DIAL_MAIN:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->LONG_LONG:Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/FieldIds;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/dialogmanager/FieldIds;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/dialogmanager/FieldIds;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    return-object v0
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->value:Ljava/lang/String;

    return-object v0
.end method
