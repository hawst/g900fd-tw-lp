.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/QueryHandler;
.source "ResolveAppointmentHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler$AppointmentQueryRunnable;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/QueryHandler;-><init>()V

    .line 85
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;
    .param p1, "x1"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->getAppointmentsByQuery(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->getStoredAppointments()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;Ljava/util/List;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # I

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->sendScheduleResolvedEvent(Ljava/util/List;I)V

    return-void
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # Ljava/util/List;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->addQueriedAppointmentsToStore(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method private addQueriedAppointmentsToStore(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "queriedAppointments":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    .local p2, "storedAppointments":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    invoke-interface {p2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 76
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CALENDAR_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v0, v1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 77
    return-void
.end method

.method private clearCacheOnRequest(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V
    .locals 2
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v1, 0x0

    .line 38
    const-string/jumbo v0, "clear.cache"

    invoke-static {p1, v0, v1, v1}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CALENDAR_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const/4 v1, 0x0

    invoke-interface {p2, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 41
    :cond_0
    return-void
.end method

.method private getAppointmentsByQuery(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;
    .locals 4
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    const/4 v2, 0x0

    .line 45
    .local v2, "queriedAppointments":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getParameterNames()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 46
    new-instance v2, Ljava/util/LinkedList;

    .end local v2    # "queriedAppointments":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 47
    .restart local v2    # "queriedAppointments":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getNextScheduleEvent(Landroid/content/Context;)Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    move-result-object v0

    .line 48
    .local v0, "appointment":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    if-eqz v0, :cond_0

    .line 49
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    .end local v0    # "appointment":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    :cond_0
    :goto_0
    return-object v2

    .line 53
    :cond_1
    const/4 v3, 0x0

    invoke-static {p1, v3}, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->extractScheduleQuery(Lcom/vlingo/sdk/recognition/VLAction;Ljava/util/List;)Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;

    move-result-object v1

    .line 54
    .local v1, "criteria":Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getScheduledEvents(Landroid/content/Context;Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;)Ljava/util/List;

    move-result-object v2

    goto :goto_0
.end method

.method private getStoredAppointments()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CALENDAR_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    move-object v0, v1

    check-cast v0, Ljava/util/List;

    .line 63
    .local v0, "storedAppointments":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    if-nez v0, :cond_0

    .line 64
    new-instance v0, Ljava/util/LinkedList;

    .end local v0    # "storedAppointments":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 66
    .restart local v0    # "storedAppointments":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    :cond_0
    return-object v0
.end method

.method private sendScheduleResolvedEvent(Ljava/util/List;I)V
    .locals 3
    .param p2, "offset"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 70
    .local p1, "queriedAppointments":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ScheduleResolvedEvent;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, p1, p2, v1}, Lcom/vlingo/core/internal/dialogmanager/events/ScheduleResolvedEvent;-><init>(Ljava/util/List;II)V

    .line 71
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ScheduleResolvedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 72
    return-void
.end method


# virtual methods
.method public executeQuery(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 3
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->clearCacheOnRequest(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 30
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler$AppointmentQueryRunnable;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler$AppointmentQueryRunnable;-><init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;Lcom/vlingo/sdk/recognition/VLAction;)V

    const-string/jumbo v2, "AppointmentQueryRunnable"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 32
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v0

    const-string/jumbo v1, "appointment"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 34
    const/4 v0, 0x1

    return v0
.end method

.method protected sendEmptyEvent()V
    .locals 3

    .prologue
    .line 81
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ScheduleResolvedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ScheduleResolvedEvent;-><init>()V

    .line 82
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ScheduleResolvedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 83
    return-void
.end method
