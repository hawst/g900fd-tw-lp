.class public final enum Lcom/vlingo/core/internal/ResourceIdProvider$drawable;
.super Ljava/lang/Enum;
.source "ResourceIdProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/ResourceIdProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "drawable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/ResourceIdProvider$drawable;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

.field public static final enum core_all_logo:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

.field public static final enum core_facebook_icon:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

.field public static final enum core_sr_notification_skip:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

.field public static final enum core_stat_safereader_off:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

.field public static final enum core_stat_safereader_on:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

.field public static final enum core_stat_safereader_silent:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

.field public static final enum core_stat_safereader_sms:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

.field public static final enum core_twitter_icon:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

.field public static final enum core_weibo_icon:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 34
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    const-string/jumbo v1, "core_sr_notification_skip"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_sr_notification_skip:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    .line 35
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    const-string/jumbo v1, "core_stat_safereader_off"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_stat_safereader_off:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    .line 36
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    const-string/jumbo v1, "core_stat_safereader_on"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_stat_safereader_on:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    .line 37
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    const-string/jumbo v1, "core_stat_safereader_sms"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_stat_safereader_sms:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    .line 38
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    const-string/jumbo v1, "core_twitter_icon"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_twitter_icon:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    .line 39
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    const-string/jumbo v1, "core_facebook_icon"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_facebook_icon:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    .line 40
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    const-string/jumbo v1, "core_weibo_icon"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_weibo_icon:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    .line 41
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    const-string/jumbo v1, "core_all_logo"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_all_logo:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    .line 42
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    const-string/jumbo v1, "core_stat_safereader_silent"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_stat_safereader_silent:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    .line 33
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_sr_notification_skip:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_stat_safereader_off:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_stat_safereader_on:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_stat_safereader_sms:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_twitter_icon:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_facebook_icon:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_weibo_icon:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_all_logo:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_stat_safereader_silent:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->$VALUES:[Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/ResourceIdProvider$drawable;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 33
    const-class v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/ResourceIdProvider$drawable;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->$VALUES:[Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    return-object v0
.end method
