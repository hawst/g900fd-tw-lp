.class public Lcom/vlingo/core/internal/dialogmanager/MultipleResumeControl;
.super Ljava/lang/Object;
.source "MultipleResumeControl.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/ResumeControl;


# instance fields
.field private mNumberOfResumeControlsToWait:I

.field private mTargetResumeControl:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/dialogmanager/ResumeControl;I)V
    .locals 2
    .param p1, "targetResumeControl"    # Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    .param p2, "numberOfResumeControlsToWait"    # I

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    if-gtz p2, :cond_0

    .line 14
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Number of resume controls to wait should be greater than 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 16
    :cond_0
    iput p2, p0, Lcom/vlingo/core/internal/dialogmanager/MultipleResumeControl;->mNumberOfResumeControlsToWait:I

    .line 17
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/MultipleResumeControl;->mTargetResumeControl:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .line 18
    return-void
.end method


# virtual methods
.method public resume()V
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/vlingo/core/internal/dialogmanager/MultipleResumeControl;->mNumberOfResumeControlsToWait:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/vlingo/core/internal/dialogmanager/MultipleResumeControl;->mNumberOfResumeControlsToWait:I

    .line 23
    iget v0, p0, Lcom/vlingo/core/internal/dialogmanager/MultipleResumeControl;->mNumberOfResumeControlsToWait:I

    if-nez v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/MultipleResumeControl;->mTargetResumeControl:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/ResumeControl;->resume()V

    .line 26
    :cond_0
    return-void
.end method
