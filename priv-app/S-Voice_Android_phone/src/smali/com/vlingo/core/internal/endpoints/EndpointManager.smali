.class public final Lcom/vlingo/core/internal/endpoints/EndpointManager;
.super Ljava/lang/Object;
.source "EndpointManager.java"

# interfaces
.implements Lcom/vlingo/core/facade/endpoints/IEndpointManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/endpoints/EndpointManager$1;
    }
.end annotation


# static fields
.field private static mInstance:Lcom/vlingo/core/internal/endpoints/EndpointManager;

.field private static smFieldIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static smFieldIdsInit:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;",
            "Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18
    sput-object v0, Lcom/vlingo/core/internal/endpoints/EndpointManager;->smFieldIds:Ljava/util/Map;

    .line 19
    sput-object v0, Lcom/vlingo/core/internal/endpoints/EndpointManager;->smFieldIdsInit:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;",
            "Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "fieldIds":Ljava/util/Map;, "Ljava/util/Map<Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;>;"
    const/16 v1, 0x1e

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    sget-object v0, Lcom/vlingo/core/internal/endpoints/EndpointManager;->smFieldIdsInit:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/vlingo/core/internal/endpoints/EndpointManager;->smFieldIdsInit:Ljava/util/Map;

    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/vlingo/core/internal/endpoints/EndpointManager;->smFieldIds:Ljava/util/Map;

    .line 28
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/endpoints/EndpointManager;->smFieldIdsInit:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 29
    invoke-static {}, Lcom/vlingo/core/internal/endpoints/EndpointManager;->initFieldIds()V

    .line 30
    return-void
.end method

.method public static getInstance(Ljava/util/Map;)Lcom/vlingo/core/internal/endpoints/EndpointManager;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;",
            "Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;",
            ">;)",
            "Lcom/vlingo/core/internal/endpoints/EndpointManager;"
        }
    .end annotation

    .prologue
    .line 33
    .local p0, "fieldIds":Ljava/util/Map;, "Ljava/util/Map<Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;>;"
    sget-object v0, Lcom/vlingo/core/internal/endpoints/EndpointManager;->mInstance:Lcom/vlingo/core/internal/endpoints/EndpointManager;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/vlingo/core/internal/endpoints/EndpointManager;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/endpoints/EndpointManager;-><init>(Ljava/util/Map;)V

    sput-object v0, Lcom/vlingo/core/internal/endpoints/EndpointManager;->mInstance:Lcom/vlingo/core/internal/endpoints/EndpointManager;

    .line 37
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/endpoints/EndpointManager;->mInstance:Lcom/vlingo/core/internal/endpoints/EndpointManager;

    return-object v0
.end method

.method private static initFieldIds()V
    .locals 6

    .prologue
    .line 149
    const/4 v2, 0x0

    .line 150
    .local v2, "value":I
    sget-object v3, Lcom/vlingo/core/internal/endpoints/EndpointManager;->smFieldIdsInit:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 151
    .local v0, "field":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;>;"
    sget-object v4, Lcom/vlingo/core/internal/endpoints/EndpointManager$1;->$SwitchMap$com$vlingo$core$internal$dialogmanager$EndpointDetectionDuration:[I

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    .line 170
    :goto_1
    sget-object v3, Lcom/vlingo/core/internal/endpoints/EndpointManager;->smFieldIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 153
    :pswitch_0
    const-string/jumbo v3, "endpoint.time.speech.short"

    const/16 v4, 0x190

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 154
    goto :goto_1

    .line 156
    :pswitch_1
    const-string/jumbo v3, "endpoint.time.speech.medium"

    const/16 v4, 0x2ee

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 157
    goto :goto_1

    .line 159
    :pswitch_2
    const-string/jumbo v3, "endpoint.time.speech.medium.long"

    const/16 v4, 0x4e2

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 160
    goto :goto_1

    .line 162
    :pswitch_3
    const-string/jumbo v3, "endpoint.time.speech.long"

    const/16 v4, 0x6d6

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 163
    goto :goto_1

    .line 165
    :pswitch_4
    const-string/jumbo v3, "endpoint.time.speech.long.msg"

    const/16 v4, 0x8ca

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 166
    goto :goto_1

    .line 173
    .end local v0    # "field":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;>;"
    :cond_0
    return-void

    .line 151
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public getFieldId(Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .locals 3
    .param p1, "fieldId"    # Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;

    .prologue
    .line 124
    const-string/jumbo v1, "endpoint.time.speech.medium"

    const/16 v2, 0x2ee

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 125
    .local v0, "endpointTimeWithSpeechMilliseconds":I
    sget-object v1, Lcom/vlingo/core/internal/endpoints/EndpointManager;->smFieldIds:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126
    sget-object v1, Lcom/vlingo/core/internal/endpoints/EndpointManager;->smFieldIds:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 131
    :cond_0
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    invoke-direct {v1, p1, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;-><init>(Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;I)V

    return-object v1
.end method

.method public getFieldId(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .locals 4
    .param p1, "fieldIdString"    # Ljava/lang/String;

    .prologue
    .line 136
    sget-object v2, Lcom/vlingo/core/internal/endpoints/EndpointManager;->smFieldIds:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 138
    .local v1, "keyIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 139
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;

    .line 140
    .local v0, "fieldId":Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;
    invoke-interface {v0}, Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 141
    new-instance v3, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    sget-object v2, Lcom/vlingo/core/internal/endpoints/EndpointManager;->smFieldIds:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v3, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;-><init>(Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;I)V

    move-object v2, v3

    .line 145
    .end local v0    # "fieldId":Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setFieldIdSilenceDuration(Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;I)I
    .locals 3
    .param p1, "fieldId"    # Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;
    .param p2, "endpointTimeWithSpeechMilliseconds"    # I

    .prologue
    .line 58
    move v0, p2

    .line 59
    .local v0, "valueToUse":I
    sget-object v1, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->MAX_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v1

    if-le p2, v1, :cond_1

    .line 60
    sget-object v1, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->MAX_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v0

    .line 64
    :cond_0
    :goto_0
    sget-object v1, Lcom/vlingo/core/internal/endpoints/EndpointManager;->smFieldIds:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    return v0

    .line 61
    :cond_1
    sget-object v1, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->MIN_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v1

    if-ge p2, v1, :cond_0

    .line 62
    sget-object v1, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->MIN_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v0

    goto :goto_0
.end method

.method public setSilenceDurationNoSpeech(I)I
    .locals 2
    .param p1, "endpointTimeNoSpeechMilliseconds"    # I

    .prologue
    .line 112
    move v0, p1

    .line 113
    .local v0, "valueToUse":I
    sget-object v1, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->MAX_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v1

    if-le p1, v1, :cond_1

    .line 114
    sget-object v1, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->MAX_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v0

    .line 118
    :cond_0
    :goto_0
    const-string/jumbo v1, "endpoint.time_withoutspeech"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 119
    return v0

    .line 115
    :cond_1
    sget-object v1, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->MIN_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 116
    sget-object v1, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->MIN_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v0

    goto :goto_0
.end method

.method public setSilenceDurationWithSpeech(Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;I)I
    .locals 2
    .param p1, "category"    # Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;
    .param p2, "endpointTimeWithSpeechMilliseconds"    # I

    .prologue
    .line 80
    move v0, p2

    .line 81
    .local v0, "valueToUse":I
    sget-object v1, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->MAX_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v1

    if-le p2, v1, :cond_1

    .line 82
    sget-object v1, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->MAX_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v0

    .line 86
    :cond_0
    :goto_0
    sget-object v1, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->SHORT:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    if-ne v1, p1, :cond_2

    .line 87
    const-string/jumbo v1, "endpoint.time.speech.short"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 98
    :goto_1
    invoke-static {}, Lcom/vlingo/core/internal/endpoints/EndpointManager;->initFieldIds()V

    .line 99
    return v0

    .line 83
    :cond_1
    sget-object v1, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->MIN_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v1

    if-ge p2, v1, :cond_0

    .line 84
    sget-object v1, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->MIN_ENDPOINT_TIMEOUT_MS:Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/EndPointTimeoutEnum;->getValue()I

    move-result v0

    goto :goto_0

    .line 88
    :cond_2
    sget-object v1, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    if-ne v1, p1, :cond_3

    .line 89
    const-string/jumbo v1, "endpoint.time.speech.long"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    goto :goto_1

    .line 90
    :cond_3
    sget-object v1, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->MEDIUM_LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    if-ne v1, p1, :cond_4

    .line 91
    const-string/jumbo v1, "endpoint.time.speech.medium.long"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    goto :goto_1

    .line 92
    :cond_4
    sget-object v1, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->LONG_LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    if-ne v1, p1, :cond_5

    .line 93
    const-string/jumbo v1, "endpoint.time.speech.long.msg"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    goto :goto_1

    .line 95
    :cond_5
    const-string/jumbo v1, "endpoint.time.speech.medium"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    goto :goto_1
.end method
