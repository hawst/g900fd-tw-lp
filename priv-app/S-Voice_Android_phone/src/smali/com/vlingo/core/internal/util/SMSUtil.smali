.class public Lcom/vlingo/core/internal/util/SMSUtil;
.super Ljava/lang/Object;
.source "SMSUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/util/SMSUtil$SMSResultReceiver;,
        Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;,
        Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallback;,
        Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;
    }
.end annotation


# static fields
.field private static final ACTION_SMS_SENT:Ljava/lang/String; = "com.vlingo.client.actions.ACTION_SMS_SENT"

.field public static final ADDRESS:Ljava/lang/String; = "address"

.field public static final BODY:Ljava/lang/String; = "body"

.field public static final DATE:Ljava/lang/String; = "date"

.field public static final ERROR_CODE:Ljava/lang/String; = "err_code"

.field private static final EXTRA_URI:Ljava/lang/String; = "com.vlingo.client.extras.URI"

.field private static final INBOX:Landroid/net/Uri;

.field private static final LOCK:Ljava/lang/Object;

.field public static final MESSAGE_TYPE_ALL:I = 0x0

.field public static final MESSAGE_TYPE_DRAFT:I = 0x3

.field public static final MESSAGE_TYPE_FAILED:I = 0x5

.field public static final MESSAGE_TYPE_INBOX:I = 0x1

.field public static final MESSAGE_TYPE_OUTBOX:I = 0x4

.field public static final MESSAGE_TYPE_QUEUED:I = 0x6

.field public static final MESSAGE_TYPE_SENT:I = 0x2

.field private static final OUTBOX:Landroid/net/Uri;

.field public static final PERSON_ID:Ljava/lang/String; = "person"

.field public static final READ:Ljava/lang/String; = "read"

.field public static final STATUS:Ljava/lang/String; = "status"

.field public static final STATUS_COMPLETE:I = 0x0

.field public static final STATUS_FAILED:I = 0x80

.field public static final STATUS_NONE:I = -0x1

.field public static final STATUS_PENDING:I = 0x40

.field public static final SUBJECT:Ljava/lang/String; = "subject"

.field public static final THREAD_ID:Ljava/lang/String; = "thread_id"

.field public static final TYPE:Ljava/lang/String; = "type"

.field private static smsSendCallback:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private static smsSendReceiver:Lcom/vlingo/core/internal/util/SMSUtil$SMSResultReceiver;

.field private static smsSendReceiverRefCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-string/jumbo v0, "content://sms/inbox"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/SMSUtil;->INBOX:Landroid/net/Uri;

    .line 36
    const-string/jumbo v0, "content://sms/outbox"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/SMSUtil;->OUTBOX:Landroid/net/Uri;

    .line 614
    const/4 v0, 0x0

    sput v0, Lcom/vlingo/core/internal/util/SMSUtil;->smsSendReceiverRefCount:I

    .line 615
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/util/SMSUtil;->smsSendCallback:Ljava/util/Map;

    .line 616
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/util/SMSUtil;->LOCK:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 729
    return-void
.end method

.method static synthetic access$000()Ljava/util/Map;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/vlingo/core/internal/util/SMSUtil;->smsSendCallback:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$100(Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-static {p0}, Lcom/vlingo/core/internal/util/SMSUtil;->releaseSmsSendReceiver(Landroid/content/Context;)V

    return-void
.end method

.method public static addMessageToOutbox(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZJ)Landroid/net/Uri;
    .locals 10
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;
    .param p3, "date"    # Ljava/lang/Long;
    .param p4, "deliveryReport"    # Z
    .param p5, "threadId"    # J

    .prologue
    .line 315
    sget-object v1, Lcom/vlingo/core/internal/util/SMSUtil;->OUTBOX:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    move v7, p4

    move-wide v8, p5

    invoke-static/range {v0 .. v9}, Lcom/vlingo/core/internal/util/SMSUtil;->addMessageToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJ)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static addMessageToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJ)Landroid/net/Uri;
    .locals 4
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "address"    # Ljava/lang/String;
    .param p3, "body"    # Ljava/lang/String;
    .param p4, "subject"    # Ljava/lang/String;
    .param p5, "date"    # Ljava/lang/Long;
    .param p6, "read"    # Z
    .param p7, "deliveryReport"    # Z
    .param p8, "threadId"    # J

    .prologue
    .line 426
    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x7

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 428
    .local v1, "values":Landroid/content/ContentValues;
    const-string/jumbo v2, "address"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    if-eqz p5, :cond_0

    .line 430
    const-string/jumbo v2, "date"

    invoke-virtual {v1, v2, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 432
    :cond_0
    const-string/jumbo v3, "read"

    if-eqz p6, :cond_3

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :goto_0
    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 433
    const-string/jumbo v2, "subject"

    invoke-virtual {v1, v2, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    const-string/jumbo v2, "body"

    invoke-virtual {v1, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    if-eqz p7, :cond_1

    .line 436
    const-string/jumbo v2, "status"

    const/16 v3, 0x40

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 438
    :cond_1
    const-wide/16 v2, -0x1

    cmp-long v2, p8, v2

    if-eqz v2, :cond_2

    .line 439
    const-string/jumbo v2, "thread_id"

    invoke-static {p8, p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 441
    :cond_2
    invoke-virtual {p0, p1, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 445
    .local v0, "newUri":Landroid/net/Uri;
    return-object v0

    .line 432
    .end local v0    # "newUri":Landroid/net/Uri;
    :cond_3
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0
.end method

.method public static findMatchingContactDataInInboxOutbox(Landroid/content/Context;[Lcom/vlingo/core/internal/contacts/ContactData;)Lcom/vlingo/core/internal/contacts/ContactData;
    .locals 29
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "items"    # [Lcom/vlingo/core/internal/contacts/ContactData;

    .prologue
    .line 155
    if-eqz p1, :cond_0

    move-object/from16 v0, p1

    array-length v2, v0

    if-nez v2, :cond_2

    .line 156
    :cond_0
    const/16 v19, 0x0

    .line 272
    :cond_1
    :goto_0
    return-object v19

    .line 158
    :cond_2
    const/4 v10, 0x0

    .line 160
    .local v10, "c":Landroid/database/Cursor;
    const/16 v19, 0x0

    .line 161
    .local v19, "foundItemInbox":Lcom/vlingo/core/internal/contacts/ContactData;
    const/16 v20, 0x0

    .line 162
    .local v20, "foundItemOutbox":Lcom/vlingo/core/internal/contacts/ContactData;
    const-wide/16 v15, -0x1

    .line 163
    .local v15, "foundItemDateInbox":J
    const-wide/16 v17, -0x1

    .line 166
    .local v17, "foundItemDateOutobx":J
    new-instance v24, Ljava/util/HashMap;

    invoke-direct/range {v24 .. v24}, Ljava/util/HashMap;-><init>()V

    .line 167
    .local v24, "itemAddressTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    new-instance v25, Ljava/util/HashMap;

    invoke-direct/range {v25 .. v25}, Ljava/util/HashMap;-><init>()V

    .line 168
    .local v25, "itemIDTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    move-object/from16 v9, p1

    .local v9, "arr$":[Lcom/vlingo/core/internal/contacts/ContactData;
    array-length v0, v9

    move/from16 v26, v0

    .local v26, "len$":I
    const/16 v21, 0x0

    .local v21, "i$":I
    :goto_1
    move/from16 v0, v21

    move/from16 v1, v26

    if-ge v0, v1, :cond_3

    aget-object v23, v9, v21

    .line 169
    .local v23, "item":Lcom/vlingo/core/internal/contacts/ContactData;
    move-object/from16 v0, v23

    iget-object v8, v0, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    .line 170
    .local v8, "address":Ljava/lang/String;
    invoke-static {v8}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 171
    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v8, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    move-object/from16 v0, v23

    iget-object v2, v0, Lcom/vlingo/core/internal/contacts/ContactData;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-wide v2, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    add-int/lit8 v21, v21, 0x1

    goto :goto_1

    .line 179
    .end local v8    # "address":Ljava/lang/String;
    .end local v23    # "item":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_3
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/util/SMSUtil;->INBOX:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string/jumbo v7, "date DESC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 180
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 183
    const/16 v22, 0x0

    .line 185
    .local v22, "index":I
    :cond_4
    const-string/jumbo v2, "address"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    .line 186
    .restart local v8    # "address":Ljava/lang/String;
    const-string/jumbo v2, "body"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v27

    .line 187
    .local v27, "msgBody":Ljava/lang/String;
    const-string/jumbo v2, "person"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 188
    .local v11, "contactID":J
    const-string/jumbo v2, "date"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    .line 189
    .local v13, "date":J
    move-object/from16 v0, p0

    invoke-static {v11, v12, v0}, Lcom/vlingo/core/internal/util/ContactUtil;->getContactFullName(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v28

    .line 190
    .local v28, "name":Ljava/lang/String;
    invoke-static {v8}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 195
    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 198
    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    .end local v19    # "foundItemInbox":Lcom/vlingo/core/internal/contacts/ContactData;
    check-cast v19, Lcom/vlingo/core/internal/contacts/ContactData;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    .restart local v19    # "foundItemInbox":Lcom/vlingo/core/internal/contacts/ContactData;
    move-wide v15, v13

    .line 214
    .end local v8    # "address":Ljava/lang/String;
    .end local v11    # "contactID":J
    .end local v13    # "date":J
    .end local v22    # "index":I
    .end local v27    # "msgBody":Ljava/lang/String;
    .end local v28    # "name":Ljava/lang/String;
    :cond_5
    :goto_2
    if-eqz v10, :cond_6

    .line 215
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 221
    :cond_6
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/util/SMSUtil;->OUTBOX:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string/jumbo v7, "date DESC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 222
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 225
    const/16 v22, 0x0

    .line 227
    .restart local v22    # "index":I
    :cond_7
    const-string/jumbo v2, "address"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    .line 228
    .restart local v8    # "address":Ljava/lang/String;
    const-string/jumbo v2, "body"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v27

    .line 229
    .restart local v27    # "msgBody":Ljava/lang/String;
    const-string/jumbo v2, "person"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 230
    .restart local v11    # "contactID":J
    const-string/jumbo v2, "date"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    .line 231
    .restart local v13    # "date":J
    move-object/from16 v0, p0

    invoke-static {v11, v12, v0}, Lcom/vlingo/core/internal/util/ContactUtil;->getContactFullName(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v28

    .line 232
    .restart local v28    # "name":Ljava/lang/String;
    invoke-static {v8}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 237
    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 240
    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    .end local v20    # "foundItemOutbox":Lcom/vlingo/core/internal/contacts/ContactData;
    check-cast v20, Lcom/vlingo/core/internal/contacts/ContactData;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 241
    .restart local v20    # "foundItemOutbox":Lcom/vlingo/core/internal/contacts/ContactData;
    move-wide/from16 v17, v13

    .line 256
    .end local v8    # "address":Ljava/lang/String;
    .end local v11    # "contactID":J
    .end local v13    # "date":J
    .end local v22    # "index":I
    .end local v27    # "msgBody":Ljava/lang/String;
    .end local v28    # "name":Ljava/lang/String;
    :cond_8
    :goto_3
    if-eqz v10, :cond_9

    .line 257
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 262
    :cond_9
    if-eqz v19, :cond_10

    if-eqz v20, :cond_10

    .line 263
    cmp-long v2, v15, v17

    if-gtz v2, :cond_1

    move-object/from16 v19, v20

    .line 266
    goto/16 :goto_0

    .line 202
    .restart local v8    # "address":Ljava/lang/String;
    .restart local v11    # "contactID":J
    .restart local v13    # "date":J
    .restart local v22    # "index":I
    .restart local v27    # "msgBody":Ljava/lang/String;
    .restart local v28    # "name":Ljava/lang/String;
    :cond_a
    :try_start_2
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 205
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    .end local v19    # "foundItemInbox":Lcom/vlingo/core/internal/contacts/ContactData;
    check-cast v19, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 206
    .restart local v19    # "foundItemInbox":Lcom/vlingo/core/internal/contacts/ContactData;
    move-wide v15, v13

    .line 207
    goto/16 :goto_2

    .line 210
    :cond_b
    add-int/lit8 v22, v22, 0x1

    .line 211
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    if-eqz v2, :cond_5

    const/16 v2, 0xa

    move/from16 v0, v22

    if-lt v0, v2, :cond_4

    goto/16 :goto_2

    .line 214
    .end local v8    # "address":Ljava/lang/String;
    .end local v11    # "contactID":J
    .end local v13    # "date":J
    .end local v19    # "foundItemInbox":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v22    # "index":I
    .end local v27    # "msgBody":Ljava/lang/String;
    .end local v28    # "name":Ljava/lang/String;
    :catchall_0
    move-exception v2

    if-eqz v10, :cond_c

    .line 215
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_c
    throw v2

    .line 244
    .restart local v8    # "address":Ljava/lang/String;
    .restart local v11    # "contactID":J
    .restart local v13    # "date":J
    .restart local v19    # "foundItemInbox":Lcom/vlingo/core/internal/contacts/ContactData;
    .restart local v22    # "index":I
    .restart local v27    # "msgBody":Ljava/lang/String;
    .restart local v28    # "name":Ljava/lang/String;
    :cond_d
    :try_start_3
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 247
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    .end local v20    # "foundItemOutbox":Lcom/vlingo/core/internal/contacts/ContactData;
    check-cast v20, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 248
    .restart local v20    # "foundItemOutbox":Lcom/vlingo/core/internal/contacts/ContactData;
    move-wide/from16 v17, v13

    .line 249
    goto :goto_3

    .line 252
    :cond_e
    add-int/lit8 v22, v22, 0x1

    .line 253
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0xa

    move/from16 v0, v22

    if-lt v0, v2, :cond_7

    goto :goto_3

    .line 256
    .end local v8    # "address":Ljava/lang/String;
    .end local v11    # "contactID":J
    .end local v13    # "date":J
    .end local v20    # "foundItemOutbox":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v22    # "index":I
    .end local v27    # "msgBody":Ljava/lang/String;
    .end local v28    # "name":Ljava/lang/String;
    :catchall_1
    move-exception v2

    if-eqz v10, :cond_f

    .line 257
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_f
    throw v2

    .line 267
    .restart local v20    # "foundItemOutbox":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_10
    if-nez v19, :cond_1

    .line 269
    if-eqz v20, :cond_11

    move-object/from16 v19, v20

    .line 270
    goto/16 :goto_0

    .line 272
    :cond_11
    const/16 v19, 0x0

    goto/16 :goto_0
.end method

.method public static findMatchingDisplayItemInInboxOutbox(Landroid/content/Context;[Lcom/vlingo/core/internal/contacts/ContactMatch;)Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contacts"    # [Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    const/4 v7, 0x0

    .line 276
    if-eqz p1, :cond_0

    array-length v8, p1

    if-nez v8, :cond_1

    .line 289
    :cond_0
    :goto_0
    return-object v7

    .line 279
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 280
    .local v0, "aggregateData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    move-object v2, p1

    .local v2, "arr$":[Lcom/vlingo/core/internal/contacts/ContactMatch;
    array-length v5, v2

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_2

    aget-object v3, v2, v4

    .line 281
    .local v3, "c":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 280
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 283
    .end local v3    # "c":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-array v1, v8, [Lcom/vlingo/core/internal/contacts/ContactData;

    .line 284
    .local v1, "aggregateDataArray":[Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "aggregateDataArray":[Lcom/vlingo/core/internal/contacts/ContactData;
    check-cast v1, [Lcom/vlingo/core/internal/contacts/ContactData;

    .line 285
    .restart local v1    # "aggregateDataArray":[Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-static {p0, v1}, Lcom/vlingo/core/internal/util/SMSUtil;->findMatchingContactDataInInboxOutbox(Landroid/content/Context;[Lcom/vlingo/core/internal/contacts/ContactData;)Lcom/vlingo/core/internal/contacts/ContactData;

    move-result-object v6

    .line 286
    .local v6, "matchedData":Lcom/vlingo/core/internal/contacts/ContactData;
    if-eqz v6, :cond_0

    .line 287
    iget-object v7, v6, Lcom/vlingo/core/internal/contacts/ContactData;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    goto :goto_0
.end method

.method public static getLastNUnreadMessages(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 781
    const/4 v15, 0x0

    .line 782
    .local v15, "c":Landroid/database/Cursor;
    new-instance v18, Ljava/util/ArrayList;

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 785
    .local v18, "messages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;>;"
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/util/SMSUtil;->INBOX:Landroid/net/Uri;

    const/4 v4, 0x0

    const-string/jumbo v5, "read = 0"

    const/4 v6, 0x0

    const-string/jumbo v7, "date DESC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 786
    invoke-static {v15}, Lcom/vlingo/core/internal/util/CursorUtil;->isValid(Landroid/database/Cursor;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 787
    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 792
    const/16 v17, 0x0

    .line 794
    .local v17, "index":I
    :cond_0
    const/4 v7, 0x0

    invoke-interface {v15, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 795
    .local v8, "id":J
    const-string/jumbo v7, "address"

    invoke-interface {v15, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v15, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 796
    .local v3, "address":Ljava/lang/String;
    const-string/jumbo v7, "body"

    invoke-interface {v15, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v15, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 797
    .local v5, "msgBody":Ljava/lang/String;
    const-string/jumbo v7, "subject"

    invoke-interface {v15, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v15, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 798
    .local v6, "subject":Ljava/lang/String;
    const-string/jumbo v7, "date"

    invoke-interface {v15, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v15, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 799
    .local v12, "date":J
    const-string/jumbo v7, "type"

    invoke-interface {v15, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v15, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 800
    .local v14, "type":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-static {v3, v0}, Lcom/vlingo/core/internal/util/ContactUtil;->getContactFullNameFromPhoneNumber(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 801
    .local v4, "name":Ljava/lang/String;
    invoke-static {v3}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 804
    new-instance v2, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;

    const/4 v7, 0x0

    const-wide/16 v10, -0x1

    invoke-direct/range {v2 .. v14}, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLjava/lang/String;)V

    .line 805
    .local v2, "msg":Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;
    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 806
    add-int/lit8 v17, v17, 0x1

    .line 807
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-eqz v7, :cond_1

    move/from16 v0, v17

    move/from16 v1, p1

    if-lt v0, v1, :cond_0

    .line 816
    .end local v2    # "msg":Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;
    .end local v3    # "address":Ljava/lang/String;
    .end local v4    # "name":Ljava/lang/String;
    .end local v5    # "msgBody":Ljava/lang/String;
    .end local v6    # "subject":Ljava/lang/String;
    .end local v8    # "id":J
    .end local v12    # "date":J
    .end local v14    # "type":Ljava/lang/String;
    .end local v17    # "index":I
    :cond_1
    if-eqz v15, :cond_2

    .line 817
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 820
    :cond_2
    :goto_0
    return-object v18

    .line 810
    :catch_0
    move-exception v16

    .line 813
    .local v16, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 816
    if-eqz v15, :cond_2

    .line 817
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 816
    .end local v16    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    if-eqz v15, :cond_3

    .line 817
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v7
.end method

.method public static getMostRecentMessageFromInbox(Landroid/content/Context;)Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 511
    const/4 v1, 0x1

    invoke-static {p0, v2, v1}, Lcom/vlingo/core/internal/util/SMSUtil;->getRecentMessagesFromInbox(Landroid/content/Context;ZI)Ljava/util/ArrayList;

    move-result-object v0

    .line 512
    .local v0, "results":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 513
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;

    .line 516
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getRecentMessagesFromInbox(Landroid/content/Context;Z)Ljava/util/ArrayList;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "onlyShowMostRecentMessageFromEachContact"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 520
    const/16 v0, 0x14

    invoke-static {p0, p1, v0}, Lcom/vlingo/core/internal/util/SMSUtil;->getRecentMessagesFromInbox(Landroid/content/Context;ZI)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static getRecentMessagesFromInbox(Landroid/content/Context;ZI)Ljava/util/ArrayList;
    .locals 23
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "onlyShowMostRecentMessageFromEachContact"    # Z
    .param p2, "maxResults"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "ZI)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 524
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 525
    .local v17, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;>;"
    new-instance v18, Ljava/util/HashSet;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashSet;-><init>()V

    .line 526
    .local v18, "seenAddresses":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v15, 0x0

    .line 528
    .local v15, "cur":Landroid/database/Cursor;
    move/from16 v20, p2

    .line 529
    .local v20, "uniqueLimit":I
    const/16 v19, 0x32

    .line 530
    .local v19, "totalLimit":I
    const/16 v16, 0x0

    .line 531
    .local v16, "index":I
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/util/SMSUtil;->INBOX:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string/jumbo v7, "date DESC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 532
    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v21

    if-nez v21, :cond_1

    .line 574
    if-eqz v15, :cond_0

    .line 576
    :try_start_1
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 581
    :cond_0
    :goto_0
    return-object v17

    .line 537
    :cond_1
    :try_start_2
    const-string/jumbo v21, "address"

    move-object/from16 v0, v21

    invoke-interface {v15, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    invoke-interface {v15, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    .line 542
    .local v3, "address":Ljava/lang/String;
    if-eqz v3, :cond_3

    .line 543
    if-eqz p1, :cond_2

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_3

    .line 545
    :cond_2
    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/vlingo/core/internal/util/ContactUtil;->getPersonIdFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v10

    .line 546
    .local v10, "contactID":J
    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/vlingo/core/internal/util/ContactUtil;->getLookupKeyFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 549
    .local v7, "lookupKey":Ljava/lang/String;
    const-wide/16 v21, -0x1

    cmp-long v21, v10, v21

    if-eqz v21, :cond_5

    .line 550
    move-object/from16 v0, p0

    invoke-static {v10, v11, v0}, Lcom/vlingo/core/internal/util/ContactUtil;->getContactFullName(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 555
    .local v4, "name":Ljava/lang/String;
    :goto_1
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-interface {v15, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 556
    .local v8, "id":J
    const-string/jumbo v21, "body"

    move-object/from16 v0, v21

    invoke-interface {v15, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    invoke-interface {v15, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 557
    .local v5, "body":Ljava/lang/String;
    const-string/jumbo v21, "subject"

    move-object/from16 v0, v21

    invoke-interface {v15, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    invoke-interface {v15, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 558
    .local v6, "subject":Ljava/lang/String;
    const-string/jumbo v21, "date"

    move-object/from16 v0, v21

    invoke-interface {v15, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    invoke-interface {v15, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 559
    .local v12, "date":J
    const-string/jumbo v21, "type"

    move-object/from16 v0, v21

    invoke-interface {v15, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    invoke-interface {v15, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 563
    .local v14, "type":Ljava/lang/String;
    new-instance v2, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;

    invoke-direct/range {v2 .. v14}, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLjava/lang/String;)V

    .line 564
    .local v2, "message":Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;
    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 565
    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 568
    .end local v2    # "message":Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;
    .end local v4    # "name":Ljava/lang/String;
    .end local v5    # "body":Ljava/lang/String;
    .end local v6    # "subject":Ljava/lang/String;
    .end local v7    # "lookupKey":Ljava/lang/String;
    .end local v8    # "id":J
    .end local v10    # "contactID":J
    .end local v12    # "date":J
    .end local v14    # "type":Ljava/lang/String;
    :cond_3
    add-int/lit8 v16, v16, 0x1

    .line 569
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v21

    if-eqz v21, :cond_4

    const/16 v21, 0x32

    move/from16 v0, v16

    move/from16 v1, v21

    if-ge v0, v1, :cond_4

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v21

    move/from16 v0, v21

    move/from16 v1, v20

    if-lt v0, v1, :cond_1

    .line 574
    :cond_4
    if-eqz v15, :cond_0

    .line 576
    :try_start_3
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 577
    :catch_0
    move-exception v21

    goto/16 :goto_0

    .line 552
    .restart local v7    # "lookupKey":Ljava/lang/String;
    .restart local v10    # "contactID":J
    :cond_5
    :try_start_4
    invoke-static {v3}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v4

    .restart local v4    # "name":Ljava/lang/String;
    goto :goto_1

    .line 571
    .end local v3    # "address":Ljava/lang/String;
    .end local v4    # "name":Ljava/lang/String;
    .end local v7    # "lookupKey":Ljava/lang/String;
    .end local v10    # "contactID":J
    :catch_1
    move-exception v21

    .line 574
    if-eqz v15, :cond_0

    .line 576
    :try_start_5
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_0

    .line 577
    :catch_2
    move-exception v21

    goto/16 :goto_0

    .line 574
    :catchall_0
    move-exception v21

    if-eqz v15, :cond_6

    .line 576
    :try_start_6
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    .line 578
    :cond_6
    :goto_2
    throw v21

    .line 577
    :catch_3
    move-exception v21

    goto/16 :goto_0

    :catch_4
    move-exception v22

    goto :goto_2
.end method

.method public static getSMSIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p0, "address"    # Ljava/lang/String;
    .param p1, "body"    # Ljava/lang/String;

    .prologue
    .line 116
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sms:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 117
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "address"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 118
    const-string/jumbo v1, "sms_body"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 119
    const-string/jumbo v1, "vnd.android-dir/mms-sms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 120
    return-object v0
.end method

.method public static getSMSIntent([Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 8
    .param p0, "addresses"    # [Ljava/lang/String;
    .param p1, "body"    # Ljava/lang/String;

    .prologue
    .line 124
    const-string/jumbo v5, ""

    .line 125
    .local v5, "smsSendString":Ljava/lang/String;
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 126
    .local v4, "s":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 125
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 128
    .end local v4    # "s":Ljava/lang/String;
    :cond_0
    const/4 v6, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 131
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v6, "android.intent.action.VIEW"

    const-string/jumbo v7, "sms:"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v2, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 132
    .local v2, "intent":Landroid/content/Intent;
    const-string/jumbo v6, "address"

    invoke-virtual {v2, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 133
    const-string/jumbo v6, "sms_body"

    invoke-virtual {v2, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 134
    const-string/jumbo v6, "vnd.android-dir/mms-sms"

    invoke-virtual {v2, v6}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    return-object v2
.end method

.method public static getSMSReplyIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "to"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;

    .prologue
    .line 52
    const/4 v13, 0x0

    .line 53
    .local v13, "replyAddress":Ljava/lang/String;
    const/4 v15, 0x0

    .line 54
    .local v15, "replyName":Ljava/lang/String;
    const/4 v14, 0x0

    .line 56
    .local v14, "replyMsgBody":Ljava/lang/String;
    if-eqz p1, :cond_2

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 58
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    .line 59
    .local v16, "toWords":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 61
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/util/SMSUtil;->INBOX:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string/jumbo v6, "date DESC"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 62
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 64
    :cond_0
    const-string/jumbo v1, "address"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    .line 65
    .local v7, "address":Ljava/lang/String;
    const-string/jumbo v1, "body"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v11

    .line 66
    .local v11, "msgBody":Ljava/lang/String;
    const-string/jumbo v1, "person"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 67
    .local v9, "contactID":J
    move-object/from16 v0, p0

    invoke-static {v9, v10, v0}, Lcom/vlingo/core/internal/util/ContactUtil;->getContactFullName(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    .line 70
    .local v12, "name":Ljava/lang/String;
    if-eqz v12, :cond_5

    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_5

    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/util/SMSUtil;->isNameMatch([Ljava/lang/String;[Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_5

    .line 75
    move-object v13, v7

    .line 76
    move-object v15, v12

    .line 77
    move-object v14, v11

    .line 83
    .end local v7    # "address":Ljava/lang/String;
    .end local v9    # "contactID":J
    .end local v11    # "msgBody":Ljava/lang/String;
    .end local v12    # "name":Ljava/lang/String;
    :cond_1
    :goto_0
    if-eqz v8, :cond_2

    .line 84
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 89
    .end local v8    # "c":Landroid/database/Cursor;
    .end local v16    # "toWords":[Ljava/lang/String;
    :cond_2
    if-nez v13, :cond_4

    .line 91
    const/4 v8, 0x0

    .line 93
    .restart local v8    # "c":Landroid/database/Cursor;
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/util/SMSUtil;->INBOX:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string/jumbo v6, "date DESC"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 94
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 95
    const-string/jumbo v1, "address"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v13

    .line 96
    const-string/jumbo v1, "body"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v14

    .line 97
    const-string/jumbo v1, "person"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 98
    .restart local v9    # "contactID":J
    move-object/from16 v0, p0

    invoke-static {v9, v10, v0}, Lcom/vlingo/core/internal/util/ContactUtil;->getContactFullName(JLandroid/content/Context;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v15

    .line 103
    .end local v9    # "contactID":J
    :cond_3
    if-eqz v8, :cond_4

    .line 104
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 112
    .end local v8    # "c":Landroid/database/Cursor;
    :cond_4
    move-object/from16 v0, p2

    invoke-static {v13, v0}, Lcom/vlingo/core/internal/util/SMSUtil;->getSMSIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    return-object v1

    .line 80
    .restart local v7    # "address":Ljava/lang/String;
    .restart local v8    # "c":Landroid/database/Cursor;
    .restart local v9    # "contactID":J
    .restart local v11    # "msgBody":Ljava/lang/String;
    .restart local v12    # "name":Ljava/lang/String;
    .restart local v16    # "toWords":[Ljava/lang/String;
    :cond_5
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 83
    .end local v7    # "address":Ljava/lang/String;
    .end local v9    # "contactID":J
    .end local v11    # "msgBody":Ljava/lang/String;
    .end local v12    # "name":Ljava/lang/String;
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_6

    .line 84
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v1

    .line 103
    .end local v16    # "toWords":[Ljava/lang/String;
    :catchall_1
    move-exception v1

    if-eqz v8, :cond_7

    .line 104
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v1
.end method

.method private static isNameMatch([Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 4
    .param p0, "name1"    # [Ljava/lang/String;
    .param p1, "name2"    # [Ljava/lang/String;

    .prologue
    .line 139
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 140
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    array-length v2, p1

    if-ge v1, v2, :cond_1

    .line 141
    aget-object v2, p0, v0

    aget-object v3, p1, v1

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 142
    const/4 v2, 0x1

    .line 146
    .end local v1    # "j":I
    :goto_2
    return v2

    .line 140
    .restart local v1    # "j":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 139
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 146
    .end local v1    # "j":I
    :cond_2
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public static moveMessageToFolder(Landroid/content/Context;Landroid/net/Uri;I)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "folder"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 463
    if-nez p1, :cond_0

    .line 506
    :goto_0
    return v6

    .line 469
    :cond_0
    const/4 v2, 0x0

    .line 470
    .local v2, "markAsUnread":Z
    const/4 v1, 0x0

    .line 471
    .local v1, "markAsRead":Z
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 487
    :goto_1
    :pswitch_0
    new-instance v4, Landroid/content/ContentValues;

    const/4 v7, 0x2

    invoke-direct {v4, v7}, Landroid/content/ContentValues;-><init>(I)V

    .line 489
    .local v4, "values":Landroid/content/ContentValues;
    const-string/jumbo v7, "type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 490
    if-eqz v2, :cond_2

    .line 491
    const-string/jumbo v7, "read"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 496
    :cond_1
    :goto_2
    const/4 v3, 0x0

    .line 499
    .local v3, "result":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7, p1, v4, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 506
    :goto_3
    if-ne v5, v3, :cond_3

    :goto_4
    move v6, v5

    goto :goto_0

    .line 477
    .end local v3    # "result":I
    .end local v4    # "values":Landroid/content/ContentValues;
    :pswitch_1
    const/4 v1, 0x1

    .line 478
    goto :goto_1

    .line 481
    :pswitch_2
    const/4 v2, 0x1

    .line 482
    goto :goto_1

    .line 492
    .restart local v4    # "values":Landroid/content/ContentValues;
    :cond_2
    if-eqz v1, :cond_1

    .line 493
    const-string/jumbo v7, "read"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_2

    .line 500
    .restart local v3    # "result":I
    :catch_0
    move-exception v0

    .line 501
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v7, "SMSUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    move v5, v6

    .line 506
    goto :goto_4

    .line 471
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private static releaseSmsSendReceiver(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 836
    sget-object v2, Lcom/vlingo/core/internal/util/SMSUtil;->LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 837
    :try_start_0
    sget v1, Lcom/vlingo/core/internal/util/SMSUtil;->smsSendReceiverRefCount:I

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/vlingo/core/internal/util/SMSUtil;->smsSendReceiverRefCount:I

    .line 838
    sget v1, Lcom/vlingo/core/internal/util/SMSUtil;->smsSendReceiverRefCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 840
    :try_start_1
    sget-object v1, Lcom/vlingo/core/internal/util/SMSUtil;->smsSendReceiver:Lcom/vlingo/core/internal/util/SMSUtil$SMSResultReceiver;

    invoke-virtual {p0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 847
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v2

    .line 848
    return-void

    .line 841
    :catch_0
    move-exception v0

    .line 844
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 847
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public static sendSMS(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallback;)V
    .locals 25
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallback;

    .prologue
    .line 650
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v19

    .line 651
    .local v19, "id":J
    new-instance v14, Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;

    move-object/from16 v0, p3

    invoke-direct {v14, v0}, Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;-><init>(Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallback;)V

    .line 652
    .local v14, "cbWrapper":Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;
    sget-object v4, Lcom/vlingo/core/internal/util/SMSUtil;->smsSendCallback:Ljava/util/Map;

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 653
    const/16 v24, 0x0

    .line 658
    .local v24, "uri":Landroid/net/Uri;
    :try_start_0
    sget-object v4, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MsgUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v4}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v17

    .line 659
    .local v17, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/16 v22, 0x0

    .line 660
    .local v22, "msgUtil":Lcom/vlingo/core/internal/util/MsgUtil;
    if-eqz v17, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move-result v4

    if-eqz v4, :cond_0

    .line 662
    :try_start_1
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/vlingo/core/internal/util/MsgUtil;

    move-object/from16 v22, v0
    :try_end_1
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 672
    :cond_0
    :goto_0
    :try_start_2
    new-instance v21, Landroid/content/IntentFilter;

    const-string/jumbo v4, "com.vlingo.client.actions.ACTION_SMS_SENT"

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 673
    .local v21, "in":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/SMSUtil;->setupSmsSendReceiver(Landroid/content/Context;Landroid/content/IntentFilter;)V

    .line 677
    invoke-static/range {p1 .. p1}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 678
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    invoke-static/range {v3 .. v9}, Lcom/vlingo/core/internal/util/SMSUtil;->addMessageToOutbox(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZJ)Landroid/net/Uri;

    move-result-object v24

    .line 682
    new-instance v23, Landroid/content/Intent;

    const-string/jumbo v4, "com.vlingo.client.actions.ACTION_SMS_SENT"

    move-object/from16 v0, v23

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 683
    .local v23, "sentIntent":Landroid/content/Intent;
    const-string/jumbo v4, "id"

    move-object/from16 v0, v23

    move-wide/from16 v1, v19

    invoke-virtual {v0, v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 684
    const-string/jumbo v4, "com.vlingo.client.extras.URI"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 686
    const/4 v4, 0x0

    const/high16 v5, 0x10000000

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-static {v0, v4, v1, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v12

    .line 690
    .local v12, "sentPendingIntent":Landroid/app/PendingIntent;
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v3

    .line 691
    .local v3, "smsManager":Landroid/telephony/SmsManager;
    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Landroid/telephony/SmsManager;->divideMessage(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 692
    .local v6, "parts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_4

    .line 693
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 694
    .local v7, "sentPendingIntents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v18

    if-ge v0, v4, :cond_2

    .line 695
    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 694
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 663
    .end local v3    # "smsManager":Landroid/telephony/SmsManager;
    .end local v6    # "parts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "sentPendingIntents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .end local v12    # "sentPendingIntent":Landroid/app/PendingIntent;
    .end local v18    # "i":I
    .end local v21    # "in":Landroid/content/IntentFilter;
    .end local v23    # "sentIntent":Landroid/content/Intent;
    :catch_0
    move-exception v15

    .line 664
    .local v15, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v15}, Ljava/lang/InstantiationException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    .line 714
    .end local v15    # "e":Ljava/lang/InstantiationException;
    .end local v17    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .end local v22    # "msgUtil":Lcom/vlingo/core/internal/util/MsgUtil;
    :catch_1
    move-exception v15

    .line 715
    .local v15, "e":Ljava/lang/IllegalArgumentException;
    sget-object v4, Lcom/vlingo/core/internal/util/SMSUtil;->smsSendCallback:Ljava/util/Map;

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 716
    const/4 v4, 0x1

    const/4 v5, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallback;->onSMSSent(ZI)V

    .line 717
    const/4 v4, 0x5

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-static {v0, v1, v4}, Lcom/vlingo/core/internal/util/SMSUtil;->moveMessageToFolder(Landroid/content/Context;Landroid/net/Uri;I)Z

    .line 718
    invoke-static/range {p0 .. p0}, Lcom/vlingo/core/internal/util/SMSUtil;->releaseSmsSendReceiver(Landroid/content/Context;)V

    .line 727
    .end local v15    # "e":Ljava/lang/IllegalArgumentException;
    :cond_1
    :goto_2
    return-void

    .line 665
    .restart local v17    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .restart local v22    # "msgUtil":Lcom/vlingo/core/internal/util/MsgUtil;
    :catch_2
    move-exception v15

    .line 666
    .local v15, "e":Ljava/lang/IllegalAccessException;
    :try_start_3
    invoke-virtual {v15}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    .line 719
    .end local v15    # "e":Ljava/lang/IllegalAccessException;
    .end local v17    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .end local v22    # "msgUtil":Lcom/vlingo/core/internal/util/MsgUtil;
    :catch_3
    move-exception v16

    .line 722
    .local v16, "ex":Ljava/lang/Exception;
    sget-object v4, Lcom/vlingo/core/internal/util/SMSUtil;->smsSendCallback:Ljava/util/Map;

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 723
    const/4 v4, 0x1

    const/4 v5, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallback;->onSMSSent(ZI)V

    .line 724
    const/4 v4, 0x5

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-static {v0, v1, v4}, Lcom/vlingo/core/internal/util/SMSUtil;->moveMessageToFolder(Landroid/content/Context;Landroid/net/Uri;I)Z

    .line 725
    invoke-static/range {p0 .. p0}, Lcom/vlingo/core/internal/util/SMSUtil;->releaseSmsSendReceiver(Landroid/content/Context;)V

    goto :goto_2

    .line 697
    .end local v16    # "ex":Ljava/lang/Exception;
    .restart local v3    # "smsManager":Landroid/telephony/SmsManager;
    .restart local v6    # "parts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v7    # "sentPendingIntents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .restart local v12    # "sentPendingIntent":Landroid/app/PendingIntent;
    .restart local v17    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .restart local v18    # "i":I
    .restart local v21    # "in":Landroid/content/IntentFilter;
    .restart local v22    # "msgUtil":Lcom/vlingo/core/internal/util/MsgUtil;
    .restart local v23    # "sentIntent":Landroid/content/Intent;
    :cond_2
    :try_start_4
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v14, v4}, Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;->setParts(I)V

    .line 698
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 699
    if-eqz v22, :cond_1

    .line 700
    const/4 v5, 0x0

    const/4 v8, 0x0

    move-object/from16 v3, v22

    move-object/from16 v4, p1

    invoke-interface/range {v3 .. v8}, Lcom/vlingo/core/internal/util/MsgUtil;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_2

    .line 703
    :cond_3
    const/4 v5, 0x0

    const/4 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v3 .. v8}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_2

    .line 706
    .end local v7    # "sentPendingIntents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .end local v18    # "i":I
    :cond_4
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 707
    if-eqz v22, :cond_1

    .line 708
    const/4 v10, 0x0

    const/4 v13, 0x0

    move-object/from16 v8, v22

    move-object/from16 v9, p1

    move-object/from16 v11, p2

    invoke-interface/range {v8 .. v13}, Lcom/vlingo/core/internal/util/MsgUtil;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    goto :goto_2

    .line 711
    :cond_5
    const/4 v10, 0x0

    const/4 v13, 0x0

    move-object v8, v3

    move-object/from16 v9, p1

    move-object/from16 v11, p2

    invoke-virtual/range {v8 .. v13}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_2
.end method

.method private static setupSmsSendReceiver(Landroid/content/Context;Landroid/content/IntentFilter;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "in"    # Landroid/content/IntentFilter;

    .prologue
    .line 824
    sget-object v1, Lcom/vlingo/core/internal/util/SMSUtil;->LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 825
    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/util/SMSUtil;->smsSendReceiver:Lcom/vlingo/core/internal/util/SMSUtil$SMSResultReceiver;

    if-eqz v0, :cond_0

    sget v0, Lcom/vlingo/core/internal/util/SMSUtil;->smsSendReceiverRefCount:I

    if-nez v0, :cond_1

    .line 826
    :cond_0
    new-instance v0, Lcom/vlingo/core/internal/util/SMSUtil$SMSResultReceiver;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/SMSUtil$SMSResultReceiver;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/util/SMSUtil;->smsSendReceiver:Lcom/vlingo/core/internal/util/SMSUtil$SMSResultReceiver;

    .line 827
    sget-object v0, Lcom/vlingo/core/internal/util/SMSUtil;->smsSendReceiver:Lcom/vlingo/core/internal/util/SMSUtil$SMSResultReceiver;

    invoke-virtual {p0, v0, p1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 828
    const/4 v0, 0x1

    sput v0, Lcom/vlingo/core/internal/util/SMSUtil;->smsSendReceiverRefCount:I

    .line 832
    :goto_0
    monitor-exit v1

    .line 833
    return-void

    .line 830
    :cond_1
    sget v0, Lcom/vlingo/core/internal/util/SMSUtil;->smsSendReceiverRefCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/vlingo/core/internal/util/SMSUtil;->smsSendReceiverRefCount:I

    goto :goto_0

    .line 832
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
