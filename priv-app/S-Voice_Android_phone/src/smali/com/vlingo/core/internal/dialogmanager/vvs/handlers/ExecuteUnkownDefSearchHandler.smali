.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ExecuteUnkownDefSearchHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;
.source "ExecuteUnkownDefSearchHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public actionSuccess()V
    .locals 0

    .prologue
    .line 34
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;->actionSuccess()V

    .line 38
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 3
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 24
    invoke-virtual {p0, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ExecuteUnkownDefSearchHandler;->setListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 25
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CURRENT_ACTION:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 26
    .local v0, "searchQuery":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ExecuteUnkownDefSearchHandler;->getWebSearchURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 27
    .local v1, "url":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ExecuteUnkownDefSearchHandler;->executeSearchIntentFromURL(Ljava/lang/String;)V

    .line 29
    const/4 v2, 0x0

    return v2
.end method
