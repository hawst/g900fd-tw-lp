.class public final Lcom/vlingo/core/internal/contacts/OldContactDBUtil;
.super Ljava/lang/Object;
.source "OldContactDBUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/contacts/IContactDBUtil;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/contacts/OldContactDBUtil$AppendBasicMatchToWhereClauseOnlyImpl;,
        Lcom/vlingo/core/internal/contacts/OldContactDBUtil$AppendMatchToWhereClauseOnlyImpl;,
        Lcom/vlingo/core/internal/contacts/OldContactDBUtil$ContactMatchQueryInterface;
    }
.end annotation


# static fields
.field private static final BIT_MASK:I = 0xff

.field private static final EVENT_LUNAR_CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.pantech.cursor.item/lunar_event"

.field private static final JAPANESE_HONORIFICS:Ljava/util/regex/Pattern;

.field private static final KOREAN_TITLES:Ljava/util/regex/Pattern;

.field private static final MULTIPLE_QUERIES_SEPARATOR:Ljava/lang/String; = ";"

.field private static final NORMALIZED_MIME_EXTENSION:Ljava/lang/String; = "_svoice_normalization"

.field private static final NORMALIZERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;",
            ">;"
        }
    .end annotation
.end field

.field private static final RULE_SETS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/vlingo/core/internal/contacts/OldContactDBUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 67
    const-class v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->TAG:Ljava/lang/String;

    .line 76
    const-string/jumbo v0, "(\\w+)(\u3055\u3093|\u30b5\u30f3|\u3061\u3083\u3093|\u30c1\u30e3\u30f3|\u541b|\u304f\u3093|\u30af\u30f3|\u69d8|\u3055\u307e|\u30b5\u30de)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->JAPANESE_HONORIFICS:Ljava/util/regex/Pattern;

    .line 78
    const-string/jumbo v0, "(\\w+\\s*)(\uc0ac\uc6d0|\ub300\ub9ac|\uacfc\uc7a5|\ucc28\uc7a5|\ubd80\uc7a5|\uc120\uc784|\ucc45\uc784|\uc218\uc11d|\uc774\uc0ac|\uc0c1\ubb34|\uc804\ubb34|\ubd80\uc0ac\uc7a5|\uc0ac\uc7a5|\ubd80\ud68c\uc7a5|\ud68c\uc7a5|\uc9c0\uc0ac\uc7a5|\uac10\uc0ac|\uace0\ubb38|\uc790\ubb38|\uc8fc\uc784|\uc0ac\uc5c5\ubd80\uc7a5|\ubcf8\ubd80\uc7a5|\ubd80\ubcf8\ubd80\uc7a5|\uad00\uc7a5|\uad6d\uc7a5|\uc18c\uc7a5|\uc9c0\uc810\uc7a5|\uc2e4\uc7a5|\ud300\uc7a5|\uacc4\uc7a5|\uc804\ubb38|\ud504\ub85c|\uac10\ub3c5|\ucf54\uce58|\uc0ac\uc6d0\ub2d8|\ub300\ub9ac\ub2d8|\uacfc\uc7a5\ub2d8|\ucc28\uc7a5\ub2d8|\ubd80\uc7a5\ub2d8|\uc120\uc784\ub2d8|\ucc45\uc784\ub2d8|\uc218\uc11d\ub2d8|\uc774\uc0ac\ub2d8|\uc0c1\ubb34\ub2d8|\uc804\ubb34\ub2d8|\ubd80\uc0ac\uc7a5\ub2d8|\uc0ac\uc7a5\ub2d8|\ubd80\ud68c\uc7a5\ub2d8|\ud68c\uc7a5\ub2d8|\uc9c0\uc0ac\uc7a5\ub2d8|\uac10\uc0ac\ub2d8|\uace0\ubb38\ub2d8|\uc790\ubb38\ub2d8|\uc0ac\uc5c5\ubd80\uc7a5\ub2d8|\ubcf8\ubd80\uc7a5\ub2d8|\ubd80\ubcf8\ubd80\uc7a5\ub2d8|\uad00\uc7a5\ub2d8|\uad6d\uc7a5\ub2d8|\uc18c\uc7a5\ub2d8|\uc9c0\uc810\uc7a5\ub2d8|\uc2e4\uc7a5\ub2d8|\ud300\uc7a5\ub2d8|\uacc4\uc7a5\ub2d8|\uc8fc\uc784\ub2d8|\uc804\ubb38\ub2d8|\ud504\ub85c\ub2d8|\uac10\ub3c5\ub2d8|\ucf54\uce58\ub2d8)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->KOREAN_TITLES:Ljava/util/regex/Pattern;

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->RULE_SETS:Ljava/util/List;

    .line 93
    sget-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->RULE_SETS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    sget-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->RULE_SETS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    sget-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->RULE_SETS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    sget-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->RULE_SETS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/rules/FrenchContactRuleSet;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/rules/FrenchContactRuleSet;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    sget-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->RULE_SETS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/rules/EFIGSContactRuleSet;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/rules/EFIGSContactRuleSet;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->NORMALIZERS:Ljava/util/List;

    .line 102
    sget-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->NORMALIZERS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/normalizers/SpacesContactNameNormalizer;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/normalizers/SpacesContactNameNormalizer;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    sget-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->NORMALIZERS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/normalizers/DowncaseContactNameNormalizer;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/normalizers/DowncaseContactNameNormalizer;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    sget-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->NORMALIZERS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    sget-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->NORMALIZERS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/normalizers/SpecialCharactersContactNameNormalizer;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/normalizers/SpecialCharactersContactNameNormalizer;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    sget-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->NORMALIZERS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/normalizers/AccentedCharactersContactNameNormalizer;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/normalizers/AccentedCharactersContactNameNormalizer;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    sget-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->NORMALIZERS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/normalizers/JapaneseHonorificsContactNameNormalizer;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/normalizers/JapaneseHonorificsContactNameNormalizer;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    sget-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->NORMALIZERS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    sget-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->NORMALIZERS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    sget-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->NORMALIZERS:Ljava/util/List;

    new-instance v1, Lcom/vlingo/core/internal/contacts/normalizers/SuffixNormalizer;

    invoke-direct {v1}, Lcom/vlingo/core/internal/contacts/normalizers/SuffixNormalizer;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->instance:Lcom/vlingo/core/internal/contacts/OldContactDBUtil;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1020
    return-void
.end method

.method private additionalPhoneticMatching(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactRule;Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)V
    .locals 21
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "rule"    # Lcom/vlingo/core/internal/contacts/ContactRule;
    .param p4, "ruleSet"    # Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/ContactRule;",
            "Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 236
    .local p5, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    .local p6, "localMatches":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p7, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 237
    .local v7, "phoneticLocalMatches":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    move-object/from16 v0, p4

    instance-of v1, v0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;

    if-nez v1, :cond_0

    move-object/from16 v0, p4

    instance-of v1, v0, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;

    if-nez v1, :cond_0

    .line 238
    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 270
    :goto_0
    return-void

    .line 242
    :cond_0
    invoke-virtual/range {p6 .. p6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 243
    .local v18, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    move-object/from16 v0, p4

    instance-of v1, v0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;

    if-eqz v1, :cond_2

    .line 244
    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    move-object/from16 v1, p4

    .line 245
    check-cast v1, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;

    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 246
    .local v4, "newQueryWithPhoneticName":Ljava/lang/String;
    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->skipExtraData()Z

    move-result v5

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v6, p5

    move-object/from16 v8, p7

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->findMatchesByNamePerPass(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)Z

    goto :goto_1

    .end local v4    # "newQueryWithPhoneticName":Ljava/lang/String;
    :cond_2
    move-object/from16 v17, p4

    .line 249
    check-cast v17, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;

    .line 250
    .local v17, "japanRuleSet":Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/core/internal/contacts/ContactRule;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "Partial Match"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 251
    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneticFirstName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getFirstName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/16 v19, 0x1

    .line 252
    .local v19, "queryHasFirstName":Z
    :goto_2
    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneticLastName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLastName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/16 v20, 0x1

    .line 253
    .local v20, "queryHasLastName":Z
    :goto_3
    if-eqz v19, :cond_6

    if-eqz v20, :cond_6

    .line 263
    .end local v19    # "queryHasFirstName":Z
    .end local v20    # "queryHasLastName":Z
    :cond_3
    :goto_4
    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 264
    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->phoneticName:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;->queryToWhereClauseWithPhoneticFullName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->skipExtraData()Z

    move-result v12

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v13, p5

    move-object v14, v7

    move-object/from16 v15, p7

    invoke-virtual/range {v8 .. v15}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->findMatchesByNamePerPass(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)Z

    goto/16 :goto_1

    .line 251
    :cond_4
    const/16 v19, 0x0

    goto :goto_2

    .line 252
    .restart local v19    # "queryHasFirstName":Z
    :cond_5
    const/16 v20, 0x0

    goto :goto_3

    .line 255
    .restart local v20    # "queryHasLastName":Z
    :cond_6
    if-eqz v19, :cond_7

    .line 256
    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneticFirstName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;->queryToWhereClauseWithPhoneticFirstName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->skipExtraData()Z

    move-result v12

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v13, p5

    move-object v14, v7

    move-object/from16 v15, p7

    invoke-virtual/range {v8 .. v15}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->findMatchesByNamePerPass(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)Z

    goto :goto_4

    .line 258
    :cond_7
    if-eqz v20, :cond_3

    .line 259
    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneticLastName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;->queryToWhereClauseWithPhoneticLastName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->skipExtraData()Z

    move-result v12

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v13, p5

    move-object v14, v7

    move-object/from16 v15, p7

    invoke-virtual/range {v8 .. v15}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->findMatchesByNamePerPass(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)Z

    goto :goto_4

    .line 269
    .end local v17    # "japanRuleSet":Lcom/vlingo/core/internal/contacts/rules/JapaneseContactRuleSet;
    .end local v18    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v19    # "queryHasFirstName":Z
    .end local v20    # "queryHasLastName":Z
    :cond_8
    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    goto/16 :goto_0
.end method

.method public static varargs appendBasicMatchToWhereClauseOnlyWithNorm(Ljava/util/List;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "column"    # Ljava/lang/String;
    .param p2, "mimetype"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1061
    .local p0, "words":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil$AppendBasicMatchToWhereClauseOnlyImpl;

    invoke-direct {v0}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil$AppendBasicMatchToWhereClauseOnlyImpl;-><init>()V

    invoke-static {p0, p1, v0, p2}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->matchGenericWithMimeNorm(Ljava/util/List;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/OldContactDBUtil$ContactMatchQueryInterface;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static appendMatchToWhereClause(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "column"    # Ljava/lang/String;
    .param p2, "mimetype"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 954
    .local p0, "words":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 955
    :cond_0
    const/4 v3, 0x0

    .line 979
    :goto_0
    return-object v3

    .line 958
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 959
    .local v1, "where":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "(("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 961
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_4

    .line 962
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 963
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string/jumbo v4, "\'"

    const-string/jumbo v5, "\'\'"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 964
    .local v2, "word":Ljava/lang/String;
    if-lez v0, :cond_2

    .line 965
    const-string/jumbo v3, " OR "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 968
    :cond_2
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " LIKE \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "%\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " OR "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " LIKE \'% "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "%\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 961
    .end local v2    # "word":Ljava/lang/String;
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 975
    :cond_4
    const-string/jumbo v3, ") AND ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "mimetype"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\'))"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 979
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0
.end method

.method public static varargs appendMatchToWhereClauseMimeType([Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "mimetype"    # [Ljava/lang/String;

    .prologue
    .line 984
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 986
    .local v5, "where":Ljava/lang/StringBuilder;
    array-length v6, p0

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 987
    const-string/jumbo v6, "mimetype"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " = \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v7, p0, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1003
    :goto_0
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 989
    :cond_0
    const-string/jumbo v6, " ( "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 990
    const/4 v1, 0x1

    .line 991
    .local v1, "first":Z
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    .line 992
    .local v4, "type":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 993
    const/4 v1, 0x0

    .line 998
    :goto_2
    const-string/jumbo v6, "mimetype"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " = \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 991
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 995
    :cond_1
    const-string/jumbo v6, " OR "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1000
    .end local v4    # "type":Ljava/lang/String;
    :cond_2
    const-string/jumbo v6, " ) "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static appendMatchToWhereClauseOnly(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "column"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1106
    .local p0, "words":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 1107
    :cond_0
    const/4 v3, 0x0

    .line 1128
    :goto_0
    return-object v3

    .line 1110
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1111
    .local v1, "where":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1113
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_4

    .line 1114
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1115
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string/jumbo v4, "\'"

    const-string/jumbo v5, "\'\'"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 1116
    .local v2, "word":Ljava/lang/String;
    if-lez v0, :cond_2

    .line 1117
    const-string/jumbo v3, " OR "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1120
    :cond_2
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " LIKE \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "%\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " OR "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " LIKE \'% "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "%\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1113
    .end local v2    # "word":Ljava/lang/String;
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1126
    :cond_4
    const-string/jumbo v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1128
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static appendMatchToWhereClauseOnlyWithNorm(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "column"    # Ljava/lang/String;
    .param p2, "mimetype"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1057
    .local p0, "words":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil$AppendMatchToWhereClauseOnlyImpl;

    invoke-direct {v0}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil$AppendMatchToWhereClauseOnlyImpl;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {p0, p1, v0, v1}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->matchGenericWithMimeNorm(Ljava/util/List;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/OldContactDBUtil$ContactMatchQueryInterface;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static applyScores(Ljava/util/HashMap;Ljava/util/List;Ljava/util/HashMap;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;Lcom/vlingo/core/internal/contacts/ContactExitCriteria;Ljava/lang/String;)V
    .locals 6
    .param p3, "scorer"    # Lcom/vlingo/core/internal/contacts/scoring/ContactScore;
    .param p4, "exitCriteria"    # Lcom/vlingo/core/internal/contacts/ContactExitCriteria;
    .param p5, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Lcom/vlingo/core/internal/contacts/scoring/ContactScore;",
            "Lcom/vlingo/core/internal/contacts/ContactExitCriteria;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "matchTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p1, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .local p2, "localMatches":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/high16 v5, 0x40a00000    # 5.0f

    .line 273
    invoke-virtual {p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 274
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 275
    .local v2, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {p3, p5, v2}, Lcom/vlingo/core/internal/contacts/scoring/ContactScore;->getScore(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactMatch;)I

    move-result v3

    int-to-float v3, v3

    iput v3, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    .line 276
    iget v3, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    float-to-int v3, v3

    invoke-virtual {p4, v3}, Lcom/vlingo/core/internal/contacts/ContactExitCriteria;->tallyScore(I)Z

    goto :goto_0

    .line 278
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v2    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_0
    invoke-virtual {p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 279
    .restart local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 280
    .restart local v2    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget v3, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    float-to-int v3, v3

    invoke-virtual {p4, v3}, Lcom/vlingo/core/internal/contacts/ContactExitCriteria;->keepMatch(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 282
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget v3, v3, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    iget v4, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    div-float/2addr v4, v5

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 283
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    :cond_2
    iget v3, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    div-float/2addr v3, v5

    iput v3, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->score:F

    goto :goto_1

    .line 288
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v2    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_3
    return-void
.end method

.method private static augmentWords([Ljava/lang/String;)Ljava/util/List;
    .locals 11
    .param p0, "rawWords"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1460
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    .line 1462
    .local v8, "words":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    move v4, v3

    .end local v3    # "i$":I
    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v6, v0, v4

    .line 1464
    .local v6, "word":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "(?i).*"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ".*"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v7

    .line 1467
    .local v7, "wordPattern":Ljava/util/regex/Pattern;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getContactNameList()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .end local v4    # "i$":I
    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1468
    .local v1, "contact":Ljava/lang/String;
    invoke-virtual {v7, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 1469
    .local v2, "contactMatcher":Ljava/util/regex/Matcher;
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1472
    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1462
    .end local v1    # "contact":Ljava/lang/String;
    .end local v2    # "contactMatcher":Ljava/util/regex/Matcher;
    :cond_1
    add-int/lit8 v3, v4, 0x1

    .local v3, "i$":I
    move v4, v3

    .end local v3    # "i$":I
    .restart local v4    # "i$":I
    goto :goto_0

    .line 1476
    .end local v6    # "word":Ljava/lang/String;
    .end local v7    # "wordPattern":Ljava/util/regex/Pattern;
    :cond_2
    return-object v8
.end method

.method private static augmentWordsForSpaces([Ljava/lang/String;)Ljava/util/List;
    .locals 10
    .param p0, "rawWords"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1444
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    .line 1445
    .local v7, "words":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    move v3, v2

    .end local v2    # "i$":I
    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v6, v0, v3

    .line 1446
    .local v6, "word":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getContactNameList()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .end local v3    # "i$":I
    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1447
    .local v1, "contact":Ljava/lang/String;
    const-string/jumbo v8, "\\s+"

    const-string/jumbo v9, ""

    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    .line 1448
    .local v5, "normalizedString":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1451
    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1445
    .end local v1    # "contact":Ljava/lang/String;
    .end local v5    # "normalizedString":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v3, 0x1

    .local v2, "i$":I
    move v3, v2

    .end local v2    # "i$":I
    .restart local v3    # "i$":I
    goto :goto_0

    .line 1455
    .end local v6    # "word":Ljava/lang/String;
    :cond_2
    return-object v7
.end method

.method private static varargs basicClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p0, "query"    # Ljava/lang/String;
    .param p1, "fields"    # [Ljava/lang/String;

    .prologue
    .line 862
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 864
    .local v3, "dispNameWhere":Ljava/lang/StringBuilder;
    const-string/jumbo v12, ";"

    invoke-virtual {p0, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    .line 866
    .local v8, "isMultiple":Z
    const/4 v6, 0x0

    .line 867
    .local v6, "fieldsCounter":I
    if-eqz v8, :cond_3

    .line 868
    const-string/jumbo v12, ";"

    invoke-virtual {p0, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 869
    .local v0, "allQueries":[Ljava/lang/String;
    const/4 v12, 0x1

    array-length v13, v0

    invoke-static {v0, v12, v13}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/String;

    .line 870
    .local v10, "queries":[Ljava/lang/String;
    const/4 v11, 0x0

    .local v11, "queryCounter":I
    :goto_0
    array-length v12, v10

    if-ge v11, v12, :cond_5

    .line 871
    const-string/jumbo v12, " ("

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 872
    const/4 v6, 0x0

    .line 873
    aget-object v12, v10, v11

    invoke-static {v12}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->getFormattedQueryForMultiplePhoneticCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 875
    .local v2, "curQuery":Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->genEqualityOperator(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 877
    .local v4, "equalityClause":Ljava/lang/String;
    move-object v1, p1

    .local v1, "arr$":[Ljava/lang/String;
    array-length v9, v1

    .local v9, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_1
    if-ge v7, v9, :cond_1

    aget-object v5, v1, v7

    .line 878
    .local v5, "field":Ljava/lang/String;
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "\'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 883
    add-int/lit8 v6, v6, 0x1

    .line 884
    array-length v12, p1

    if-eq v6, v12, :cond_0

    .line 885
    const-string/jumbo v12, " OR "

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 877
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 888
    .end local v5    # "field":Ljava/lang/String;
    :cond_1
    add-int/lit8 v12, v11, 0x1

    array-length v13, v10

    if-ge v12, v13, :cond_2

    .line 889
    const-string/jumbo v12, ") OR "

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 870
    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 891
    :cond_2
    const-string/jumbo v12, ")"

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 896
    .end local v0    # "allQueries":[Ljava/lang/String;
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v2    # "curQuery":Ljava/lang/String;
    .end local v4    # "equalityClause":Ljava/lang/String;
    .end local v7    # "i$":I
    .end local v9    # "len$":I
    .end local v10    # "queries":[Ljava/lang/String;
    .end local v11    # "queryCounter":I
    :cond_3
    invoke-static {p0}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->genEqualityOperator(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 898
    .restart local v4    # "equalityClause":Ljava/lang/String;
    move-object v1, p1

    .restart local v1    # "arr$":[Ljava/lang/String;
    array-length v9, v1

    .restart local v9    # "len$":I
    const/4 v7, 0x0

    .restart local v7    # "i$":I
    :goto_3
    if-ge v7, v9, :cond_5

    aget-object v5, v1, v7

    .line 899
    .restart local v5    # "field":Ljava/lang/String;
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "\'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 900
    add-int/lit8 v6, v6, 0x1

    .line 901
    array-length v12, p1

    if-eq v6, v12, :cond_4

    .line 902
    const-string/jumbo v12, " OR "

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 898
    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 906
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v4    # "equalityClause":Ljava/lang/String;
    .end local v5    # "field":Ljava/lang/String;
    .end local v7    # "i$":I
    .end local v9    # "len$":I
    :cond_5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    return-object v12
.end method

.method private static varargs basicClauseList(Ljava/util/List;[Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "fields"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 835
    .local p0, "words":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 837
    .local v1, "dispNameWhere":Ljava/lang/StringBuilder;
    const/4 v4, 0x1

    .line 839
    .local v4, "first":Z
    const-string/jumbo v9, " ("

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 840
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 841
    .local v8, "word":Ljava/lang/String;
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v7, :cond_0

    aget-object v3, v0, v6

    .line 842
    .local v3, "field":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 843
    const/4 v4, 0x0

    .line 848
    :goto_1
    invoke-static {v8}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->genEqualityOperator(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 850
    .local v2, "equalityClause":Ljava/lang/String;
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 841
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 845
    .end local v2    # "equalityClause":Ljava/lang/String;
    :cond_1
    const-string/jumbo v9, " OR "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 857
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v3    # "field":Ljava/lang/String;
    .end local v6    # "i$":I
    .end local v7    # "len$":I
    .end local v8    # "word":Ljava/lang/String;
    :cond_2
    const-string/jumbo v9, ")"

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 858
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    return-object v9
.end method

.method private static varargs basicClauseWithMime(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "query"    # Ljava/lang/String;
    .param p1, "mimetype"    # Ljava/lang/String;
    .param p2, "fields"    # [Ljava/lang/String;

    .prologue
    .line 922
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 923
    .local v0, "dispNameWhere":Ljava/lang/StringBuilder;
    const-string/jumbo v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 924
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->appendMatchToWhereClauseMimeType([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 925
    const-string/jumbo v1, " AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 927
    invoke-static {p0, p2}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->basicClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 929
    const-string/jumbo v1, ") ) "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 930
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static cleanNormalizers()V
    .locals 3

    .prologue
    .line 1722
    sget-object v2, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->NORMALIZERS:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;

    .line 1723
    .local v1, "n":Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;
    instance-of v2, v1, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;

    if-eqz v2, :cond_0

    .line 1724
    check-cast v1, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;

    .end local v1    # "n":Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;->cleanTool()V

    goto :goto_0

    .line 1727
    :cond_1
    return-void
.end method

.method private static genEqualityOperator(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "word"    # Ljava/lang/String;

    .prologue
    .line 910
    const-string/jumbo v2, "%"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "_"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 913
    .local v1, "useLike":Z
    :goto_0
    if-eqz v1, :cond_2

    .line 914
    const-string/jumbo v0, " LIKE \'"

    .line 918
    .local v0, "equalityClause":Ljava/lang/String;
    :goto_1
    return-object v0

    .line 910
    .end local v0    # "equalityClause":Ljava/lang/String;
    .end local v1    # "useLike":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 916
    .restart local v1    # "useLike":Z
    :cond_2
    const-string/jumbo v0, " = \'"

    .restart local v0    # "equalityClause":Ljava/lang/String;
    goto :goto_1
.end method

.method private getAllContactData(Landroid/content/Context;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 135
    .local p2, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    .local p3, "localMatches":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p4, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/4 v0, 0x0

    .line 136
    .local v0, "count":I
    invoke-virtual {p3}, Ljava/util/HashMap;->size()I

    move-result v1

    if-lez v1, :cond_1

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->PHONE_NUMBER:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-virtual {p2, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->EMAIL_ADDRESS:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-virtual {p2, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->ADDRESS:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-virtual {p2, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->BIRTHDAY:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-virtual {p2, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 144
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->getContactDetails(Landroid/content/Context;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)I

    move-result v0

    .line 151
    :cond_1
    const/4 v0, 0x0

    .line 152
    invoke-virtual {p3}, Ljava/util/HashMap;->size()I

    move-result v1

    if-lez v1, :cond_3

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->SOCIAL_NETWORK:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-virtual {p2, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p2}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 153
    :cond_2
    invoke-direct {p0, p1, p3, p4}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->getSocialContactData(Landroid/content/Context;Ljava/util/HashMap;Ljava/util/List;)I

    move-result v0

    .line 156
    :cond_3
    return v0
.end method

.method private static getChineseDisplayNameWhereClause(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 11
    .param p0, "query"    # Ljava/lang/String;
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 365
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 366
    const/4 v8, 0x0

    .line 407
    :goto_0
    return-object v8

    .line 369
    :cond_0
    const-string/jumbo v8, "\\s"

    const-string/jumbo v9, ""

    invoke-virtual {p0, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 371
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 372
    .local v7, "words":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 373
    .local v4, "partialWords":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 375
    .local v1, "dispPartialNameWhere":Ljava/lang/StringBuilder;
    invoke-virtual {v7, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 377
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v9, 0x3

    if-ge v8, v9, :cond_2

    .line 378
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 379
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "__"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 381
    new-instance v1, Ljava/lang/StringBuilder;

    .end local v1    # "dispPartialNameWhere":Ljava/lang/StringBuilder;
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 382
    .restart local v1    # "dispPartialNameWhere":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v3, v8, :cond_2

    .line 383
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const-string/jumbo v9, "\'"

    const-string/jumbo v10, "\'\'"

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    .line 385
    .local v6, "word":Ljava/lang/String;
    invoke-static {v6}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->genEqualityOperator(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 387
    .local v2, "equalityClause":Ljava/lang/String;
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 388
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ge v3, v8, :cond_1

    .line 389
    const-string/jumbo v8, " OR "

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 382
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 394
    .end local v2    # "equalityClause":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v6    # "word":Ljava/lang/String;
    :cond_2
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 395
    .local v5, "whereClause":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v3, v8, :cond_4

    .line 396
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 397
    .local v0, "dispNameWhere":Ljava/lang/StringBuilder;
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const-string/jumbo v9, "\'"

    const-string/jumbo v10, "\'\'"

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    .line 399
    .restart local v6    # "word":Ljava/lang/String;
    invoke-static {v6}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->genEqualityOperator(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 401
    .restart local v2    # "equalityClause":Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 402
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 403
    if-nez v3, :cond_3

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_3

    .line 404
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 395
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 407
    .end local v0    # "dispNameWhere":Ljava/lang/StringBuilder;
    .end local v2    # "equalityClause":Ljava/lang/String;
    .end local v6    # "word":Ljava/lang/String;
    :cond_4
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-array v8, v8, [Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    goto/16 :goto_0
.end method

.method private getContactCursor(Landroid/content/Context;)Landroid/database/Cursor;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 1521
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1526
    :goto_0
    return-object v4

    .line 1524
    :cond_0
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string/jumbo v1, "data2"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string/jumbo v1, "data3"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string/jumbo v1, "data1"

    aput-object v1, v2, v0

    .line 1525
    .local v2, "proj":[Ljava/lang/String;
    const-string/jumbo v3, "mimetype=\'vnd.android.cursor.item/name\' AND in_visible_group=1"

    .line 1526
    .local v3, "where":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    goto :goto_0
.end method

.method private getExactContactMatchByQuery(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 20
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nameSelection"    # Ljava/lang/String;

    .prologue
    .line 484
    const-string/jumbo v17, "mimetype=\'vnd.android.cursor.item/name\' OR mimetype=\'vnd.android.cursor.item/nickname\' OR mimetype=\'vnd.android.cursor.item/organization\'"

    .line 488
    .local v17, "mimeTypeSelection":Ljava/lang/String;
    const/16 v1, 0x8

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "contact_id"

    aput-object v2, v3, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "raw_contact_id"

    aput-object v2, v3, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "lookup"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "display_name"

    aput-object v2, v3, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "starred"

    aput-object v2, v3, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "data1"

    aput-object v2, v3, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "times_contacted"

    aput-object v2, v3, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "phonetic_name"

    aput-object v2, v3, v1

    .line 498
    .local v3, "projection":[Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") AND ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 500
    .local v4, "selection":Ljava/lang/String;
    const/4 v13, 0x0

    .line 501
    .local v13, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    const/4 v14, 0x0

    .line 505
    .local v14, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 526
    if-eqz v14, :cond_0

    .line 527
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object v5, v13

    .line 531
    .end local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :goto_0
    return-object v13

    .line 508
    .restart local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_1
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 510
    if-eqz v14, :cond_5

    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 511
    const-string/jumbo v1, "contact_id"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    .line 512
    .local v12, "contactId":Ljava/lang/Long;
    const-string/jumbo v1, "lookup"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 513
    .local v10, "lookupKey":Ljava/lang/String;
    const-string/jumbo v1, "display_name"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 514
    .local v6, "displayName":Ljava/lang/String;
    const-string/jumbo v1, "starred"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 515
    .local v18, "starred":I
    const-string/jumbo v1, "times_contacted"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 516
    .local v19, "timesContacted":I
    const-string/jumbo v1, "data1"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 517
    .local v15, "data1":Ljava/lang/String;
    const-string/jumbo v1, "phonetic_name"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 519
    .local v7, "phoneticName":Ljava/lang/String;
    new-instance v5, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v1, 0x1

    move/from16 v0, v18

    if-ne v0, v1, :cond_3

    const/4 v11, 0x1

    :goto_1
    invoke-direct/range {v5 .. v11}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 520
    .end local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .local v5, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :try_start_2
    move/from16 v0, v19

    invoke-virtual {v5, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->setTimesContacted(I)V

    .line 521
    invoke-virtual {v5, v15}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addExtraName(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 526
    .end local v6    # "displayName":Ljava/lang/String;
    .end local v7    # "phoneticName":Ljava/lang/String;
    .end local v10    # "lookupKey":Ljava/lang/String;
    .end local v12    # "contactId":Ljava/lang/Long;
    .end local v15    # "data1":Ljava/lang/String;
    .end local v18    # "starred":I
    .end local v19    # "timesContacted":I
    :goto_2
    if-eqz v14, :cond_2

    .line 527
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_3
    move-object v13, v5

    .line 531
    .end local v5    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    goto/16 :goto_0

    .line 519
    .restart local v6    # "displayName":Ljava/lang/String;
    .restart local v7    # "phoneticName":Ljava/lang/String;
    .restart local v10    # "lookupKey":Ljava/lang/String;
    .restart local v12    # "contactId":Ljava/lang/Long;
    .restart local v15    # "data1":Ljava/lang/String;
    .restart local v18    # "starred":I
    .restart local v19    # "timesContacted":I
    :cond_3
    const/4 v11, 0x0

    goto :goto_1

    .line 523
    .end local v6    # "displayName":Ljava/lang/String;
    .end local v7    # "phoneticName":Ljava/lang/String;
    .end local v10    # "lookupKey":Ljava/lang/String;
    .end local v12    # "contactId":Ljava/lang/Long;
    .end local v15    # "data1":Ljava/lang/String;
    .end local v18    # "starred":I
    .end local v19    # "timesContacted":I
    :catch_0
    move-exception v16

    move-object v5, v13

    .line 524
    .end local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v5    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .local v16, "ex":Ljava/lang/Exception;
    :goto_4
    :try_start_3
    sget-object v1, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "getExactContactMatch: Caught Exception: "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v8, "\n"

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static/range {v16 .. v16}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 526
    if-eqz v14, :cond_2

    .line 527
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 526
    .end local v5    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v16    # "ex":Ljava/lang/Exception;
    .restart local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :catchall_0
    move-exception v1

    move-object v5, v13

    .end local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v5    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :goto_5
    if-eqz v14, :cond_4

    .line 527
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v1

    .line 526
    :catchall_1
    move-exception v1

    goto :goto_5

    .line 523
    .restart local v6    # "displayName":Ljava/lang/String;
    .restart local v7    # "phoneticName":Ljava/lang/String;
    .restart local v10    # "lookupKey":Ljava/lang/String;
    .restart local v12    # "contactId":Ljava/lang/Long;
    .restart local v15    # "data1":Ljava/lang/String;
    .restart local v18    # "starred":I
    .restart local v19    # "timesContacted":I
    :catch_1
    move-exception v16

    goto :goto_4

    .end local v5    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v6    # "displayName":Ljava/lang/String;
    .end local v7    # "phoneticName":Ljava/lang/String;
    .end local v10    # "lookupKey":Ljava/lang/String;
    .end local v12    # "contactId":Ljava/lang/Long;
    .end local v15    # "data1":Ljava/lang/String;
    .end local v18    # "starred":I
    .end local v19    # "timesContacted":I
    .restart local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_5
    move-object v5, v13

    .end local v13    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v5    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    goto :goto_2
.end method

.method private static getFacebookAccountTypeAndName(Landroid/content/Context;)[Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1480
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v5

    .line 1481
    .local v5, "mgr":Landroid/accounts/AccountManager;
    invoke-virtual {v5}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    .line 1482
    .local v1, "accounts":[Landroid/accounts/Account;
    move-object v2, v1

    .local v2, "arr$":[Landroid/accounts/Account;
    array-length v4, v2

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v2, v3

    .line 1485
    .local v0, "a":Landroid/accounts/Account;
    iget-object v6, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v6, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "facebook"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1486
    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v8, v6, v7

    .line 1489
    .end local v0    # "a":Landroid/accounts/Account;
    :goto_1
    return-object v6

    .line 1482
    .restart local v0    # "a":Landroid/accounts/Account;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1489
    .end local v0    # "a":Landroid/accounts/Account;
    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method private static getFormattedQueryForMultiplePhoneticCase(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "query"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 934
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 935
    .local v2, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v3, :cond_0

    invoke-virtual {p0, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v3

    .line 936
    .local v0, "containsAnyOnTheBeginning":Z
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v3, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v1, v3

    .line 937
    .local v1, "containsAnyOnTheEnd":Z
    :goto_1
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 938
    invoke-virtual {v2, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 950
    :goto_2
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .end local v0    # "containsAnyOnTheBeginning":Z
    .end local v1    # "containsAnyOnTheEnd":Z
    :cond_0
    move v0, v4

    .line 935
    goto :goto_0

    .restart local v0    # "containsAnyOnTheBeginning":Z
    :cond_1
    move v1, v4

    .line 936
    goto :goto_1

    .line 939
    .restart local v1    # "containsAnyOnTheEnd":Z
    :cond_2
    if-eqz v0, :cond_3

    .line 940
    invoke-virtual {v2, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, ";%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 942
    :cond_3
    if-eqz v1, :cond_4

    .line 943
    const-string/jumbo v3, "%;"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 946
    :cond_4
    const-string/jumbo v3, "%;"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, ";%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2
.end method

.method public static declared-synchronized getInstance()Lcom/vlingo/core/internal/contacts/OldContactDBUtil;
    .locals 2

    .prologue
    .line 116
    const-class v1, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->instance:Lcom/vlingo/core/internal/contacts/OldContactDBUtil;

    if-nez v0, :cond_0

    .line 117
    new-instance v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;

    invoke-direct {v0}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->instance:Lcom/vlingo/core/internal/contacts/OldContactDBUtil;

    .line 119
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->instance:Lcom/vlingo/core/internal/contacts/OldContactDBUtil;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static getJapaneseDisplayNameWhereClause(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 14
    .param p0, "query"    # Ljava/lang/String;
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 411
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 412
    const/4 v8, 0x0

    .line 459
    :cond_0
    return-object v8

    .line 415
    :cond_1
    const-string/jumbo v11, "\\s"

    const-string/jumbo v12, ""

    invoke-virtual {p0, v11, v12}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 418
    move-object v6, p0

    .line 420
    .local v6, "origQuery":Ljava/lang/String;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 421
    .local v10, "words":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v11, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->JAPANESE_HONORIFICS:Ljava/util/regex/Pattern;

    invoke-virtual {v11, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    .line 422
    .local v5, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 423
    const/4 v11, 0x1

    invoke-virtual {v5, v11}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 424
    .local v1, "deHonored":Ljava/lang/String;
    const/4 v11, 0x2

    new-array v7, v11, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v1, v7, v11

    const/4 v11, 0x1

    aput-object v6, v7, v11

    .line 426
    .local v7, "rawWords":[Ljava/lang/String;
    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 427
    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 428
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "%"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 429
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "%"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 430
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "%"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 431
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "%"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 432
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "%"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "%"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 433
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "%"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "%"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 443
    .end local v1    # "deHonored":Ljava/lang/String;
    :goto_0
    invoke-static {v7}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->augmentWordsForSpaces([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 444
    .local v0, "augmentedWords":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_2

    .line 445
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 448
    :cond_2
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v11

    new-array v8, v11, [Ljava/lang/String;

    .line 450
    .local v8, "retVal":[Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-ge v4, v11, :cond_0

    .line 451
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 452
    .local v2, "dispNameWhere":Ljava/lang/StringBuilder;
    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    const-string/jumbo v12, "\'"

    const-string/jumbo v13, "\'\'"

    invoke-virtual {v11, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    .line 454
    .local v9, "word":Ljava/lang/String;
    invoke-static {v9}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->genEqualityOperator(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 456
    .local v3, "equalityClause":Ljava/lang/String;
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "\'"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 457
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v8, v4

    .line 450
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 435
    .end local v0    # "augmentedWords":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "dispNameWhere":Ljava/lang/StringBuilder;
    .end local v3    # "equalityClause":Ljava/lang/String;
    .end local v4    # "i":I
    .end local v7    # "rawWords":[Ljava/lang/String;
    .end local v8    # "retVal":[Ljava/lang/String;
    .end local v9    # "word":Ljava/lang/String;
    :cond_3
    const/4 v11, 0x1

    new-array v7, v11, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v6, v7, v11

    .line 437
    .restart local v7    # "rawWords":[Ljava/lang/String;
    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 438
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "%"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 439
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "%"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 440
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "%"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "%"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private static getKoreanDisplayNameWhereClause(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 13
    .param p0, "query"    # Ljava/lang/String;
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 291
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 292
    const/4 v7, 0x0

    .line 361
    :cond_0
    return-object v7

    .line 295
    :cond_1
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 297
    .local v9, "words":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v10

    const/4 v11, 0x4

    if-le v10, v11, :cond_2

    .line 298
    sget-object v10, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->KOREAN_TITLES:Ljava/util/regex/Pattern;

    invoke-virtual {v10, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    .line 299
    .local v5, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 300
    invoke-interface {v9, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 301
    const/4 v10, 0x1

    invoke-virtual {v5, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 305
    .end local v5    # "m":Ljava/util/regex/Matcher;
    :cond_2
    move-object v6, p0

    .line 308
    .local v6, "origQuery":Ljava/lang/String;
    const-string/jumbo v10, "\\s+"

    const-string/jumbo v11, ""

    invoke-virtual {p0, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 318
    const-string/jumbo v2, "\uc774"

    .line 320
    .local v2, "familiarityMarker":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_3

    .line 321
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "%"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 347
    :goto_0
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    new-array v7, v10, [Ljava/lang/String;

    .line 349
    .local v7, "retVal":[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    if-ge v3, v10, :cond_0

    .line 350
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 351
    .local v0, "dispNameWhere":Ljava/lang/StringBuilder;
    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    const-string/jumbo v11, "\'"

    const-string/jumbo v12, "\'\'"

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    .line 356
    .local v8, "word":Ljava/lang/String;
    invoke-static {v8}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->genEqualityOperator(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 358
    .local v1, "equalityClause":Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "\'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 359
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v7, v3

    .line 349
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 322
    .end local v0    # "dispNameWhere":Ljava/lang/StringBuilder;
    .end local v1    # "equalityClause":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v7    # "retVal":[Ljava/lang/String;
    .end local v8    # "word":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v10

    const/4 v11, 0x2

    if-ne v10, v11, :cond_4

    .line 323
    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 325
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "%"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "%"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 326
    :cond_4
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v10

    const/4 v11, 0x3

    if-ne v10, v11, :cond_6

    .line 327
    const-string/jumbo v10, "\uc774"

    invoke-virtual {p0, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 328
    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 329
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x2

    invoke-virtual {p0, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 330
    invoke-interface {v9, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 332
    :cond_5
    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 333
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "%"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 334
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "%"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {p0, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 335
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v11, 0x0

    const/4 v12, 0x2

    invoke-virtual {p0, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "%"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 341
    :cond_6
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    .line 342
    .local v4, "len":I
    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 343
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "%"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "%"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 344
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "%"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    add-int/lit8 v11, v4, -0x2

    invoke-virtual {p0, v11, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private getSocialContactData(Landroid/content/Context;Ljava/util/HashMap;Ljava/util/List;)I
    .locals 20
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1308
    .local p2, "matchTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p3, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/4 v15, 0x0

    .line 1310
    .local v15, "count":I
    invoke-static/range {p1 .. p1}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->getFacebookAccountTypeAndName(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v17

    .line 1312
    .local v17, "facebookAccountTypeAndName":[Ljava/lang/String;
    if-eqz v17, :cond_6

    .line 1313
    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v4, "contact_id"

    aput-object v4, v3, v1

    const/4 v1, 0x1

    const-string/jumbo v4, "sourceid"

    aput-object v4, v3, v1

    .line 1318
    .local v3, "proj":[Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    .line 1319
    .local v19, "whereBuilder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "_id"

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, " IN ("

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1320
    const/16 v16, 0x0

    .line 1321
    .local v16, "counter":I
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_0
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v1

    move/from16 v0, v18

    if-ge v0, v1, :cond_1

    .line 1322
    const-string/jumbo v1, "?"

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1323
    add-int/lit8 v16, v16, 0x1

    .line 1324
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v1

    move/from16 v0, v16

    if-ge v0, v1, :cond_0

    .line 1325
    const-string/jumbo v1, ","

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1321
    :cond_0
    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    .line 1328
    :cond_1
    const-string/jumbo v1, ")"

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1330
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string/jumbo v4, "account_name"

    const/4 v6, 0x1

    aget-object v6, v17, v6

    invoke-virtual {v1, v4, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string/jumbo v4, "account_type"

    const/4 v6, 0x0

    aget-object v6, v17, v6

    invoke-virtual {v1, v4, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 1337
    .local v2, "rawContactUri":Landroid/net/Uri;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1338
    const/4 v1, 0x0

    .line 1367
    .end local v2    # "rawContactUri":Landroid/net/Uri;
    .end local v3    # "proj":[Ljava/lang/String;
    .end local v16    # "counter":I
    .end local v18    # "i":I
    .end local v19    # "whereBuilder":Ljava/lang/StringBuilder;
    :goto_1
    return v1

    .line 1342
    .restart local v2    # "rawContactUri":Landroid/net/Uri;
    .restart local v3    # "proj":[Ljava/lang/String;
    .restart local v16    # "counter":I
    .restart local v18    # "i":I
    .restart local v19    # "whereBuilder":Ljava/lang/StringBuilder;
    :cond_2
    const/4 v10, 0x0

    .line 1344
    .local v10, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1345
    if-eqz v10, :cond_5

    .line 1346
    :goto_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1347
    const-string/jumbo v1, "contact_id"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 1348
    .local v11, "colIndexContactID":I
    const-string/jumbo v1, "sourceid"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 1349
    .local v12, "colIndexSource":I
    invoke-interface {v10, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    .line 1350
    .local v13, "contactID":J
    invoke-interface {v10, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1351
    .local v7, "source":Ljava/lang/String;
    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 1352
    .local v5, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-eqz v7, :cond_3

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_4

    .line 1353
    :cond_3
    const-string/jumbo v7, "facebook"

    .line 1354
    :cond_4
    new-instance v4, Lcom/vlingo/core/internal/contacts/ContactData;

    sget-object v6, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Facebook:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    const/16 v8, 0x7d1

    const/4 v9, 0x0

    invoke-direct/range {v4 .. v9}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    invoke-virtual {v5, v4}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addSocial(Lcom/vlingo/core/internal/contacts/ContactData;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1355
    add-int/lit8 v15, v15, 0x1

    .line 1356
    goto :goto_2

    .line 1362
    .end local v5    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v7    # "source":Ljava/lang/String;
    .end local v11    # "colIndexContactID":I
    .end local v12    # "colIndexSource":I
    .end local v13    # "contactID":J
    :cond_5
    if-eqz v10, :cond_6

    .line 1363
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .end local v2    # "rawContactUri":Landroid/net/Uri;
    .end local v3    # "proj":[Ljava/lang/String;
    .end local v10    # "c":Landroid/database/Cursor;
    .end local v16    # "counter":I
    .end local v18    # "i":I
    .end local v19    # "whereBuilder":Ljava/lang/StringBuilder;
    :cond_6
    :goto_3
    move v1, v15

    .line 1367
    goto :goto_1

    .line 1358
    .restart local v2    # "rawContactUri":Landroid/net/Uri;
    .restart local v3    # "proj":[Ljava/lang/String;
    .restart local v10    # "c":Landroid/database/Cursor;
    .restart local v16    # "counter":I
    .restart local v18    # "i":I
    .restart local v19    # "whereBuilder":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v1

    .line 1362
    if-eqz v10, :cond_6

    .line 1363
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 1362
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_7

    .line 1363
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v1
.end method

.method public static getWhere(Ljava/util/EnumSet;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1279
    .local p0, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    const-string/jumbo v0, ""

    .line 1281
    .local v0, "where":Ljava/lang/String;
    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->PHONE_NUMBER:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-virtual {p0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1282
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "mimetype=\'vnd.android.cursor.item/phone_v2\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1284
    :cond_1
    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->EMAIL_ADDRESS:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-virtual {p0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1285
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 1286
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " OR "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1288
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "mimetype=\'vnd.android.cursor.item/email_v2\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1290
    :cond_4
    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->ADDRESS:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-virtual {p0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {p0}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1291
    :cond_5
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_6

    .line 1292
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " OR "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1294
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "mimetype=\'vnd.android.cursor.item/postal-address_v2\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1296
    :cond_7
    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactLookupType;->BIRTHDAY:Lcom/vlingo/core/internal/contacts/ContactLookupType;

    invoke-virtual {p0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    invoke-virtual {p0}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1297
    :cond_8
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_9

    .line 1298
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " OR "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1300
    :cond_9
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "((mimetype=\'vnd.android.cursor.item/contact_event\' OR mimetype=\'vnd.pantech.cursor.item/lunar_event\') AND data2=3)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1304
    :cond_a
    return-object v0
.end method

.method public static varargs matchGenericWithMimeNorm(Ljava/util/List;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/OldContactDBUtil$ContactMatchQueryInterface;[Ljava/lang/String;)Ljava/lang/String;
    .locals 17
    .param p1, "column"    # Ljava/lang/String;
    .param p2, "cmqi"    # Lcom/vlingo/core/internal/contacts/OldContactDBUtil$ContactMatchQueryInterface;
    .param p3, "mimetype"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/OldContactDBUtil$ContactMatchQueryInterface;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1065
    .local p0, "words":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1066
    .local v9, "normalWords":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 1067
    .local v14, "word":Ljava/lang/String;
    invoke-static {v14}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->normalizeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1068
    .local v8, "normalWord":Ljava/lang/String;
    invoke-static {v8}, Lcom/vlingo/core/internal/util/StringUtils;->isBlank(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_0

    .line 1072
    if-eqz v8, :cond_1

    invoke-virtual {v8, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_1

    .line 1073
    const-string/jumbo v15, " "

    invoke-virtual {v8, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 1074
    .local v12, "rawWords":[Ljava/lang/String;
    array-length v15, v12

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 1075
    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1082
    .end local v12    # "rawWords":[Ljava/lang/String;
    :cond_1
    invoke-interface {v9, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1077
    .restart local v12    # "rawWords":[Ljava/lang/String;
    :cond_2
    move-object v2, v12

    .local v2, "arr$":[Ljava/lang/String;
    array-length v7, v2

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_1
    if-ge v6, v7, :cond_1

    aget-object v11, v2, v6

    .line 1078
    .local v11, "rawWord":Ljava/lang/String;
    invoke-interface {v9, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1077
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1084
    .end local v2    # "arr$":[Ljava/lang/String;
    .end local v6    # "i$":I
    .end local v7    # "len$":I
    .end local v8    # "normalWord":Ljava/lang/String;
    .end local v11    # "rawWord":Ljava/lang/String;
    .end local v12    # "rawWords":[Ljava/lang/String;
    .end local v14    # "word":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p3

    array-length v15, v0

    mul-int/lit8 v15, v15, 0x2

    new-array v10, v15, [Ljava/lang/String;

    .line 1085
    .local v10, "normalizedMimeTypes":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 1086
    .local v3, "i":I
    move-object/from16 v2, p3

    .restart local v2    # "arr$":[Ljava/lang/String;
    array-length v7, v2

    .restart local v7    # "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    move v4, v3

    .end local v3    # "i":I
    .local v4, "i":I
    :goto_2
    if-ge v5, v7, :cond_4

    aget-object v13, v2, v5

    .line 1087
    .local v13, "type":Ljava/lang/String;
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "i":I
    .restart local v3    # "i":I
    aput-object v13, v10, v4

    .line 1088
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .restart local v4    # "i":I
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v16, "_svoice_normalization"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v10, v3

    .line 1086
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1091
    .end local v13    # "type":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v9, v0, v1, v10}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->matchGenericWithNorm(Ljava/util/List;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/OldContactDBUtil$ContactMatchQueryInterface;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    return-object v15
.end method

.method private static varargs matchGenericWithNorm(Ljava/util/List;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/OldContactDBUtil$ContactMatchQueryInterface;[Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "column"    # Ljava/lang/String;
    .param p2, "cmqi"    # Lcom/vlingo/core/internal/contacts/OldContactDBUtil$ContactMatchQueryInterface;
    .param p3, "mimetype"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/OldContactDBUtil$ContactMatchQueryInterface;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1095
    .local p0, "words":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1096
    .local v0, "dispNameWhere":Ljava/lang/StringBuilder;
    const-string/jumbo v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1097
    invoke-static {p3}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->appendMatchToWhereClauseMimeType([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1098
    const-string/jumbo v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1099
    invoke-interface {p2, p0, p1}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil$ContactMatchQueryInterface;->generateBasicWhereClause(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1100
    const-string/jumbo v1, " ) "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1101
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static normalizeContactData(Landroid/content/ContentResolver;Ljava/util/List;)V
    .locals 32
    .param p0, "contentResolver"    # Landroid/content/ContentResolver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1618
    .local p1, "rawContactIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const-string/jumbo v3, ","

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/vlingo/core/internal/util/StringUtils;->join(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 1619
    .local v19, "joinedIds":Ljava/lang/String;
    const/4 v3, 0x6

    new-array v12, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "data1"

    aput-object v4, v12, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "data2"

    aput-object v4, v12, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "data3"

    aput-object v4, v12, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "data4"

    aput-object v4, v12, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "data5"

    aput-object v4, v12, v3

    const/4 v3, 0x5

    const-string/jumbo v4, "data6"

    aput-object v4, v12, v3

    .line 1620
    .local v12, "dataColumnNames":[Ljava/lang/String;
    const/16 v3, 0x8

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "mimetype"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "raw_contact_id"

    aput-object v4, v5, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "data1"

    aput-object v4, v5, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "data2"

    aput-object v4, v5, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "data3"

    aput-object v4, v5, v3

    const/4 v3, 0x5

    const-string/jumbo v4, "data4"

    aput-object v4, v5, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "data5"

    aput-object v4, v5, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "data6"

    aput-object v4, v5, v3

    .line 1621
    .local v5, "projection":[Ljava/lang/String;
    const/4 v10, 0x0

    .line 1623
    .local v10, "c":Landroid/database/Cursor;
    sget-object v30, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    .line 1624
    .local v30, "uri":Landroid/net/Uri;
    invoke-virtual/range {v30 .. v30}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string/jumbo v4, "caller_is_syncadapter"

    const-string/jumbo v6, "true"

    invoke-virtual {v3, v4, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v30

    .line 1626
    const/4 v3, 0x3

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/4 v3, 0x0

    const-string/jumbo v4, "vnd.android.cursor.item/name_svoice_normalization"

    aput-object v4, v21, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "vnd.android.cursor.item/nickname_svoice_normalization"

    aput-object v4, v21, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "vnd.android.cursor.item/organization_svoice_normalization"

    aput-object v4, v21, v3

    .line 1636
    .local v21, "mimetypeList":[Ljava/lang/String;
    :try_start_0
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    .line 1637
    .local v22, "minetypes":Ljava/lang/StringBuilder;
    const/4 v15, 0x1

    .line 1638
    .local v15, "first":Z
    move-object/from16 v9, v21

    .local v9, "arr$":[Ljava/lang/String;
    array-length v0, v9

    move/from16 v20, v0

    .local v20, "len$":I
    const/16 v17, 0x0

    .local v17, "i$":I
    :goto_0
    move/from16 v0, v17

    move/from16 v1, v20

    if-ge v0, v1, :cond_2

    aget-object v29, v9, v17

    .line 1639
    .local v29, "type":Ljava/lang/String;
    if-eqz v15, :cond_0

    .line 1640
    const/4 v15, 0x0

    .line 1644
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1638
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 1642
    :cond_0
    const-string/jumbo v3, ", "

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1708
    .end local v9    # "arr$":[Ljava/lang/String;
    .end local v15    # "first":Z
    .end local v17    # "i$":I
    .end local v20    # "len$":I
    .end local v22    # "minetypes":Ljava/lang/StringBuilder;
    .end local v29    # "type":Ljava/lang/String;
    :catch_0
    move-exception v14

    .line 1709
    .local v14, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v3, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "normalizeContactData: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v14}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1711
    if-eqz v10, :cond_1

    .line 1713
    :try_start_2
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 1719
    .end local v14    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_2
    return-void

    .line 1647
    .restart local v9    # "arr$":[Ljava/lang/String;
    .restart local v15    # "first":Z
    .restart local v17    # "i$":I
    .restart local v20    # "len$":I
    .restart local v22    # "minetypes":Ljava/lang/StringBuilder;
    :cond_2
    :try_start_3
    const-string/jumbo v3, "%s IN (%s) AND %s in (%s)"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "raw_contact_id"

    aput-object v7, v4, v6

    const/4 v6, 0x1

    aput-object v19, v4, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "mimetype"

    aput-object v7, v4, v6

    const/4 v6, 0x3

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v31

    .line 1648
    .local v31, "where":Ljava/lang/String;
    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v13

    .line 1657
    .local v13, "deletedItems":I
    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v3, "%s IN (?,?,?) AND %s IN (%s)"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string/jumbo v8, "mimetype"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string/jumbo v8, "raw_contact_id"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    aput-object v19, v6, v7

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const/4 v3, 0x3

    new-array v7, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v8, "vnd.android.cursor.item/name"

    aput-object v8, v7, v3

    const/4 v3, 0x1

    const-string/jumbo v8, "vnd.android.cursor.item/organization"

    aput-object v8, v7, v3

    const/4 v3, 0x2

    const-string/jumbo v8, "vnd.android.cursor.item/nickname"

    aput-object v8, v7, v3

    const/4 v8, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1665
    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    .line 1667
    .local v27, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1669
    .end local v17    # "i$":I
    :cond_3
    invoke-static/range {v30 .. v30}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v26

    .line 1670
    .local v26, "operationBuilder":Landroid/content/ContentProviderOperation$Builder;
    const-string/jumbo v3, "raw_contact_id"

    const-string/jumbo v4, "raw_contact_id"

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1671
    const-string/jumbo v3, "mimetype"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "mimetype"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "_svoice_normalization"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1673
    const/16 v16, 0x0

    .line 1674
    .local v16, "foundNormalizedData":Z
    move-object v9, v12

    array-length v0, v9

    move/from16 v20, v0

    const/16 v17, 0x0

    .restart local v17    # "i$":I
    move/from16 v18, v17

    .end local v17    # "i$":I
    .local v18, "i$":I
    :goto_3
    move/from16 v0, v18

    move/from16 v1, v20

    if-ge v0, v1, :cond_8

    aget-object v11, v9, v18

    .line 1677
    .local v11, "columnName":Ljava/lang/String;
    invoke-interface {v10, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 1678
    .local v24, "name":Ljava/lang/String;
    invoke-static/range {v24 .. v24}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 1679
    move-object/from16 v25, v24

    .line 1681
    .local v25, "normalizedName":Ljava/lang/String;
    const/16 v28, 0x0

    .line 1682
    .local v28, "saveNormalizedAnyway":Z
    sget-object v3, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->NORMALIZERS:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .end local v18    # "i$":I
    .local v17, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_4
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;

    .line 1683
    .local v23, "n":Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;
    invoke-virtual/range {v23 .. v23}, Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;->scoringOnly()Z

    move-result v3

    if-nez v3, :cond_4

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;->canNormalize(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1684
    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 1685
    move-object/from16 v0, v23

    instance-of v0, v0, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;

    move/from16 v28, v0

    goto :goto_4

    .line 1689
    .end local v23    # "n":Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;
    :cond_5
    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    if-eqz v28, :cond_7

    .line 1690
    :cond_6
    const/16 v16, 0x1

    .line 1691
    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v11, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1674
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v25    # "normalizedName":Ljava/lang/String;
    .end local v28    # "saveNormalizedAnyway":Z
    :cond_7
    add-int/lit8 v17, v18, 0x1

    .local v17, "i$":I
    move/from16 v18, v17

    .end local v17    # "i$":I
    .restart local v18    # "i$":I
    goto :goto_3

    .line 1697
    .end local v11    # "columnName":Ljava/lang/String;
    .end local v24    # "name":Ljava/lang/String;
    :cond_8
    if-eqz v16, :cond_9

    .line 1698
    invoke-virtual/range {v26 .. v26}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1700
    :cond_9
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1705
    .end local v16    # "foundNormalizedData":Z
    .end local v18    # "i$":I
    .end local v26    # "operationBuilder":Landroid/content/ContentProviderOperation$Builder;
    :cond_a
    invoke-static {}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->cleanNormalizers()V

    .line 1706
    const-string/jumbo v3, "com.android.contacts"

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1711
    if-eqz v10, :cond_1

    .line 1713
    :try_start_4
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_2

    .line 1714
    :catch_1
    move-exception v14

    .line 1715
    .restart local v14    # "e":Ljava/lang/Exception;
    sget-object v3, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "normalizeContactData: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v14}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1714
    .end local v9    # "arr$":[Ljava/lang/String;
    .end local v13    # "deletedItems":I
    .end local v15    # "first":Z
    .end local v20    # "len$":I
    .end local v22    # "minetypes":Ljava/lang/StringBuilder;
    .end local v27    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v31    # "where":Ljava/lang/String;
    :catch_2
    move-exception v14

    .line 1715
    sget-object v3, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "normalizeContactData: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v14}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1711
    .end local v14    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v10, :cond_b

    .line 1713
    :try_start_5
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 1716
    :cond_b
    :goto_5
    throw v3

    .line 1714
    :catch_3
    move-exception v14

    .line 1715
    .restart local v14    # "e":Ljava/lang/Exception;
    sget-object v4, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "normalizeContactData: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v14}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public static normalizeValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 123
    sget-object v2, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->NORMALIZERS:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;

    .line 124
    .local v1, "n":Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;
    invoke-virtual {v1, p0}, Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;->canNormalize(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 125
    invoke-virtual {v1, p0}, Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 128
    .end local v1    # "n":Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;
    :cond_1
    return-object p0
.end method

.method public static varargs queryToWhereClauseWithNormalization(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "query"    # Ljava/lang/String;
    .param p1, "mimetype"    # Ljava/lang/String;
    .param p2, "fields"    # [Ljava/lang/String;

    .prologue
    .line 792
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 793
    .local v0, "dispNameWhere":Ljava/lang/StringBuilder;
    const-string/jumbo v5, "\'"

    const-string/jumbo v6, "\'\'"

    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 795
    .local v4, "word":Ljava/lang/String;
    invoke-static {v4, p1, p2}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->basicClauseWithMime(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 796
    .local v1, "nonNormalClause":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->normalizeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 797
    .local v3, "normalWord":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 798
    move-object v3, v4

    .line 801
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "_svoice_normalization"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5, p2}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->basicClauseWithMime(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 802
    .local v2, "normalClause":Ljava/lang/String;
    const-string/jumbo v5, " ( "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 803
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 804
    const-string/jumbo v5, " OR "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 805
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 806
    const-string/jumbo v5, " ) "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 807
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public static varargs queryToWhereClauseWithTwoWayNormalization(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "query"    # Ljava/lang/String;
    .param p1, "mimetype"    # Ljava/lang/String;
    .param p2, "fields"    # [Ljava/lang/String;

    .prologue
    .line 817
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 818
    .local v0, "dispNameWhere":Ljava/lang/StringBuilder;
    const-string/jumbo v4, "\'"

    const-string/jumbo v5, "\'\'"

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 820
    .local v2, "word":Ljava/lang/String;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 821
    .local v3, "words":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 822
    invoke-static {v2}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->normalizeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 823
    .local v1, "normalWord":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 824
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 827
    :cond_0
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "_svoice_normalization"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->appendMatchToWhereClauseMimeType([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 828
    const-string/jumbo v4, " AND "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 829
    invoke-static {v3, p2}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->basicClauseList(Ljava/util/List;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 831
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static varargs queryToWhereClauseWithoutNormalization(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "query"    # Ljava/lang/String;
    .param p1, "mimetype"    # Ljava/lang/String;
    .param p2, "fields"    # [Ljava/lang/String;

    .prologue
    .line 811
    const-string/jumbo v1, "\'"

    const-string/jumbo v2, "\'\'"

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 812
    .local v0, "word":Ljava/lang/String;
    invoke-static {v0, p1, p2}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->basicClauseWithMime(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public convertEmailType(I)I
    .locals 1
    .param p1, "emailType"    # I

    .prologue
    .line 1261
    const/4 v0, 0x0

    .line 1262
    .local v0, "ret":I
    packed-switch p1, :pswitch_data_0

    .line 1271
    const/4 v0, 0x7

    .line 1274
    :goto_0
    return v0

    .line 1264
    :pswitch_0
    const/4 v0, 0x3

    .line 1265
    goto :goto_0

    .line 1267
    :pswitch_1
    const/4 v0, 0x1

    .line 1268
    goto :goto_0

    .line 1262
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public findMatchesByName(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)V
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 591
    .local p3, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    .local p4, "matchTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p5, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const-string/jumbo v14, "mimetype=\'vnd.android.cursor.item/name\' OR mimetype=\'vnd.android.cursor.item/nickname\' OR mimetype=\'vnd.android.cursor.item/organization\'"

    .line 594
    .local v14, "mimeWhere":Ljava/lang/String;
    const/4 v5, 0x0

    .line 597
    .local v5, "isKorean":Z
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 685
    :cond_0
    return-void

    .line 600
    :cond_1
    const/16 v19, 0x0

    .line 601
    .local v19, "where":[Ljava/lang/String;
    if-eqz p2, :cond_8

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_8

    .line 602
    const/4 v15, 0x0

    .line 603
    .local v15, "nameWhere":[Ljava/lang/String;
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/util/StringUtils;->nameIsKorean(Ljava/lang/String;)Z

    move-result v5

    .line 604
    if-eqz v5, :cond_2

    .line 606
    const-string/jumbo v1, "data1"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->getKoreanDisplayNameWhereClause(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 610
    array-length v1, v15

    new-array v0, v1, [Ljava/lang/String;

    move-object/from16 v19, v0

    .line 611
    const/4 v10, 0x0

    .line 612
    .local v10, "i":I
    move-object v9, v15

    .local v9, "arr$":[Ljava/lang/String;
    array-length v13, v9

    .local v13, "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    move v11, v10

    .end local v10    # "i":I
    .local v11, "i":I
    :goto_0
    if-ge v12, v13, :cond_5

    aget-object v17, v9, v12

    .line 613
    .local v17, "nw":Ljava/lang/String;
    add-int/lit8 v10, v11, 0x1

    .end local v11    # "i":I
    .restart local v10    # "i":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") AND ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "mimetype=\'vnd.android.cursor.item/name\' OR mimetype=\'vnd.android.cursor.item/nickname\' OR mimetype=\'vnd.android.cursor.item/organization\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v19, v11

    .line 612
    add-int/lit8 v12, v12, 0x1

    move v11, v10

    .end local v10    # "i":I
    .restart local v11    # "i":I
    goto :goto_0

    .line 628
    .end local v9    # "arr$":[Ljava/lang/String;
    .end local v11    # "i":I
    .end local v12    # "i$":I
    .end local v13    # "len$":I
    .end local v17    # "nw":Ljava/lang/String;
    :cond_2
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/util/StringUtils;->nameIsJapanese(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 629
    const-string/jumbo v1, "data1"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->getJapaneseDisplayNameWhereClause(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 630
    array-length v1, v15

    new-array v0, v1, [Ljava/lang/String;

    move-object/from16 v19, v0

    .line 631
    const/4 v10, 0x0

    .line 632
    .restart local v10    # "i":I
    move-object v9, v15

    .restart local v9    # "arr$":[Ljava/lang/String;
    array-length v13, v9

    .restart local v13    # "len$":I
    const/4 v12, 0x0

    .restart local v12    # "i$":I
    move v11, v10

    .end local v10    # "i":I
    .restart local v11    # "i":I
    :goto_1
    if-ge v12, v13, :cond_5

    aget-object v17, v9, v12

    .line 633
    .restart local v17    # "nw":Ljava/lang/String;
    add-int/lit8 v10, v11, 0x1

    .end local v11    # "i":I
    .restart local v10    # "i":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") AND ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "mimetype=\'vnd.android.cursor.item/name\' OR mimetype=\'vnd.android.cursor.item/nickname\' OR mimetype=\'vnd.android.cursor.item/organization\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v19, v11

    .line 632
    add-int/lit8 v12, v12, 0x1

    move v11, v10

    .end local v10    # "i":I
    .restart local v11    # "i":I
    goto :goto_1

    .line 635
    .end local v9    # "arr$":[Ljava/lang/String;
    .end local v11    # "i":I
    .end local v12    # "i$":I
    .end local v13    # "len$":I
    .end local v17    # "nw":Ljava/lang/String;
    :cond_3
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/util/StringUtils;->isChineseString(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 636
    const-string/jumbo v1, "data1"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->getChineseDisplayNameWhereClause(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 637
    array-length v1, v15

    new-array v0, v1, [Ljava/lang/String;

    move-object/from16 v19, v0

    .line 638
    const/4 v10, 0x0

    .line 639
    .restart local v10    # "i":I
    move-object v9, v15

    .restart local v9    # "arr$":[Ljava/lang/String;
    array-length v13, v9

    .restart local v13    # "len$":I
    const/4 v12, 0x0

    .restart local v12    # "i$":I
    move v11, v10

    .end local v10    # "i":I
    .restart local v11    # "i":I
    :goto_2
    if-ge v12, v13, :cond_5

    aget-object v17, v9, v12

    .line 640
    .restart local v17    # "nw":Ljava/lang/String;
    add-int/lit8 v10, v11, 0x1

    .end local v11    # "i":I
    .restart local v10    # "i":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") AND ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "mimetype=\'vnd.android.cursor.item/name\' OR mimetype=\'vnd.android.cursor.item/nickname\' OR mimetype=\'vnd.android.cursor.item/organization\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v19, v11

    .line 639
    add-int/lit8 v12, v12, 0x1

    move v11, v10

    .end local v10    # "i":I
    .restart local v11    # "i":I
    goto :goto_2

    .line 643
    .end local v9    # "arr$":[Ljava/lang/String;
    .end local v11    # "i":I
    .end local v12    # "i$":I
    .end local v13    # "len$":I
    .end local v17    # "nw":Ljava/lang/String;
    :cond_4
    move-object/from16 v16, p2

    .line 645
    .local v16, "newQuery":Ljava/lang/String;
    const-string/jumbo v1, " "

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    .line 647
    .local v18, "rawWords":[Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/vlingo/core/internal/util/StringUtils;->stringIsNonAsciiWithCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 648
    invoke-static/range {v18 .. v18}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->augmentWords([Ljava/lang/String;)Ljava/util/List;

    move-result-object v21

    .line 652
    .local v21, "words":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_3
    new-instance v20, Ljava/lang/StringBuilder;

    const/16 v1, 0x400

    move-object/from16 v0, v20

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 654
    .local v20, "whereBuilder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "("

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v6, "vnd.android.cursor.item/name"

    aput-object v6, v2, v3

    invoke-static {v2}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->appendMatchToWhereClauseMimeType([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " AND ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "data2"

    move-object/from16 v0, v21

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->appendMatchToWhereClauseOnly(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " OR "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "data5"

    move-object/from16 v0, v21

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->appendMatchToWhereClauseOnly(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " OR "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "data3"

    move-object/from16 v0, v21

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->appendMatchToWhereClauseOnly(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")) OR (("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 660
    const-string/jumbo v1, "data1"

    const-string/jumbo v2, "data1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 661
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "vnd.android.cursor.item/nickname"

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->appendMatchToWhereClauseMimeType([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " OR "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v6, "vnd.android.cursor.item/organization"

    aput-object v6, v2, v3

    invoke-static {v2}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->appendMatchToWhereClauseMimeType([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "data1"

    move-object/from16 v0, v21

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->appendMatchToWhereClauseOnly(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 671
    :goto_4
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    move-object/from16 v19, v0

    .line 672
    const/4 v1, 0x0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v19, v1

    .line 680
    .end local v15    # "nameWhere":[Ljava/lang/String;
    .end local v16    # "newQuery":Ljava/lang/String;
    .end local v18    # "rawWords":[Ljava/lang/String;
    .end local v20    # "whereBuilder":Ljava/lang/StringBuilder;
    .end local v21    # "words":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_5
    :goto_5
    move-object/from16 v9, v19

    .restart local v9    # "arr$":[Ljava/lang/String;
    array-length v13, v9

    .restart local v13    # "len$":I
    const/4 v12, 0x0

    .restart local v12    # "i$":I
    :goto_6
    if-ge v12, v13, :cond_0

    aget-object v4, v9, v12

    .local v4, "sWhere":Ljava/lang/String;
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    .line 681
    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->findMatchesByNamePerPass(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 680
    add-int/lit8 v12, v12, 0x1

    goto :goto_6

    .line 650
    .end local v4    # "sWhere":Ljava/lang/String;
    .end local v9    # "arr$":[Ljava/lang/String;
    .end local v12    # "i$":I
    .end local v13    # "len$":I
    .restart local v15    # "nameWhere":[Ljava/lang/String;
    .restart local v16    # "newQuery":Ljava/lang/String;
    .restart local v18    # "rawWords":[Ljava/lang/String;
    :cond_6
    invoke-static/range {v18 .. v18}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v21

    .restart local v21    # "words":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto/16 :goto_3

    .line 665
    .restart local v20    # "whereBuilder":Ljava/lang/StringBuilder;
    :cond_7
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "vnd.android.cursor.item/nickname"

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->appendMatchToWhereClauseMimeType([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "data1"

    move-object/from16 v0, v21

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->appendMatchToWhereClauseOnly(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") OR ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v6, "vnd.android.cursor.item/organization"

    aput-object v6, v2, v3

    invoke-static {v2}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->appendMatchToWhereClauseMimeType([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "data1"

    move-object/from16 v0, v21

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->appendMatchToWhereClauseOnly(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "))"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 676
    .end local v15    # "nameWhere":[Ljava/lang/String;
    .end local v16    # "newQuery":Ljava/lang/String;
    .end local v18    # "rawWords":[Ljava/lang/String;
    .end local v20    # "whereBuilder":Ljava/lang/StringBuilder;
    .end local v21    # "words":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_8
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    move-object/from16 v19, v0

    .line 677
    const/4 v1, 0x0

    const-string/jumbo v2, "(mimetype=\'vnd.android.cursor.item/name\' OR mimetype=\'vnd.android.cursor.item/nickname\' OR mimetype=\'vnd.android.cursor.item/organization\')"

    aput-object v2, v19, v1

    goto/16 :goto_5
.end method

.method public findMatchesByName2(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;Z)V
    .locals 30
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p6, "forPhone"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 164
    .local p3, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    .local p4, "matchTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p5, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    const/16 v24, 0x0

    .line 168
    .local v24, "isQueryHasLatinSymbols":Z
    sget-object v4, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->RULE_SETS:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_2
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;

    .line 169
    .local v15, "ruleSet":Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;
    if-eqz v24, :cond_3

    instance-of v4, v15, Lcom/vlingo/core/internal/contacts/rules/EFIGSContactRuleSet;

    if-eqz v4, :cond_2

    .line 172
    :cond_3
    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->canProcess(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 173
    invoke-virtual {v15}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->getExitCriteria()Lcom/vlingo/core/internal/contacts/ContactExitCriteria;

    move-result-object v21

    .line 174
    .local v21, "exitCriteria":Lcom/vlingo/core/internal/contacts/ContactExitCriteria;
    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;->generateRules(Ljava/lang/String;)Ljava/util/List;

    move-result-object v28

    .line 175
    .local v28, "rules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactRule;>;"
    const/16 v27, 0x0

    .line 176
    .local v27, "nRuleCnt":I
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .local v23, "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/vlingo/core/internal/contacts/ContactRule;

    .line 177
    .local v14, "rule":Lcom/vlingo/core/internal/contacts/ContactRule;
    invoke-virtual {v14}, Lcom/vlingo/core/internal/contacts/ContactRule;->getQuery()Ljava/lang/String;

    move-result-object v7

    .line 180
    .local v7, "query":Ljava/lang/String;
    add-int/lit8 v27, v27, 0x1

    .line 181
    invoke-virtual {v14}, Lcom/vlingo/core/internal/contacts/ContactRule;->getScore()Lcom/vlingo/core/internal/contacts/scoring/ContactScore;

    move-result-object v29

    .line 183
    .local v29, "scorer":Lcom/vlingo/core/internal/contacts/scoring/ContactScore;
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 184
    .local v10, "localMatches":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/4 v8, 0x0

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v9, p3

    move-object/from16 v11, p5

    invoke-virtual/range {v4 .. v11}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->findMatchesByNamePerPass(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)Z

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move-object/from16 v13, p2

    move-object/from16 v16, p3

    move-object/from16 v17, v10

    move-object/from16 v18, p5

    .line 188
    invoke-direct/range {v11 .. v18}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->additionalPhoneticMatching(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactRule;Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)V

    .line 191
    invoke-virtual {v10}, Ljava/util/HashMap;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    .line 193
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    invoke-direct {v0, v1, v2, v10, v3}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->getAllContactData(Landroid/content/Context;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)I

    move-result v19

    .line 198
    .local v19, "count":I
    invoke-virtual {v10}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v25

    .line 199
    .local v25, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;>;"
    :cond_5
    :goto_1
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 200
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/Map$Entry;

    .line 201
    .local v20, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 202
    .local v26, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-nez p6, :cond_6

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/contacts/ContactMatch;->containsData()Z

    move-result v4

    if-eqz v4, :cond_7

    :cond_6
    move-object/from16 v0, v26

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->hasLookupData(Ljava/util/EnumSet;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 205
    :cond_7
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .end local v20    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v26    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_8
    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move-object/from16 v11, v29

    move-object/from16 v12, v21

    move-object/from16 v13, p2

    .line 213
    invoke-static/range {v8 .. v13}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->applyScores(Ljava/util/HashMap;Ljava/util/List;Ljava/util/HashMap;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;Lcom/vlingo/core/internal/contacts/ContactExitCriteria;Ljava/lang/String;)V

    .line 216
    invoke-virtual/range {v21 .. v21}, Lcom/vlingo/core/internal/contacts/ContactExitCriteria;->returnMatches()Z

    move-result v4

    if-eqz v4, :cond_4

    goto/16 :goto_0

    .line 222
    .end local v7    # "query":Ljava/lang/String;
    .end local v10    # "localMatches":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v14    # "rule":Lcom/vlingo/core/internal/contacts/ContactRule;
    .end local v19    # "count":I
    .end local v25    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;>;"
    .end local v29    # "scorer":Lcom/vlingo/core/internal/contacts/scoring/ContactScore;
    :cond_9
    instance-of v4, v15, Lcom/vlingo/core/internal/contacts/rules/EFIGSContactRuleSet;

    if-nez v4, :cond_0

    invoke-virtual/range {p4 .. p4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 225
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/util/StringUtils;->stringHasLatinSymbols(Ljava/lang/String;)Z

    move-result v24

    .line 226
    if-nez v24, :cond_2

    goto/16 :goto_0
.end method

.method public findMatchesByNamePerPass(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)Z
    .locals 36
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "where"    # Ljava/lang/String;
    .param p4, "skipExtraData"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 694
    .local p5, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    .local p6, "matchTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p7, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/16 v2, 0xc

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v11, "contact_id"

    aput-object v11, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v11, "raw_contact_id"

    aput-object v11, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v11, "lookup"

    aput-object v11, v4, v2

    const/4 v2, 0x3

    const-string/jumbo v11, "display_name"

    aput-object v11, v4, v2

    const/4 v2, 0x4

    const-string/jumbo v11, "starred"

    aput-object v11, v4, v2

    const/4 v2, 0x5

    const-string/jumbo v11, "data1"

    aput-object v11, v4, v2

    const/4 v2, 0x6

    const-string/jumbo v11, "data2"

    aput-object v11, v4, v2

    const/4 v2, 0x7

    const-string/jumbo v11, "data3"

    aput-object v11, v4, v2

    const/16 v2, 0x8

    const-string/jumbo v11, "data7"

    aput-object v11, v4, v2

    const/16 v2, 0x9

    const-string/jumbo v11, "data9"

    aput-object v11, v4, v2

    const/16 v2, 0xa

    const-string/jumbo v11, "times_contacted"

    aput-object v11, v4, v2

    const/16 v2, 0xb

    const-string/jumbo v11, "phonetic_name"

    aput-object v11, v4, v2

    .line 709
    .local v4, "proj":[Ljava/lang/String;
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    .line 711
    .local v3, "lookupUri":Landroid/net/Uri;
    const/16 v33, 0x0

    .line 714
    .local v33, "retVal":Z
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 715
    const/4 v2, 0x0

    .line 786
    :goto_0
    return v2

    .line 719
    :cond_0
    const/4 v12, 0x0

    .line 720
    .local v12, "c":Landroid/database/Cursor;
    const/16 v32, 0x0

    .line 722
    .local v32, "recordsFound":I
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v5, p3

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 723
    if-eqz v12, :cond_2

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 724
    const-string/jumbo v2, "raw_contact_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 725
    .local v22, "colIndexRawContactID":I
    const-string/jumbo v2, "contact_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 726
    .local v13, "colIndexContactID":I
    const-string/jumbo v2, "lookup"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 727
    .local v19, "colIndexLookupKey":I
    const-string/jumbo v2, "display_name"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 728
    .local v20, "colIndexName":I
    const-string/jumbo v2, "starred"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 729
    .local v23, "colIndexStarred":I
    const-string/jumbo v2, "data1"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 730
    .local v14, "colIndexData1":I
    const-string/jumbo v2, "data2"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 731
    .local v15, "colIndexData2":I
    const-string/jumbo v2, "data3"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 732
    .local v16, "colIndexData3":I
    const-string/jumbo v2, "data7"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 733
    .local v17, "colIndexData7":I
    const-string/jumbo v2, "data9"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 734
    .local v18, "colIndexData9":I
    const-string/jumbo v2, "times_contacted"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 735
    .local v24, "colIndexTimesContacted":I
    const-string/jumbo v2, "phonetic_name"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 738
    .local v21, "colIndexPhoneticName":I
    :cond_1
    invoke-interface {v12, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 739
    .local v8, "contactID":J
    invoke-interface {v12, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 740
    .local v25, "data1":Ljava/lang/String;
    invoke-interface {v12, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 741
    .local v26, "firstName":Ljava/lang/String;
    move/from16 v0, v16

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 742
    .local v27, "lastName":Ljava/lang/String;
    move/from16 v0, v17

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 743
    .local v28, "phoneticFirstName":Ljava/lang/String;
    move/from16 v0, v18

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v29

    .line 744
    .local v29, "phoneticLastName":Ljava/lang/String;
    move/from16 v0, v20

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 745
    .local v6, "name":Ljava/lang/String;
    move/from16 v0, v24

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v35

    .line 746
    .local v35, "timesContacted":I
    move/from16 v0, v21

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 747
    .local v7, "phoneticName":Ljava/lang/String;
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 750
    .local v5, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-eqz p4, :cond_4

    move-object/from16 v0, p2

    invoke-static {v0, v6}, Lcom/vlingo/core/internal/util/StringUtils;->matchesKoreanNamePattern(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 776
    :goto_1
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 781
    .end local v5    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v6    # "name":Ljava/lang/String;
    .end local v7    # "phoneticName":Ljava/lang/String;
    .end local v8    # "contactID":J
    .end local v13    # "colIndexContactID":I
    .end local v14    # "colIndexData1":I
    .end local v15    # "colIndexData2":I
    .end local v16    # "colIndexData3":I
    .end local v17    # "colIndexData7":I
    .end local v18    # "colIndexData9":I
    .end local v19    # "colIndexLookupKey":I
    .end local v20    # "colIndexName":I
    .end local v21    # "colIndexPhoneticName":I
    .end local v22    # "colIndexRawContactID":I
    .end local v23    # "colIndexStarred":I
    .end local v24    # "colIndexTimesContacted":I
    .end local v25    # "data1":Ljava/lang/String;
    .end local v26    # "firstName":Ljava/lang/String;
    .end local v27    # "lastName":Ljava/lang/String;
    .end local v28    # "phoneticFirstName":Ljava/lang/String;
    .end local v29    # "phoneticLastName":Ljava/lang/String;
    .end local v35    # "timesContacted":I
    :cond_2
    if-eqz v12, :cond_3

    .line 782
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_3
    move/from16 v2, v33

    .line 786
    goto/16 :goto_0

    .line 754
    .restart local v5    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v6    # "name":Ljava/lang/String;
    .restart local v7    # "phoneticName":Ljava/lang/String;
    .restart local v8    # "contactID":J
    .restart local v13    # "colIndexContactID":I
    .restart local v14    # "colIndexData1":I
    .restart local v15    # "colIndexData2":I
    .restart local v16    # "colIndexData3":I
    .restart local v17    # "colIndexData7":I
    .restart local v18    # "colIndexData9":I
    .restart local v19    # "colIndexLookupKey":I
    .restart local v20    # "colIndexName":I
    .restart local v21    # "colIndexPhoneticName":I
    .restart local v22    # "colIndexRawContactID":I
    .restart local v23    # "colIndexStarred":I
    .restart local v24    # "colIndexTimesContacted":I
    .restart local v25    # "data1":Ljava/lang/String;
    .restart local v26    # "firstName":Ljava/lang/String;
    .restart local v27    # "lastName":Ljava/lang/String;
    .restart local v28    # "phoneticFirstName":Ljava/lang/String;
    .restart local v29    # "phoneticLastName":Ljava/lang/String;
    .restart local v35    # "timesContacted":I
    :cond_4
    if-eqz v5, :cond_6

    .line 755
    :try_start_1
    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addExtraName(Ljava/lang/String;)V

    .line 768
    :goto_2
    move/from16 v0, v22

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v30

    .line 769
    .local v30, "rawContactID":J
    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 770
    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 771
    move-wide/from16 v0, v30

    invoke-virtual {v5, v0, v1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->setRawContactID(J)V

    .line 772
    add-int/lit8 v32, v32, 0x1

    .line 775
    :cond_5
    const/16 v33, 0x1

    goto :goto_1

    .line 758
    .end local v30    # "rawContactID":J
    :cond_6
    move/from16 v0, v19

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 759
    .local v10, "lookupKey":Ljava/lang/String;
    move/from16 v0, v23

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v34

    .line 760
    .local v34, "starred":I
    new-instance v5, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .end local v5    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    const/4 v2, 0x1

    move/from16 v0, v34

    if-ne v0, v2, :cond_8

    const/4 v11, 0x1

    :goto_3
    invoke-direct/range {v5 .. v11}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V

    .line 761
    .restart local v5    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v0, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 762
    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addExtraName(Ljava/lang/String;)V

    .line 763
    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v5, v0, v1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addNameFields(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v5, v0, v1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addPhoneticFields(Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    move/from16 v0, v35

    invoke-virtual {v5, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->setTimesContacted(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 781
    .end local v5    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v6    # "name":Ljava/lang/String;
    .end local v7    # "phoneticName":Ljava/lang/String;
    .end local v8    # "contactID":J
    .end local v10    # "lookupKey":Ljava/lang/String;
    .end local v13    # "colIndexContactID":I
    .end local v14    # "colIndexData1":I
    .end local v15    # "colIndexData2":I
    .end local v16    # "colIndexData3":I
    .end local v17    # "colIndexData7":I
    .end local v18    # "colIndexData9":I
    .end local v19    # "colIndexLookupKey":I
    .end local v20    # "colIndexName":I
    .end local v21    # "colIndexPhoneticName":I
    .end local v22    # "colIndexRawContactID":I
    .end local v23    # "colIndexStarred":I
    .end local v24    # "colIndexTimesContacted":I
    .end local v25    # "data1":Ljava/lang/String;
    .end local v26    # "firstName":Ljava/lang/String;
    .end local v27    # "lastName":Ljava/lang/String;
    .end local v28    # "phoneticFirstName":Ljava/lang/String;
    .end local v29    # "phoneticLastName":Ljava/lang/String;
    .end local v34    # "starred":I
    .end local v35    # "timesContacted":I
    :catchall_0
    move-exception v2

    if-eqz v12, :cond_7

    .line 782
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v2

    .line 760
    .restart local v6    # "name":Ljava/lang/String;
    .restart local v7    # "phoneticName":Ljava/lang/String;
    .restart local v8    # "contactID":J
    .restart local v10    # "lookupKey":Ljava/lang/String;
    .restart local v13    # "colIndexContactID":I
    .restart local v14    # "colIndexData1":I
    .restart local v15    # "colIndexData2":I
    .restart local v16    # "colIndexData3":I
    .restart local v17    # "colIndexData7":I
    .restart local v18    # "colIndexData9":I
    .restart local v19    # "colIndexLookupKey":I
    .restart local v20    # "colIndexName":I
    .restart local v21    # "colIndexPhoneticName":I
    .restart local v22    # "colIndexRawContactID":I
    .restart local v23    # "colIndexStarred":I
    .restart local v24    # "colIndexTimesContacted":I
    .restart local v25    # "data1":Ljava/lang/String;
    .restart local v26    # "firstName":Ljava/lang/String;
    .restart local v27    # "lastName":Ljava/lang/String;
    .restart local v28    # "phoneticFirstName":Ljava/lang/String;
    .restart local v29    # "phoneticLastName":Ljava/lang/String;
    .restart local v34    # "starred":I
    .restart local v35    # "timesContacted":I
    :cond_8
    const/4 v11, 0x0

    goto :goto_3
.end method

.method public getContactDataByEmail(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactData;
    .locals 30
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "email"    # Ljava/lang/String;

    .prologue
    .line 1374
    const/16 v18, 0x0

    .line 1375
    .local v18, "c":Landroid/database/Cursor;
    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    .line 1381
    .local v3, "lookupUri":Landroid/net/Uri;
    const/16 v2, 0xa

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v12, "contact_id"

    aput-object v12, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v12, "raw_contact_id"

    aput-object v12, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v12, "lookup"

    aput-object v12, v4, v2

    const/4 v2, 0x3

    const-string/jumbo v12, "display_name"

    aput-object v12, v4, v2

    const/4 v2, 0x4

    const-string/jumbo v12, "starred"

    aput-object v12, v4, v2

    const/4 v2, 0x5

    const-string/jumbo v12, "data1"

    aput-object v12, v4, v2

    const/4 v2, 0x6

    const-string/jumbo v12, "data2"

    aput-object v12, v4, v2

    const/4 v2, 0x7

    const-string/jumbo v12, "is_super_primary"

    aput-object v12, v4, v2

    const/16 v2, 0x8

    const-string/jumbo v12, "times_contacted"

    aput-object v12, v4, v2

    const/16 v2, 0x9

    const-string/jumbo v12, "phonetic_name"

    aput-object v12, v4, v2

    .line 1395
    .local v4, "proj":[Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1396
    const/4 v12, 0x0

    .line 1439
    :cond_0
    :goto_0
    return-object v12

    .line 1398
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "data1 like \'"

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v12, "\'"

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1403
    .local v5, "where":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 1404
    if-eqz v18, :cond_3

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->isLast()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1406
    const-string/jumbo v2, "contact_id"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 1407
    .local v19, "colIndexContactID":I
    const-string/jumbo v2, "lookup"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 1408
    .local v23, "colIndexLookupKey":I
    const-string/jumbo v2, "display_name"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 1409
    .local v24, "colIndexName":I
    const-string/jumbo v2, "starred"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    .line 1410
    .local v26, "colIndexStarred":I
    const-string/jumbo v2, "data1"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 1411
    .local v21, "colIndexEmail":I
    const-string/jumbo v2, "data2"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 1412
    .local v20, "colIndexData2":I
    const-string/jumbo v2, "is_super_primary"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 1413
    .local v22, "colIndexIsDefault":I
    const-string/jumbo v2, "times_contacted"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    .line 1414
    .local v27, "colIndexTimesContacted":I
    const-string/jumbo v2, "phonetic_name"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    .line 1417
    .local v25, "colIndexPhoneticName":I
    invoke-interface/range {v18 .. v19}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 1418
    .local v9, "contactID":J
    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1419
    .local v11, "lookupKey":Ljava/lang/String;
    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1420
    .local v7, "name":Ljava/lang/String;
    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v28

    .line 1421
    .local v28, "starred":I
    move-object/from16 v0, v18

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1422
    .local v8, "phoneticName":Ljava/lang/String;
    new-instance v6, Lcom/vlingo/core/internal/contacts/ContactMatch;

    const/4 v2, 0x1

    move/from16 v0, v28

    if-ne v0, v2, :cond_2

    const/4 v12, 0x1

    :goto_1
    invoke-direct/range {v6 .. v12}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V

    .line 1424
    .local v6, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1425
    .local v15, "data1":Ljava/lang/String;
    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 1426
    .local v16, "data2":I
    move-object/from16 v0, v18

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v29

    .line 1427
    .local v29, "timesContacted":I
    invoke-virtual {v6, v15}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addExtraName(Ljava/lang/String;)V

    .line 1428
    move/from16 v0, v29

    invoke-virtual {v6, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->setTimesContacted(I)V

    .line 1429
    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 1431
    .local v17, "isDefault":I
    new-instance v12, Lcom/vlingo/core/internal/contacts/ContactData;

    sget-object v14, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Email:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    move-object v13, v6

    invoke-direct/range {v12 .. v17}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1434
    if-eqz v18, :cond_0

    .line 1435
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1422
    .end local v6    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v15    # "data1":Ljava/lang/String;
    .end local v16    # "data2":I
    .end local v17    # "isDefault":I
    .end local v29    # "timesContacted":I
    :cond_2
    const/4 v12, 0x0

    goto :goto_1

    .line 1434
    .end local v7    # "name":Ljava/lang/String;
    .end local v8    # "phoneticName":Ljava/lang/String;
    .end local v9    # "contactID":J
    .end local v11    # "lookupKey":Ljava/lang/String;
    .end local v19    # "colIndexContactID":I
    .end local v20    # "colIndexData2":I
    .end local v21    # "colIndexEmail":I
    .end local v22    # "colIndexIsDefault":I
    .end local v23    # "colIndexLookupKey":I
    .end local v24    # "colIndexName":I
    .end local v25    # "colIndexPhoneticName":I
    .end local v26    # "colIndexStarred":I
    .end local v27    # "colIndexTimesContacted":I
    .end local v28    # "starred":I
    :cond_3
    if-eqz v18, :cond_4

    .line 1435
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 1439
    :cond_4
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 1434
    :catchall_0
    move-exception v2

    if-eqz v18, :cond_5

    .line 1435
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v2
.end method

.method public getContactDetails(Landroid/content/Context;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)I
    .locals 41
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1133
    .local p2, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    .local p3, "matchTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p4, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/16 v29, 0x0

    .line 1134
    .local v29, "count":I
    const/4 v2, 0x6

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "contact_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "data1"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "data2"

    aput-object v3, v4, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "data3"

    aput-object v3, v4, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "mimetype"

    aput-object v3, v4, v2

    const/4 v2, 0x5

    const-string/jumbo v3, "is_super_primary"

    aput-object v3, v4, v2

    .line 1142
    .local v4, "proj":[Ljava/lang/String;
    new-instance v40, Ljava/lang/StringBuilder;

    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->getWhere(Ljava/util/EnumSet;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1143
    .local v40, "whereBuilder":Ljava/lang/StringBuilder;
    const-string/jumbo v2, " AND "

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " IN ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144
    const/16 v30, 0x0

    .line 1145
    .local v30, "counter":I
    const/16 v33, 0x0

    .local v33, "i":I
    :goto_0
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v33

    if-ge v0, v2, :cond_1

    .line 1146
    const-string/jumbo v2, "?"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1147
    add-int/lit8 v30, v30, 0x1

    .line 1148
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v30

    if-ge v0, v2, :cond_0

    .line 1149
    const-string/jumbo v2, ","

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145
    :cond_0
    add-int/lit8 v33, v33, 0x1

    goto :goto_0

    .line 1152
    :cond_1
    const-string/jumbo v2, ")"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1156
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1157
    const/4 v2, 0x0

    .line 1257
    :goto_1
    return v2

    .line 1159
    :cond_2
    const/16 v32, 0x1

    .line 1160
    .local v32, "firstTime":Z
    const/16 v21, 0x0

    .local v21, "colIndexContactID":I
    const/16 v22, 0x0

    .local v22, "colIndexData1":I
    const/16 v23, 0x0

    .local v23, "colIndexData2":I
    const/16 v24, 0x0

    .local v24, "colIndexData3":I
    const/16 v26, 0x0

    .local v26, "colMime":I
    const/16 v25, 0x0

    .line 1167
    .local v25, "colIsDefault":I
    const/16 v20, 0x0

    .line 1172
    .local v20, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v2

    new-array v6, v2, [Ljava/lang/String;

    .line 1174
    .local v6, "rawContactIDsString":[Ljava/lang/String;
    const/16 v33, 0x0

    .line 1175
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v34

    .local v34, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/lang/Long;

    .line 1176
    .local v35, "l":Ljava/lang/Long;
    invoke-static/range {v35 .. v35}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v33

    .line 1177
    add-int/lit8 v33, v33, 0x1

    goto :goto_2

    .line 1180
    .end local v35    # "l":Ljava/lang/Long;
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract$RawContactsEntity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v7, "contact_id"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v20

    .line 1185
    const-wide/16 v36, -0x1

    .line 1187
    .local v36, "lastContactID":J
    if-eqz v20, :cond_7

    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1188
    if-eqz v32, :cond_4

    .line 1189
    const-string/jumbo v2, "contact_id"

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 1190
    const-string/jumbo v2, "data1"

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 1191
    const-string/jumbo v2, "data2"

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 1192
    const-string/jumbo v2, "data3"

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 1193
    const-string/jumbo v2, "mimetype"

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    .line 1194
    const-string/jumbo v2, "is_super_primary"

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    .line 1195
    const/16 v32, 0x0

    .line 1197
    :cond_4
    const/4 v8, 0x0

    .line 1199
    .local v8, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_5
    invoke-interface/range {v20 .. v21}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v27

    .line 1200
    .local v27, "contactID":J
    cmp-long v2, v36, v27

    if-nez v2, :cond_6

    if-nez v8, :cond_9

    .line 1201
    :cond_6
    move-wide/from16 v36, v27

    .line 1202
    invoke-static/range {v27 .. v28}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    check-cast v8, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 1203
    .restart local v8    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-nez v8, :cond_9

    .line 1249
    :goto_3
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_5

    .line 1252
    .end local v8    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v27    # "contactID":J
    :cond_7
    if-eqz v20, :cond_8

    .line 1253
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    :cond_8
    move/from16 v2, v29

    .line 1257
    goto/16 :goto_1

    .line 1207
    .restart local v8    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v27    # "contactID":J
    :cond_9
    const/4 v7, 0x0

    .line 1208
    .local v7, "data":Lcom/vlingo/core/internal/contacts/ContactData;
    :try_start_1
    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1209
    .local v10, "data1":Ljava/lang/String;
    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 1210
    .local v17, "data2":I
    move-object/from16 v0, v20

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1211
    .local v11, "label":Ljava/lang/String;
    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v38

    .line 1212
    .local v38, "mime":Ljava/lang/String;
    move-object/from16 v0, v20

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 1213
    .local v12, "isDefault":I
    const-string/jumbo v2, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, v38

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1214
    invoke-static {v11}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b

    if-nez v17, :cond_b

    .line 1215
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    sget-object v9, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Phone:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    invoke-direct/range {v7 .. v12}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1219
    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    :goto_4
    invoke-virtual {v8, v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addPhone(Lcom/vlingo/core/internal/contacts/ContactData;)V

    .line 1246
    :cond_a
    :goto_5
    add-int/lit8 v29, v29, 0x1

    goto :goto_3

    .line 1217
    :cond_b
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    sget-object v15, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Phone:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    move-object v13, v7

    move-object v14, v8

    move-object/from16 v16, v10

    move/from16 v18, v12

    invoke-direct/range {v13 .. v18}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    goto :goto_4

    .line 1221
    :cond_c
    const-string/jumbo v2, "vnd.android.cursor.item/email_v2"

    move-object/from16 v0, v38

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1222
    invoke-static {v11}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_e

    if-nez v17, :cond_e

    .line 1223
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    sget-object v9, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Email:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    invoke-direct/range {v7 .. v12}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1227
    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    :goto_6
    invoke-virtual {v8, v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addEmail(Lcom/vlingo/core/internal/contacts/ContactData;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    .line 1252
    .end local v6    # "rawContactIDsString":[Ljava/lang/String;
    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v8    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v10    # "data1":Ljava/lang/String;
    .end local v11    # "label":Ljava/lang/String;
    .end local v12    # "isDefault":I
    .end local v17    # "data2":I
    .end local v27    # "contactID":J
    .end local v34    # "i$":Ljava/util/Iterator;
    .end local v36    # "lastContactID":J
    .end local v38    # "mime":Ljava/lang/String;
    :catchall_0
    move-exception v2

    if-eqz v20, :cond_d

    .line 1253
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    :cond_d
    throw v2

    .line 1225
    .restart local v6    # "rawContactIDsString":[Ljava/lang/String;
    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    .restart local v8    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v10    # "data1":Ljava/lang/String;
    .restart local v11    # "label":Ljava/lang/String;
    .restart local v12    # "isDefault":I
    .restart local v17    # "data2":I
    .restart local v27    # "contactID":J
    .restart local v34    # "i$":Ljava/util/Iterator;
    .restart local v36    # "lastContactID":J
    .restart local v38    # "mime":Ljava/lang/String;
    :cond_e
    :try_start_2
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    sget-object v15, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Email:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    move-object v13, v7

    move-object v14, v8

    move-object/from16 v16, v10

    move/from16 v18, v12

    invoke-direct/range {v13 .. v18}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    goto :goto_6

    .line 1229
    :cond_f
    const-string/jumbo v2, "vnd.android.cursor.item/postal-address_v2"

    move-object/from16 v0, v38

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1230
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    sget-object v15, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Address:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    move-object v13, v7

    move-object v14, v8

    move-object/from16 v16, v10

    move/from16 v18, v12

    invoke-direct/range {v13 .. v18}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    .line 1231
    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v8, v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addAddress(Lcom/vlingo/core/internal/contacts/ContactData;)V

    goto :goto_5

    .line 1233
    :cond_10
    const-string/jumbo v2, "vnd.android.cursor.item/contact_event"

    move-object/from16 v0, v38

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    const-string/jumbo v2, "vnd.pantech.cursor.item/lunar_event"

    move-object/from16 v0, v38

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1234
    :cond_11
    new-instance v39, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy-MM-dd"

    move-object/from16 v0, v39

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1235
    .local v39, "sdfInput":Ljava/text/SimpleDateFormat;
    const/16 v16, 0x0

    .line 1237
    .local v16, "formattedBirthday":Ljava/lang/String;
    :try_start_3
    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v19

    .line 1238
    .local v19, "birthday":Ljava/util/Date;
    invoke-static/range {p1 .. p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/text/ParseException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v16

    .line 1243
    .end local v19    # "birthday":Ljava/util/Date;
    :goto_7
    :try_start_4
    new-instance v7, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    sget-object v15, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Birthday:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    move-object v13, v7

    move-object v14, v8

    move/from16 v18, v12

    invoke-direct/range {v13 .. v18}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    .line 1244
    .restart local v7    # "data":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v8, v7}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addBirthday(Lcom/vlingo/core/internal/contacts/ContactData;)V

    goto/16 :goto_5

    .line 1239
    :catch_0
    move-exception v31

    .line 1240
    .local v31, "e":Ljava/text/ParseException;
    sget-object v2, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "date string did not match \"yyyy-MM-dd\""

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1241
    move-object/from16 v16, v10

    goto :goto_7
.end method

.method public getContactMatchByPhoneNumber(Ljava/lang/String;Landroid/content/Context;)Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 18
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1731
    const/4 v1, 0x4

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v6, "_id"

    aput-object v6, v3, v1

    const/4 v1, 0x1

    const-string/jumbo v6, "lookup"

    aput-object v6, v3, v1

    const/4 v1, 0x2

    const-string/jumbo v6, "display_name"

    aput-object v6, v3, v1

    const/4 v1, 0x3

    const-string/jumbo v6, "starred"

    aput-object v6, v3, v1

    .line 1732
    .local v3, "projection":[Ljava/lang/String;
    const/16 v16, 0x0

    .line 1733
    .local v16, "cursor":Landroid/database/Cursor;
    if-eqz p1, :cond_2

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 1734
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1735
    const/4 v4, 0x0

    .line 1761
    :goto_0
    return-object v4

    .line 1737
    :cond_0
    sget-object v1, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static/range {p1 .. p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1738
    .local v2, "contactUri":Landroid/net/Uri;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1742
    const/4 v4, 0x0

    .line 1744
    .local v4, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-static/range {v16 .. v16}, Lcom/vlingo/core/internal/util/CursorUtil;->isValid(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->getCount()I

    move-result v1

    const/4 v6, 0x1

    if-lt v1, v6, :cond_5

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1746
    :try_start_0
    const-string/jumbo v1, "_id"

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    .line 1747
    .local v15, "contactId":Ljava/lang/Long;
    const-string/jumbo v1, "lookup"

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1748
    .local v8, "lookupKey":Ljava/lang/String;
    const-string/jumbo v1, "display_name"

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1749
    .local v5, "displayName":Ljava/lang/String;
    const-string/jumbo v1, "starred"

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 1751
    .local v17, "starred":I
    new-instance v4, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .end local v4    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v1, 0x1

    move/from16 v0, v17

    if-ne v0, v1, :cond_3

    const/4 v9, 0x1

    :goto_1
    invoke-direct/range {v4 .. v9}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;JLjava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1753
    .restart local v4    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-eqz v16, :cond_1

    .line 1754
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 1760
    .end local v5    # "displayName":Ljava/lang/String;
    .end local v8    # "lookupKey":Ljava/lang/String;
    .end local v15    # "contactId":Ljava/lang/Long;
    .end local v17    # "starred":I
    :cond_1
    :goto_2
    new-instance v9, Lcom/vlingo/core/internal/contacts/ContactData;

    sget-object v11, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Phone:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v10, v4

    move-object/from16 v12, p1

    invoke-direct/range {v9 .. v14}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    invoke-virtual {v4, v9}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addPhone(Lcom/vlingo/core/internal/contacts/ContactData;)V

    goto/16 :goto_0

    .line 1740
    .end local v2    # "contactUri":Landroid/net/Uri;
    .end local v4    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_2
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 1751
    .restart local v2    # "contactUri":Landroid/net/Uri;
    .restart local v5    # "displayName":Ljava/lang/String;
    .restart local v8    # "lookupKey":Ljava/lang/String;
    .restart local v15    # "contactId":Ljava/lang/Long;
    .restart local v17    # "starred":I
    :cond_3
    const/4 v9, 0x0

    goto :goto_1

    .line 1753
    .end local v5    # "displayName":Ljava/lang/String;
    .end local v8    # "lookupKey":Ljava/lang/String;
    .end local v15    # "contactId":Ljava/lang/Long;
    .end local v17    # "starred":I
    :catchall_0
    move-exception v1

    if-eqz v16, :cond_4

    .line 1754
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v1

    .line 1758
    .restart local v4    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_5
    new-instance v4, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .end local v4    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    const-wide/16 v11, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v9, v4

    move-object/from16 v10, p1

    invoke-direct/range {v9 .. v14}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;JLjava/lang/String;Z)V

    .restart local v4    # "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    goto :goto_2
.end method

.method public getContactMatches(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;Z)Ljava/util/List;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "query"    # Ljava/lang/String;
    .param p4, "forPhone"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;Z)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 537
    .local p3, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    const-string/jumbo v0, "new_contact_match_algo"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->getContactMatches(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;ZZ)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getContactMatches(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;ZZ)Ljava/util/List;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "query"    # Ljava/lang/String;
    .param p4, "forPhone"    # Z
    .param p5, "newAlgo"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactLookupType;",
            ">;ZZ)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 548
    .local p3, "searchTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/core/internal/contacts/ContactLookupType;>;"
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 549
    .local v4, "matchTable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 554
    .local v5, "rawContactIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    if-eqz p5, :cond_1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v6, p4

    .line 555
    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->findMatchesByName2(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;Z)V

    .line 556
    new-instance v10, Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v10, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 584
    .local v10, "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_0
    return-object v10

    .end local v10    # "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_1
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    .line 558
    invoke-virtual/range {v0 .. v5}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->findMatchesByName(Landroid/content/Context;Ljava/lang/String;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)V

    .line 560
    invoke-direct {p0, p1, p3, v4, v5}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->getAllContactData(Landroid/content/Context;Ljava/util/EnumSet;Ljava/util/HashMap;Ljava/util/List;)I

    move-result v7

    .line 565
    .local v7, "count":I
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 566
    .restart local v10    # "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 567
    .local v9, "match":Lcom/vlingo/core/internal/contacts/ContactMatch;
    if-nez p4, :cond_3

    invoke-virtual {v9}, Lcom/vlingo/core/internal/contacts/ContactMatch;->containsData()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_3
    invoke-virtual {v9, p3}, Lcom/vlingo/core/internal/contacts/ContactMatch;->hasLookupData(Ljava/util/EnumSet;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 568
    invoke-interface {v10, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getExactContactMatch(Landroid/content/Context;J)Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # J

    .prologue
    .line 478
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "contact_id  = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 479
    .local v0, "idSelection":Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->getExactContactMatchByQuery(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v1

    return-object v1
.end method

.method public getExactContactMatch(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactMatch;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "query"    # Ljava/lang/String;

    .prologue
    .line 464
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 465
    :cond_0
    const/4 v1, 0x0

    .line 474
    :goto_0
    return-object v1

    .line 470
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "data1 LIKE \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 474
    .local v0, "nameSelection":Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->getExactContactMatchByQuery(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v1

    goto :goto_0
.end method

.method public isAddressBookEmpty(Landroid/content/Context;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1494
    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v0, "contact_id"

    aput-object v0, v2, v8

    .line 1495
    .local v2, "proj":[Ljava/lang/String;
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    .line 1496
    .local v1, "lookupUri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 1499
    .local v6, "c":Landroid/database/Cursor;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1510
    :goto_0
    return v8

    .line 1504
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1505
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1506
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    move v0, v7

    .line 1509
    :goto_1
    if-eqz v6, :cond_1

    .line 1510
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    move v8, v0

    goto :goto_0

    :cond_2
    move v0, v8

    .line 1506
    goto :goto_1

    .line 1509
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 1510
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public isProviderStatusNormal(Landroid/content/ContentResolver;)Z
    .locals 9
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1595
    const-string/jumbo v0, "content://com.android.contacts/provider_status"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v0, "status"

    aput-object v0, v2, v4

    const/4 v0, 0x1

    const-string/jumbo v4, "data1"

    aput-object v4, v2, v0

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1598
    .local v6, "c":Landroid/database/Cursor;
    const/4 v8, -0x1

    .line 1599
    .local v8, "status":I
    const/4 v7, 0x0

    .line 1602
    .local v7, "retVal":Z
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1603
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 1605
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1611
    if-nez v8, :cond_1

    .line 1612
    const/4 v7, 0x1

    .line 1614
    :cond_1
    return v7

    .line 1605
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public populateContactMapping(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1533
    const/4 v0, 0x0

    .line 1536
    .local v0, "cur":Landroid/database/Cursor;
    :try_start_0
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/contacts/OldContactDBUtil;->getContactCursor(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v0

    .line 1537
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1538
    .local v7, "updatedContactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v0, :cond_4

    .line 1540
    const-string/jumbo v8, "data2"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 1541
    .local v6, "givenNameColumn":I
    const-string/jumbo v8, "data3"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 1542
    .local v4, "familyNameColumn":I
    const-string/jumbo v8, "data1"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 1543
    .local v2, "displayNameColumn":I
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1546
    :cond_0
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1547
    .local v5, "givenName":Ljava/lang/String;
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1548
    .local v3, "familyName":Ljava/lang/String;
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1551
    .local v1, "displayName":Ljava/lang/String;
    if-eqz v5, :cond_1

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 1555
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1558
    :cond_1
    if-eqz v3, :cond_2

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 1562
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1565
    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 1569
    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1571
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-nez v8, :cond_0

    .line 1574
    .end local v1    # "displayName":Ljava/lang/String;
    .end local v2    # "displayNameColumn":I
    .end local v3    # "familyName":Ljava/lang/String;
    .end local v4    # "familyNameColumn":I
    .end local v5    # "givenName":Ljava/lang/String;
    .end local v6    # "givenNameColumn":I
    :cond_4
    invoke-static {v7}, Lcom/vlingo/core/internal/settings/Settings;->setContactNameList(Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1577
    if-eqz v0, :cond_5

    .line 1581
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1586
    :cond_5
    :goto_0
    return-void

    .line 1577
    .end local v7    # "updatedContactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catchall_0
    move-exception v8

    if-eqz v0, :cond_6

    .line 1581
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 1583
    :cond_6
    :goto_1
    throw v8

    .line 1582
    .restart local v7    # "updatedContactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_0
    move-exception v8

    goto :goto_0

    .end local v7    # "updatedContactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_1
    move-exception v9

    goto :goto_1
.end method
