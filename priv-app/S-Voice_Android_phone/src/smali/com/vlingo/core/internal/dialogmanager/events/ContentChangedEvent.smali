.class public Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;
.super Lcom/vlingo/core/internal/dialogmanager/DialogEvent;
.source "ContentChangedEvent.java"


# instance fields
.field private final NAME:Ljava/lang/String;

.field private content:Ljava/lang/String;

.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;-><init>(ZZZ)V

    .line 16
    const-string/jumbo v0, "content-changed"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;->NAME:Ljava/lang/String;

    .line 24
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;->type:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;->content:Ljava/lang/String;

    .line 26
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getVLDialogEvent()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    .locals 3

    .prologue
    .line 37
    new-instance v0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    const-string/jumbo v1, "content-changed"

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;-><init>(Ljava/lang/String;)V

    .line 38
    .local v0, "builder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;->type:Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;->content:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 40
    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;

    move-result-object v1

    return-object v1
.end method
