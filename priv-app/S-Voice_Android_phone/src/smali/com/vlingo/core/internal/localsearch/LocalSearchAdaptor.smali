.class public Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;
.super Ljava/lang/Object;
.source "LocalSearchAdaptor.java"

# interfaces
.implements Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor$1;,
        Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor$DetailsLocalSearchRequestListener;
    }
.end annotation


# instance fields
.field protected currentKeyterms:Ljava/lang/String;

.field private currentSearchQuery:Ljava/lang/String;

.field private currentSpokenLocation:Ljava/lang/String;

.field private currentSpokenSearch:Ljava/lang/String;

.field private failReason:Ljava/lang/String;

.field private isRequestComplete:Z

.field private isRequestFailed:Z

.field private localSearchDetailsListing:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchListing;",
            ">;"
        }
    .end annotation
.end field

.field private localSearchListings:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchListing;",
            ">;"
        }
    .end annotation
.end field

.field private lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

.field private numberOfRetries:I

.field private providers:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-boolean v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->isRequestComplete:Z

    .line 25
    iput-boolean v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->isRequestFailed:Z

    .line 26
    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->failReason:Ljava/lang/String;

    .line 31
    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .line 33
    iput v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->numberOfRetries:I

    .line 37
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    .line 38
    new-instance v0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    invoke-direct {v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    .line 39
    iput-boolean v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->isRequestComplete:Z

    .line 40
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->currentSearchQuery:Ljava/lang/String;

    .line 41
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->currentSpokenLocation:Ljava/lang/String;

    .line 42
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->currentKeyterms:Ljava/lang/String;

    .line 43
    return-void
.end method

.method private executeSearch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "search"    # Ljava/lang/String;
    .param p2, "spokenLocation"    # Ljava/lang/String;
    .param p3, "region"    # Ljava/lang/String;
    .param p4, "force"    # Z

    .prologue
    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->isRequestComplete:Z

    .line 56
    if-nez p1, :cond_0

    .line 57
    const-string/jumbo p1, ""

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->currentSearchQuery:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 59
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->currentSearchQuery:Ljava/lang/String;

    .line 61
    :cond_1
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->currentSearchQuery:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 62
    iget v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->numberOfRetries:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->numberOfRetries:I

    .line 65
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->currentSearchQuery:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p4, :cond_4

    .line 66
    :cond_3
    iput-object p1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->currentSearchQuery:Ljava/lang/String;

    .line 67
    iput-object p1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->currentSpokenSearch:Ljava/lang/String;

    .line 68
    iput-object p2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->currentSpokenLocation:Ljava/lang/String;

    .line 69
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 70
    const-string/jumbo v0, "language"

    const-string/jumbo v1, "zh-CN"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "zh-CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 71
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->sendChineseSearchRequet(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :cond_4
    :goto_0
    return-void

    .line 73
    :cond_5
    const-string/jumbo v0, "Not match conditon"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->onRequestFailed(Ljava/lang/String;)V

    goto :goto_0

    .line 76
    :cond_6
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    invoke-virtual {v0, p1, p2, p0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sendSearchRequest(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V

    goto :goto_0
.end method

.method private sendChineseSearchRequet(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "spokenLocation"    # Ljava/lang/String;
    .param p3, "region"    # Ljava/lang/String;

    .prologue
    .line 300
    :try_start_0
    invoke-static {p2, p3, p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchChineseURLMaker;->getBusinessURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 301
    .local v1, "requetURL":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 302
    iget-object v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    .line 310
    .end local v1    # "requetURL":Ljava/lang/String;
    :goto_0
    return-void

    .line 305
    .restart local v1    # "requetURL":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    invoke-virtual {v2, v1, p0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sendChineseSearchRequest(Ljava/lang/String;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 306
    .end local v1    # "requetURL":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 307
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->onRequestFailed(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setLocalSearchListings(Ljava/util/Vector;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchListing;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, "items":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/vlingo/core/internal/localsearch/LocalSearchListing;>;"
    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    monitor-enter v1

    .line 92
    :try_start_0
    iput-object p1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    .line 93
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->setProviders(Ljava/util/Vector;)V

    .line 95
    return-void

    .line 93
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private setProviders(Ljava/util/Vector;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchListing;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 98
    .local p1, "items":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/vlingo/core/internal/localsearch/LocalSearchListing;>;"
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    iput-object v3, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->providers:Ljava/util/HashSet;

    .line 99
    invoke-virtual {p1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    .line 100
    .local v1, "item":Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getProvider()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 101
    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getProvider()Ljava/lang/String;

    move-result-object v2

    .line 102
    .local v2, "provider":Ljava/lang/String;
    iget-object v3, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->providers:Ljava/util/HashSet;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 105
    .end local v1    # "item":Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    .end local v2    # "provider":Ljava/lang/String;
    :cond_1
    return-void
.end method


# virtual methods
.method public executeDetailRequest(Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V
    .locals 2
    .param p1, "currentBusinessItem"    # Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    .param p2, "listenerFromWidget"    # Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .prologue
    .line 82
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    new-instance v1, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor$DetailsLocalSearchRequestListener;

    invoke-direct {v1, p0, p2}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor$DetailsLocalSearchRequestListener;-><init>(Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V

    invoke-virtual {v0, p1, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sendChineseMoreDetailsRequest(Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V

    .line 88
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    new-instance v1, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor$DetailsLocalSearchRequestListener;

    invoke-direct {v1, p0, p2}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor$DetailsLocalSearchRequestListener;-><init>(Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V

    invoke-virtual {v0, p1, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sendMoreDetailsRequest(Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V

    goto :goto_0
.end method

.method public executeForceSearch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "search"    # Ljava/lang/String;
    .param p2, "spokenLocation"    # Ljava/lang/String;
    .param p3, "region"    # Ljava/lang/String;
    .param p4, "isForced"    # Z

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->executeSearch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 47
    return-void
.end method

.method public executeSearch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "search"    # Ljava/lang/String;
    .param p2, "spokenLocation"    # Ljava/lang/String;
    .param p3, "region"    # Ljava/lang/String;

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->executeSearch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 52
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 124
    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    monitor-enter v1

    .line 125
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getCurrentSearchQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->currentSearchQuery:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentSpokenLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->currentSpokenLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentSpokenSearch()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->currentSpokenSearch:Ljava/lang/String;

    return-object v0
.end method

.method public getFailReason()Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->failReason:Ljava/lang/String;

    return-object v0
.end method

.method public getItem(I)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 114
    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    monitor-enter v1

    .line 115
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 116
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    monitor-exit v1

    .line 118
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getKeyTerms()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->currentKeyterms:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalSearchDetailsListing()Ljava/util/Vector;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchListing;",
            ">;"
        }
    .end annotation

    .prologue
    .line 282
    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->localSearchDetailsListing:Ljava/util/Vector;

    monitor-enter v1

    .line 283
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->localSearchDetailsListing:Ljava/util/Vector;

    monitor-exit v1

    return-object v0

    .line 284
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getNumberOfRetries()I
    .locals 1

    .prologue
    .line 268
    iget v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->numberOfRetries:I

    return v0
.end method

.method public getWidgetListener()Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    return-object v0
.end method

.method public isRequestComplete()Z
    .locals 1

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->isRequestComplete:Z

    return v0
.end method

.method public isRequestFailed()Z
    .locals 1

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->isRequestFailed:Z

    return v0
.end method

.method public onRequestComplete(ZLjava/lang/Object;)V
    .locals 2
    .param p1, "success"    # Z
    .param p2, "items"    # Ljava/lang/Object;

    .prologue
    .line 201
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->isRequestComplete:Z

    .line 202
    if-eqz p1, :cond_1

    .line 205
    instance-of v1, p2, Landroid/util/Pair;

    if-eqz v1, :cond_1

    move-object v1, p2

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    instance-of v1, v1, Ljava/util/Vector;

    if-eqz v1, :cond_1

    move-object v0, p2

    .line 206
    check-cast v0, Landroid/util/Pair;

    .line 207
    .local v0, "items2":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;Ljava/util/Vector<Lcom/vlingo/core/internal/localsearch/LocalSearchListing;>;>;"
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/util/Vector;

    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->setLocalSearchListings(Ljava/util/Vector;)V

    .line 208
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 209
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->getQuery()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->currentSearchQuery:Ljava/lang/String;

    .line 211
    :cond_0
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->getLocalizedCityState()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 212
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->getLocalizedCityState()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->currentSpokenLocation:Ljava/lang/String;

    .line 221
    .end local v0    # "items2":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;Ljava/util/Vector<Lcom/vlingo/core/internal/localsearch/LocalSearchListing;>;>;"
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    if-eqz v1, :cond_2

    .line 222
    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onResponseReceived()V

    .line 224
    :cond_2
    return-void
.end method

.method public onRequestFailed(Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;)V
    .locals 2
    .param p1, "failureType"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;

    .prologue
    .line 239
    sget-object v0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor$1;->$SwitchMap$com$vlingo$core$internal$localsearch$LocalSearchRequestListener$LocalSearchFailureType:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 251
    :goto_0
    return-void

    .line 241
    :pswitch_0
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_bad_response:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0

    .line 244
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_no_results:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0

    .line 247
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_bad_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0

    .line 239
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onRequestFailed(Ljava/lang/String;)V
    .locals 1
    .param p1, "failReasonParam"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 228
    iput-boolean v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->isRequestComplete:Z

    .line 229
    iput-boolean v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->isRequestFailed:Z

    .line 230
    iput-object p1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->failReason:Ljava/lang/String;

    .line 232
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed()V

    .line 235
    :cond_0
    return-void
.end method

.method public onRequestScheduled()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 256
    iput-boolean v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->isRequestComplete:Z

    .line 257
    iput-boolean v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->isRequestFailed:Z

    .line 259
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestScheduled()V

    .line 262
    :cond_0
    return-void
.end method

.method public resetNumberOfRetries()V
    .locals 1

    .prologue
    .line 275
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->numberOfRetries:I

    .line 276
    return-void
.end method

.method public resetSearch()V
    .locals 2

    .prologue
    .line 108
    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    monitor-enter v1

    .line 109
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 110
    monitor-exit v1

    .line 111
    return-void

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setKeyTerms(Ljava/lang/String;)V
    .locals 0
    .param p1, "keyterms"    # Ljava/lang/String;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->currentKeyterms:Ljava/lang/String;

    .line 192
    return-void
.end method

.method public setLocalSearchDetailsListing(Ljava/util/Vector;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchListing;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 292
    .local p1, "localSearchDetailsListing":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/vlingo/core/internal/localsearch/LocalSearchListing;>;"
    monitor-enter p1

    .line 293
    :try_start_0
    iput-object p1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->localSearchDetailsListing:Ljava/util/Vector;

    .line 294
    monitor-exit p1

    .line 295
    return-void

    .line 294
    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V
    .locals 0
    .param p1, "widgetListener"    # Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .prologue
    .line 187
    iput-object p1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .line 188
    return-void
.end method
