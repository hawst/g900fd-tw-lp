.class public final Lcom/vlingo/core/internal/schedule/ScheduleUtil;
.super Ljava/lang/Object;
.source "ScheduleUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;,
        Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;,
        Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;
    }
.end annotation


# static fields
.field public static final ACCOUNT_KEY:Ljava/lang/String; = "accountKey"

.field public static final ACCOUNT_NAME:Ljava/lang/String; = "accountName"

.field public static final ACCOUNT_NAME_MYTASK:Ljava/lang/String; = "My task"

.field public static final ATTENDEES_URI:Landroid/net/Uri;

.field public static final ATTENDEE_EMAIL:Ljava/lang/String; = "attendeeEmail"

.field public static final ATTENDEE_EVENT_ID:Ljava/lang/String; = "event_id"

.field public static final ATTENDEE_NAME:Ljava/lang/String; = "attendeeName"

.field public static final ATTENDEE_RELATIONSHIP:Ljava/lang/String; = "attendeeRelationship"

.field public static final ATTENDEE_STATUS:Ljava/lang/String; = "attendeeStatus"

.field public static final ATTENDEE_TYPE:Ljava/lang/String; = "attendeeType"

.field private static final AVAILABLE:I = 0x1

.field public static final BODY:Ljava/lang/String; = "body"

.field public static final BODY_SIZE:Ljava/lang/String; = "body_size"

.field public static final BODY_TYPE:Ljava/lang/String; = "bodyType"

.field public static final COMPLETE:Ljava/lang/String; = "complete"

.field private static final DEFAULT_MAX_DISPLAY_MATCHES:I = 0x6

.field private static final DEFAULT_MAX_QUERY_MATCHES:I = 0xfa0

.field private static DELETED_CHECK:I = 0x0

.field public static final DUE_DATE:Ljava/lang/String; = "due_date"

.field private static final EMAIL_PATTERN:Ljava/lang/String; = ".*@.*\\..*"

.field public static final EVENTS_DEFAULT_SORT_ORDER:Ljava/lang/String; = "begin ASC"

.field public static final EVENTS_INSTANCES_CONTENT_URI:Landroid/net/Uri;

.field public static final EVENT_PROJECTION:[Ljava/lang/String;

.field public static final GROUP_ID:Ljava/lang/String; = "groupId"

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final IMPORTANCE:Ljava/lang/String; = "importance"

.field private static MAX_ATTENDEES:I = 0x0

.field public static final MY_CALENDAR:Ljava/lang/String; = "My calendar"

.field private static final NOT_AVAILABLE:I = 0x0

.field private static final NOT_INITIALIZED:I = -0x1

.field private static final PROJECTION_ACCESS_LEVEL:I = 0x4

.field private static final PROJECTION_ACCOUNT_NAME_INDEX:I = 0x1

.field private static final PROJECTION_ACCOUNT_TYPE:I = 0x5

.field private static final PROJECTION_DISPLAY_NAME_INDEX:I = 0x2

.field private static final PROJECTION_ID_INDEX:I = 0x0

.field private static final PROJECTION_SYNC_EVENTS:I = 0x3

.field public static final REMINDER_PROJECTION:[Ljava/lang/String;

.field public static final REMINDER_SET:Ljava/lang/String; = "reminder_set"

.field public static final REMINDER_TIME:Ljava/lang/String; = "reminder_time"

.field public static final REMINDER_TYPE:Ljava/lang/String; = "reminder_type"

.field public static final SCHEDULE_END_PROJECTION:[Ljava/lang/String;

.field public static final SCHEDULE_PROJECTION:[Ljava/lang/String;

.field public static final SCHEDULE_SORT_ORDER:Ljava/lang/String; = "allDay DESC, begin ASC, title ASC"

.field public static final START_DATE:Ljava/lang/String; = "start_date"

.field public static final SUBJECT:Ljava/lang/String; = "subject"

.field public static final SYNC_DIRTY:Ljava/lang/String; = "_sync_dirty"

.field private static final TAG:Ljava/lang/String;

.field public static final TASKSACCOUTS_CONTENT_URI:Landroid/net/Uri;

.field public static final TASKSGROUP_CONTENT_URI:Landroid/net/Uri;

.field public static final TASKS_REMINDERS_CONTENT_URI:Landroid/net/Uri;

.field static final TASKS_WHERE:Ljava/lang/String; = "_id=?"

.field public static final TASK_ACCOUNT_KEY:Ljava/lang/String; = "accountKey"

.field public static final TASK_ACCOUNT_NAME:Ljava/lang/String; = "accountName"

.field public static final TASK_BODY:Ljava/lang/String; = "body"

.field public static final TASK_BODY_SIZE:Ljava/lang/String; = "body_size"

.field public static final TASK_BODY_TYPE:Ljava/lang/String; = "bodyType"

.field public static final TASK_COMPLETE:Ljava/lang/String; = "complete"

.field public static final TASK_DELETED:Ljava/lang/String; = "deleted"

.field public static final TASK_DUE_DATE:Ljava/lang/String; = "due_date"

.field public static final TASK_GROUPID:Ljava/lang/String; = "groupId"

.field public static final TASK_ID:Ljava/lang/String; = "_id"

.field public static final TASK_IMPORTANCE:Ljava/lang/String; = "importance"

.field protected static final TASK_INDEX_REMINDER_ACCOUNTKEY:I = 0x7

.field protected static final TASK_INDEX_REMINDER_DUEDATE:I = 0x6

.field protected static final TASK_INDEX_REMINDER_ID:I = 0x0

.field protected static final TASK_INDEX_REMINDER_REMINDER_TIME:I = 0x2

.field protected static final TASK_INDEX_REMINDER_REMINDER_TYPE:I = 0x8

.field protected static final TASK_INDEX_REMINDER_STARTDATE:I = 0x5

.field protected static final TASK_INDEX_REMINDER_STATE:I = 0x3

.field protected static final TASK_INDEX_REMINDER_SUBJECT:I = 0x4

.field protected static final TASK_INDEX_REMINDER_TASKID:I = 0x1

.field public static final TASK_PROJECTION:[Ljava/lang/String;

.field public static final TASK_REMINDER_SET:Ljava/lang/String; = "reminder_set"

.field public static final TASK_REMINDER_TIME:Ljava/lang/String; = "reminder_time"

.field public static final TASK_REMINDER_TYPE:Ljava/lang/String; = "reminder_type"

.field public static final TASK_SORT_ORDER:Ljava/lang/String; = "due_date"

.field public static final TASK_SUBJECT:Ljava/lang/String; = "subject"

.field public static final TASK_URI:Ljava/lang/String; = "content://com.android.calendar/syncTasks"

.field public static final TASK_UTC_DUE_DATE:Ljava/lang/String; = "utc_due_date"

.field public static final UTC_DUE_DATE:Ljava/lang/String; = "utc_due_date"

.field public static final UTC_START_DATE:Ljava/lang/String; = "utc_start_date"


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 49
    const-class v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->TAG:Ljava/lang/String;

    .line 96
    const-string/jumbo v0, "content://com.android.calendar/TasksReminders"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->TASKS_REMINDERS_CONTENT_URI:Landroid/net/Uri;

    .line 99
    const-string/jumbo v0, "content://com.android.calendar/TasksAccounts"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->TASKSACCOUTS_CONTENT_URI:Landroid/net/Uri;

    .line 102
    const-string/jumbo v0, "content://com.android.calendar/taskGroup"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->TASKSGROUP_CONTENT_URI:Landroid/net/Uri;

    .line 105
    const-string/jumbo v0, "content://com.android.calendar/instances/when"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->EVENTS_INSTANCES_CONTENT_URI:Landroid/net/Uri;

    .line 109
    const-string/jumbo v0, "content://com.android.calendar/attendees"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->ATTENDEES_URI:Landroid/net/Uri;

    .line 119
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "_id"

    aput-object v1, v0, v3

    const-string/jumbo v1, "task_id"

    aput-object v1, v0, v4

    const-string/jumbo v1, "reminder_time"

    aput-object v1, v0, v5

    const-string/jumbo v1, "state"

    aput-object v1, v0, v6

    const-string/jumbo v1, "subject"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "start_date"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "due_date"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "accountKey"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "reminder_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->REMINDER_PROJECTION:[Ljava/lang/String;

    .line 143
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "_id"

    aput-object v1, v0, v3

    const-string/jumbo v1, "title"

    aput-object v1, v0, v4

    const-string/jumbo v1, "allDay"

    aput-object v1, v0, v5

    const-string/jumbo v1, "begin"

    aput-object v1, v0, v6

    const-string/jumbo v1, "end"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "event_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "eventLocation"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "description"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "organizer"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "calendar_access_level"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "ownerAccount"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "guestsCanModify"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->SCHEDULE_PROJECTION:[Ljava/lang/String;

    .line 160
    new-array v0, v6, [Ljava/lang/String;

    const-string/jumbo v1, "_id"

    aput-object v1, v0, v3

    const-string/jumbo v1, "dtend"

    aput-object v1, v0, v4

    const-string/jumbo v1, "duration"

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->SCHEDULE_END_PROJECTION:[Ljava/lang/String;

    .line 166
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "_id"

    aput-object v1, v0, v3

    const-string/jumbo v1, "subject"

    aput-object v1, v0, v4

    const-string/jumbo v1, "due_date"

    aput-object v1, v0, v5

    const-string/jumbo v1, "utc_due_date"

    aput-object v1, v0, v6

    const-string/jumbo v1, "accountName"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "reminder_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "reminder_set"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "reminder_time"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "importance"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "groupId"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "body"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "complete"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->TASK_PROJECTION:[Ljava/lang/String;

    .line 181
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "_id"

    aput-object v1, v0, v3

    const-string/jumbo v1, "account_name"

    aput-object v1, v0, v4

    const-string/jumbo v1, "calendar_displayName"

    aput-object v1, v0, v5

    const-string/jumbo v1, "sync_events"

    aput-object v1, v0, v6

    const-string/jumbo v1, "calendar_access_level"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "account_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->EVENT_PROJECTION:[Ljava/lang/String;

    .line 201
    const/16 v0, 0xf

    sput v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->MAX_ATTENDEES:I

    .line 205
    const/4 v0, -0x1

    sput v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->DELETED_CHECK:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addAttendeesToEvent(Landroid/content/Context;Ljava/util/List;I)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "eventID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .local p1, "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const/4 v6, 0x1

    .line 1190
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 1191
    .local v2, "count":I
    const/4 v3, 0x0

    .line 1192
    .local v3, "i":I
    const/4 v1, 0x0

    .line 1194
    .local v1, "contactData":Lcom/vlingo/core/internal/contacts/ContactData;
    if-lez p2, :cond_0

    .line 1196
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    .line 1198
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "contactData":Lcom/vlingo/core/internal/contacts/ContactData;
    check-cast v1, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 1201
    .restart local v1    # "contactData":Lcom/vlingo/core/internal/contacts/ContactData;
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1202
    .local v0, "attendees":Landroid/content/ContentValues;
    const-string/jumbo v4, "attendeeEmail"

    iget-object v5, v1, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1203
    const-string/jumbo v4, "attendeeName"

    iget-object v5, v1, Lcom/vlingo/core/internal/contacts/ContactData;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v5, v5, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1204
    const-string/jumbo v4, "attendeeRelationship"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1205
    const-string/jumbo v4, "attendeeStatus"

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1206
    const-string/jumbo v4, "attendeeType"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1207
    const-string/jumbo v4, "event_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1209
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->ATTENDEES_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1196
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1212
    .end local v0    # "attendees":Landroid/content/ContentValues;
    :cond_0
    return-void
.end method

.method private static addDeleted(Landroid/content/Context;Landroid/net/Uri;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "taskUri"    # Landroid/net/Uri;

    .prologue
    const/4 v8, 0x0

    const/4 v9, -0x1

    const/4 v7, 0x1

    .line 338
    sget v1, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->DELETED_CHECK:I

    if-ne v1, v9, :cond_0

    .line 339
    const/4 v6, 0x0

    .line 341
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 342
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 343
    const-string/jumbo v1, "deleted"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v9, :cond_1

    .line 344
    const/4 v1, 0x0

    sput v1, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->DELETED_CHECK:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 350
    :goto_0
    if-eqz v6, :cond_0

    .line 351
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 355
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_0
    sget v1, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->DELETED_CHECK:I

    if-ne v1, v7, :cond_3

    move v1, v7

    :goto_1
    return v1

    .line 347
    .restart local v0    # "contentResolver":Landroid/content/ContentResolver;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_1
    const/4 v1, 0x1

    :try_start_1
    sput v1, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->DELETED_CHECK:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 350
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_2

    .line 351
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v1

    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_3
    move v1, v8

    .line 355
    goto :goto_1
.end method

.method public static addEvent(Landroid/content/Context;Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Landroid/net/Uri;
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "event"    # Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
        }
    .end annotation

    .prologue
    .line 1043
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1044
    .local v2, "contentResolver":Landroid/content/ContentResolver;
    if-nez p1, :cond_0

    .line 1045
    new-instance v14, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;

    const-string/jumbo v15, "Null ScheduleEvent detected.  Internal Error"

    invoke-direct {v14, v15}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;-><init>(Ljava/lang/String;)V

    throw v14
    :try_end_0
    .catch Lcom/vlingo/core/internal/schedule/ScheduleUtilException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1103
    .end local v2    # "contentResolver":Landroid/content/ContentResolver;
    :catch_0
    move-exception v11

    .line 1104
    .local v11, "sux":Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
    sget-object v14, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "RuntimeException in ScheduleUtil: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v11}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;->getMessage()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1105
    invoke-virtual {v11}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;->printStackTrace()V

    .line 1106
    throw v11

    .line 1048
    .end local v11    # "sux":Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
    .restart local v2    # "contentResolver":Landroid/content/ContentResolver;
    :cond_0
    :try_start_1
    invoke-static/range {p0 .. p0}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->hasCalendarAccount(Landroid/content/Context;)Z

    move-result v14

    if-nez v14, :cond_1

    .line 1050
    new-instance v14, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;

    const-string/jumbo v15, "Error no calendar account synced."

    invoke-direct {v14, v15}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;-><init>(Ljava/lang/String;)V

    throw v14
    :try_end_1
    .catch Lcom/vlingo/core/internal/schedule/ScheduleUtilException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1107
    .end local v2    # "contentResolver":Landroid/content/ContentResolver;
    :catch_1
    move-exception v3

    .line 1108
    .local v3, "e":Ljava/lang/Exception;
    sget-object v14, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "VLG_CarActivity "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1109
    new-instance v12, Ljava/io/StringWriter;

    invoke-direct {v12}, Ljava/io/StringWriter;-><init>()V

    .line 1110
    .local v12, "sw":Ljava/io/StringWriter;
    new-instance v9, Ljava/io/PrintWriter;

    invoke-direct {v9, v12}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 1111
    .local v9, "pw":Ljava/io/PrintWriter;
    invoke-virtual {v3, v9}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 1112
    sget-object v14, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "VLG_CarActivity "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v16, "\n\r"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v12}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1113
    new-instance v14, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;

    const-string/jumbo v15, "Error in adding an event."

    invoke-direct {v14, v15}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 1053
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v9    # "pw":Ljava/io/PrintWriter;
    .end local v12    # "sw":Ljava/io/StringWriter;
    .restart local v2    # "contentResolver":Landroid/content/ContentResolver;
    :cond_1
    :try_start_2
    new-instance v13, Landroid/content/ContentValues;

    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 1055
    .local v13, "values":Landroid/content/ContentValues;
    const-string/jumbo v14, "title"

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1056
    const-string/jumbo v14, "calendar_id"

    invoke-static/range {p0 .. p0}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getCurrentCalendarId(Landroid/content/Context;)J

    move-result-wide v15

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1057
    const-string/jumbo v14, "eventTimezone"

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1059
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getDescription()Ljava/lang/String;

    move-result-object v10

    .line 1061
    .local v10, "strValue":Ljava/lang/String;
    if-eqz v10, :cond_2

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v14

    if-lez v14, :cond_2

    .line 1063
    const-string/jumbo v14, "description"

    invoke-virtual {v13, v14, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1066
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getLocation()Ljava/lang/String;

    move-result-object v10

    .line 1067
    if-eqz v10, :cond_3

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v14

    if-lez v14, :cond_3

    .line 1069
    const-string/jumbo v14, "eventLocation"

    invoke-virtual {v13, v14, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1071
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getOrganizer()Ljava/lang/String;

    move-result-object v10

    .line 1072
    if-eqz v10, :cond_4

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v14

    if-lez v14, :cond_4

    .line 1074
    const-string/jumbo v14, "organizer"

    invoke-virtual {v13, v14, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1077
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getAllDay()Z

    move-result v14

    if-eqz v14, :cond_7

    const-wide/16 v6, 0x1

    .line 1078
    .local v6, "lngValue":J
    :goto_0
    const-string/jumbo v14, "allDay"

    long-to-int v15, v6

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1080
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBegin()J

    move-result-wide v6

    .line 1081
    const-wide/16 v14, 0x0

    cmp-long v14, v6, v14

    if-lez v14, :cond_5

    const-string/jumbo v14, "dtstart"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1083
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEnd()J

    move-result-wide v6

    .line 1084
    const-wide/16 v14, 0x0

    cmp-long v14, v6, v14

    if-lez v14, :cond_6

    const-string/jumbo v14, "dtend"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1086
    :cond_6
    sget-object v5, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    .line 1087
    .local v5, "eventUri":Landroid/net/Uri;
    invoke-virtual {v2, v5, v13}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v8

    .line 1088
    .local v8, "newEventUri":Landroid/net/Uri;
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setNewEventUri(Landroid/net/Uri;)V

    .line 1090
    if-nez v8, :cond_8

    .line 1091
    new-instance v14, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;

    const-string/jumbo v15, "Error in adding an event."

    invoke-direct {v14, v15}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 1077
    .end local v5    # "eventUri":Landroid/net/Uri;
    .end local v6    # "lngValue":J
    .end local v8    # "newEventUri":Landroid/net/Uri;
    :cond_7
    const-wide/16 v6, 0x0

    goto :goto_0

    .line 1094
    .restart local v5    # "eventUri":Landroid/net/Uri;
    .restart local v6    # "lngValue":J
    .restart local v8    # "newEventUri":Landroid/net/Uri;
    :cond_8
    invoke-static {v8}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getRowIDFromURI(Landroid/net/Uri;)I

    move-result v4

    .line 1096
    .local v4, "eventID":I
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getContactDataList()Ljava/util/List;

    move-result-object v1

    .line 1098
    .local v1, "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    if-eqz v1, :cond_9

    invoke-static/range {p0 .. p0}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->canSaveAttendee(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 1099
    move-object/from16 v0, p0

    invoke-static {v0, v1, v4}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->addAttendeesToEvent(Landroid/content/Context;Ljava/util/List;I)V
    :try_end_2
    .catch Lcom/vlingo/core/internal/schedule/ScheduleUtilException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 1102
    :cond_9
    return-object v8
.end method

.method public static addTask(Landroid/content/Context;Lcom/vlingo/core/internal/schedule/ScheduleTask;)Landroid/net/Uri;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "task"    # Lcom/vlingo/core/internal/schedule/ScheduleTask;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
        }
    .end annotation

    .prologue
    .line 1247
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1248
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-nez p1, :cond_0

    .line 1249
    new-instance v7, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;

    const-string/jumbo v8, "Null ScheduleTask detected.  Internal Error"

    invoke-direct {v7, v8}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_0
    .catch Lcom/vlingo/core/internal/schedule/ScheduleUtilException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1274
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    :catch_0
    move-exception v5

    .line 1275
    .local v5, "sux":Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
    sget-object v7, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->TAG:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1276
    invoke-virtual {v5}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;->printStackTrace()V

    .line 1279
    throw v5

    .line 1251
    .end local v5    # "sux":Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
    .restart local v0    # "contentResolver":Landroid/content/ContentResolver;
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getContentValues(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Landroid/content/ContentValues;

    move-result-object v6

    .line 1253
    .local v6, "values":Landroid/content/ContentValues;
    const-string/jumbo v7, "content://com.android.calendar/syncTasks"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_1
    .catch Lcom/vlingo/core/internal/schedule/ScheduleUtilException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    .line 1254
    .local v2, "eventUri":Landroid/net/Uri;
    const/4 v3, 0x0

    .line 1256
    .local v3, "newEventUri":Landroid/net/Uri;
    :try_start_2
    invoke-virtual {v0, v2, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    .line 1258
    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getReminderTime()J

    move-result-wide v7

    const-wide/16 v9, -0x1

    cmp-long v7, v7, v9

    if-eqz v7, :cond_1

    .line 1259
    invoke-static {v3, p1}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getReminderContentValues(Landroid/net/Uri;Lcom/vlingo/core/internal/schedule/ScheduleTask;)Landroid/content/ContentValues;

    move-result-object v4

    .line 1260
    .local v4, "reminderValues":Landroid/content/ContentValues;
    sget-object v7, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->TASKS_REMINDERS_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v7, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/vlingo/core/internal/schedule/ScheduleUtilException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1267
    .end local v4    # "reminderValues":Landroid/content/ContentValues;
    :cond_1
    if-nez v3, :cond_2

    .line 1268
    :try_start_3
    new-instance v7, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;

    const-string/jumbo v8, "Error in adding a task."

    invoke-direct {v7, v8}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1263
    :catch_1
    move-exception v1

    .line 1264
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    new-instance v7, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Error in adding a task."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1271
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :cond_2
    invoke-virtual {p1, v3}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setNewEventUri(Landroid/net/Uri;)V

    .line 1272
    invoke-static {v3}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getRowIDFromURI(Landroid/net/Uri;)I

    move-result v7

    int-to-long v7, v7

    invoke-virtual {p1, v7, v8}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setEventID(J)V
    :try_end_3
    .catch Lcom/vlingo/core/internal/schedule/ScheduleUtilException; {:try_start_3 .. :try_end_3} :catch_0

    .line 1273
    return-object v3
.end method

.method public static canSaveAttendee(Landroid/content/Context;)Z
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 1679
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-le v1, v2, :cond_0

    .line 1680
    invoke-static {p0}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getCurrentCalendarFromProvider(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1685
    .local v0, "calendar":Ljava/lang/String;
    :goto_0
    if-eqz v0, :cond_1

    const-string/jumbo v1, "My calendar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1686
    const/4 v1, 0x1

    .line 1689
    :goto_1
    return v1

    .line 1682
    .end local v0    # "calendar":Ljava/lang/String;
    :cond_0
    invoke-static {p0}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getCurrentCalendar(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "calendar":Ljava/lang/String;
    goto :goto_0

    .line 1689
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static deleteEvent(Landroid/content/Context;J)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1025
    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 1026
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v1, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1027
    .local v0, "rowsDeleted":I
    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    .line 1028
    new-instance v2, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;

    const-string/jumbo v3, "Error in deleting a task."

    invoke-direct {v2, v3}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1030
    :cond_0
    return-void
.end method

.method public static deleteReminder(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "deleteTaskUri"    # Landroid/net/Uri;

    .prologue
    const/4 v12, 0x1

    .line 1486
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 1487
    .local v6, "contentResolver":Landroid/content/ContentResolver;
    const-string/jumbo v0, "content://com.android.calendar/syncTasks"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1488
    .local v1, "taskUri":Landroid/net/Uri;
    const/4 v7, 0x0

    .line 1490
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1491
    const/4 v0, -0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1492
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 1493
    .local v8, "mId":I
    const/4 v9, 0x0

    .line 1495
    .local v9, "reminderId":I
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1496
    int-to-long v2, v8

    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 1497
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 1502
    :cond_1
    sget-object v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->TASKS_REMINDERS_CONTENT_URI:Landroid/net/Uri;

    int-to-long v2, v9

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v11

    .line 1503
    .local v11, "uri":Landroid/net/Uri;
    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {v6, v11, v0, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    .line 1504
    .local v10, "rowsDeleted":I
    if-eq v10, v12, :cond_2

    .line 1507
    :cond_2
    if-eqz v7, :cond_3

    .line 1508
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1511
    :cond_3
    return-void

    .line 1507
    .end local v8    # "mId":I
    .end local v9    # "reminderId":I
    .end local v10    # "rowsDeleted":I
    .end local v11    # "uri":Landroid/net/Uri;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_4

    .line 1508
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public static deleteTask(Landroid/content/Context;J)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
        }
    .end annotation

    .prologue
    .line 1463
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1465
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const-string/jumbo v5, "content://com.android.calendar/syncTasks"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 1466
    .local v4, "taskUri":Landroid/net/Uri;
    invoke-static {v4, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 1469
    .local v1, "myTask":Landroid/net/Uri;
    invoke-static {p0, v1}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->deleteReminder(Landroid/content/Context;Landroid/net/Uri;)V

    .line 1470
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v0, v1, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 1471
    .local v2, "rowsDeleted":I
    const/4 v5, 0x1

    if-eq v2, v5, :cond_0

    .line 1472
    new-instance v5, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;

    const-string/jumbo v6, "Error in deleting a task."

    invoke-direct {v5, v6}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Lcom/vlingo/core/internal/schedule/ScheduleUtilException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1477
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    .end local v1    # "myTask":Landroid/net/Uri;
    .end local v2    # "rowsDeleted":I
    .end local v4    # "taskUri":Landroid/net/Uri;
    :catch_0
    move-exception v3

    .line 1478
    .local v3, "sux":Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
    sget-object v5, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->TAG:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1479
    invoke-virtual {v3}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;->printStackTrace()V

    .line 1480
    throw v3

    .line 1475
    .end local v3    # "sux":Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
    .restart local v0    # "contentResolver":Landroid/content/ContentResolver;
    .restart local v1    # "myTask":Landroid/net/Uri;
    .restart local v2    # "rowsDeleted":I
    .restart local v4    # "taskUri":Landroid/net/Uri;
    :cond_0
    return-void
.end method

.method private static doesEventHaveDuration(Landroid/content/Context;J)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J

    .prologue
    .line 282
    const/4 v6, 0x0

    .line 284
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 286
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 287
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    sget-object v2, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->SCHEDULE_END_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 289
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 290
    const-string/jumbo v2, "duration"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 292
    .local v7, "iDurationID":I
    invoke-interface {v6, v7}, Landroid/database/Cursor;->isNull(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 293
    const/4 v2, 0x1

    .line 297
    if-eqz v6, :cond_0

    .line 298
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v7    # "iDurationID":I
    :cond_0
    :goto_0
    return v2

    .line 295
    :cond_1
    const/4 v2, 0x0

    .line 297
    if-eqz v6, :cond_0

    .line 298
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 297
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    .end local v1    # "uri":Landroid/net/Uri;
    :catchall_0
    move-exception v2

    if-eqz v6, :cond_2

    .line 298
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v2
.end method

.method public static getCalendarPreference(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "parameter"    # Ljava/lang/String;

    .prologue
    .line 1723
    const-string/jumbo v8, "com.sec.android.calendar.preference"

    .line 1724
    .local v8, "preferenceAuthority":Ljava/lang/String;
    const-string/jumbo v2, "content://com.sec.android.calendar.preference/Preference"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1725
    .local v1, "preferenceContentURI":Landroid/net/Uri;
    const/4 v9, 0x0

    .line 1726
    .local v9, "result":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1729
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1730
    .local v0, "mContentResolver":Landroid/content/ContentResolver;
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1732
    if-eqz v6, :cond_0

    .line 1733
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 1734
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1735
    const/4 v2, 0x0

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 1745
    :cond_0
    if-eqz v6, :cond_1

    .line 1746
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1747
    const/4 v6, 0x0

    .line 1751
    .end local v0    # "mContentResolver":Landroid/content/ContentResolver;
    :cond_1
    :goto_0
    return-object v9

    .line 1741
    :catch_0
    move-exception v7

    .line 1742
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1743
    const/4 v9, 0x0

    .line 1745
    if-eqz v6, :cond_1

    .line 1746
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1747
    const/4 v6, 0x0

    goto :goto_0

    .line 1745
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v6, :cond_2

    .line 1746
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1747
    const/4 v6, 0x0

    :cond_2
    throw v2
.end method

.method public static getCalendars(Landroid/content/Context;)Ljava/util/List;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1624
    const/4 v8, 0x0

    .line 1625
    .local v8, "cursor":Landroid/database/Cursor;
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    .line 1627
    .local v7, "calendars":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;>;"
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1628
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->EVENT_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1631
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1633
    new-instance v6, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;

    invoke-direct {v6}, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;-><init>()V

    .line 1635
    .local v6, "cal":Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v6, v1, v2}, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->setID(J)V

    .line 1636
    const/4 v1, 0x2

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->setDisplayName(Ljava/lang/String;)V

    .line 1637
    const/4 v1, 0x1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->setAccountName(Ljava/lang/String;)V

    .line 1638
    const/4 v1, 0x3

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v6, v1}, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->setEnabled(I)V

    .line 1639
    const/4 v1, 0x4

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v6, v1}, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->setAccessLevel(I)V

    .line 1640
    const/4 v1, 0x5

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->setAccountType(Ljava/lang/String;)V

    .line 1642
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1645
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    .end local v6    # "cal":Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_0

    .line 1646
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v1

    .line 1645
    .restart local v0    # "contentResolver":Landroid/content/ContentResolver;
    :cond_1
    if-eqz v8, :cond_2

    .line 1646
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1650
    :cond_2
    return-object v7
.end method

.method private static getContentValues(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Landroid/content/ContentValues;
    .locals 7
    .param p0, "task"    # Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .prologue
    const/4 v3, 0x0

    .line 1284
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1286
    .local v2, "values":Landroid/content/ContentValues;
    const-string/jumbo v4, "subject"

    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1287
    const-string/jumbo v4, "body"

    const-string/jumbo v5, ""

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1291
    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->hasDueDate()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1292
    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getBegin()J

    move-result-wide v0

    .line 1293
    .local v0, "dueMillis":J
    const-string/jumbo v4, "due_date"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1294
    const-string/jumbo v4, "utc_due_date"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1305
    .end local v0    # "dueMillis":J
    :cond_0
    const-string/jumbo v4, "accountName"

    const-string/jumbo v5, "My task"

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1306
    const-string/jumbo v4, "accountKey"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1307
    const-string/jumbo v4, "reminder_type"

    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getReminderType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1308
    const-string/jumbo v4, "reminder_time"

    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getReminderTime()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1309
    const-string/jumbo v4, "reminder_set"

    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getReminderSet()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1310
    const-string/jumbo v4, "importance"

    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getImportance()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1311
    const-string/jumbo v4, "groupId"

    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getGroupid()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1312
    const-string/jumbo v4, "bodyType"

    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getBodyType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1313
    const-string/jumbo v4, "body_size"

    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getBodyLength()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1314
    const-string/jumbo v4, "complete"

    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getCompleted()Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v3, 0x1

    :cond_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1316
    return-object v2
.end method

.method public static getCurrentCalendar(Landroid/content/Context;)Ljava/lang/String;
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 1693
    const-string/jumbo v2, "content://com.sec.android.calendar.preference/Preference"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1694
    .local v1, "preferenceContentURI":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1695
    .local v0, "mContentResolver":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 1696
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 1699
    .local v8, "result":Ljava/lang/String;
    const/4 v2, 0x0

    :try_start_0
    const-string/jumbo v3, "preference_defaultCalendar"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1700
    if-eqz v6, :cond_0

    .line 1701
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 1702
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1703
    const/4 v2, 0x0

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 1713
    :cond_0
    if-eqz v6, :cond_1

    .line 1714
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1715
    const/4 v6, 0x0

    .line 1719
    :cond_1
    :goto_0
    return-object v8

    .line 1709
    :catch_0
    move-exception v7

    .line 1710
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1711
    const/4 v8, 0x0

    .line 1713
    if-eqz v6, :cond_1

    .line 1714
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1715
    const/4 v6, 0x0

    goto :goto_0

    .line 1713
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v6, :cond_2

    .line 1714
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1715
    const/4 v6, 0x0

    :cond_2
    throw v2
.end method

.method public static getCurrentCalendarFromProvider(Landroid/content/Context;)Ljava/lang/String;
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 1755
    const-string/jumbo v8, "com.sec.android.calendar.preference"

    .line 1756
    .local v8, "preferenceAuthority":Ljava/lang/String;
    const-string/jumbo v2, "content://com.sec.android.calendar.preference/Preference"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1757
    .local v1, "preferenceContentUri":Landroid/net/Uri;
    const/4 v9, 0x0

    .line 1758
    .local v9, "result":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1761
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1762
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const/4 v2, 0x0

    const-string/jumbo v3, "preference_defaultCalendar"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1764
    if-eqz v6, :cond_0

    .line 1765
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 1766
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1767
    const/4 v2, 0x0

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 1777
    :cond_0
    if-eqz v6, :cond_1

    .line 1778
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1779
    const/4 v6, 0x0

    .line 1783
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    :cond_1
    :goto_0
    return-object v9

    .line 1773
    :catch_0
    move-exception v7

    .line 1774
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1775
    const/4 v9, 0x0

    .line 1777
    if-eqz v6, :cond_1

    .line 1778
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1779
    const/4 v6, 0x0

    goto :goto_0

    .line 1777
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v6, :cond_2

    .line 1778
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1779
    const/4 v6, 0x0

    :cond_2
    throw v2
.end method

.method private static getCurrentCalendarId(Landroid/content/Context;)J
    .locals 17
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1118
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1119
    .local v1, "contentResolver":Landroid/content/ContentResolver;
    const-wide/16 v14, -0x1

    .line 1121
    .local v14, "id":J
    const/4 v2, 0x5

    new-array v3, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v4, "account_name"

    aput-object v4, v3, v2

    const/4 v2, 0x1

    const-string/jumbo v4, "ownerAccount"

    aput-object v4, v3, v2

    const/4 v2, 0x2

    const-string/jumbo v4, "_id"

    aput-object v4, v3, v2

    const/4 v2, 0x3

    const-string/jumbo v4, "sync_events"

    aput-object v4, v3, v2

    const/4 v2, 0x4

    const-string/jumbo v4, "calendar_displayName"

    aput-object v4, v3, v2

    .line 1128
    .local v3, "proj":[Ljava/lang/String;
    const/4 v12, 0x0

    .line 1130
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string/jumbo v16, ""

    .line 1131
    .local v16, "visibleCalendarsWhere":Ljava/lang/String;
    const-string/jumbo v2, "use_hidden_calendars"

    const/4 v4, 0x1

    invoke-static {v2, v4}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1132
    const-string/jumbo v16, " AND visible=1"

    .line 1134
    :cond_0
    sget-object v2, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "sync_events=1 AND calendar_access_level>=500"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 1135
    const-string/jumbo v2, "preference_defaultCalendar"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getCalendarPreference(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1136
    .local v10, "currentCalendar":Ljava/lang/String;
    const-string/jumbo v2, "preference_defaultCalendar_display_name"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getCalendarPreference(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1137
    .local v11, "currentDisplayName":Ljava/lang/String;
    if-eqz v12, :cond_2

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1138
    const-string/jumbo v2, "_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 1140
    :cond_1
    const-string/jumbo v2, "account_name"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1141
    .local v7, "calAcct":Ljava/lang/String;
    const-string/jumbo v2, "ownerAccount"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1142
    .local v9, "calOwner":Ljava/lang/String;
    const-string/jumbo v2, "calendar_displayName"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1144
    .local v8, "calDisplayName":Ljava/lang/String;
    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1145
    const-string/jumbo v2, "_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v14

    .line 1159
    .end local v7    # "calAcct":Ljava/lang/String;
    .end local v8    # "calDisplayName":Ljava/lang/String;
    .end local v9    # "calOwner":Ljava/lang/String;
    :cond_2
    :goto_0
    if-eqz v12, :cond_3

    .line 1160
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1163
    .end local v10    # "currentCalendar":Ljava/lang/String;
    .end local v11    # "currentDisplayName":Ljava/lang/String;
    .end local v16    # "visibleCalendarsWhere":Ljava/lang/String;
    :cond_3
    :goto_1
    return-wide v14

    .line 1149
    .restart local v7    # "calAcct":Ljava/lang/String;
    .restart local v8    # "calDisplayName":Ljava/lang/String;
    .restart local v9    # "calOwner":Ljava/lang/String;
    .restart local v10    # "currentCalendar":Ljava/lang/String;
    .restart local v11    # "currentDisplayName":Ljava/lang/String;
    .restart local v16    # "visibleCalendarsWhere":Ljava/lang/String;
    :cond_4
    :try_start_1
    const-string/jumbo v2, ".*@.*\\..*"

    invoke-virtual {v7, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1150
    const-string/jumbo v2, "_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 1154
    :cond_5
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    goto :goto_0

    .line 1156
    .end local v7    # "calAcct":Ljava/lang/String;
    .end local v8    # "calDisplayName":Ljava/lang/String;
    .end local v9    # "calOwner":Ljava/lang/String;
    .end local v10    # "currentCalendar":Ljava/lang/String;
    .end local v11    # "currentDisplayName":Ljava/lang/String;
    .end local v16    # "visibleCalendarsWhere":Ljava/lang/String;
    :catch_0
    move-exception v13

    .line 1157
    .local v13, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1159
    if-eqz v12, :cond_3

    .line 1160
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 1159
    .end local v13    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v12, :cond_6

    .line 1160
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v2
.end method

.method public static getDateEndMillis(Ljava/lang/String;)J
    .locals 4
    .param p0, "date"    # Ljava/lang/String;

    .prologue
    .line 533
    if-nez p0, :cond_0

    .line 535
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Date should not be null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 537
    :cond_0
    const-wide/16 v0, 0x0

    .line 542
    .local v0, "argMillis":J
    const/4 v2, 0x1

    invoke-static {p0, v2}, Lcom/vlingo/core/internal/schedule/DateUtil;->getMillisFromString(Ljava/lang/String;Z)J

    move-result-wide v0

    .line 543
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/schedule/DateUtil;->endOfGivenDay(J)J

    move-result-wide v2

    return-wide v2
.end method

.method public static getDateStartMillis(Ljava/lang/String;)J
    .locals 4
    .param p0, "date"    # Ljava/lang/String;

    .prologue
    .line 519
    if-nez p0, :cond_0

    .line 521
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Date should not be null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 523
    :cond_0
    const-wide/16 v0, 0x0

    .line 528
    .local v0, "argMillis":J
    const/4 v2, 0x1

    invoke-static {p0, v2}, Lcom/vlingo/core/internal/schedule/DateUtil;->getMillisFromString(Ljava/lang/String;Z)J

    move-result-wide v0

    .line 529
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/schedule/DateUtil;->startOfGivenDay(J)J

    move-result-wide v2

    return-wide v2
.end method

.method private static getEvent(Landroid/content/Context;Landroid/database/Cursor;Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;)Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "indices"    # Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;

    .prologue
    .line 418
    new-instance v5, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-direct {v5}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;-><init>()V

    .line 419
    .local v5, "event":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    move-object/from16 v0, p2

    iget v13, v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->TITLE_COL:I

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v5, v13}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setTitle(Ljava/lang/String;)V

    .line 420
    move-object/from16 v0, p2

    iget v13, v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->EVENT_LOCATION_COL:I

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v5, v13}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setLocation(Ljava/lang/String;)V

    .line 421
    move-object/from16 v0, p2

    iget v13, v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->DESCRIPTION_COL:I

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v5, v13}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setDescription(Ljava/lang/String;)V

    .line 422
    move-object/from16 v0, p2

    iget v13, v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->ORGANIZER_COL:I

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v5, v13}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setOrganizer(Ljava/lang/String;)V

    .line 423
    move-object/from16 v0, p2

    iget v13, v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->EVENT_ID_COL:I

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    invoke-virtual {v5, v13, v14}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setEventID(J)V

    .line 424
    move-object/from16 v0, p2

    iget v13, v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->ALL_DAY_COL:I

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_1

    .line 425
    const/4 v13, 0x1

    invoke-virtual {v5, v13}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setAllDay(Z)V

    .line 426
    move-object/from16 v0, p2

    iget v13, v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->BEGIN_COL:I

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 427
    .local v11, "start":J
    move-object/from16 v0, p2

    iget v13, v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->END_COL:I

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 428
    .local v3, "end":J
    add-long v13, v11, v3

    const-wide/16 v15, 0x2

    div-long v7, v13, v15

    .line 429
    .local v7, "mid":J
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v7, v8}, Ljava/util/Date;-><init>(J)V

    .line 430
    .local v2, "d":Ljava/util/Date;
    const/4 v13, 0x0

    invoke-virtual {v2, v13}, Ljava/util/Date;->setHours(I)V

    .line 431
    const/4 v13, 0x0

    invoke-virtual {v2, v13}, Ljava/util/Date;->setMinutes(I)V

    .line 432
    const/4 v13, 0x0

    invoke-virtual {v2, v13}, Ljava/util/Date;->setSeconds(I)V

    .line 433
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v13

    invoke-virtual {v5, v13, v14}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setBegin(J)V

    .line 434
    const/16 v13, 0x17

    invoke-virtual {v2, v13}, Ljava/util/Date;->setHours(I)V

    .line 435
    const/16 v13, 0x3b

    invoke-virtual {v2, v13}, Ljava/util/Date;->setMinutes(I)V

    .line 436
    const/16 v13, 0x3b

    invoke-virtual {v2, v13}, Ljava/util/Date;->setSeconds(I)V

    .line 437
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v13

    invoke-virtual {v5, v13, v14}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setEnd(J)V

    .line 445
    .end local v2    # "d":Ljava/util/Date;
    .end local v3    # "end":J
    .end local v7    # "mid":J
    .end local v11    # "start":J
    :goto_0
    move-object/from16 v0, p2

    iget v13, v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->ACCESS_LEVEL_COL:I

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 446
    .local v1, "accessLevel":I
    move-object/from16 v0, p2

    iget v13, v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->OWNER_ACCOUNT_COL:I

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 447
    .local v10, "ownerAccount":Ljava/lang/String;
    move-object/from16 v0, p2

    iget v13, v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->ORGANIZER_COL:I

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 448
    .local v9, "organizer":Ljava/lang/String;
    move-object/from16 v0, p2

    iget v13, v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->GUESTS_CAN_MODIFY_COL:I

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 449
    .local v6, "guestCanModify":I
    const/16 v13, 0x1f4

    if-lt v1, v13, :cond_0

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_2

    if-nez v6, :cond_2

    :cond_0
    const/4 v13, 0x1

    :goto_1
    invoke-virtual {v5, v13}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setReadOnly(Z)V

    .line 455
    return-object v5

    .line 442
    .end local v1    # "accessLevel":I
    .end local v6    # "guestCanModify":I
    .end local v9    # "organizer":Ljava/lang/String;
    .end local v10    # "ownerAccount":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p2

    iget v13, v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->BEGIN_COL:I

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    invoke-virtual {v5, v13, v14}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setBegin(J)V

    .line 443
    move-object/from16 v0, p2

    iget v13, v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;->END_COL:I

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    invoke-virtual {v5, v13, v14}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setEnd(J)V

    goto :goto_0

    .line 449
    .restart local v1    # "accessLevel":I
    .restart local v6    # "guestCanModify":I
    .restart local v9    # "organizer":Ljava/lang/String;
    .restart local v10    # "ownerAccount":Ljava/lang/String;
    :cond_2
    const/4 v13, 0x0

    goto :goto_1
.end method

.method public static getEventAttendees(Landroid/content/Context;Lcom/vlingo/core/internal/schedule/ScheduleEvent;I)Ljava/util/List;
    .locals 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "event"    # Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .param p2, "maxCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            "I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 547
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    .line 548
    .local v8, "attendeeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 549
    .local v1, "contentResolver":Landroid/content/ContentResolver;
    const/16 v18, 0x0

    .line 550
    .local v18, "numRows":I
    const/16 v17, 0x0

    .line 551
    .local v17, "numColumns":I
    const/4 v15, 0x0

    .line 552
    .local v15, "i":I
    const/16 v16, 0x0

    .line 553
    .local v16, "j":I
    const-string/jumbo v12, ""

    .line 554
    .local v12, "columnName":Ljava/lang/String;
    const-string/jumbo v13, ""

    .line 555
    .local v13, "columnValue":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "event_id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getID()J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 556
    .local v4, "selection":Ljava/lang/String;
    const-string/jumbo v9, ""

    .line 558
    .local v9, "attendeeName":Ljava/lang/String;
    const/4 v10, 0x0

    .line 561
    .local v10, "c":Landroid/database/Cursor;
    :try_start_0
    sget-object v2, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->ATTENDEES_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    .line 562
    if-nez v10, :cond_1

    .line 602
    if-eqz v10, :cond_0

    .line 603
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 606
    :cond_0
    :goto_0
    return-object v8

    .line 566
    :cond_1
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 567
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v18

    .line 568
    invoke-interface {v10}, Landroid/database/Cursor;->getColumnCount()I

    move-result v17

    .line 570
    const/4 v15, 0x0

    :goto_1
    move/from16 v0, v18

    if-ge v15, v0, :cond_4

    .line 584
    const-string/jumbo v2, "attendeeName"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 585
    invoke-static {v9}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 586
    const-string/jumbo v2, "attendeeEmail"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 587
    .local v7, "attendeeEmail":Ljava/lang/String;
    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 588
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBUtilManager;->getContactDBUtil()Lcom/vlingo/core/internal/contacts/IContactDBUtil;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-interface {v2, v0, v7}, Lcom/vlingo/core/internal/contacts/IContactDBUtil;->getContactDataByEmail(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactData;

    move-result-object v11

    .line 589
    .local v11, "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    if-eqz v11, :cond_2

    .line 590
    iget-object v2, v11, Lcom/vlingo/core/internal/contacts/ContactData;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v9, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 591
    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 597
    .end local v7    # "attendeeEmail":Ljava/lang/String;
    .end local v11    # "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_2
    :goto_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    .line 570
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 595
    :cond_3
    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 599
    :catch_0
    move-exception v14

    .line 600
    .local v14, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 602
    if-eqz v10, :cond_0

    .line 603
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 602
    .end local v14    # "e":Ljava/lang/Exception;
    :cond_4
    if-eqz v10, :cond_0

    .line 603
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 602
    :catchall_0
    move-exception v2

    if-eqz v10, :cond_5

    .line 603
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v2
.end method

.method private static getEventAttendeesCD(Landroid/content/Context;Lcom/vlingo/core/internal/schedule/ScheduleEvent;I)Ljava/util/List;
    .locals 27
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "event"    # Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .param p2, "maxCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 611
    new-instance v17, Ljava/util/LinkedList;

    invoke-direct/range {v17 .. v17}, Ljava/util/LinkedList;-><init>()V

    .line 612
    .local v17, "attendeeList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 613
    .local v2, "contentResolver":Landroid/content/ContentResolver;
    const/16 v26, 0x0

    .line 614
    .local v26, "numRows":I
    const/16 v25, 0x0

    .line 615
    .local v25, "numColumns":I
    const/16 v23, 0x0

    .line 616
    .local v23, "i":I
    const/16 v24, 0x0

    .line 617
    .local v24, "j":I
    const-string/jumbo v20, ""

    .line 618
    .local v20, "columnName":Ljava/lang/String;
    const-string/jumbo v21, ""

    .line 619
    .local v21, "columnValue":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "event_id = "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getID()J

    move-result-wide v8

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 620
    .local v5, "selection":Ljava/lang/String;
    const-string/jumbo v18, ""

    .line 621
    .local v18, "attendeeName":Ljava/lang/String;
    const/4 v3, 0x2

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v8, "attendeeName"

    aput-object v8, v4, v3

    const/4 v3, 0x1

    const-string/jumbo v8, "attendeeEmail"

    aput-object v8, v4, v3

    .line 623
    .local v4, "projection":[Ljava/lang/String;
    const/16 v19, 0x0

    .line 626
    .local v19, "c":Landroid/database/Cursor;
    :try_start_0
    sget-object v3, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->ATTENDEES_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 627
    if-eqz v19, :cond_2

    .line 629
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    .line 630
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    move-result v26

    .line 631
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getColumnCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v25

    .line 633
    const/16 v23, 0x0

    move-object/from16 v7, v18

    .end local v18    # "attendeeName":Ljava/lang/String;
    .local v7, "attendeeName":Ljava/lang/String;
    :goto_0
    move/from16 v0, v23

    move/from16 v1, v26

    if-ge v0, v1, :cond_3

    .line 646
    :try_start_1
    const-string/jumbo v3, "attendeeEmail"

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 647
    .local v10, "attendeeEmail":Ljava/lang/String;
    const-string/jumbo v3, "attendeeName"

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 648
    invoke-static {v10}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getOrganizer()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getOrganizer()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 649
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBUtilManager;->getContactDBUtil()Lcom/vlingo/core/internal/contacts/IContactDBUtil;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-interface {v3, v0, v10}, Lcom/vlingo/core/internal/contacts/IContactDBUtil;->getContactDataByEmail(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactData;

    move-result-object v11

    .line 650
    .local v11, "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    if-nez v11, :cond_0

    .line 651
    new-instance v6, Lcom/vlingo/core/internal/contacts/ContactMatch;

    const-wide/16 v8, 0x0

    const/4 v11, 0x0

    invoke-direct/range {v6 .. v11}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;JLjava/lang/String;Z)V

    .line 652
    .end local v11    # "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    .local v6, "cm":Lcom/vlingo/core/internal/contacts/ContactMatch;
    new-instance v11, Lcom/vlingo/core/internal/contacts/ContactData;

    sget-object v13, Lcom/vlingo/core/internal/contacts/ContactData$Kind;->Email:Lcom/vlingo/core/internal/contacts/ContactData$Kind;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v12, v6

    move-object v14, v10

    invoke-direct/range {v11 .. v16}, Lcom/vlingo/core/internal/contacts/ContactData;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactData$Kind;Ljava/lang/String;II)V

    .line 654
    .end local v6    # "cm":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v11    # "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_0
    move-object/from16 v0, v17

    invoke-interface {v0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 656
    .end local v11    # "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_1
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 633
    add-int/lit8 v23, v23, 0x1

    goto :goto_0

    .end local v7    # "attendeeName":Ljava/lang/String;
    .end local v10    # "attendeeEmail":Ljava/lang/String;
    .restart local v18    # "attendeeName":Ljava/lang/String;
    :cond_2
    move-object/from16 v7, v18

    .line 662
    .end local v18    # "attendeeName":Ljava/lang/String;
    .restart local v7    # "attendeeName":Ljava/lang/String;
    :cond_3
    if-eqz v19, :cond_4

    .line 663
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 666
    :cond_4
    :goto_1
    return-object v17

    .line 659
    .end local v7    # "attendeeName":Ljava/lang/String;
    .restart local v18    # "attendeeName":Ljava/lang/String;
    :catch_0
    move-exception v22

    move-object/from16 v7, v18

    .line 660
    .end local v18    # "attendeeName":Ljava/lang/String;
    .restart local v7    # "attendeeName":Ljava/lang/String;
    .local v22, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_2
    invoke-virtual/range {v22 .. v22}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 662
    if-eqz v19, :cond_4

    .line 663
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 662
    .end local v7    # "attendeeName":Ljava/lang/String;
    .end local v22    # "e":Ljava/lang/Exception;
    .restart local v18    # "attendeeName":Ljava/lang/String;
    :catchall_0
    move-exception v3

    move-object/from16 v7, v18

    .end local v18    # "attendeeName":Ljava/lang/String;
    .restart local v7    # "attendeeName":Ljava/lang/String;
    :goto_3
    if-eqz v19, :cond_5

    .line 663
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v3

    .line 662
    :catchall_1
    move-exception v3

    goto :goto_3

    .line 659
    :catch_1
    move-exception v22

    goto :goto_2
.end method

.method public static getEventText(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Ljava/lang/String;
    .locals 6
    .param p0, "e"    # Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .prologue
    .line 1514
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1515
    .local v1, "sb":Ljava/lang/StringBuffer;
    if-eqz p0, :cond_0

    .line 1516
    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1517
    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1518
    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getAllDay()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1520
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_schedule_all_day:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1521
    .local v0, "allDayString":Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1528
    .end local v0    # "allDayString":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 1525
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBegin()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEnd()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->makeTimeTTSString(JJ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method private static getEvents(Landroid/content/Context;ILandroid/database/Cursor;Z)Ljava/util/List;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "limit"    # I
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "shouldCountFromEnd"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Landroid/database/Cursor;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 385
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 387
    .local v1, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    if-eqz p2, :cond_5

    .line 389
    if-eqz p3, :cond_3

    .line 390
    :try_start_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToLast()Z

    move-result v2

    .line 395
    .local v2, "hasValues":Z
    :goto_0
    if-eqz v2, :cond_1

    .line 396
    new-instance v3, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;

    invoke-direct {v3, p2}, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;-><init>(Landroid/database/Cursor;)V

    .line 398
    .local v3, "indices":Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;
    :cond_0
    invoke-static {p0, p2, v3}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getEvent(Landroid/content/Context;Landroid/database/Cursor;Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;)Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    move-result-object v0

    .line 402
    .local v0, "event":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 404
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v4, p1, :cond_1

    if-eqz p3, :cond_4

    invoke-interface {p2}, Landroid/database/Cursor;->moveToPrevious()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_0

    .line 410
    .end local v0    # "event":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .end local v2    # "hasValues":Z
    .end local v3    # "indices":Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;
    :cond_1
    :goto_1
    if-eqz p2, :cond_2

    .line 411
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 414
    :cond_2
    return-object v1

    .line 393
    :cond_3
    :try_start_1
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    .restart local v2    # "hasValues":Z
    goto :goto_0

    .line 404
    .restart local v0    # "event":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .restart local v3    # "indices":Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;
    :cond_4
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_0

    goto :goto_1

    .line 407
    .end local v0    # "event":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .end local v2    # "hasValues":Z
    .end local v3    # "indices":Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleEventIndices;
    :cond_5
    sget-object v4, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "Null event cursor.  This should never happen as the values passed are android contract values."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 410
    :catchall_0
    move-exception v4

    if-eqz p2, :cond_6

    .line 411
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v4
.end method

.method public static getFiredCalendarAlerts(Landroid/content/Context;I)Ljava/util/List;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 909
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 910
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const-string/jumbo v3, "state=1"

    .line 911
    .local v3, "selection":Ljava/lang/String;
    const-string/jumbo v5, "allDay DESC, begin ASC, title ASC LIMIT 50"

    .line 913
    .local v5, "order":Ljava/lang/String;
    sget-object v1, Landroid/provider/CalendarContract$CalendarAlerts;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->SCHEDULE_PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 914
    .local v6, "c":Landroid/database/Cursor;
    const/4 v1, 0x0

    invoke-static {p0, p1, v6, v1}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getEvents(Landroid/content/Context;ILandroid/database/Cursor;Z)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public static getNextScheduleEvent(Landroid/content/Context;)Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    .line 849
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 850
    .local v2, "nowMillis":J
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/schedule/DateUtil;->endOfGivenDay(J)J

    move-result-wide v4

    .line 852
    .local v4, "endMillis":J
    const/4 v7, 0x0

    .line 857
    .local v7, "event":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    const/4 v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getTimeBoxedScheduleItems(Landroid/content/Context;IJJZ)Ljava/util/List;

    move-result-object v8

    .line 858
    .local v8, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    .line 859
    .local v9, "eventsFound":I
    if-lez v9, :cond_0

    .line 860
    invoke-interface {v8, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "event":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    check-cast v7, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 862
    .restart local v7    # "event":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    :cond_0
    return-object v7
.end method

.method public static getNextScheduleItems(Landroid/content/Context;JJI)Ljava/util/List;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "startMillis"    # J
    .param p3, "endMillis"    # J
    .param p5, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "JJI)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 868
    const/4 v6, 0x0

    move-object v0, p0

    move v1, p5

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v6}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getTimeBoxedScheduleItems(Landroid/content/Context;IJJZ)Ljava/util/List;

    move-result-object v7

    .line 869
    .local v7, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    return-object v7
.end method

.method public static getPrevScheduleItems(Landroid/content/Context;I)Ljava/util/List;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 881
    const-wide/16 v2, 0x0

    .line 882
    .local v2, "beginMillis":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 884
    .local v4, "nowMillis":J
    new-instance v8, Landroid/text/format/Time;

    invoke-direct {v8}, Landroid/text/format/Time;-><init>()V

    .line 885
    .local v8, "time":Landroid/text/format/Time;
    invoke-virtual {v8, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 886
    iput v6, v8, Landroid/text/format/Time;->hour:I

    .line 887
    iput v6, v8, Landroid/text/format/Time;->minute:I

    .line 888
    iput v6, v8, Landroid/text/format/Time;->second:I

    .line 889
    invoke-virtual {v8, v6}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    move-object v0, p0

    move v1, p1

    .line 896
    invoke-static/range {v0 .. v6}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getTimeBoxedScheduleItems(Landroid/content/Context;IJJZ)Ljava/util/List;

    move-result-object v7

    .line 897
    .local v7, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    return-object v7
.end method

.method public static getReminderContentValues(Landroid/net/Uri;Lcom/vlingo/core/internal/schedule/ScheduleTask;)Landroid/content/ContentValues;
    .locals 8
    .param p0, "newEventUri"    # Landroid/net/Uri;
    .param p1, "task"    # Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .prologue
    const/4 v7, 0x0

    .line 1216
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1217
    .local v3, "values":Landroid/content/ContentValues;
    invoke-virtual {p0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1218
    .local v2, "mid":I
    const-string/jumbo v4, "task_id"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1219
    const-string/jumbo v4, "reminder_time"

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getReminderTime()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1220
    const-string/jumbo v4, "state"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1221
    const-string/jumbo v4, "start_date"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1222
    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->hasDueDate()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1223
    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getBegin()J

    move-result-wide v0

    .line 1224
    .local v0, "dueMillis":J
    const-string/jumbo v4, "due_date"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1226
    .end local v0    # "dueMillis":J
    :cond_0
    const-string/jumbo v4, "accountkey"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1227
    const-string/jumbo v4, "subject"

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1228
    const-string/jumbo v4, "reminder_type"

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1232
    return-object v3
.end method

.method private static getRowIDFromURI(Landroid/net/Uri;)I
    .locals 6
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1168
    const/4 v3, 0x0

    .line 1170
    .local v3, "rowID":I
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    .line 1171
    .local v4, "uriPaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p0, :cond_0

    .line 1173
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    .line 1174
    .local v0, "count":I
    if-lez v0, :cond_0

    .line 1177
    add-int/lit8 v5, v0, -0x1

    :try_start_0
    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1178
    .local v2, "number":Ljava/lang/String;
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 1185
    .end local v0    # "count":I
    .end local v2    # "number":Ljava/lang/String;
    :cond_0
    :goto_0
    return v3

    .line 1179
    .restart local v0    # "count":I
    :catch_0
    move-exception v1

    .line 1180
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getScheduledEvents(Landroid/content/Context;Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;)Ljava/util/List;
    .locals 16
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "queryObject"    # Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 709
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    .line 711
    .local v13, "nowMillis":J
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->getBegin()J

    move-result-wide v3

    .line 715
    .local v3, "begin":J
    const-wide/16 v1, 0x0

    cmp-long v1, v3, v1

    if-nez v1, :cond_0

    .line 716
    move-wide v3, v13

    .line 722
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->getEnd()J

    move-result-wide v5

    .line 726
    .local v5, "end":J
    const-wide/16 v1, 0x0

    cmp-long v1, v5, v1

    if-nez v1, :cond_1

    .line 727
    invoke-static {v3, v4}, Lcom/vlingo/core/internal/schedule/DateUtil;->endOfGivenDay(J)J

    move-result-wide v5

    .line 731
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->getCount()I

    move-result v12

    .line 732
    .local v12, "maxCount":I
    const/4 v1, 0x1

    if-ge v12, v1, :cond_2

    .line 733
    const/4 v12, 0x6

    .line 741
    :cond_2
    const/16 v2, 0xfa0

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->shouldCountFromEnd()Z

    move-result v7

    move-object/from16 v1, p0

    invoke-static/range {v1 .. v7}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getTimeBoxedScheduleItems(Landroid/content/Context;IJJZ)Ljava/util/List;

    move-result-object v9

    .line 743
    .local v9, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    const/4 v10, 0x0

    .line 744
    .local v10, "eventsAdded":I
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 745
    .local v15, "results":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 746
    .local v8, "event":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->matches(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 747
    if-ge v10, v12, :cond_3

    .line 748
    sget v1, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->MAX_ATTENDEES:I

    move-object/from16 v0, p0

    invoke-static {v0, v8, v1}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getEventAttendeesCD(Landroid/content/Context;Lcom/vlingo/core/internal/schedule/ScheduleEvent;I)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v8, v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setContactDataList(Ljava/util/List;)V

    .line 749
    invoke-interface {v15, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 750
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 754
    .end local v8    # "event":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    :cond_4
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_8

    .line 755
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_5
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 756
    .restart local v8    # "event":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->containsString(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 757
    if-ge v10, v12, :cond_5

    .line 758
    sget v1, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->MAX_ATTENDEES:I

    move-object/from16 v0, p0

    invoke-static {v0, v8, v1}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getEventAttendeesCD(Landroid/content/Context;Lcom/vlingo/core/internal/schedule/ScheduleEvent;I)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v8, v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setContactDataList(Ljava/util/List;)V

    .line 759
    invoke-interface {v15, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 760
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 764
    .end local v8    # "event":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    :cond_6
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_8

    .line 765
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_7
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 766
    .restart local v8    # "event":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/vlingo/core/internal/schedule/ScheduleQueryObject;->containsWord(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Z

    move-result v1

    if-eqz v1, :cond_7

    if-ge v10, v12, :cond_7

    .line 767
    sget v1, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->MAX_ATTENDEES:I

    move-object/from16 v0, p0

    invoke-static {v0, v8, v1}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getEventAttendeesCD(Landroid/content/Context;Lcom/vlingo/core/internal/schedule/ScheduleEvent;I)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v8, v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setContactDataList(Ljava/util/List;)V

    .line 768
    invoke-interface {v15, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 769
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 774
    .end local v8    # "event":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    :cond_8
    return-object v15
.end method

.method public static getTask(Landroid/content/Context;J)Lcom/vlingo/core/internal/schedule/ScheduleTask;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
        }
    .end annotation

    .prologue
    .line 1328
    const/4 v7, 0x0

    .line 1329
    .local v7, "cur":Landroid/database/Cursor;
    const/4 v10, 0x0

    .line 1331
    .local v10, "task":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1333
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const-string/jumbo v2, "content://com.android.calendar/syncTasks"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 1334
    .local v11, "taskUri":Landroid/net/Uri;
    invoke-static {v11, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 1337
    .local v1, "myTask":Landroid/net/Uri;
    sget-object v2, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->TASK_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1339
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 1341
    .local v6, "count":I
    if-eqz v7, :cond_0

    const/4 v2, 0x1

    if-ge v6, v2, :cond_2

    .line 1342
    :cond_0
    new-instance v2, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;

    const-string/jumbo v3, "Error in getting task."

    invoke-direct {v2, v3}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Lcom/vlingo/core/internal/schedule/ScheduleUtilException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1349
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    .end local v1    # "myTask":Landroid/net/Uri;
    .end local v6    # "count":I
    .end local v11    # "taskUri":Landroid/net/Uri;
    :catch_0
    move-exception v9

    .line 1350
    .local v9, "sux":Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
    :try_start_1
    sget-object v2, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->TAG:Ljava/lang/String;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1351
    invoke-virtual {v9}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;->printStackTrace()V

    .line 1352
    throw v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1354
    .end local v9    # "sux":Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
    :catchall_0
    move-exception v2

    if-eqz v7, :cond_1

    .line 1355
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v2

    .line 1345
    .restart local v0    # "contentResolver":Landroid/content/ContentResolver;
    .restart local v1    # "myTask":Landroid/net/Uri;
    .restart local v6    # "count":I
    .restart local v11    # "taskUri":Landroid/net/Uri;
    :cond_2
    :try_start_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1346
    new-instance v8, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;

    invoke-direct {v8, v7}, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;-><init>(Landroid/database/Cursor;)V

    .line 1347
    .local v8, "indices":Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;
    invoke-static {v7, v8}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getTask(Landroid/database/Cursor;Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;)Lcom/vlingo/core/internal/schedule/ScheduleTask;
    :try_end_2
    .catch Lcom/vlingo/core/internal/schedule/ScheduleUtilException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v10

    .line 1354
    .end local v8    # "indices":Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;
    :cond_3
    if-eqz v7, :cond_4

    .line 1355
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1358
    :cond_4
    return-object v10
.end method

.method private static getTask(Landroid/database/Cursor;Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;)Lcom/vlingo/core/internal/schedule/ScheduleTask;
    .locals 3
    .param p0, "cur"    # Landroid/database/Cursor;
    .param p1, "indices"    # Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;

    .prologue
    .line 1362
    new-instance v0, Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-direct {v0}, Lcom/vlingo/core/internal/schedule/ScheduleTask;-><init>()V

    .line 1363
    .local v0, "task":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    iget v1, p1, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_SUBJECT_COL:I

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setTitle(Ljava/lang/String;)V

    .line 1364
    iget v1, p1, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_ID_COL:I

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setEventID(J)V

    .line 1365
    iget v1, p1, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_DUE_DATE_COL:I

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setBegin(J)V

    .line 1366
    iget v1, p1, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_UTC_DUE_DATE_COL:I

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setUtcDueDate(J)V

    .line 1367
    iget v1, p1, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_ACCOUNT_NAME_COL:I

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setAccountName(Ljava/lang/String;)V

    .line 1368
    iget v1, p1, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_REMINDER_TYPE_COL:I

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setReminderType(I)V

    .line 1369
    iget v1, p1, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_REMINDER_SET_COL:I

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setReminderSet(I)V

    .line 1370
    iget v1, p1, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_REMINDER_TIME_COL:I

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setReminderTime(J)V

    .line 1371
    iget v1, p1, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_IMPORTANCE_COL:I

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setImportance(I)V

    .line 1372
    iget v1, p1, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_GROUPID_COL:I

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setGroupId(I)V

    .line 1373
    iget v1, p1, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_BODY_COL:I

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setDescription(Ljava/lang/String;)V

    .line 1374
    iget v1, p1, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_COMPLETE_COL:I

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setCompleted(Z)V

    .line 1375
    return-object v0

    .line 1374
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static getTaskItems(Landroid/content/Context;ILcom/vlingo/core/internal/schedule/TaskQueryObject;)Ljava/util/List;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "limit"    # I
    .param p2, "queryObject"    # Lcom/vlingo/core/internal/schedule/TaskQueryObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/vlingo/core/internal/schedule/TaskQueryObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleTask;",
            ">;"
        }
    .end annotation

    .prologue
    .line 361
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 362
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const-string/jumbo v2, "content://com.android.calendar/syncTasks"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 364
    .local v1, "taskUri":Landroid/net/Uri;
    const-string/jumbo v3, "complete=0"

    .line 365
    .local v3, "where":Ljava/lang/String;
    invoke-static {p0, v1}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->addDeleted(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 366
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, " AND deleted=0"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 373
    :cond_0
    invoke-virtual {p2}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->getMatchUndated()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p2}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->getBegin()J

    move-result-wide v4

    const-wide/16 v7, 0x0

    cmp-long v2, v4, v7

    if-lez v2, :cond_1

    .line 374
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, " AND due_date>="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->getBegin()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, " AND "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "due_date"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "<="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->getEnd()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 376
    :cond_1
    sget-object v2, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->TASK_PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "due_date"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 381
    .local v6, "c":Landroid/database/Cursor;
    invoke-virtual {p2}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->shouldCountFromEnd()Z

    move-result v2

    invoke-static {p1, v6, v2}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getTasks(ILandroid/database/Cursor;Z)Ljava/util/List;

    move-result-object v2

    return-object v2
.end method

.method private static getTasks(ILandroid/database/Cursor;Z)Ljava/util/List;
    .locals 6
    .param p0, "limit"    # I
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "shouldCountFromEnd"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/database/Cursor;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleTask;",
            ">;"
        }
    .end annotation

    .prologue
    .line 487
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 488
    .local v4, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    if-eqz p1, :cond_1

    .line 490
    if-eqz p2, :cond_3

    .line 491
    invoke-interface {p1}, Landroid/database/Cursor;->moveToLast()Z

    move-result v1

    .line 496
    .local v1, "hasValues":Z
    :goto_0
    if-eqz v1, :cond_1

    .line 497
    new-instance v2, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;

    invoke-direct {v2, p1}, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;-><init>(Landroid/database/Cursor;)V

    .line 499
    .local v2, "indices":Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;
    :cond_0
    invoke-static {p1, v2}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getTask(Landroid/database/Cursor;Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;)Lcom/vlingo/core/internal/schedule/ScheduleTask;

    move-result-object v3

    .line 501
    .local v3, "task":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {v3, v5}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->normalize(Z)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 508
    :goto_1
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 509
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-ge v5, p0, :cond_1

    if-eqz p2, :cond_4

    invoke-interface {p1}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v5

    if-nez v5, :cond_0

    .line 512
    .end local v1    # "hasValues":Z
    .end local v2    # "indices":Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;
    .end local v3    # "task":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    :cond_1
    :goto_2
    if-eqz p1, :cond_2

    .line 513
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 515
    :cond_2
    return-object v4

    .line 494
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    .restart local v1    # "hasValues":Z
    goto :goto_0

    .line 502
    .restart local v2    # "indices":Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;
    .restart local v3    # "task":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    :catch_0
    move-exception v0

    .line 504
    .local v0, "e":Ljava/text/ParseException;
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_1

    .line 509
    .end local v0    # "e":Ljava/text/ParseException;
    :cond_4
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_0

    goto :goto_2
.end method

.method public static getTasks(Landroid/content/Context;Lcom/vlingo/core/internal/schedule/TaskQueryObject;)Ljava/util/List;
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "queryObject"    # Lcom/vlingo/core/internal/schedule/TaskQueryObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vlingo/core/internal/schedule/TaskQueryObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleTask;",
            ">;"
        }
    .end annotation

    .prologue
    .line 780
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 782
    .local v7, "nowMillis":J
    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->getBegin()J

    move-result-wide v1

    .line 786
    .local v1, "begin":J
    const-wide/16 v12, 0x0

    cmp-long v12, v1, v12

    if-nez v12, :cond_0

    .line 787
    move-wide v1, v7

    .line 794
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->getEnd()J

    move-result-wide v3

    .line 798
    .local v3, "end":J
    const-wide/16 v12, 0x0

    cmp-long v12, v3, v12

    if-nez v12, :cond_1

    .line 799
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/schedule/DateUtil;->endOfGivenDay(J)J

    move-result-wide v3

    .line 804
    :cond_1
    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->getCount()I

    move-result v6

    .line 805
    .local v6, "maxCount":I
    const/4 v12, 0x1

    if-ge v6, v12, :cond_2

    .line 806
    const/4 v6, 0x6

    .line 814
    :cond_2
    const/16 v12, 0xfa0

    invoke-static {p0, v12, p1}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getTaskItems(Landroid/content/Context;ILcom/vlingo/core/internal/schedule/TaskQueryObject;)Ljava/util/List;

    move-result-object v11

    .line 816
    .local v11, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    const/4 v0, 0x0

    .line 817
    .local v0, "addedCount":I
    new-instance v9, Ljava/util/LinkedList;

    invoke-direct {v9}, Ljava/util/LinkedList;-><init>()V

    .line 818
    .local v9, "results":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .line 819
    .local v10, "task":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    invoke-virtual {p1, v10}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->matches(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 820
    if-ge v0, v6, :cond_3

    .line 821
    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 822
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 826
    .end local v10    # "task":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    :cond_4
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v12

    if-nez v12, :cond_8

    .line 827
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .line 828
    .restart local v10    # "task":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    invoke-virtual {p1, v10}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->containsString(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 829
    if-ge v0, v6, :cond_5

    .line 830
    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 831
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 835
    .end local v10    # "task":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    :cond_6
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v12

    if-nez v12, :cond_8

    .line 836
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_7
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .line 837
    .restart local v10    # "task":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    invoke-virtual {p1, v10}, Lcom/vlingo/core/internal/schedule/TaskQueryObject;->containsWord(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Z

    move-result v12

    if-eqz v12, :cond_7

    if-ge v0, v6, :cond_7

    .line 838
    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 839
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 844
    .end local v10    # "task":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    :cond_8
    return-object v9
.end method

.method private static getTimeBoxedScheduleItems(Landroid/content/Context;IJJZ)Ljava/util/List;
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "limit"    # I
    .param p2, "beginMS"    # J
    .param p4, "endMS"    # J
    .param p6, "shouldCountFromEnd"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IJJZ)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 308
    const/4 v9, 0x0

    .line 309
    .local v9, "c":Landroid/database/Cursor;
    const-string/jumbo v2, "use_hidden_calendars"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 310
    sget-object v14, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->EVENTS_INSTANCES_CONTENT_URI:Landroid/net/Uri;

    .line 311
    .local v14, "uri":Landroid/net/Uri;
    invoke-virtual {v14}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    .line 312
    .local v8, "builder":Landroid/net/Uri$Builder;
    move-wide/from16 v0, p2

    invoke-static {v8, v0, v1}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 313
    move-wide/from16 v0, p4

    invoke-static {v8, v0, v1}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 314
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->SCHEDULE_PROJECTION:[Ljava/lang/String;

    const-string/jumbo v5, "calendar_access_level != 200"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/String;

    const-string/jumbo v7, "begin ASC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 326
    .end local v8    # "builder":Landroid/net/Uri$Builder;
    .end local v14    # "uri":Landroid/net/Uri;
    :goto_0
    move/from16 v0, p1

    move/from16 v1, p6

    invoke-static {p0, v0, v9, v1}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getEvents(Landroid/content/Context;ILandroid/database/Cursor;Z)Ljava/util/List;

    move-result-object v13

    .line 327
    .local v13, "prefilterList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 328
    .local v12, "postFilterList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 329
    .local v10, "evnt":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    invoke-virtual {v10}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBegin()J

    move-result-wide v2

    cmp-long v2, v2, p2

    if-ltz v2, :cond_1

    invoke-virtual {v10}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBegin()J

    move-result-wide v2

    cmp-long v2, v2, p4

    if-lez v2, :cond_2

    :cond_1
    invoke-virtual {v10}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBegin()J

    move-result-wide v2

    cmp-long v2, p2, v2

    if-ltz v2, :cond_0

    invoke-virtual {v10}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEnd()J

    move-result-wide v2

    cmp-long v2, p2, v2

    if-gtz v2, :cond_0

    .line 330
    :cond_2
    invoke-interface {v12, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 318
    .end local v10    # "evnt":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v12    # "postFilterList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    .end local v13    # "prefilterList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    :cond_3
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->SCHEDULE_PROJECTION:[Ljava/lang/String;

    move-wide/from16 v4, p2

    move-wide/from16 v6, p4

    invoke-static/range {v2 .. v7}, Landroid/provider/CalendarContract$Instances;->query(Landroid/content/ContentResolver;[Ljava/lang/String;JJ)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    goto :goto_0

    .line 334
    .restart local v11    # "i$":Ljava/util/Iterator;
    .restart local v12    # "postFilterList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    .restart local v13    # "prefilterList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    :cond_4
    return-object v12

    .line 319
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v12    # "postFilterList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    .end local v13    # "prefilterList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static getTimeTTSString(J)Ljava/lang/String;
    .locals 8
    .param p0, "mills"    # J

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1542
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1543
    .local v0, "sb":Ljava/lang/StringBuffer;
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 1544
    .local v1, "t1":Landroid/text/format/Time;
    invoke-virtual {v1, p0, p1}, Landroid/text/format/Time;->set(J)V

    .line 1545
    invoke-virtual {v1, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 1550
    const-string/jumbo v4, "%H %M "

    invoke-virtual {v1, v4}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1551
    .local v2, "time":Ljava/lang/String;
    const-string/jumbo v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 1552
    .local v3, "timeSplit":[Ljava/lang/String;
    aget-object v4, v3, v7

    const-string/jumbo v5, "00"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1553
    const-string/jumbo v4, " zero "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1558
    :goto_0
    aget-object v4, v3, v6

    const-string/jumbo v5, "00"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1559
    const-string/jumbo v4, "hundred hours "

    invoke-virtual {v1, v4}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1566
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 1556
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v3, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 1562
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v3, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1
.end method

.method public static hasCalendarAccount(Landroid/content/Context;)Z
    .locals 4
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 1661
    invoke-static {p0}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getCalendars(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 1664
    .local v1, "accounts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;

    .line 1665
    .local v0, "account":Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1668
    const/4 v3, 0x1

    .line 1673
    .end local v0    # "account":Lcom/vlingo/core/internal/schedule/ScheduleUtil$ScheduleCalendar;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static isAttendeeContainInEvent(Landroid/content/Context;JLjava/lang/String;)Z
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "eventId"    # J
    .param p3, "query"    # Ljava/lang/String;

    .prologue
    .line 677
    const/4 v10, 0x0

    .line 678
    .local v10, "isContain":Z
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "event_id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 679
    .local v3, "selection":Ljava/lang/String;
    const/4 v7, 0x0

    .line 682
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->ATTENDEES_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 683
    if-eqz v7, :cond_0

    .line 684
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 685
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge v9, v0, :cond_0

    .line 686
    const-string/jumbo v0, "attendeeName"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 687
    .local v6, "attendeeName":Ljava/lang/String;
    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v6, p3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    .line 689
    const/4 v10, 0x1

    .line 698
    .end local v6    # "attendeeName":Ljava/lang/String;
    .end local v9    # "i":I
    :cond_0
    if-eqz v7, :cond_1

    .line 699
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 703
    :cond_1
    :goto_1
    return v10

    .line 692
    .restart local v6    # "attendeeName":Ljava/lang/String;
    .restart local v9    # "i":I
    :cond_2
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 685
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 695
    .end local v6    # "attendeeName":Ljava/lang/String;
    .end local v9    # "i":I
    :catch_0
    move-exception v8

    .line 696
    .local v8, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 698
    if-eqz v7, :cond_1

    .line 699
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 698
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_3

    .line 699
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public static listTask(Landroid/content/Context;)[Lcom/vlingo/core/internal/schedule/ScheduleTask;
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
        }
    .end annotation

    .prologue
    const/4 v13, 0x1

    .line 1416
    const/4 v7, 0x0

    .line 1417
    .local v7, "cur":Landroid/database/Cursor;
    const/4 v12, 0x0

    .line 1419
    .local v12, "tasksArray":[Lcom/vlingo/core/internal/schedule/ScheduleTask;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1421
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const-string/jumbo v2, "content://com.android.calendar/syncTasks"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1424
    .local v1, "eventUri":Landroid/net/Uri;
    sget-object v2, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->TASK_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1426
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 1428
    .local v6, "count":I
    if-eqz v7, :cond_0

    if-ge v6, v13, :cond_2

    .line 1429
    :cond_0
    new-instance v2, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;

    const-string/jumbo v3, "Error in listing tasks."

    invoke-direct {v2, v3}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Lcom/vlingo/core/internal/schedule/ScheduleUtilException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1442
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    .end local v1    # "eventUri":Landroid/net/Uri;
    .end local v6    # "count":I
    .end local v12    # "tasksArray":[Lcom/vlingo/core/internal/schedule/ScheduleTask;
    :catch_0
    move-exception v9

    .line 1443
    .local v9, "sux":Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
    :try_start_1
    sget-object v2, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->TAG:Ljava/lang/String;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1444
    invoke-virtual {v9}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;->printStackTrace()V

    .line 1445
    throw v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1447
    .end local v9    # "sux":Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
    :catchall_0
    move-exception v2

    if-eqz v7, :cond_1

    .line 1448
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v2

    .line 1432
    .restart local v0    # "contentResolver":Landroid/content/ContentResolver;
    .restart local v1    # "eventUri":Landroid/net/Uri;
    .restart local v6    # "count":I
    .restart local v12    # "tasksArray":[Lcom/vlingo/core/internal/schedule/ScheduleTask;
    :cond_2
    :try_start_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1433
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1434
    .local v11, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    new-instance v8, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;

    invoke-direct {v8, v7}, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;-><init>(Landroid/database/Cursor;)V

    .line 1436
    .local v8, "indices":Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;
    :cond_3
    invoke-static {v7, v8}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getTask(Landroid/database/Cursor;Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;)Lcom/vlingo/core/internal/schedule/ScheduleTask;

    move-result-object v10

    .line 1437
    .local v10, "task":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    invoke-interface {v11, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1438
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1440
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-interface {v11, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v12

    .end local v12    # "tasksArray":[Lcom/vlingo/core/internal/schedule/ScheduleTask;
    check-cast v12, [Lcom/vlingo/core/internal/schedule/ScheduleTask;
    :try_end_2
    .catch Lcom/vlingo/core/internal/schedule/ScheduleUtilException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1447
    .end local v8    # "indices":Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;
    .end local v10    # "task":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    .end local v11    # "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    .restart local v12    # "tasksArray":[Lcom/vlingo/core/internal/schedule/ScheduleTask;
    :cond_4
    if-eqz v7, :cond_5

    .line 1448
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1451
    :cond_5
    return-object v12
.end method

.method private static makeTimeTTSString(JJ)Ljava/lang/String;
    .locals 3
    .param p0, "beginTime"    # J
    .param p2, "endTime"    # J

    .prologue
    .line 1532
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1533
    .local v0, "sb":Ljava/lang/StringBuffer;
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getTimeTTSString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1534
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_schedule_to:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1535
    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1536
    invoke-static {p2, p3}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getTimeTTSString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1538
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static mergeContactList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 968
    .local p0, "originalContactList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .local p1, "changedContactList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 969
    .local v3, "mergedContactList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-interface {v3, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 970
    if-eqz p0, :cond_2

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    if-eqz v5, :cond_2

    .line 971
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 972
    .local v0, "changedContactData":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 973
    .local v4, "originalContactData":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-virtual {v0, v4}, Lcom/vlingo/core/internal/contacts/ContactData;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 974
    invoke-interface {v3, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 979
    .end local v0    # "changedContactData":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "originalContactData":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_2
    return-object v3
.end method

.method public static updateEvent(Landroid/content/Context;Lcom/vlingo/core/internal/schedule/ScheduleEvent;Lcom/vlingo/core/internal/schedule/ScheduleEvent;)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "originalEvent"    # Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .param p2, "changedEvent"    # Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
        }
    .end annotation

    .prologue
    const-wide/16 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 918
    invoke-virtual {p2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getAllDay()Z

    move-result v0

    .line 919
    .local v0, "bAllDay":Z
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 920
    .local v5, "values":Landroid/content/ContentValues;
    const-string/jumbo v6, "title"

    invoke-virtual {p2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 921
    const-string/jumbo v6, "eventLocation"

    invoke-virtual {p2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getLocation()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 922
    const-string/jumbo v6, "description"

    invoke-virtual {p2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getDescription()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 923
    if-eqz v0, :cond_1

    .line 924
    const-string/jumbo v6, "dtstart"

    invoke-virtual {p2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBegin()J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/vlingo/core/internal/schedule/DateUtil;->getUTCTimeMillis(J)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 925
    const-string/jumbo v6, "eventTimezone"

    const-string/jumbo v7, "UTC"

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 926
    const-string/jumbo v6, "allDay"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 935
    :goto_0
    iget-wide v6, p1, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->id:J

    invoke-static {p0, v6, v7}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->doesEventHaveDuration(Landroid/content/Context;J)Z

    move-result v6

    if-nez v6, :cond_0

    .line 936
    invoke-virtual {p2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEnd()J

    move-result-wide v6

    cmp-long v6, v6, v11

    if-eqz v6, :cond_3

    .line 937
    if-eqz v0, :cond_2

    .line 938
    const-string/jumbo v6, "dtend"

    invoke-virtual {p2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEnd()J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/vlingo/core/internal/schedule/DateUtil;->getUTCTimeMillis(J)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 948
    :cond_0
    :goto_1
    sget-object v6, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getID()J

    move-result-wide v7

    invoke-static {v6, v7, v8}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    .line 949
    .local v4, "updateUri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual {v6, v4, v5, v10, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 950
    .local v3, "rowsUpdated":I
    if-eq v3, v9, :cond_4

    .line 951
    new-instance v6, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;

    const-string/jumbo v7, "Error in updating event."

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 929
    .end local v3    # "rowsUpdated":I
    .end local v4    # "updateUri":Landroid/net/Uri;
    :cond_1
    const-string/jumbo v6, "dtstart"

    invoke-virtual {p2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBegin()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 930
    const-string/jumbo v6, "allDay"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 941
    :cond_2
    const-string/jumbo v6, "dtend"

    invoke-virtual {p2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEnd()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1

    .line 942
    :cond_3
    invoke-virtual {p2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getDuration()J

    move-result-wide v6

    cmp-long v6, v6, v11

    if-eqz v6, :cond_0

    .line 943
    const-string/jumbo v6, "duration"

    invoke-virtual {p2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getDuration()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1

    .line 955
    .restart local v3    # "rowsUpdated":I
    .restart local v4    # "updateUri":Landroid/net/Uri;
    :cond_4
    invoke-static {v4}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getRowIDFromURI(Landroid/net/Uri;)I

    move-result v1

    .line 956
    .local v1, "eventID":I
    sget v6, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->MAX_ATTENDEES:I

    invoke-static {p0, p1, v6}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getEventAttendeesCD(Landroid/content/Context;Lcom/vlingo/core/internal/schedule/ScheduleEvent;I)Ljava/util/List;

    move-result-object v6

    invoke-virtual {p2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getContactDataList()Ljava/util/List;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->mergeContactList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 960
    .local v2, "mergedContactList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    if-eqz v2, :cond_5

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_5

    .line 961
    invoke-static {p0, v2, v1}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->addAttendeesToEvent(Landroid/content/Context;Ljava/util/List;I)V

    .line 963
    :cond_5
    return-void
.end method

.method public static updateReminder(Landroid/content/Context;Landroid/net/Uri;Lcom/vlingo/core/internal/schedule/ScheduleTask;)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "updateUri"    # Landroid/net/Uri;
    .param p2, "changedTask"    # Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .prologue
    .line 996
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 997
    .local v6, "contentResolver":Landroid/content/ContentResolver;
    const/4 v7, 0x0

    .line 999
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->TASKS_REMINDERS_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1001
    const/4 v0, -0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1002
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 1003
    .local v8, "mId":I
    const/4 v9, 0x0

    .line 1005
    .local v9, "reminderId":I
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1006
    int-to-long v0, v8

    const/4 v2, 0x1

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1007
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 1012
    :cond_1
    invoke-virtual {p2}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getReminderTime()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 1013
    invoke-static {p1, p2}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getReminderContentValues(Landroid/net/Uri;Lcom/vlingo/core/internal/schedule/ScheduleTask;)Landroid/content/ContentValues;

    move-result-object v10

    .line 1014
    .local v10, "reminderValues":Landroid/content/ContentValues;
    sget-object v0, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->TASKS_REMINDERS_CONTENT_URI:Landroid/net/Uri;

    int-to-long v1, v9

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v11

    .line 1015
    .local v11, "uri":Landroid/net/Uri;
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {v6, v11, v10, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1018
    .end local v10    # "reminderValues":Landroid/content/ContentValues;
    .end local v11    # "uri":Landroid/net/Uri;
    :cond_2
    if-eqz v7, :cond_3

    .line 1019
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1022
    :cond_3
    return-void

    .line 1018
    .end local v8    # "mId":I
    .end local v9    # "reminderId":I
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_4

    .line 1019
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public static updateTask(Landroid/content/Context;Lcom/vlingo/core/internal/schedule/ScheduleTask;Lcom/vlingo/core/internal/schedule/ScheduleTask;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "originalTask"    # Lcom/vlingo/core/internal/schedule/ScheduleTask;
    .param p2, "changedTask"    # Lcom/vlingo/core/internal/schedule/ScheduleTask;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 983
    invoke-static {p2}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getContentValues(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Landroid/content/ContentValues;

    move-result-object v3

    .line 984
    .local v3, "values":Landroid/content/ContentValues;
    const-string/jumbo v4, "content://com.android.calendar/syncTasks"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 985
    .local v1, "taskUri":Landroid/net/Uri;
    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getID()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 986
    .local v2, "updateUri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, v2, v3, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 988
    .local v0, "rowsUpdated":I
    invoke-static {p0, v2, p2}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->updateReminder(Landroid/content/Context;Landroid/net/Uri;Lcom/vlingo/core/internal/schedule/ScheduleTask;)V

    .line 989
    const/4 v4, 0x1

    if-eq v0, v4, :cond_0

    .line 990
    new-instance v4, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;

    const-string/jumbo v5, "Error in updating task."

    invoke-direct {v4, v5}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 992
    :cond_0
    return-void
.end method
