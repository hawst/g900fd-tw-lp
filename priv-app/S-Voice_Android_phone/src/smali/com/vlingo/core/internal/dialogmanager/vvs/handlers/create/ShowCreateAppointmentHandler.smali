.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "ShowCreateAppointmentHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# instance fields
.field private final DESCRIPTION:Ljava/lang/String;

.field private final TITLE:Ljava/lang/String;

.field private event:Lcom/vlingo/core/internal/schedule/ScheduleEvent;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 45
    const-string/jumbo v0, "description"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->DESCRIPTION:Ljava/lang/String;

    .line 46
    const-string/jumbo v0, "title"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->TITLE:Ljava/lang/String;

    return-void
.end method

.method private createAppointment()V
    .locals 2

    .prologue
    .line 97
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SCHEDULE_APPOINTMENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/ScheduleAppointmentAction;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/actions/ScheduleAppointmentAction;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->event:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/ScheduleAppointmentAction;->event(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Lcom/vlingo/core/internal/dialogmanager/actions/ScheduleAppointmentAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/actions/ScheduleAppointmentAction;->queue()V

    .line 100
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->sendAcceptedText()V

    .line 101
    return-void
.end method

.method private getScheduleEvent(Lcom/vlingo/sdk/recognition/VLAction;)Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .locals 3
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 93
    .local v0, "attendees":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-static {p1, v0}, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->extractScheduleEvent(Lcom/vlingo/sdk/recognition/VLAction;Ljava/util/List;)Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    move-result-object v1

    return-object v1
.end method

.method private sendAcceptedText()V
    .locals 9

    .prologue
    .line 104
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->event:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    if-eqz v0, :cond_1

    .line 105
    const/4 v7, 0x0

    .line 106
    .local v7, "attendees":Ljava/lang/String;
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->event:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getAttendeeNames()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->event:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getAttendeeNames()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    .line 109
    :cond_0
    new-instance v0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->event:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->event:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBeginDate()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->event:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBeginTime()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->event:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEndTime()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->event:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getDuration()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->event:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getLocation()Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/vlingo/core/internal/recognition/acceptedtext/AddEventAcceptedText;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V

    .line 112
    .end local v7    # "attendees":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private sendInvitations()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 115
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->event:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getContactDataList()Ljava/util/List;

    move-result-object v1

    .line 116
    .local v1, "attendees":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    if-eqz v1, :cond_1

    .line 117
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 119
    .local v2, "email":Lcom/vlingo/core/internal/contacts/ContactData;
    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SEND_EMAIL:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v7, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;

    invoke-virtual {p0, v5, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;

    invoke-interface {v5, v2}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;->contact(Lcom/vlingo/core/internal/contacts/ContactData;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;

    move-result-object v5

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->event:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v7}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;->subject(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;

    move-result-object v5

    const-string/jumbo v7, "content"

    invoke-interface {v5, v7}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;->message(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;

    move-result-object v0

    .local v0, "action":Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;
    move-object v5, v0

    .line 124
    check-cast v5, Lcom/vlingo/core/internal/dialogmanager/DMAction;

    new-instance v7, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler$1;

    invoke-direct {v7, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler$1;-><init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;)V

    invoke-virtual {v5, v7}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->listener(Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)Lcom/vlingo/core/internal/dialogmanager/DMAction;

    .line 130
    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;->queue()V

    .line 131
    if-nez v2, :cond_0

    move-object v3, v6

    .line 132
    .local v3, "emailStr":Ljava/lang/String;
    :goto_1
    new-instance v5, Lcom/vlingo/core/internal/recognition/acceptedtext/EmailAcceptedText;

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->event:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v3, v7, v6}, Lcom/vlingo/core/internal/recognition/acceptedtext/EmailAcceptedText;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V

    goto :goto_0

    .line 131
    .end local v3    # "emailStr":Ljava/lang/String;
    :cond_0
    iget-object v3, v2, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    goto :goto_1

    .line 135
    .end local v0    # "action":Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;
    .end local v2    # "email":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method


# virtual methods
.method public actionFail(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 150
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;

    invoke-direct {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;-><init>(Ljava/lang/String;)V

    .line 151
    .local v0, "actionFailedEvent":Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 152
    return-void
.end method

.method public actionSuccess()V
    .locals 3

    .prologue
    .line 143
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;-><init>()V

    .line 144
    .local v0, "actionCompletedEvent":Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 145
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->sendInvitations()V

    .line 146
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 6
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 63
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 65
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v3

    const-string/jumbo v4, "appointment"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 68
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->getScheduleEvent(Lcom/vlingo/sdk/recognition/VLAction;)Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->event:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 69
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->event:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->setCreatingEvent(Z)V

    .line 70
    const/4 v1, 0x0

    .line 72
    .local v1, "decos":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    const-string/jumbo v3, "confirm"

    invoke-static {p1, v3, v5, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v0

    .line 73
    .local v0, "confirm":Z
    const-string/jumbo v3, "execute"

    invoke-static {p1, v3, v5, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v2

    .line 75
    .local v2, "execute":Z
    if-eqz v0, :cond_0

    .line 76
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeConfirmButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v1

    .line 79
    :cond_0
    if-nez v0, :cond_1

    if-eqz v2, :cond_2

    .line 80
    :cond_1
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ScheduleEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->event:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-interface {p2, v3, v1, v4, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 83
    :cond_2
    if-eqz v2, :cond_3

    .line 84
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->createAppointment()V

    .line 86
    :cond_3
    return v5
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "used"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 167
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.Default"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 168
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;

    invoke-direct {v1}, Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;-><init>()V

    .line 169
    .local v1, "actionConfirmedEvent":Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v4, v1, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 205
    .end local v1    # "actionConfirmedEvent":Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;
    .end local p2    # "used":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-void

    .line 170
    .restart local p2    # "used":Ljava/lang/Object;
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.Cancel"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 171
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;-><init>()V

    .line 172
    .local v0, "actionCancelledEvent":Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v4, v0, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    goto :goto_0

    .line 173
    .end local v0    # "actionCancelledEvent":Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.BodyChange"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 182
    const-string/jumbo v4, "title"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 183
    const-string/jumbo v4, "title"

    invoke-static {p1, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->extractParamString(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 184
    .local v3, "message":Ljava/lang/String;
    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;

    const-string/jumbo v4, "title"

    invoke-direct {v2, v4, v3}, Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    .local v2, "contentChangedEvent":Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v4, v2, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->queueEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    goto :goto_0

    .line 187
    .end local v2    # "contentChangedEvent":Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;
    .end local v3    # "message":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.View"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 190
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 191
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SCHEDULE_VIEW:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v5, Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;

    invoke-virtual {p0, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;

    check-cast p2, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .end local p2    # "used":Ljava/lang/Object;
    invoke-virtual {v4, p2}, Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;->scheduleEvent(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;->queue()V

    goto :goto_0

    .line 194
    .restart local p2    # "used":Ljava/lang/Object;
    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.Edit"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 197
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 198
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SCHEDULE_EDIT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v5, Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;

    invoke-virtual {p0, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;

    check-cast p2, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .end local p2    # "used":Ljava/lang/Object;
    invoke-virtual {v4, p2}, Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;->scheduleEvent(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;->queue()V

    goto/16 :goto_0

    .line 203
    .restart local p2    # "used":Ljava/lang/Object;
    :cond_5
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowCreateAppointmentHandler;->throwUnknownActionException(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
