.class public Lcom/vlingo/core/internal/util/CallLogUtil;
.super Ljava/lang/Object;
.source "CallLogUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static findSimilarNameFromCallLog(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 23
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    const/4 v9, 0x0

    .line 114
    .local v9, "c":Landroid/database/Cursor;
    new-instance v19, Ljava/util/Vector;

    invoke-direct/range {v19 .. v19}, Ljava/util/Vector;-><init>()V

    .line 116
    .local v19, "nameVector":Ljava/util/Vector;
    invoke-static {}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->getCommonTitles()Ljava/util/HashSet;

    move-result-object v10

    .line 117
    .local v10, "commonTitles":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {v10}, Ljava/util/HashSet;->size()I

    move-result v3

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v22, v0

    .line 120
    .local v22, "titlesArray":[Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/vlingo/core/internal/util/StringUtils;->isKorean(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 121
    const/4 v3, 0x0

    .line 174
    :cond_0
    :goto_0
    return-object v3

    .line 124
    :cond_1
    const/4 v3, 0x0

    aget-object v3, v22, v3

    if-nez v3, :cond_2

    .line 125
    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 128
    :cond_2
    const-string/jumbo v3, "korean_name_similarity_value_min"

    const v4, 0x3f4ccccd    # 0.8f

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v16

    .line 129
    .local v16, "minSimilarity":F
    const-wide/16 v14, 0x0

    .line 130
    .local v14, "maxScore":D
    const-string/jumbo v13, ""

    .line 134
    .local v13, "lastName":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Landroid/provider/ContactsContract$Contacts;->CONTENT_STREQUENT_URI:Landroid/net/Uri;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string/jumbo v7, "display_name"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "_id"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "starred"

    aput-object v7, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 139
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 140
    const/4 v12, 0x0

    .line 141
    .local v12, "index":I
    const-string/jumbo v3, "display_name"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 144
    .local v18, "nameIdx":I
    :cond_3
    move/from16 v0, v18

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 146
    .local v17, "name":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/vlingo/core/internal/util/StringUtils;->isKorean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 147
    const-wide/16 v20, 0x0

    .line 149
    .local v20, "tempScore":D
    move-object/from16 v13, v17

    .line 150
    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/CallLogUtil;->preprocessName(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 152
    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->getDistanceBetweenNames(Ljava/lang/String;Ljava/lang/String;)D

    move-result-wide v20

    .line 153
    move/from16 v0, v16

    float-to-double v3, v0

    cmpl-double v3, v20, v3

    if-ltz v3, :cond_4

    .line 154
    new-instance v3, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;

    move-object/from16 v0, v17

    move-wide/from16 v1, v20

    invoke-direct {v3, v0, v1, v2}, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;-><init>(Ljava/lang/String;D)V

    move-object/from16 v0, v19

    invoke-static {v0, v3}, Lcom/vlingo/core/internal/util/CallLogUtil;->insertNameByScore(Ljava/util/Vector;Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;)I

    .line 157
    .end local v20    # "tempScore":D
    :cond_4
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_3

    .line 169
    .end local v12    # "index":I
    .end local v17    # "name":Ljava/lang/String;
    .end local v18    # "nameIdx":I
    :cond_5
    if-eqz v9, :cond_6

    .line 170
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 174
    :cond_6
    invoke-static/range {v19 .. v19}, Lcom/vlingo/core/internal/util/CallLogUtil;->getSortedNameList(Ljava/util/Vector;)Ljava/util/List;

    move-result-object v3

    goto/16 :goto_0

    .line 159
    :catch_0
    move-exception v11

    .line 164
    .local v11, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    .line 165
    invoke-static/range {v19 .. v19}, Lcom/vlingo/core/internal/util/CallLogUtil;->getSortedNameList(Ljava/util/Vector;)Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 169
    if-eqz v9, :cond_0

    .line 170
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 169
    .end local v11    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v9, :cond_7

    .line 170
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v3
.end method

.method public static findSimilarNameFromCallLog(Landroid/content/Context;Ljava/util/List;)Ljava/util/Map;
    .locals 26
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 178
    .local p1, "query":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v9, 0x0

    .line 179
    .local v9, "c":Landroid/database/Cursor;
    new-instance v21, Ljava/util/HashMap;

    invoke-direct/range {v21 .. v21}, Ljava/util/HashMap;-><init>()V

    .line 181
    .local v21, "nameVector":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Vector<Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;>;>;"
    const/4 v12, 0x0

    .line 184
    .local v12, "hasKorean":Z
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/String;

    .line 185
    .local v22, "s":Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Lcom/vlingo/core/internal/util/StringUtils;->isKorean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 186
    const/4 v12, 0x1

    .line 188
    :cond_0
    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 190
    .end local v22    # "s":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/contacts/rules/KoreanContactRuleSet;->getCommonTitles()Ljava/util/HashSet;

    move-result-object v10

    .line 191
    .local v10, "commonTitles":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {v10}, Ljava/util/HashSet;->size()I

    move-result v3

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v25, v0

    .line 194
    .local v25, "titlesArray":[Ljava/lang/String;
    if-nez v12, :cond_3

    .line 195
    const/4 v3, 0x0

    .line 249
    :cond_2
    :goto_1
    return-object v3

    .line 198
    :cond_3
    const/4 v3, 0x0

    aget-object v3, v25, v3

    if-nez v3, :cond_4

    .line 199
    move-object/from16 v0, v25

    invoke-virtual {v10, v0}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 202
    :cond_4
    const-string/jumbo v3, "korean_name_similarity_value_min"

    const v4, 0x3f4ccccd    # 0.8f

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v18

    .line 203
    .local v18, "minSimilarity":F
    const-wide/16 v16, 0x0

    .line 204
    .local v16, "maxScore":D
    const-string/jumbo v15, ""

    .line 208
    .local v15, "lastName":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Landroid/provider/ContactsContract$Contacts;->CONTENT_STREQUENT_URI:Landroid/net/Uri;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string/jumbo v7, "display_name"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "_id"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "starred"

    aput-object v7, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 213
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 214
    const/4 v14, 0x0

    .line 215
    .local v14, "index":I
    const-string/jumbo v3, "display_name"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 218
    .local v20, "nameIdx":I
    :cond_5
    move/from16 v0, v20

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 220
    .local v19, "name":Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Lcom/vlingo/core/internal/util/StringUtils;->isKorean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 221
    const-wide/16 v23, 0x0

    .line 223
    .local v23, "tempScore":D
    move-object/from16 v15, v19

    .line 224
    move-object/from16 v0, v19

    move-object/from16 v1, v25

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/CallLogUtil;->preprocessName(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 225
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_6
    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/String;

    .line 226
    .restart local v22    # "s":Ljava/lang/String;
    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->getDistanceBetweenNames(Ljava/lang/String;Ljava/lang/String;)D

    move-result-wide v23

    .line 227
    move/from16 v0, v18

    float-to-double v3, v0

    cmpl-double v3, v23, v3

    if-ltz v3, :cond_6

    .line 228
    invoke-interface/range {v21 .. v22}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Vector;

    new-instance v4, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;

    move-object/from16 v0, v19

    move-wide/from16 v1, v23

    invoke-direct {v4, v0, v1, v2}, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;-><init>(Ljava/lang/String;D)V

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/util/CallLogUtil;->insertNameByScore(Ljava/util/Vector;Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 234
    .end local v14    # "index":I
    .end local v19    # "name":Ljava/lang/String;
    .end local v20    # "nameIdx":I
    .end local v22    # "s":Ljava/lang/String;
    .end local v23    # "tempScore":D
    :catch_0
    move-exception v11

    .line 239
    .local v11, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    .line 240
    invoke-static/range {v21 .. v21}, Lcom/vlingo/core/internal/util/CallLogUtil;->getSortedNameMap(Ljava/util/Map;)Ljava/util/Map;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 244
    if-eqz v9, :cond_2

    .line 245
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 232
    .end local v11    # "e":Ljava/lang/Exception;
    .restart local v14    # "index":I
    .restart local v19    # "name":Ljava/lang/String;
    .restart local v20    # "nameIdx":I
    :cond_7
    :try_start_2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    if-nez v3, :cond_5

    .line 244
    .end local v14    # "index":I
    .end local v19    # "name":Ljava/lang/String;
    .end local v20    # "nameIdx":I
    :cond_8
    if-eqz v9, :cond_9

    .line 245
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 249
    :cond_9
    invoke-static/range {v21 .. v21}, Lcom/vlingo/core/internal/util/CallLogUtil;->getSortedNameMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v3

    goto/16 :goto_1

    .line 244
    :catchall_0
    move-exception v3

    if-eqz v9, :cond_a

    .line 245
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v3
.end method

.method public static getLastNMissedCalls(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/util/LoggedCall;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    const/4 v7, 0x0

    .line 56
    .local v7, "c":Landroid/database/Cursor;
    new-instance v14, Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-direct {v14, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 57
    .local v14, "missedCalls":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/util/LoggedCall;>;"
    const/4 v1, 0x4

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "name"

    aput-object v2, v3, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "number"

    aput-object v2, v3, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "date"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "new"

    aput-object v2, v3, v1

    .line 63
    .local v3, "projection":[Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v4, "type = ? AND new = ?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const/16 v18, 0x3

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v5, v6

    const/4 v6, 0x1

    const-string/jumbo v18, "1"

    aput-object v18, v5, v6

    const-string/jumbo v6, "date DESC "

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 70
    invoke-static {v7}, Lcom/vlingo/core/internal/util/CursorUtil;->isValid(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 71
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 72
    const/4 v13, 0x0

    .line 73
    .local v13, "index":I
    const-string/jumbo v1, "name"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 74
    .local v16, "nameIdx":I
    const-string/jumbo v1, "date"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 75
    .local v9, "dateIdx":I
    const-string/jumbo v1, "number"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 78
    .local v17, "numberIdx":I
    :cond_0
    move/from16 v0, v16

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 79
    .local v15, "name":Ljava/lang/String;
    invoke-interface {v7, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 81
    .local v10, "dateMS":J
    invoke-static {v15}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 82
    move/from16 v0, v17

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 86
    :cond_1
    invoke-static {v15, v10, v11}, Lcom/vlingo/core/internal/util/LoggedCall;->newInstance(Ljava/lang/String;J)Lcom/vlingo/core/internal/util/LoggedCall;

    move-result-object v8

    .line 87
    .local v8, "call":Lcom/vlingo/core/internal/util/LoggedCall;
    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_2

    move/from16 v0, p1

    if-lt v13, v0, :cond_0

    .line 97
    .end local v8    # "call":Lcom/vlingo/core/internal/util/LoggedCall;
    .end local v9    # "dateIdx":I
    .end local v10    # "dateMS":J
    .end local v13    # "index":I
    .end local v15    # "name":Ljava/lang/String;
    .end local v16    # "nameIdx":I
    .end local v17    # "numberIdx":I
    :cond_2
    if-eqz v7, :cond_3

    .line 98
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 101
    :cond_3
    :goto_0
    return-object v14

    .line 91
    :catch_0
    move-exception v12

    .line 94
    .local v12, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 97
    if-eqz v7, :cond_3

    .line 98
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 97
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_4

    .line 98
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v1
.end method

.method private static getSortedNameList(Ljava/util/Vector;)Ljava/util/List;
    .locals 5
    .param p0, "nameVector"    # Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 301
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 302
    .local v1, "nameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v2

    .line 304
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 305
    invoke-virtual {p0, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;

    .line 306
    .local v3, "tempData":Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 304
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 309
    .end local v3    # "tempData":Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
    :cond_0
    return-object v1
.end method

.method private static getSortedNameMap(Ljava/util/Map;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;",
            ">;>;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 313
    .local p0, "nameMapVector":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Vector<Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;>;>;"
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 314
    .local v3, "nameMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 315
    .local v5, "s":Ljava/lang/String;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 316
    .local v2, "nameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Vector;

    .line 317
    .local v4, "nameVector":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;>;"
    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v6

    .line 318
    .local v6, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v6, :cond_0

    .line 319
    invoke-virtual {v4, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;

    .line 320
    .local v7, "tempData":Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 318
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 322
    .end local v7    # "tempData":Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
    :cond_0
    invoke-interface {v3, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 325
    .end local v0    # "i":I
    .end local v2    # "nameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "nameVector":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;>;"
    .end local v5    # "s":Ljava/lang/String;
    .end local v6    # "size":I
    :cond_1
    return-object v3
.end method

.method private static insertNameByScore(Ljava/util/Vector;Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;)I
    .locals 8
    .param p1, "data"    # Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;",
            ">;",
            "Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;",
            ")I"
        }
    .end annotation

    .prologue
    .line 280
    .local p0, "nameVector":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;>;"
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v2

    .line 281
    .local v2, "size":I
    if-lez v2, :cond_2

    .line 282
    invoke-virtual {p0}, Ljava/util/Vector;->lastElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;

    .line 283
    .local v1, "lastData":Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;->getScore()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;->getScore()D

    move-result-wide v6

    cmpl-double v4, v4, v6

    if-ltz v4, :cond_0

    .line 284
    invoke-virtual {p0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 285
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .line 297
    .end local v1    # "lastData":Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
    :goto_0
    return v0

    .line 287
    .restart local v1    # "lastData":Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_3

    .line 288
    invoke-virtual {p0, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;

    .line 289
    .local v3, "tempData":Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;->getScore()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;->getScore()D

    move-result-wide v6

    cmpg-double v4, v4, v6

    if-gez v4, :cond_1

    .line 290
    invoke-virtual {p0, p1, v0}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    goto :goto_0

    .line 287
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 295
    .end local v0    # "i":I
    .end local v1    # "lastData":Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
    .end local v3    # "tempData":Lcom/vlingo/core/internal/util/CallLogUtil$CallLogData;
    :cond_2
    invoke-virtual {p0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 297
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static preprocessName(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "titlesArray"    # [Ljava/lang/String;

    .prologue
    .line 258
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p1

    if-ge v1, v4, :cond_0

    .line 259
    aget-object v4, p1, v1

    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 260
    .local v0, "getIdx":I
    if-lez v0, :cond_2

    .line 261
    const/4 v4, 0x0

    invoke-virtual {p0, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 267
    .end local v0    # "getIdx":I
    :cond_0
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v2, v4, :cond_3

    .line 268
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticEncoder;->isHangul(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 269
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v3

    .line 270
    .local v3, "specialChars":Ljava/lang/String;
    const-string/jumbo v4, " "

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 267
    .end local v3    # "specialChars":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 258
    .end local v2    # "j":I
    .restart local v0    # "getIdx":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 274
    .end local v0    # "getIdx":I
    .restart local v2    # "j":I
    :cond_3
    const-string/jumbo v4, "\\s+"

    const-string/jumbo v5, ""

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 276
    return-object p0
.end method
