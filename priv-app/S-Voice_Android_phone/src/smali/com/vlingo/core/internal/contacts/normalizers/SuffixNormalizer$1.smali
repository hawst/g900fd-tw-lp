.class final Lcom/vlingo/core/internal/contacts/normalizers/SuffixNormalizer$1;
.super Ljava/util/LinkedHashMap;
.source "SuffixNormalizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/contacts/normalizers/SuffixNormalizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap",
        "<",
        "Ljava/util/regex/Pattern;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 16
    const-string/jumbo v0, "e"

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/contacts/normalizers/SuffixNormalizer$1;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/normalizers/SuffixNormalizer$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    const-string/jumbo v0, "s"

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/contacts/normalizers/SuffixNormalizer$1;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/normalizers/SuffixNormalizer$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    return-void
.end method

.method private createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;
    .locals 2
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 21
    # getter for: Lcom/vlingo/core/internal/contacts/normalizers/SuffixNormalizer;->canNormalizeStr:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/contacts/normalizers/SuffixNormalizer;->access$000()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    # setter for: Lcom/vlingo/core/internal/contacts/normalizers/SuffixNormalizer;->canNormalizeStr:Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/contacts/normalizers/SuffixNormalizer;->access$002(Ljava/lang/String;)Ljava/lang/String;

    .line 26
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "(\\b[\\w]+)("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")(\\b\\s?)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    return-object v0

    .line 24
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/vlingo/core/internal/contacts/normalizers/SuffixNormalizer;->canNormalizeStr:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/contacts/normalizers/SuffixNormalizer;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "((\\b[\\w]+)("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")(\\b\\s?))"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    # setter for: Lcom/vlingo/core/internal/contacts/normalizers/SuffixNormalizer;->canNormalizeStr:Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/contacts/normalizers/SuffixNormalizer;->access$002(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method
