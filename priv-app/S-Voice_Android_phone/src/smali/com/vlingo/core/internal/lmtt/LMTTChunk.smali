.class Lcom/vlingo/core/internal/lmtt/LMTTChunk;
.super Ljava/lang/Object;
.source "LMTTChunk.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/lmtt/LMTTChunk$4;
    }
.end annotation


# static fields
.field private static final DEFAULT_CHUNK_DELAY_MS:I = 0x3a98

.field private static final DEFAULT_CHUNK_RETRY_DELAY:I = 0x1d4c0

.field private static final DEFAULT_MAX_RETRIES:I = 0x5


# instance fields
.field private mChunkItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/lmtt/LMTTItem;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentState:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

.field private mIsFirst:Z

.field private mIsWhole:Z

.field private mRetryCount:I


# direct methods
.method constructor <init>(Ljava/util/List;ZZ)V
    .locals 3
    .param p2, "isWhole"    # Z
    .param p3, "isFirst"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/lmtt/LMTTItem;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .local p1, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/lmtt/LMTTItem;>;"
    const/4 v2, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-boolean v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mIsFirst:Z

    .line 39
    iput-object p1, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mChunkItems:Ljava/util/List;

    .line 40
    iput-boolean p2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mIsWhole:Z

    .line 41
    iput-boolean p3, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mIsFirst:Z

    .line 42
    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->START_STATE:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    iput-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mCurrentState:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    .line 43
    iput v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mRetryCount:I

    .line 45
    if-eqz p3, :cond_0

    .line 46
    const-string/jumbo v1, "lmtt.last_chunk_retrycount"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 47
    .local v0, "lastRetryCount":I
    if-lez v0, :cond_0

    .line 48
    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->STATE_RETRY:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    iput-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mCurrentState:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    .line 49
    iput v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mRetryCount:I

    .line 52
    .end local v0    # "lastRetryCount":I
    :cond_0
    return-void
.end method

.method constructor <init>(ZZ)V
    .locals 1
    .param p1, "isWhole"    # Z
    .param p2, "isFirst"    # Z

    .prologue
    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, v0, p1, p2}, Lcom/vlingo/core/internal/lmtt/LMTTChunk;-><init>(Ljava/util/List;ZZ)V

    .line 56
    return-void
.end method

.method private getTrainerList()Lcom/vlingo/sdk/training/VLTrainerUpdateList;
    .locals 18

    .prologue
    .line 166
    invoke-static {}, Lcom/vlingo/sdk/training/VLTrainerUpdateList;->createList()Lcom/vlingo/sdk/training/VLTrainerUpdateList;

    move-result-object v1

    .line 167
    .local v1, "list":Lcom/vlingo/sdk/training/VLTrainerUpdateList;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mChunkItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vlingo/core/internal/lmtt/LMTTItem;

    .line 168
    .local v15, "item":Lcom/vlingo/core/internal/lmtt/LMTTItem;
    sget-object v2, Lcom/vlingo/core/internal/lmtt/LMTTChunk$4;->$SwitchMap$com$vlingo$core$internal$lmtt$LMTTItem$ChangeType:[I

    iget-object v3, v15, Lcom/vlingo/core/internal/lmtt/LMTTItem;->changeType:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 170
    :pswitch_0
    sget-object v2, Lcom/vlingo/core/internal/lmtt/LMTTChunk$4;->$SwitchMap$com$vlingo$core$internal$lmtt$LMTTItem$LmttItemType:[I

    iget-object v3, v15, Lcom/vlingo/core/internal/lmtt/LMTTItem;->type:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 171
    :pswitch_1
    iget-wide v2, v15, Lcom/vlingo/core/internal/lmtt/LMTTItem;->uid:J

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/sdk/training/VLTrainerUpdateList;->removeContact(J)V

    goto :goto_0

    .line 172
    :pswitch_2
    iget-wide v2, v15, Lcom/vlingo/core/internal/lmtt/LMTTItem;->uid:J

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/sdk/training/VLTrainerUpdateList;->removeSong(J)V

    goto :goto_0

    .line 173
    :pswitch_3
    iget-wide v2, v15, Lcom/vlingo/core/internal/lmtt/LMTTItem;->uid:J

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/sdk/training/VLTrainerUpdateList;->removePlayList(J)V

    goto :goto_0

    .line 179
    :pswitch_4
    sget-object v2, Lcom/vlingo/core/internal/lmtt/LMTTChunk$4;->$SwitchMap$com$vlingo$core$internal$lmtt$LMTTItem$LmttItemType:[I

    iget-object v3, v15, Lcom/vlingo/core/internal/lmtt/LMTTItem;->type:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_2

    goto :goto_0

    :pswitch_5
    move-object v13, v15

    .line 181
    check-cast v13, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;

    .line 182
    .local v13, "contact":Lcom/vlingo/core/internal/lmtt/LMTTContactItem;
    iget-wide v2, v15, Lcom/vlingo/core/internal/lmtt/LMTTItem;->uid:J

    iget-object v4, v13, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->firstName:Ljava/lang/String;

    iget-object v5, v13, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->middleName:Ljava/lang/String;

    iget-object v6, v13, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->lastName:Ljava/lang/String;

    iget-object v7, v13, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->nickname:Ljava/lang/String;

    iget-object v8, v13, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->fullName:Ljava/lang/String;

    iget-object v9, v13, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->companyName:Ljava/lang/String;

    iget-object v10, v13, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->phoneticFirstName:Ljava/lang/String;

    iget-object v11, v13, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->phoneticMiddleName:Ljava/lang/String;

    iget-object v12, v13, Lcom/vlingo/core/internal/lmtt/LMTTContactItem;->phoneticLastName:Ljava/lang/String;

    invoke-virtual/range {v1 .. v12}, Lcom/vlingo/sdk/training/VLTrainerUpdateList;->addContact(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .end local v13    # "contact":Lcom/vlingo/core/internal/lmtt/LMTTContactItem;
    :pswitch_6
    move-object/from16 v17, v15

    .line 185
    check-cast v17, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;

    .line 186
    .local v17, "song":Lcom/vlingo/core/internal/lmtt/LMTTSongItem;
    iget-wide v2, v15, Lcom/vlingo/core/internal/lmtt/LMTTItem;->uid:J

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->title:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->artist:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-object v6, v0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->album:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-object v7, v0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->composer:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-object v8, v0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->genre:Ljava/lang/String;

    move-object/from16 v0, v17

    iget v9, v0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->year:I

    move-object/from16 v0, v17

    iget-object v10, v0, Lcom/vlingo/core/internal/lmtt/LMTTSongItem;->folder:Ljava/lang/String;

    invoke-virtual/range {v1 .. v10}, Lcom/vlingo/sdk/training/VLTrainerUpdateList;->addSong(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_0

    .end local v17    # "song":Lcom/vlingo/core/internal/lmtt/LMTTSongItem;
    :pswitch_7
    move-object/from16 v16, v15

    .line 189
    check-cast v16, Lcom/vlingo/core/internal/lmtt/LMTTPlaylistItem;

    .line 190
    .local v16, "playlist":Lcom/vlingo/core/internal/lmtt/LMTTPlaylistItem;
    iget-wide v2, v15, Lcom/vlingo/core/internal/lmtt/LMTTItem;->uid:J

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/vlingo/core/internal/lmtt/LMTTPlaylistItem;->title:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/vlingo/sdk/training/VLTrainerUpdateList;->addPlaylist(JLjava/lang/String;)V

    goto/16 :goto_0

    .line 198
    .end local v15    # "item":Lcom/vlingo/core/internal/lmtt/LMTTItem;
    .end local v16    # "playlist":Lcom/vlingo/core/internal/lmtt/LMTTPlaylistItem;
    :cond_0
    return-object v1

    .line 168
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
        :pswitch_4
    .end packed-switch

    .line 170
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 179
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public getDelay()J
    .locals 4

    .prologue
    .line 149
    const/4 v0, 0x0

    .line 150
    .local v0, "delay":I
    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mCurrentState:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    sget-object v3, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->START_STATE:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    if-ne v2, v3, :cond_1

    .line 153
    iget-boolean v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mIsFirst:Z

    if-nez v2, :cond_0

    .line 154
    const-string/jumbo v2, "lmtt.chunk_delay_ms"

    const/16 v3, 0x3a98

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 162
    :cond_0
    int-to-long v2, v0

    return-wide v2

    .line 156
    :cond_1
    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mCurrentState:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    sget-object v3, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->STATE_RETRY:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    if-ne v2, v3, :cond_0

    .line 157
    const-string/jumbo v2, "lmtt.chunk_retry_delay_ms"

    const v3, 0x1d4c0

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 158
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mRetryCount:I

    if-ge v1, v2, :cond_0

    .line 159
    mul-int/lit8 v0, v0, 0x2

    .line 158
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getIsWhole()Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mIsWhole:Z

    return v0
.end method

.method public getItemList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/lmtt/LMTTItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mChunkItems:Ljava/util/List;

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mChunkItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 72
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 73
    const-string/jumbo v1, " [State="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mCurrentState:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 74
    const-string/jumbo v1, ", RetryCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mRetryCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 75
    const-string/jumbo v1, ", Whole="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mIsWhole:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 76
    const-string/jumbo v1, ", First="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mIsFirst:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 77
    const-string/jumbo v1, ", Size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->getSize()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 78
    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public transmit(Lcom/vlingo/sdk/training/VLTrainerListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/vlingo/sdk/training/VLTrainerListener;

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->getSize()I

    move-result v1

    if-nez v1, :cond_0

    .line 85
    new-instance v1, Lcom/vlingo/core/internal/lmtt/LMTTChunk$1;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/core/internal/lmtt/LMTTChunk$1;-><init>(Lcom/vlingo/core/internal/lmtt/LMTTChunk;Lcom/vlingo/sdk/training/VLTrainerListener;)V

    invoke-static {v1}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 109
    :goto_0
    return-void

    .line 92
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->getTrainerList()Lcom/vlingo/sdk/training/VLTrainerUpdateList;

    move-result-object v0

    .line 94
    .local v0, "updateList":Lcom/vlingo/sdk/training/VLTrainerUpdateList;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->getIsWhole()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 95
    new-instance v1, Lcom/vlingo/core/internal/lmtt/LMTTChunk$2;

    invoke-direct {v1, p0, v0, p1}, Lcom/vlingo/core/internal/lmtt/LMTTChunk$2;-><init>(Lcom/vlingo/core/internal/lmtt/LMTTChunk;Lcom/vlingo/sdk/training/VLTrainerUpdateList;Lcom/vlingo/sdk/training/VLTrainerListener;)V

    invoke-static {v1}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 102
    :cond_1
    new-instance v1, Lcom/vlingo/core/internal/lmtt/LMTTChunk$3;

    invoke-direct {v1, p0, v0, p1}, Lcom/vlingo/core/internal/lmtt/LMTTChunk$3;-><init>(Lcom/vlingo/core/internal/lmtt/LMTTChunk;Lcom/vlingo/sdk/training/VLTrainerUpdateList;Lcom/vlingo/sdk/training/VLTrainerListener;)V

    invoke-static {v1}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public updateState(Z)Lcom/vlingo/core/internal/lmtt/LMTTChunkState;
    .locals 4
    .param p1, "success"    # Z

    .prologue
    .line 112
    const/4 v1, 0x0

    .line 114
    .local v1, "newState":Lcom/vlingo/core/internal/lmtt/LMTTChunkState;
    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mCurrentState:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    sget-object v3, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->FINISHED_STATE:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mCurrentState:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    sget-object v3, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->ERRORED_OUT_STATE:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    if-ne v2, v3, :cond_1

    .line 116
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mCurrentState:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    .line 136
    :goto_0
    sget-object v2, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->STATE_RETRY:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    if-ne v1, v2, :cond_5

    .line 138
    const-string/jumbo v2, "lmtt.last_chunk_retrycount"

    iget v3, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mRetryCount:I

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 144
    :goto_1
    iput-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mCurrentState:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    .line 145
    return-object v1

    .line 117
    :cond_1
    if-eqz p1, :cond_2

    .line 119
    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->FINISHED_STATE:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    goto :goto_0

    .line 124
    :cond_2
    const-string/jumbo v2, "lmtt.chunk_retries"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 125
    .local v0, "maxRetries":I
    iget v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mRetryCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mRetryCount:I

    .line 126
    if-ltz v0, :cond_3

    iget v2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->mRetryCount:I

    if-gt v2, v0, :cond_4

    .line 127
    :cond_3
    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->STATE_RETRY:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    goto :goto_0

    .line 129
    :cond_4
    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->ERRORED_OUT_STATE:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    goto :goto_0

    .line 141
    .end local v0    # "maxRetries":I
    :cond_5
    const-string/jumbo v2, "lmtt.last_chunk_retrycount"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    goto :goto_1
.end method
