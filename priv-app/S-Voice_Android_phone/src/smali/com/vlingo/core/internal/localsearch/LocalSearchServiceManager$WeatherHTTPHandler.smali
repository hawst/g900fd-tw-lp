.class Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$WeatherHTTPHandler;
.super Ljava/lang/Object;
.source "LocalSearchServiceManager.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/http/HttpCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WeatherHTTPHandler"
.end annotation


# instance fields
.field m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

.field final synthetic this$0:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V
    .locals 1
    .param p2, "listener"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .prologue
    .line 347
    iput-object p1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$WeatherHTTPHandler;->this$0:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 345
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$WeatherHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .line 348
    iput-object p2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$WeatherHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .line 349
    return-void
.end method


# virtual methods
.method public onCancelled(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 393
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$WeatherHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    const-string/jumbo v1, "Cancelled"

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Ljava/lang/String;)V

    .line 396
    return-void
.end method

.method public onFailure(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 386
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$WeatherHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    const-string/jumbo v1, "Failure"

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Ljava/lang/String;)V

    .line 389
    return-void
.end method

.method public onResponse(Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpResponse;)V
    .locals 6
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;
    .param p2, "response"    # Lcom/vlingo/sdk/internal/http/HttpResponse;

    .prologue
    .line 353
    const/4 v1, 0x0

    .line 354
    .local v1, "result":Lcom/vlingo/core/internal/weather/WeatherElement;
    iget v3, p2, Lcom/vlingo/sdk/internal/http/HttpResponse;->responseCode:I

    const/16 v4, 0xc8

    if-ne v3, v4, :cond_1

    .line 357
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/http/HttpResponse;->getDataAsBytes()[B

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 358
    .local v0, "isXml":Ljava/io/InputStream;
    invoke-static {v0}, Lcom/vlingo/core/internal/weather/WeatherResponseParser;->parse(Ljava/io/InputStream;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v1

    .line 359
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/vlingo/core/internal/weather/WeatherElement;->getErrorMessage()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v2, 0x1

    .line 362
    .local v2, "success":Z
    :goto_0
    iget-object v3, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$WeatherHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    invoke-interface {v3, v2, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestComplete(ZLjava/lang/Object;)V

    .line 367
    .end local v0    # "isXml":Ljava/io/InputStream;
    .end local v2    # "success":Z
    :goto_1
    return-void

    .line 359
    .restart local v0    # "isXml":Ljava/io/InputStream;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 364
    .end local v0    # "isXml":Ljava/io/InputStream;
    :cond_1
    iget-object v3, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$WeatherHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "responseCode="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p2, Lcom/vlingo/sdk/internal/http/HttpResponse;->responseCode:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onTimeout(Lcom/vlingo/sdk/internal/http/HttpRequest;)Z
    .locals 2
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 378
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$WeatherHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    const-string/jumbo v1, "Timeout"

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Ljava/lang/String;)V

    .line 381
    const/4 v0, 0x0

    return v0
.end method

.method public onWillExecuteRequest(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 373
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$WeatherHTTPHandler;->m_listener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestScheduled()V

    .line 374
    return-void
.end method
