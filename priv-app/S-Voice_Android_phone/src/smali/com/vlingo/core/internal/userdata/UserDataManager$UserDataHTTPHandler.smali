.class Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;
.super Ljava/lang/Object;
.source "UserDataManager.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/http/HttpCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/userdata/UserDataManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UserDataHTTPHandler"
.end annotation


# instance fields
.field private final callback:Lcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;

.field private final deleteLocalData:Z


# direct methods
.method constructor <init>(ZLcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;)V
    .locals 0
    .param p1, "deleteLocalData"    # Z
    .param p2, "callback"    # Lcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    iput-boolean p1, p0, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;->deleteLocalData:Z

    .line 136
    iput-object p2, p0, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;->callback:Lcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;

    .line 137
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;)Lcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;

    .prologue
    .line 129
    iget-object v0, p0, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;->callback:Lcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;->deleteLocalData:Z

    return v0
.end method

.method private onFailure(Ljava/lang/String;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 226
    iget-object v0, p0, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;->callback:Lcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;

    if-eqz v0, :cond_0

    .line 227
    new-instance v0, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler$2;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler$2;-><init>(Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 234
    :cond_0
    return-void
.end method


# virtual methods
.method public onCancelled(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 223
    return-void
.end method

.method public onFailure(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 216
    const-string/jumbo v0, ""

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;->onFailure(Ljava/lang/String;)V

    .line 217
    return-void
.end method

.method public onResponse(Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpResponse;)V
    .locals 5
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;
    .param p2, "response"    # Lcom/vlingo/sdk/internal/http/HttpResponse;

    .prologue
    .line 140
    iget v3, p2, Lcom/vlingo/sdk/internal/http/HttpResponse;->responseCode:I

    const/16 v4, 0xc8

    if-ne v3, v4, :cond_1

    .line 144
    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/http/HttpResponse;->getDataAsBytes()[B

    move-result-object v0

    .line 145
    .local v0, "body":[B
    const/4 v2, 0x0

    .line 146
    .local v2, "tmpErrMsg":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 147
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    const-class v4, Lcom/vlingo/core/internal/userdata/UserDataManager;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/util/ServerErrorUtil;->getErrorMessageFromXML(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 149
    :cond_0
    move-object v1, v2

    .line 151
    .local v1, "errorMessage":Ljava/lang/String;
    new-instance v3, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler$1;

    invoke-direct {v3, p0, v1}, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler$1;-><init>(Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 193
    .end local v0    # "body":[B
    .end local v1    # "errorMessage":Ljava/lang/String;
    .end local v2    # "tmpErrMsg":Ljava/lang/String;
    :goto_0
    return-void

    .line 191
    :cond_1
    const-string/jumbo v3, ""

    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;->onFailure(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onTimeout(Lcom/vlingo/sdk/internal/http/HttpRequest;)Z
    .locals 1
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 206
    const-string/jumbo v0, ""

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;->onFailure(Ljava/lang/String;)V

    .line 208
    const/4 v0, 0x0

    return v0
.end method

.method public onWillExecuteRequest(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 199
    return-void
.end method
