.class public Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
.super Ljava/lang/Object;
.source "DoubleMetaphone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DoubleMetaphoneResult"
.end annotation


# instance fields
.field private final alternate:Ljava/lang/StringBuilder;

.field private final maxLength:I

.field private final primary:Ljava/lang/StringBuilder;

.field final synthetic this$0:Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;I)V
    .locals 2
    .param p2, "maxLength"    # I

    .prologue
    .line 1163
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->this$0:Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1159
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->this$0:Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->getMaxCodeLen()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->primary:Ljava/lang/StringBuilder;

    .line 1160
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->this$0:Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->getMaxCodeLen()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->alternate:Ljava/lang/StringBuilder;

    .line 1164
    iput p2, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->maxLength:I

    .line 1165
    return-void
.end method


# virtual methods
.method public append(C)V
    .locals 0
    .param p1, "value"    # C

    .prologue
    .line 1168
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->appendPrimary(C)V

    .line 1169
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->appendAlternate(C)V

    .line 1170
    return-void
.end method

.method public append(CC)V
    .locals 0
    .param p1, "primaryChar"    # C
    .param p2, "alternateChar"    # C

    .prologue
    .line 1173
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->appendPrimary(C)V

    .line 1174
    invoke-virtual {p0, p2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->appendAlternate(C)V

    .line 1175
    return-void
.end method

.method public append(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1190
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->appendPrimary(Ljava/lang/String;)V

    .line 1191
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->appendAlternate(Ljava/lang/String;)V

    .line 1192
    return-void
.end method

.method public append(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "primaryChar"    # Ljava/lang/String;
    .param p2, "alternateChar"    # Ljava/lang/String;

    .prologue
    .line 1195
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->appendPrimary(Ljava/lang/String;)V

    .line 1196
    invoke-virtual {p0, p2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->appendAlternate(Ljava/lang/String;)V

    .line 1197
    return-void
.end method

.method public appendAlternate(C)V
    .locals 2
    .param p1, "value"    # C

    .prologue
    .line 1184
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->alternate:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    iget v1, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->maxLength:I

    if-ge v0, v1, :cond_0

    .line 1185
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->alternate:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1187
    :cond_0
    return-void
.end method

.method public appendAlternate(Ljava/lang/String;)V
    .locals 3
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1209
    iget v1, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->maxLength:I

    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->alternate:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    sub-int v0, v1, v2

    .line 1210
    .local v0, "addChars":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-gt v1, v0, :cond_0

    .line 1211
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->alternate:Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1215
    :goto_0
    return-void

    .line 1213
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->alternate:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public appendPrimary(C)V
    .locals 2
    .param p1, "value"    # C

    .prologue
    .line 1178
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->primary:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    iget v1, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->maxLength:I

    if-ge v0, v1, :cond_0

    .line 1179
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->primary:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1181
    :cond_0
    return-void
.end method

.method public appendPrimary(Ljava/lang/String;)V
    .locals 3
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1200
    iget v1, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->maxLength:I

    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->primary:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    sub-int v0, v1, v2

    .line 1201
    .local v0, "addChars":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-gt v1, v0, :cond_0

    .line 1202
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->primary:Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1206
    :goto_0
    return-void

    .line 1204
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->primary:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public getAlternate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1222
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->alternate:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrimary()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1218
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->primary:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isComplete()Z
    .locals 2

    .prologue
    .line 1226
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->primary:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    iget v1, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->maxLength:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->alternate:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    iget v1, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->maxLength:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
