.class public Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;
.super Ljava/lang/Object;
.source "VoconLmttQueue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;
    }
.end annotation


# instance fields
.field private currentTask:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;

.field private final taskQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;",
            ">;"
        }
    .end annotation
.end field

.field private final taskQueueSync:Ljava/lang/Object;

.field private final workerHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1
    .param p1, "workerHandler"    # Landroid/os/Handler;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->taskQueueSync:Ljava/lang/Object;

    .line 17
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->taskQueue:Ljava/util/LinkedList;

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->currentTask:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;

    .line 21
    iput-object p1, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->workerHandler:Landroid/os/Handler;

    .line 22
    return-void
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->completeCurrentTask()V

    return-void
.end method

.method private completeCurrentTask()V
    .locals 2

    .prologue
    .line 90
    iget-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->taskQueueSync:Ljava/lang/Object;

    monitor-enter v1

    .line 91
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->currentTask:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;

    if-eqz v0, :cond_0

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->currentTask:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;

    .line 96
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->processNextTaskIfPossible()V

    .line 97
    monitor-exit v1

    .line 98
    return-void

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private processNextTaskIfPossible()V
    .locals 3

    .prologue
    .line 77
    iget-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->taskQueueSync:Ljava/lang/Object;

    monitor-enter v1

    .line 78
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->currentTask:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->taskQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->taskQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->currentTask:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;

    .line 80
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->currentTask:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->workerHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->currentTask:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 86
    :cond_0
    monitor-exit v1

    .line 87
    return-void

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public addTask(Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;)V
    .locals 6
    .param p1, "task"    # Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;

    .prologue
    .line 25
    iget-object v4, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->taskQueueSync:Ljava/lang/Object;

    monitor-enter v4

    .line 26
    const/4 v0, 0x0

    .line 27
    .local v0, "addToStartOfQueue":Z
    :try_start_0
    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->currentTask:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->currentTask:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->getUploadType()Lcom/vlingo/core/internal/lmttvocon/UploadType;

    move-result-object v3

    invoke-virtual {p1}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->getUploadType()Lcom/vlingo/core/internal/lmttvocon/UploadType;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/vlingo/core/internal/lmttvocon/UploadType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->isStartFromScratch()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->currentTask:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->isStartFromScratch()Z

    move-result v3

    if-nez v3, :cond_1

    .line 30
    :cond_0
    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->currentTask:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;

    # invokes: Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->removeFromQueue()V
    invoke-static {v3}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->access$000(Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;)V

    .line 31
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->currentTask:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;

    .line 32
    const/4 v0, 0x1

    .line 34
    :cond_1
    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->taskQueue:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 35
    .local v2, "taskIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;>;"
    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 36
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;

    .line 37
    .local v1, "taskFromQueue":Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->getUploadType()Lcom/vlingo/core/internal/lmttvocon/UploadType;

    move-result-object v3

    invoke-virtual {p1}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->getUploadType()Lcom/vlingo/core/internal/lmttvocon/UploadType;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/vlingo/core/internal/lmttvocon/UploadType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 38
    invoke-virtual {p1}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->isStartFromScratch()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 39
    invoke-virtual {v1}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->isStartFromScratch()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 42
    monitor-exit v4

    .line 65
    .end local v1    # "taskFromQueue":Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;
    :goto_1
    return-void

    .line 46
    .restart local v1    # "taskFromQueue":Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 64
    .end local v1    # "taskFromQueue":Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;
    .end local v2    # "taskIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 51
    .restart local v1    # "taskFromQueue":Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;
    .restart local v2    # "taskIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;>;"
    :cond_4
    :try_start_1
    monitor-exit v4

    goto :goto_1

    .line 57
    .end local v1    # "taskFromQueue":Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;
    :cond_5
    # invokes: Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->linkWithQueue(Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;)V
    invoke-static {p1, p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->access$100(Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;)V

    .line 58
    if-eqz v0, :cond_6

    .line 59
    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->taskQueue:Ljava/util/LinkedList;

    invoke-virtual {v3, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 63
    :goto_2
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->processNextTaskIfPossible()V

    .line 64
    monitor-exit v4

    goto :goto_1

    .line 61
    :cond_6
    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->taskQueue:Ljava/util/LinkedList;

    invoke-virtual {v3, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method public cancelAll()V
    .locals 2

    .prologue
    .line 68
    iget-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->taskQueueSync:Ljava/lang/Object;

    monitor-enter v1

    .line 69
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->taskQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 70
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->currentTask:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->currentTask:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;

    # invokes: Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->removeFromQueue()V
    invoke-static {v0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->access$000(Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;)V

    .line 73
    :cond_0
    monitor-exit v1

    .line 74
    return-void

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getCurrentTask()Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->currentTask:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;

    return-object v0
.end method

.method public getTaskQueue()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->taskQueue:Ljava/util/LinkedList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
