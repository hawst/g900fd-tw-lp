.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;
.super Ljava/lang/Object;
.source "PhraseSpotterAudioLogger.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/AudioLogger;


# static fields
.field private static final BUFFER_LENGTH_MS:I = 0x4e20

.field private static smLoggerId:I


# instance fields
.field private mBAOutputStream:Ljava/io/ByteArrayOutputStream;

.field private mBytesLeft:I

.field private mDataOutputStream:Ljava/io/DataOutputStream;

.field private mId:I

.field private mIsDone:Z

.field private mPStartingSample:I

.field private mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

.field private mSampleRate:I

.field private mSamplesPerChunk:I

.field private mScore:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->smLoggerId:I

    return-void
.end method

.method constructor <init>(IIILcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "chunkSize"    # I
    .param p3, "sampleRate"    # I
    .param p4, "psParams"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mId:I

    .line 41
    iput p2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mSamplesPerChunk:I

    .line 42
    iput p3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mSampleRate:I

    .line 43
    iput-object p4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    .line 44
    mul-int/lit16 v0, p3, 0x4e20

    div-int/lit16 v0, v0, 0x3e8

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBytesLeft:I

    .line 45
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    iget v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBytesLeft:I

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBAOutputStream:Ljava/io/ByteArrayOutputStream;

    .line 46
    new-instance v0, Ljava/io/DataOutputStream;

    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBAOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mDataOutputStream:Ljava/io/DataOutputStream;

    .line 47
    return-void
.end method

.method static getInstance(IILcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;
    .locals 2
    .param p0, "chunkSize"    # I
    .param p1, "sampleRate"    # I
    .param p2, "psParams"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    .prologue
    .line 36
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    sget v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->smLoggerId:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->smLoggerId:I

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;-><init>(IIILcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;)V

    return-object v0
.end method


# virtual methods
.method public declared-synchronized dumpToFile()V
    .locals 12

    .prologue
    .line 80
    monitor-enter p0

    :try_start_0
    iget-boolean v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mIsDone:Z

    if-nez v9, :cond_0

    .line 81
    new-instance v5, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "PhraseSpotter"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mId:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ".raw"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v9, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 82
    .local v5, "file":Ljava/io/File;
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 83
    .local v6, "fos":Ljava/io/FileOutputStream;
    new-instance v0, Ljava/io/BufferedOutputStream;

    invoke-direct {v0, v6}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 84
    .local v0, "bos":Ljava/io/BufferedOutputStream;
    new-instance v4, Ljava/io/DataOutputStream;

    invoke-direct {v4, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 86
    .local v4, "dos":Ljava/io/DataOutputStream;
    iget-object v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBAOutputStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 87
    .local v1, "byteData":[B
    invoke-virtual {v4, v1}, Ljava/io/DataOutputStream;->write([B)V

    .line 88
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->flush()V

    .line 89
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V

    .line 91
    iget v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPStartingSample:I

    mul-int/lit16 v9, v9, 0x3e8

    iget v10, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mSampleRate:I

    div-int v7, v9, v10

    .line 92
    .local v7, "pStartingSampleMs":I
    iget-object v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getCoreSpotterParams()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getDeltaD()I

    move-result v9

    sub-int v3, v7, v9

    .line 93
    .local v3, "dStartingSampleMs":I
    iget v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mSampleRate:I

    mul-int/2addr v9, v3

    div-int/lit16 v2, v9, 0x3e8

    .line 95
    .local v2, "dStartingSample":I
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 96
    .local v8, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v9, "CONFIG\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    const-string/jumbo v9, "---------------\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    const-string/jumbo v9, "CHUNK_LENGTH_MS: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    iget-object v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getChunkLength()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 100
    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    const-string/jumbo v9, "PREBUFFER_LENGTH_MS: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    iget-object v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getPreBufferLength()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 103
    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    const-string/jumbo v9, "DELTA_D: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    iget-object v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getCoreSpotterParams()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getDeltaD()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 106
    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    const-string/jumbo v9, "SEAMLESS_TIMEOUT_MS: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    iget-object v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getSeamlessTimeout()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 109
    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    const-string/jumbo v9, "\nPHRASESPOTTER PARAMS\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    const-string/jumbo v9, "---------------\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    const-string/jumbo v9, "Language: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    iget-object v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getLanguage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    const-string/jumbo v9, "CGFilePath: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    iget-object v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getCoreSpotterParams()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getCGFilename()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    const-string/jumbo v9, "Beam: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    iget-object v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getCoreSpotterParams()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getBeam()F

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 120
    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    const-string/jumbo v9, "AbsBeam: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    iget-object v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getCoreSpotterParams()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getAbsbeam()F

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 123
    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    const-string/jumbo v9, "AOffset: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    iget-object v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getCoreSpotterParams()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getAoffset()F

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 126
    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    const-string/jumbo v9, "Delay: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    iget-object v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getCoreSpotterParams()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;->getDelay()F

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 129
    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    const-string/jumbo v9, "\nDATA\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    const-string/jumbo v9, "---------------\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    const-string/jumbo v9, "SamplesPerChunk: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    iget v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mSamplesPerChunk:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 134
    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    const-string/jumbo v9, "SampleRate: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    iget v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mSampleRate:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 137
    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    const-string/jumbo v9, "D Sample#: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 140
    const-string/jumbo v9, " ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "ms)"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    const-string/jumbo v9, "P Sample#: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    iget v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPStartingSample:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 144
    const-string/jumbo v9, " ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "ms)"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    const-string/jumbo v9, "Score: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    iget v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mScore:F

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 148
    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    new-instance v5, Ljava/io/File;

    .end local v5    # "file":Ljava/io/File;
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "PhraseSpotter"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mId:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ".txt"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v9, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 151
    .restart local v5    # "file":Ljava/io/File;
    new-instance v6, Ljava/io/FileOutputStream;

    .end local v6    # "fos":Ljava/io/FileOutputStream;
    invoke-direct {v6, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 152
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    new-instance v0, Ljava/io/BufferedOutputStream;

    .end local v0    # "bos":Ljava/io/BufferedOutputStream;
    invoke-direct {v0, v6}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 153
    .restart local v0    # "bos":Ljava/io/BufferedOutputStream;
    new-instance v4, Ljava/io/DataOutputStream;

    .end local v4    # "dos":Ljava/io/DataOutputStream;
    invoke-direct {v4, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 154
    .restart local v4    # "dos":Ljava/io/DataOutputStream;
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/io/DataOutputStream;->write([B)V

    .line 156
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->flush()V

    .line 157
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 163
    .end local v0    # "bos":Ljava/io/BufferedOutputStream;
    .end local v1    # "byteData":[B
    .end local v2    # "dStartingSample":I
    .end local v3    # "dStartingSampleMs":I
    .end local v4    # "dos":Ljava/io/DataOutputStream;
    .end local v5    # "file":Ljava/io/File;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .end local v7    # "pStartingSampleMs":I
    .end local v8    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    const/4 v9, 0x1

    :try_start_1
    iput-boolean v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mIsDone:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 165
    :goto_0
    monitor-exit p0

    return-void

    .line 160
    :catch_0
    move-exception v9

    .line 163
    const/4 v9, 0x1

    :try_start_2
    iput-boolean v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mIsDone:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 80
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9

    .line 163
    :catchall_1
    move-exception v9

    const/4 v10, 0x1

    :try_start_3
    iput-boolean v10, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mIsDone:Z

    throw v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method declared-synchronized markP()V
    .locals 1

    .prologue
    .line 70
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mDataOutputStream:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->size()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mPStartingSample:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    monitor-exit p0

    return-void

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized setScore(F)V
    .locals 1
    .param p1, "score"    # F

    .prologue
    .line 74
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mScore:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    monitor-exit p0

    return-void

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public writeData([SII)V
    .locals 5
    .param p1, "data"    # [S
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 52
    add-int v2, p2, p3

    .line 54
    .local v2, "indexLimit":I
    move v1, p2

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    iget v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBytesLeft:I

    if-lez v3, :cond_1

    .line 56
    :try_start_0
    iget-object v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mDataOutputStream:Ljava/io/DataOutputStream;

    aget-short v4, p1, v1

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 57
    iget v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBytesLeft:I

    add-int/lit8 v3, v3, -0x2

    iput v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBytesLeft:I

    .line 58
    iget v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->mBytesLeft:I

    if-nez v3, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;->dumpToFile()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 67
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    return-void
.end method
