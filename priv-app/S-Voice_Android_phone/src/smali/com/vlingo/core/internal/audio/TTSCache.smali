.class public final Lcom/vlingo/core/internal/audio/TTSCache;
.super Ljava/lang/Object;
.source "TTSCache.java"


# static fields
.field private static final CACHE_PRUNE_SIZE:I = 0x32

.field public static final DOMAIN_LOCAL_TTS:Ljava/lang/String; = "local_tts"

.field public static final DOMAIN_NETWORK_TTS:Ljava/lang/String; = "network_tts"

.field private static final FILETYPE_DEFAULT:Ljava/lang/String; = ".tts"

.field private static final FILETYPE_LOCAL:Ljava/lang/String; = ".wav"

.field private static final FILETYPE_NETWORK:Ljava/lang/String; = ".mp3"

.field private static final LOG_TAG:Ljava/lang/String; = "TTSCache"

.field private static final MAX_CACHE_SIZE:I = 0x64

.field private static final PERSISTANT_CACHE_SUFFIX:Ljava/lang/String; = "_persist"

.field private static final TTS_NONCACHE_TEMP_FILENAME_ROOT:Ljava/lang/String; = "tmp"

.field private static final TTS_SUBDIRECTORY_GENERAL:Ljava/lang/String; = "tts"

.field private static final TTS_SUBDIRECTORY_LOCAL_CACHING:Ljava/lang/String; = "local_tts"

.field private static final TTS_SUBDIRECTORY_LOCAL_NO_CACHING:Ljava/lang/String; = "files"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static cacheIsEmpty(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 5
    .param p0, "domain"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 316
    const/4 v4, 0x0

    invoke-static {p0, v4, p1}, Lcom/vlingo/core/internal/audio/TTSCache;->getFullFolderPath(Ljava/lang/String;Lcom/vlingo/core/internal/audio/TTSRequest;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 317
    .local v2, "fullFolderPath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 318
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    .line 322
    :cond_0
    :goto_0
    return v3

    .line 321
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 322
    .local v1, "fileNames":[Ljava/io/File;
    array-length v4, v1

    if-eqz v4, :cond_0

    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static cacheTTSRequest(Lcom/vlingo/core/internal/audio/TTSRequest;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 8
    .param p0, "request"    # Lcom/vlingo/core/internal/audio/TTSRequest;
    .param p1, "sourceFilePath"    # Ljava/lang/String;
    .param p2, "domain"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 217
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->isTtsCachingOn()Z

    move-result v1

    .line 218
    .local v1, "caching":Z
    if-nez v1, :cond_0

    .line 264
    .end local p1    # "sourceFilePath":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 221
    .restart local p1    # "sourceFilePath":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/TTSRequest;->getCacheKey()Ljava/lang/String;

    move-result-object v4

    .line 222
    .local v4, "key":Ljava/lang/String;
    invoke-static {p2, p0, p3}, Lcom/vlingo/core/internal/audio/TTSCache;->getFullFolderPath(Ljava/lang/String;Lcom/vlingo/core/internal/audio/TTSRequest;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 223
    .local v3, "fullFolderPath":Ljava/lang/String;
    invoke-static {v4, p2}, Lcom/vlingo/core/internal/audio/TTSCache;->getFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 224
    .local v0, "cacheFileName":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 225
    .local v2, "folder":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    .line 228
    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    .line 234
    :cond_1
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 235
    .local v5, "srcFile":Ljava/io/File;
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 236
    .local v6, "targetFile":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 239
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 241
    :cond_2
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 244
    invoke-virtual {v5, v6}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 247
    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 251
    :cond_3
    invoke-static {v5, v6}, Lcom/vlingo/core/internal/util/FileUtils;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 254
    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 258
    :cond_4
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    goto :goto_0
.end method

.method public static cachesAreEmpty(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 305
    const-string/jumbo v0, "network_tts"

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/audio/TTSCache;->cacheIsEmpty(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 306
    const/4 v0, 0x0

    .line 308
    :goto_0
    return v0

    :cond_0
    const-string/jumbo v0, "local_tts"

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/audio/TTSCache;->cacheIsEmpty(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method private static cleanUp(Ljava/lang/String;)V
    .locals 7
    .param p0, "fullFolderPath"    # Ljava/lang/String;

    .prologue
    .line 271
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 272
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 273
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 276
    .local v1, "fileNames":[Ljava/lang/String;
    array-length v5, v1

    const/16 v6, 0x64

    if-le v5, v6, :cond_1

    .line 278
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 279
    .local v2, "fsCacheFiles":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v5, v1

    if-ge v3, v5, :cond_0

    .line 280
    new-instance v5, Ljava/io/File;

    aget-object v6, v1, v3

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 279
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 282
    :cond_0
    new-instance v5, Lcom/vlingo/core/internal/audio/TTSCache$1;

    invoke-direct {v5}, Lcom/vlingo/core/internal/audio/TTSCache$1;-><init>()V

    invoke-static {v2, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 291
    array-length v5, v1

    add-int/lit8 v4, v5, -0x32

    .line 292
    .local v4, "numToDelete":I
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_1

    .line 293
    const/4 v5, 0x0

    invoke-interface {v2, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 292
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 298
    .end local v1    # "fileNames":[Ljava/lang/String;
    .end local v2    # "fsCacheFiles":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    .end local v3    # "i":I
    .end local v4    # "numToDelete":I
    :cond_1
    return-void
.end method

.method public static deleteCachedRequest(Ljava/lang/String;)Z
    .locals 2
    .param p0, "targetFileFullPath"    # Ljava/lang/String;

    .prologue
    .line 201
    if-nez p0, :cond_0

    .line 204
    const/4 v1, 0x0

    .line 207
    :goto_0
    return v1

    .line 206
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 207
    .local v0, "targetFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    goto :goto_0
.end method

.method public static deleteTempFile(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    .line 335
    const/4 v1, 0x0

    .line 336
    .local v1, "deletedLocal":Z
    const/4 v2, 0x0

    .line 337
    .local v2, "deletedRemote":Z
    const/4 v0, 0x0

    .line 341
    .local v0, "caching":Z
    const-string/jumbo v3, "local_tts"

    .local v3, "domain":Ljava/lang/String;
    move-object v7, v8

    .line 342
    check-cast v7, Ljava/lang/String;

    invoke-static {v7, v3, v0}, Lcom/vlingo/core/internal/audio/TTSCache;->getTempFilename(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 343
    .local v6, "tmpWavFileName":Ljava/lang/String;
    invoke-static {v3, p0, v0}, Lcom/vlingo/core/internal/audio/TTSCache;->getTempDirectory(Ljava/lang/String;Landroid/content/Context;Z)Ljava/io/File;

    move-result-object v5

    .line 344
    .local v5, "targetDir":Ljava/io/File;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 345
    .local v4, "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    move-result v1

    .line 347
    const-string/jumbo v3, "network_tts"

    .line 348
    check-cast v8, Ljava/lang/String;

    invoke-static {v8, v3, v0}, Lcom/vlingo/core/internal/audio/TTSCache;->getTempFilename(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 349
    invoke-static {v3, p0, v0}, Lcom/vlingo/core/internal/audio/TTSCache;->getTempDirectory(Ljava/lang/String;Landroid/content/Context;Z)Ljava/io/File;

    move-result-object v5

    .line 350
    new-instance v4, Ljava/io/File;

    .end local v4    # "file":Ljava/io/File;
    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 351
    .restart local v4    # "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    move-result v2

    .line 353
    if-nez v1, :cond_0

    if-eqz v2, :cond_1

    :cond_0
    const/4 v7, 0x1

    :goto_0
    return v7

    :cond_1
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public static getCachedTTSPath(Lcom/vlingo/core/internal/audio/TTSRequest;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 8
    .param p0, "request"    # Lcom/vlingo/core/internal/audio/TTSRequest;
    .param p1, "domain"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 170
    const/4 v5, 0x0

    .line 171
    .local v5, "toReturn":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->isTtsCachingOn()Z

    move-result v1

    .line 172
    .local v1, "caching":Z
    invoke-static {}, Lcom/vlingo/core/internal/util/DeviceWorkarounds;->doesDeviceSupportCachedPaths()Z

    move-result v6

    if-eqz v6, :cond_0

    if-eqz v1, :cond_0

    .line 173
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/TTSRequest;->getCacheKey()Ljava/lang/String;

    move-result-object v4

    .line 174
    .local v4, "key":Ljava/lang/String;
    invoke-static {p1, p0, p2}, Lcom/vlingo/core/internal/audio/TTSCache;->getFullFolderPath(Ljava/lang/String;Lcom/vlingo/core/internal/audio/TTSRequest;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 175
    .local v3, "fullFolderPath":Ljava/lang/String;
    invoke-static {v4, p1}, Lcom/vlingo/core/internal/audio/TTSCache;->getFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 178
    .local v0, "cacheFileName":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 181
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 182
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/io/File;->setLastModified(J)Z

    .line 185
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    .line 191
    .end local v0    # "cacheFileName":Ljava/lang/String;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "fullFolderPath":Ljava/lang/String;
    .end local v4    # "key":Ljava/lang/String;
    :cond_0
    return-object v5
.end method

.method private static getFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "domain"    # Ljava/lang/String;

    .prologue
    .line 141
    move-object v0, p0

    .line 142
    .local v0, "cacheFileName":Ljava/lang/String;
    const-string/jumbo v1, "local_tts"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 143
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".wav"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 149
    :goto_0
    return-object v0

    .line 144
    :cond_0
    const-string/jumbo v1, "network_tts"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 145
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".mp3"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 147
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".tts"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getFullFolderPath(Ljava/lang/String;Lcom/vlingo/core/internal/audio/TTSRequest;Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p0, "domain"    # Ljava/lang/String;
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/TTSRequest;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 153
    invoke-virtual {p2}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    .line 154
    .local v0, "cacheDir":Ljava/io/File;
    const-string/jumbo v4, "language"

    const-string/jumbo v5, "en-US"

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x2d

    const/16 v6, 0x5f

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    .line 155
    .local v2, "locale":Ljava/lang/String;
    const-string/jumbo v3, ""

    .line 156
    .local v3, "persist":Ljava/lang/String;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 159
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 160
    .local v1, "fullFolderPath":Ljava/lang/String;
    return-object v1
.end method

.method private static getTempDirectory(Ljava/lang/String;Landroid/content/Context;Z)Ljava/io/File;
    .locals 4
    .param p0, "domain"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "caching"    # Z

    .prologue
    const/4 v3, 0x3

    .line 82
    const-string/jumbo v2, "local_tts"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz p2, :cond_1

    .line 83
    const-string/jumbo v2, "local_tts"

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    .line 100
    .local v1, "targetDir":Ljava/io/File;
    :goto_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 101
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    move-result v2

    if-nez v2, :cond_3

    .line 116
    :cond_0
    :goto_1
    return-object v1

    .line 85
    .end local v1    # "targetDir":Ljava/io/File;
    :cond_1
    if-eqz p2, :cond_2

    .line 88
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string/jumbo v3, "tts"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .restart local v1    # "targetDir":Ljava/io/File;
    goto :goto_0

    .line 94
    .end local v1    # "targetDir":Ljava/io/File;
    :cond_2
    const-string/jumbo v0, "files"

    .line 95
    .local v0, "dirName":Ljava/lang/String;
    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    .restart local v1    # "targetDir":Ljava/io/File;
    goto :goto_0

    .line 105
    .end local v0    # "dirName":Ljava/lang/String;
    :cond_3
    invoke-virtual {v1}, Ljava/io/File;->canWrite()Z

    move-result v2

    if-nez v2, :cond_0

    .line 106
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/io/File;->setWritable(Z)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_1
.end method

.method public static getTempFile(Lcom/vlingo/core/internal/audio/TTSRequest;Ljava/lang/String;Landroid/content/Context;)Ljava/io/File;
    .locals 6
    .param p0, "request"    # Lcom/vlingo/core/internal/audio/TTSRequest;
    .param p1, "domain"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 123
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->isTtsCachingOn()Z

    move-result v0

    .line 124
    .local v0, "caching":Z
    invoke-static {}, Lcom/vlingo/core/internal/util/DeviceWorkarounds;->doesDeviceSupportCachedPaths()Z

    move-result v3

    if-nez v3, :cond_0

    .line 129
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "/sdcard/tmp-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/TTSRequest;->getTextToSpeak()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ".wav"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 137
    :goto_0
    return-object v3

    .line 131
    :cond_0
    if-nez v0, :cond_1

    .line 135
    :cond_1
    invoke-static {p0, p1, v0}, Lcom/vlingo/core/internal/audio/TTSCache;->getTempFilename(Lcom/vlingo/core/internal/audio/TTSRequest;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 136
    .local v1, "filename":Ljava/lang/String;
    invoke-static {p1, p2, v0}, Lcom/vlingo/core/internal/audio/TTSCache;->getTempDirectory(Ljava/lang/String;Landroid/content/Context;Z)Ljava/io/File;

    move-result-object v2

    .line 137
    .local v2, "targetDir":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static getTempFilename(Lcom/vlingo/core/internal/audio/TTSRequest;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1
    .param p0, "request"    # Lcom/vlingo/core/internal/audio/TTSRequest;
    .param p1, "domain"    # Ljava/lang/String;
    .param p2, "caching"    # Z

    .prologue
    .line 52
    if-eqz p2, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/TTSRequest;->getCacheKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/vlingo/core/internal/audio/TTSCache;->getTempFilename(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 55
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, p1, p2}, Lcom/vlingo/core/internal/audio/TTSCache;->getTempFilename(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getTempFilename(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 4
    .param p0, "cacheKey"    # Ljava/lang/String;
    .param p1, "domain"    # Ljava/lang/String;
    .param p2, "caching"    # Z

    .prologue
    .line 61
    if-eqz p2, :cond_0

    .line 62
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, "filename":Ljava/lang/String;
    :goto_0
    const-string/jumbo v1, "local_tts"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 67
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".wav"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 75
    :goto_1
    return-object v0

    .line 64
    .end local v0    # "filename":Ljava/lang/String;
    :cond_0
    const-string/jumbo v0, "tmp"

    .restart local v0    # "filename":Ljava/lang/String;
    goto :goto_0

    .line 69
    :cond_1
    const-string/jumbo v1, "network_tts"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 70
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".mp3"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 73
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".tts"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static purgeCache(Landroid/content/Context;Z)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "purgePersistentCache"    # Z

    .prologue
    .line 357
    const/4 v1, 0x0

    .line 361
    .local v1, "numDeleted":I
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/TTSCache;->cachesAreEmpty(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, v1

    .line 385
    .end local v1    # "numDeleted":I
    .local v2, "numDeleted":I
    :goto_0
    return v2

    .line 367
    .end local v2    # "numDeleted":I
    .restart local v1    # "numDeleted":I
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->isTtsCachingOn()Z

    move-result v0

    .line 368
    .local v0, "caching":Z
    if-nez v0, :cond_1

    .line 373
    :cond_1
    const-string/jumbo v3, "network_tts"

    invoke-static {v3, p0}, Lcom/vlingo/core/internal/audio/TTSCache;->purgeCache(Ljava/lang/String;Landroid/content/Context;)I

    move-result v3

    add-int/2addr v1, v3

    .line 374
    if-eqz p1, :cond_2

    .line 375
    const-string/jumbo v3, "network_tts_persist"

    invoke-static {v3, p0}, Lcom/vlingo/core/internal/audio/TTSCache;->purgeCache(Ljava/lang/String;Landroid/content/Context;)I

    move-result v3

    add-int/2addr v1, v3

    .line 379
    :cond_2
    const-string/jumbo v3, "local_tts"

    invoke-static {v3, p0}, Lcom/vlingo/core/internal/audio/TTSCache;->purgeCache(Ljava/lang/String;Landroid/content/Context;)I

    move-result v3

    add-int/2addr v1, v3

    .line 380
    if-eqz p1, :cond_3

    .line 381
    const-string/jumbo v3, "local_tts_persist"

    invoke-static {v3, p0}, Lcom/vlingo/core/internal/audio/TTSCache;->purgeCache(Ljava/lang/String;Landroid/content/Context;)I

    move-result v3

    add-int/2addr v1, v3

    :cond_3
    move v2, v1

    .line 385
    .end local v1    # "numDeleted":I
    .restart local v2    # "numDeleted":I
    goto :goto_0
.end method

.method private static purgeCache(Ljava/lang/String;Landroid/content/Context;)I
    .locals 10
    .param p0, "domain"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 389
    const/4 v8, 0x0

    .line 392
    .local v8, "numDeleted":I
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->isTtsCachingOn()Z

    move-result v1

    .line 393
    .local v1, "caching":Z
    if-nez v1, :cond_0

    .line 397
    :cond_0
    const/4 v9, 0x0

    invoke-static {p0, v9, p1}, Lcom/vlingo/core/internal/audio/TTSCache;->getFullFolderPath(Ljava/lang/String;Lcom/vlingo/core/internal/audio/TTSRequest;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 398
    .local v5, "fullFolderPath":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 399
    .local v2, "dir":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 400
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .line 403
    .local v4, "fileNames":[Ljava/io/File;
    move-object v0, v4

    .local v0, "arr$":[Ljava/io/File;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v7, :cond_2

    aget-object v3, v0, v6

    .line 404
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 405
    add-int/lit8 v8, v8, 0x1

    .line 403
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 408
    .end local v3    # "file":Ljava/io/File;
    :cond_2
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 416
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v4    # "fileNames":[Ljava/io/File;
    .end local v6    # "i$":I
    .end local v7    # "len$":I
    :cond_3
    return v8
.end method
