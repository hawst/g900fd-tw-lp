.class public final enum Lcom/vlingo/core/internal/domain/DomainName;
.super Ljava/lang/Enum;
.source "DomainName.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/domain/DomainName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/domain/DomainName;

.field private static final DOMAINS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/domain/DomainName;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum VOICEDIAL:Lcom/vlingo/core/internal/domain/DomainName;


# instance fields
.field private value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 13
    new-instance v5, Lcom/vlingo/core/internal/domain/DomainName;

    const-string/jumbo v6, "VOICEDIAL"

    const-string/jumbo v7, "voicedial"

    invoke-direct {v5, v6, v8, v7}, Lcom/vlingo/core/internal/domain/DomainName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/vlingo/core/internal/domain/DomainName;->VOICEDIAL:Lcom/vlingo/core/internal/domain/DomainName;

    .line 11
    const/4 v5, 0x1

    new-array v5, v5, [Lcom/vlingo/core/internal/domain/DomainName;

    sget-object v6, Lcom/vlingo/core/internal/domain/DomainName;->VOICEDIAL:Lcom/vlingo/core/internal/domain/DomainName;

    aput-object v6, v5, v8

    sput-object v5, Lcom/vlingo/core/internal/domain/DomainName;->$VALUES:[Lcom/vlingo/core/internal/domain/DomainName;

    .line 18
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 19
    .local v1, "dimainMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/domain/DomainName;>;"
    invoke-static {}, Lcom/vlingo/core/internal/domain/DomainName;->values()[Lcom/vlingo/core/internal/domain/DomainName;

    move-result-object v0

    .local v0, "arr$":[Lcom/vlingo/core/internal/domain/DomainName;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v2, v0, v3

    .line 20
    .local v2, "domain":Lcom/vlingo/core/internal/domain/DomainName;
    invoke-virtual {v2}, Lcom/vlingo/core/internal/domain/DomainName;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 22
    .end local v2    # "domain":Lcom/vlingo/core/internal/domain/DomainName;
    :cond_0
    sput-object v1, Lcom/vlingo/core/internal/domain/DomainName;->DOMAINS:Ljava/util/Map;

    .line 23
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    iput-object p3, p0, Lcom/vlingo/core/internal/domain/DomainName;->value:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public static get(Ljava/lang/String;)Lcom/vlingo/core/internal/domain/DomainName;
    .locals 1
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 36
    sget-object v0, Lcom/vlingo/core/internal/domain/DomainName;->DOMAINS:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/domain/DomainName;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/domain/DomainName;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lcom/vlingo/core/internal/domain/DomainName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/domain/DomainName;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/domain/DomainName;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/vlingo/core/internal/domain/DomainName;->$VALUES:[Lcom/vlingo/core/internal/domain/DomainName;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/domain/DomainName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/domain/DomainName;

    return-object v0
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vlingo/core/internal/domain/DomainName;->value:Ljava/lang/String;

    return-object v0
.end method
