.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "AnswerQuestionHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$TrueKnowledge;,
        Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$WolframAlpha;,
        Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;,
        Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$Reaction;,
        Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;
    }
.end annotation


# static fields
.field private static final NUMBER_OF_ANSWER:I = 0x5

.field private static mHandler:Landroid/os/Handler;

.field static moreButtonText:Ljava/lang/String;

.field static sAnswerPrompt:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 345
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->reset()V

    return-void
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 40
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->handleWithGoogleNow(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->getRandomAnswer()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 40
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    :goto_0
    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->mHandler:Landroid/os/Handler;

    .line 90
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->mHandler:Landroid/os/Handler;

    return-object v0

    .line 89
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->mHandler:Landroid/os/Handler;

    goto :goto_0
.end method

.method private getRandomAnswer()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 389
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 390
    .local v0, "random":Ljava/util/Random;
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 402
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_WEB_SEARCH:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    .line 392
    :pswitch_0
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_WEB_SEARCH:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 394
    :pswitch_1
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_WEB_SEARCH_1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 396
    :pswitch_2
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_WEB_SEARCH_2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 398
    :pswitch_3
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_WEB_SEARCH_3:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 400
    :pswitch_4
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_WEB_SEARCH_4:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 390
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private handleWithGoogleNow(Ljava/lang/String;)V
    .locals 3
    .param p1, "question"    # Ljava/lang/String;

    .prologue
    .line 406
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->LAUNCH_ACTIVITY:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    const-string/jumbo v1, "com.google.android.googlequicksearchbox"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->enclosingPackage(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v0

    const-string/jumbo v1, "com.google.android.googlequicksearchbox.SearchActivity"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->activity(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "query,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->extra(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v0

    const-string/jumbo v1, "android.intent.action.WEB_SEARCH"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->action(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v0

    const-string/jumbo v1, "Google Search"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->app(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->queue()V

    .line 413
    return-void
.end method

.method private processLocally(Lcom/vlingo/core/internal/questions/parser/ServerResponse;)V
    .locals 2
    .param p1, "answer"    # Lcom/vlingo/core/internal/questions/parser/ServerResponse;

    .prologue
    .line 84
    invoke-virtual {p1}, Lcom/vlingo/core/internal/questions/parser/ServerResponse;->getAnswer()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/answers/AnswerManager;->replaceAnswerVariables(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 85
    .local v0, "ans":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 86
    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 4
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v3, 0x0

    .line 59
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 61
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v1

    const-string/jumbo v2, "qanda"

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 63
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_qa_more:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->moreButtonText:Ljava/lang/String;

    .line 64
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_answer_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->sAnswerPrompt:Ljava/lang/String;

    .line 66
    new-instance v0, Lcom/vlingo/core/internal/questions/parser/ServerResponse;

    invoke-direct {v0}, Lcom/vlingo/core/internal/questions/parser/ServerResponse;-><init>()V

    .line 68
    .local v0, "answer":Lcom/vlingo/core/internal/questions/parser/ServerResponse;
    const-string/jumbo v1, "Answer"

    invoke-interface {p1, v1}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/questions/parser/ServerResponse;->setAnswer(Ljava/lang/String;)V

    .line 69
    const-string/jumbo v1, "Question"

    invoke-interface {p1, v1}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/questions/parser/ServerResponse;->setQuestion(Ljava/lang/String;)V

    .line 70
    const-string/jumbo v1, "ProviderSrcResponses"

    invoke-static {p1, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/questions/parser/ServerResponse;->setXML(Ljava/lang/String;)V

    .line 76
    invoke-virtual {v0}, Lcom/vlingo/core/internal/questions/parser/ServerResponse;->getXML()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 77
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->processLocally(Lcom/vlingo/core/internal/questions/parser/ServerResponse;)V

    .line 79
    :cond_0
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;

    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-direct {v1, p0, v0, v2, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;-><init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;Lcom/vlingo/core/internal/questions/parser/ServerResponse;Landroid/os/Handler;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->start()V

    .line 80
    const/4 v1, 0x1

    return v1
.end method
