.class public final enum Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;
.super Ljava/lang/Enum;
.source "WithSpeechSilenceDurationCategory.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

.field public static final enum LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

.field public static final enum LONG_LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

.field public static final enum MEDIUM:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

.field public static final enum MEDIUM_LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

.field public static final enum SHORT:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    const-string/jumbo v1, "SHORT"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->SHORT:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    .line 5
    new-instance v0, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    const-string/jumbo v1, "MEDIUM"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->MEDIUM:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    .line 6
    new-instance v0, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    const-string/jumbo v1, "MEDIUM_LONG"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->MEDIUM_LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    .line 7
    new-instance v0, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    const-string/jumbo v1, "LONG"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    .line 8
    new-instance v0, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    const-string/jumbo v1, "LONG_LONG"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->LONG_LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    .line 3
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    sget-object v1, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->SHORT:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->MEDIUM:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->MEDIUM_LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->LONG_LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->$VALUES:[Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->$VALUES:[Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    return-object v0
.end method
