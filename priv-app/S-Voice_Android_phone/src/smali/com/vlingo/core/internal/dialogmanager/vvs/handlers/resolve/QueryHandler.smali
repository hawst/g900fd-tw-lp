.class public abstract Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/QueryHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "QueryHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 1
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 17
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 18
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/QueryHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->doesEventResolveQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 19
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/QueryHandler;->sendEmptyEvent()V

    .line 20
    const/4 v0, 0x0

    .line 24
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/QueryHandler;->executeQuery(Lcom/vlingo/sdk/recognition/VLAction;)Z

    move-result v0

    goto :goto_0
.end method

.method protected abstract executeQuery(Lcom/vlingo/sdk/recognition/VLAction;)Z
.end method

.method protected abstract sendEmptyEvent()V
.end method
