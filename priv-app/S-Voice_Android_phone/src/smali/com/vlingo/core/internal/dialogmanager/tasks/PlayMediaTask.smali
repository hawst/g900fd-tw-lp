.class public Lcom/vlingo/core/internal/dialogmanager/tasks/PlayMediaTask;
.super Lcom/vlingo/core/internal/dialogmanager/tasks/PlayAudioTask;
.source "PlayMediaTask.java"


# instance fields
.field private mFileToPlayResid:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "fileResId"    # I

    .prologue
    .line 21
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayMediaTask;-><init>(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;I)V

    .line 22
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;I)V
    .locals 0
    .param p1, "audioPlaybackListener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;
    .param p2, "fileResId"    # I

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayAudioTask;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayMediaTask;->mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .line 26
    iput p2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayMediaTask;->mFileToPlayResid:I

    .line 27
    return-void
.end method

.method private playMedia()V
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayMediaTask;->mFileToPlayResid:I

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/MediaAudioRequest;->getRequest(I)Lcom/vlingo/core/internal/audio/MediaAudioRequest;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->play(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 46
    return-void
.end method


# virtual methods
.method public isSystemTts()Z
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    return v0
.end method

.method public run()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayMediaTask;->playMedia()V

    .line 36
    return-void
.end method
