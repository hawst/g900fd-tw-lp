.class public Lcom/vlingo/core/internal/questions/parser/ProviderResponse;
.super Lcom/vlingo/core/internal/questions/parser/ResponseElement;
.source "ProviderResponse.java"

# interfaces
.implements Lcom/vlingo/core/internal/questions/Answer;


# static fields
.field private static DEBUG:I


# instance fields
.field private mAnswer:Lcom/vlingo/core/internal/questions/parser/ServerResponse;

.field private mSections:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/questions/parser/SectionElement;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput v0, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->DEBUG:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/questions/parser/ResponseElement;-><init>(Ljava/lang/String;)V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->mSections:Ljava/util/ArrayList;

    .line 29
    return-void
.end method


# virtual methods
.method public add(Lcom/vlingo/core/internal/questions/parser/SectionElement;)V
    .locals 1
    .param p1, "section"    # Lcom/vlingo/core/internal/questions/parser/SectionElement;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->mSections:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 53
    return-void
.end method

.method public getAnswer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->mAnswer:Lcom/vlingo/core/internal/questions/parser/ServerResponse;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->mAnswer:Lcom/vlingo/core/internal/questions/parser/ServerResponse;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/questions/parser/ServerResponse;->getAnswer()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getInformationalSections()[Lcom/vlingo/core/internal/questions/Answer$Section;
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->getSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v0

    return-object v0
.end method

.method public getProvider()Ljava/lang/String;
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->attributes:Ljava/util/Map;

    sget-object v1, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->PROVIDER:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getQuestion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->mAnswer:Lcom/vlingo/core/internal/questions/parser/ServerResponse;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->mAnswer:Lcom/vlingo/core/internal/questions/parser/ServerResponse;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/questions/parser/ServerResponse;->getQuestion()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getRawResponse()Ljava/lang/String;
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->attributes:Ljava/util/Map;

    sget-object v1, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->RAW_RESPONSE:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getSection(Ljava/lang/String;)Lcom/vlingo/core/internal/questions/Answer$Section;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 60
    iget-object v4, p0, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->mSections:Ljava/util/ArrayList;

    const/4 v5, 0x0

    new-array v5, v5, [Lcom/vlingo/core/internal/questions/Answer$Section;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/questions/Answer$Section;

    .local v0, "arr$":[Lcom/vlingo/core/internal/questions/Answer$Section;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 61
    .local v3, "section":Lcom/vlingo/core/internal/questions/Answer$Section;
    invoke-interface {v3}, Lcom/vlingo/core/internal/questions/Answer$Section;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 63
    .end local v3    # "section":Lcom/vlingo/core/internal/questions/Answer$Section;
    :goto_1
    return-object v3

    .line 60
    .restart local v3    # "section":Lcom/vlingo/core/internal/questions/Answer$Section;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 63
    .end local v3    # "section":Lcom/vlingo/core/internal/questions/Answer$Section;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getSections()[Lcom/vlingo/core/internal/questions/Answer$Section;
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->mSections:Ljava/util/ArrayList;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/vlingo/core/internal/questions/Answer$Section;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/questions/Answer$Section;

    return-object v0
.end method

.method public getSimpleResponse()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->attributes:Ljava/util/Map;

    sget-object v1, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->SIMPLE_RESPONSE:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public hasAnswer()Z
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->getSimpleResponse()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->getSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->getSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_1

    .line 42
    :cond_0
    const/4 v0, 0x0

    .line 44
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hasMoreInformation()Z
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->getInformationalSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->getInformationalSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSection(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->getSection(Ljava/lang/String;)Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected paran(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setAnswer(Lcom/vlingo/core/internal/questions/parser/ServerResponse;)V
    .locals 0
    .param p1, "answer"    # Lcom/vlingo/core/internal/questions/parser/ServerResponse;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->mAnswer:Lcom/vlingo/core/internal/questions/parser/ServerResponse;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 11

    .prologue
    .line 74
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 76
    .local v2, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "  Question"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->getQuestion()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->quoted(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->paran(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "  Answer"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->getAnswer()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->quoted(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->paran(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "  Provider"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->getProvider()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->quoted(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->paran(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "  Response"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->getSimpleResponse()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->quoted(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->paran(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "  RawResponse"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget v9, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->DEBUG:I

    if-lez v9, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->getRawResponse()Ljava/lang/String;

    move-result-object v9

    :goto_0
    invoke-virtual {p0, v9}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->quoted(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->paran(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->getSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v0

    .local v0, "arr$":[Lcom/vlingo/core/internal/questions/Answer$Section;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    move v4, v3

    .end local v0    # "arr$":[Lcom/vlingo/core/internal/questions/Answer$Section;
    .end local v3    # "i$":I
    .end local v5    # "len$":I
    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_2

    aget-object v7, v0, v4

    .line 97
    .local v7, "section":Lcom/vlingo/core/internal/questions/Answer$Section;
    const-string/jumbo v9, "\n"

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "  Section"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v7}, Lcom/vlingo/core/internal/questions/Answer$Section;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->quoted(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->paran(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    invoke-interface {v7}, Lcom/vlingo/core/internal/questions/Answer$Section;->getSubsections()[Lcom/vlingo/core/internal/questions/Answer$Subsection;

    move-result-object v1

    .local v1, "arr$":[Lcom/vlingo/core/internal/questions/Answer$Subsection;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v3, 0x0

    .end local v4    # "i$":I
    .restart local v3    # "i$":I
    :goto_2
    if-ge v3, v6, :cond_1

    aget-object v8, v1, v3

    .line 104
    .local v8, "subsection":Lcom/vlingo/core/internal/questions/Answer$Subsection;
    const-string/jumbo v9, "\n"

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "    Subsection"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "    ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "      text"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v8}, Lcom/vlingo/core/internal/questions/Answer$Subsection;->getText()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->quoted(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->paran(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "      url"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v8}, Lcom/vlingo/core/internal/questions/Answer$Subsection;->getImageUrl()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->quoted(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->paran(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "      height"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v8}, Lcom/vlingo/core/internal/questions/Answer$Subsection;->getHeight()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->quoted(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->paran(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "    )"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    .line 76
    .end local v1    # "arr$":[Lcom/vlingo/core/internal/questions/Answer$Subsection;
    .end local v3    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "section":Lcom/vlingo/core/internal/questions/Answer$Section;
    .end local v8    # "subsection":Lcom/vlingo/core/internal/questions/Answer$Subsection;
    :cond_0
    const-string/jumbo v9, "..."

    goto/16 :goto_0

    .line 95
    .restart local v1    # "arr$":[Lcom/vlingo/core/internal/questions/Answer$Subsection;
    .restart local v3    # "i$":I
    .restart local v6    # "len$":I
    .restart local v7    # "section":Lcom/vlingo/core/internal/questions/Answer$Section;
    :cond_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    .end local v3    # "i$":I
    .restart local v4    # "i$":I
    goto/16 :goto_1

    .line 124
    .end local v1    # "arr$":[Lcom/vlingo/core/internal/questions/Answer$Subsection;
    .end local v6    # "len$":I
    .end local v7    # "section":Lcom/vlingo/core/internal/questions/Answer$Section;
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    return-object v9
.end method
