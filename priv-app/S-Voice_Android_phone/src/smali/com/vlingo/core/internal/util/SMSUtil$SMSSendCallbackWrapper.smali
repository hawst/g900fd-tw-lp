.class public Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;
.super Ljava/lang/Object;
.source "SMSUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/SMSUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SMSSendCallbackWrapper"
.end annotation


# instance fields
.field private callback:Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallback;

.field private parts:I


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallback;

    .prologue
    .line 626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 624
    const/4 v0, 0x1

    iput v0, p0, Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;->parts:I

    .line 627
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;->setCallback(Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallback;)V

    .line 628
    iget v0, p0, Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;->parts:I

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;->setParts(I)V

    .line 630
    return-void
.end method


# virtual methods
.method public getCallback()Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallback;
    .locals 1

    .prologue
    .line 633
    iget-object v0, p0, Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;->callback:Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallback;

    return-object v0
.end method

.method public getParts()I
    .locals 1

    .prologue
    .line 641
    iget v0, p0, Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;->parts:I

    return v0
.end method

.method public setCallback(Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallback;

    .prologue
    .line 637
    iput-object p1, p0, Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;->callback:Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallback;

    .line 638
    return-void
.end method

.method public setParts(I)V
    .locals 0
    .param p1, "parts"    # I

    .prologue
    .line 645
    iput p1, p0, Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;->parts:I

    .line 646
    return-void
.end method
