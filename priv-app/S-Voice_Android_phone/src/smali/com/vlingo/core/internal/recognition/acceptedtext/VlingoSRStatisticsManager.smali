.class public Lcom/vlingo/core/internal/recognition/acceptedtext/VlingoSRStatisticsManager;
.super Ljava/lang/Object;
.source "VlingoSRStatisticsManager.java"


# static fields
.field private static instance:Lcom/vlingo/core/internal/recognition/acceptedtext/VlingoSRStatisticsManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/vlingo/core/internal/recognition/acceptedtext/VlingoSRStatisticsManager;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/VlingoSRStatisticsManager;->instance:Lcom/vlingo/core/internal/recognition/acceptedtext/VlingoSRStatisticsManager;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/vlingo/core/internal/recognition/acceptedtext/VlingoSRStatisticsManager;

    invoke-direct {v0}, Lcom/vlingo/core/internal/recognition/acceptedtext/VlingoSRStatisticsManager;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/VlingoSRStatisticsManager;->instance:Lcom/vlingo/core/internal/recognition/acceptedtext/VlingoSRStatisticsManager;

    .line 31
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/recognition/acceptedtext/VlingoSRStatisticsManager;->instance:Lcom/vlingo/core/internal/recognition/acceptedtext/VlingoSRStatisticsManager;

    return-object v0
.end method


# virtual methods
.method public sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V
    .locals 3
    .param p1, "at"    # Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;

    .prologue
    .line 50
    if-eqz p1, :cond_0

    .line 54
    invoke-virtual {p1}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->getGUttId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 55
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/VLSdk;->getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->getGUttId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->getXMLString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/vlingo/sdk/recognition/VLRecognizer;->acceptedText(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :cond_0
    return-void
.end method
