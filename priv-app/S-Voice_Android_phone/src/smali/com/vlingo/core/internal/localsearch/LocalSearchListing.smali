.class public abstract Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
.super Ljava/lang/Object;
.source "LocalSearchListing.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;,
        Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;
    }
.end annotation


# instance fields
.field m_reviews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;",
            ">;"
        }
    .end annotation
.end field

.field private m_values:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "keyBusinessID"    # Ljava/lang/String;
    .param p2, "valueBusinessID"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->m_reviews:Ljava/util/ArrayList;

    .line 65
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->m_values:Ljava/util/Hashtable;

    .line 66
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->putValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    return-void
.end method

.method private getReviewById(Ljava/lang/String;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;
    .locals 4
    .param p1, "reviewId"    # Ljava/lang/String;

    .prologue
    .line 222
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 223
    iget-object v3, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->m_reviews:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 224
    .local v1, "numReviews":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 225
    iget-object v3, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->m_reviews:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;

    .line 226
    .local v2, "rev":Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;
    iget-object v3, v2, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;->id:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 231
    .end local v0    # "i":I
    .end local v1    # "numReviews":I
    .end local v2    # "rev":Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;
    :goto_1
    return-object v2

    .line 224
    .restart local v0    # "i":I
    .restart local v1    # "numReviews":I
    .restart local v2    # "rev":Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 231
    .end local v0    # "i":I
    .end local v1    # "numReviews":I
    .end local v2    # "rev":Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private removeReviewById(Ljava/lang/String;)V
    .locals 2
    .param p1, "reviewId"    # Ljava/lang/String;

    .prologue
    .line 215
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getReviewById(Ljava/lang/String;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;

    move-result-object v0

    .line 216
    .local v0, "rev":Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;
    if-eqz v0, :cond_0

    .line 217
    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->m_reviews:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 219
    :cond_0
    return-void
.end method


# virtual methods
.method addReview(Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;)V
    .locals 1
    .param p1, "rev"    # Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;

    .prologue
    .line 210
    iget-object v0, p1, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;->id:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->removeReviewById(Ljava/lang/String;)V

    .line 211
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->m_reviews:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    return-void
.end method

.method public areMoreDetailsAvailable()Z
    .locals 3

    .prologue
    .line 149
    iget-object v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->m_values:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    .line 150
    .local v0, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 151
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 152
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->m_values:Ljava/util/Hashtable;

    invoke-virtual {v2, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;

    iget-boolean v2, v2, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;->isMoreAvailable:Z

    if-eqz v2, :cond_0

    .line 153
    const/4 v2, 0x1

    .line 156
    .end local v1    # "key":Ljava/lang/String;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public areMoreDetailsAvailable(Ljava/lang/String;)Z
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 160
    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->m_values:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;

    .line 161
    .local v0, "f":Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;
    if-eqz v0, :cond_0

    .line 162
    iget-boolean v1, v0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;->isMoreAvailable:Z

    .line 164
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public abstract getAddressLine1()Ljava/lang/String;
.end method

.method public abstract getAddressLine2()Ljava/lang/String;
.end method

.method public abstract getCaption()Ljava/lang/String;
.end method

.method public abstract getCityState()Ljava/lang/String;
.end method

.method public abstract getClickUrl()Ljava/lang/String;
.end method

.method public abstract getDistanceString()Ljava/lang/String;
.end method

.method public getDouble(Ljava/lang/String;D)D
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "defaultValue"    # D

    .prologue
    .line 182
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->hasValue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 183
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 185
    .local v0, "value":Ljava/lang/String;
    :try_start_0
    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    .line 192
    .end local v0    # "value":Ljava/lang/String;
    .end local p2    # "defaultValue":D
    :cond_0
    :goto_0
    return-wide p2

    .line 187
    .restart local v0    # "value":Ljava/lang/String;
    .restart local p2    # "defaultValue":D
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public abstract getFullAddress()Ljava/lang/String;
.end method

.method public getInteger(Ljava/lang/String;I)I
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "defaultValue"    # I

    .prologue
    .line 196
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->hasValue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 197
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 199
    .local v0, "value":Ljava/lang/String;
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 206
    .end local v0    # "value":Ljava/lang/String;
    .end local p2    # "defaultValue":I
    :cond_0
    :goto_0
    return p2

    .line 201
    .restart local v0    # "value":Ljava/lang/String;
    .restart local p2    # "defaultValue":I
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public abstract getLatitude()Ljava/lang/String;
.end method

.method public abstract getListingID()Ljava/lang/String;
.end method

.method public abstract getLongitude()Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getPhoneNumber()Ljava/lang/String;
.end method

.method public abstract getPhoneNumberFormatted()Ljava/lang/String;
.end method

.method public abstract getProvider()Ljava/lang/String;
.end method

.method public abstract getRating()D
.end method

.method public abstract getReserveUrl()Ljava/lang/String;
.end method

.method public getReview(I)Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 119
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->m_reviews:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;

    return-object v0
.end method

.method public abstract getReviewCount()I
.end method

.method public getReviews()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->m_reviews:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 168
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->hasValue(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->m_values:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;

    iget-object v0, v0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;->value:Ljava/lang/String;

    .line 172
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 177
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 178
    .local v0, "value":Ljava/lang/String;
    if-nez v0, :cond_0

    .end local p2    # "defaultValue":Ljava/lang/String;
    :goto_0
    return-object p2

    .restart local p2    # "defaultValue":Ljava/lang/String;
    :cond_0
    move-object p2, v0

    goto :goto_0
.end method

.method public abstract getSynopsis()Ljava/lang/String;
.end method

.method public abstract getUrl()Ljava/lang/String;
.end method

.method public hasReviews()Z
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->m_reviews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getReviewCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 113
    :cond_0
    const/4 v0, 0x1

    .line 115
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasValue(Ljava/lang/String;)Z
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 127
    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->m_values:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 128
    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->m_values:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;

    .line 129
    .local v0, "field":Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;
    iget-object v1, v0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;->value:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;->value:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 130
    const/4 v1, 0x1

    .line 133
    .end local v0    # "field":Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public abstract isOrganic()Z
.end method

.method public abstract isSponsored()Z
.end method

.method public markValueAsAvailable(Ljava/lang/String;)V
    .locals 3
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 141
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->m_values:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->m_values:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;

    iput-boolean v2, v0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;->isMoreAvailable:Z

    .line 146
    :goto_0
    return-void

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->m_values:Ljava/util/Hashtable;

    new-instance v1, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;

    invoke-direct {v1, p0, v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;-><init>(Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Z)V

    invoke-virtual {v0, p1, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public putValue(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->m_values:Ljava/util/Hashtable;

    new-instance v1, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;

    invoke-direct {v1, p0, p2}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Field;-><init>(Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    return-void
.end method
