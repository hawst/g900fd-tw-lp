.class public interface abstract Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
.super Ljava/lang/Object;
.source "PhraseSpotterListener.java"


# virtual methods
.method public abstract onPhraseDetected(Ljava/lang/String;)V
.end method

.method public abstract onPhraseSpotted(Ljava/lang/String;)V
.end method

.method public abstract onPhraseSpotterStarted()V
.end method

.method public abstract onPhraseSpotterStopped()V
.end method

.method public abstract onSeamlessPhraseSpotted(Ljava/lang/String;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
.end method
