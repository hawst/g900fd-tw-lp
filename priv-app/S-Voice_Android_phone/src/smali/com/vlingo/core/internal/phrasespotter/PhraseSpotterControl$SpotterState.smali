.class abstract Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
.super Ljava/lang/Object;
.source "PhraseSpotterControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "SpotterState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnter()V
    .locals 3

    .prologue
    .line 236
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".onEnter():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    return-void
.end method

.method public onExit()V
    .locals 3

    .prologue
    .line 240
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".onExit():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    return-void
.end method

.method public start(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V
    .locals 3
    .param p1, "phraseSpotterParams"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .param p2, "listener"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    .prologue
    .line 220
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".start()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 229
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".stop()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    const/4 v1, 0x0

    # setter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRestartWhenIdle:Z
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$002(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Z)Z

    .line 231
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
