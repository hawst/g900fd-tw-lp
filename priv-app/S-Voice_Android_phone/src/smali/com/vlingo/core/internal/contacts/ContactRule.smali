.class public Lcom/vlingo/core/internal/contacts/ContactRule;
.super Ljava/lang/Object;
.source "ContactRule.java"


# instance fields
.field private final name:Ljava/lang/String;

.field private final query:Ljava/lang/String;

.field private final score:Lcom/vlingo/core/internal/contacts/scoring/ContactScore;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "score"    # Lcom/vlingo/core/internal/contacts/scoring/ContactScore;

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactRule;->name:Ljava/lang/String;

    .line 12
    iput-object p2, p0, Lcom/vlingo/core/internal/contacts/ContactRule;->query:Ljava/lang/String;

    .line 13
    iput-object p3, p0, Lcom/vlingo/core/internal/contacts/ContactRule;->score:Lcom/vlingo/core/internal/contacts/scoring/ContactScore;

    .line 14
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactRule;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactRule;->query:Ljava/lang/String;

    return-object v0
.end method

.method public getScore()Lcom/vlingo/core/internal/contacts/scoring/ContactScore;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactRule;->score:Lcom/vlingo/core/internal/contacts/scoring/ContactScore;

    return-object v0
.end method
