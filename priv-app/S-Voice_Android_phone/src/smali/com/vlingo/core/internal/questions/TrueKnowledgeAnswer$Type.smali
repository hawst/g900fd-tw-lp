.class public final enum Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;
.super Ljava/lang/Enum;
.source "TrueKnowledgeAnswer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

.field public static final enum FACT_ANSWER:Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

.field public static final enum LIST_ANSWER:Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

.field public static final enum SIMPLE_ANSWER:Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

.field public static final enum YES_NO_ANSWER:Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14
    new-instance v0, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

    const-string/jumbo v1, "SIMPLE_ANSWER"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;->SIMPLE_ANSWER:Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

    new-instance v0, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

    const-string/jumbo v1, "YES_NO_ANSWER"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;->YES_NO_ANSWER:Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

    new-instance v0, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

    const-string/jumbo v1, "FACT_ANSWER"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;->FACT_ANSWER:Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

    new-instance v0, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

    const-string/jumbo v1, "LIST_ANSWER"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;->LIST_ANSWER:Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

    sget-object v1, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;->SIMPLE_ANSWER:Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;->YES_NO_ANSWER:Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;->FACT_ANSWER:Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;->LIST_ANSWER:Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;->$VALUES:[Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;->$VALUES:[Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

    return-object v0
.end method
