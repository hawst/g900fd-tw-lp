.class public Lcom/vlingo/core/internal/messages/SMSMMSAlert;
.super Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
.source "SMSMMSAlert.java"


# instance fields
.field private attachments:I

.field private body:Ljava/lang/String;

.field private subject:Ljava/lang/String;


# direct methods
.method public constructor <init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "id"    # J
    .param p3, "timeStamp"    # J
    .param p5, "address"    # Ljava/lang/String;
    .param p6, "senderDisplayName"    # Ljava/lang/String;
    .param p7, "body"    # Ljava/lang/String;
    .param p8, "subject"    # Ljava/lang/String;
    .param p9, "type"    # Ljava/lang/String;

    .prologue
    .line 43
    move-object v1, p0

    move-wide v2, p1

    move-object v4, p5

    move-object v5, p6

    move-wide v6, p3

    move-object/from16 v8, p9

    invoke-direct/range {v1 .. v8}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;-><init>(JLjava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 17
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->body:Ljava/lang/String;

    .line 18
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->subject:Ljava/lang/String;

    .line 19
    const/4 v1, 0x0

    iput v1, p0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->attachments:I

    .line 44
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->body:Ljava/lang/String;

    .line 45
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->subject:Ljava/lang/String;

    .line 46
    return-void
.end method


# virtual methods
.method public getAttachments()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->attachments:I

    return v0
.end method

.method public getBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->body:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayableMessageText(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getMessageText()Ljava/lang/String;

    move-result-object v0

    .line 71
    .local v0, "toReturn":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->isMMS()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 76
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_message_empty_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$string;)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 81
    :cond_0
    :goto_0
    return-object v0

    .line 78
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_message_empty_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$string;)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getMessageText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->body:Ljava/lang/String;

    .line 91
    .local v0, "toReturn":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->body:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 92
    iget-object v0, p0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->subject:Ljava/lang/String;

    .line 94
    :cond_0
    invoke-static {v0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 95
    const-string/jumbo v0, ""

    .line 97
    :cond_1
    return-object v0
.end method

.method public getSenderName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 138
    .local v0, "name":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139
    invoke-virtual {p0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 141
    :cond_0
    return-object v0
.end method

.method public getSubject()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->subject:Ljava/lang/String;

    return-object v0
.end method

.method public isMMS()Z
    .locals 2

    .prologue
    .line 53
    const-string/jumbo v0, "MMS"

    iget-object v1, p0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isSMS()Z
    .locals 2

    .prologue
    .line 60
    const-string/jumbo v0, "SMS"

    iget-object v1, p0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setAttachments(I)V
    .locals 0
    .param p1, "attachments"    # I

    .prologue
    .line 28
    iput p1, p0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->attachments:I

    .line 29
    return-void
.end method

.method public setBody(Ljava/lang/String;)V
    .locals 0
    .param p1, "body"    # Ljava/lang/String;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->body:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 0
    .param p1, "subject"    # Ljava/lang/String;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->subject:Ljava/lang/String;

    .line 129
    return-void
.end method
