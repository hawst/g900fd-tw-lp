.class public Lcom/vlingo/core/internal/VlingoAndroidCore;
.super Ljava/lang/Object;
.source "VlingoAndroidCore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/VlingoAndroidCore$1;,
        Lcom/vlingo/core/internal/VlingoAndroidCore$PreferenceChangeListener;
    }
.end annotation


# static fields
.field private static LOG_TAG:Ljava/lang/String;

.field private static coreTester:Lcom/vlingo/core/internal/dialogmanager/CoreTester;

.field private static preventSdkReload:Z

.field private static reloadWasRequested:Z

.field private static smAppId:Ljava/lang/String;

.field private static smAppName:Ljava/lang/String;

.field private static smAppVersion:Ljava/lang/String;

.field private static smApplicationContentProviderBase:Ljava/lang/String;

.field private static smCoreResourceProvider:Lcom/vlingo/core/internal/ResourceIdProvider;

.field private static smDialogOriginator:Ljava/lang/String;

.field private static smEndpointManager:Lcom/vlingo/core/internal/endpoints/EndpointManager;

.field private static smIsCarMode:Z

.field private static smPackageName:Ljava/lang/String;

.field private static smPhoneListener:Landroid/telephony/PhoneStateListener;

.field private static smPrefListener:Lcom/vlingo/core/internal/VlingoAndroidCore$PreferenceChangeListener;

.field private static smRecoMode:Lcom/vlingo/core/facade/RecognitionMode;

.field private static smReloadSDKOnIdle:Z

.field private static smSalesCode:Ljava/lang/String;

.field private static smServerInfo:Lcom/vlingo/core/internal/util/CoreServerInfo;

.field private static smTelephonyManager:Landroid/telephony/TelephonyManager;

.field private static useEmbeddedDialogManager:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 83
    const-string/jumbo v0, "VlingoAndroidCore"

    sput-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->LOG_TAG:Ljava/lang/String;

    .line 90
    sput-boolean v1, Lcom/vlingo/core/internal/VlingoAndroidCore;->smIsCarMode:Z

    .line 101
    sput-object v2, Lcom/vlingo/core/internal/VlingoAndroidCore;->smApplicationContentProviderBase:Ljava/lang/String;

    .line 102
    sget-object v0, Lcom/vlingo/core/facade/RecognitionMode;->CLOUD:Lcom/vlingo/core/facade/RecognitionMode;

    sput-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smRecoMode:Lcom/vlingo/core/facade/RecognitionMode;

    .line 103
    const/4 v0, 0x1

    sput-boolean v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->useEmbeddedDialogManager:Z

    .line 104
    sput-boolean v1, Lcom/vlingo/core/internal/VlingoAndroidCore;->preventSdkReload:Z

    .line 105
    sput-boolean v1, Lcom/vlingo/core/internal/VlingoAndroidCore;->reloadWasRequested:Z

    .line 106
    sput-object v2, Lcom/vlingo/core/internal/VlingoAndroidCore;->coreTester:Lcom/vlingo/core/internal/dialogmanager/CoreTester;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 517
    return-void
.end method

.method static synthetic access$000()V
    .locals 0

    .prologue
    .line 79
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->reloadSDK()V

    return-void
.end method

.method private static buildDebugSettings()Lcom/vlingo/sdk/util/SDKDebugSettings;
    .locals 15

    .prologue
    const/4 v14, 0x0

    const/4 v12, 0x0

    .line 335
    const/4 v2, 0x0

    .line 336
    .local v2, "debugSettings":Lcom/vlingo/sdk/util/SDKDebugSettings;
    const-string/jumbo v13, "FAKE_LAT_LONG"

    invoke-static {v13, v12}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 337
    .local v4, "fakeLocation":Z
    const-string/jumbo v13, "FORCE_NON_DM"

    invoke-static {v13, v12}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 338
    .local v5, "forceNonDM":Z
    const-string/jumbo v13, "audiofilelog_enabled"

    invoke-static {v13, v12}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    .line 339
    .local v11, "waveformLogging":Z
    const-string/jumbo v13, "driving_mode_audio_files"

    invoke-static {v13, v12}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v13

    if-eqz v13, :cond_6

    const-string/jumbo v13, "driving_mode_on"

    invoke-static {v13, v12}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v13

    if-eqz v13, :cond_6

    const/4 v9, 0x1

    .line 340
    .local v9, "saveAudioFiles":Z
    :goto_0
    const-string/jumbo v13, "FAKE_DEVICE_MODEL"

    invoke-static {v13, v12}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 341
    .local v3, "fakeDeviceModel":Z
    const-string/jumbo v12, "SERVER_RESONSE_LOGGGING"

    sget-object v13, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->NONE:Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    invoke-virtual {v13}, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->getCode()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->get(Ljava/lang/String;)Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    move-result-object v10

    .line 343
    .local v10, "serverLoggingState":Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;
    if-nez v11, :cond_0

    if-nez v4, :cond_0

    if-nez v5, :cond_0

    if-nez v3, :cond_0

    if-nez v9, :cond_0

    sget-object v12, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->NONE:Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    if-eq v10, v12, :cond_5

    .line 345
    :cond_0
    new-instance v2, Lcom/vlingo/sdk/util/SDKDebugSettings;

    .end local v2    # "debugSettings":Lcom/vlingo/sdk/util/SDKDebugSettings;
    invoke-direct {v2}, Lcom/vlingo/sdk/util/SDKDebugSettings;-><init>()V

    .line 346
    .restart local v2    # "debugSettings":Lcom/vlingo/sdk/util/SDKDebugSettings;
    const-string/jumbo v12, "CARRIER_COUNTRY"

    invoke-static {v12, v14}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 347
    .local v1, "carrierCountry":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 348
    invoke-virtual {v2, v1}, Lcom/vlingo/sdk/util/SDKDebugSettings;->setCarrierCountry(Ljava/lang/String;)V

    .line 350
    :cond_1
    const-string/jumbo v12, "CARRIER"

    invoke-static {v12, v14}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 351
    .local v0, "carrier":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 352
    invoke-virtual {v2, v0}, Lcom/vlingo/sdk/util/SDKDebugSettings;->setCarrier(Ljava/lang/String;)V

    .line 354
    :cond_2
    const-string/jumbo v12, "FAKE_LAT"

    invoke-static {v12, v14}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 355
    .local v6, "lat":Ljava/lang/String;
    const-string/jumbo v12, "FAKE_LONG"

    invoke-static {v12, v14}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 356
    .local v7, "lng":Ljava/lang/String;
    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_3

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_3

    .line 357
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "Lat="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, ";Long="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, ";Alt=0.0"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 360
    .local v8, "locationString":Ljava/lang/String;
    invoke-virtual {v2, v8}, Lcom/vlingo/sdk/util/SDKDebugSettings;->setLocation(Ljava/lang/String;)V

    .line 362
    .end local v8    # "locationString":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2, v5}, Lcom/vlingo/sdk/util/SDKDebugSettings;->setForceNonDM(Z)V

    .line 364
    sget-object v12, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->NONE:Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    if-eq v10, v12, :cond_4

    .line 365
    invoke-virtual {v2, v10}, Lcom/vlingo/sdk/util/SDKDebugSettings;->setServerResponseLoggingState(Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;)V

    .line 366
    const-string/jumbo v12, "SERVER_RESONSE_FILE"

    const-string/jumbo v13, "serverReponseFile"

    invoke-static {v12, v13}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v12}, Lcom/vlingo/sdk/util/SDKDebugSettings;->setmRawServerLogBase(Ljava/lang/String;)V

    .line 369
    :cond_4
    invoke-virtual {v2, v11}, Lcom/vlingo/sdk/util/SDKDebugSettings;->setLogRecoWaveform(Z)V

    .line 370
    invoke-virtual {v2, v9}, Lcom/vlingo/sdk/util/SDKDebugSettings;->setSaveAudioFiles(Z)V

    .line 372
    if-eqz v3, :cond_5

    .line 373
    const-string/jumbo v12, "DEVICE_MODEL"

    invoke-static {v12, v14}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v12}, Lcom/vlingo/sdk/util/SDKDebugSettings;->setModelNumber(Ljava/lang/String;)V

    .line 376
    .end local v0    # "carrier":Ljava/lang/String;
    .end local v1    # "carrierCountry":Ljava/lang/String;
    .end local v6    # "lat":Ljava/lang/String;
    .end local v7    # "lng":Ljava/lang/String;
    :cond_5
    return-object v2

    .end local v3    # "fakeDeviceModel":Z
    .end local v9    # "saveAudioFiles":Z
    .end local v10    # "serverLoggingState":Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;
    :cond_6
    move v9, v12

    .line 339
    goto/16 :goto_0
.end method

.method public static deleteAllPersonalData(ZLcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;)V
    .locals 0
    .param p0, "deleteLocalData"    # Z
    .param p1, "callback"    # Lcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;

    .prologue
    .line 468
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/userdata/UserDataManager;->deleteUserDataOnServer(ZLcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;)V

    .line 469
    return-void
.end method

.method public static deleteLocalApplicationData()V
    .locals 0

    .prologue
    .line 478
    invoke-static {}, Lcom/vlingo/core/internal/userdata/UserDataManager;->clearLocalData()V

    .line 479
    return-void
.end method

.method public static destroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 276
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smPrefListener:Lcom/vlingo/core/internal/VlingoAndroidCore$PreferenceChangeListener;

    if-eqz v0, :cond_0

    .line 277
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/VlingoAndroidCore;->smPrefListener:Lcom/vlingo/core/internal/VlingoAndroidCore$PreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 278
    sput-object v2, Lcom/vlingo/core/internal/VlingoAndroidCore;->smPrefListener:Lcom/vlingo/core/internal/VlingoAndroidCore$PreferenceChangeListener;

    .line 280
    :cond_0
    sput-boolean v3, Lcom/vlingo/core/internal/VlingoAndroidCore;->reloadWasRequested:Z

    .line 281
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->destroy()V

    .line 282
    invoke-static {}, Lcom/vlingo/core/internal/audio/RemoteControlManager;->getInstance()Lcom/vlingo/core/internal/audio/RemoteControlManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/RemoteControlManager;->destroy()V

    .line 283
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/TTSEngine;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/TTSEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/TTSEngine;->destroy()V

    .line 284
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->deinit()V

    .line 285
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->deinit()V

    .line 286
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smPhoneListener:Landroid/telephony/PhoneStateListener;

    check-cast v0, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;->clearListeners()V

    .line 287
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smTelephonyManager:Landroid/telephony/TelephonyManager;

    sget-object v1, Lcom/vlingo/core/internal/VlingoAndroidCore;->smPhoneListener:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v0, v1, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 288
    sput-object v2, Lcom/vlingo/core/internal/VlingoAndroidCore;->smPhoneListener:Landroid/telephony/PhoneStateListener;

    .line 289
    sput-object v2, Lcom/vlingo/core/internal/VlingoAndroidCore;->smTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 290
    return-void
.end method

.method public static getApplicationContentProviderBase()Ljava/lang/String;
    .locals 1

    .prologue
    .line 678
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smApplicationContentProviderBase:Ljava/lang/String;

    return-object v0
.end method

.method public static getDialogOriginator()Ljava/lang/String;
    .locals 1

    .prologue
    .line 628
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smDialogOriginator:Ljava/lang/String;

    return-object v0
.end method

.method public static getEndpointManager()Lcom/vlingo/core/internal/endpoints/EndpointManager;
    .locals 1

    .prologue
    .line 297
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smEndpointManager:Lcom/vlingo/core/internal/endpoints/EndpointManager;

    return-object v0
.end method

.method public static getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .locals 1
    .param p0, "fieldId"    # Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    .prologue
    .line 314
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smEndpointManager:Lcom/vlingo/core/internal/endpoints/EndpointManager;

    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/endpoints/EndpointManager;->getFieldId(Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    return-object v0
.end method

.method public static getFieldId(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .locals 1
    .param p0, "fieldIdString"    # Ljava/lang/String;

    .prologue
    .line 322
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smEndpointManager:Lcom/vlingo/core/internal/endpoints/EndpointManager;

    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/endpoints/EndpointManager;->getFieldId(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    return-object v0
.end method

.method public static getPhoneStateListener()Landroid/telephony/PhoneStateListener;
    .locals 1

    .prologue
    .line 636
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smPhoneListener:Landroid/telephony/PhoneStateListener;

    return-object v0
.end method

.method public static getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;
    .locals 1

    .prologue
    .line 293
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smCoreResourceProvider:Lcom/vlingo/core/internal/ResourceIdProvider;

    return-object v0
.end method

.method public static getRevision()Ljava/lang/String;
    .locals 1

    .prologue
    .line 674
    const-string/jumbo v0, "13002"

    return-object v0
.end method

.method public static getSDKRecoMode()Lcom/vlingo/sdk/recognition/RecognitionMode;
    .locals 2

    .prologue
    .line 658
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore$1;->$SwitchMap$com$vlingo$core$facade$RecognitionMode:[I

    sget-object v1, Lcom/vlingo/core/internal/VlingoAndroidCore;->smRecoMode:Lcom/vlingo/core/facade/RecognitionMode;

    invoke-virtual {v1}, Lcom/vlingo/core/facade/RecognitionMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 662
    sget-object v0, Lcom/vlingo/sdk/recognition/RecognitionMode;->HYBRID:Lcom/vlingo/sdk/recognition/RecognitionMode;

    :goto_0
    return-object v0

    .line 659
    :pswitch_0
    sget-object v0, Lcom/vlingo/sdk/recognition/RecognitionMode;->HYBRID:Lcom/vlingo/sdk/recognition/RecognitionMode;

    goto :goto_0

    .line 660
    :pswitch_1
    sget-object v0, Lcom/vlingo/sdk/recognition/RecognitionMode;->CLOUD:Lcom/vlingo/sdk/recognition/RecognitionMode;

    goto :goto_0

    .line 661
    :pswitch_2
    sget-object v0, Lcom/vlingo/sdk/recognition/RecognitionMode;->EMBEDDED:Lcom/vlingo/sdk/recognition/RecognitionMode;

    goto :goto_0

    .line 658
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getSalesCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 326
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smSalesCode:Ljava/lang/String;

    return-object v0
.end method

.method public static getServerInfo()Lcom/vlingo/core/internal/util/CoreServerInfo;
    .locals 1

    .prologue
    .line 261
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smServerInfo:Lcom/vlingo/core/internal/util/CoreServerInfo;

    return-object v0
.end method

.method public static getSmEndpointManager()Lcom/vlingo/core/internal/endpoints/EndpointManager;
    .locals 1

    .prologue
    .line 318
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smEndpointManager:Lcom/vlingo/core/internal/endpoints/EndpointManager;

    return-object v0
.end method

.method public static getTermsManager()Lcom/vlingo/core/internal/terms/TermsManager;
    .locals 1

    .prologue
    .line 310
    invoke-static {}, Lcom/vlingo/core/internal/terms/TermsManager;->getInstance()Lcom/vlingo/core/internal/terms/TermsManager;

    move-result-object v0

    return-object v0
.end method

.method public static init(Landroid/content/Context;Lcom/vlingo/core/internal/ResourceIdProvider;Lcom/vlingo/core/internal/util/VlingoApplicationInterface;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/util/CoreServerInfo;Lcom/vlingo/core/facade/RecognitionMode;Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;Ljava/lang/String;Ljava/util/Map;Z)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resourceProvider"    # Lcom/vlingo/core/internal/ResourceIdProvider;
    .param p2, "vai"    # Lcom/vlingo/core/internal/util/VlingoApplicationInterface;
    .param p3, "appId"    # Ljava/lang/String;
    .param p4, "appName"    # Ljava/lang/String;
    .param p5, "appVersion"    # Ljava/lang/String;
    .param p6, "salesCode"    # Ljava/lang/String;
    .param p7, "serverInfo"    # Lcom/vlingo/core/internal/util/CoreServerInfo;
    .param p8, "recoMode"    # Lcom/vlingo/core/facade/RecognitionMode;
    .param p9, "factory"    # Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;
    .param p10, "applicationContentProviderBase"    # Ljava/lang/String;
    .param p12, "useEDM"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vlingo/core/internal/ResourceIdProvider;",
            "Lcom/vlingo/core/internal/util/VlingoApplicationInterface;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/util/CoreServerInfo;",
            "Lcom/vlingo/core/facade/RecognitionMode;",
            "Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;",
            "Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 128
    .local p11, "fieldIds":Ljava/util/Map;, "Ljava/util/Map<Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;>;"
    const/4 v9, 0x0

    .line 129
    .local v9, "dialogOriginator":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->init(Landroid/content/Context;)V

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move/from16 v13, p12

    .line 130
    invoke-static/range {v0 .. v13}, Lcom/vlingo/core/internal/VlingoAndroidCore;->init(Landroid/content/Context;Lcom/vlingo/core/internal/ResourceIdProvider;Lcom/vlingo/core/internal/util/VlingoApplicationInterface;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/util/CoreServerInfo;Lcom/vlingo/core/facade/RecognitionMode;Ljava/lang/String;Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;Ljava/lang/String;Ljava/util/Map;Z)V

    .line 132
    return-void
.end method

.method public static init(Landroid/content/Context;Lcom/vlingo/core/internal/ResourceIdProvider;Lcom/vlingo/core/internal/util/VlingoApplicationInterface;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/util/CoreServerInfo;Lcom/vlingo/core/facade/RecognitionMode;Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;Ljava/util/Map;Z)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resourceProvider"    # Lcom/vlingo/core/internal/ResourceIdProvider;
    .param p2, "vai"    # Lcom/vlingo/core/internal/util/VlingoApplicationInterface;
    .param p3, "appId"    # Ljava/lang/String;
    .param p4, "appName"    # Ljava/lang/String;
    .param p5, "appVersion"    # Ljava/lang/String;
    .param p6, "salesCode"    # Ljava/lang/String;
    .param p7, "serverInfo"    # Lcom/vlingo/core/internal/util/CoreServerInfo;
    .param p8, "recoMode"    # Lcom/vlingo/core/facade/RecognitionMode;
    .param p9, "factory"    # Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;
    .param p11, "useEDM"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vlingo/core/internal/ResourceIdProvider;",
            "Lcom/vlingo/core/internal/util/VlingoApplicationInterface;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/util/CoreServerInfo;",
            "Lcom/vlingo/core/facade/RecognitionMode;",
            "Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;",
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;",
            "Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 115
    .local p10, "fieldIds":Ljava/util/Map;, "Ljava/util/Map<Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;>;"
    const/4 v9, 0x0

    .line 116
    .local v9, "dialogOriginator":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->init(Landroid/content/Context;)V

    .line 117
    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v10, p9

    move-object/from16 v12, p10

    move/from16 v13, p11

    invoke-static/range {v0 .. v13}, Lcom/vlingo/core/internal/VlingoAndroidCore;->init(Landroid/content/Context;Lcom/vlingo/core/internal/ResourceIdProvider;Lcom/vlingo/core/internal/util/VlingoApplicationInterface;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/util/CoreServerInfo;Lcom/vlingo/core/facade/RecognitionMode;Ljava/lang/String;Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;Ljava/lang/String;Ljava/util/Map;Z)V

    .line 119
    return-void
.end method

.method private static init(Landroid/content/Context;Lcom/vlingo/core/internal/ResourceIdProvider;Lcom/vlingo/core/internal/util/VlingoApplicationInterface;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/util/CoreServerInfo;Lcom/vlingo/core/facade/RecognitionMode;Ljava/lang/String;Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;Ljava/lang/String;Ljava/util/Map;Z)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resourceProvider"    # Lcom/vlingo/core/internal/ResourceIdProvider;
    .param p2, "vai"    # Lcom/vlingo/core/internal/util/VlingoApplicationInterface;
    .param p3, "appId"    # Ljava/lang/String;
    .param p4, "appName"    # Ljava/lang/String;
    .param p5, "appVersion"    # Ljava/lang/String;
    .param p6, "salesCode"    # Ljava/lang/String;
    .param p7, "serverInfo"    # Lcom/vlingo/core/internal/util/CoreServerInfo;
    .param p8, "recoMode"    # Lcom/vlingo/core/facade/RecognitionMode;
    .param p9, "dialogOriginator"    # Ljava/lang/String;
    .param p10, "factory"    # Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;
    .param p11, "applicationContentProviderBase"    # Ljava/lang/String;
    .param p13, "useEDM"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vlingo/core/internal/ResourceIdProvider;",
            "Lcom/vlingo/core/internal/util/VlingoApplicationInterface;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/util/CoreServerInfo;",
            "Lcom/vlingo/core/facade/RecognitionMode;",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;",
            "Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 554
    .local p12, "fieldIds":Ljava/util/Map;, "Ljava/util/Map<Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;>;"
    sget-object v2, Lcom/vlingo/core/internal/VlingoAndroidCore;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "AppName: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", Core: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "2.5.0.13002"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", SDK: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/vlingo/sdk/VLSdk;->VERSION:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", EDM:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "13002"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    const-string/jumbo v2, "phone"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    sput-object v2, Lcom/vlingo/core/internal/VlingoAndroidCore;->smTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 556
    new-instance v2, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

    invoke-direct {v2}, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;-><init>()V

    sput-object v2, Lcom/vlingo/core/internal/VlingoAndroidCore;->smPhoneListener:Landroid/telephony/PhoneStateListener;

    .line 557
    sget-object v2, Lcom/vlingo/core/internal/VlingoAndroidCore;->smTelephonyManager:Landroid/telephony/TelephonyManager;

    sget-object v3, Lcom/vlingo/core/internal/VlingoAndroidCore;->smPhoneListener:Landroid/telephony/PhoneStateListener;

    const/16 v4, 0x20

    invoke-virtual {v2, v3, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 558
    sput-object p1, Lcom/vlingo/core/internal/VlingoAndroidCore;->smCoreResourceProvider:Lcom/vlingo/core/internal/ResourceIdProvider;

    .line 559
    const/4 v2, 0x0

    sput-boolean v2, Lcom/vlingo/core/internal/VlingoAndroidCore;->smReloadSDKOnIdle:Z

    .line 560
    sput-object p9, Lcom/vlingo/core/internal/VlingoAndroidCore;->smDialogOriginator:Ljava/lang/String;

    .line 562
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/vlingo/core/internal/VlingoAndroidCore;->smPackageName:Ljava/lang/String;

    .line 563
    const/4 v2, 0x0

    sput-boolean v2, Lcom/vlingo/core/internal/VlingoAndroidCore;->smIsCarMode:Z

    .line 564
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getCarModePackageName()Ljava/lang/String;

    move-result-object v0

    .line 565
    .local v0, "carModePackageName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 566
    sget-object v2, Lcom/vlingo/core/internal/VlingoAndroidCore;->smPackageName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    sput-boolean v2, Lcom/vlingo/core/internal/VlingoAndroidCore;->smIsCarMode:Z

    .line 568
    :cond_0
    sput-object p3, Lcom/vlingo/core/internal/VlingoAndroidCore;->smAppId:Ljava/lang/String;

    .line 569
    sput-object p4, Lcom/vlingo/core/internal/VlingoAndroidCore;->smAppName:Ljava/lang/String;

    .line 570
    sput-object p5, Lcom/vlingo/core/internal/VlingoAndroidCore;->smAppVersion:Ljava/lang/String;

    .line 571
    sput-object p6, Lcom/vlingo/core/internal/VlingoAndroidCore;->smSalesCode:Ljava/lang/String;

    .line 573
    sput-object p7, Lcom/vlingo/core/internal/VlingoAndroidCore;->smServerInfo:Lcom/vlingo/core/internal/util/CoreServerInfo;

    .line 574
    sput-object p11, Lcom/vlingo/core/internal/VlingoAndroidCore;->smApplicationContentProviderBase:Ljava/lang/String;

    .line 575
    sput-object p8, Lcom/vlingo/core/internal/VlingoAndroidCore;->smRecoMode:Lcom/vlingo/core/facade/RecognitionMode;

    .line 576
    sget-object v2, Lcom/vlingo/core/internal/VlingoAndroidCore;->smRecoMode:Lcom/vlingo/core/facade/RecognitionMode;

    if-nez v2, :cond_1

    .line 579
    sget-object v2, Lcom/vlingo/core/facade/RecognitionMode;->CLOUD:Lcom/vlingo/core/facade/RecognitionMode;

    sput-object v2, Lcom/vlingo/core/internal/VlingoAndroidCore;->smRecoMode:Lcom/vlingo/core/facade/RecognitionMode;

    .line 581
    :cond_1
    sput-boolean p13, Lcom/vlingo/core/internal/VlingoAndroidCore;->useEmbeddedDialogManager:Z

    .line 583
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->timeApplicationStart()V

    .line 584
    invoke-static {p0}, Lcom/vlingo/core/internal/settings/Settings;->init(Landroid/content/Context;)V

    .line 586
    const/4 v1, 0x1

    .line 587
    .local v1, "preConnectService":Z
    const-string/jumbo v2, "tos_accepted"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 588
    invoke-static {p0, v1}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->init(Landroid/content/Context;Z)V

    .line 590
    :cond_2
    const-class v2, Lcom/vlingo/core/internal/contacts/mru/MRUDatabaseStore;

    invoke-static {v2}, Lcom/vlingo/core/internal/contacts/mru/MRU;->setMRUStoreImpl(Ljava/lang/Class;)V

    .line 611
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->setInterface(Lcom/vlingo/core/internal/util/VlingoApplicationInterface;)V

    .line 613
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/vlingo/core/internal/VlingoAndroidCore;->initSdk(Z)V

    .line 615
    new-instance v2, Lcom/vlingo/core/internal/VlingoAndroidCore$PreferenceChangeListener;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/vlingo/core/internal/VlingoAndroidCore$PreferenceChangeListener;-><init>(Lcom/vlingo/core/internal/VlingoAndroidCore$1;)V

    sput-object v2, Lcom/vlingo/core/internal/VlingoAndroidCore;->smPrefListener:Lcom/vlingo/core/internal/VlingoAndroidCore$PreferenceChangeListener;

    .line 617
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/VlingoAndroidCore;->smPrefListener:Lcom/vlingo/core/internal/VlingoAndroidCore$PreferenceChangeListener;

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 618
    if-nez p12, :cond_3

    .line 619
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->generateFieldIdsMap()Ljava/util/Map;

    move-result-object p12

    .line 622
    :cond_3
    invoke-static/range {p12 .. p12}, Lcom/vlingo/core/internal/endpoints/EndpointManager;->getInstance(Ljava/util/Map;)Lcom/vlingo/core/internal/endpoints/EndpointManager;

    move-result-object v2

    sput-object v2, Lcom/vlingo/core/internal/VlingoAndroidCore;->smEndpointManager:Lcom/vlingo/core/internal/endpoints/EndpointManager;

    .line 624
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->getInstance()Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->onCoreInitialization()V

    .line 625
    return-void
.end method

.method public static init(Landroid/content/Context;Lcom/vlingo/core/internal/associatedservice/BResourceIdProvider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/util/CoreServerInfo;Lcom/vlingo/core/facade/RecognitionMode;Ljava/lang/String;Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;Ljava/util/Map;Z)V
    .locals 21
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resourceIdProvider"    # Lcom/vlingo/core/internal/associatedservice/BResourceIdProvider;
    .param p2, "applicationContentProviderBase"    # Ljava/lang/String;
    .param p3, "remoteDeviceModelName"    # Ljava/lang/String;
    .param p4, "dialogOriginator"    # Ljava/lang/String;
    .param p5, "serverInfo"    # Lcom/vlingo/core/internal/util/CoreServerInfo;
    .param p6, "recoMode"    # Lcom/vlingo/core/facade/RecognitionMode;
    .param p7, "serviceVersionNumber"    # Ljava/lang/String;
    .param p8, "factory"    # Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;
    .param p10, "useEDM"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vlingo/core/internal/associatedservice/BResourceIdProvider;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/util/CoreServerInfo;",
            "Lcom/vlingo/core/facade/RecognitionMode;",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;",
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;",
            "Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 167
    .local p9, "fieldIds":Ljava/util/Map;, "Ljava/util/Map<Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;Lcom/vlingo/core/internal/dialogmanager/EndpointDetectionDuration;>;"
    if-nez p0, :cond_0

    .line 168
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "context cannot be null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 170
    :cond_0
    if-nez p1, :cond_1

    .line 171
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "resourceIdProvider cannot be null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 173
    :cond_1
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 174
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "applicationContentProviderBase cannot be null or empty"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 176
    :cond_2
    invoke-static/range {p4 .. p4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 177
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "dialogOriginator cannot be null or empty"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 180
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->init(Landroid/content/Context;)V

    .line 182
    invoke-static {}, Lcom/vlingo/core/internal/associatedservice/ApplicationInterfaceImpl;->getInstance()Lcom/vlingo/core/internal/associatedservice/ApplicationInterfaceImpl;

    move-result-object v5

    .line 183
    .local v5, "vai":Lcom/vlingo/core/internal/util/VlingoApplicationInterface;
    new-instance v18, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 188
    .local v18, "applicationQueryManager":Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;
    const-string/jumbo v3, "SETTINGS/tos_accepted"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 189
    .local v20, "tosAcceptedStringValue":Ljava/lang/String;
    if-nez v20, :cond_4

    .line 190
    const-string/jumbo v19, "applicationQueryManager returned null, ConfigProvider not ready?"

    .line 193
    .local v19, "msg":Ljava/lang/String;
    new-instance v3, Ljava/lang/IllegalStateException;

    move-object/from16 v0, v19

    invoke-direct {v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 195
    .end local v19    # "msg":Ljava/lang/String;
    :cond_4
    const-string/jumbo v3, "true"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 196
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "TOS not yet accepted, value="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 199
    .restart local v19    # "msg":Ljava/lang/String;
    new-instance v3, Ljava/lang/IllegalStateException;

    move-object/from16 v0, v19

    invoke-direct {v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 205
    .end local v19    # "msg":Ljava/lang/String;
    :cond_5
    const-string/jumbo v3, "app_id"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 206
    .local v6, "appId":Ljava/lang/String;
    const-string/jumbo v3, "app_name"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 207
    .local v7, "appName":Ljava/lang/String;
    const-string/jumbo v3, "app_version"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 208
    .local v8, "appVersion":Ljava/lang/String;
    invoke-static {v8}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    const/16 v3, 0x2e

    invoke-virtual {v8, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    if-lez v3, :cond_9

    .line 209
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x0

    const/16 v10, 0x2e

    invoke-virtual {v8, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v8, v4, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p7

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 216
    const-string/jumbo v3, "sales_code"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 218
    .local v9, "salesCode":Ljava/lang/String;
    if-eqz p3, :cond_6

    .line 219
    invoke-static/range {p3 .. p3}, Lcom/vlingo/core/internal/VlingoAndroidCore;->setRemoteDeviceModelName(Ljava/lang/String;)V

    :cond_6
    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    move-object/from16 v12, p4

    move-object/from16 v13, p8

    move-object/from16 v14, p2

    move-object/from16 v15, p9

    move/from16 v16, p10

    .line 222
    invoke-static/range {v3 .. v16}, Lcom/vlingo/core/internal/VlingoAndroidCore;->init(Landroid/content/Context;Lcom/vlingo/core/internal/ResourceIdProvider;Lcom/vlingo/core/internal/util/VlingoApplicationInterface;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/util/CoreServerInfo;Lcom/vlingo/core/facade/RecognitionMode;Ljava/lang/String;Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;Ljava/lang/String;Ljava/util/Map;Z)V

    .line 227
    const-string/jumbo v3, "language"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 234
    .local v17, "appLanguage":Ljava/lang/String;
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->CANCEL:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/CancelActionHandler;

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 235
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->LISTEN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ListenHandler;

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 236
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->DIALOG_CANCEL:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/DialogCancelHandler;

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 237
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->RESOLVE_CONTACT:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandler;

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 238
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SET_CONFIG:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SetConfigHandler;

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 239
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SET_PARAMS:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SetParamsHandler;

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 240
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SET_TURN_PARAMS:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SetTurnParamsHandler;

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 241
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SETTING_CHANGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/SettingChangeHandler;

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 242
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_SYSTEM_TURN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ShowSystemTurnHandler;

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 243
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_USER_TURN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ShowUserTurnHandler;

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 244
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SPEAK_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v4, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SpeakMessageHandler;

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 245
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->SHOW_WEB_SEARCH_BUTTON:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v4, Lcom/vlingo/core/internal/dialogmanager/WebSearchButtonHandler;

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 247
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->setupStandardMappings()V

    .line 249
    const-string/jumbo v3, "hello_request_complete"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_7

    .line 251
    invoke-static {}, Lcom/vlingo/core/internal/util/SayHello;->sendHello()Z

    .line 255
    :cond_7
    if-eqz p8, :cond_8

    .line 256
    new-instance v3, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer;

    move-object/from16 v0, p8

    invoke-direct {v3, v0}, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer;-><init>(Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)V

    .line 258
    :cond_8
    return-void

    .line 213
    .end local v9    # "salesCode":Ljava/lang/String;
    .end local v17    # "appLanguage":Ljava/lang/String;
    :cond_9
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "AppVersion cannot be null or empty and should contain SVN version"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method private static initSdk(Z)V
    .locals 14
    .param p0, "reload"    # Z

    .prologue
    .line 420
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->buildDebugSettings()Lcom/vlingo/sdk/util/SDKDebugSettings;

    move-result-object v11

    .line 421
    .local v11, "debugSettings":Lcom/vlingo/sdk/util/SDKDebugSettings;
    const-string/jumbo v0, "USE_EDM"

    sget-boolean v1, Lcom/vlingo/core/internal/VlingoAndroidCore;->useEmbeddedDialogManager:Z

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v12

    .line 424
    .local v12, "useEDM":Z
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smServerInfo:Lcom/vlingo/core/internal/util/CoreServerInfo;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils;->getAsrUri(Lcom/vlingo/core/internal/util/CoreServerInfo;)Ljava/lang/String;

    move-result-object v6

    .line 425
    .local v6, "asrUri":Ljava/lang/String;
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smServerInfo:Lcom/vlingo/core/internal/util/CoreServerInfo;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils;->getTtsUri(Lcom/vlingo/core/internal/util/CoreServerInfo;)Ljava/lang/String;

    move-result-object v7

    .line 426
    .local v7, "ttsUri":Ljava/lang/String;
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smServerInfo:Lcom/vlingo/core/internal/util/CoreServerInfo;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils;->getVcsUri(Lcom/vlingo/core/internal/util/CoreServerInfo;)Ljava/lang/String;

    move-result-object v13

    .line 427
    .local v13, "vcsUri":Ljava/lang/String;
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smServerInfo:Lcom/vlingo/core/internal/util/CoreServerInfo;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils;->getLogUri(Lcom/vlingo/core/internal/util/CoreServerInfo;)Ljava/lang/String;

    move-result-object v8

    .line 428
    .local v8, "logUri":Ljava/lang/String;
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smServerInfo:Lcom/vlingo/core/internal/util/CoreServerInfo;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils;->getHelloUri(Lcom/vlingo/core/internal/util/CoreServerInfo;)Ljava/lang/String;

    move-result-object v9

    .line 429
    .local v9, "helloUri":Ljava/lang/String;
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smServerInfo:Lcom/vlingo/core/internal/util/CoreServerInfo;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils;->getLMTTUri(Lcom/vlingo/core/internal/util/CoreServerInfo;)Ljava/lang/String;

    move-result-object v10

    .line 430
    .local v10, "lmttUri":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->isLocationEnabled()Z

    move-result v5

    .line 433
    .local v5, "locationEnabled":Z
    invoke-static {v13}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->setVcsUri(Ljava/lang/String;)V

    .line 434
    invoke-static {}, Lcom/vlingo/sdk/internal/audio/AudioDevice;->getInstance()Lcom/vlingo/sdk/internal/audio/AudioDevice;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getClientAudioValues()Lcom/vlingo/core/facade/audio/ClientAudioValues;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/audio/AudioDevice;->setClientAudioValues(Lcom/vlingo/sdk/internal/audio/IClientAudioValues;)V

    .line 436
    if-eqz p0, :cond_0

    .line 437
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/VlingoAndroidCore;->smAppId:Ljava/lang/String;

    sget-object v2, Lcom/vlingo/core/internal/VlingoAndroidCore;->smAppName:Ljava/lang/String;

    sget-object v3, Lcom/vlingo/core/internal/VlingoAndroidCore;->smAppVersion:Ljava/lang/String;

    sget-object v4, Lcom/vlingo/core/internal/VlingoAndroidCore;->smSalesCode:Ljava/lang/String;

    invoke-static/range {v0 .. v12}, Lcom/vlingo/sdk/VLSdk;->reload(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/util/SDKDebugSettings;Z)Lcom/vlingo/sdk/VLSdk;

    .line 443
    :goto_0
    return-void

    .line 440
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/VlingoAndroidCore;->smAppId:Ljava/lang/String;

    sget-object v2, Lcom/vlingo/core/internal/VlingoAndroidCore;->smAppName:Ljava/lang/String;

    sget-object v3, Lcom/vlingo/core/internal/VlingoAndroidCore;->smAppVersion:Ljava/lang/String;

    sget-object v4, Lcom/vlingo/core/internal/VlingoAndroidCore;->smSalesCode:Ljava/lang/String;

    invoke-static/range {v0 .. v12}, Lcom/vlingo/sdk/VLSdk;->create(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/util/SDKDebugSettings;Z)Lcom/vlingo/sdk/VLSdk;

    goto :goto_0
.end method

.method public static isAssociatedService()Z
    .locals 1

    .prologue
    .line 646
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smDialogOriginator:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isBMode()Z
    .locals 1

    .prologue
    .line 650
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smDialogOriginator:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isCarMode()Z
    .locals 1

    .prologue
    .line 654
    sget-boolean v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smIsCarMode:Z

    return v0
.end method

.method public static isChinesePhone()Z
    .locals 1

    .prologue
    .line 330
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isChinesePhone()Z

    move-result v0

    return v0
.end method

.method public static onDialogIdle()V
    .locals 1

    .prologue
    .line 413
    sget-boolean v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smReloadSDKOnIdle:Z

    if-eqz v0, :cond_0

    .line 414
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/core/internal/VlingoAndroidCore;->reloadSdkIfNeeded(Z)V

    .line 416
    :cond_0
    return-void
.end method

.method public static declared-synchronized preventSdkReload(Z)V
    .locals 2
    .param p0, "preventReload"    # Z

    .prologue
    .line 667
    const-class v1, Lcom/vlingo/core/internal/VlingoAndroidCore;

    monitor-enter v1

    :try_start_0
    sput-boolean p0, Lcom/vlingo/core/internal/VlingoAndroidCore;->preventSdkReload:Z

    .line 668
    if-nez p0, :cond_0

    .line 669
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/vlingo/core/internal/VlingoAndroidCore;->reloadSdkIfNeeded(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 671
    :cond_0
    monitor-exit v1

    return-void

    .line 667
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static regBackgroundImage(Landroid/content/Context;Landroid/view/ViewGroup;ILjava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "layout"    # Landroid/view/ViewGroup;
    .param p2, "backgroundid"    # I
    .param p3, "version"    # Ljava/lang/String;

    .prologue
    .line 506
    const-string/jumbo v3, "unwatermarked.versions"

    const-string/jumbo v4, ""

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 508
    .local v2, "unwatermarkedversions":Ljava/lang/String;
    invoke-virtual {v2, p3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 509
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 510
    .local v1, "bmp":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 511
    .local v0, "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    sget-object v3, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v4, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v3, v4}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 512
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 515
    .end local v0    # "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    .end local v1    # "bmp":Landroid/graphics/Bitmap;
    :cond_0
    return-void
.end method

.method private static reloadSDK()V
    .locals 1

    .prologue
    .line 446
    const/4 v0, 0x1

    sput-boolean v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->reloadWasRequested:Z

    .line 447
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/vlingo/core/internal/VlingoAndroidCore;->reloadSdkIfNeeded(Z)V

    .line 448
    return-void
.end method

.method private static reloadSdkIfNeeded(Z)V
    .locals 3
    .param p0, "isIdle"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 489
    sget-boolean v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->preventSdkReload:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->reloadWasRequested:Z

    if-nez v0, :cond_1

    .line 503
    :cond_0
    :goto_0
    return-void

    .line 493
    :cond_1
    if-nez p0, :cond_2

    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isIdle()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 494
    :cond_2
    sput-boolean v1, Lcom/vlingo/core/internal/VlingoAndroidCore;->smReloadSDKOnIdle:Z

    .line 495
    sget-boolean v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->reloadWasRequested:Z

    if-eqz v0, :cond_3

    .line 497
    invoke-static {v2}, Lcom/vlingo/core/internal/VlingoAndroidCore;->initSdk(Z)V

    .line 499
    :cond_3
    sput-boolean v1, Lcom/vlingo/core/internal/VlingoAndroidCore;->reloadWasRequested:Z

    goto :goto_0

    .line 501
    :cond_4
    sput-boolean v2, Lcom/vlingo/core/internal/VlingoAndroidCore;->smReloadSDKOnIdle:Z

    goto :goto_0
.end method

.method public static setDialogOriginator(Ljava/lang/String;)V
    .locals 0
    .param p0, "strOrg"    # Ljava/lang/String;

    .prologue
    .line 632
    sput-object p0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smDialogOriginator:Ljava/lang/String;

    .line 633
    return-void
.end method

.method public static setRemoteDeviceModelName(Ljava/lang/String;)V
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 135
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "name cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 139
    :cond_0
    const-string/jumbo v0, "FAKE_DEVICE_MODEL"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 140
    const-string/jumbo v0, "DEVICE_MODEL"

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    return-void
.end method

.method public static startAutomationPlayback(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/CoreTester$OnFinishedCallback;)V
    .locals 2
    .param p0, "inputFileName"    # Ljava/lang/String;
    .param p1, "callback"    # Lcom/vlingo/core/internal/dialogmanager/CoreTester$OnFinishedCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 396
    sget-object v1, Lcom/vlingo/core/internal/VlingoAndroidCore;->coreTester:Lcom/vlingo/core/internal/dialogmanager/CoreTester;

    if-nez v1, :cond_0

    .line 397
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    .line 398
    .local v0, "flow":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/CoreTester;

    invoke-direct {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/CoreTester;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V

    sput-object v1, Lcom/vlingo/core/internal/VlingoAndroidCore;->coreTester:Lcom/vlingo/core/internal/dialogmanager/CoreTester;

    .line 400
    .end local v0    # "flow":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    :cond_0
    sget-object v1, Lcom/vlingo/core/internal/VlingoAndroidCore;->coreTester:Lcom/vlingo/core/internal/dialogmanager/CoreTester;

    invoke-virtual {v1, p0, p1}, Lcom/vlingo/core/internal/dialogmanager/CoreTester;->startReplay(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/CoreTester$OnFinishedCallback;)V

    .line 401
    return-void
.end method

.method public static startAutomationRecording(Ljava/lang/String;)V
    .locals 2
    .param p0, "outputFileName"    # Ljava/lang/String;

    .prologue
    .line 380
    sget-object v1, Lcom/vlingo/core/internal/VlingoAndroidCore;->coreTester:Lcom/vlingo/core/internal/dialogmanager/CoreTester;

    if-nez v1, :cond_0

    .line 381
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    .line 382
    .local v0, "flow":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/CoreTester;

    invoke-direct {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/CoreTester;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V

    sput-object v1, Lcom/vlingo/core/internal/VlingoAndroidCore;->coreTester:Lcom/vlingo/core/internal/dialogmanager/CoreTester;

    .line 384
    .end local v0    # "flow":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    :cond_0
    sget-object v1, Lcom/vlingo/core/internal/VlingoAndroidCore;->coreTester:Lcom/vlingo/core/internal/dialogmanager/CoreTester;

    invoke-virtual {v1, p0}, Lcom/vlingo/core/internal/dialogmanager/CoreTester;->startRecording(Ljava/lang/String;)V

    .line 385
    return-void
.end method

.method public static stopAutomationPlayback()Z
    .locals 1

    .prologue
    .line 404
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->coreTester:Lcom/vlingo/core/internal/dialogmanager/CoreTester;

    if-nez v0, :cond_0

    .line 405
    const/4 v0, 0x0

    .line 409
    :goto_0
    return v0

    .line 407
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->coreTester:Lcom/vlingo/core/internal/dialogmanager/CoreTester;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/CoreTester;->stopReplay()V

    .line 408
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->coreTester:Lcom/vlingo/core/internal/dialogmanager/CoreTester;

    .line 409
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static stopAutomationRecording()V
    .locals 1

    .prologue
    .line 388
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->coreTester:Lcom/vlingo/core/internal/dialogmanager/CoreTester;

    if-nez v0, :cond_0

    .line 392
    :goto_0
    return-void

    .line 391
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/VlingoAndroidCore;->coreTester:Lcom/vlingo/core/internal/dialogmanager/CoreTester;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/CoreTester;->stopRecording()V

    goto :goto_0
.end method

.method public static updateServerInfo(Lcom/vlingo/core/internal/util/CoreServerInfo;)V
    .locals 0
    .param p0, "serverInfo"    # Lcom/vlingo/core/internal/util/CoreServerInfo;

    .prologue
    .line 265
    sput-object p0, Lcom/vlingo/core/internal/VlingoAndroidCore;->smServerInfo:Lcom/vlingo/core/internal/util/CoreServerInfo;

    .line 266
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->reloadSDK()V

    .line 267
    return-void
.end method

.method public static updateServerResponseLogging()V
    .locals 0

    .prologue
    .line 270
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->reloadSDK()V

    .line 271
    invoke-static {}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->resetResponseCount()V

    .line 272
    return-void
.end method
