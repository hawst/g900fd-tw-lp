.class Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;
.super Ljava/lang/Thread;
.source "ContactFetchAndSortRequestBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->sortContacts()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;->this$0:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 106
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;->this$0:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    # setter for: Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->hasStarted:Z
    invoke-static {v3, v4}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->access$002(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;Z)Z

    .line 111
    :try_start_0
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;->this$0:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    # getter for: Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->matches:Ljava/util/List;
    invoke-static {v3}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->access$100(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 112
    .local v0, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;->this$0:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    # getter for: Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->contactType:Lcom/vlingo/core/internal/contacts/ContactType;
    invoke-static {v3}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->access$200(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/ContactType;->getPreferredTarget()I

    move-result v3

    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;->this$0:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    # getter for: Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->requestedTypes:[I
    invoke-static {v4}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->access$300(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)[I

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/vlingo/core/internal/contacts/ContactMatch;->computeScores(I[I)V

    .line 113
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;->this$0:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    # getter for: Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->mru:Lcom/vlingo/core/internal/contacts/mru/MRU;
    invoke-static {v3}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->access$400(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)Lcom/vlingo/core/internal/contacts/mru/MRU;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;->this$0:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    # getter for: Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->contactType:Lcom/vlingo/core/internal/contacts/ContactType;
    invoke-static {v4}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->access$200(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/vlingo/core/internal/contacts/ContactMatch;->computeMRUScore(Lcom/vlingo/core/internal/contacts/mru/MRU;Lcom/vlingo/core/internal/contacts/ContactType;)V

    .line 114
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;->this$0:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    # getter for: Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->mru:Lcom/vlingo/core/internal/contacts/mru/MRU;
    invoke-static {v3}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->access$400(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)Lcom/vlingo/core/internal/contacts/mru/MRU;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;->this$0:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    # getter for: Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->contactType:Lcom/vlingo/core/internal/contacts/ContactType;
    invoke-static {v4}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->access$200(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/vlingo/core/internal/contacts/ContactMatch;->computeMRUScoresForData(Lcom/vlingo/core/internal/contacts/mru/MRU;Lcom/vlingo/core/internal/contacts/ContactType;)V

    .line 115
    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->sortDetails()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 149
    .end local v0    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v2    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v1

    .line 150
    .local v1, "ex":Ljava/lang/RuntimeException;
    # getter for: Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->log:Lcom/vlingo/core/internal/logging/Logger;
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->access$900()Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Caught RuntimeException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " stack trace: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 152
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;->this$0:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    # getter for: Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->callback:Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;
    invoke-static {v3}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->access$800(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;->onAsyncSortingFailed()V

    .line 154
    .end local v1    # "ex":Ljava/lang/RuntimeException;
    :cond_0
    :goto_1
    return-void

    .line 125
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;->this$0:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    # getter for: Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->sortedContacts:Ljava/util/List;
    invoke-static {v3}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->access$500(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;->this$0:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->getContactMatchComparator()Ljava/util/Comparator;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 134
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;->this$0:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    # getter for: Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->sortedContacts:Ljava/util/List;
    invoke-static {v3}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->access$500(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;->this$0:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    # getter for: Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->query:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->access$600()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->getContactMatchQueryComparator(Ljava/lang/String;)Ljava/util/Comparator;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 143
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;->this$0:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    const/4 v4, 0x1

    # setter for: Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->doneRunning:Z
    invoke-static {v3, v4}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->access$702(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;Z)Z

    .line 145
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;->this$0:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    # getter for: Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->callback:Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;
    invoke-static {v3}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->access$800(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 146
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;->this$0:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    # getter for: Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->callback:Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;
    invoke-static {v3}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->access$800(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;->this$0:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    # getter for: Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->sortedContacts:Ljava/util/List;
    invoke-static {v4}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->access$500(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;->onAsyncSortingUpdated(Ljava/util/List;)V

    .line 147
    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;->this$0:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    # getter for: Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->callback:Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;
    invoke-static {v3}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->access$800(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$1;->this$0:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    # getter for: Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->sortedContacts:Ljava/util/List;
    invoke-static {v4}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->access$500(Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;->onAsyncSortingFinished(Ljava/util/List;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
