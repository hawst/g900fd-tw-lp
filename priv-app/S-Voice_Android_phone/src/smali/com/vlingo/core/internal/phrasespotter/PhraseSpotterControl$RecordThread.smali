.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;
.super Ljava/lang/Thread;
.source "PhraseSpotterControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RecordThread"
.end annotation


# instance fields
.field private mChunkLength:I

.field private mChunkSize:I

.field private mIsStereo:Z

.field private mMicStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

.field private mMonoRawAudioBuffer:[S

.field private mPreBuffer:Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;

.field private mRawAudioBuffer:[S

.field private mSampleRate:I

.field mSpotterEngines:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Ljava/lang/String;)V
    .locals 1
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 390
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .line 391
    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 380
    const/16 v0, 0x3e80

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mSampleRate:I

    .line 392
    return-void
.end method

.method private destroyEngines()V
    .locals 3

    .prologue
    .line 498
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mSpotterEngines:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mSpotterEngines:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 510
    :cond_0
    return-void

    .line 502
    :cond_1
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mSpotterEngines:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;

    .line 504
    .local v0, "engine":Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;
    :try_start_0
    invoke-interface {v0}, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;->destroy()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 505
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private handlePhraseSpotted(Ljava/lang/String;ZI)V
    .locals 9
    .param p1, "spottedPhrase"    # Ljava/lang/String;
    .param p2, "isSeamlessEnabled"    # Z
    .param p3, "deltaD"    # I

    .prologue
    .line 582
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "handlePhraseSpotted() spottedPhrase="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", isSeamlessEnabled="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", deltaD="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 584
    if-nez p2, :cond_0

    .line 587
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mListener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    invoke-static {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    move-result-object v6

    invoke-interface {v6, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;->onPhraseSpotted(Ljava/lang/String;)V

    .line 624
    :goto_0
    return-void

    .line 589
    :cond_0
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mAudioLogger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;
    invoke-static {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;->markP()V

    .line 591
    const/4 v3, 0x0

    .line 592
    .local v3, "preBufferCarryOverSamples":I
    if-gez p3, :cond_3

    .line 593
    iget v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkLength:I

    div-int v6, p3, v6

    iget v7, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    mul-int/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 608
    :cond_1
    const/4 v5, 0x0

    .line 609
    .local v5, "seamlessBuffer":[S
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPreBuffer:Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->getArray()[S

    move-result-object v4

    .line 610
    .local v4, "preBufferSamples":[S
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPreBuffer:Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;

    .line 611
    array-length v6, v4

    invoke-static {v3, v6}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 612
    if-lez v3, :cond_2

    .line 613
    array-length v6, v4

    sub-int/2addr v6, v3

    array-length v7, v4

    invoke-static {v4, v6, v7}, Ljava/util/Arrays;->copyOfRange([SII)[S

    move-result-object v5

    .line 616
    :cond_2
    invoke-direct {p0, v5}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->transferMicForReco([S)Lcom/vlingo/core/internal/audio/MicrophoneStream;

    move-result-object v2

    .line 618
    .local v2, "micStream":Lcom/vlingo/core/internal/audio/MicrophoneStream;
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v7, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStoppingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    invoke-static {v7}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v7

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V
    invoke-static {v6, v7}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V

    .line 622
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mListener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    invoke-static {v6}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    move-result-object v6

    invoke-interface {v6, p1, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;->onSeamlessPhraseSpotted(Ljava/lang/String;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    goto :goto_0

    .line 597
    .end local v2    # "micStream":Lcom/vlingo/core/internal/audio/MicrophoneStream;
    .end local v4    # "preBufferSamples":[S
    .end local v5    # "seamlessBuffer":[S
    :cond_3
    if-lez p3, :cond_1

    .line 598
    iget v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkLength:I

    div-int v0, p3, v6

    .line 602
    .local v0, "chunksToDiscard":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_1

    .line 603
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->readChunk()Z

    .line 602
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private initAudio()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 544
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v2, v2, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mVlRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    if-eqz v2, :cond_0

    .line 550
    :cond_0
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    invoke-static {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getChunkLength()I

    move-result v2

    iput v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkLength:I

    .line 552
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    invoke-static {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->isTesting()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 553
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v2, v2, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mVlRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    sget-object v3, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->PHRASESPOTTING:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    invoke-static {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getAudioSourceType()Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    move-result-object v4

    invoke-static {v2, v3, v4, v5}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->requestMock(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;I)Lcom/vlingo/core/internal/audio/MicrophoneStream;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mMicStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .line 558
    :goto_0
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mMicStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->isStereo()Z

    move-result v2

    iput-boolean v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mIsStereo:Z

    .line 559
    iget-boolean v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mIsStereo:Z

    if-eqz v2, :cond_2

    const/4 v0, 0x2

    .line 560
    .local v0, "numChannels":I
    :goto_1
    iget v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkLength:I

    iget v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mSampleRate:I

    mul-int/2addr v2, v3

    mul-int/2addr v2, v0

    div-int/lit16 v2, v2, 0x3e8

    iput v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    .line 561
    iget v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    new-array v2, v2, [S

    iput-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mRawAudioBuffer:[S

    .line 564
    iget-boolean v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mIsStereo:Z

    if-eqz v2, :cond_3

    .line 565
    iget v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    div-int/lit8 v2, v2, 0x2

    new-array v2, v2, [S

    iput-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mMonoRawAudioBuffer:[S

    .line 569
    :goto_2
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    invoke-static {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getPreBufferLength()I

    move-result v2

    iget v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkLength:I

    div-int v1, v2, v3

    .line 570
    .local v1, "preBufferChunkCount":I
    new-instance v2, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;

    iget v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    invoke-direct {v2, v3, v1}, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;-><init>(II)V

    iput-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPreBuffer:Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;

    .line 579
    return-void

    .line 556
    .end local v0    # "numChannels":I
    .end local v1    # "preBufferChunkCount":I
    :cond_1
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v2, v2, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mVlRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    sget-object v3, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->PHRASESPOTTING:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    invoke-static {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getAudioSourceType()Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    move-result-object v4

    invoke-static {v2, v3, v4, v5}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->request(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;I)Lcom/vlingo/core/internal/audio/MicrophoneStream;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mMicStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    goto :goto_0

    .line 559
    :cond_2
    const/4 v0, 0x1

    goto :goto_1

    .line 567
    .restart local v0    # "numChannels":I
    :cond_3
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mRawAudioBuffer:[S

    iput-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mMonoRawAudioBuffer:[S

    goto :goto_2
.end method

.method private initializeEngines(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;)Z
    .locals 8
    .param p1, "psParameters"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    .prologue
    const/4 v5, 0x0

    .line 449
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mSpotterEngines:Ljava/util/ArrayList;

    .line 451
    invoke-virtual {p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getCoreSpotterParams()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 454
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mSpotterEngines:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getCoreSpotterParams()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v7

    invoke-static {v7}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;->getInstance(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;)Lcom/vlingo/core/internal/phrasespotter/CoreSpotter;

    move-result-object v7

    invoke-virtual {v6, v5, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 457
    :cond_0
    sget-object v6, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v6}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v1

    .line 458
    .local v1, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    if-eqz v1, :cond_1

    .line 460
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;

    .line 463
    .local v4, "spotterEngine":Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mSpotterEngines:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 475
    .end local v4    # "spotterEngine":Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;
    :cond_1
    :goto_0
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mSpotterEngines:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 476
    .local v2, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;>;"
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 477
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;

    .line 479
    .local v3, "spotter":Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;
    :try_start_1
    iget v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mSampleRate:I

    invoke-interface {v3, v6}, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;->init(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 480
    :catch_0
    move-exception v0

    .line 483
    .local v0, "e":Ljava/lang/Exception;
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 464
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;>;"
    .end local v3    # "spotter":Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;
    :catch_1
    move-exception v0

    .line 467
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 487
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;>;"
    :cond_2
    iget-object v6, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mSpotterEngines:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-gtz v6, :cond_3

    .line 491
    :goto_2
    return v5

    :cond_3
    const/4 v5, 0x1

    goto :goto_2
.end method

.method private processAudio()V
    .locals 11

    .prologue
    .line 513
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "processAudio()"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    const/4 v0, 0x0

    .line 515
    .local v0, "chunksRead":I
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    .line 516
    .local v5, "startTime":J
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->readChunk()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 517
    add-int/lit8 v0, v0, 0x1

    .line 518
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mSpotterEngines:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;

    .line 519
    .local v1, "engine":Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mMonoRawAudioBuffer:[S

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mMonoRawAudioBuffer:[S

    array-length v10, v10

    invoke-interface {v1, v8, v9, v10}, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;->processShortArray([SII)Ljava/lang/String;

    move-result-object v3

    .line 520
    .local v3, "ps":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 521
    invoke-interface {v1}, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;->getSpottedPhraseScore()F

    move-result v4

    .line 522
    .local v4, "score":F
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "  phrase spotted = \'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\', score="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "; engine="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mAudioLogger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;
    invoke-static {v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;

    move-result-object v8

    invoke-virtual {v8, v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;->setScore(F)V

    .line 525
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    invoke-static {v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v8

    iget-object v7, v8, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->stats:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$PhraseSpotterStats;

    .line 526
    .local v7, "stats":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$PhraseSpotterStats;
    if-eqz v7, :cond_2

    .line 527
    iput v0, v7, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$PhraseSpotterStats;->chunksRead:I

    .line 528
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v5

    iput-wide v8, v7, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$PhraseSpotterStats;->timeInMs:J

    .line 529
    iput v4, v7, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$PhraseSpotterStats;->score:F

    .line 531
    :cond_2
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mListener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    invoke-static {v8}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    move-result-object v8

    invoke-interface {v8, v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;->onPhraseDetected(Ljava/lang/String;)V

    .line 532
    invoke-interface {v1, v3}, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;->useSeamlessFeature(Ljava/lang/String;)Z

    move-result v8

    invoke-interface {v1}, Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;->getDeltaD()I

    move-result v9

    invoke-direct {p0, v3, v8, v9}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->handlePhraseSpotted(Ljava/lang/String;ZI)V

    .line 538
    .end local v1    # "engine":Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "ps":Ljava/lang/String;
    .end local v4    # "score":F
    .end local v7    # "stats":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$PhraseSpotterStats;
    :goto_0
    return-void

    .line 537
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "RecordThread.processAudio(): current state is now "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mCurrentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    invoke-static {v10}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ", exiting"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private readChunk()Z
    .locals 12

    .prologue
    const/4 v8, 0x0

    .line 627
    const/4 v7, 0x0

    .line 628
    .local v7, "totalNumRead":I
    const/4 v2, 0x0

    .line 629
    .local v2, "numRead":I
    iget v5, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    .line 630
    .local v5, "remainingToRead":I
    const/4 v6, 0x1

    .line 634
    .local v6, "ret":Z
    :cond_0
    :goto_0
    if-lez v5, :cond_3

    .line 635
    iget-object v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->isInSpottingState()Z

    move-result v9

    if-nez v9, :cond_1

    .line 675
    :goto_1
    return v8

    .line 638
    :cond_1
    iget-object v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mMicStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    iget-object v10, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mRawAudioBuffer:[S

    invoke-virtual {v9, v10, v7, v5}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->read([SII)I

    move-result v2

    .line 639
    if-ltz v2, :cond_2

    .line 640
    sub-int/2addr v5, v2

    .line 641
    add-int/2addr v7, v2

    .line 642
    if-lez v5, :cond_0

    .line 644
    :try_start_0
    iget-object v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    invoke-static {v9}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->getRecorderSleep()I

    move-result v9

    int-to-long v9, v9

    invoke-static {v9, v10}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 645
    :catch_0
    move-exception v0

    .line 646
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 650
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_2
    iget-object v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mMicStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    instance-of v9, v9, Lcom/vlingo/core/internal/audio/FileTestMicrophoneStream;

    if-eqz v9, :cond_4

    .line 652
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "readChunk(): EOF on test input file"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 653
    const/4 v6, 0x0

    .line 663
    :cond_3
    iget-boolean v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mIsStereo:Z

    if-eqz v9, :cond_5

    .line 664
    const/4 v3, 0x0

    .line 665
    .local v3, "offset":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    iget v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    if-ge v1, v9, :cond_5

    .line 666
    iget-object v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mMonoRawAudioBuffer:[S

    add-int/lit8 v4, v3, 0x1

    .end local v3    # "offset":I
    .local v4, "offset":I
    iget-object v10, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mRawAudioBuffer:[S

    aget-short v10, v10, v1

    aput-short v10, v9, v3

    .line 665
    add-int/lit8 v1, v1, 0x2

    move v3, v4

    .end local v4    # "offset":I
    .restart local v3    # "offset":I
    goto :goto_2

    .line 658
    .end local v1    # "i":I
    .end local v3    # "offset":I
    :cond_4
    new-instance v8, Ljava/lang/IllegalStateException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Error reading chunk, read() returned: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 670
    :cond_5
    if-eqz v6, :cond_6

    .line 671
    iget-object v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mAudioLogger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;
    invoke-static {v9}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;

    move-result-object v9

    iget-object v10, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mRawAudioBuffer:[S

    iget v11, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mChunkSize:I

    invoke-virtual {v9, v10, v8, v11}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;->writeData([SII)V

    .line 672
    iget-object v8, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mPreBuffer:Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;

    iget-object v9, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mRawAudioBuffer:[S

    invoke-virtual {v8, v9}, Lcom/vlingo/core/internal/util/CircularShortBlockBuffer;->write([S)V

    :cond_6
    move v8, v6

    .line 675
    goto/16 :goto_1
.end method

.method private stopRecording()V
    .locals 4

    .prologue
    .line 691
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mMicStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    if-eqz v1, :cond_0

    .line 695
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mMicStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 699
    :goto_0
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mAudioLogger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;
    invoke-static {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;->dumpToFile()V

    .line 701
    :cond_0
    return-void

    .line 696
    :catch_0
    move-exception v0

    .line 697
    .local v0, "ex":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "PhraseSpotter VLG_EXCEPTION-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private transferMicForReco([S)Lcom/vlingo/core/internal/audio/MicrophoneStream;
    .locals 2
    .param p1, "buffer"    # [S

    .prologue
    .line 679
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mMicStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .line 681
    .local v0, "micStream":Lcom/vlingo/core/internal/audio/MicrophoneStream;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->mMicStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .line 682
    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->enableAudioFiltering()V

    .line 683
    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->setBufferedData([S)V

    .line 684
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mAudioLogger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;
    invoke-static {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;->hasTarget()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 685
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mAudioLogger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;
    invoke-static {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;->target()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterAudioLogger;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->setAudioLogger(Lcom/vlingo/core/internal/audio/AudioLogger;)V

    .line 687
    :cond_0
    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 396
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "run()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStateTransitionLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_5

    .line 400
    :try_start_1
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->isInStartingState()Z

    move-result v2

    if-nez v2, :cond_2

    .line 401
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "  currentState is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mCurrentState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    invoke-static {v5}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", returning"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 424
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->destroyEngines()V

    .line 425
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->stopRecording()V

    .line 427
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStateTransitionLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 429
    :try_start_2
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->isInStoppingState()Z

    move-result v2

    if-nez v2, :cond_0

    .line 430
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStoppingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    invoke-static {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v4

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V
    invoke-static {v2, v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V

    .line 432
    :cond_0
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mIdleState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    invoke-static {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1000(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v4

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V
    invoke-static {v2, v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V

    .line 434
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;
    invoke-static {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$700(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    if-ne v2, v4, :cond_1

    .line 435
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    const/4 v4, 0x0

    # setter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;
    invoke-static {v2, v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$702(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;

    .line 440
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v4, "  RecordThread ending"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    monitor-exit v3

    .line 443
    :goto_0
    return-void

    .line 441
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 404
    :cond_2
    :try_start_3
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mSpottingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    invoke-static {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1400(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v4

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V
    invoke-static {v2, v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V

    .line 406
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 408
    :try_start_4
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    invoke-static {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->initializeEngines(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;)Z

    .line 411
    invoke-static {}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->getInstance()Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;

    move-result-object v1

    .line 412
    .local v1, "recoMgr":Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;->isActive()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 413
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "  can\'t start spotter, m_recoMgr.isActive() returned true"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_5

    .line 424
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->destroyEngines()V

    .line 425
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->stopRecording()V

    .line 427
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStateTransitionLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 429
    :try_start_5
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->isInStoppingState()Z

    move-result v2

    if-nez v2, :cond_3

    .line 430
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStoppingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    invoke-static {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v4

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V
    invoke-static {v2, v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V

    .line 432
    :cond_3
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mIdleState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    invoke-static {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1000(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v4

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V
    invoke-static {v2, v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V

    .line 434
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;
    invoke-static {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$700(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    if-ne v2, v4, :cond_4

    .line 435
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    const/4 v4, 0x0

    # setter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;
    invoke-static {v2, v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$702(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;

    .line 440
    :cond_4
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v4, "  RecordThread ending"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    monitor-exit v3

    goto :goto_0

    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v2

    .line 406
    .end local v1    # "recoMgr":Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;
    :catchall_2
    move-exception v2

    :try_start_6
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    throw v2
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    .line 421
    :catch_0
    move-exception v0

    .line 422
    .local v0, "e":Ljava/lang/Exception;
    :try_start_8
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Caught exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    .line 424
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->destroyEngines()V

    .line 425
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->stopRecording()V

    .line 427
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStateTransitionLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 429
    :try_start_9
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->isInStoppingState()Z

    move-result v2

    if-nez v2, :cond_5

    .line 430
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStoppingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    invoke-static {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v4

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V
    invoke-static {v2, v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V

    .line 432
    :cond_5
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mIdleState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    invoke-static {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1000(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v4

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V
    invoke-static {v2, v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V

    .line 434
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;
    invoke-static {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$700(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    if-ne v2, v4, :cond_6

    .line 435
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    const/4 v4, 0x0

    # setter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;
    invoke-static {v2, v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$702(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;

    .line 440
    :cond_6
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v4, "  RecordThread ending"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    monitor-exit v3

    goto/16 :goto_0

    :catchall_3
    move-exception v2

    monitor-exit v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    throw v2

    .line 417
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "recoMgr":Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;
    :cond_7
    :try_start_a
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->initAudio()V

    .line 418
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->processAudio()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    .line 424
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->destroyEngines()V

    .line 425
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->stopRecording()V

    .line 427
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStateTransitionLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 429
    :try_start_b
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->isInStoppingState()Z

    move-result v2

    if-nez v2, :cond_8

    .line 430
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStoppingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    invoke-static {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v4

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V
    invoke-static {v2, v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V

    .line 432
    :cond_8
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mIdleState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    invoke-static {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1000(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v4

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V
    invoke-static {v2, v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V

    .line 434
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;
    invoke-static {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$700(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    if-ne v2, v4, :cond_9

    .line 435
    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    const/4 v4, 0x0

    # setter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;
    invoke-static {v2, v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$702(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;

    .line 440
    :cond_9
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v4, "  RecordThread ending"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    monitor-exit v3

    goto/16 :goto_0

    :catchall_4
    move-exception v2

    monitor-exit v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    throw v2

    .line 424
    .end local v1    # "recoMgr":Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;
    :catchall_5
    move-exception v2

    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->destroyEngines()V

    .line 425
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->stopRecording()V

    .line 427
    iget-object v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStateTransitionLock:Ljava/lang/Object;
    invoke-static {v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 429
    :try_start_c
    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->isInStoppingState()Z

    move-result v4

    if-nez v4, :cond_a

    .line 430
    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v5, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStoppingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    invoke-static {v5}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v5

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V
    invoke-static {v4, v5}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V

    .line 432
    :cond_a
    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v5, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mIdleState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    invoke-static {v5}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1000(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v5

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V
    invoke-static {v4, v5}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V

    .line 434
    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;
    invoke-static {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$700(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;

    move-result-object v4

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    if-ne v4, v5, :cond_b

    .line 435
    iget-object v4, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    const/4 v5, 0x0

    # setter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;
    invoke-static {v4, v5}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$702(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;

    .line 440
    :cond_b
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "  RecordThread ending"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    monitor-exit v3
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_6

    throw v2

    :catchall_6
    move-exception v2

    :try_start_d
    monitor-exit v3
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_6

    throw v2
.end method
