.class Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;
.super Landroid/os/Handler;
.source "DMServerTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ProcessingTone"
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ProcessingTone"

.field private static final RELEASE:I = 0x3

.field private static final START:I = 0x1

.field private static final STOP:I = 0x2

.field private static final VOLUME_DOWN:I = 0x4


# instance fields
.field private mediaPlayer:Landroid/media/MediaPlayer;

.field private receiver:Landroid/content/BroadcastReceiver;

.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)V
    .locals 10

    .prologue
    const/4 v0, -0x1

    const/4 v9, 0x0

    .line 649
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 650
    const-string/jumbo v1, "ProcessingTone"

    const-string/jumbo v2, "ProcessingTone()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 652
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isAppCarModeEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_processing_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$raw;)I

    move-result v8

    .line 654
    .local v8, "processingTone":I
    :goto_0
    if-ne v8, v0, :cond_1

    .line 686
    :goto_1
    return-void

    .end local v8    # "processingTone":I
    :cond_0
    move v8, v0

    .line 652
    goto :goto_0

    .line 658
    .restart local v8    # "processingTone":I
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    move-result-object v6

    .line 661
    .local v6, "afd":Landroid/content/res/AssetFileDescriptor;
    :try_start_0
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 662
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v2

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    .line 663
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V

    .line 664
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 666
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getCurrentStream()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 667
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getCurrentStreamMaxVolume()I

    move-result v1

    int-to-float v1, v1

    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getCurrentStreamMaxVolume()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 669
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    .line 671
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone$1;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone$1;-><init>(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->receiver:Landroid/content/BroadcastReceiver;

    .line 677
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->receiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "com.vlingo.client.app.action.THINKING_AUDIO_SYNC"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 678
    :catch_0
    move-exception v7

    .line 679
    .local v7, "e":Ljava/io/IOException;
    const-string/jumbo v0, "ProcessingTone"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Error playing tone: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    iput-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->mediaPlayer:Landroid/media/MediaPlayer;

    goto/16 :goto_1

    .line 682
    .end local v7    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v7

    .line 683
    .local v7, "e":Ljava/lang/IllegalStateException;
    const-string/jumbo v0, "ProcessingTone"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Error playing tone: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    iput-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->mediaPlayer:Landroid/media/MediaPlayer;

    goto/16 :goto_1
.end method


# virtual methods
.method public declared-synchronized handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 689
    monitor-enter p0

    :try_start_0
    const-string/jumbo v7, "ProcessingTone"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Got "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p1, Landroid/os/Message;->what:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 691
    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->mediaPlayer:Landroid/media/MediaPlayer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v7, :cond_1

    .line 739
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 695
    :cond_1
    :try_start_1
    iget v7, p1, Landroid/os/Message;->what:I

    packed-switch v7, :pswitch_data_0

    goto :goto_0

    .line 697
    :pswitch_0
    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->gotResults:Z
    invoke-static {v7}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$1600(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v7

    if-nez v7, :cond_0

    .line 699
    :try_start_2
    const-string/jumbo v7, "ProcessingTone"

    const-string/jumbo v8, "Start playing processing tone. "

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 701
    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v7}, Landroid/media/MediaPlayer;->start()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 702
    :catch_0
    move-exception v2

    .line 703
    .local v2, "e":Ljava/lang/Exception;
    :try_start_3
    const-string/jumbo v7, "ProcessingTone"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Error playing tone. "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 689
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    .line 708
    :pswitch_1
    :try_start_4
    const-string/jumbo v7, "ProcessingTone"

    const-string/jumbo v8, "Stop playing processing tone. "

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 710
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->getCurrentStreamVolume()I

    move-result v7

    int-to-float v1, v7

    .line 711
    .local v1, "currentVolume":F
    const/16 v6, 0x32

    .line 712
    .local v6, "timeChunk":I
    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->processingToneFadeOut:I
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$1800()I

    move-result v7

    div-int v0, v7, v6

    .line 713
    .local v0, "chunkCount":I
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_1
    if-gt v3, v0, :cond_2

    .line 714
    new-instance v4, Landroid/os/Message;

    invoke-direct {v4}, Landroid/os/Message;-><init>()V

    .line 715
    .local v4, "message":Landroid/os/Message;
    const/4 v7, 0x4

    iput v7, v4, Landroid/os/Message;->what:I

    .line 716
    sub-int v7, v0, v3

    int-to-float v7, v7

    mul-float/2addr v7, v1

    int-to-float v8, v0

    div-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    iput-object v7, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 718
    const-string/jumbo v7, "ProcessingTone"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Sending fade out request. Volume: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 720
    mul-int v7, v3, v6

    int-to-long v7, v7

    invoke-virtual {p0, v4, v7, v8}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 713
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 722
    .end local v4    # "message":Landroid/os/Message;
    :cond_2
    const/4 v7, 0x3

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->processingToneFadeOut:I
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$1800()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {p0, v7, v8, v9}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 725
    .end local v0    # "chunkCount":I
    .end local v1    # "currentVolume":F
    .end local v3    # "i":I
    .end local v6    # "timeChunk":I
    :pswitch_2
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v5

    .line 727
    .local v5, "newVolume":F
    const-string/jumbo v7, "ProcessingTone"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Volume down: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 728
    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v7, :cond_0

    .line 729
    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v7, v5, v5}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto/16 :goto_0

    .line 733
    .end local v5    # "newVolume":F
    :pswitch_3
    const-string/jumbo v7, "ProcessingTone"

    const-string/jumbo v8, "Start releasing media player."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 735
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->release()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 695
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method protected release()V
    .locals 2

    .prologue
    .line 742
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 743
    const-string/jumbo v0, "ProcessingTone"

    const-string/jumbo v1, "Releasing media player."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 746
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 749
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->receiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 750
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 752
    :cond_1
    return-void
.end method

.method stop()V
    .locals 2

    .prologue
    .line 755
    const-string/jumbo v0, "ProcessingTone"

    const-string/jumbo v1, "stop()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 756
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    const/4 v1, 0x1

    # setter for: Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->gotResults:Z
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->access$1602(Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;Z)Z

    .line 757
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask$ProcessingTone;->sendEmptyMessage(I)Z

    .line 758
    return-void
.end method
