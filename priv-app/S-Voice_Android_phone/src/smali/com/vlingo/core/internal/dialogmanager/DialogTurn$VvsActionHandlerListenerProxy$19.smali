.class Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$19;
.super Ljava/lang/Object;
.source "DialogTurn.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

.field final synthetic val$decorators:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

.field final synthetic val$key:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

.field final synthetic val$listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field final synthetic val$object:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0

    .prologue
    .line 624
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$19;->this$1:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$19;->val$key:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    iput-object p3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$19;->val$decorators:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    iput-object p4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$19;->val$object:Ljava/lang/Object;

    iput-object p5, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$19;->val$listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 626
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$19;->this$1:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->access$100(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 627
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$19;->this$1:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->access$100(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$19;->val$key:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$19;->val$decorators:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$19;->val$object:Ljava/lang/Object;

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$19;->val$listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 629
    :cond_0
    return-void
.end method
