.class public Lcom/vlingo/core/internal/questions/parser/AnswerParser;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "AnswerParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/questions/parser/AnswerParser$ATTRIBUTES;,
        Lcom/vlingo/core/internal/questions/parser/AnswerParser$TAGS;
    }
.end annotation


# static fields
.field public static DEBUG:I

.field public static DEBUG_ALL:I

.field public static DEBUG_ATTRIBUTES:I

.field public static DEBUG_DETAILS:I

.field public static DEBUG_NONE:I

.field public static DEBUG_OPS:I


# instance fields
.field private elements:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private imageCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/FutureTask",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;>;"
        }
    .end annotation
.end field

.field private imageQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentImage:Lcom/vlingo/core/internal/questions/parser/ImageElement;

.field private mCurrentResponse:Lcom/vlingo/core/internal/questions/parser/ProviderResponse;

.field private mCurrentSection:Lcom/vlingo/core/internal/questions/parser/SectionElement;

.field private mCurrentSubsection:Lcom/vlingo/core/internal/questions/parser/SubsectionElement;

.field private responses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/questions/parser/ProviderResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    sput v0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->DEBUG_NONE:I

    .line 51
    const/4 v0, 0x1

    sput v0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->DEBUG_OPS:I

    .line 52
    const/4 v0, 0x2

    sput v0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->DEBUG_ATTRIBUTES:I

    .line 53
    const/4 v0, 0x3

    sput v0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->DEBUG_DETAILS:I

    .line 54
    const/16 v0, 0x3e7

    sput v0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->DEBUG_ALL:I

    .line 56
    sget v0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->DEBUG_ALL:I

    sput v0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->DEBUG:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->imageCache:Ljava/util/Map;

    .line 73
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->imageQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->responses:Ljava/util/List;

    .line 86
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->elements:Ljava/util/Stack;

    .line 114
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)Lcom/vlingo/core/internal/questions/parser/ProviderResponse;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->mCurrentResponse:Lcom/vlingo/core/internal/questions/parser/ProviderResponse;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->responses:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)Lcom/vlingo/core/internal/questions/parser/SectionElement;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->mCurrentSection:Lcom/vlingo/core/internal/questions/parser/SectionElement;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)Lcom/vlingo/core/internal/questions/parser/SubsectionElement;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->mCurrentSubsection:Lcom/vlingo/core/internal/questions/parser/SubsectionElement;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)Lcom/vlingo/core/internal/questions/parser/ImageElement;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->mCurrentImage:Lcom/vlingo/core/internal/questions/parser/ImageElement;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->imageQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/questions/parser/AnswerParser;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/questions/parser/AnswerParser;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->loadImage(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private loadImage(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 336
    const/4 v2, 0x0

    .line 345
    .local v2, "image":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->imageCache:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 348
    :try_start_0
    iget-object v3, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->imageCache:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/FutureTask;

    invoke-virtual {v3}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Landroid/graphics/drawable/Drawable;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 401
    :goto_0
    return-object v2

    .line 360
    :cond_0
    :try_start_1
    new-instance v1, Ljava/util/concurrent/FutureTask;

    new-instance v3, Lcom/vlingo/core/internal/questions/parser/AnswerParser$6;

    invoke-direct {v3, p0, p1}, Lcom/vlingo/core/internal/questions/parser/AnswerParser$6;-><init>(Lcom/vlingo/core/internal/questions/parser/AnswerParser;Ljava/lang/String;)V

    invoke-direct {v1, v3}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 384
    .local v1, "fetch":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<Landroid/graphics/drawable/Drawable;>;"
    iget-object v3, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->imageCache:Ljava/util/Map;

    invoke-interface {v3, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 385
    invoke-virtual {v1}, Ljava/util/concurrent/FutureTask;->run()V

    .line 386
    invoke-virtual {v1}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Landroid/graphics/drawable/Drawable;

    move-object v2, v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 388
    .end local v1    # "fetch":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<Landroid/graphics/drawable/Drawable;>;"
    :catch_0
    move-exception v3

    goto :goto_0

    .line 351
    :catch_1
    move-exception v3

    goto :goto_0
.end method

.method private newElement(Ljava/lang/String;)Lcom/vlingo/core/internal/questions/parser/ResponseElement;
    .locals 3
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 255
    const/4 v0, 0x0

    .line 257
    .local v0, "element":Lcom/vlingo/core/internal/questions/parser/ResponseElement;
    const-string/jumbo v1, "ProviderSrcResponse"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 258
    new-instance v1, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;

    invoke-direct {v1, p1}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->mCurrentResponse:Lcom/vlingo/core/internal/questions/parser/ProviderResponse;

    .line 259
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->mCurrentResponse:Lcom/vlingo/core/internal/questions/parser/ProviderResponse;

    .line 260
    iget-object v1, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->elements:Ljava/util/Stack;

    new-instance v2, Lcom/vlingo/core/internal/questions/parser/AnswerParser$1;

    invoke-direct {v2, p0}, Lcom/vlingo/core/internal/questions/parser/AnswerParser$1;-><init>(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)V

    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    :goto_0
    return-object v0

    .line 264
    :cond_0
    const-string/jumbo v1, "Section"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 265
    new-instance v1, Lcom/vlingo/core/internal/questions/parser/SectionElement;

    invoke-direct {v1, p1}, Lcom/vlingo/core/internal/questions/parser/SectionElement;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->mCurrentSection:Lcom/vlingo/core/internal/questions/parser/SectionElement;

    .line 266
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->mCurrentSection:Lcom/vlingo/core/internal/questions/parser/SectionElement;

    .line 267
    iget-object v1, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->elements:Ljava/util/Stack;

    new-instance v2, Lcom/vlingo/core/internal/questions/parser/AnswerParser$2;

    invoke-direct {v2, p0}, Lcom/vlingo/core/internal/questions/parser/AnswerParser$2;-><init>(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)V

    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 271
    :cond_1
    const-string/jumbo v1, "Subsection"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 272
    new-instance v1, Lcom/vlingo/core/internal/questions/parser/SubsectionElement;

    invoke-direct {v1, p1}, Lcom/vlingo/core/internal/questions/parser/SubsectionElement;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->mCurrentSubsection:Lcom/vlingo/core/internal/questions/parser/SubsectionElement;

    .line 273
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->mCurrentSubsection:Lcom/vlingo/core/internal/questions/parser/SubsectionElement;

    .line 274
    iget-object v1, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->elements:Ljava/util/Stack;

    new-instance v2, Lcom/vlingo/core/internal/questions/parser/AnswerParser$3;

    invoke-direct {v2, p0}, Lcom/vlingo/core/internal/questions/parser/AnswerParser$3;-><init>(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)V

    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 278
    :cond_2
    const-string/jumbo v1, "Image"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 279
    new-instance v1, Lcom/vlingo/core/internal/questions/parser/ImageElement;

    invoke-direct {v1, p1}, Lcom/vlingo/core/internal/questions/parser/ImageElement;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->mCurrentImage:Lcom/vlingo/core/internal/questions/parser/ImageElement;

    .line 280
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->mCurrentImage:Lcom/vlingo/core/internal/questions/parser/ImageElement;

    .line 281
    iget-object v1, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->elements:Ljava/util/Stack;

    new-instance v2, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;

    invoke-direct {v2, p0}, Lcom/vlingo/core/internal/questions/parser/AnswerParser$4;-><init>(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)V

    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 319
    :cond_3
    new-instance v0, Lcom/vlingo/core/internal/questions/parser/ResponseElement;

    .end local v0    # "element":Lcom/vlingo/core/internal/questions/parser/ResponseElement;
    invoke-direct {v0, p1}, Lcom/vlingo/core/internal/questions/parser/ResponseElement;-><init>(Ljava/lang/String;)V

    .line 320
    .restart local v0    # "element":Lcom/vlingo/core/internal/questions/parser/ResponseElement;
    iget-object v1, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->elements:Ljava/util/Stack;

    new-instance v2, Lcom/vlingo/core/internal/questions/parser/AnswerParser$5;

    invoke-direct {v2, p0}, Lcom/vlingo/core/internal/questions/parser/AnswerParser$5;-><init>(Lcom/vlingo/core/internal/questions/parser/AnswerParser;)V

    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static parse(Ljava/lang/String;)[Lcom/vlingo/core/internal/questions/parser/ProviderResponse;
    .locals 4
    .param p0, "xml"    # Ljava/lang/String;

    .prologue
    .line 131
    if-nez p0, :cond_0

    .line 132
    const/4 v2, 0x0

    .line 165
    :goto_0
    return-object v2

    .line 134
    :cond_0
    new-instance v0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    invoke-direct {v0}, Lcom/vlingo/core/internal/questions/parser/AnswerParser;-><init>()V

    .line 136
    .local v0, "parser":Lcom/vlingo/core/internal/questions/parser/AnswerParser;
    new-instance v1, Lorg/xml/sax/InputSource;

    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    .line 137
    .local v1, "source":Lorg/xml/sax/InputSource;
    const-string/jumbo v2, "UTF-8"

    invoke-virtual {v1, v2}, Lorg/xml/sax/InputSource;->setEncoding(Ljava/lang/String;)V

    .line 143
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Ljavax/xml/parsers/SAXParser;->parse(Lorg/xml/sax/InputSource;Lorg/xml/sax/helpers/DefaultHandler;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    :goto_1
    const-string/jumbo v2, "wa.image.preloads"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 153
    invoke-direct {v0}, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->waitForDownloads()V

    .line 165
    :cond_1
    invoke-virtual {v0}, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->getResponses()[Lcom/vlingo/core/internal/questions/parser/ProviderResponse;

    move-result-object v2

    goto :goto_0

    .line 144
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method private waitForDownloads()V
    .locals 2

    .prologue
    .line 171
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->imageQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 172
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->imageQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->imageQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    goto :goto_0

    .line 178
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 179
    :catch_0
    move-exception v0

    .line 183
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->imageQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 185
    :goto_1
    return-void

    .line 178
    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 183
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->imageQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    goto :goto_1

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->imageQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    throw v0
.end method


# virtual methods
.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;

    .prologue
    .line 225
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->elements:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 226
    return-void
.end method

.method public error(Lorg/xml/sax/SAXParseException;)V
    .locals 0
    .param p1, "e"    # Lorg/xml/sax/SAXParseException;

    .prologue
    .line 236
    return-void
.end method

.method public fatalError(Lorg/xml/sax/SAXParseException;)V
    .locals 0
    .param p1, "e"    # Lorg/xml/sax/SAXParseException;

    .prologue
    .line 241
    return-void
.end method

.method public getResponses()[Lcom/vlingo/core/internal/questions/parser/ProviderResponse;
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->responses:Ljava/util/List;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/vlingo/core/internal/questions/parser/ProviderResponse;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/questions/parser/ProviderResponse;

    return-object v0
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "attributes"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 201
    invoke-direct {p0, p2}, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->newElement(Ljava/lang/String;)Lcom/vlingo/core/internal/questions/parser/ResponseElement;

    move-result-object v0

    .line 206
    .local v0, "element":Lcom/vlingo/core/internal/questions/parser/ResponseElement;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p4}, Lorg/xml/sax/Attributes;->getLength()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 220
    invoke-virtual {v0}, Lcom/vlingo/core/internal/questions/parser/ResponseElement;->getAttributes()Ljava/util/Map;

    move-result-object v2

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getLocalName(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 222
    :cond_0
    return-void
.end method

.method public warning(Lorg/xml/sax/SAXParseException;)V
    .locals 0
    .param p1, "e"    # Lorg/xml/sax/SAXParseException;

    .prologue
    .line 231
    return-void
.end method
