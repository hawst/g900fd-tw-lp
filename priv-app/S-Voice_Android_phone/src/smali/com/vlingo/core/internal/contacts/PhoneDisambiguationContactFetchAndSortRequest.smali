.class public Lcom/vlingo/core/internal/contacts/PhoneDisambiguationContactFetchAndSortRequest;
.super Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;
.source "PhoneDisambiguationContactFetchAndSortRequest.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactType;[ILcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "type"    # Lcom/vlingo/core/internal/contacts/ContactType;
    .param p4, "requestedTypes"    # [I
    .param p5, "callback"    # Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;

    .prologue
    .line 13
    invoke-direct/range {p0 .. p5}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactType;[ILcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;)V

    .line 14
    return-void
.end method


# virtual methods
.method protected getContactMatches(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactType;)Ljava/util/List;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "queryParam"    # Ljava/lang/String;
    .param p3, "contactTypeParam"    # Lcom/vlingo/core/internal/contacts/ContactType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/ContactType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequest;->getContactMatches(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactType;)Ljava/util/List;

    move-result-object v1

    .line 19
    .local v1, "contactMatches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 20
    .local v0, "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/PhoneDisambiguationContactFetchAndSortRequest;->getRequestedTypes()[I

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/vlingo/core/internal/contacts/ContactMatch;->chooseSinglePhone([I)V

    goto :goto_0

    .line 22
    .end local v0    # "contactMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_0
    return-object v1
.end method
