.class Lcom/vlingo/core/internal/audio/TTSEngine$TTSLocalDefaultEngine;
.super Lcom/vlingo/core/internal/audio/TTSLocalEngine;
.source "TTSEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/audio/TTSEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TTSLocalDefaultEngine"
.end annotation


# instance fields
.field private mTextToSpeach:Landroid/speech/tts/TextToSpeech;


# direct methods
.method public constructor <init>(Landroid/speech/tts/TextToSpeech;)V
    .locals 0
    .param p1, "textToSpeach"    # Landroid/speech/tts/TextToSpeech;

    .prologue
    .line 717
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/TTSLocalEngine;-><init>()V

    .line 718
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/TTSEngine$TTSLocalDefaultEngine;->mTextToSpeach:Landroid/speech/tts/TextToSpeech;

    .line 719
    return-void
.end method


# virtual methods
.method public getEngines()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/speech/tts/TextToSpeech$EngineInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 759
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$TTSLocalDefaultEngine;->mTextToSpeach:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->getEngines()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLanguage()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 754
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$TTSLocalDefaultEngine;->mTextToSpeach:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->getLanguage()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public setLanguage(Ljava/util/Locale;)I
    .locals 1
    .param p1, "currentLocale"    # Ljava/util/Locale;

    .prologue
    .line 744
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$TTSLocalDefaultEngine;->mTextToSpeach:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0, p1}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    move-result v0

    return v0
.end method

.method public setOnUtteranceCompletedListener(Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;)I
    .locals 1
    .param p1, "listener"    # Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;

    .prologue
    .line 723
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$TTSLocalDefaultEngine;->mTextToSpeach:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0, p1}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceCompletedListener(Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;)I

    move-result v0

    return v0
.end method

.method public setSpeechRate(F)V
    .locals 1
    .param p1, "localTtsSpeechRateNormal"    # F

    .prologue
    .line 749
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$TTSLocalDefaultEngine;->mTextToSpeach:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0, p1}, Landroid/speech/tts/TextToSpeech;->setSpeechRate(F)I

    .line 750
    return-void
.end method

.method public shutdown()V
    .locals 1

    .prologue
    .line 739
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$TTSLocalDefaultEngine;->mTextToSpeach:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 740
    return-void
.end method

.method public stop()I
    .locals 1

    .prologue
    .line 734
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$TTSLocalDefaultEngine;->mTextToSpeach:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    move-result v0

    return v0
.end method

.method public synthesizeToFile(Ljava/lang/String;Ljava/util/HashMap;Ljava/lang/String;)I
    .locals 1
    .param p1, "textToSynthesize"    # Ljava/lang/String;
    .param p3, "localFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 729
    .local p2, "myHashRender":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/TTSEngine$TTSLocalDefaultEngine;->mTextToSpeach:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0, p1, p2, p3}, Landroid/speech/tts/TextToSpeech;->synthesizeToFile(Ljava/lang/String;Ljava/util/HashMap;Ljava/lang/String;)I

    move-result v0

    return v0
.end method
