.class public Lcom/vlingo/core/internal/audio/FileTestMicrophoneStream;
.super Lcom/vlingo/core/internal/audio/MicrophoneStream;
.source "FileTestMicrophoneStream.java"


# instance fields
.field lastTime:J

.field private final mDis:Ljava/io/DataInputStream;

.field shortsRead:I


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)V
    .locals 5
    .param p1, "srContext"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext;
    .param p2, "audioSourceType"    # Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/audio/MicrophoneStream;-><init>(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)V

    .line 17
    const/4 v2, 0x0

    iput v2, p0, Lcom/vlingo/core/internal/audio/FileTestMicrophoneStream;->shortsRead:I

    .line 19
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/vlingo/core/internal/audio/FileTestMicrophoneStream;->lastTime:J

    .line 30
    const/4 v0, 0x0

    .line 32
    .local v0, "dis":Ljava/io/DataInputStream;
    :try_start_0
    new-instance v0, Ljava/io/DataInputStream;

    .end local v0    # "dis":Ljava/io/DataInputStream;
    new-instance v2, Ljava/io/BufferedInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getAudioSourceInfo()Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    .restart local v0    # "dis":Ljava/io/DataInputStream;
    :goto_0
    iput-object v0, p0, Lcom/vlingo/core/internal/audio/FileTestMicrophoneStream;->mDis:Ljava/io/DataInputStream;

    .line 39
    return-void

    .line 34
    .end local v0    # "dis":Ljava/io/DataInputStream;
    :catch_0
    move-exception v1

    .line 35
    .local v1, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 36
    const/4 v0, 0x0

    .restart local v0    # "dis":Ljava/io/DataInputStream;
    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/FileTestMicrophoneStream;->mDis:Ljava/io/DataInputStream;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/FileTestMicrophoneStream;->mDis:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V

    .line 48
    :cond_0
    invoke-super {p0}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    monitor-exit p0

    return-void

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized read()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/FileTestMicrophoneStream;->mDis:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->read()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized read([B)I
    .locals 1
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/FileTestMicrophoneStream;->mDis:Ljava/io/DataInputStream;

    invoke-virtual {v0, p1}, Ljava/io/DataInputStream;->read([B)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized read([BII)I
    .locals 1
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/FileTestMicrophoneStream;->mDis:Ljava/io/DataInputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/DataInputStream;->read([BII)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized read([SII)I
    .locals 14
    .param p1, "audioData"    # [S
    .param p2, "offsetInShorts"    # I
    .param p3, "sizeInShorts"    # I

    .prologue
    .line 53
    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 54
    .local v6, "now":J
    iget-wide v12, p0, Lcom/vlingo/core/internal/audio/FileTestMicrophoneStream;->lastTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-long v1, v6, v12

    .line 57
    .local v1, "duration":J
    const-wide/16 v12, 0xa

    cmp-long v12, v1, v12

    if-gez v12, :cond_0

    .line 59
    const-wide/16 v12, 0xa

    sub-long v10, v12, v1

    .line 60
    .local v10, "sleeptime":J
    :try_start_1
    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 65
    .end local v10    # "sleeptime":J
    :cond_0
    :goto_0
    const/4 v5, 0x0

    .line 66
    .local v5, "i":I
    const/4 v9, 0x0

    .line 68
    .local v9, "ret":I
    const/4 v5, 0x0

    move/from16 v8, p2

    .end local p2    # "offsetInShorts":I
    .local v8, "offsetInShorts":I
    :goto_1
    move/from16 v0, p3

    if-ge v5, v0, :cond_1

    .line 69
    add-int/lit8 p2, v8, 0x1

    .end local v8    # "offsetInShorts":I
    .restart local p2    # "offsetInShorts":I
    :try_start_2
    iget-object v12, p0, Lcom/vlingo/core/internal/audio/FileTestMicrophoneStream;->mDis:Ljava/io/DataInputStream;

    invoke-virtual {v12}, Ljava/io/DataInputStream;->readShort()S

    move-result v12

    aput-short v12, p1, v8
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 68
    add-int/lit8 v5, v5, 0x1

    move/from16 v8, p2

    .end local p2    # "offsetInShorts":I
    .restart local v8    # "offsetInShorts":I
    goto :goto_1

    :cond_1
    move/from16 p2, v8

    .line 81
    .end local v8    # "offsetInShorts":I
    .restart local p2    # "offsetInShorts":I
    :goto_2
    :try_start_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v12

    iput-wide v12, p0, Lcom/vlingo/core/internal/audio/FileTestMicrophoneStream;->lastTime:J

    .line 82
    iget v12, p0, Lcom/vlingo/core/internal/audio/FileTestMicrophoneStream;->shortsRead:I

    add-int/2addr v12, v5

    iput v12, p0, Lcom/vlingo/core/internal/audio/FileTestMicrophoneStream;->shortsRead:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 83
    if-ltz v9, :cond_2

    .line 84
    move v9, v5

    .line 86
    :cond_2
    monitor-exit p0

    return v9

    .line 72
    :catch_0
    move-exception v3

    .line 73
    .local v3, "e":Ljava/io/IOException;
    const/4 v9, -0x1

    .line 75
    :try_start_4
    iget-object v12, p0, Lcom/vlingo/core/internal/audio/FileTestMicrophoneStream;->mDis:Ljava/io/DataInputStream;

    invoke-virtual {v12}, Ljava/io/DataInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 77
    :catch_1
    move-exception v4

    .line 78
    .local v4, "e1":Ljava/io/IOException;
    :try_start_5
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 53
    .end local v1    # "duration":J
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "e1":Ljava/io/IOException;
    .end local v5    # "i":I
    .end local v6    # "now":J
    .end local v9    # "ret":I
    :catchall_0
    move-exception v12

    monitor-exit p0

    throw v12

    .line 62
    .restart local v1    # "duration":J
    .restart local v6    # "now":J
    .restart local v10    # "sleeptime":J
    :catch_2
    move-exception v12

    goto :goto_0
.end method

.method public startRecordingJB(I)V
    .locals 2
    .param p1, "audioSessionId"    # I

    .prologue
    .line 116
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "startRecordingJB is not supported on this stub class."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public startRecordingJBNew(I)V
    .locals 2
    .param p1, "audioSessionId"    # I

    .prologue
    .line 111
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "startRecordingJBNew is not supported on this stub class."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public startRecordingJBOld(I)V
    .locals 2
    .param p1, "audioSessionId"    # I

    .prologue
    .line 106
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "startRecordingJBOld is not supported on this stub class."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
