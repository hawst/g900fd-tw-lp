.class public interface abstract Lcom/vlingo/core/internal/questions/parser/AnswerParser$TAGS;
.super Ljava/lang/Object;
.source "AnswerParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/questions/parser/AnswerParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TAGS"
.end annotation


# static fields
.field public static final IMAGE:Ljava/lang/String; = "Image"

.field public static final IMAGES:Ljava/lang/String; = "Images"

.field public static final RESPONSE:Ljava/lang/String; = "ProviderSrcResponse"

.field public static final SECTION:Ljava/lang/String; = "Section"

.field public static final SECTIONS:Ljava/lang/String; = "Sections"

.field public static final SUB_SECTION:Ljava/lang/String; = "Subsection"

.field public static final SUB_SECTIONS:Ljava/lang/String; = "Subsections"
