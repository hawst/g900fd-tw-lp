.class final Lcom/vlingo/core/internal/bluetooth/BluetoothManager$1;
.super Ljava/lang/Object;
.source "BluetoothManager.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->bluetoothManagerInit()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 2
    .param p1, "profile"    # I
    .param p2, "proxy"    # Landroid/bluetooth/BluetoothProfile;

    .prologue
    .line 171
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    .line 172
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->notifyBluetoothServiceConnectedToListener()V

    .line 199
    .end local p2    # "proxy":Landroid/bluetooth/BluetoothProfile;
    :goto_0
    return-void

    .line 176
    .restart local p2    # "proxy":Landroid/bluetooth/BluetoothProfile;
    :cond_0
    check-cast p2, Landroid/bluetooth/BluetoothHeadset;

    .end local p2    # "proxy":Landroid/bluetooth/BluetoothProfile;
    # setter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;
    invoke-static {p2}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$102(Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/BluetoothHeadset;

    .line 179
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->getBTdevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    # setter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$202(Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    .line 180
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$200()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 184
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$100()Landroid/bluetooth/BluetoothHeadset;

    move-result-object v0

    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$200()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothHeadset;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 198
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->notifyBluetoothServiceConnectedToListener()V

    goto :goto_0
.end method

.method public onServiceDisconnected(I)V
    .locals 2
    .param p1, "profile"    # I

    .prologue
    .line 205
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$100()Landroid/bluetooth/BluetoothHeadset;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 206
    # getter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$100()Landroid/bluetooth/BluetoothHeadset;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    .line 207
    .local v0, "deviceList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 208
    const/4 v1, 0x0

    # setter for: Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;
    invoke-static {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->access$102(Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/BluetoothHeadset;

    .line 213
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_0
    return-void
.end method
