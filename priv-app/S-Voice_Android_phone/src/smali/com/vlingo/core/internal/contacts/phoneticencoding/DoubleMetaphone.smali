.class public Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;
.super Ljava/lang/Object;
.source "DoubleMetaphone.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    }
.end annotation


# static fields
.field private static final ES_EP_EB_EL_EY_IB_IL_IN_IE_EI_ER:[Ljava/lang/String;

.field private static final L_R_N_M_B_H_F_V_W_SPACE:[Ljava/lang/String;

.field private static final L_T_K_S_N_M_B_Z:[Ljava/lang/String;

.field private static final SILENT_START:[Ljava/lang/String;

.field private static final VOWELS:Ljava/lang/String; = "AEIOUY"


# instance fields
.field private final SEPARATOR:C

.field private firstVowelProcessed:Z

.field private lastVowelProcessed:Z

.field private maxCodeLen:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 44
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "GN"

    aput-object v1, v0, v3

    const-string/jumbo v1, "KN"

    aput-object v1, v0, v4

    const-string/jumbo v1, "PN"

    aput-object v1, v0, v5

    const-string/jumbo v1, "WR"

    aput-object v1, v0, v6

    const-string/jumbo v1, "PS"

    aput-object v1, v0, v7

    sput-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->SILENT_START:[Ljava/lang/String;

    .line 46
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "L"

    aput-object v1, v0, v3

    const-string/jumbo v1, "R"

    aput-object v1, v0, v4

    const-string/jumbo v1, "N"

    aput-object v1, v0, v5

    const-string/jumbo v1, "M"

    aput-object v1, v0, v6

    const-string/jumbo v1, "B"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "H"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "F"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "V"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "W"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, " "

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->L_R_N_M_B_H_F_V_W_SPACE:[Ljava/lang/String;

    .line 48
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "ES"

    aput-object v1, v0, v3

    const-string/jumbo v1, "EP"

    aput-object v1, v0, v4

    const-string/jumbo v1, "EB"

    aput-object v1, v0, v5

    const-string/jumbo v1, "EL"

    aput-object v1, v0, v6

    const-string/jumbo v1, "EY"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "IB"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "IL"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "IN"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "IE"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "EI"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "ER"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->ES_EP_EB_EL_EY_IB_IL_IN_IE_EI_ER:[Ljava/lang/String;

    .line 50
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "L"

    aput-object v1, v0, v3

    const-string/jumbo v1, "T"

    aput-object v1, v0, v4

    const-string/jumbo v1, "K"

    aput-object v1, v0, v5

    const-string/jumbo v1, "S"

    aput-object v1, v0, v6

    const-string/jumbo v1, "N"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "M"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "B"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "Z"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->L_T_K_S_N_M_B_Z:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/16 v0, 0x14

    iput v0, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->maxCodeLen:I

    .line 63
    const/16 v0, 0x20

    iput-char v0, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->SEPARATOR:C

    .line 80
    return-void
.end method

.method private checkOnVowelBeforeIndex(Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;ILjava/lang/String;)V
    .locals 2
    .param p1, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p2, "index"    # I
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 267
    iget-boolean v1, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->firstVowelProcessed:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    if-le p2, v1, :cond_0

    .line 268
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 269
    invoke-virtual {p3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->isVowel(C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 270
    invoke-direct {p0, p1, v0, p3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleAEIOUY(Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;ILjava/lang/String;)I

    .line 275
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 268
    .restart local v0    # "i":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private cleanInput(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1110
    if-nez p1, :cond_1

    .line 1117
    :cond_0
    :goto_0
    return-object v0

    .line 1113
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 1114
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1117
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private conditionC0(Ljava/lang/String;I)Z
    .locals 7
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1015
    const/4 v3, 0x4

    new-array v4, v2, [Ljava/lang/String;

    const-string/jumbo v5, "CHIA"

    aput-object v5, v4, v1

    invoke-static {p1, p2, v3, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v1, v2

    .line 1025
    :cond_0
    :goto_0
    return v1

    .line 1017
    :cond_1
    if-le p2, v2, :cond_0

    .line 1019
    add-int/lit8 v3, p2, -0x2

    invoke-virtual {p0, p1, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v3

    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->isVowel(C)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1021
    add-int/lit8 v3, p2, -0x1

    const/4 v4, 0x3

    new-array v5, v2, [Ljava/lang/String;

    const-string/jumbo v6, "ACH"

    aput-object v6, v5, v1

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1024
    add-int/lit8 v3, p2, 0x2

    invoke-virtual {p0, p1, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    .line 1025
    .local v0, "c":C
    const/16 v3, 0x49

    if-eq v0, v3, :cond_2

    const/16 v3, 0x45

    if-ne v0, v3, :cond_3

    :cond_2
    add-int/lit8 v3, p2, -0x2

    const/4 v4, 0x6

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const-string/jumbo v6, "BACHER"

    aput-object v6, v5, v1

    const-string/jumbo v6, "MACHER"

    aput-object v6, v5, v2

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method private conditionCH0(Ljava/lang/String;I)Z
    .locals 8
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1034
    if-nez p2, :cond_1

    add-int/lit8 v2, p2, 0x1

    new-array v3, v5, [Ljava/lang/String;

    const-string/jumbo v4, "HARAC"

    aput-object v4, v3, v1

    const-string/jumbo v4, "HARIS"

    aput-object v4, v3, v0

    invoke-static {p1, v2, v7, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    add-int/lit8 v2, p2, 0x1

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "HOR"

    aput-object v4, v3, v1

    const-string/jumbo v4, "HYM"

    aput-object v4, v3, v0

    const-string/jumbo v4, "HIA"

    aput-object v4, v3, v5

    const-string/jumbo v4, "HEM"

    aput-object v4, v3, v6

    invoke-static {p1, v2, v6, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v3, "CHORE"

    aput-object v3, v2, v1

    invoke-static {p1, v1, v7, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private conditionCH1(Ljava/lang/String;I)Z
    .locals 9
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1044
    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "VAN "

    aput-object v3, v2, v0

    const-string/jumbo v3, "VON "

    aput-object v3, v2, v1

    invoke-static {p1, v0, v8, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    new-array v2, v1, [Ljava/lang/String;

    const-string/jumbo v3, "SCH"

    aput-object v3, v2, v0

    invoke-static {p1, v0, v7, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    add-int/lit8 v2, p2, -0x2

    const/4 v3, 0x6

    new-array v4, v7, [Ljava/lang/String;

    const-string/jumbo v5, "ORCHES"

    aput-object v5, v4, v0

    const-string/jumbo v5, "ARCHIT"

    aput-object v5, v4, v1

    const-string/jumbo v5, "ORCHID"

    aput-object v5, v4, v6

    invoke-static {p1, v2, v3, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    add-int/lit8 v2, p2, 0x2

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "T"

    aput-object v4, v3, v0

    const-string/jumbo v4, "S"

    aput-object v4, v3, v1

    invoke-static {p1, v2, v1, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    add-int/lit8 v2, p2, -0x1

    new-array v3, v8, [Ljava/lang/String;

    const-string/jumbo v4, "A"

    aput-object v4, v3, v0

    const-string/jumbo v4, "O"

    aput-object v4, v3, v1

    const-string/jumbo v4, "U"

    aput-object v4, v3, v6

    const-string/jumbo v4, "E"

    aput-object v4, v3, v7

    invoke-static {p1, v2, v1, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    if-nez p2, :cond_2

    :cond_0
    add-int/lit8 v2, p2, 0x2

    sget-object v3, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->L_R_N_M_B_H_F_V_W_SPACE:[Ljava/lang/String;

    invoke-static {p1, v2, v1, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    add-int/lit8 v2, p2, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    return v0
.end method

.method private conditionL0(Ljava/lang/String;I)Z
    .locals 7
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1055
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x3

    if-ne p2, v2, :cond_0

    add-int/lit8 v2, p2, -0x1

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "ILLO"

    aput-object v4, v3, v0

    const-string/jumbo v4, "ILLA"

    aput-object v4, v3, v1

    const-string/jumbo v4, "ALLE"

    aput-object v4, v3, v5

    invoke-static {p1, v2, v6, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    new-array v3, v5, [Ljava/lang/String;

    const-string/jumbo v4, "AS"

    aput-object v4, v3, v0

    const-string/jumbo v4, "OS"

    aput-object v4, v3, v1

    invoke-static {p1, v2, v5, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    new-array v3, v5, [Ljava/lang/String;

    const-string/jumbo v4, "A"

    aput-object v4, v3, v0

    const-string/jumbo v4, "O"

    aput-object v4, v3, v1

    invoke-static {p1, v2, v1, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    add-int/lit8 v2, p2, -0x1

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v4, "ALLE"

    aput-object v4, v3, v0

    invoke-static {p1, v2, v6, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v0, v1

    :cond_3
    return v0
.end method

.method private conditionM0(Ljava/lang/String;I)Z
    .locals 6
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1065
    add-int/lit8 v2, p2, 0x1

    invoke-virtual {p0, p1, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v2

    const/16 v3, 0x4d

    if-ne v2, v3, :cond_1

    .line 1068
    :cond_0
    :goto_0
    return v0

    :cond_1
    add-int/lit8 v2, p2, -0x1

    const/4 v3, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const-string/jumbo v5, "UMB"

    aput-object v5, v4, v1

    invoke-static {p1, v2, v3, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    add-int/lit8 v2, p2, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-eq v2, v3, :cond_0

    add-int/lit8 v2, p2, 0x2

    const/4 v3, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const-string/jumbo v5, "ER"

    aput-object v5, v4, v1

    invoke-static {p1, v2, v3, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected static varargs contains(Ljava/lang/String;II[Ljava/lang/String;)Z
    .locals 8
    .param p0, "value"    # Ljava/lang/String;
    .param p1, "start"    # I
    .param p2, "length"    # I
    .param p3, "criteria"    # [Ljava/lang/String;

    .prologue
    .line 1138
    const/4 v4, 0x0

    .line 1139
    .local v4, "result":Z
    if-ltz p1, :cond_0

    add-int v6, p1, p2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    if-gt v6, v7, :cond_0

    .line 1140
    add-int v6, p1, p2

    invoke-virtual {p0, p1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 1142
    .local v5, "target":Ljava/lang/String;
    move-object v0, p3

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 1143
    .local v1, "element":Ljava/lang/String;
    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1144
    const/4 v4, 0x1

    .line 1149
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "element":Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v5    # "target":Ljava/lang/String;
    :cond_0
    return v4

    .line 1142
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "element":Ljava/lang/String;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    .restart local v5    # "target":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private handleAEIOUY(Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;ILjava/lang/String;)I
    .locals 1
    .param p1, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p2, "index"    # I
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 301
    iget-boolean v0, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->firstVowelProcessed:Z

    if-nez v0, :cond_0

    .line 302
    invoke-direct {p0, p2, p3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->isOnlyLastVowel(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 303
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleFirstVowel(Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;ILjava/lang/String;)V

    .line 306
    :cond_0
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_1

    iget-boolean v0, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->lastVowelProcessed:Z

    if-nez v0, :cond_1

    .line 307
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleLastVowel(Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;ILjava/lang/String;)V

    .line 313
    :cond_1
    add-int/lit8 v0, p2, 0x1

    return v0
.end method

.method private handleC(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I
    .locals 9
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p3, "index"    # I

    .prologue
    const/16 v8, 0x53

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 470
    invoke-direct {p0, p1, p3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->conditionC0(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 471
    const/16 v0, 0x4b

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 472
    add-int/lit8 p3, p3, 0x2

    :goto_0
    move v0, p3

    .line 515
    :goto_1
    return v0

    .line 473
    :cond_0
    if-nez p3, :cond_1

    const/4 v0, 0x6

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "CAESAR"

    aput-object v2, v1, v5

    invoke-static {p1, p3, v0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 474
    invoke-virtual {p2, v8}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 475
    add-int/lit8 p3, p3, 0x2

    goto :goto_0

    .line 476
    :cond_1
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "CH"

    aput-object v1, v0, v5

    invoke-static {p1, p3, v6, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 477
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleCH(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I

    move-result p3

    goto :goto_0

    .line 478
    :cond_2
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "CZ"

    aput-object v1, v0, v5

    invoke-static {p1, p3, v6, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    add-int/lit8 v0, p3, -0x2

    const/4 v1, 0x4

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "WICZ"

    aput-object v3, v2, v5

    invoke-static {p1, v0, v1, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 481
    const/16 v0, 0x58

    invoke-virtual {p2, v8, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(CC)V

    .line 482
    add-int/lit8 p3, p3, 0x2

    goto :goto_0

    .line 483
    :cond_3
    add-int/lit8 v0, p3, 0x1

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "CIA"

    aput-object v2, v1, v5

    invoke-static {p1, v0, v7, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 485
    const/16 v0, 0x58

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 486
    add-int/lit8 p3, p3, 0x3

    goto :goto_0

    .line 487
    :cond_4
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "CC"

    aput-object v1, v0, v5

    invoke-static {p1, p3, v6, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    if-ne p3, v4, :cond_5

    invoke-virtual {p0, p1, v5}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x4d

    if-eq v0, v1, :cond_6

    .line 490
    :cond_5
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleCC(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I

    move-result v0

    goto :goto_1

    .line 491
    :cond_6
    new-array v0, v7, [Ljava/lang/String;

    const-string/jumbo v1, "CK"

    aput-object v1, v0, v5

    const-string/jumbo v1, "CG"

    aput-object v1, v0, v4

    const-string/jumbo v1, "CQ"

    aput-object v1, v0, v6

    invoke-static {p1, p3, v6, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 492
    const/16 v0, 0x4b

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 493
    add-int/lit8 p3, p3, 0x2

    goto/16 :goto_0

    .line 494
    :cond_7
    new-array v0, v7, [Ljava/lang/String;

    const-string/jumbo v1, "CI"

    aput-object v1, v0, v5

    const-string/jumbo v1, "CE"

    aput-object v1, v0, v4

    const-string/jumbo v1, "CY"

    aput-object v1, v0, v6

    invoke-static {p1, p3, v6, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 496
    new-array v0, v7, [Ljava/lang/String;

    const-string/jumbo v1, "CIO"

    aput-object v1, v0, v5

    const-string/jumbo v1, "CIE"

    aput-object v1, v0, v4

    const-string/jumbo v1, "CIA"

    aput-object v1, v0, v6

    invoke-static {p1, p3, v7, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 497
    const/16 v0, 0x58

    invoke-virtual {p2, v8, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(CC)V

    .line 501
    :goto_2
    add-int/lit8 p3, p3, 0x2

    goto/16 :goto_0

    .line 499
    :cond_8
    invoke-virtual {p2, v8}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_2

    .line 503
    :cond_9
    const/16 v0, 0x4b

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 504
    add-int/lit8 v0, p3, 0x1

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, " C"

    aput-object v2, v1, v5

    const-string/jumbo v2, " Q"

    aput-object v2, v1, v4

    const-string/jumbo v2, " G"

    aput-object v2, v1, v6

    invoke-static {p1, v0, v6, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 506
    add-int/lit8 p3, p3, 0x3

    goto/16 :goto_0

    .line 507
    :cond_a
    add-int/lit8 v0, p3, 0x1

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "C"

    aput-object v2, v1, v5

    const-string/jumbo v2, "K"

    aput-object v2, v1, v4

    const-string/jumbo v2, "Q"

    aput-object v2, v1, v6

    invoke-static {p1, v0, v4, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    add-int/lit8 v0, p3, 0x1

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "CE"

    aput-object v2, v1, v5

    const-string/jumbo v2, "CI"

    aput-object v2, v1, v4

    invoke-static {p1, v0, v6, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 509
    add-int/lit8 p3, p3, 0x2

    goto/16 :goto_0

    .line 511
    :cond_b
    add-int/lit8 p3, p3, 0x1

    goto/16 :goto_0
.end method

.method private handleCC(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I
    .locals 6
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p3, "index"    # I

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 522
    add-int/lit8 v0, p3, 0x2

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "I"

    aput-object v2, v1, v5

    const-string/jumbo v2, "E"

    aput-object v2, v1, v4

    const-string/jumbo v2, "H"

    aput-object v2, v1, v3

    invoke-static {p1, v0, v4, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    add-int/lit8 v0, p3, 0x2

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "HU"

    aput-object v2, v1, v5

    invoke-static {p1, v0, v3, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 525
    if-ne p3, v4, :cond_0

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x41

    if-eq v0, v1, :cond_1

    :cond_0
    add-int/lit8 v0, p3, -0x1

    const/4 v1, 0x5

    new-array v2, v3, [Ljava/lang/String;

    const-string/jumbo v3, "UCCEE"

    aput-object v3, v2, v5

    const-string/jumbo v3, "UCCES"

    aput-object v3, v2, v4

    invoke-static {p1, v0, v1, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 528
    :cond_1
    const-string/jumbo v0, "KS"

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    .line 533
    :goto_0
    add-int/lit8 p3, p3, 0x3

    .line 539
    :goto_1
    return p3

    .line 531
    :cond_2
    const/16 v0, 0x58

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_0

    .line 535
    :cond_3
    const/16 v0, 0x4b

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 536
    add-int/lit8 p3, p3, 0x2

    goto :goto_1
.end method

.method private handleCH(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I
    .locals 7
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p3, "index"    # I

    .prologue
    const/4 v6, 0x1

    const/16 v5, 0x58

    const/4 v4, 0x0

    const/16 v3, 0x4b

    .line 546
    if-lez p3, :cond_0

    const/4 v0, 0x4

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "CHAE"

    aput-object v2, v1, v4

    invoke-static {p1, p3, v0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 547
    invoke-virtual {p2, v3, v5}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(CC)V

    .line 548
    add-int/lit8 v0, p3, 0x2

    .line 567
    :goto_0
    return v0

    .line 549
    :cond_0
    invoke-direct {p0, p1, p3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->conditionCH0(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 551
    invoke-virtual {p2, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 552
    add-int/lit8 v0, p3, 0x2

    goto :goto_0

    .line 553
    :cond_1
    invoke-direct {p0, p1, p3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->conditionCH1(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 555
    invoke-virtual {p2, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 556
    add-int/lit8 v0, p3, 0x2

    goto :goto_0

    .line 558
    :cond_2
    if-lez p3, :cond_4

    .line 559
    const/4 v0, 0x2

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "MC"

    aput-object v2, v1, v4

    invoke-static {p1, v4, v0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 560
    invoke-virtual {p2, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 567
    :goto_1
    add-int/lit8 v0, p3, 0x2

    goto :goto_0

    .line 562
    :cond_3
    invoke-virtual {p2, v5, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(CC)V

    goto :goto_1

    .line 565
    :cond_4
    invoke-virtual {p2, v5}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_1
.end method

.method private handleD(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I
    .locals 6
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p3, "index"    # I

    .prologue
    const/16 v2, 0x54

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 575
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "DG"

    aput-object v1, v0, v5

    invoke-static {p1, p3, v4, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 577
    add-int/lit8 v0, p3, 0x2

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "I"

    aput-object v2, v1, v5

    const-string/jumbo v2, "E"

    aput-object v2, v1, v3

    const-string/jumbo v2, "Y"

    aput-object v2, v1, v4

    invoke-static {p1, v0, v3, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 578
    const/16 v0, 0x4a

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 579
    add-int/lit8 p3, p3, 0x3

    .line 592
    :goto_0
    return p3

    .line 582
    :cond_0
    const-string/jumbo v0, "TK"

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    .line 583
    add-int/lit8 p3, p3, 0x2

    goto :goto_0

    .line 585
    :cond_1
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "DT"

    aput-object v1, v0, v5

    const-string/jumbo v1, "DD"

    aput-object v1, v0, v3

    invoke-static {p1, p3, v4, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 586
    invoke-virtual {p2, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 587
    add-int/lit8 p3, p3, 0x2

    goto :goto_0

    .line 589
    :cond_2
    invoke-virtual {p2, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 590
    add-int/lit8 p3, p3, 0x1

    goto :goto_0
.end method

.method private handleFirstVowel(Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;ILjava/lang/String;)V
    .locals 6
    .param p1, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p2, "index"    # I
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0x49

    const/16 v4, 0x41

    const/16 v2, 0x4f

    const/16 v3, 0x55

    .line 321
    invoke-virtual {p3, p2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 373
    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->firstVowelProcessed:Z

    .line 374
    return-void

    .line 323
    :sswitch_0
    invoke-virtual {p1, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_0

    .line 326
    :sswitch_1
    add-int/lit8 v1, p2, 0x1

    invoke-virtual {p0, p3, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v1

    const/16 v2, 0x45

    if-ne v1, v2, :cond_0

    .line 328
    invoke-virtual {p1, v5}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_0

    .line 329
    :cond_0
    add-int/lit8 v1, p2, 0x1

    invoke-virtual {p0, p3, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v1

    if-ne v1, v3, :cond_1

    .line 331
    invoke-virtual {p1, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_0

    .line 333
    :cond_1
    invoke-virtual {p1, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_0

    .line 337
    :sswitch_2
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, p3, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    .line 338
    .local v0, "lastChar":C
    add-int/lit8 v1, p2, 0x1

    invoke-virtual {p0, p3, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v1

    if-ne v1, v2, :cond_2

    .line 340
    const-string/jumbo v1, "AI"

    invoke-virtual {p1, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto :goto_0

    .line 341
    :cond_2
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->isVowel(C)Z

    move-result v1

    if-eqz v1, :cond_3

    if-eq v0, v4, :cond_3

    .line 342
    const-string/jumbo v1, "AI"

    invoke-virtual {p1, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto :goto_0

    .line 344
    :cond_3
    invoke-virtual {p1, v5}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_0

    .line 348
    .end local v0    # "lastChar":C
    :sswitch_3
    add-int/lit8 v1, p2, 0x1

    invoke-virtual {p0, p3, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v1

    if-ne v1, v2, :cond_4

    .line 350
    invoke-virtual {p1, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_0

    .line 352
    :cond_4
    invoke-virtual {p1, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_0

    .line 356
    :sswitch_4
    invoke-virtual {p1, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_0

    .line 359
    :sswitch_5
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, p3, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    .line 360
    .restart local v0    # "lastChar":C
    add-int/lit8 v1, p2, 0x1

    invoke-virtual {p0, p3, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v1

    if-ne v1, v2, :cond_5

    .line 362
    invoke-virtual {p1, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_0

    .line 363
    :cond_5
    add-int/lit8 v1, p2, 0x1

    invoke-virtual {p0, p3, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v1

    if-ne v1, v3, :cond_6

    .line 365
    invoke-virtual {p1, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto/16 :goto_0

    .line 366
    :cond_6
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->isVowel(C)Z

    move-result v1

    if-eqz v1, :cond_7

    if-eq v0, v4, :cond_7

    .line 367
    const-string/jumbo v1, "AI"

    invoke-virtual {p1, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 369
    :cond_7
    invoke-virtual {p1, v5}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto/16 :goto_0

    .line 321
    nop

    :sswitch_data_0
    .sparse-switch
        0x41 -> :sswitch_0
        0x45 -> :sswitch_1
        0x49 -> :sswitch_2
        0x4f -> :sswitch_3
        0x55 -> :sswitch_4
        0x59 -> :sswitch_5
    .end sparse-switch
.end method

.method private handleG(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;IZ)I
    .locals 9
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p3, "index"    # I
    .param p4, "slavoGermanic"    # Z

    .prologue
    const/16 v8, 0x4a

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 600
    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x48

    if-ne v0, v1, :cond_0

    .line 601
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleGH(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I

    move-result p3

    .line 653
    :goto_0
    return p3

    .line 602
    :cond_0
    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x4e

    if-ne v0, v1, :cond_3

    .line 603
    if-ne p3, v5, :cond_1

    invoke-virtual {p0, p1, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->isVowel(C)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p4, :cond_1

    .line 604
    const-string/jumbo v0, "KN"

    const-string/jumbo v1, "N"

    invoke-virtual {p2, v0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    :goto_1
    add-int/lit8 p3, p3, 0x2

    goto :goto_0

    .line 605
    :cond_1
    add-int/lit8 v0, p3, 0x2

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "EY"

    aput-object v2, v1, v4

    invoke-static {p1, v0, v6, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x59

    if-eq v0, v1, :cond_2

    if-nez p4, :cond_2

    .line 607
    const-string/jumbo v0, "N"

    const-string/jumbo v1, "KN"

    invoke-virtual {p2, v0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 609
    :cond_2
    const-string/jumbo v0, "KN"

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto :goto_1

    .line 612
    :cond_3
    add-int/lit8 v0, p3, 0x1

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "LI"

    aput-object v2, v1, v4

    invoke-static {p1, v0, v6, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    if-nez p4, :cond_4

    .line 613
    const-string/jumbo v0, "KL"

    const-string/jumbo v1, "L"

    invoke-virtual {p2, v0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    add-int/lit8 p3, p3, 0x2

    goto :goto_0

    .line 615
    :cond_4
    if-nez p3, :cond_6

    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x59

    if-eq v0, v1, :cond_5

    add-int/lit8 v0, p3, 0x1

    sget-object v1, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->ES_EP_EB_EL_EY_IB_IL_IN_IE_EI_ER:[Ljava/lang/String;

    invoke-static {p1, v0, v6, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 619
    :cond_5
    invoke-virtual {p2, v8}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 620
    add-int/lit8 p3, p3, 0x2

    goto/16 :goto_0

    .line 621
    :cond_6
    add-int/lit8 v0, p3, 0x1

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "ER"

    aput-object v2, v1, v4

    invoke-static {p1, v0, v6, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x59

    if-ne v0, v1, :cond_8

    :cond_7
    const/4 v0, 0x6

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "DANGER"

    aput-object v2, v1, v4

    const-string/jumbo v2, "RANGER"

    aput-object v2, v1, v5

    const-string/jumbo v2, "MANGER"

    aput-object v2, v1, v6

    invoke-static {p1, v4, v0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    add-int/lit8 v0, p3, -0x1

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "E"

    aput-object v2, v1, v4

    const-string/jumbo v2, "I"

    aput-object v2, v1, v5

    invoke-static {p1, v0, v5, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    add-int/lit8 v0, p3, -0x1

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "RGY"

    aput-object v2, v1, v4

    const-string/jumbo v2, "OGY"

    aput-object v2, v1, v5

    invoke-static {p1, v0, v7, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 627
    const/16 v0, 0x4b

    invoke-virtual {p2, v0, v8}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(CC)V

    .line 628
    add-int/lit8 p3, p3, 0x2

    goto/16 :goto_0

    .line 629
    :cond_8
    add-int/lit8 v0, p3, 0x1

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "E"

    aput-object v2, v1, v4

    const-string/jumbo v2, "I"

    aput-object v2, v1, v5

    const-string/jumbo v2, "Y"

    aput-object v2, v1, v6

    invoke-static {p1, v0, v5, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    add-int/lit8 v0, p3, -0x1

    const/4 v1, 0x4

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "AGGI"

    aput-object v3, v2, v4

    const-string/jumbo v3, "OGGI"

    aput-object v3, v2, v5

    invoke-static {p1, v0, v1, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 632
    :cond_9
    const/4 v0, 0x4

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "VAN "

    aput-object v2, v1, v4

    const-string/jumbo v2, "VON "

    aput-object v2, v1, v5

    invoke-static {p1, v4, v0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "SCH"

    aput-object v1, v0, v4

    invoke-static {p1, v4, v7, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    add-int/lit8 v0, p3, 0x1

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "ET"

    aput-object v2, v1, v4

    invoke-static {p1, v0, v6, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 636
    :cond_a
    const/16 v0, 0x4b

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 642
    :goto_2
    add-int/lit8 p3, p3, 0x2

    goto/16 :goto_0

    .line 637
    :cond_b
    add-int/lit8 v0, p3, 0x1

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "IER"

    aput-object v2, v1, v4

    invoke-static {p1, v0, v7, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 638
    invoke-virtual {p2, v8}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_2

    .line 640
    :cond_c
    const/16 v0, 0x4b

    invoke-virtual {p2, v8, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(CC)V

    goto :goto_2

    .line 643
    :cond_d
    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x47

    if-ne v0, v1, :cond_e

    .line 644
    add-int/lit8 p3, p3, 0x2

    .line 645
    const/16 v0, 0x47

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto/16 :goto_0

    .line 646
    :cond_e
    if-nez p3, :cond_f

    new-array v0, v7, [Ljava/lang/String;

    const-string/jumbo v1, "GE"

    aput-object v1, v0, v4

    const-string/jumbo v1, "GI"

    aput-object v1, v0, v5

    const-string/jumbo v1, "GY"

    aput-object v1, v0, v6

    invoke-static {p1, p3, v6, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 647
    add-int/lit8 p3, p3, 0x1

    .line 648
    invoke-virtual {p2, v8}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto/16 :goto_0

    .line 650
    :cond_f
    add-int/lit8 p3, p3, 0x1

    .line 651
    const/16 v0, 0x47

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto/16 :goto_0
.end method

.method private handleGH(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I
    .locals 8
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p3, "index"    # I

    .prologue
    const/16 v7, 0x49

    const/4 v6, 0x3

    const/4 v3, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 660
    if-lez p3, :cond_0

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->isVowel(C)Z

    move-result v0

    if-nez v0, :cond_0

    .line 661
    const/16 v0, 0x4b

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 662
    add-int/lit8 p3, p3, 0x2

    .line 692
    :goto_0
    return p3

    .line 663
    :cond_0
    if-nez p3, :cond_2

    .line 664
    add-int/lit8 v0, p3, 0x2

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v7, :cond_1

    .line 665
    const/16 v0, 0x4a

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 669
    :goto_1
    add-int/lit8 p3, p3, 0x2

    goto :goto_0

    .line 667
    :cond_1
    const/16 v0, 0x4b

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_1

    .line 670
    :cond_2
    if-le p3, v4, :cond_3

    add-int/lit8 v0, p3, -0x2

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "B"

    aput-object v2, v1, v5

    const-string/jumbo v2, "H"

    aput-object v2, v1, v4

    const-string/jumbo v2, "D"

    aput-object v2, v1, v3

    invoke-static {p1, v0, v4, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_3
    if-le p3, v3, :cond_4

    add-int/lit8 v0, p3, -0x3

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "B"

    aput-object v2, v1, v5

    const-string/jumbo v2, "H"

    aput-object v2, v1, v4

    const-string/jumbo v2, "D"

    aput-object v2, v1, v3

    invoke-static {p1, v0, v4, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    if-le p3, v6, :cond_6

    add-int/lit8 v0, p3, -0x4

    new-array v1, v3, [Ljava/lang/String;

    const-string/jumbo v2, "B"

    aput-object v2, v1, v5

    const-string/jumbo v2, "H"

    aput-object v2, v1, v4

    invoke-static {p1, v0, v4, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 674
    :cond_5
    add-int/lit8 p3, p3, 0x2

    goto :goto_0

    .line 676
    :cond_6
    if-le p3, v3, :cond_8

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x55

    if-ne v0, v1, :cond_8

    add-int/lit8 v0, p3, -0x3

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "C"

    aput-object v2, v1, v5

    const-string/jumbo v2, "G"

    aput-object v2, v1, v4

    const-string/jumbo v2, "L"

    aput-object v2, v1, v3

    const-string/jumbo v2, "R"

    aput-object v2, v1, v6

    const/4 v2, 0x4

    const-string/jumbo v3, "T"

    aput-object v3, v1, v2

    invoke-static {p1, v0, v4, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 679
    const/16 v0, 0x46

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 689
    :cond_7
    :goto_2
    add-int/lit8 p3, p3, 0x2

    goto/16 :goto_0

    .line 680
    :cond_8
    if-lez p3, :cond_9

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-eq v0, v7, :cond_9

    .line 681
    const/16 v0, 0x4b

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_2

    .line 682
    :cond_9
    add-int/lit8 v0, p3, 0x2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v0, v1, :cond_a

    add-int/lit8 v0, p3, -0x2

    const/4 v1, 0x4

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "EIGH"

    aput-object v3, v2, v5

    invoke-static {p1, v0, v1, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 683
    invoke-virtual {p2, v7}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 684
    iput-boolean v4, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->lastVowelProcessed:Z

    goto :goto_2

    .line 685
    :cond_a
    add-int/lit8 v0, p3, 0x2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v0, v1, :cond_7

    add-int/lit8 v0, p3, -0x1

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "AGH"

    aput-object v2, v1, v5

    invoke-static {p1, v0, v6, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 686
    const/16 v0, 0x41

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 687
    iput-boolean v4, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->lastVowelProcessed:Z

    goto :goto_2
.end method

.method private handleH(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I
    .locals 3
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p3, "index"    # I

    .prologue
    const/16 v2, 0x41

    .line 700
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p3, v0, :cond_2

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v2, :cond_2

    .line 701
    add-int/lit8 v0, p3, -0x2

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x49

    if-eq v0, v1, :cond_0

    add-int/lit8 v0, p3, -0x2

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x59

    if-eq v0, v1, :cond_0

    add-int/lit8 v0, p3, -0x2

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x45

    if-ne v0, v1, :cond_1

    .line 702
    :cond_0
    const-string/jumbo v0, "IA"

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    .line 706
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->lastVowelProcessed:Z

    .line 707
    add-int/lit8 p3, p3, 0x1

    .line 719
    :goto_1
    return p3

    .line 704
    :cond_1
    invoke-virtual {p2, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_0

    .line 710
    :cond_2
    if-eqz p3, :cond_3

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->isVowel(C)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->isVowel(C)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 712
    const/16 v0, 0x48

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 713
    add-int/lit8 p3, p3, 0x2

    goto :goto_1

    .line 716
    :cond_4
    add-int/lit8 p3, p3, 0x1

    goto :goto_1
.end method

.method private handleJ(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;IZ)I
    .locals 7
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p3, "index"    # I
    .param p4, "slavoGermanic"    # Z

    .prologue
    const/16 v6, 0x41

    const/4 v2, 0x4

    const/16 v5, 0x4a

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 727
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "JOSE"

    aput-object v1, v0, v3

    invoke-static {p1, p3, v2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "SAN "

    aput-object v1, v0, v3

    invoke-static {p1, v3, v2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 729
    :cond_0
    if-nez p3, :cond_1

    add-int/lit8 v0, p3, 0x4

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x20

    if-eq v0, v1, :cond_2

    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eq v0, v2, :cond_2

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "SAN "

    aput-object v1, v0, v3

    invoke-static {p1, v3, v2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 731
    :cond_2
    const/16 v0, 0x48

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 735
    :goto_0
    add-int/lit8 p3, p3, 0x1

    .line 761
    :goto_1
    return p3

    .line 733
    :cond_3
    const/16 v0, 0x48

    invoke-virtual {p2, v5, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(CC)V

    goto :goto_0

    .line 737
    :cond_4
    if-nez p3, :cond_6

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "JOSE"

    aput-object v1, v0, v3

    invoke-static {p1, p3, v2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 738
    invoke-virtual {p2, v5, v6}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(CC)V

    .line 755
    :cond_5
    :goto_2
    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v5, :cond_b

    .line 756
    add-int/lit8 p3, p3, 0x2

    goto :goto_1

    .line 739
    :cond_6
    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->isVowel(C)Z

    move-result v0

    if-eqz v0, :cond_9

    if-nez p4, :cond_9

    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-eq v0, v6, :cond_7

    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x4f

    if-ne v0, v1, :cond_9

    .line 742
    :cond_7
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    if-ne p3, v0, :cond_8

    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v6, :cond_8

    .line 743
    const-string/jumbo v0, "IA"

    const-string/jumbo v1, "H"

    invoke-virtual {p2, v0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;Ljava/lang/String;)V

    .line 744
    iput-boolean v4, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->lastVowelProcessed:Z

    goto :goto_2

    .line 746
    :cond_8
    const/16 v0, 0x48

    invoke-virtual {p2, v5, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(CC)V

    goto :goto_2

    .line 748
    :cond_9
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p3, v0, :cond_a

    .line 749
    const/16 v0, 0x20

    invoke-virtual {p2, v5, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(CC)V

    goto :goto_2

    .line 750
    :cond_a
    add-int/lit8 v0, p3, 0x1

    sget-object v1, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->L_T_K_S_N_M_B_Z:[Ljava/lang/String;

    invoke-static {p1, v0, v4, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    add-int/lit8 v0, p3, -0x1

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "S"

    aput-object v2, v1, v3

    const-string/jumbo v2, "K"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string/jumbo v3, "L"

    aput-object v3, v1, v2

    invoke-static {p1, v0, v4, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 752
    invoke-virtual {p2, v5}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto/16 :goto_2

    .line 758
    :cond_b
    add-int/lit8 p3, p3, 0x1

    goto/16 :goto_1
.end method

.method private handleL(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I
    .locals 2
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p3, "index"    # I

    .prologue
    const/16 v1, 0x4c

    .line 768
    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v1, :cond_1

    .line 769
    invoke-direct {p0, p1, p3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->conditionL0(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 770
    invoke-virtual {p2, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->appendPrimary(C)V

    .line 774
    :goto_0
    add-int/lit8 p3, p3, 0x2

    .line 779
    :goto_1
    return p3

    .line 772
    :cond_0
    invoke-virtual {p2, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_0

    .line 776
    :cond_1
    add-int/lit8 p3, p3, 0x1

    .line 777
    invoke-virtual {p2, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_1
.end method

.method private handleLastVowel(Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;ILjava/lang/String;)V
    .locals 6
    .param p1, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p2, "index"    # I
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0x45

    const/16 v4, 0x55

    const/16 v1, 0x49

    const/16 v3, 0x4f

    const/16 v2, 0x41

    .line 380
    invoke-virtual {p3, p2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 463
    :cond_0
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->lastVowelProcessed:Z

    .line 464
    return-void

    .line 382
    :sswitch_0
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, p3, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-eq v0, v1, :cond_1

    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, p3, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x59

    if-eq v0, v1, :cond_1

    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, p3, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v5, :cond_2

    .line 384
    :cond_1
    const-string/jumbo v0, "IA"

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto :goto_0

    .line 385
    :cond_2
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, p3, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v4, :cond_3

    .line 387
    const-string/jumbo v0, "UA"

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto :goto_0

    .line 389
    :cond_3
    invoke-virtual {p1, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_0

    .line 393
    :sswitch_1
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, p3, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v1, :cond_6

    .line 394
    add-int/lit8 v0, p2, -0x2

    invoke-virtual {p0, p3, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v4, :cond_4

    .line 396
    const-string/jumbo v0, "UI"

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto :goto_0

    .line 397
    :cond_4
    add-int/lit8 v0, p2, -0x2

    invoke-virtual {p0, p3, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v3, :cond_5

    .line 399
    const-string/jumbo v0, "OI"

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto :goto_0

    .line 402
    :cond_5
    const-string/jumbo v0, "I"

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto :goto_0

    .line 404
    :cond_6
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, p3, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v5, :cond_7

    .line 406
    const-string/jumbo v0, "I"

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto :goto_0

    .line 407
    :cond_7
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, p3, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v4, :cond_8

    .line 409
    const-string/jumbo v0, "U"

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 410
    :cond_8
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, p3, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x59

    if-ne v0, v1, :cond_b

    .line 411
    add-int/lit8 v0, p2, -0x2

    invoke-virtual {p0, p3, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v3, :cond_9

    .line 413
    const-string/jumbo v0, "OY"

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 414
    :cond_9
    add-int/lit8 v0, p2, -0x2

    invoke-virtual {p0, p3, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v2, :cond_a

    .line 416
    const-string/jumbo v0, "AY"

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 419
    :cond_a
    const-string/jumbo v0, "I"

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 421
    :cond_b
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, p3, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v3, :cond_c

    .line 423
    const-string/jumbo v0, "O"

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 424
    :cond_c
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, p3, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v2, :cond_0

    .line 426
    const-string/jumbo v0, "AY"

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 430
    :sswitch_2
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, p3, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v2, :cond_d

    .line 432
    const-string/jumbo v0, "AY"

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 434
    :cond_d
    const-string/jumbo v0, "I"

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 438
    :sswitch_3
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, p3, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v1, :cond_e

    .line 440
    const-string/jumbo v0, "IO"

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 442
    :cond_e
    const-string/jumbo v0, "O"

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 446
    :sswitch_4
    invoke-virtual {p1, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto/16 :goto_0

    .line 449
    :sswitch_5
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, p3, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v5, :cond_f

    add-int/lit8 v0, p2, -0x2

    invoke-virtual {p0, p3, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v3, :cond_f

    .line 451
    const-string/jumbo v0, "OI"

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 452
    :cond_f
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, p3, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v3, :cond_10

    .line 454
    const-string/jumbo v0, "OY"

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 455
    :cond_10
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, p3, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v2, :cond_11

    .line 457
    const-string/jumbo v0, "AY"

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 459
    :cond_11
    invoke-virtual {p1, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto/16 :goto_0

    .line 380
    nop

    :sswitch_data_0
    .sparse-switch
        0x41 -> :sswitch_0
        0x45 -> :sswitch_1
        0x49 -> :sswitch_2
        0x4f -> :sswitch_3
        0x55 -> :sswitch_4
        0x59 -> :sswitch_5
    .end sparse-switch
.end method

.method private handleP(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I
    .locals 5
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p3, "index"    # I

    .prologue
    const/4 v4, 0x1

    .line 786
    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x48

    if-ne v0, v1, :cond_0

    .line 787
    const/16 v0, 0x46

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 788
    add-int/lit8 p3, p3, 0x2

    .line 793
    :goto_0
    return p3

    .line 790
    :cond_0
    const/16 v0, 0x50

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 791
    add-int/lit8 v0, p3, 0x1

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "P"

    aput-object v3, v1, v2

    const-string/jumbo v2, "B"

    aput-object v2, v1, v4

    invoke-static {p1, v0, v4, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    add-int/lit8 p3, p3, 0x2

    :goto_1
    goto :goto_0

    :cond_1
    add-int/lit8 p3, p3, 0x1

    goto :goto_1
.end method

.method private handleR(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;IZ)I
    .locals 7
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p3, "index"    # I
    .param p4, "slavoGermanic"    # Z

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v4, 0x52

    const/4 v3, 0x2

    .line 801
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p3, v0, :cond_0

    if-nez p4, :cond_0

    add-int/lit8 v0, p3, -0x2

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "IE"

    aput-object v2, v1, v5

    invoke-static {p1, v0, v3, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, p3, -0x4

    new-array v1, v3, [Ljava/lang/String;

    const-string/jumbo v2, "ME"

    aput-object v2, v1, v5

    const-string/jumbo v2, "MA"

    aput-object v2, v1, v6

    invoke-static {p1, v0, v3, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 804
    invoke-virtual {p2, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->appendAlternate(C)V

    .line 808
    :goto_0
    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v4, :cond_1

    add-int/lit8 v0, p3, 0x2

    :goto_1
    return v0

    .line 806
    :cond_0
    invoke-virtual {p2, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_0

    .line 808
    :cond_1
    add-int/lit8 v0, p3, 0x1

    goto :goto_1
.end method

.method private handleS(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;IZ)I
    .locals 9
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p3, "index"    # I
    .param p4, "slavoGermanic"    # Z

    .prologue
    const/4 v8, 0x3

    const/16 v7, 0x53

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 816
    add-int/lit8 v0, p3, -0x1

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "ISL"

    aput-object v2, v1, v5

    const-string/jumbo v2, "YSL"

    aput-object v2, v1, v4

    invoke-static {p1, v0, v8, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 818
    add-int/lit8 p3, p3, 0x1

    .line 858
    :goto_0
    return p3

    .line 819
    :cond_0
    if-nez p3, :cond_1

    const/4 v0, 0x5

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "SUGAR"

    aput-object v2, v1, v5

    invoke-static {p1, p3, v0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 821
    const/16 v0, 0x58

    invoke-virtual {p2, v0, v7}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(CC)V

    .line 822
    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    .line 823
    :cond_1
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "SH"

    aput-object v1, v0, v5

    invoke-static {p1, p3, v6, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 824
    add-int/lit8 v0, p3, 0x1

    const/4 v1, 0x4

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "HEIM"

    aput-object v3, v2, v5

    const-string/jumbo v3, "HOEK"

    aput-object v3, v2, v4

    const-string/jumbo v3, "HOLM"

    aput-object v3, v2, v6

    const-string/jumbo v3, "HOLZ"

    aput-object v3, v2, v8

    invoke-static {p1, v0, v1, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 826
    invoke-virtual {p2, v7}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 830
    :goto_1
    add-int/lit8 p3, p3, 0x2

    goto :goto_0

    .line 828
    :cond_2
    const/16 v0, 0x58

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_1

    .line 831
    :cond_3
    new-array v0, v6, [Ljava/lang/String;

    const-string/jumbo v1, "SIO"

    aput-object v1, v0, v5

    const-string/jumbo v1, "SIA"

    aput-object v1, v0, v4

    invoke-static {p1, p3, v8, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x4

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "SIAN"

    aput-object v2, v1, v5

    invoke-static {p1, p3, v0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 833
    :cond_4
    if-eqz p4, :cond_5

    .line 834
    invoke-virtual {p2, v7}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 838
    :goto_2
    add-int/lit8 p3, p3, 0x3

    goto :goto_0

    .line 836
    :cond_5
    const/16 v0, 0x58

    invoke-virtual {p2, v7, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(CC)V

    goto :goto_2

    .line 839
    :cond_6
    if-nez p3, :cond_7

    add-int/lit8 v0, p3, 0x1

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "M"

    aput-object v2, v1, v5

    const-string/jumbo v2, "N"

    aput-object v2, v1, v4

    const-string/jumbo v2, "L"

    aput-object v2, v1, v6

    const-string/jumbo v2, "W"

    aput-object v2, v1, v8

    invoke-static {p1, v0, v4, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    :cond_7
    add-int/lit8 v0, p3, 0x1

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "Z"

    aput-object v2, v1, v5

    invoke-static {p1, v0, v4, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 845
    :cond_8
    const/16 v0, 0x58

    invoke-virtual {p2, v7, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(CC)V

    .line 846
    add-int/lit8 v0, p3, 0x1

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "Z"

    aput-object v2, v1, v5

    invoke-static {p1, v0, v4, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    add-int/lit8 p3, p3, 0x2

    :goto_3
    goto/16 :goto_0

    :cond_9
    add-int/lit8 p3, p3, 0x1

    goto :goto_3

    .line 847
    :cond_a
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "SC"

    aput-object v1, v0, v5

    invoke-static {p1, p3, v6, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 848
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleSC(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I

    move-result p3

    goto/16 :goto_0

    .line 850
    :cond_b
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p3, v0, :cond_c

    add-int/lit8 v0, p3, -0x2

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "AI"

    aput-object v2, v1, v5

    const-string/jumbo v2, "OI"

    aput-object v2, v1, v4

    invoke-static {p1, v0, v6, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 852
    invoke-virtual {p2, v7}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->appendAlternate(C)V

    .line 856
    :goto_4
    add-int/lit8 v0, p3, 0x1

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "S"

    aput-object v2, v1, v5

    const-string/jumbo v2, "Z"

    aput-object v2, v1, v4

    invoke-static {p1, v0, v4, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    add-int/lit8 p3, p3, 0x2

    :goto_5
    goto/16 :goto_0

    .line 854
    :cond_c
    invoke-virtual {p2, v7}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_4

    .line 856
    :cond_d
    add-int/lit8 p3, p3, 0x1

    goto :goto_5
.end method

.method private handleSC(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I
    .locals 9
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p3, "index"    # I

    .prologue
    const/16 v8, 0x53

    const/4 v7, 0x0

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 865
    add-int/lit8 v0, p3, 0x2

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x48

    if-ne v0, v1, :cond_3

    .line 867
    add-int/lit8 v0, p3, 0x3

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "OO"

    aput-object v2, v1, v7

    const-string/jumbo v2, "ER"

    aput-object v2, v1, v5

    const-string/jumbo v2, "EN"

    aput-object v2, v1, v4

    const-string/jumbo v2, "UY"

    aput-object v2, v1, v6

    const/4 v2, 0x4

    const-string/jumbo v3, "ED"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string/jumbo v3, "EM"

    aput-object v3, v1, v2

    invoke-static {p1, v0, v4, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 869
    add-int/lit8 v0, p3, 0x3

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "ER"

    aput-object v2, v1, v7

    const-string/jumbo v2, "EN"

    aput-object v2, v1, v5

    invoke-static {p1, v0, v4, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 871
    const-string/jumbo v0, "X"

    const-string/jumbo v1, "SK"

    invoke-virtual {p2, v0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;Ljava/lang/String;)V

    .line 887
    :goto_0
    add-int/lit8 v0, p3, 0x3

    return v0

    .line 873
    :cond_0
    const-string/jumbo v0, "SK"

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto :goto_0

    .line 876
    :cond_1
    if-nez p3, :cond_2

    invoke-virtual {p0, p1, v6}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->isVowel(C)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0, p1, v6}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x57

    if-eq v0, v1, :cond_2

    .line 877
    const/16 v0, 0x58

    invoke-virtual {p2, v0, v8}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(CC)V

    goto :goto_0

    .line 879
    :cond_2
    const/16 v0, 0x58

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_0

    .line 882
    :cond_3
    add-int/lit8 v0, p3, 0x2

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "I"

    aput-object v2, v1, v7

    const-string/jumbo v2, "E"

    aput-object v2, v1, v5

    const-string/jumbo v2, "Y"

    aput-object v2, v1, v4

    invoke-static {p1, v0, v5, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 883
    invoke-virtual {p2, v8}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_0

    .line 885
    :cond_4
    const-string/jumbo v0, "SK"

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private handleSpace(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I
    .locals 3
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p3, "index"    # I

    .prologue
    const/16 v2, 0x20

    .line 1000
    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    if-eq v0, v2, :cond_0

    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x2e

    if-eq v0, v1, :cond_0

    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x2d

    if-ne v0, v1, :cond_1

    .line 1001
    :cond_0
    add-int/lit8 p3, p3, 0x1

    .line 1003
    :cond_1
    invoke-virtual {p2, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 1004
    invoke-virtual {p2, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 1005
    add-int/lit8 p3, p3, 0x1

    .line 1006
    return p3
.end method

.method private handleT(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I
    .locals 8
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p3, "index"    # I

    .prologue
    const/16 v7, 0x54

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 894
    const/4 v0, 0x4

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "TION"

    aput-object v2, v1, v3

    invoke-static {p1, p3, v0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 895
    const/16 v0, 0x58

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 896
    add-int/lit8 p3, p3, 0x3

    .line 914
    :goto_0
    return p3

    .line 897
    :cond_0
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "TIA"

    aput-object v1, v0, v3

    const-string/jumbo v1, "TCH"

    aput-object v1, v0, v4

    invoke-static {p1, p3, v6, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 898
    const/16 v0, 0x58

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 899
    add-int/lit8 p3, p3, 0x3

    goto :goto_0

    .line 900
    :cond_1
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "TH"

    aput-object v1, v0, v3

    invoke-static {p1, p3, v5, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "TTH"

    aput-object v1, v0, v3

    invoke-static {p1, p3, v6, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 901
    :cond_2
    add-int/lit8 v0, p3, 0x2

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "OM"

    aput-object v2, v1, v3

    const-string/jumbo v2, "AM"

    aput-object v2, v1, v4

    invoke-static {p1, v0, v5, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x4

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "VAN "

    aput-object v2, v1, v3

    const-string/jumbo v2, "VON "

    aput-object v2, v1, v4

    invoke-static {p1, v3, v0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "SCH"

    aput-object v1, v0, v3

    invoke-static {p1, v3, v6, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 905
    :cond_3
    invoke-virtual {p2, v7}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 909
    :goto_1
    add-int/lit8 p3, p3, 0x2

    goto :goto_0

    .line 907
    :cond_4
    const/16 v0, 0x30

    invoke-virtual {p2, v0, v7}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(CC)V

    goto :goto_1

    .line 911
    :cond_5
    invoke-virtual {p2, v7}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 912
    add-int/lit8 v0, p3, 0x1

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "T"

    aput-object v2, v1, v3

    const-string/jumbo v2, "D"

    aput-object v2, v1, v4

    invoke-static {p1, v0, v4, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    add-int/lit8 p3, p3, 0x2

    :goto_2
    goto/16 :goto_0

    :cond_6
    add-int/lit8 p3, p3, 0x1

    goto :goto_2
.end method

.method private handleW(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I
    .locals 9
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p3, "index"    # I

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 921
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "WR"

    aput-object v1, v0, v4

    invoke-static {p1, p3, v6, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 923
    const/16 v0, 0x52

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 924
    add-int/lit8 p3, p3, 0x2

    .line 950
    :goto_0
    return p3

    .line 926
    :cond_0
    if-nez p3, :cond_3

    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->isVowel(C)Z

    move-result v0

    if-nez v0, :cond_1

    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "WH"

    aput-object v1, v0, v4

    invoke-static {p1, p3, v6, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 928
    :cond_1
    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->isVowel(C)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 930
    const/16 v0, 0x41

    const/16 v1, 0x46

    invoke-virtual {p2, v0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(CC)V

    .line 935
    :goto_1
    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    .line 933
    :cond_2
    const/16 v0, 0x41

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_1

    .line 936
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p3, v0, :cond_4

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->isVowel(C)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    add-int/lit8 v0, p3, -0x1

    const/4 v1, 0x5

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "EWSKI"

    aput-object v3, v2, v4

    const-string/jumbo v3, "EWSKY"

    aput-object v3, v2, v5

    const-string/jumbo v3, "OWSKI"

    aput-object v3, v2, v6

    const-string/jumbo v3, "OWSKY"

    aput-object v3, v2, v7

    invoke-static {p1, v0, v1, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "SCH"

    aput-object v1, v0, v4

    invoke-static {p1, v4, v7, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 940
    :cond_5
    const/16 v0, 0x46

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->appendAlternate(C)V

    .line 941
    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    .line 942
    :cond_6
    new-array v0, v6, [Ljava/lang/String;

    const-string/jumbo v1, "WICZ"

    aput-object v1, v0, v4

    const-string/jumbo v1, "WITZ"

    aput-object v1, v0, v5

    invoke-static {p1, p3, v8, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 944
    const-string/jumbo v0, "TS"

    const-string/jumbo v1, "FX"

    invoke-virtual {p2, v0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;Ljava/lang/String;)V

    .line 945
    add-int/lit8 p3, p3, 0x4

    goto/16 :goto_0

    .line 947
    :cond_7
    add-int/lit8 p3, p3, 0x1

    goto/16 :goto_0
.end method

.method private handleX(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I
    .locals 7
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p3, "index"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 957
    if-nez p3, :cond_0

    .line 958
    const/16 v0, 0x53

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 959
    add-int/lit8 p3, p3, 0x1

    .line 972
    :goto_0
    return p3

    .line 961
    :cond_0
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "XZ"

    aput-object v1, v0, v6

    invoke-static {p1, p3, v5, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 962
    const-string/jumbo v0, "KS"

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    .line 963
    add-int/lit8 p3, p3, 0x2

    .line 970
    :cond_1
    :goto_1
    add-int/lit8 v0, p3, 0x1

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "C"

    aput-object v2, v1, v6

    const-string/jumbo v2, "X"

    aput-object v2, v1, v4

    invoke-static {p1, v0, v4, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    add-int/lit8 p3, p3, 0x2

    :goto_2
    goto :goto_0

    .line 964
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p3, v0, :cond_3

    add-int/lit8 v0, p3, -0x3

    const/4 v1, 0x3

    new-array v2, v5, [Ljava/lang/String;

    const-string/jumbo v3, "IAU"

    aput-object v3, v2, v6

    const-string/jumbo v3, "EAU"

    aput-object v3, v2, v4

    invoke-static {p1, v0, v1, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    add-int/lit8 v0, p3, -0x2

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "AU"

    aput-object v2, v1, v6

    const-string/jumbo v2, "OU"

    aput-object v2, v1, v4

    invoke-static {p1, v0, v5, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 968
    :cond_3
    const-string/jumbo v0, "KS"

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;)V

    goto :goto_1

    .line 970
    :cond_4
    add-int/lit8 p3, p3, 0x1

    goto :goto_2
.end method

.method private handleZ(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;IZ)I
    .locals 5
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    .param p3, "index"    # I
    .param p4, "slavoGermanic"    # Z

    .prologue
    const/4 v4, 0x2

    .line 980
    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x48

    if-ne v0, v1, :cond_0

    .line 982
    const/16 v0, 0x4a

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 983
    add-int/lit8 p3, p3, 0x2

    .line 993
    :goto_0
    return p3

    .line 985
    :cond_0
    add-int/lit8 v0, p3, 0x1

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "ZO"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "ZI"

    aput-object v3, v1, v2

    const-string/jumbo v2, "ZA"

    aput-object v2, v1, v4

    invoke-static {p1, v0, v4, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->contains(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p4, :cond_2

    if-lez p3, :cond_2

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x54

    if-eq v0, v1, :cond_2

    .line 987
    :cond_1
    const-string/jumbo v0, "S"

    const-string/jumbo v1, "TS"

    invoke-virtual {p2, v0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(Ljava/lang/String;Ljava/lang/String;)V

    .line 991
    :goto_1
    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x5a

    if-ne v0, v1, :cond_3

    add-int/lit8 p3, p3, 0x2

    :goto_2
    goto :goto_0

    .line 989
    :cond_2
    const/16 v0, 0x53

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_1

    .line 991
    :cond_3
    add-int/lit8 p3, p3, 0x1

    goto :goto_2
.end method

.method private isOnlyLastVowel(ILjava/lang/String;)Z
    .locals 5
    .param p1, "index"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 282
    add-int/lit8 v0, p1, 0x1

    .local v0, "i":I
    :goto_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_2

    .line 283
    invoke-virtual {p0, p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v4

    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->isVowel(C)Z

    move-result v4

    if-nez v4, :cond_1

    .line 292
    :cond_0
    :goto_1
    return v2

    .line 282
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 287
    :cond_2
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {p0, p2, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v1

    .line 288
    .local v1, "lastChar":C
    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->isVowel(C)Z

    move-result v4

    if-nez v4, :cond_3

    const/16 v4, 0x48

    if-ne v1, v4, :cond_0

    .line 289
    :cond_3
    iput-boolean v3, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->firstVowelProcessed:Z

    move v2, v3

    .line 290
    goto :goto_1
.end method

.method private isSilentStart(Ljava/lang/String;)Z
    .locals 6
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1096
    const/4 v4, 0x0

    .line 1097
    .local v4, "result":Z
    sget-object v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->SILENT_START:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 1098
    .local v1, "element":Ljava/lang/String;
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1099
    const/4 v4, 0x1

    .line 1103
    .end local v1    # "element":Ljava/lang/String;
    :cond_0
    return v4

    .line 1097
    .restart local v1    # "element":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private isSlavoGermanic(Ljava/lang/String;)Z
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v1, -0x1

    .line 1079
    const/16 v0, 0x57

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-gt v0, v1, :cond_0

    const/16 v0, 0x4b

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-gt v0, v1, :cond_0

    const-string/jumbo v0, "CZ"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-gt v0, v1, :cond_0

    const-string/jumbo v0, "WITZ"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isVowel(C)Z
    .locals 2
    .param p1, "ch"    # C

    .prologue
    .line 1087
    const-string/jumbo v0, "AEIOUY"

    invoke-virtual {v0, p1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected charAt(Ljava/lang/String;I)C
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 1126
    if-ltz p2, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lt p2, v0, :cond_1

    .line 1127
    :cond_0
    const/4 v0, 0x0

    .line 1129
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_0
.end method

.method public doubleMetaphone(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->doubleMetaphone(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public doubleMetaphone(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 9
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "alternate"    # Z

    .prologue
    const/4 v3, 0x1

    const/16 v8, 0x4e

    const/16 v7, 0x4b

    const/16 v6, 0x46

    const/4 v0, 0x0

    .line 100
    iput-boolean v0, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->firstVowelProcessed:Z

    .line 101
    iput-boolean v0, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->lastVowelProcessed:Z

    .line 102
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->cleanInput(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 103
    if-nez p1, :cond_0

    .line 104
    const/4 v3, 0x0

    .line 219
    :goto_0
    return-object v3

    .line 107
    :cond_0
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->isSlavoGermanic(Ljava/lang/String;)Z

    move-result v2

    .line 108
    .local v2, "slavoGermanic":Z
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->isSilentStart(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v0, v3

    .line 110
    .local v0, "index":I
    :cond_1
    new-instance v1, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->getMaxCodeLen()I

    move-result v4

    invoke-direct {v1, p0, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;-><init>(Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;I)V

    .line 112
    .local v1, "result":Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;
    const/16 v4, 0x20

    invoke-virtual {v1, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 113
    :goto_1
    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->isComplete()Z

    move-result v4

    if-nez v4, :cond_b

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-gt v0, v4, :cond_b

    .line 114
    invoke-direct {p0, v1, v0, p1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->checkOnVowelBeforeIndex(Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;ILjava/lang/String;)V

    .line 115
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 210
    add-int/lit8 v0, v0, 0x1

    .line 211
    goto :goto_1

    .line 122
    :sswitch_0
    invoke-direct {p0, v1, v0, p1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleAEIOUY(Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;ILjava/lang/String;)I

    move-result v0

    .line 123
    goto :goto_1

    .line 125
    :sswitch_1
    if-nez v0, :cond_2

    .line 126
    const/16 v4, 0x42

    invoke-virtual {v1, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 130
    :goto_2
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0, p1, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v4

    const/16 v5, 0x42

    if-ne v4, v5, :cond_3

    add-int/lit8 v0, v0, 0x2

    .line 131
    :goto_3
    goto :goto_1

    .line 128
    :cond_2
    const/16 v4, 0x50

    invoke-virtual {v1, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    goto :goto_2

    .line 130
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 134
    :sswitch_2
    const/16 v4, 0x53

    invoke-virtual {v1, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 135
    add-int/lit8 v0, v0, 0x1

    .line 136
    goto :goto_1

    .line 138
    :sswitch_3
    invoke-direct {p0, p1, v1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleC(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I

    move-result v0

    .line 139
    goto :goto_1

    .line 141
    :sswitch_4
    invoke-direct {p0, p1, v1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleD(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I

    move-result v0

    .line 142
    goto :goto_1

    .line 144
    :sswitch_5
    invoke-virtual {v1, v6}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 145
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0, p1, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v4

    if-ne v4, v6, :cond_4

    add-int/lit8 v0, v0, 0x2

    .line 146
    :goto_4
    goto :goto_1

    .line 145
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 148
    :sswitch_6
    invoke-direct {p0, p1, v1, v0, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleG(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;IZ)I

    move-result v0

    .line 149
    goto :goto_1

    .line 151
    :sswitch_7
    invoke-direct {p0, p1, v1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleH(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I

    move-result v0

    .line 152
    goto :goto_1

    .line 154
    :sswitch_8
    invoke-direct {p0, p1, v1, v0, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleJ(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;IZ)I

    move-result v0

    .line 155
    goto :goto_1

    .line 157
    :sswitch_9
    invoke-virtual {v1, v7}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 158
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0, p1, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v4

    if-ne v4, v7, :cond_5

    add-int/lit8 v0, v0, 0x2

    .line 159
    :goto_5
    goto :goto_1

    .line 158
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 161
    :sswitch_a
    invoke-direct {p0, p1, v1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleL(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I

    move-result v0

    .line 162
    goto/16 :goto_1

    .line 164
    :sswitch_b
    const/16 v4, 0x4d

    invoke-virtual {v1, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 165
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0, p1, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v4

    const/16 v5, 0x43

    if-ne v4, v5, :cond_6

    .line 166
    const/16 v4, 0x41

    invoke-virtual {v1, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 167
    iput-boolean v3, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->firstVowelProcessed:Z

    .line 169
    :cond_6
    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->conditionM0(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_7

    add-int/lit8 v0, v0, 0x2

    .line 170
    :goto_6
    goto/16 :goto_1

    .line 169
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 172
    :sswitch_c
    invoke-virtual {v1, v8}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 173
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0, p1, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v4

    if-ne v4, v8, :cond_8

    add-int/lit8 v0, v0, 0x2

    .line 174
    :goto_7
    goto/16 :goto_1

    .line 173
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 177
    :sswitch_d
    invoke-virtual {v1, v8}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 178
    add-int/lit8 v0, v0, 0x1

    .line 179
    goto/16 :goto_1

    .line 181
    :sswitch_e
    invoke-direct {p0, p1, v1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleP(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I

    move-result v0

    .line 182
    goto/16 :goto_1

    .line 184
    :sswitch_f
    invoke-virtual {v1, v7}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 185
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0, p1, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v4

    const/16 v5, 0x51

    if-ne v4, v5, :cond_9

    add-int/lit8 v0, v0, 0x2

    .line 186
    :goto_8
    goto/16 :goto_1

    .line 185
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 188
    :sswitch_10
    invoke-direct {p0, p1, v1, v0, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleR(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;IZ)I

    move-result v0

    .line 189
    goto/16 :goto_1

    .line 191
    :sswitch_11
    invoke-direct {p0, p1, v1, v0, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleS(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;IZ)I

    move-result v0

    .line 192
    goto/16 :goto_1

    .line 194
    :sswitch_12
    invoke-direct {p0, p1, v1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleT(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I

    move-result v0

    .line 195
    goto/16 :goto_1

    .line 197
    :sswitch_13
    invoke-virtual {v1, v6}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 198
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0, p1, v4}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v4

    const/16 v5, 0x56

    if-ne v4, v5, :cond_a

    add-int/lit8 v0, v0, 0x2

    .line 199
    :goto_9
    goto/16 :goto_1

    .line 198
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 201
    :sswitch_14
    invoke-direct {p0, p1, v1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleW(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I

    move-result v0

    .line 202
    goto/16 :goto_1

    .line 204
    :sswitch_15
    invoke-direct {p0, p1, v1, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleX(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;I)I

    move-result v0

    .line 205
    goto/16 :goto_1

    .line 207
    :sswitch_16
    invoke-direct {p0, p1, v1, v0, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleZ(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;IZ)I

    move-result v0

    .line 208
    goto/16 :goto_1

    .line 214
    :cond_b
    iget-boolean v3, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->lastVowelProcessed:Z

    if-nez v3, :cond_c

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0, p1, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->charAt(Ljava/lang/String;I)C

    move-result v3

    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->isVowel(C)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 215
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-direct {p0, v1, v3, p1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->handleLastVowel(Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;ILjava/lang/String;)V

    .line 218
    :cond_c
    const/16 v3, 0x20

    invoke-virtual {v1, v3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->append(C)V

    .line 219
    if-eqz p2, :cond_d

    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->getAlternate()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    :cond_d
    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone$DoubleMetaphoneResult;->getPrimary()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 115
    nop

    :sswitch_data_0
    .sparse-switch
        0x41 -> :sswitch_0
        0x42 -> :sswitch_1
        0x43 -> :sswitch_3
        0x44 -> :sswitch_4
        0x45 -> :sswitch_0
        0x46 -> :sswitch_5
        0x47 -> :sswitch_6
        0x48 -> :sswitch_7
        0x49 -> :sswitch_0
        0x4a -> :sswitch_8
        0x4b -> :sswitch_9
        0x4c -> :sswitch_a
        0x4d -> :sswitch_b
        0x4e -> :sswitch_c
        0x4f -> :sswitch_0
        0x50 -> :sswitch_e
        0x51 -> :sswitch_f
        0x52 -> :sswitch_10
        0x53 -> :sswitch_11
        0x54 -> :sswitch_12
        0x55 -> :sswitch_0
        0x56 -> :sswitch_13
        0x57 -> :sswitch_14
        0x58 -> :sswitch_15
        0x59 -> :sswitch_0
        0x5a -> :sswitch_16
        0xc7 -> :sswitch_2
        0xd1 -> :sswitch_d
    .end sparse-switch
.end method

.method public getMaxCodeLen()I
    .locals 1

    .prologue
    .line 255
    iget v0, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->maxCodeLen:I

    return v0
.end method

.method public isDoubleMetaphoneEqual(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "value1"    # Ljava/lang/String;
    .param p2, "value2"    # Ljava/lang/String;

    .prologue
    .line 233
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->isDoubleMetaphoneEqual(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isDoubleMetaphoneEqual(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 2
    .param p1, "value1"    # Ljava/lang/String;
    .param p2, "value2"    # Ljava/lang/String;
    .param p3, "alternate"    # Z

    .prologue
    .line 247
    invoke-virtual {p0, p1, p3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->doubleMetaphone(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p2, p3}, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->doubleMetaphone(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setMaxCodeLen(I)V
    .locals 0
    .param p1, "maxCodeLen"    # I

    .prologue
    .line 263
    iput p1, p0, Lcom/vlingo/core/internal/contacts/phoneticencoding/DoubleMetaphone;->maxCodeLen:I

    .line 264
    return-void
.end method
