.class public Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "SendEmailAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;


# instance fields
.field private contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation
.end field

.field private message:Ljava/lang/String;

.field private subject:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->contacts:Ljava/util/List;

    return-void
.end method

.method private contacts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->contacts:Ljava/util/List;

    if-nez v0, :cond_0

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->contacts:Ljava/util/List;

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->contacts:Ljava/util/List;

    return-object v0
.end method

.method private extractAddresses()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->contacts()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v0, v2, [Ljava/lang/String;

    .line 94
    .local v0, "addresses":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->contacts()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 95
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->contacts()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    iget-object v2, v2, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 94
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 97
    :cond_0
    return-object v0
.end method

.method private updateMRU()V
    .locals 6

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->contacts()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 102
    .local v0, "contact":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-static {}, Lcom/vlingo/core/internal/contacts/mru/MRU;->getMRU()Lcom/vlingo/core/internal/contacts/mru/MRU;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactType;->EMAIL:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/ContactType;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/vlingo/core/internal/contacts/ContactData;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-wide v4, v4, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    long-to-int v4, v4

    iget-object v5, v0, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5}, Lcom/vlingo/core/internal/contacts/mru/MRU;->incrementCount(Ljava/lang/String;ILjava/lang/String;)F

    goto :goto_0

    .line 104
    .end local v0    # "contact":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_0
    return-void
.end method


# virtual methods
.method public contact(Lcom/vlingo/core/internal/contacts/ContactData;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;
    .locals 1
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactData;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->contacts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    return-object p0
.end method

.method public contacts(Ljava/util/List;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)",
            "Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;"
        }
    .end annotation

    .prologue
    .line 35
    .local p1, "contactDataList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->contacts:Ljava/util/List;

    .line 36
    return-object p0
.end method

.method protected execute()V
    .locals 4

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->contacts()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->contacts()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 79
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->updateMRU()V

    .line 80
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.SEND"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 81
    .local v0, "action":Landroid/content/Intent;
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->extractAddresses()[Ljava/lang/String;

    move-result-object v1

    .line 82
    .local v1, "addresses":[Ljava/lang/String;
    const-string/jumbo v2, "android.intent.extra.EMAIL"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 83
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->subject:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 84
    const-string/jumbo v2, "android.intent.extra.SUBJECT"

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->subject:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 85
    :cond_0
    const-string/jumbo v2, "android.intent.extra.TEXT"

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->message:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    const-string/jumbo v2, "message/rfc822"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 87
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 88
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 90
    .end local v0    # "action":Landroid/content/Intent;
    .end local v1    # "addresses":[Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public message(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;
    .locals 0
    .param p1, "messageParam"    # Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->message:Ljava/lang/String;

    .line 63
    return-object p0
.end method

.method public bridge synthetic message(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->message(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;

    move-result-object v0

    return-object v0
.end method

.method public subject(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;
    .locals 0
    .param p1, "subjectParam"    # Ljava/lang/String;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->subject:Ljava/lang/String;

    .line 54
    return-object p0
.end method
