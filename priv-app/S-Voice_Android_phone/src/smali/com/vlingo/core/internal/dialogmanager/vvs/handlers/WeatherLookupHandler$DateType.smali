.class public final enum Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;
.super Ljava/lang/Enum;
.source "WeatherLookupHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DateType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

.field public static final enum OTHER:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

.field public static final enum TODAY:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

.field public static final enum TOMORROW:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 46
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    const-string/jumbo v1, "TODAY"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;->TODAY:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    const-string/jumbo v1, "TOMORROW"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;->TOMORROW:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    const-string/jumbo v1, "OTHER"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;->OTHER:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    .line 45
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;->TODAY:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;->TOMORROW:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;->OTHER:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 45
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    return-object v0
.end method
