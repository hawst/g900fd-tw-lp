.class public final Lcom/vlingo/core/internal/bluetooth/BluetoothManager;
.super Ljava/lang/Object;
.source "BluetoothManager.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;,
        Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;,
        Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoTimeoutTask;,
        Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;
    }
.end annotation


# static fields
.field private static final BT_SAMPLE_EXTRA_PARAM:Ljava/lang/String; = "android.bluetooth.hfp.extra.SCO_SAMPLERATE"

.field private static final BT_SAMPLE_EXTRA_PARAM_DEFAULT:I = 0x1

.field private static final LISTENERS:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;",
            ">;"
        }
    .end annotation
.end field

.field private static final SCO_RETRY_DELAY:I = 0x7d0

.field private static final SCO_RETRY_FLAG:Ljava/lang/String; = "sco_retry_flag"

.field private static final SETTING_KEY_BVRA_SUPPORT:Ljava/lang/String; = "supported_bvra"

.field public static final STREAM_BLUETOOTH_SCO:I = 0x6

.field private static final TAG:Ljava/lang/String;

.field private static btStateBroadcastReceiver:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;

.field private static cancelTurnTiming:Z

.field private static ignoreBtConnectEvent:Z

.field private static instance:Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

.field private static mAppCloseType:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

.field private static mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

.field private static mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

.field private static volatile mIsStartSCOinProgress:Z

.field private static mOnScoConnectedTask:Ljava/lang/Runnable;

.field private static mOnScoConnectedTaskTimeout:J

.field private static mProfileListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private static mRightBeforeForeground:Z

.field private static mSampleRate:I

.field private static mScoDisconnectingByAudioFocusLoss:Z

.field private static mScoTimeoutTask:Ljava/util/TimerTask;

.field private static mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

.field private static mVolumeFallback:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 94
    const-class v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    .line 101
    sput-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;

    .line 105
    sput-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 106
    sput-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    .line 108
    sput-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->btStateBroadcastReceiver:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;

    .line 110
    sput-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->instance:Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    .line 112
    sput-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    .line 114
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->LISTENERS:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 116
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;->BT_BUTTON:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    sput-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mAppCloseType:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    .line 117
    sput-boolean v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z

    .line 118
    sput-boolean v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mRightBeforeForeground:Z

    .line 119
    sput-boolean v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->ignoreBtConnectEvent:Z

    .line 120
    const/4 v0, 0x1

    sput-boolean v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->cancelTurnTiming:Z

    .line 123
    const/16 v0, 0x1f40

    sput v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mSampleRate:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1178
    return-void
.end method

.method static synthetic access$100()Landroid/bluetooth/BluetoothHeadset;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    return-object v0
.end method

.method static synthetic access$1002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 90
    sput-boolean p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mIsStartSCOinProgress:Z

    return p0
.end method

.method static synthetic access$102(Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/BluetoothHeadset;
    .locals 0
    .param p0, "x0"    # Landroid/bluetooth/BluetoothHeadset;

    .prologue
    .line 90
    sput-object p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    return-object p0
.end method

.method static synthetic access$1102(Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;)Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    .prologue
    .line 90
    sput-object p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mAppCloseType:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    return-object p0
.end method

.method static synthetic access$1200(Z)V
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 90
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->notifyScoStateToListeners(Z)V

    return-void
.end method

.method static synthetic access$1300(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1
    .param p0, "x0"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 90
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isIgnoreIntentForBDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1400(I)V
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 90
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->setBluetoothSampleRate(I)V

    return-void
.end method

.method static synthetic access$1502(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 90
    sput-boolean p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z

    return p0
.end method

.method static synthetic access$200()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method static synthetic access$202(Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
    .locals 0
    .param p0, "x0"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 90
    sput-object p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    return-object p0
.end method

.method static synthetic access$300()Ljava/util/TimerTask;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;

    return-object v0
.end method

.method static synthetic access$302(Ljava/util/TimerTask;)Ljava/util/TimerTask;
    .locals 0
    .param p0, "x0"    # Ljava/util/TimerTask;

    .prologue
    .line 90
    sput-object p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;

    return-object p0
.end method

.method static synthetic access$400(Z)V
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 90
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopSCO(Z)V

    return-void
.end method

.method static synthetic access$500(Z)V
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 90
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startSCO(Z)V

    return-void
.end method

.method static synthetic access$600()V
    .locals 0

    .prologue
    .line 90
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startSCO()V

    return-void
.end method

.method static synthetic access$700()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mOnScoConnectedTask:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$900()V
    .locals 0

    .prologue
    .line 90
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->cancelScoTimeoutTask()V

    return-void
.end method

.method public static addListener(Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;

    .prologue
    .line 1379
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->LISTENERS:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 1380
    return-void
.end method

.method public static appCloseType()Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;
    .locals 1

    .prologue
    .line 301
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mAppCloseType:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    return-object v0
.end method

.method public static appCloseType(Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;)V
    .locals 0
    .param p0, "type"    # Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    .prologue
    .line 297
    sput-object p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mAppCloseType:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    .line 298
    return-void
.end method

.method public static declared-synchronized bluetoothManagerDestroy()V
    .locals 3

    .prologue
    .line 245
    const-class v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "==[SVoiceBT, LatencyCheck]BT bluetoothManagerDestroy=="

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->enableWideBandSpeech(Z)V

    .line 247
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    iget-object v0, v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 252
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    iget-object v0, v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 254
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->LISTENERS:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 255
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    .line 256
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mRightBeforeForeground:Z

    .line 257
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->btStateBroadcastReceiver:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;

    if-eqz v0, :cond_1

    .line 258
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;->NORMAL:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    sput-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mAppCloseType:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    .line 263
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->closeBtProxy()V

    .line 264
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->btStateBroadcastReceiver:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 265
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->btStateBroadcastReceiver:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;

    .line 267
    const/4 v0, -0x1

    sput v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mVolumeFallback:I

    .line 269
    :cond_1
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->instance:Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->removeListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 270
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->instance:Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    if-eqz v0, :cond_2

    .line 271
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->instance:Lcom/vlingo/core/internal/bluetooth/BluetoothManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 273
    :cond_2
    monitor-exit v1

    return-void

    .line 245
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized bluetoothManagerInit()Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 133
    const-class v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v3

    const/4 v4, 0x0

    :try_start_0
    sput-boolean v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mIsStartSCOinProgress:Z

    .line 134
    sget-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;->BT_BUTTON:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    sput-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mAppCloseType:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    .line 135
    const/4 v4, 0x0

    sput-boolean v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z

    .line 136
    const/4 v4, 0x0

    sput-boolean v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->ignoreBtConnectEvent:Z

    .line 137
    const/4 v4, -0x1

    sput v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mVolumeFallback:I

    .line 138
    sget-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    if-nez v4, :cond_0

    .line 139
    new-instance v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    invoke-direct {v4}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;-><init>()V

    sput-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    .line 140
    sget-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;->start()V

    .line 141
    :goto_0
    sget-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    iget-object v4, v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;->mHandler:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_0

    .line 145
    const-wide/16 v4, 0x0

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 146
    :catch_0
    move-exception v4

    goto :goto_0

    .line 150
    :cond_0
    :try_start_2
    sget-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->btStateBroadcastReceiver:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;

    if-nez v4, :cond_2

    .line 151
    new-instance v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;

    const/4 v4, 0x0

    invoke-direct {v2, v4}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;-><init>(Lcom/vlingo/core/internal/bluetooth/BluetoothManager$1;)V

    sput-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->btStateBroadcastReceiver:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;

    .line 152
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 153
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v2, "com.vlingo.client.app.action.APPLICATION_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 154
    const-string/jumbo v2, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 155
    const-string/jumbo v2, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 156
    const-string/jumbo v2, "android.bluetooth.device.action.ACL_DISCONNECT_REQUESTED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 157
    const-string/jumbo v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 158
    const-string/jumbo v2, "android.media.ACTION_SCO_AUDIO_STATE_UPDATED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 159
    const-string/jumbo v2, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 160
    const-string/jumbo v2, "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 161
    const-string/jumbo v2, "com.vlingo.client.app.action.AUDIO_FOCUS_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 162
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->btStateBroadcastReceiver:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$BTStateBroadcastReceiver;

    invoke-virtual {v2, v4, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 165
    new-instance v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$1;

    invoke-direct {v2}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$1;-><init>()V

    sput-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mProfileListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 217
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mProfileListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    const/4 v6, 0x1

    invoke-virtual {v2, v4, v5, v6}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    .line 225
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->instance:Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    if-nez v2, :cond_1

    .line 226
    new-instance v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    invoke-direct {v2}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;-><init>()V

    sput-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->instance:Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    .line 228
    :cond_1
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->instance:Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->addListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 235
    :goto_1
    monitor-exit v3

    return v1

    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_2
    move v1, v2

    goto :goto_1

    .line 133
    .restart local v0    # "filter":Landroid/content/IntentFilter;
    :catchall_0
    move-exception v1

    monitor-exit v3

    throw v1
.end method

.method private static cancelScoConnectedTask()V
    .locals 2

    .prologue
    .line 869
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mOnScoConnectedTask:Ljava/lang/Runnable;

    .line 870
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mOnScoConnectedTaskTimeout:J

    .line 871
    return-void
.end method

.method private static cancelScoTimeoutTask()V
    .locals 1

    .prologue
    .line 860
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 861
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 862
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;

    .line 864
    :cond_0
    return-void
.end method

.method public static declared-synchronized closeBtProxy()V
    .locals 4

    .prologue
    .line 284
    const-class v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "====[SVoiceBT, LatencyCheck] closeBtProxy==="

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v0, :cond_0

    .line 286
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopSCO(Z)V

    .line 290
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    const/4 v2, 0x1

    sget-object v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v0, v2, v3}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    .line 291
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294
    :cond_0
    monitor-exit v1

    return-void

    .line 284
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static considerRightBeforeForeground(Z)V
    .locals 0
    .param p0, "flag"    # Z

    .prologue
    .line 1429
    sput-boolean p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mRightBeforeForeground:Z

    .line 1430
    return-void
.end method

.method public static disconnectScoByIdle()Z
    .locals 1

    .prologue
    .line 1417
    sget-boolean v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z

    return v0
.end method

.method public static enableWideBandSpeech(Z)V
    .locals 5
    .param p0, "bEnable"    # Z

    .prologue
    .line 696
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->shouldSetWideBandSpeechToUse16khzVoice()Z

    move-result v2

    if-nez v2, :cond_1

    .line 747
    :cond_0
    :goto_0
    return-void

    .line 704
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isBMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 708
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v2, :cond_0

    .line 709
    if-eqz p0, :cond_2

    .line 713
    :try_start_0
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string/jumbo v4, "enableWBS"

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 714
    .local v1, "m":Ljava/lang/reflect/Method;
    if-eqz v1, :cond_0

    .line 715
    sget-object v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Object;

    invoke-virtual {v1, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_0

    .line 717
    .end local v1    # "m":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 719
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 731
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :cond_2
    :try_start_1
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string/jumbo v4, "disableWBS"

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 732
    .restart local v1    # "m":Ljava/lang/reflect/Method;
    if-eqz v1, :cond_0

    .line 733
    sget-object v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Object;

    invoke-virtual {v1, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 735
    .end local v1    # "m":Ljava/lang/reflect/Method;
    :catch_1
    move-exception v0

    .line 737
    .restart local v0    # "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 742
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_2
    move-exception v2

    goto :goto_0

    .line 740
    :catch_3
    move-exception v2

    goto :goto_0

    .line 738
    :catch_4
    move-exception v2

    goto :goto_0

    .line 724
    :catch_5
    move-exception v2

    goto :goto_0

    .line 722
    :catch_6
    move-exception v2

    goto :goto_0

    .line 720
    :catch_7
    move-exception v2

    goto :goto_0
.end method

.method public static getBTdevice()Landroid/bluetooth/BluetoothDevice;
    .locals 9

    .prologue
    .line 952
    const/4 v0, 0x0

    .line 953
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    const/4 v4, -0x1

    .line 954
    .local v4, "idx":I
    sget-object v6, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-nez v6, :cond_1

    .line 986
    :cond_0
    :goto_0
    return-object v0

    .line 958
    :cond_1
    sget-object v6, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;

    move-result-object v1

    .line 959
    .local v1, "deviceList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    .line 960
    .local v5, "nConnectedDeviceNum":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v5, :cond_3

    .line 961
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/bluetooth/BluetoothDevice;

    invoke-static {v6}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->getSamsungHandsfreeDeviceType(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    move-result-object v2

    .line 962
    .local v2, "deviceType":Ljava/lang/String;
    sget-object v6, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "[SVoiceBT] getBTdevice deviceList("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ")\'s deviceType:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 963
    if-eqz v2, :cond_2

    .line 964
    const-string/jumbo v6, "WA"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 965
    move v4, v3

    .line 960
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 970
    .end local v2    # "deviceType":Ljava/lang/String;
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_4

    .line 971
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v6

    if-eqz v6, :cond_5

    const/4 v6, -0x1

    if-eq v4, v6, :cond_5

    .line 972
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 982
    .restart local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_4
    if-eqz v0, :cond_0

    .line 983
    sget-object v6, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "[SVoiceBT] getBTdevice device\'s deviceName:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 974
    :cond_5
    const/4 v3, 0x0

    :goto_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v3, v6, :cond_4

    .line 975
    if-eq v4, v3, :cond_6

    .line 976
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 974
    .restart local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method private static getBluetoothHeadsetConnctedDevices()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 929
    :try_start_0
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 932
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-object v1

    .line 930
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 931
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 932
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public static getBluetoothSampleRate()I
    .locals 1

    .prologue
    .line 1448
    sget v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mSampleRate:I

    return v0
.end method

.method public static getSamsungHandsfreeDeviceType(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    .locals 6
    .param p0, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 991
    sget-object v5, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->BDeviceUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v5}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v4

    .line 992
    .local v4, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v2, 0x0

    .line 993
    .local v2, "deviceUtil":Lcom/vlingo/core/internal/util/BDeviceUtil;
    const/4 v1, 0x0

    .line 994
    .local v1, "deviceType":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 996
    :try_start_0
    invoke-virtual {v4}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/vlingo/core/internal/util/BDeviceUtil;

    move-object v2, v0

    .line 997
    if-eqz v2, :cond_0

    .line 998
    sget-object v5, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-interface {v2, v5, p0}, Lcom/vlingo/core/internal/util/BDeviceUtil;->getSamsungHandsfreeDeviceType(Landroid/bluetooth/BluetoothHeadset;Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 1007
    :cond_0
    :goto_0
    return-object v1

    .line 1000
    :catch_0
    move-exception v3

    .line 1001
    .local v3, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v3}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 1002
    .end local v3    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v3

    .line 1003
    .local v3, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v3}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0
.end method

.method public static ignoreBtConnectEvent(Z)V
    .locals 0
    .param p0, "flag"    # Z

    .prologue
    .line 1421
    sput-boolean p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->ignoreBtConnectEvent:Z

    .line 1422
    return-void
.end method

.method public static isBVRASupportDevice()Z
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 355
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 368
    .local v0, "context":Landroid/content/Context;
    :cond_0
    :goto_0
    return v3

    .line 358
    .end local v0    # "context":Landroid/content/Context;
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 360
    .restart local v0    # "context":Landroid/content/Context;
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v6, "supported_bvra"

    const/4 v7, 0x1

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 361
    .local v2, "v":I
    if-nez v2, :cond_0

    move v3, v4

    goto :goto_0

    .line 362
    .end local v2    # "v":I
    :catch_0
    move-exception v1

    .line 364
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move v3, v4

    .line 368
    goto :goto_0
.end method

.method private static isBluetoothAdapterOn()Z
    .locals 2

    .prologue
    .line 351
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isBluetoothAudioOn()Z
    .locals 4

    .prologue
    .line 808
    const/4 v1, 0x0

    .line 810
    .local v1, "useHeadsetForRecord":Z
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isOnlyBDeviceConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 811
    const/4 v1, 0x0

    .line 832
    .local v0, "am":Landroid/media/AudioManager;
    :goto_0
    return v1

    .line 814
    .end local v0    # "am":Landroid/media/AudioManager;
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "audio"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 815
    .restart local v0    # "am":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v1

    .line 832
    goto :goto_0
.end method

.method public static isBluetoothAudioSupported()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 835
    const-string/jumbo v3, "listen_over_bluetooth"

    invoke-static {v3, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 836
    .local v1, "useBluetoothForAudio":Z
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBVRASupportDevice()Z

    move-result v0

    .line 837
    .local v0, "supportBVRA":Z
    if-eqz v1, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    .line 840
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isHeadsetConnected()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 322
    const/4 v1, 0x1

    .line 324
    .local v1, "headsetConnected":Z
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isOnlyBDeviceConnected()Z

    move-result v5

    if-eqz v5, :cond_1

    move v1, v4

    .line 347
    :cond_0
    :goto_0
    return v1

    .line 331
    :cond_1
    :try_start_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    .line 332
    .local v0, "defaultBluetoothAdapter":Landroid/bluetooth/BluetoothAdapter;
    if-eqz v0, :cond_2

    .line 333
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/bluetooth/BluetoothAdapter;->getProfileConnectionState(I)I

    move-result v3

    .line 336
    .local v3, "state":I
    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 337
    const/4 v1, 0x0

    goto :goto_0

    .line 340
    .end local v3    # "state":I
    :cond_2
    const-string/jumbo v5, "bluetooth_headset_connected"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 342
    .end local v0    # "defaultBluetoothAdapter":Landroid/bluetooth/BluetoothAdapter;
    :catch_0
    move-exception v2

    .line 343
    .local v2, "mex":Ljava/lang/NoSuchMethodError;
    const-string/jumbo v5, "bluetooth_headset_connected"

    invoke-static {v5, v4}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_0
.end method

.method private static isIgnoreIntentForBDevice(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 2
    .param p0, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 381
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v1, :cond_0

    if-eqz p0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 383
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->toString()Ljava/lang/String;

    move-result-object v0

    .line 384
    .local v0, "bDevice":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothDevice;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 385
    const/4 v1, 0x1

    .line 388
    .end local v0    # "bDevice":Ljava/lang/String;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static isListenOverBTEnabled()Z
    .locals 2

    .prologue
    .line 376
    const-string/jumbo v0, "listen_over_bluetooth"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isOnlyBDeviceConnected()Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 937
    sget-object v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v3, :cond_0

    .line 938
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->getBluetoothHeadsetConnctedDevices()Ljava/util/List;

    move-result-object v0

    .line 939
    .local v0, "deviceList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 940
    .local v2, "nConnectedDeviceNum":I
    if-ne v2, v4, :cond_0

    .line 941
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/bluetooth/BluetoothDevice;

    invoke-static {v3}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->getSamsungHandsfreeDeviceType(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    move-result-object v1

    .line 942
    .local v1, "deviceType":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string/jumbo v3, "WA"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v4

    .line 948
    .end local v1    # "deviceType":Ljava/lang/String;
    :goto_0
    return v3

    :cond_0
    move v3, v5

    goto :goto_0
.end method

.method public static notifyBluetoothServiceConnectedToListener()V
    .locals 3

    .prologue
    .line 1411
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->LISTENERS:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;

    .line 1412
    .local v1, "listener":Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;
    invoke-interface {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;->onBluetoothServiceConnected()V

    goto :goto_0

    .line 1414
    .end local v1    # "listener":Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;
    :cond_0
    return-void
.end method

.method private static notifyHeadsetStateToListeners(Z)V
    .locals 3
    .param p0, "state"    # Z

    .prologue
    .line 1401
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->LISTENERS:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;

    .line 1402
    .local v1, "listener":Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;
    if-eqz p0, :cond_0

    .line 1403
    invoke-interface {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;->onHeadsetConnected()V

    goto :goto_0

    .line 1405
    :cond_0
    invoke-interface {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;->onHeadsetDisconnected()V

    goto :goto_0

    .line 1408
    .end local v1    # "listener":Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;
    :cond_1
    return-void
.end method

.method private static notifyScoStateToListeners(Z)V
    .locals 3
    .param p0, "state"    # Z

    .prologue
    .line 1391
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->LISTENERS:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;

    .line 1392
    .local v1, "listener":Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;
    if-eqz p0, :cond_0

    .line 1393
    invoke-interface {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;->onScoConnected()V

    goto :goto_0

    .line 1395
    :cond_0
    invoke-interface {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;->onScoDisconnected()V

    goto :goto_0

    .line 1398
    .end local v1    # "listener":Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;
    :cond_1
    return-void
.end method

.method public static declared-synchronized onAppStateChanged(Z)V
    .locals 4
    .param p0, "isShown"    # Z

    .prologue
    .line 420
    const-class v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "[SVoiceBT, LatencyCheck] BT onAppStateChanged(), isShown="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    if-eqz p0, :cond_1

    .line 426
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->ignoreBtConnectEvent(Z)V

    .line 427
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    const/16 v2, 0xc

    if-ne v0, v2, :cond_0

    .line 428
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startSCO()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 436
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 433
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopSCO(Z)V

    .line 434
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->ignoreBtConnectEvent(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 420
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized onHeadsetStateChanged(Z)V
    .locals 6
    .param p0, "isConnected"    # Z

    .prologue
    .line 443
    const-class v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "[SVoiceBT, LatencyCheck] BT onHeadsetStateChanged(): isConnected= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->updateHeadsetStateSetting(Z)V

    .line 445
    if-eqz p0, :cond_2

    .line 447
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->openBtProxy()V

    .line 450
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getVlingoApp()Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/util/VlingoApplicationInterface;->isAppInForeground()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 463
    :cond_0
    const-string/jumbo v2, "launch_car_on_bt_connect"

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 464
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getVlingoApp()Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/util/VlingoApplicationInterface;->startMainActivity()V

    .line 467
    :cond_1
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->notifyHeadsetStateToListeners(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 500
    :goto_0
    monitor-exit v3

    return-void

    .line 472
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->getBluetoothHeadsetConnctedDevices()Ljava/util/List;

    move-result-object v0

    .line 473
    .local v0, "deviceList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 474
    .local v1, "nConnectedDeviceNum":I
    if-nez v1, :cond_3

    .line 475
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->closeBtProxy()V

    .line 477
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v2

    if-nez v2, :cond_5

    .line 478
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSmEndpointManager()Lcom/vlingo/core/internal/endpoints/EndpointManager;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 479
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSmEndpointManager()Lcom/vlingo/core/internal/endpoints/EndpointManager;

    move-result-object v2

    sget-object v4, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->LONG_LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    const/16 v5, 0x8ca

    invoke-virtual {v2, v4, v5}, Lcom/vlingo/core/internal/endpoints/EndpointManager;->setSilenceDurationWithSpeech(Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;I)I

    .line 482
    :cond_4
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getVlingoApp()Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/util/VlingoApplicationInterface;->isAppInForeground()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 485
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopSCO(Z)V

    .line 498
    :cond_5
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->notifyHeadsetStateToListeners(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 443
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    .end local v1    # "nConnectedDeviceNum":I
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static declared-synchronized onListenOverBTSettingChanged(Z)V
    .locals 2
    .param p0, "isEnabled"    # Z

    .prologue
    .line 397
    const-class v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v1

    if-eqz p0, :cond_0

    .line 407
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startSCO()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413
    :goto_0
    monitor-exit v1

    return-void

    .line 411
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopSCO(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 397
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized onScoStateChanged(ZI)V
    .locals 8
    .param p0, "isConnected"    # Z
    .param p1, "priorState"    # I

    .prologue
    const/4 v7, 0x1

    .line 511
    const-class v5, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v5

    :try_start_0
    sget-boolean v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mRightBeforeForeground:Z

    if-nez v4, :cond_1

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getVlingoApp()Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/util/VlingoApplicationInterface;->isAppInForeground()Z

    move-result v4

    if-nez v4, :cond_1

    .line 512
    const/4 v4, 0x0

    sput-boolean v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z

    .line 515
    if-nez p0, :cond_0

    .line 516
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->notifyScoStateToListeners(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 620
    :cond_0
    :goto_0
    monitor-exit v5

    return-void

    .line 526
    :cond_1
    if-eqz p0, :cond_4

    .line 529
    :try_start_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSmEndpointManager()Lcom/vlingo/core/internal/endpoints/EndpointManager;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 530
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 531
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSmEndpointManager()Lcom/vlingo/core/internal/endpoints/EndpointManager;

    move-result-object v4

    sget-object v6, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->MEDIUM_LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    const/16 v7, 0x4e2

    invoke-virtual {v4, v6, v7}, Lcom/vlingo/core/internal/endpoints/EndpointManager;->setSilenceDurationWithSpeech(Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;I)I

    .line 537
    :cond_2
    :goto_1
    const/4 v4, 0x1

    invoke-static {v4}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->notifyScoStateToListeners(Z)V

    .line 538
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startSCOConnectedTask()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 511
    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4

    .line 533
    :cond_3
    :try_start_2
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSmEndpointManager()Lcom/vlingo/core/internal/endpoints/EndpointManager;

    move-result-object v4

    sget-object v6, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->LONG_LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    const/16 v7, 0xa8c

    invoke-virtual {v4, v6, v7}, Lcom/vlingo/core/internal/endpoints/EndpointManager;->setSilenceDurationWithSpeech(Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;I)I

    goto :goto_1

    .line 542
    :cond_4
    const/16 v4, 0xb

    if-ne v4, p1, :cond_a

    .line 545
    const/4 v1, 0x0

    .line 546
    .local v1, "retry":Z
    sget-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v4, :cond_8

    .line 552
    :try_start_3
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v4

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Landroid/bluetooth/BluetoothAdapter;->getProfileConnectionState(I)I
    :try_end_3
    .catch Ljava/lang/NoSuchMethodError; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v2

    .line 558
    .local v2, "state":I
    :goto_2
    const/4 v4, 0x2

    if-eq v2, v4, :cond_5

    if-ne v2, v7, :cond_6

    .line 559
    :cond_5
    const/4 v1, 0x1

    .line 565
    .end local v2    # "state":I
    :cond_6
    :goto_3
    if-eqz v1, :cond_9

    .line 567
    :try_start_4
    sget-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;

    if-eqz v4, :cond_7

    .line 568
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->cancelScoTimeoutTask()V

    .line 570
    :cond_7
    new-instance v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoTimeoutTask;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct {v4, v6, v7}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoTimeoutTask;-><init>(ZZ)V

    sput-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;

    .line 572
    const/4 v3, 0x0

    .line 573
    .local v3, "timer":Ljava/util/Timer;
    invoke-static {}, Lcom/vlingo/core/internal/util/TimerSingleton;->getTimer()Ljava/util/Timer;

    move-result-object v3

    .line 574
    sget-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoTimeoutTask:Ljava/util/TimerTask;

    const-wide/16 v6, 0x7d0

    invoke-virtual {v3, v4, v6, v7}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 582
    .end local v3    # "timer":Ljava/util/Timer;
    :goto_4
    const/4 v4, 0x0

    sput-boolean v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z

    goto :goto_0

    .line 553
    :catch_0
    move-exception v0

    .line 554
    .local v0, "mex":Ljava/lang/NoSuchMethodError;
    const/4 v2, 0x1

    .restart local v2    # "state":I
    goto :goto_2

    .line 563
    .end local v0    # "mex":Ljava/lang/NoSuchMethodError;
    .end local v2    # "state":I
    :cond_8
    const/4 v1, 0x1

    goto :goto_3

    .line 579
    :cond_9
    const/4 v4, 0x1

    invoke-static {v4}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopSCO(Z)V

    .line 580
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->notifyScoStateToListeners(Z)V

    goto :goto_4

    .line 588
    .end local v1    # "retry":Z
    :cond_a
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v4

    if-nez v4, :cond_b

    .line 589
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSmEndpointManager()Lcom/vlingo/core/internal/endpoints/EndpointManager;

    move-result-object v4

    if-eqz v4, :cond_b

    .line 590
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSmEndpointManager()Lcom/vlingo/core/internal/endpoints/EndpointManager;

    move-result-object v4

    sget-object v6, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->LONG_LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    const/16 v7, 0x8ca

    invoke-virtual {v4, v6, v7}, Lcom/vlingo/core/internal/endpoints/EndpointManager;->setSilenceDurationWithSpeech(Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;I)I

    .line 595
    :cond_b
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->enableWideBandSpeech(Z)V

    .line 596
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->notifyScoStateToListeners(Z)V

    .line 600
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isListenOverBTEnabled()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 601
    sget-object v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mAppCloseType:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    sget-object v6, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;->BT_BUTTON:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    if-ne v4, v6, :cond_c

    sget-boolean v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z

    if-nez v4, :cond_c

    .line 618
    :cond_c
    :goto_5
    const/4 v4, 0x0

    sput-boolean v4, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z

    goto/16 :goto_0

    .line 614
    :cond_d
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isListenOverBTEnabled()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v4

    if-eqz v4, :cond_c

    goto :goto_5
.end method

.method public static declared-synchronized openBtProxy()V
    .locals 5

    .prologue
    .line 276
    const-class v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "====[SVoiceBT, LatencyCheck] openBtProxy==="

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-nez v0, :cond_0

    .line 278
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->bluetoothManagerInit()Z

    .line 279
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mProfileListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v4}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281
    :cond_0
    monitor-exit v1

    return-void

    .line 276
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static removeListener(Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;

    .prologue
    .line 1384
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->LISTENERS:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 1385
    return-void
.end method

.method public static resumeListenOverBTSCO()V
    .locals 1

    .prologue
    .line 892
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAdapterOn()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isListenOverBTEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 893
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 894
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 911
    :cond_0
    return-void
.end method

.method public static declared-synchronized runTaskOnBluetoothAudioOn(Ljava/lang/Runnable;J)V
    .locals 4
    .param p0, "task"    # Ljava/lang/Runnable;
    .param p1, "maxWait"    # J

    .prologue
    .line 853
    const-class v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v1

    :try_start_0
    sput-object p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mOnScoConnectedTask:Ljava/lang/Runnable;

    .line 854
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v2, p1

    sput-wide v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mOnScoConnectedTaskTimeout:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 855
    monitor-exit v1

    return-void

    .line 853
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static setBluetoothSampleRate(I)V
    .locals 4
    .param p0, "sampleRateType"    # I

    .prologue
    const/16 v3, 0x1f40

    .line 1433
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "[SVoiceBT, LatencyCheck, SampleRate] SampleRateType of headset = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1434
    packed-switch p0, :pswitch_data_0

    .line 1442
    sput v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mSampleRate:I

    .line 1445
    :goto_0
    return-void

    .line 1436
    :pswitch_0
    sput v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mSampleRate:I

    goto :goto_0

    .line 1439
    :pswitch_1
    const/16 v0, 0x3e80

    sput v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mSampleRate:I

    goto :goto_0

    .line 1434
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static setCancelTurnTiming(Z)V
    .locals 0
    .param p0, "flag"    # Z

    .prologue
    .line 1425
    sput-boolean p0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->cancelTurnTiming:Z

    .line 1426
    return-void
.end method

.method private static declared-synchronized startSCO()V
    .locals 2

    .prologue
    .line 669
    const-class v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    invoke-static {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startSCO(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 670
    monitor-exit v0

    return-void

    .line 669
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static declared-synchronized startSCO(Z)V
    .locals 5
    .param p0, "retry"    # Z

    .prologue
    .line 626
    const-class v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "[SVoiceBT, LatencyCheck] BT startSCO()"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBVRASupportDevice()Z

    move-result v2

    if-nez v2, :cond_1

    .line 629
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "[SVoiceBT, LatencyCheck] BT Ignoring startSCO() (device doesn\'t support BVRA)"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 666
    :cond_0
    :goto_0
    monitor-exit v3

    return-void

    .line 633
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isListenOverBTEnabled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 634
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "[SVoiceBT, LatencyCheck] BT Ignoring startSCO() (ListenOverBluetooth setting is OFF)"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 626
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 638
    :cond_2
    :try_start_2
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAdapterOn()Z

    move-result v2

    if-nez v2, :cond_3

    .line 639
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "[SVoiceBT, LatencyCheck] BT Ignoring startSCO() (Bluetooth Adapter is OFF)"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 649
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isOnlyBDeviceConnected()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 650
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "[SVoiceBT, LatencyCheck] BT Ignoring startSCO() (ONLY B-Device is connected on H-Device Mode)"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 654
    :cond_4
    sget-boolean v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mIsStartSCOinProgress:Z

    if-nez v2, :cond_5

    sget-boolean v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->ignoreBtConnectEvent:Z

    if-nez v2, :cond_5

    .line 655
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 656
    .local v0, "data":Landroid/os/Bundle;
    const-string/jumbo v2, "sco_retry_flag"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 657
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    iget-object v2, v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 658
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    iget-object v2, v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 659
    .local v1, "msg":Landroid/os/Message;
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 660
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoWorkerTask:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;

    iget-object v2, v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$ScoWorkerTask;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 664
    .end local v0    # "data":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :cond_5
    sget-object v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "[SVoiceBT, LatencyCheck] BT Ignoring startSCO() as ANOTHER in progress"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static startSCOConnectedTask()V
    .locals 3

    .prologue
    .line 792
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->cancelScoTimeoutTask()V

    .line 793
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mOnScoConnectedTask:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 796
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mOnScoConnectedTask:Ljava/lang/Runnable;

    const-wide/16 v1, 0xc8

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V

    .line 798
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->cancelScoConnectedTask()V

    .line 799
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->notifyScoStateToListeners(Z)V

    .line 800
    return-void
.end method

.method public static declared-synchronized startScoOnStartRecognition()V
    .locals 5

    .prologue
    .line 673
    const-class v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "[SVoiceBT, LatencyCheck] startScoOnStartRecognition"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 674
    const/4 v1, 0x0

    sput-boolean v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z

    .line 675
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->ignoreBtConnectEvent(Z)V

    .line 676
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startSCO(Z)V

    .line 677
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->ignoreBtConnectEvent(Z)V

    .line 679
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->cancelTurnTiming:Z

    if-eqz v1, :cond_0

    .line 680
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelAudio()V

    .line 681
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 683
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 684
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v1

    const/4 v3, 0x6

    const/4 v4, 0x2

    invoke-virtual {v1, v3, v4}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 687
    :cond_0
    monitor-exit v2

    return-void

    .line 673
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private static declared-synchronized stopSCO(Z)V
    .locals 5
    .param p0, "ignoreListenOverBTSetting"    # Z

    .prologue
    .line 754
    const-class v2, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "[SVoiceBT, LatencyCheck] BT stopSCO()"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 755
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->enableWideBandSpeech(Z)V

    .line 756
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isListenOverBTEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    if-nez p0, :cond_0

    .line 757
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "[SVoiceBT, LatencyCheck] BT Ignoring stopSCO() (ListenOverBluetooth setting is OFF)"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 786
    :goto_0
    monitor-exit v2

    return-void

    .line 761
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance()Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->remoteHasUiFocus()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getVlingoApp()Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/util/VlingoApplicationInterface;->isAppInForeground()Z

    move-result v1

    if-nez v1, :cond_1

    .line 762
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "[SVoiceBT, LatencyCheck] BT Ignoring stopSCO() remote has ui focus"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 754
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 766
    :cond_1
    :try_start_2
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBVRASupportDevice()Z

    move-result v1

    if-nez v1, :cond_2

    .line 767
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "[SVoiceBT, LatencyCheck] BT Ignoring stopSCO() (device doesn\'t support BVRA)"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 771
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/PhoneUtil;->phoneInUse(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_3

    sget-boolean v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mIsStartSCOinProgress:Z

    if-nez v1, :cond_3

    .line 772
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "[SVoiceBT, LatencyCheck] BT Ignoring stopSCO() bluetooth Audio off"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 776
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->cancelScoTimeoutTask()V

    .line 778
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v1, :cond_4

    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v1, :cond_4

    .line 779
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    sget-object v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1, v3}, Landroid/bluetooth/BluetoothHeadset;->stopVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    .line 780
    .local v0, "result":Z
    const/4 v1, 0x0

    sput-boolean v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mIsStartSCOinProgress:Z

    .line 781
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "[SVoiceBT, LatencyCheck] BT StartScoThread:run() stopVoiceRecognition() result = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 784
    .end local v0    # "result":Z
    :cond_4
    sget-object v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "[SVoiceBT, LatencyCheck] BT stopSCO() mBluetoothHeadset is NULL"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public static declared-synchronized stopScoOnIdle()V
    .locals 3

    .prologue
    .line 690
    const-class v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "[SVoiceBT, LatencyCheck] stopScoOnIdle"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 691
    const/4 v0, 0x1

    sput-boolean v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->mScoDisconnectingByAudioFocusLoss:Z

    .line 692
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopSCO(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 693
    monitor-exit v1

    return-void

    .line 690
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static suspendListenOverBTSCO()V
    .locals 1

    .prologue
    .line 879
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAdapterOn()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isListenOverBTEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 880
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 885
    :cond_0
    return-void
.end method

.method public static declared-synchronized updateHeadsetStateSetting(Z)V
    .locals 2
    .param p0, "isConnected"    # Z

    .prologue
    .line 313
    const-class v1, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;

    monitor-enter v1

    :try_start_0
    const-string/jumbo v0, "bluetooth_headset_connected"

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 314
    monitor-exit v1

    return-void

    .line 313
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 1370
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isIdle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1373
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->suspendListenOverBTSCO()V

    .line 1375
    :cond_0
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 1348
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isIdle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1351
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->suspendListenOverBTSCO()V

    .line 1353
    :cond_0
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 1359
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isIdle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1362
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->suspendListenOverBTSCO()V

    .line 1364
    :cond_0
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 1335
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->resumeListenOverBTSCO()V

    .line 1337
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1338
    :cond_0
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/audio/AudioRequest;->setStream(I)V

    .line 1342
    :goto_0
    return-void

    .line 1340
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/audio/AudioRequest;->setStream(I)V

    goto :goto_0
.end method
