.class public Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "EditScheduleAction.java"


# instance fields
.field private mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method


# virtual methods
.method protected execute()V
    .locals 5

    .prologue
    .line 22
    const/4 v1, 0x1

    .line 23
    .local v1, "typeEvent":I
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.EDIT"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 24
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getNewEventUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 26
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getNewEventUri()Landroid/net/Uri;

    move-result-object v2

    if-nez v2, :cond_0

    .line 27
    const-string/jumbo v2, "vnd.android.cursor.item/event"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 30
    :cond_0
    const-string/jumbo v2, "action_event_type"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 31
    const-string/jumbo v2, "title"

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 32
    const-string/jumbo v2, "beginTime"

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBegin()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 33
    const-string/jumbo v2, "endTime"

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEnd()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 34
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 36
    return-void
.end method

.method public readyToExec()V
    .locals 0

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;->execute()V

    .line 43
    return-void
.end method

.method public scheduleEvent(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;
    .locals 0
    .param p1, "scheduleEvent"    # Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/EditScheduleAction;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 18
    return-object p0
.end method
