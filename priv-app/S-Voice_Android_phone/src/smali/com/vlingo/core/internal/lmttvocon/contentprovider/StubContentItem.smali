.class public Lcom/vlingo/core/internal/lmttvocon/contentprovider/StubContentItem;
.super Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
.source "StubContentItem.java"


# instance fields
.field private final checksum:I


# direct methods
.method public constructor <init>(JI)V
    .locals 0
    .param p1, "id"    # J
    .param p3, "checksum"    # I

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;-><init>(J)V

    .line 11
    iput p3, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/StubContentItem;->checksum:I

    .line 12
    return-void
.end method


# virtual methods
.method public getChecksum()I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/StubContentItem;->checksum:I

    return v0
.end method

.method public getOrthographies()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
