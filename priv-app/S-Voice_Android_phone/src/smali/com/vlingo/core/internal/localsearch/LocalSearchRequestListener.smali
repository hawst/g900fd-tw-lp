.class public interface abstract Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;
.super Ljava/lang/Object;
.source "LocalSearchRequestListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;
    }
.end annotation


# virtual methods
.method public abstract onRequestComplete(ZLjava/lang/Object;)V
.end method

.method public abstract onRequestFailed(Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;)V
.end method

.method public abstract onRequestFailed(Ljava/lang/String;)V
.end method

.method public abstract onRequestScheduled()V
.end method
