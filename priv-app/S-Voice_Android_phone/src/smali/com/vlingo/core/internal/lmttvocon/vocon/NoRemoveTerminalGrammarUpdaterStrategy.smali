.class public Lcom/vlingo/core/internal/lmttvocon/vocon/NoRemoveTerminalGrammarUpdaterStrategy;
.super Ljava/lang/Object;
.source "NoRemoveTerminalGrammarUpdaterStrategy.java"

# interfaces
.implements Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdaterStrategy;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public startFromScratch(IIII)Z
    .locals 3
    .param p1, "allActionsCount"    # I
    .param p2, "addedActionsCount"    # I
    .param p3, "modifiedActionsCount"    # I
    .param p4, "removedActionsCount"    # I

    .prologue
    const/4 v0, 0x1

    .line 6
    if-gtz p1, :cond_1

    .line 15
    :cond_0
    :goto_0
    return v0

    .line 9
    :cond_1
    if-gtz p4, :cond_0

    if-gtz p3, :cond_0

    .line 12
    if-lez p2, :cond_2

    if-lez p1, :cond_2

    mul-int/lit8 v1, p2, 0x64

    div-int/2addr v1, p1

    const/16 v2, 0xa

    if-gt v1, v2, :cond_0

    .line 15
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
