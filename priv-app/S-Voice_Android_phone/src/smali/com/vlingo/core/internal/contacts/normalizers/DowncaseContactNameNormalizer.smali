.class public Lcom/vlingo/core/internal/contacts/normalizers/DowncaseContactNameNormalizer;
.super Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;
.source "DowncaseContactNameNormalizer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/normalizers/ContactNameNormalizer;-><init>()V

    return-void
.end method


# virtual methods
.method public canNormalize(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isRussian(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public normalize(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
