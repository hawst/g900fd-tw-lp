.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$2;
.super Ljava/lang/Object;
.source "PhraseSpotter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->onPhraseSpotterStopped()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)V
    .locals 0

    .prologue
    .line 322
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$2;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 324
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$2;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    monitor-enter v1

    .line 325
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$2;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->restartWhenStopped:Z
    invoke-static {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$800(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 326
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "onPhraseSpotterStopped: restartWhenStopped==true, so restarting."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$2;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    const/4 v2, 0x0

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->startPhraseSpottingInternal(Z)V
    invoke-static {v0, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Z)V

    .line 328
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$2;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    const/4 v2, 0x0

    # setter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->restartWhenStopped:Z
    invoke-static {v0, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$802(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Z)Z

    .line 333
    :cond_0
    :goto_0
    monitor-exit v1

    .line 334
    return-void

    .line 330
    :cond_1
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$2;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->listener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$900(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$2;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->listener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$900(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;->onPhraseSpotterStopped()V

    goto :goto_0

    .line 333
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
