.class public final Lcom/vlingo/core/internal/associatedservice/UiFocusManager;
.super Ljava/lang/Object;
.source "UiFocusManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/associatedservice/UiFocusManager$UiFocusBroadcastReceiver;,
        Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;
    }
.end annotation


# static fields
.field public static final ACTION_ABANDONED_UIFOCUS:Ljava/lang/String; = "com.vlingo.core.action.ABANDONED_UIFOCUS"

.field public static final ACTION_ACQUIRED_UIFOCUS:Ljava/lang/String; = "com.vlingo.core.action.ACQUIRED_UIFOCUS"

.field public static final ACTION_REQUEST_UIFOCUS:Ljava/lang/String; = "com.vlingo.core.action.REQUEST_UIFOCUS"

.field public static final EXTRA_ID:Ljava/lang/String; = "id"

.field static final TAG:Ljava/lang/String;

.field public static final UIFOCUS_GAIN:I = 0x1

.field public static final UIFOCUS_LOSS:I

.field public static final VERSION:I

.field private static smInstance:Lcom/vlingo/core/internal/associatedservice/UiFocusManager;


# instance fields
.field private mHasFocus:Z

.field private mId:Ljava/lang/String;

.field private mListenerWeakRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private mRemoteId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p1, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mId:Ljava/lang/String;

    .line 102
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mHasFocus:Z

    .line 103
    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 51
    invoke-static {p0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/associatedservice/UiFocusManager;Landroid/content/Context;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/associatedservice/UiFocusManager;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Z

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->setRemoteHasUiFocus(Landroid/content/Context;Z)V

    return-void
.end method

.method static synthetic access$202(Lcom/vlingo/core/internal/associatedservice/UiFocusManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/associatedservice/UiFocusManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mRemoteId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/associatedservice/UiFocusManager;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/associatedservice/UiFocusManager;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->onRemoteUiFocusRequest(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/associatedservice/UiFocusManager;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/associatedservice/UiFocusManager;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->onRemoteUiFocusAbandoned(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/associatedservice/UiFocusManager;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/associatedservice/UiFocusManager;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->onRemoteUiFocusAcquired(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/vlingo/core/internal/associatedservice/UiFocusManager;
    .locals 4

    .prologue
    .line 86
    const-class v2, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    monitor-enter v2

    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 87
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 88
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "core has not been initialized!"

    invoke-direct {v1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 90
    :cond_0
    :try_start_1
    invoke-static {v0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    monitor-exit v2

    return-object v1
.end method

.method private static declared-synchronized getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 94
    const-class v1, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->smInstance:Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    if-nez v0, :cond_0

    .line 95
    new-instance v0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->smInstance:Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    .line 97
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->smInstance:Lcom/vlingo/core/internal/associatedservice/UiFocusManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getRemoteHasUiFocus(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 287
    invoke-static {}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->hasSvoiceProvider()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 288
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 289
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string/jumbo v2, "uifocus.remote_has_focus"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 292
    .end local v0    # "prefs":Landroid/content/SharedPreferences;
    :goto_0
    return v1

    .line 291
    :cond_0
    invoke-static {p1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v2

    invoke-direct {v2, p1, v1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->setRemoteHasUiFocus(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method public static hasSvoiceProvider()Z
    .locals 1

    .prologue
    .line 278
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isGearProviderExistOnPhone()Z

    move-result v0

    return v0
.end method

.method private notifyListener(I)V
    .locals 2
    .param p1, "focusChange"    # I

    .prologue
    .line 201
    iget-object v1, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mListenerWeakRef:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    .line 202
    iget-object v1, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mListenerWeakRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    .line 203
    .local v0, "l":Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;
    if-eqz v0, :cond_0

    .line 204
    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;->onUiFocusChange(I)V

    .line 207
    .end local v0    # "l":Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;
    :cond_0
    return-void
.end method

.method private onRemoteUiFocusAbandoned(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "remoteId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 241
    sget-object v1, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onRemoteUiFocusAbandoned() remoteId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", localId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->setRemoteHasUiFocus(Landroid/content/Context;Z)V

    .line 247
    iget-object v1, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mListenerWeakRef:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    .line 253
    iget-object v1, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mListenerWeakRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    .line 254
    .local v0, "l":Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;
    if-eqz v0, :cond_0

    .line 255
    invoke-virtual {p0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->hasUiFocus()Z

    move-result v1

    if-nez v1, :cond_0

    .line 256
    iput-boolean v4, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mHasFocus:Z

    .line 257
    const-string/jumbo v1, "com.vlingo.core.action.ACQUIRED_UIFOCUS"

    invoke-direct {p0, p1, v1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->sendBroadcast(Landroid/content/Context;Ljava/lang/String;)V

    .line 258
    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->notifyListener(I)V

    .line 262
    .end local v0    # "l":Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;
    :cond_0
    return-void
.end method

.method private onRemoteUiFocusAcquired(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "remoteId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 266
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onRemoteUiFocusAcquired() remoteId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", localId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->setRemoteHasUiFocus(Landroid/content/Context;Z)V

    .line 271
    invoke-virtual {p0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->hasUiFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 272
    iput-boolean v3, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mHasFocus:Z

    .line 273
    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->notifyListener(I)V

    .line 275
    :cond_0
    return-void
.end method

.method private onRemoteUiFocusRequest(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "remoteId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 221
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onRemoteUiFocusRequest() remoteId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", localId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->setRemoteHasUiFocus(Landroid/content/Context;Z)V

    .line 230
    invoke-virtual {p0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->hasUiFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    iput-boolean v3, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mHasFocus:Z

    .line 232
    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->notifyListener(I)V

    .line 236
    :cond_0
    const-string/jumbo v0, "com.vlingo.core.action.ABANDONED_UIFOCUS"

    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->sendBroadcast(Landroid/content/Context;Ljava/lang/String;)V

    .line 237
    return-void
.end method

.method private requestUiFocusInternal(ZLcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;)V
    .locals 4
    .param p1, "force"    # Z
    .param p2, "listener"    # Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    .prologue
    const/4 v3, 0x1

    .line 148
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "requestUiFocusInternal() force="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", mId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    if-nez p2, :cond_0

    .line 151
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "listener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 154
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mListenerWeakRef:Ljava/lang/ref/WeakReference;

    .line 156
    invoke-virtual {p0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->hasUiFocus()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 158
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "  already has UI focus, notifying..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->notifyListener(I)V

    .line 179
    :cond_1
    :goto_0
    return-void

    .line 162
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->remoteHasUiFocus()Z

    move-result v0

    if-nez v0, :cond_3

    .line 165
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "  no one has UI focus, taking ownership and notifying..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    iput-boolean v3, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mHasFocus:Z

    .line 168
    const-string/jumbo v0, "com.vlingo.core.action.ACQUIRED_UIFOCUS"

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->sendBroadcast(Ljava/lang/String;)V

    .line 169
    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->notifyListener(I)V

    goto :goto_0

    .line 171
    :cond_3
    if-eqz p1, :cond_1

    .line 174
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "  remote has UI focus, broadcasting request..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    const-string/jumbo v0, "com.vlingo.core.action.REQUEST_UIFOCUS"

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->sendBroadcast(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private sendBroadcast(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "action"    # Ljava/lang/String;

    .prologue
    .line 215
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 216
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "id"

    iget-object v2, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 217
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 218
    return-void
.end method

.method private sendBroadcast(Ljava/lang/String;)V
    .locals 2
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 210
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 211
    .local v0, "context":Landroid/content/Context;
    invoke-direct {p0, v0, p1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->sendBroadcast(Landroid/content/Context;Ljava/lang/String;)V

    .line 212
    return-void
.end method

.method private setRemoteHasUiFocus(Landroid/content/Context;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "value"    # Z

    .prologue
    .line 297
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 298
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 299
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v2, "uifocus.remote_has_focus"

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 300
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 301
    return-void
.end method


# virtual methods
.method public abandonUiFocus()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 186
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "abandonUiFocus() mId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    iget-object v0, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 189
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "core is not initialized !"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 192
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->hasUiFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 193
    iput-boolean v3, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mHasFocus:Z

    .line 194
    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->notifyListener(I)V

    .line 195
    const-string/jumbo v0, "com.vlingo.core.action.ABANDONED_UIFOCUS"

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->sendBroadcast(Ljava/lang/String;)V

    .line 197
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mListenerWeakRef:Ljava/lang/ref/WeakReference;

    .line 198
    return-void
.end method

.method public getRemoteId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mRemoteId:Ljava/lang/String;

    return-object v0
.end method

.method public hasUiFocus()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->mHasFocus:Z

    return v0
.end method

.method public initRemoteHasUiFocus(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 304
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->setRemoteHasUiFocus(Landroid/content/Context;Z)V

    .line 305
    return-void
.end method

.method public remoteHasUiFocus()Z
    .locals 2

    .prologue
    .line 118
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 119
    .local v0, "context":Landroid/content/Context;
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getRemoteHasUiFocus(Landroid/content/Context;)Z

    move-result v1

    return v1
.end method

.method public requestUiFocus(Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    .prologue
    .line 130
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->requestUiFocusInternal(ZLcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;)V

    .line 131
    return-void
.end method

.method public waitForUiFocus(Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    .prologue
    .line 143
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->requestUiFocusInternal(ZLcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;)V

    .line 144
    return-void
.end method
