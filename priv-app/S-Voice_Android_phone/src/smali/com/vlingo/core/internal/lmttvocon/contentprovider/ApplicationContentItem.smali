.class public Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;
.super Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
.source "ApplicationContentItem.java"


# instance fields
.field private final aliases:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final name:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/util/Collection;)V
    .locals 0
    .param p1, "id"    # J
    .param p3, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 13
    .local p4, "aliases":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;-><init>(J)V

    .line 14
    iput-object p3, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;->name:Ljava/lang/String;

    .line 15
    iput-object p4, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;->aliases:Ljava/util/Collection;

    .line 16
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 45
    if-nez p1, :cond_1

    .line 52
    :cond_0
    :goto_0
    return v1

    .line 48
    :cond_1
    instance-of v2, p1, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 49
    check-cast v0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;

    .line 50
    .local v0, "application":Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;->getId()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;->getId()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;->name:Ljava/lang/String;

    if-nez v2, :cond_2

    iget-object v2, v0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;->name:Ljava/lang/String;

    if-nez v2, :cond_0

    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;->name:Ljava/lang/String;

    iget-object v3, v0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public getChecksum()I
    .locals 4

    .prologue
    .line 20
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;->name:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;->makeChecksum([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getOrthographies()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 30
    .local v2, "orthographies":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;->name:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 31
    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;->name:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 33
    :cond_0
    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;->aliases:Ljava/util/Collection;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;->aliases:Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 34
    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;->aliases:Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 35
    .local v0, "alias":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 36
    invoke-interface {v2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 40
    .end local v0    # "alias":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    return-object v2
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ApplicationContentItem;->getChecksum()I

    move-result v0

    return v0
.end method
