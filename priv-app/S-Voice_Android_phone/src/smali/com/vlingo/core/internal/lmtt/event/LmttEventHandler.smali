.class public final Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;
.super Ljava/lang/Object;
.source "LmttEventHandler.java"

# interfaces
.implements Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;


# static fields
.field public static final EVENT_APP_LAUNCHED:Ljava/lang/String; = "app_launched"

.field public static final EVENT_TOS_ACCEPTED:Ljava/lang/String; = "tos_accepted"

.field public static final EVENT_USER_UTT:Ljava/lang/String; = "user_utt"

.field private static smInstance:Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->smInstance:Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static forceFullLmttUpdate(Ljava/lang/String;)V
    .locals 3
    .param p0, "type"    # Ljava/lang/String;

    .prologue
    .line 467
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 468
    .local v0, "extras":Landroid/os/Bundle;
    const-string/jumbo v1, "com.vlingo.lmtt.extra.CLEAR_LMTT"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 469
    if-eqz p0, :cond_0

    .line 470
    const-string/jumbo v1, "com.vlingo.lmtt.extra.LMTT_TYPE"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    :cond_0
    const-string/jumbo v1, "com.vlingo.lmtt.action.SEND_UPDATE"

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;->startLmttService(Ljava/lang/String;ZLandroid/os/Bundle;)V

    .line 473
    return-void
.end method

.method public static getInstance()Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->smInstance:Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;

    invoke-direct {v0}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->smInstance:Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;

    .line 41
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->smInstance:Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;

    return-object v0
.end method

.method private static isTosAccepted()Z
    .locals 2

    .prologue
    .line 433
    const-string/jumbo v0, "tos_accepted"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private performRoleCheck()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 416
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->isMaster()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 428
    :cond_0
    :goto_0
    return v0

    .line 420
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->otherMasterExists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 427
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->becomeMaster()V

    .line 428
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static reinitBgLmttService()V
    .locals 3

    .prologue
    .line 447
    const-string/jumbo v0, "com.vlingo.lmtt.action.REINITIALIZE"

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;->startLmttService(Ljava/lang/String;ZLandroid/os/Bundle;)V

    .line 448
    return-void
.end method

.method private static resetInactivity()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 457
    const/4 v0, 0x1

    invoke-static {v1, v0, v1}, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;->startLmttService(Ljava/lang/String;ZLandroid/os/Bundle;)V

    .line 458
    return-void
.end method

.method private static sendLmttLanguageUpdate()V
    .locals 3

    .prologue
    .line 451
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 452
    .local v0, "extras":Landroid/os/Bundle;
    const-string/jumbo v1, "com.vlingo.lmtt.extra.SKIP_DELAY"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 453
    const-string/jumbo v1, "com.vlingo.lmtt.action.LANGUAGE_CHANGE"

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;->startLmttService(Ljava/lang/String;ZLandroid/os/Bundle;)V

    .line 454
    return-void
.end method

.method private static sendLmttUpdate(ZZ)V
    .locals 2
    .param p0, "resetInactivity"    # Z
    .param p1, "skipDelay"    # Z

    .prologue
    .line 461
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 462
    .local v0, "extras":Landroid/os/Bundle;
    const-string/jumbo v1, "com.vlingo.lmtt.extra.SKIP_DELAY"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 463
    const-string/jumbo v1, "com.vlingo.lmtt.action.SEND_UPDATE"

    invoke-static {v1, p0, v0}, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;->startLmttService(Ljava/lang/String;ZLandroid/os/Bundle;)V

    .line 464
    return-void
.end method

.method private static startBgLmttService()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 442
    const/4 v0, 0x0

    invoke-static {v1, v0, v1}, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;->startLmttService(Ljava/lang/String;ZLandroid/os/Bundle;)V

    .line 443
    return-void
.end method

.method private static useFgLmttService()Z
    .locals 1

    .prologue
    .line 437
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public onAppLaunched()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 144
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->isTosAccepted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 148
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;->sendHeartBeat(Z)V

    goto :goto_0

    .line 151
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->isMaster()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 152
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->useFgLmttService()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    invoke-static {v1, v1}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->sendLmttUpdate(ZZ)V

    goto :goto_0

    .line 156
    :cond_3
    const-string/jumbo v0, "app_launched"

    invoke-static {v0}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->sendEventToMaster(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public onBootCompleted()V
    .locals 1

    .prologue
    .line 198
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->isTosAccepted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 201
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-nez v0, :cond_0

    .line 204
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->isMaster()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->useFgLmttService()Z

    move-result v0

    if-nez v0, :cond_0

    .line 209
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->startBgLmttService()V

    goto :goto_0
.end method

.method public onCoreInitialization()V
    .locals 3

    .prologue
    .line 172
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->isMaster()Z

    move-result v2

    if-nez v2, :cond_0

    .line 176
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->getMasterLanguage()Ljava/lang/String;

    move-result-object v1

    .line 177
    .local v1, "masterLanguage":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 180
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 181
    .local v0, "context":Landroid/content/Context;
    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->setLanguageApplication(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public onLanguageChanged()V
    .locals 2

    .prologue
    .line 84
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->isTosAccepted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 87
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v1

    if-nez v1, :cond_0

    .line 90
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isMdsoApplication()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 93
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v0

    .line 94
    .local v0, "language":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->setLanguageToAllNodes(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSettingChanged(Ljava/lang/String;)V
    .locals 12
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const-wide/16 v10, 0x0

    const/4 v9, -0x1

    .line 265
    if-nez p1, :cond_1

    .line 351
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v7

    if-nez v7, :cond_0

    .line 271
    const-string/jumbo v7, "language"

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 276
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->isMaster()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 277
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->sendLmttLanguageUpdate()V

    goto :goto_0

    .line 279
    :cond_2
    const-string/jumbo v7, "lmtt"

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    const-string/jumbo v7, "LMTT"

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 281
    :cond_3
    const-string/jumbo v7, "lmtt.enable."

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 284
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->isMaster()Z

    move-result v7

    if-nez v7, :cond_4

    .line 285
    const/4 v7, 0x1

    invoke-static {p1, v7}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 286
    .local v0, "boolVal":Z
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-static {p1, v7}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->setMasterSetting(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 290
    .end local v0    # "boolVal":Z
    :cond_4
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->useFgLmttService()Z

    move-result v7

    if-nez v7, :cond_0

    .line 291
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->reinitBgLmttService()V

    goto :goto_0

    .line 293
    :cond_5
    const-string/jumbo v7, "lmtt.update.version"

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    const-string/jumbo v7, ".previous"

    invoke-virtual {p1, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_9

    .line 297
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->isMaster()Z

    move-result v7

    if-nez v7, :cond_6

    .line 298
    invoke-static {p1, v9}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 299
    .local v1, "intVal":I
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {p1, v7}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->setMasterSetting(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 303
    .end local v1    # "intVal":I
    :cond_6
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ".previous"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 304
    .local v4, "previousKey":Ljava/lang/String;
    invoke-static {v4, v9}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 305
    .local v5, "previousValue":I
    invoke-static {p1, v9}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 310
    .local v3, "newValue":I
    const-string/jumbo v7, "appstate.config_update_count"

    invoke-static {v7, v10, v11}, Lcom/vlingo/core/internal/settings/Settings;->getLong(Ljava/lang/String;J)J

    move-result-wide v7

    cmp-long v7, v7, v10

    if-nez v7, :cond_7

    .line 313
    invoke-static {v4, v3}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 314
    :cond_7
    if-le v3, v5, :cond_0

    .line 317
    invoke-static {v4, v3}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 318
    const/4 v2, 0x0

    .line 319
    .local v2, "lmttSubType":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    const-string/jumbo v8, "lmtt.update.version"

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-le v7, v8, :cond_8

    .line 320
    const-string/jumbo v7, "lmtt.update.version"

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 324
    :cond_8
    invoke-static {v2}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->forceFullLmttUpdate(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 326
    .end local v2    # "lmttSubType":Ljava/lang/String;
    .end local v3    # "newValue":I
    .end local v4    # "previousKey":Ljava/lang/String;
    .end local v5    # "previousValue":I
    :cond_9
    const-string/jumbo v7, "LMTT_HOST_NAME"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 329
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->isMaster()Z

    move-result v7

    if-nez v7, :cond_0

    .line 330
    const/4 v7, 0x0

    invoke-static {p1, v7}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 331
    .local v6, "stringVal":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 332
    invoke-static {p1, v6}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->setMasterSetting(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 335
    .end local v6    # "stringVal":Ljava/lang/String;
    :cond_a
    const-string/jumbo v7, "lmtt.chunk_delay_ms"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_b

    const-string/jumbo v7, "lmtt.chunk_retries"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_b

    const-string/jumbo v7, "lmtt.chunk_retry_delay_ms"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_b

    const-string/jumbo v7, "lmtt.chunk_size"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_b

    const-string/jumbo v7, "lmtt.client_shield_duration_mins"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_b

    const-string/jumbo v7, "lmtt.no_activity_shutdown_period_mins"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_b

    const-string/jumbo v7, "lmtt.task_retries"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 344
    :cond_b
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->isMaster()Z

    move-result v7

    if-nez v7, :cond_0

    .line 345
    invoke-static {p1, v9}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 346
    .restart local v1    # "intVal":I
    if-eq v1, v9, :cond_0

    .line 347
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {p1, v7}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->setMasterSetting(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public onSlaveEvent(Ljava/lang/String;)V
    .locals 3
    .param p1, "eventName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 381
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->isTosAccepted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 404
    :cond_0
    :goto_0
    return-void

    .line 384
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-nez v0, :cond_0

    .line 387
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->isMaster()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    const-string/jumbo v0, "app_launched"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 392
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->useFgLmttService()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 393
    invoke-static {v1, v1}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->sendLmttUpdate(ZZ)V

    goto :goto_0

    .line 395
    :cond_2
    const-string/jumbo v0, "tos_accepted"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 396
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->useFgLmttService()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 397
    invoke-static {v2, v2}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->sendLmttUpdate(ZZ)V

    goto :goto_0

    .line 399
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->resetInactivity()V

    goto :goto_0

    .line 401
    :cond_4
    const-string/jumbo v0, "user_utt"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 402
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->resetInactivity()V

    goto :goto_0
.end method

.method public onTosAccepted()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 57
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->isTosAccepted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 69
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->performRoleCheck()Z

    .line 64
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->isMaster()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 65
    invoke-static {v1, v1}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->sendLmttUpdate(ZZ)V

    goto :goto_0

    .line 67
    :cond_2
    const-string/jumbo v0, "tos_accepted"

    invoke-static {v0}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->sendEventToMaster(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public onUiShown()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 110
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->isTosAccepted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isMdsoApplication()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->performRoleCheck()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    invoke-static {v1, v1}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->sendLmttUpdate(ZZ)V

    goto :goto_0
.end method

.method public onUserDataCleared()V
    .locals 0

    .prologue
    .line 363
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;->stopLmttService()V

    .line 364
    return-void
.end method

.method public onUserUtt()V
    .locals 1

    .prologue
    .line 226
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->isTosAccepted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 238
    :goto_0
    return-void

    .line 229
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 230
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;->sendHeartBeat(Z)V

    goto :goto_0

    .line 233
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->isMaster()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 234
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->resetInactivity()V

    goto :goto_0

    .line 236
    :cond_2
    const-string/jumbo v0, "user_utt"

    invoke-static {v0}, Lcom/vlingo/core/internal/lmtt/event/InternalMdsoUtils;->sendEventToMaster(Ljava/lang/String;)Z

    goto :goto_0
.end method
