.class public Lcom/vlingo/core/internal/dialogmanager/events/TaskResolvedEvent;
.super Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;
.source "TaskResolvedEvent.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent",
        "<",
        "Lcom/vlingo/core/internal/schedule/ScheduleTask;",
        ">;"
    }
.end annotation


# instance fields
.field private final NAME:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;-><init>()V

    .line 21
    const-string/jumbo v0, "task-resolved"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/TaskResolvedEvent;->NAME:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public constructor <init>(Ljava/util/List;II)V
    .locals 1
    .param p2, "offset"    # I
    .param p3, "totalCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleTask;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 24
    .local p1, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;-><init>(Ljava/util/List;II)V

    .line 21
    const-string/jumbo v0, "task-resolved"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/TaskResolvedEvent;->NAME:Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    const-string/jumbo v0, "task-resolved"

    return-object v0
.end method

.method public getVLDialogEvent()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    .locals 8

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/events/TaskResolvedEvent;->getItems()Ljava/util/List;

    move-result-object v6

    if-nez v6, :cond_0

    const/4 v6, 0x0

    .line 59
    :goto_0
    return-object v6

    .line 40
    :cond_0
    new-instance v0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    const-string/jumbo v6, "task-resolved"

    invoke-direct {v0, v6}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;-><init>(Ljava/lang/String;)V

    .line 41
    .local v0, "builder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/events/TaskResolvedEvent;->writeTotalCount(Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;)V

    .line 42
    new-instance v5, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;

    const-string/jumbo v6, "tasks"

    invoke-direct {v5, v6}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;-><init>(Ljava/lang/String;)V

    .line 43
    .local v5, "tasksBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/events/TaskResolvedEvent;->getOffset()I

    move-result v2

    .line 45
    .local v2, "id":I
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/events/TaskResolvedEvent;->getItems()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .line 46
    .local v3, "task":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    new-instance v4, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;

    const-string/jumbo v6, "task"

    invoke-direct {v4, v6}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;-><init>(Ljava/lang/String;)V

    .line 48
    .local v4, "taskBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    const-string/jumbo v6, "id"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 49
    add-int/lit8 v2, v2, 0x1

    .line 50
    invoke-virtual {v3}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 51
    const-string/jumbo v6, "title"

    invoke-virtual {v3}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 53
    :cond_1
    invoke-virtual {v3}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getBeginDate()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 54
    const-string/jumbo v6, "date"

    invoke-virtual {v3}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getBeginDate()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 56
    :cond_2
    invoke-virtual {v4}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->eventFieldGroup(Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    goto :goto_1

    .line 58
    .end local v3    # "task":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    .end local v4    # "taskBuilder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;
    :cond_3
    invoke-virtual {v5}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->eventFieldGroup(Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 59
    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;

    move-result-object v6

    goto :goto_0
.end method
