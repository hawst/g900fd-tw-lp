.class final enum Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;
.super Ljava/lang/Enum;
.source "VDSMSBaseController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

.field public static final enum NEED_CONFIRMATION:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

.field public static final enum NEED_CONTACT:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

.field public static final enum NEED_CONTACT_REFINEMENT:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

.field public static final enum NEED_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

.field public static final enum NEED_REDIAL_CONFIRMATION:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

.field public static final enum NEED_TYPE:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

.field public static final enum POST_CONFIRMATION:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 59
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    const-string/jumbo v1, "NEED_CONTACT"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_CONTACT:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    const-string/jumbo v1, "NEED_CONTACT_REFINEMENT"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_CONTACT_REFINEMENT:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    const-string/jumbo v1, "NEED_TYPE"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_TYPE:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    const-string/jumbo v1, "NEED_MESSAGE"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    const-string/jumbo v1, "NEED_CONFIRMATION"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_CONFIRMATION:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    const-string/jumbo v1, "POST_CONFIRMATION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->POST_CONFIRMATION:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    const-string/jumbo v1, "NEED_REDIAL_CONFIRMATION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_REDIAL_CONFIRMATION:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    .line 58
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_CONTACT:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_CONTACT_REFINEMENT:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_TYPE:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_CONFIRMATION:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->POST_CONFIRMATION:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->NEED_REDIAL_CONFIRMATION:Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 58
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/dialogmanager/controllers/VDSMSBaseController$State;

    return-object v0
.end method
