.class public interface abstract Lcom/vlingo/core/internal/dialogmanager/features/MemoFeatures$Action;
.super Ljava/lang/Object;
.source "MemoFeatures.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/features/MemoFeatures;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Action"
.end annotation


# static fields
.field public static final FIND_MOST_RECENT:Ljava/lang/String; = "memo.findmostrecent"

.field public static final SAVE:Ljava/lang/String; = "memo.save"
