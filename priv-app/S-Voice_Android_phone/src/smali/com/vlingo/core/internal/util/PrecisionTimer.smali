.class public Lcom/vlingo/core/internal/util/PrecisionTimer;
.super Ljava/lang/Object;
.source "PrecisionTimer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/util/PrecisionTimer$1;,
        Lcom/vlingo/core/internal/util/PrecisionTimer$Remainder;,
        Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;,
        Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;
    }
.end annotation


# instance fields
.field private precision:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

.field private start:J

.field private stop:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210
    sget-object v0, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->NANOSECONDS:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    iput-object v0, p0, Lcom/vlingo/core/internal/util/PrecisionTimer;->precision:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    return-void
.end method

.method private computeElapsedTime()J
    .locals 4

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/PrecisionTimer;->running()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/PrecisionTimer;->now()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer;->start:J

    sub-long/2addr v0, v2

    .line 204
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/vlingo/core/internal/util/PrecisionTimer;->stop:J

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer;->start:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method public static newTimer()Lcom/vlingo/core/internal/util/PrecisionTimer;
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/vlingo/core/internal/util/PrecisionTimer;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/PrecisionTimer;-><init>()V

    return-object v0
.end method

.method private now()J
    .locals 2

    .prologue
    .line 196
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public static startTimer()Lcom/vlingo/core/internal/util/PrecisionTimer;
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/vlingo/core/internal/util/PrecisionTimer;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/PrecisionTimer;-><init>()V

    .line 23
    .local v0, "timer":Lcom/vlingo/core/internal/util/PrecisionTimer;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/PrecisionTimer;->start()Lcom/vlingo/core/internal/util/PrecisionTimer;

    .line 24
    return-object v0
.end method

.method private started()Z
    .locals 4

    .prologue
    .line 179
    iget-wide v0, p0, Lcom/vlingo/core/internal/util/PrecisionTimer;->start:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private state()Ljava/lang/String;
    .locals 2

    .prologue
    .line 184
    const-string/jumbo v0, "not started"

    .line 186
    .local v0, "s":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/PrecisionTimer;->running()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 187
    const-string/jumbo v0, "running"

    .line 191
    :cond_0
    :goto_0
    return-object v0

    .line 188
    :cond_1
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/PrecisionTimer;->stopped()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 189
    const-string/jumbo v0, "stopped"

    goto :goto_0
.end method

.method private stopped()Z
    .locals 4

    .prologue
    .line 180
    iget-wide v0, p0, Lcom/vlingo/core/internal/util/PrecisionTimer;->stop:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public elapsed()J
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/PrecisionTimer;->computeElapsedTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public elapsedInterval()Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;
    .locals 4

    .prologue
    .line 41
    new-instance v0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;

    invoke-direct {p0}, Lcom/vlingo/core/internal/util/PrecisionTimer;->computeElapsedTime()J

    move-result-wide v1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;-><init>(JLcom/vlingo/core/internal/util/PrecisionTimer$1;)V

    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/PrecisionTimer;->precision()Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->precision(Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;)Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;

    move-result-object v0

    return-object v0
.end method

.method public precision()Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/core/internal/util/PrecisionTimer;->precision:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    return-object v0
.end method

.method public precision(Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;)Lcom/vlingo/core/internal/util/PrecisionTimer;
    .locals 0
    .param p1, "precisionParam"    # Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/vlingo/core/internal/util/PrecisionTimer;->precision:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    .line 59
    return-object p0
.end method

.method public running()Z
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/PrecisionTimer;->started()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/vlingo/core/internal/util/PrecisionTimer;->stopped()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public start()Lcom/vlingo/core/internal/util/PrecisionTimer;
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/PrecisionTimer;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/vlingo/core/internal/util/PrecisionTimer;->start:J

    .line 31
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/core/internal/util/PrecisionTimer;->stop:J

    .line 32
    return-object p0
.end method

.method public stop()Lcom/vlingo/core/internal/util/PrecisionTimer;
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/PrecisionTimer;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/vlingo/core/internal/util/PrecisionTimer;->stop:J

    .line 37
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 68
    .local v0, "s":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    const-string/jumbo v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/PrecisionTimer;->elapsedInterval()Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 71
    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/PrecisionTimer;->state()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
