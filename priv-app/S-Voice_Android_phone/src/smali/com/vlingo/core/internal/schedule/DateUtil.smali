.class public final Lcom/vlingo/core/internal/schedule/DateUtil;
.super Ljava/lang/Object;
.source "DateUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/schedule/DateUtil$DateType;
    }
.end annotation


# static fields
.field public static CANNONICAL_FULL_DATE_FORMAT:Ljava/lang/String;

.field public static CANONICAL_DATE_AND_TIME_FORMAT:Ljava/lang/String;

.field public static CANONICAL_DATE_FORMAT:Ljava/lang/String;

.field public static CANONICAL_RELAIVE_TIME_FORMAT:Ljava/lang/String;

.field public static CANONICAL_TIME_FORMAT:Ljava/lang/String;

.field public static DAY_AND_MONTH_BAD_FORMAT:Ljava/lang/String;

.field public static DAY_AND_MONTH_GOOD_FORMAT:Ljava/lang/String;

.field public static ONE_DAY:J

.field public static ONE_HOUR:J

.field public static ONE_MINUTE:J

.field public static ONE_SECOND:J

.field private static dateMatcher:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Lcom/vlingo/core/internal/schedule/DateUtil$DateType;",
            "Ljava/lang/Integer;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x3c

    .line 30
    const-wide/16 v0, 0x3e8

    sput-wide v0, Lcom/vlingo/core/internal/schedule/DateUtil;->ONE_SECOND:J

    .line 31
    sget-wide v0, Lcom/vlingo/core/internal/schedule/DateUtil;->ONE_SECOND:J

    mul-long/2addr v0, v2

    sput-wide v0, Lcom/vlingo/core/internal/schedule/DateUtil;->ONE_MINUTE:J

    .line 32
    sget-wide v0, Lcom/vlingo/core/internal/schedule/DateUtil;->ONE_MINUTE:J

    mul-long/2addr v0, v2

    sput-wide v0, Lcom/vlingo/core/internal/schedule/DateUtil;->ONE_HOUR:J

    .line 33
    const-wide/16 v0, 0x18

    sget-wide v2, Lcom/vlingo/core/internal/schedule/DateUtil;->ONE_HOUR:J

    mul-long/2addr v0, v2

    sput-wide v0, Lcom/vlingo/core/internal/schedule/DateUtil;->ONE_DAY:J

    .line 36
    const-string/jumbo v0, "yyyy-MM-dd"

    sput-object v0, Lcom/vlingo/core/internal/schedule/DateUtil;->CANONICAL_DATE_FORMAT:Ljava/lang/String;

    .line 37
    const-string/jumbo v0, "MMMM dd, yyyy"

    sput-object v0, Lcom/vlingo/core/internal/schedule/DateUtil;->CANNONICAL_FULL_DATE_FORMAT:Ljava/lang/String;

    .line 38
    const-string/jumbo v0, "HH:mm"

    sput-object v0, Lcom/vlingo/core/internal/schedule/DateUtil;->CANONICAL_TIME_FORMAT:Ljava/lang/String;

    .line 39
    const-string/jumbo v0, "yyyy-MM-dd HH:mm"

    sput-object v0, Lcom/vlingo/core/internal/schedule/DateUtil;->CANONICAL_DATE_AND_TIME_FORMAT:Ljava/lang/String;

    .line 40
    const-string/jumbo v0, "+HH:mm:ss"

    sput-object v0, Lcom/vlingo/core/internal/schedule/DateUtil;->CANONICAL_RELAIVE_TIME_FORMAT:Ljava/lang/String;

    .line 41
    const-string/jumbo v0, "--MM-dd"

    sput-object v0, Lcom/vlingo/core/internal/schedule/DateUtil;->DAY_AND_MONTH_BAD_FORMAT:Ljava/lang/String;

    .line 42
    const-string/jumbo v0, "dd MMMM"

    sput-object v0, Lcom/vlingo/core/internal/schedule/DateUtil;->DAY_AND_MONTH_GOOD_FORMAT:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static adjustTimeForCriteria(Landroid/text/format/Time;Lcom/vlingo/core/internal/schedule/DateUtil$DateType;I)Landroid/text/format/Time;
    .locals 4
    .param p0, "t"    # Landroid/text/format/Time;
    .param p1, "dt"    # Lcom/vlingo/core/internal/schedule/DateUtil$DateType;
    .param p2, "i"    # I

    .prologue
    const/4 v2, 0x1

    .line 325
    sget-object v0, Lcom/vlingo/core/internal/schedule/DateUtil$DateType;->WEEK_DAY:Lcom/vlingo/core/internal/schedule/DateUtil$DateType;

    if-ne p1, v0, :cond_1

    .line 328
    invoke-virtual {p0, v2}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v0

    iget v1, p0, Landroid/text/format/Time;->weekDay:I

    sub-int v1, p2, v1

    add-int/lit8 v1, v1, 0x7

    rem-int/lit8 v1, v1, 0x7

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 357
    :cond_0
    :goto_0
    return-object p0

    .line 329
    :cond_1
    sget-object v0, Lcom/vlingo/core/internal/schedule/DateUtil$DateType;->MONTH_DAY:Lcom/vlingo/core/internal/schedule/DateUtil$DateType;

    if-ne p1, v0, :cond_4

    .line 332
    iget v0, p0, Landroid/text/format/Time;->monthDay:I

    if-ge p2, v0, :cond_3

    .line 333
    const/16 v0, 0xc

    iget v1, p0, Landroid/text/format/Time;->month:I

    if-ne v0, v1, :cond_2

    .line 334
    iget v0, p0, Landroid/text/format/Time;->year:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, p2, v2, v0}, Landroid/text/format/Time;->set(III)V

    goto :goto_0

    .line 336
    :cond_2
    iget v0, p0, Landroid/text/format/Time;->month:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Landroid/text/format/Time;->year:I

    invoke-virtual {p0, p2, v0, v1}, Landroid/text/format/Time;->set(III)V

    goto :goto_0

    .line 339
    :cond_3
    iget v0, p0, Landroid/text/format/Time;->month:I

    iget v1, p0, Landroid/text/format/Time;->year:I

    invoke-virtual {p0, p2, v0, v1}, Landroid/text/format/Time;->set(III)V

    goto :goto_0

    .line 342
    :cond_4
    sget-object v0, Lcom/vlingo/core/internal/schedule/DateUtil$DateType;->MONTH:Lcom/vlingo/core/internal/schedule/DateUtil$DateType;

    if-ne p1, v0, :cond_0

    .line 343
    iget v0, p0, Landroid/text/format/Time;->month:I

    if-eq p2, v0, :cond_0

    .line 350
    iget v0, p0, Landroid/text/format/Time;->month:I

    if-ge p2, v0, :cond_5

    .line 352
    iget v0, p0, Landroid/text/format/Time;->year:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v2, p2, v0}, Landroid/text/format/Time;->set(III)V

    .line 355
    :cond_5
    iget v0, p0, Landroid/text/format/Time;->year:I

    invoke-virtual {p0, v2, p2, v0}, Landroid/text/format/Time;->set(III)V

    goto :goto_0
.end method

.method public static adjustTimeForDateString(Ljava/lang/String;Landroid/text/format/Time;)J
    .locals 4
    .param p0, "date"    # Ljava/lang/String;
    .param p1, "time"    # Landroid/text/format/Time;

    .prologue
    .line 207
    :try_start_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    sget-object v2, Lcom/vlingo/core/internal/schedule/DateUtil;->CANONICAL_DATE_FORMAT:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 208
    .local v1, "format":Ljava/text/SimpleDateFormat;
    invoke-virtual {v1, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 209
    .local v0, "d":Ljava/util/Date;
    iget v2, p1, Landroid/text/format/Time;->hour:I

    invoke-virtual {v0, v2}, Ljava/util/Date;->setHours(I)V

    .line 210
    iget v2, p1, Landroid/text/format/Time;->minute:I

    invoke-virtual {v0, v2}, Ljava/util/Date;->setMinutes(I)V

    .line 211
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 216
    .end local v0    # "d":Ljava/util/Date;
    .end local v1    # "format":Ljava/text/SimpleDateFormat;
    :goto_0
    return-wide v2

    .line 212
    :catch_0
    move-exception v2

    .line 216
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    goto :goto_0
.end method

.method public static adjustTimeForTimeString(Ljava/lang/String;Landroid/text/format/Time;)J
    .locals 2
    .param p0, "timeString"    # Ljava/lang/String;
    .param p1, "t"    # Landroid/text/format/Time;

    .prologue
    .line 221
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/vlingo/core/internal/schedule/DateUtil;->getTimeFromTimeString(Ljava/lang/String;Landroid/text/format/Time;Z)J

    move-result-wide v0

    return-wide v0
.end method

.method private static buildDayObject(Ljava/util/Date;)Ljava/util/Calendar;
    .locals 3
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    const/4 v2, 0x0

    .line 544
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 545
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 546
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 547
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 548
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 549
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 550
    return-object v0
.end method

.method public static diffInDays(Ljava/lang/String;)I
    .locals 7
    .param p0, "date"    # Ljava/lang/String;

    .prologue
    .line 97
    const/4 v1, 0x0

    .line 99
    .local v1, "day":Ljava/util/Calendar;
    :try_start_0
    invoke-static {p0}, Lcom/vlingo/core/internal/schedule/DateUtil;->getDateFromCanonicalString(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/core/internal/schedule/DateUtil;->buildDayObject(Ljava/util/Date;)Ljava/util/Calendar;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 105
    :goto_0
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-static {v3}, Lcom/vlingo/core/internal/schedule/DateUtil;->buildDayObject(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v0

    .line 106
    .local v0, "currDate":Ljava/util/Calendar;
    if-nez v1, :cond_0

    .line 107
    const/4 v3, 0x0

    .line 109
    :goto_1
    return v3

    .line 100
    .end local v0    # "currDate":Ljava/util/Calendar;
    :catch_0
    move-exception v2

    .line 103
    .local v2, "e":Ljava/text/ParseException;
    invoke-virtual {v2}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0

    .line 109
    .end local v2    # "e":Ljava/text/ParseException;
    .restart local v0    # "currDate":Ljava/util/Calendar;
    :cond_0
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    sub-long/2addr v3, v5

    sget-wide v5, Lcom/vlingo/core/internal/schedule/DateUtil;->ONE_DAY:J

    div-long/2addr v3, v5

    long-to-int v3, v3

    goto :goto_1
.end method

.method public static endOfGivenDay(J)J
    .locals 3
    .param p0, "startMillis"    # J

    .prologue
    const/16 v2, 0x3b

    .line 167
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 168
    .local v0, "end":Landroid/text/format/Time;
    invoke-virtual {v0, p0, p1}, Landroid/text/format/Time;->set(J)V

    .line 169
    const/16 v1, 0x17

    iput v1, v0, Landroid/text/format/Time;->hour:I

    .line 170
    iput v2, v0, Landroid/text/format/Time;->minute:I

    .line 171
    iput v2, v0, Landroid/text/format/Time;->second:I

    .line 172
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v1

    return-wide v1
.end method

.method public static formatDisplayTime(Ljava/util/Date;)Ljava/lang/String;
    .locals 3
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    .line 554
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v1

    .line 555
    .local v1, "timeFormat24State":Z
    const/4 v0, 0x0

    .line 556
    .local v0, "formattedCurrentTime":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 557
    const-string/jumbo v2, "k:mm"

    invoke-static {v2, p0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 561
    :goto_0
    return-object v0

    .line 559
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/vlingo/core/internal/schedule/DateUtil;->get12HourTime(Ljava/util/Date;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static formatMemoDate(JLjava/lang/String;Ljava/util/Locale;)Ljava/lang/String;
    .locals 9
    .param p0, "date"    # J
    .param p2, "phoneDateFormat"    # Ljava/lang/String;
    .param p3, "currentLocale"    # Ljava/util/Locale;

    .prologue
    const/4 v8, 0x7

    const/4 v4, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 402
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 403
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 404
    const-string/jumbo v1, ""

    .line 406
    .local v1, "dateString":Ljava/lang/String;
    invoke-virtual {p3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 407
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0, v8, v5, p3}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 409
    :cond_0
    if-eqz p2, :cond_2

    sget-object v2, Lcom/vlingo/core/internal/schedule/DateUtil;->CANONICAL_DATE_FORMAT:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 410
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%tY.%tm.%td"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v6

    aput-object v0, v4, v5

    aput-object v0, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 416
    :goto_0
    invoke-virtual {p3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 417
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0, v8, v5, p3}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 419
    :cond_1
    return-object v1

    .line 411
    :cond_2
    if-eqz p2, :cond_3

    const-string/jumbo v2, "dd-MM-yyyy"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 412
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%td.%tm.%tY"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v6

    aput-object v0, v4, v5

    aput-object v0, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 414
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%tm.%td.%tY"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v6

    aput-object v0, v4, v5

    aput-object v0, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static formatMemoTime(JZLjava/util/Locale;)Ljava/lang/String;
    .locals 7
    .param p0, "date"    # J
    .param p2, "timeFormat24State"    # Z
    .param p3, "currentLocale"    # Ljava/util/Locale;

    .prologue
    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 423
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 424
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 425
    const-string/jumbo v1, ""

    .line 426
    .local v1, "timeString":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 427
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%tR"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 432
    :goto_0
    return-object v1

    .line 429
    :cond_0
    const-string/jumbo v2, "%tI:%tM"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 430
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x9

    invoke-virtual {v0, v3, v6, p3}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static formatScheduleDate(JLjava/lang/String;Ljava/util/Locale;)Ljava/lang/String;
    .locals 8
    .param p0, "date"    # J
    .param p2, "phoneDateFormat"    # Ljava/lang/String;
    .param p3, "currentLocale"    # Ljava/util/Locale;

    .prologue
    const/4 v4, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 462
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 463
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 464
    const-string/jumbo v1, ""

    .line 465
    .local v1, "dateString":Ljava/lang/String;
    sget-object v2, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-virtual {p3, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 466
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%tY-%tm-%td"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    aput-object v0, v4, v6

    aput-object v0, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 476
    :goto_0
    return-object v1

    .line 468
    :cond_0
    if-eqz p2, :cond_1

    sget-object v2, Lcom/vlingo/core/internal/schedule/DateUtil;->CANONICAL_DATE_FORMAT:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 469
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%tY-%tm-%td"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    aput-object v0, v4, v6

    aput-object v0, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 470
    :cond_1
    if-eqz p2, :cond_2

    const-string/jumbo v2, "dd-MM-yyyy"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 471
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%td-%tm-%tY"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    aput-object v0, v4, v6

    aput-object v0, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 473
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%tm-%td-%tY"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    aput-object v0, v4, v6

    aput-object v0, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static formatScheduleTime(JJZLjava/util/Locale;)Ljava/lang/String;
    .locals 5
    .param p0, "begin"    # J
    .param p2, "end"    # J
    .param p4, "timeFormat24State"    # Z
    .param p5, "currentLocale"    # Ljava/util/Locale;

    .prologue
    const-wide/16 v3, 0x0

    .line 480
    const-string/jumbo v0, ""

    .line 481
    .local v0, "timeString":Ljava/lang/String;
    cmp-long v1, p0, v3

    if-eqz v1, :cond_0

    .line 482
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0, p1, p4, p5}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatWidgetTime(JZLjava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 484
    :cond_0
    cmp-long v1, p2, v3

    if-eqz v1, :cond_1

    .line 485
    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-virtual {p5, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 486
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2, p3, p4, p5}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatWidgetTime(JZLjava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 491
    :cond_1
    :goto_0
    return-object v0

    .line 488
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2, p3, p4, p5}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatWidgetTime(JZLjava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static formatWidgetDate(Ljava/util/Date;Ljava/util/Locale;)Ljava/lang/String;
    .locals 8
    .param p0, "dateValue"    # Ljava/util/Date;
    .param p1, "currentLocale"    # Ljava/util/Locale;

    .prologue
    .line 436
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_cradle_date:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v6, v7}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 437
    .local v0, "cradleDate":Ljava/lang/String;
    const-string/jumbo v2, ""

    .line 438
    .local v2, "dateString":Ljava/lang/String;
    sget-object v6, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-virtual {p1, v6}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    sget-object v6, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {p1, v6}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    sget-object v6, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-virtual {p1, v6}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 440
    :cond_0
    sget-object v6, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-virtual {p1, v6}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 441
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string/jumbo v6, "MMMMd"

    invoke-direct {v3, v6, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 445
    .local v3, "sdfDate":Ljava/text/SimpleDateFormat;
    :goto_0
    invoke-virtual {v3, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 447
    .local v1, "date":Ljava/lang/String;
    sget-object v6, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {p1, v6}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 448
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string/jumbo v6, " (EEEE)"

    invoke-direct {v4, v6, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 452
    .local v4, "sdfWeekDay":Ljava/text/SimpleDateFormat;
    :goto_1
    invoke-virtual {v4, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 453
    .local v5, "weekDay":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 458
    .end local v1    # "date":Ljava/lang/String;
    .end local v4    # "sdfWeekDay":Ljava/text/SimpleDateFormat;
    .end local v5    # "weekDay":Ljava/lang/String;
    :goto_2
    return-object v2

    .line 443
    .end local v3    # "sdfDate":Ljava/text/SimpleDateFormat;
    :cond_1
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string/jumbo v6, "MMMM d"

    invoke-direct {v3, v6, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .restart local v3    # "sdfDate":Ljava/text/SimpleDateFormat;
    goto :goto_0

    .line 450
    .restart local v1    # "date":Ljava/lang/String;
    :cond_2
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string/jumbo v6, ", EEEE"

    invoke-direct {v4, v6, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .restart local v4    # "sdfWeekDay":Ljava/text/SimpleDateFormat;
    goto :goto_1

    .line 455
    .end local v1    # "date":Ljava/lang/String;
    .end local v3    # "sdfDate":Ljava/text/SimpleDateFormat;
    .end local v4    # "sdfWeekDay":Ljava/text/SimpleDateFormat;
    :cond_3
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string/jumbo v6, "EEE, d MMM"

    invoke-direct {v3, v6, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 456
    .restart local v3    # "sdfDate":Ljava/text/SimpleDateFormat;
    invoke-virtual {v3, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method

.method public static formatWidgetTime(JZLjava/util/Locale;)Ljava/lang/String;
    .locals 10
    .param p0, "time"    # J
    .param p2, "timeFormat24State"    # Z
    .param p3, "currentLocale"    # Ljava/util/Locale;

    .prologue
    const/4 v7, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 495
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 496
    .local v2, "cal":Ljava/util/Calendar;
    invoke-virtual {v2, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 497
    if-eqz p2, :cond_0

    .line 498
    const-string/jumbo v5, "%tR"

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 517
    :goto_0
    return-object v5

    .line 500
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_format_time_AM:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    .line 501
    .local v1, "amString":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_format_time_PM:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v3

    .line 502
    .local v3, "pmString":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v4

    .line 503
    .local v4, "space":Ljava/lang/String;
    const-string/jumbo v0, ""

    .line 504
    .local v0, "amPm":Ljava/lang/String;
    const/16 v5, 0x9

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 512
    :goto_1
    sget-object v5, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-virtual {p3, v5}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 513
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "%tl:%tM"

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v8

    aput-object v2, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 506
    :pswitch_0
    move-object v0, v1

    .line 507
    goto :goto_1

    .line 509
    :pswitch_1
    move-object v0, v3

    goto :goto_1

    .line 514
    :cond_1
    sget-object v5, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-virtual {p3, v5}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    sget-object v5, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {p3, v5}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 515
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "%tl:%tM"

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v8

    aput-object v2, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 517
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "%tl:%tM"

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v8

    aput-object v2, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 504
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static fromNow(J)J
    .locals 2
    .param p0, "adjustment"    # J

    .prologue
    .line 63
    invoke-static {}, Lcom/vlingo/core/internal/schedule/DateUtil;->now()J

    move-result-wide v0

    add-long/2addr v0, p0

    return-wide v0
.end method

.method public static get12HourTime(Ljava/util/Date;Ljava/util/Locale;)Ljava/lang/String;
    .locals 6
    .param p0, "now"    # Ljava/util/Date;
    .param p1, "currentLocale"    # Ljava/util/Locale;

    .prologue
    .line 523
    const/4 v2, 0x0

    .line 524
    .local v2, "formattedCurrentTime":Ljava/lang/String;
    sget-object v3, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {p1, v3}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-virtual {p1, v3}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 525
    :cond_0
    const/4 v3, 0x2

    new-array v0, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_format_time_AM:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v3

    const/4 v3, 0x1

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_format_time_PM:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v3

    .line 527
    .local v0, "ampmSymbols":[Ljava/lang/String;
    new-instance v1, Ljava/text/DateFormatSymbols;

    invoke-direct {v1}, Ljava/text/DateFormatSymbols;-><init>()V

    .line 528
    .local v1, "formatSymbols":Ljava/text/DateFormatSymbols;
    invoke-virtual {v1, v0}, Ljava/text/DateFormatSymbols;->setAmPmStrings([Ljava/lang/String;)V

    .line 529
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string/jumbo v4, "a h:mm"

    invoke-direct {v3, v4, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/text/DateFormatSymbols;)V

    invoke-virtual {v3, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 533
    .end local v0    # "ampmSymbols":[Ljava/lang/String;
    .end local v1    # "formatSymbols":Ljava/text/DateFormatSymbols;
    :goto_0
    return-object v2

    .line 531
    :cond_1
    const/4 v3, 0x3

    invoke-static {v3, p1}, Ljava/text/DateFormat;->getTimeInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static getDateFromCanonicalDateAndTimeString(Ljava/lang/String;)Ljava/util/Date;
    .locals 2
    .param p0, "canonicalDateAndTime"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 367
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/vlingo/core/internal/schedule/DateUtil;->CANONICAL_DATE_AND_TIME_FORMAT:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 368
    .local v0, "format":Ljava/text/SimpleDateFormat;
    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    return-object v1
.end method

.method public static getDateFromCanonicalString(Ljava/lang/String;)Ljava/util/Date;
    .locals 2
    .param p0, "canonicalDate"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 361
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/vlingo/core/internal/schedule/DateUtil;->CANONICAL_DATE_FORMAT:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 362
    .local v0, "format":Ljava/text/SimpleDateFormat;
    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    return-object v1
.end method

.method public static getDayAndMonthFromString(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;
    .locals 5
    .param p0, "data"    # Ljava/lang/String;
    .param p1, "locale"    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 390
    new-instance v2, Ljava/text/SimpleDateFormat;

    sget-object v4, Lcom/vlingo/core/internal/schedule/DateUtil;->DAY_AND_MONTH_BAD_FORMAT:Ljava/lang/String;

    invoke-direct {v2, v4, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 391
    .local v2, "format":Ljava/text/SimpleDateFormat;
    invoke-virtual {v2, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 392
    .local v0, "date":Ljava/util/Date;
    sget-object v4, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-virtual {p1, v4}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 393
    const/4 v4, 0x1

    invoke-static {v4, p1}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 394
    .local v1, "dateString":Ljava/lang/String;
    const-string/jumbo v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 397
    .end local v1    # "dateString":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 396
    :cond_0
    new-instance v3, Ljava/text/SimpleDateFormat;

    sget-object v4, Lcom/vlingo/core/internal/schedule/DateUtil;->DAY_AND_MONTH_GOOD_FORMAT:Ljava/lang/String;

    invoke-direct {v3, v4, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 397
    .local v3, "format1":Ljava/text/SimpleDateFormat;
    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public static getLocailizeDateStringFromCanonicalString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "canonicalDate"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 379
    const/4 v1, 0x1

    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    .line 380
    .local v0, "df":Ljava/text/DateFormat;
    invoke-static {p0}, Lcom/vlingo/core/internal/schedule/DateUtil;->getDateFromCanonicalString(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getLongDateStringFromPhonePreferencesString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "canonicalDate"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 384
    const/4 v2, 0x1

    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    .line 385
    .local v0, "appdf":Ljava/text/DateFormat;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 386
    .local v1, "phonedf":Ljava/text/DateFormat;
    invoke-virtual {v1, p0}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getMillisFromString(Ljava/lang/String;Z)J
    .locals 3
    .param p0, "data"    # Ljava/lang/String;
    .param p1, "isDate"    # Z

    .prologue
    .line 177
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 178
    .local v0, "t":Landroid/text/format/Time;
    invoke-static {}, Lcom/vlingo/core/internal/schedule/DateUtil;->now()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/text/format/Time;->set(J)V

    .line 179
    invoke-static {p0, p1, v0}, Lcom/vlingo/core/internal/schedule/DateUtil;->getMillisFromString(Ljava/lang/String;ZLandroid/text/format/Time;)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getMillisFromString(Ljava/lang/String;ZLandroid/text/format/Time;)J
    .locals 2
    .param p0, "data"    # Ljava/lang/String;
    .param p1, "isDate"    # Z
    .param p2, "t"    # Landroid/text/format/Time;

    .prologue
    .line 184
    if-eqz p1, :cond_0

    invoke-static {p0, p2}, Lcom/vlingo/core/internal/schedule/DateUtil;->adjustTimeForDateString(Ljava/lang/String;Landroid/text/format/Time;)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p0, p2}, Lcom/vlingo/core/internal/schedule/DateUtil;->adjustTimeForTimeString(Ljava/lang/String;Landroid/text/format/Time;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static getMillisFromTimeString(Ljava/lang/String;)J
    .locals 3
    .param p0, "time"    # Ljava/lang/String;

    .prologue
    .line 189
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 190
    .local v0, "t":Landroid/text/format/Time;
    invoke-static {}, Lcom/vlingo/core/internal/schedule/DateUtil;->now()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/text/format/Time;->set(J)V

    .line 191
    invoke-static {p0, v0}, Lcom/vlingo/core/internal/schedule/DateUtil;->adjustTimeForTimeString(Ljava/lang/String;Landroid/text/format/Time;)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getTTSPlayingFormat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "dateString"    # Ljava/lang/String;
    .param p1, "currentISOLanguage"    # Ljava/lang/String;

    .prologue
    .line 571
    move-object v0, p0

    .line 573
    .local v0, "changedFormat":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v4

    .line 574
    .local v4, "phonedf":Ljava/text/DateFormat;
    invoke-virtual {v4, p0}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 575
    .local v1, "date":Ljava/util/Date;
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v5, "dd/MM/yyyy"

    invoke-static {p1}, Lcom/vlingo/core/internal/settings/Settings;->getLocaleForIsoLanguage(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v6

    invoke-direct {v2, v5, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 576
    .local v2, "dateFormat":Ljava/text/DateFormat;
    const-string/jumbo v5, "ko-KR"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string/jumbo v5, "en-US"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 578
    :cond_0
    new-instance v2, Ljava/text/SimpleDateFormat;

    .end local v2    # "dateFormat":Ljava/text/DateFormat;
    const-string/jumbo v5, "MM/dd/yyyy"

    invoke-static {p1}, Lcom/vlingo/core/internal/settings/Settings;->getLocaleForIsoLanguage(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v6

    invoke-direct {v2, v5, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 583
    .restart local v2    # "dateFormat":Ljava/text/DateFormat;
    :cond_1
    :goto_0
    invoke-virtual {v2, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 587
    .end local v1    # "date":Ljava/util/Date;
    .end local v2    # "dateFormat":Ljava/text/DateFormat;
    .end local v4    # "phonedf":Ljava/text/DateFormat;
    :goto_1
    return-object v0

    .line 579
    .restart local v1    # "date":Ljava/util/Date;
    .restart local v2    # "dateFormat":Ljava/text/DateFormat;
    .restart local v4    # "phonedf":Ljava/text/DateFormat;
    :cond_2
    const-string/jumbo v5, "ja-JP"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string/jumbo v5, "zh-CN"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 581
    :cond_3
    const/4 v5, 0x1

    invoke-static {p1}, Lcom/vlingo/core/internal/settings/Settings;->getLocaleForIsoLanguage(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v6

    invoke-static {v5, v6}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 584
    .end local v1    # "date":Ljava/util/Date;
    .end local v2    # "dateFormat":Ljava/text/DateFormat;
    .end local v4    # "phonedf":Ljava/text/DateFormat;
    :catch_0
    move-exception v3

    .line 585
    .local v3, "e":Ljava/text/ParseException;
    invoke-virtual {v3}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_1
.end method

.method public static getTimeFromCanonicalTimeString(Ljava/lang/String;)Ljava/util/Date;
    .locals 2
    .param p0, "canonicalDateAndTime"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 373
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/vlingo/core/internal/schedule/DateUtil;->CANONICAL_RELAIVE_TIME_FORMAT:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 374
    .local v0, "format":Ljava/text/SimpleDateFormat;
    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    return-object v1
.end method

.method public static getTimeFromTimeString(Ljava/lang/String;Landroid/text/format/Time;Z)J
    .locals 20
    .param p0, "timeString"    # Ljava/lang/String;
    .param p1, "t"    # Landroid/text/format/Time;
    .param p2, "adjustForNextDay"    # Z

    .prologue
    .line 234
    const/4 v15, 0x0

    .line 235
    .local v15, "now":Z
    const-string/jumbo v2, "+"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 236
    const/4 v15, 0x1

    .line 241
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 243
    :cond_0
    const/4 v12, 0x0

    .line 244
    .local v12, "hour":Ljava/lang/String;
    const/4 v13, 0x0

    .line 245
    .local v13, "minute":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v5, 0x5

    if-eq v2, v5, :cond_1

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v5, 0x4

    if-ne v2, v5, :cond_2

    .line 246
    :cond_1
    const/16 v2, 0x3a

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    .line 247
    .local v8, "colonIndex":I
    if-lez v8, :cond_2

    .line 248
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 249
    add-int/lit8 v2, v8, 0x1

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 254
    .end local v8    # "colonIndex":I
    :cond_2
    const/4 v4, -0x1

    .line 255
    .local v4, "hr":I
    const/4 v3, -0x1

    .line 257
    .local v3, "mn":I
    if-eqz v12, :cond_4

    const-string/jumbo v2, "\\d+"

    invoke-virtual {v12, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    if-eqz v13, :cond_4

    const-string/jumbo v2, "\\d+"

    invoke-virtual {v13, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 259
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 260
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 264
    const/16 v2, 0x18

    if-ge v4, v2, :cond_3

    const/16 v2, 0x3c

    if-ge v3, v2, :cond_3

    if-ltz v4, :cond_3

    if-gez v3, :cond_4

    .line 267
    :cond_3
    const/4 v4, -0x1

    .line 268
    const/4 v3, -0x1

    .line 272
    :cond_4
    const/4 v2, -0x1

    if-eq v2, v4, :cond_5

    const/4 v2, -0x1

    if-eq v2, v3, :cond_5

    if-eqz v15, :cond_5

    .line 275
    move-object/from16 v0, p1

    iget v0, v0, Landroid/text/format/Time;->hour:I

    move/from16 v16, v0

    .line 276
    .local v16, "nowHr":I
    move-object/from16 v0, p1

    iget v0, v0, Landroid/text/format/Time;->minute:I

    move/from16 v17, v0

    .line 277
    .local v17, "nowMn":I
    add-int v2, v17, v3

    div-int/lit8 v14, v2, 0x3c

    .line 278
    .local v14, "mnRollOver":I
    add-int v2, v17, v3

    rem-int/lit8 v3, v2, 0x3c

    .line 279
    add-int v2, v16, v4

    add-int/2addr v2, v14

    rem-int/lit8 v4, v2, 0x18

    .line 283
    .end local v14    # "mnRollOver":I
    .end local v16    # "nowHr":I
    .end local v17    # "nowMn":I
    :cond_5
    const/4 v9, 0x0

    .line 284
    .local v9, "extraDays":I
    if-eqz p2, :cond_6

    .line 285
    mul-int/lit8 v2, v4, 0x3c

    add-int/2addr v2, v3

    move-object/from16 v0, p1

    iget v5, v0, Landroid/text/format/Time;->hour:I

    mul-int/lit8 v5, v5, 0x3c

    move-object/from16 v0, p1

    iget v6, v0, Landroid/text/format/Time;->minute:I

    add-int/2addr v5, v6

    if-ge v2, v5, :cond_6

    .line 286
    const/4 v9, 0x1

    .line 290
    :cond_6
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 291
    .local v1, "returnTime":Landroid/text/format/Time;
    int-to-long v5, v9

    const-wide/16 v18, 0x18

    mul-long v5, v5, v18

    const-wide/16 v18, 0x3c

    mul-long v5, v5, v18

    const-wide/16 v18, 0x3c

    mul-long v5, v5, v18

    const-wide/16 v18, 0x3e8

    mul-long v10, v5, v18

    .line 292
    .local v10, "extraMilliseconds":J
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v5

    add-long/2addr v5, v10

    invoke-virtual {v1, v5, v6}, Landroid/text/format/Time;->set(J)V

    .line 293
    const/4 v2, 0x0

    iget v5, v1, Landroid/text/format/Time;->monthDay:I

    iget v6, v1, Landroid/text/format/Time;->month:I

    iget v7, v1, Landroid/text/format/Time;->year:I

    invoke-virtual/range {v1 .. v7}, Landroid/text/format/Time;->set(IIIIII)V

    .line 294
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v5

    return-wide v5
.end method

.method public static getUTCTimeMillis(J)J
    .locals 4
    .param p0, "millis"    # J

    .prologue
    .line 565
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 566
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v1

    .line 567
    .local v1, "offset":I
    int-to-long v2, v1

    add-long/2addr v2, p0

    return-wide v2
.end method

.method private static initMatcher()V
    .locals 9

    .prologue
    .line 301
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    sput-object v3, Lcom/vlingo/core/internal/schedule/DateUtil;->dateMatcher:Ljava/util/ArrayList;

    .line 305
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_monthMatchers:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/ResourceIdProvider;->getStringArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;)[Ljava/lang/String;

    move-result-object v2

    .line 307
    .local v2, "monthMatchers":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v3, 0xc

    if-ge v1, v3, :cond_1

    .line 308
    array-length v3, v2

    if-gt v1, v3, :cond_0

    .line 309
    sget-object v3, Lcom/vlingo/core/internal/schedule/DateUtil;->dateMatcher:Ljava/util/ArrayList;

    new-instance v4, Landroid/util/Pair;

    aget-object v5, v2, v1

    new-instance v6, Landroid/util/Pair;

    sget-object v7, Lcom/vlingo/core/internal/schedule/DateUtil$DateType;->MONTH:Lcom/vlingo/core/internal/schedule/DateUtil$DateType;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-direct {v4, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 307
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 315
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_dayMatchers:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/ResourceIdProvider;->getStringArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;)[Ljava/lang/String;

    move-result-object v0

    .line 317
    .local v0, "dayMatchers":[Ljava/lang/String;
    const/4 v1, 0x0

    :goto_1
    const/4 v3, 0x7

    if-ge v1, v3, :cond_3

    .line 318
    array-length v3, v0

    if-gt v1, v3, :cond_2

    .line 319
    sget-object v3, Lcom/vlingo/core/internal/schedule/DateUtil;->dateMatcher:Ljava/util/ArrayList;

    new-instance v4, Landroid/util/Pair;

    aget-object v5, v0, v1

    new-instance v6, Landroid/util/Pair;

    sget-object v7, Lcom/vlingo/core/internal/schedule/DateUtil$DateType;->WEEK_DAY:Lcom/vlingo/core/internal/schedule/DateUtil$DateType;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-direct {v4, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 317
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 322
    :cond_3
    return-void
.end method

.method public static isSameOrFurtherDate(Ljava/util/Date;Ljava/util/Date;)Z
    .locals 3
    .param p0, "date1"    # Ljava/util/Date;
    .param p1, "date2"    # Ljava/util/Date;

    .prologue
    .line 537
    invoke-static {p0}, Lcom/vlingo/core/internal/schedule/DateUtil;->buildDayObject(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v0

    .line 538
    .local v0, "cal1":Ljava/util/Calendar;
    invoke-static {p1}, Lcom/vlingo/core/internal/schedule/DateUtil;->buildDayObject(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v1

    .line 540
    .local v1, "cal2":Ljava/util/Calendar;
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isToday(Ljava/lang/String;)Z
    .locals 10
    .param p0, "date"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    .line 81
    const-wide/16 v0, 0x0

    .line 82
    .local v0, "argMillis":J
    const-wide/16 v4, 0x0

    .line 83
    .local v4, "nowMillis":J
    const-wide/16 v6, 0x0

    .line 84
    .local v6, "nowStartMillis":J
    const-wide/16 v2, 0x0

    .line 89
    .local v2, "nowEndMillis":J
    invoke-static {p0, v8}, Lcom/vlingo/core/internal/schedule/DateUtil;->getMillisFromString(Ljava/lang/String;Z)J

    move-result-wide v0

    .line 90
    invoke-static {}, Lcom/vlingo/core/internal/schedule/DateUtil;->now()J

    move-result-wide v4

    .line 91
    invoke-static {v4, v5}, Lcom/vlingo/core/internal/schedule/DateUtil;->startOfGivenDay(J)J

    move-result-wide v6

    .line 92
    invoke-static {v4, v5}, Lcom/vlingo/core/internal/schedule/DateUtil;->endOfGivenDay(J)J

    move-result-wide v2

    .line 93
    cmp-long v9, v0, v6

    if-ltz v9, :cond_0

    cmp-long v9, v0, v2

    if-gtz v9, :cond_0

    :goto_0
    return v8

    :cond_0
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public static now()J
    .locals 2

    .prologue
    .line 54
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public static plusDays(Ljava/util/Date;I)Ljava/util/Date;
    .locals 2
    .param p0, "currentDate"    # Ljava/util/Date;
    .param p1, "days"    # I

    .prologue
    .line 114
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 115
    .local v0, "calendar":Ljava/util/GregorianCalendar;
    invoke-virtual {v0, p0}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V

    .line 116
    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Ljava/util/GregorianCalendar;->add(II)V

    .line 117
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v1

    return-object v1
.end method

.method public static startOfGivenDay(J)J
    .locals 3
    .param p0, "startMillis"    # J

    .prologue
    const/4 v1, 0x0

    .line 158
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 159
    .local v0, "time":Landroid/text/format/Time;
    invoke-virtual {v0, p0, p1}, Landroid/text/format/Time;->set(J)V

    .line 160
    iput v1, v0, Landroid/text/format/Time;->hour:I

    .line 161
    iput v1, v0, Landroid/text/format/Time;->minute:I

    .line 162
    iput v1, v0, Landroid/text/format/Time;->second:I

    .line 163
    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v1

    return-wide v1
.end method

.method public static topOfTheHour(J)Landroid/text/format/Time;
    .locals 2
    .param p0, "millis"    # J

    .prologue
    .line 72
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 73
    .local v0, "t":Landroid/text/format/Time;
    invoke-virtual {v0, p0, p1}, Landroid/text/format/Time;->set(J)V

    .line 74
    const/4 v1, 0x0

    iput v1, v0, Landroid/text/format/Time;->second:I

    .line 75
    iget v1, v0, Landroid/text/format/Time;->second:I

    iput v1, v0, Landroid/text/format/Time;->minute:I

    .line 76
    return-object v0
.end method
