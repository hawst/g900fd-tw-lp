.class public Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;
.super Ljava/lang/Object;
.source "ASyncAudioDataSink.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MicData"
.end annotation


# instance fields
.field private final audioData:[S

.field private final audioSourceId:I

.field private final offsetInShorts:I

.field private final sizeInShorts:I


# direct methods
.method public constructor <init>([SIII)V
    .locals 3
    .param p1, "audioData"    # [S
    .param p2, "offsetInShorts"    # I
    .param p3, "sizeInShorts"    # I
    .param p4, "audioSourceId"    # I

    .prologue
    const/4 v2, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    array-length v0, p1

    new-array v0, v0, [S

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;->audioData:[S

    .line 27
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;->audioData:[S

    array-length v1, p1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 28
    iput p2, p0, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;->offsetInShorts:I

    .line 29
    iput p3, p0, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;->sizeInShorts:I

    .line 30
    iput p4, p0, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;->audioSourceId:I

    .line 31
    return-void
.end method


# virtual methods
.method public getAudioData()[S
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;->audioData:[S

    return-object v0
.end method

.method public getAudioSourceId()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;->audioSourceId:I

    return v0
.end method

.method public getOffsetInShorts()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;->offsetInShorts:I

    return v0
.end method

.method public getSizeInShorts()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink$MicData;->sizeInShorts:I

    return v0
.end method
