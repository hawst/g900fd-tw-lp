.class final Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;
.super Ljava/util/LinkedHashMap;
.source "DataTable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap",
        "<",
        "Ljava/lang/String;",
        "Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 31
    const-string/jumbo v0, "raw_contact_id"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->INTEGER:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    const-string/jumbo v0, "contact_id"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->INTEGER:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    const-string/jumbo v0, "version"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->INTEGER:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    const-string/jumbo v0, "mimetype"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->TEXT:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    const-string/jumbo v0, "data1"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->TEXT:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    const-string/jumbo v0, "data2"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->TEXT:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    const-string/jumbo v0, "data3"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->TEXT:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    const-string/jumbo v0, "data4"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->TEXT:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    const-string/jumbo v0, "data5"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->TEXT:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    const-string/jumbo v0, "data6"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->TEXT:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    const-string/jumbo v0, "data7"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->TEXT:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    const-string/jumbo v0, "data8"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->TEXT:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    const-string/jumbo v0, "data9"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->TEXT:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    const-string/jumbo v0, "times_contacted"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->INTEGER:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    const-string/jumbo v0, "starred"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->INTEGER:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    const-string/jumbo v0, "display_name"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->TEXT:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    const-string/jumbo v0, "phonetic_name"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->TEXT:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    const-string/jumbo v0, "lookup"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->TEXT:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    const-string/jumbo v0, "in_visible_group"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->INTEGER:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    const-string/jumbo v0, "original_id"

    sget-object v1, Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;->LONG:Lcom/vlingo/core/internal/contacts/contentprovider/FieldType;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    return-void
.end method
