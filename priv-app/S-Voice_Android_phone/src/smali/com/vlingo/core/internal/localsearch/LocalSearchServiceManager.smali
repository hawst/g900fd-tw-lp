.class public Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;
.super Ljava/lang/Object;
.source "LocalSearchServiceManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$AuditLogHTTPHandler;,
        Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$InfoActivityHTTPHandler;,
        Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$HttpCallbackHandler;,
        Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$NaverHTTPHandler;,
        Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$WorldTimeHTTPHandler;,
        Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$WeatherHTTPHandler;,
        Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;
    }
.end annotation


# static fields
.field static final AUDIT_LOG_MAX_RETRYS:I = 0xa

.field public static final INFO_ACTIVITY_AUTO_DIALED_CALL:Ljava/lang/String; = "UserAutoDialCall"

.field public static final INFO_ACTIVITY_CLICKED_DETAILS:Ljava/lang/String; = "UserClickedDetails"

.field public static final INFO_ACTIVITY_CLICKED_LINK:Ljava/lang/String; = "UserClickedLink"

.field public static final INFO_ACTIVITY_CLICKED_NAVIGATE:Ljava/lang/String; = "UserClickedNavigate"

.field public static final INFO_ACTIVITY_CLICKED_ON_MAP:Ljava/lang/String; = "UserClickedOnMap"

.field public static final INFO_ACTIVITY_CLICKED_RESERVE:Ljava/lang/String; = "UserClickedReserve"

.field public static final INFO_ACTIVITY_CLICKED_TO_CALL_BRIEF:Ljava/lang/String; = "UserClickedToCallBrief"

.field public static final INFO_ACTIVITY_CLICKED_TO_CALL_DETAIL:Ljava/lang/String; = "UserClickedToCallDetail"

.field static final INFO_ACTIVITY_MAX_RETRYS:I = 0xa

.field static final INFO_ACTIVITY_RETRY_DELAY_MS:I = 0x4e20

.field private static final TAG:Ljava/lang/String;

.field private static maxSponListings:I

.field private static sm_vcsUrl:Lcom/vlingo/sdk/internal/http/URL;


# instance fields
.field m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

.field m_dateFormat:Ljava/text/SimpleDateFormat;

.field m_requestListener:Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    .line 40
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd HH:mm:ss"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_dateFormat:Ljava/text/SimpleDateFormat;

    .line 56
    const-string/jumbo v0, "localsearch.max_spon_listing"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->maxSponListings:I

    .line 57
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static setVcsUri(Ljava/lang/String;)V
    .locals 3
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 47
    new-instance v0, Lcom/vlingo/sdk/internal/http/URL;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/localsearch/localsearch"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sm_vcsUrl:Lcom/vlingo/sdk/internal/http/URL;

    .line 48
    return-void
.end method


# virtual methods
.method addActionTime(Ljava/lang/StringBuilder;)V
    .locals 2
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 795
    const-string/jumbo v0, "<ActionTime>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 796
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_dateFormat:Ljava/text/SimpleDateFormat;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 797
    const-string/jumbo v0, "</ActionTime>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 798
    return-void
.end method

.method addLocation(Ljava/lang/StringBuilder;)V
    .locals 4
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 781
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getCellTowerInfo()Ljava/lang/String;

    move-result-object v1

    .line 783
    .local v1, "locationString":Ljava/lang/String;
    const-string/jumbo v3, "Lat="

    invoke-virtual {p0, v1, v3}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->getStringValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 784
    .local v0, "latString":Ljava/lang/String;
    const-string/jumbo v3, "Long="

    invoke-virtual {p0, v1, v3}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->getStringValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 785
    .local v2, "lonString":Ljava/lang/String;
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    .line 786
    const-string/jumbo v3, "<Lat>"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 787
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 788
    const-string/jumbo v3, "</Lat><Lon>"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 789
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 790
    const-string/jumbo v3, "</Lon>"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 792
    :cond_0
    return-void
.end method

.method cancelCurrentRequest()V
    .locals 1

    .prologue
    .line 774
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    if-eqz v0, :cond_0

    .line 775
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/HttpRequest;->cancel()V

    .line 776
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    .line 778
    :cond_0
    return-void
.end method

.method getStringValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "input"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 801
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    .line 802
    .local v1, "keyLen":I
    invoke-virtual {p1, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 803
    .local v2, "startIndex":I
    if-gez v2, :cond_1

    .line 809
    :cond_0
    :goto_0
    return-object v3

    .line 805
    :cond_1
    add-int/2addr v2, v1

    .line 806
    const/16 v4, 0x3b

    invoke-virtual {p1, v4, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 807
    .local v0, "endIndex":I
    if-ltz v0, :cond_0

    .line 809
    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public sendAuditLogRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "requestType"    # Ljava/lang/String;
    .param p2, "trackingId"    # Ljava/lang/String;
    .param p3, "action"    # Ljava/lang/String;

    .prologue
    .line 684
    const/16 v0, 0xa

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sendAuditLogRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 685
    return-void
.end method

.method public sendAuditLogRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 9
    .param p1, "requestType"    # Ljava/lang/String;
    .param p2, "trackingId"    # Ljava/lang/String;
    .param p3, "action"    # Ljava/lang/String;
    .param p4, "retryNum"    # I

    .prologue
    .line 688
    new-instance v7, Ljava/lang/StringBuilder;

    const/16 v0, 0xc8

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 689
    .local v7, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v0, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 690
    const-string/jumbo v0, "<"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 691
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 692
    const-string/jumbo v0, ">"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 693
    const-string/jumbo v0, "<TrackingID>"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 694
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 695
    const-string/jumbo v0, "</TrackingID>"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 696
    const-string/jumbo v0, "<Action>"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 697
    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 698
    const-string/jumbo v0, "</Action> "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 699
    invoke-virtual {p0, v7}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->addActionTime(Ljava/lang/StringBuilder;)V

    .line 700
    const-string/jumbo v0, "</"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 701
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 702
    const-string/jumbo v0, ">"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 703
    const-string/jumbo v8, "LocalSearch-AuditLogHandler"

    new-instance v0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$AuditLogHTTPHandler;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$AuditLogHTTPHandler;-><init>(Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v1, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sm_vcsUrl:Lcom/vlingo/sdk/internal/http/URL;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v8, v0, v1, v2}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->createVLRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Lcom/vlingo/sdk/internal/http/URL;Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    move-result-object v6

    .line 704
    .local v6, "req":Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    invoke-virtual {v6}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->start()V

    .line 705
    return-void
.end method

.method public declared-synchronized sendChineseMoreDetailsRequest(Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V
    .locals 5
    .param p1, "currentBusinessItem"    # Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    .param p2, "requestListener"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .prologue
    .line 229
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->cancelCurrentRequest()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 231
    :try_start_1
    invoke-virtual {p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getListingID()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchChineseURLMaker;->getUrlRecentReviewsForBusiness(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 234
    .local v1, "requestURL":Ljava/lang/String;
    const-string/jumbo v2, "ChineseLocalSearch-Details"

    new-instance v3, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;

    new-instance v4, Lcom/vlingo/core/internal/localsearch/LocalSearchChineseResponseParser;

    invoke-direct {v4}, Lcom/vlingo/core/internal/localsearch/LocalSearchChineseResponseParser;-><init>()V

    invoke-direct {v3, p0, p1, p2, v4}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;-><init>(Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;Lcom/vlingo/core/internal/localsearch/LocalSearchParser;)V

    invoke-static {v2, v3, v1}, Lcom/vlingo/sdk/internal/http/HttpRequest;->createRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Ljava/lang/String;)Lcom/vlingo/sdk/internal/http/HttpRequest;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    .line 235
    iget-object v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    const-string/jumbo v3, "GET"

    invoke-virtual {v2, v3}, Lcom/vlingo/sdk/internal/http/HttpRequest;->setMethod(Ljava/lang/String;)V

    .line 236
    iget-object v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    const/16 v3, 0x2710

    invoke-virtual {v2, v3}, Lcom/vlingo/sdk/internal/http/HttpRequest;->setTimeout(I)V

    .line 237
    iget-object v2, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/http/HttpRequest;->start()V
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 241
    .end local v1    # "requestURL":Ljava/lang/String;
    :goto_0
    monitor-exit p0

    return-void

    .line 238
    :catch_0
    move-exception v0

    .line 239
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    :try_start_2
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;->onRequestFailed(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 229
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized sendChineseSearchRequest(Ljava/lang/String;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V
    .locals 3
    .param p1, "requestURL"    # Ljava/lang/String;
    .param p2, "requestListener"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .prologue
    .line 186
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->cancelCurrentRequest()V

    .line 189
    const-string/jumbo v0, "ChineseLocalSearch-Search"

    new-instance v1, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;

    new-instance v2, Lcom/vlingo/core/internal/localsearch/LocalSearchChineseResponseParser;

    invoke-direct {v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchChineseResponseParser;-><init>()V

    invoke-direct {v1, p0, p2, v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;-><init>(Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;Lcom/vlingo/core/internal/localsearch/LocalSearchParser;)V

    invoke-static {v0, v1, p1}, Lcom/vlingo/sdk/internal/http/HttpRequest;->createRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Ljava/lang/String;)Lcom/vlingo/sdk/internal/http/HttpRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    .line 190
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    const-string/jumbo v1, "GET"

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/http/HttpRequest;->setMethod(Ljava/lang/String;)V

    .line 191
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/http/HttpRequest;->setTimeout(I)V

    .line 192
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/HttpRequest;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 193
    monitor-exit p0

    return-void

    .line 186
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public sendInfoActivityRequest(Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Ljava/lang/String;)V
    .locals 1
    .param p1, "bi"    # Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    .param p2, "actionType"    # Ljava/lang/String;

    .prologue
    .line 584
    const/16 v0, 0xa

    invoke-virtual {p0, p1, p2, v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sendInfoActivityRequest(Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Ljava/lang/String;I)V

    .line 585
    return-void
.end method

.method public sendInfoActivityRequest(Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Ljava/lang/String;I)V
    .locals 7
    .param p1, "bi"    # Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    .param p2, "actionType"    # Ljava/lang/String;
    .param p3, "retryNum"    # I

    .prologue
    .line 588
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0xc8

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 589
    .local v2, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 590
    const-string/jumbo v3, "<InfoActivityRequest><ListingID>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 591
    invoke-virtual {p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getListingID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 592
    const-string/jumbo v3, "</ListingID>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 593
    invoke-virtual {p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    .line 594
    .local v0, "phoneNumber":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 595
    const-string/jumbo v3, "<PhoneNumber>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 596
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 597
    const-string/jumbo v3, "</PhoneNumber>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 599
    :cond_0
    const-string/jumbo v3, "<Action>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 600
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 601
    const-string/jumbo v3, "</Action> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 602
    invoke-virtual {p0, v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->addActionTime(Ljava/lang/StringBuilder;)V

    .line 603
    const-string/jumbo v3, "</InfoActivityRequest>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 606
    const-string/jumbo v3, "LocalSearch-InfoActivity"

    new-instance v4, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$InfoActivityHTTPHandler;

    invoke-direct {v4, p0, p1, p2, p3}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$InfoActivityHTTPHandler;-><init>(Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Ljava/lang/String;I)V

    sget-object v5, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sm_vcsUrl:Lcom/vlingo/sdk/internal/http/URL;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v4, v5, v6}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->createVLRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Lcom/vlingo/sdk/internal/http/URL;Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    move-result-object v1

    .line 607
    .local v1, "req":Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->start()V

    .line 608
    return-void
.end method

.method public declared-synchronized sendMoreDetailsRequest(Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V
    .locals 5
    .param p1, "currentBusinessItem"    # Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    .param p2, "requestListener"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .prologue
    .line 212
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->cancelCurrentRequest()V

    .line 214
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 215
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    const-string/jumbo v1, "<ListingDetailRequest><ListingID>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    invoke-virtual {p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getListingID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    const-string/jumbo v1, "</ListingID>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->addActionTime(Ljava/lang/StringBuilder;)V

    .line 220
    const-string/jumbo v1, "</ListingDetailRequest>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    const-string/jumbo v1, "LocalSearch-Details"

    new-instance v2, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;

    new-instance v3, Lcom/vlingo/core/internal/localsearch/LocalSearchResponseParser;

    invoke-direct {v3}, Lcom/vlingo/core/internal/localsearch/LocalSearchResponseParser;-><init>()V

    invoke-direct {v2, p0, p1, p2, v3}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;-><init>(Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;Lcom/vlingo/core/internal/localsearch/LocalSearchParser;)V

    sget-object v3, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sm_vcsUrl:Lcom/vlingo/sdk/internal/http/URL;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->createVLRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Lcom/vlingo/sdk/internal/http/URL;Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    .line 224
    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->updateLocalSearchRequestTimeout(Lcom/vlingo/sdk/internal/http/HttpRequest;)V

    .line 225
    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/http/HttpRequest;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226
    monitor-exit p0

    return-void

    .line 212
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized sendNaverRequest(Ljava/lang/String;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V
    .locals 5
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "requestListener"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .prologue
    .line 140
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->cancelCurrentRequest()V

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 142
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    const-string/jumbo v1, "<NaverPassThroughRequest Query=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    const-string/jumbo v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    const-string/jumbo v1, "/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    const-string/jumbo v1, "NaverRequest-Request"

    new-instance v2, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$NaverHTTPHandler;

    invoke-direct {v2, p0, p2}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$NaverHTTPHandler;-><init>(Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V

    sget-object v3, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sm_vcsUrl:Lcom/vlingo/sdk/internal/http/URL;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->createVLRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Lcom/vlingo/sdk/internal/http/URL;Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    .line 151
    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/http/HttpRequest;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    monitor-exit p0

    return-void

    .line 140
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized sendRequest(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V
    .locals 2
    .param p1, "taskName"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "requestListener"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .prologue
    .line 123
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->cancelCurrentRequest()V

    .line 124
    new-instance v0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$HttpCallbackHandler;

    invoke-direct {v0, p0, p3}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$HttpCallbackHandler;-><init>(Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V

    invoke-static {p1, v0, p2}, Lcom/vlingo/sdk/internal/http/HttpRequest;->createRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Ljava/lang/String;)Lcom/vlingo/sdk/internal/http/HttpRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    .line 125
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    const-string/jumbo v1, "GET"

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/http/HttpRequest;->setMethod(Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/HttpRequest;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    monitor-exit p0

    return-void

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized sendSearchRequest(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V
    .locals 5
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "spokenLocation"    # Ljava/lang/String;
    .param p3, "requestListener"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .prologue
    .line 164
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->cancelCurrentRequest()V

    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 167
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    const-string/jumbo v1, "<LocalSearchRequest><Query>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    const-string/jumbo v1, "</Query>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    invoke-static {p2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 172
    const-string/jumbo v1, "<SpokenLocation>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    const-string/jumbo v1, "</SpokenLocation>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    :cond_0
    const-string/jumbo v1, "<MaxListings>20</MaxListings><MaxSponListings>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->maxSponListings:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</MaxSponListings></LocalSearchRequest>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    const-string/jumbo v1, "LocalSearch-Search"

    new-instance v2, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;

    new-instance v3, Lcom/vlingo/core/internal/localsearch/LocalSearchResponseParser;

    invoke-direct {v3}, Lcom/vlingo/core/internal/localsearch/LocalSearchResponseParser;-><init>()V

    invoke-direct {v2, p0, p3, v3}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$LocalSearchHTTPHandler;-><init>(Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;Lcom/vlingo/core/internal/localsearch/LocalSearchParser;)V

    sget-object v3, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sm_vcsUrl:Lcom/vlingo/sdk/internal/http/URL;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->createVLRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Lcom/vlingo/sdk/internal/http/URL;Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    .line 181
    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->updateLocalSearchRequestTimeout(Lcom/vlingo/sdk/internal/http/HttpRequest;)V

    .line 182
    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/http/HttpRequest;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    monitor-exit p0

    return-void

    .line 164
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized sendWeatherRequest(Ljava/lang/String;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "requestListener"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .prologue
    .line 69
    monitor-enter p0

    :try_start_0
    const-string/jumbo v0, "1"

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sendWeatherRequest(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    monitor-exit p0

    return-void

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized sendWeatherRequest(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;I)V
    .locals 5
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "days"    # Ljava/lang/String;
    .param p3, "requestListener"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;
    .param p4, "version"    # I

    .prologue
    .line 73
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->cancelCurrentRequest()V

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 75
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    const-string/jumbo v1, "<WeatherRequest"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 79
    const-string/jumbo v1, " Query=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    const-string/jumbo v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    :cond_0
    if-eqz p2, :cond_1

    .line 85
    const-string/jumbo v1, " ForecastDays=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    const-string/jumbo v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    :cond_1
    const/4 v1, 0x1

    if-le p4, v1, :cond_2

    .line 91
    const-string/jumbo v1, " Version=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 93
    const-string/jumbo v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    :cond_2
    const-string/jumbo v1, "/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    const-string/jumbo v1, "WeatherRequest-Request"

    new-instance v2, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$WeatherHTTPHandler;

    invoke-direct {v2, p0, p3}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$WeatherHTTPHandler;-><init>(Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V

    sget-object v3, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sm_vcsUrl:Lcom/vlingo/sdk/internal/http/URL;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->createVLRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Lcom/vlingo/sdk/internal/http/URL;Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    .line 100
    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/http/HttpRequest;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    monitor-exit p0

    return-void

    .line 73
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized sendWorldTimeRequest(Ljava/lang/String;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V
    .locals 5
    .param p1, "location"    # Ljava/lang/String;
    .param p2, "requestListener"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;

    .prologue
    .line 104
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->cancelCurrentRequest()V

    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 106
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    const-string/jumbo v1, "<WorldTimeRequest"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 110
    const-string/jumbo v1, " Query=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    const-string/jumbo v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    :cond_0
    const-string/jumbo v1, "/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    const-string/jumbo v1, "WorldTimeRequest-Request"

    new-instance v2, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$WorldTimeHTTPHandler;

    invoke-direct {v2, p0, p2}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager$WorldTimeHTTPHandler;-><init>(Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V

    sget-object v3, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sm_vcsUrl:Lcom/vlingo/sdk/internal/http/URL;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->createVLRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Lcom/vlingo/sdk/internal/http/URL;Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    .line 119
    iget-object v1, p0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->m_currentRequest:Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/http/HttpRequest;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    monitor-exit p0

    return-void

    .line 104
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method protected updateLocalSearchRequestTimeout(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 200
    const-string/jumbo v1, "vcs.timeout.ms"

    const/4 v2, -0x1

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 201
    .local v0, "timeout":I
    if-lez v0, :cond_0

    .line 202
    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/http/HttpRequest;->setTimeout(I)V

    .line 204
    :cond_0
    return-void
.end method
