.class public Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;
.super Ljava/lang/Object;
.source "SMSMMSAlertSource.java"

# interfaces
.implements Lcom/vlingo/core/internal/safereader/ISafeReaderAlertSource;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;,
        Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final HANDLE_CHANGE_WAIT_MMS_MS:J

.field private final HANDLE_CHANGE_WAIT_MS:J

.field private final MSG_HIGH_PRIORITY:I

.field private final MSG_LOW_PRIORITY:I

.field private final MSG_NEW_MMS_INCOMING_ALERT:I

.field private final MSG_NEW_SMS_INCOMING_ALERT:I

.field private intentFilter:Landroid/content/IntentFilter;

.field private mContext:Landroid/content/Context;

.field public mHandler:Landroid/os/Handler;

.field private final m_timeChangedReceiver:Landroid/content/BroadcastReceiver;

.field private mmsReceivingBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private observer:Landroid/database/ContentObserver;

.field private priority:I

.field private safeReaderAlertChangeListener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

.field private safeReaderMMSAlertChangeListener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

.field private safeReaderSMSAlertChangeListener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->HANDLE_CHANGE_WAIT_MS:J

    .line 34
    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->HANDLE_CHANGE_WAIT_MMS_MS:J

    .line 35
    iput v3, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->MSG_NEW_SMS_INCOMING_ALERT:I

    .line 36
    const/4 v0, 0x2

    iput v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->MSG_NEW_MMS_INCOMING_ALERT:I

    .line 37
    iput v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->MSG_LOW_PRIORITY:I

    .line 38
    iput v3, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->MSG_HIGH_PRIORITY:I

    .line 40
    iput-object v4, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->observer:Landroid/database/ContentObserver;

    .line 41
    iput-object v4, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->mmsReceivingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 47
    iput v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->priority:I

    .line 118
    new-instance v0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$1;-><init>(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->mHandler:Landroid/os/Handler;

    .line 265
    new-instance v0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$2;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$2;-><init>(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->m_timeChangedReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;)Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->safeReaderAlertChangeListener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

    return-object v0
.end method

.method static synthetic access$002(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;)Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;
    .param p1, "x1"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->safeReaderAlertChangeListener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

    return-object p1
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;)Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->safeReaderMMSAlertChangeListener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

    return-object v0
.end method

.method static synthetic access$102(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;)Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;
    .param p1, "x1"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->safeReaderMMSAlertChangeListener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

    return-object p1
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;)Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->safeReaderSMSAlertChangeListener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

    return-object v0
.end method

.method static synthetic access$202(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;)Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;
    .param p1, "x1"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->safeReaderSMSAlertChangeListener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

    return-object p1
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    .prologue
    .line 26
    iget v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->priority:I

    return v0
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public getBroadCastReceiver()Landroid/content/BroadcastReceiver;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->mmsReceivingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    const-string/jumbo v0, "SMSMMSAlertSource"

    return-object v0
.end method

.method public getObserver()Landroid/database/ContentObserver;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->observer:Landroid/database/ContentObserver;

    return-object v0
.end method

.method public getPriority()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->priority:I

    return v0
.end method

.method public onStart(Landroid/content/Context;Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "changeListener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->mContext:Landroid/content/Context;

    .line 78
    new-instance v1, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2, p2}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$SMSMMSContentObserver;-><init>(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;Landroid/os/Handler;Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->observer:Landroid/database/ContentObserver;

    .line 79
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v0

    .line 80
    .local v0, "smsmmsProvider":Lcom/vlingo/core/internal/messages/SMSMMSProvider;
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->observer:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->registerSMSMMSContentObserver(Landroid/database/ContentObserver;)V

    .line 84
    new-instance v1, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver;

    invoke-direct {v1, p0, p2}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$MMSReceivingBroadcastReceiver;-><init>(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->mmsReceivingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 85
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->mmsReceivingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "com.android.mms.MMS_BR_FOR_VLINGO"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 87
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->mmsReceivingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "com.android.mms.RECEIVED_MSG"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 91
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    iput-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->intentFilter:Landroid/content/IntentFilter;

    .line 92
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->intentFilter:Landroid/content/IntentFilter;

    const-string/jumbo v2, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 93
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->intentFilter:Landroid/content/IntentFilter;

    const-string/jumbo v2, "android.intent.action.TIME_SET"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 94
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->m_timeChangedReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->intentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 95
    return-void
.end method

.method public onStop(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 101
    iget-object v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->observer:Landroid/database/ContentObserver;

    if-eqz v2, :cond_0

    .line 102
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v1

    .line 103
    .local v1, "smsmmsProvider":Lcom/vlingo/core/internal/messages/SMSMMSProvider;
    iget-object v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->observer:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 104
    iput-object v3, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->observer:Landroid/database/ContentObserver;

    .line 107
    .end local v1    # "smsmmsProvider":Lcom/vlingo/core/internal/messages/SMSMMSProvider;
    :cond_0
    iget-object v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->mmsReceivingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_1

    .line 108
    iget-object v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->mmsReceivingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 109
    iput-object v3, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->mmsReceivingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 112
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->m_timeChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :goto_0
    return-void

    .line 113
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->getName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "brodcast receiver wasn\'t registered"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setPriority(Z)V
    .locals 1
    .param p1, "high"    # Z

    .prologue
    .line 55
    if-eqz p1, :cond_0

    .line 56
    const/4 v0, 0x1

    iput v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->priority:I

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->priority:I

    goto :goto_0
.end method
