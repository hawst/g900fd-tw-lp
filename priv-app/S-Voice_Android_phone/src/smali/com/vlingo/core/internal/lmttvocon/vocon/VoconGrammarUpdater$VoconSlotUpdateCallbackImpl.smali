.class final Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$VoconSlotUpdateCallbackImpl;
.super Ljava/lang/Object;
.source "VoconGrammarUpdater.java"

# interfaces
.implements Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "VoconSlotUpdateCallbackImpl"
.end annotation


# instance fields
.field private final grammarUpdateCallback:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;

.field private final startFromScratch:Z

.field private successChecksumAfterUpdate:I

.field private final uploadType:Lcom/vlingo/core/internal/lmttvocon/UploadType;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/lmttvocon/UploadType;ZLcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;I)V
    .locals 0
    .param p1, "uploadType"    # Lcom/vlingo/core/internal/lmttvocon/UploadType;
    .param p2, "startFromScratch"    # Z
    .param p3, "grammarUpdateCallback"    # Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;
    .param p4, "successChecksumAfterUpdate"    # I

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    iput-object p3, p0, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$VoconSlotUpdateCallbackImpl;->grammarUpdateCallback:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;

    .line 136
    iput-boolean p2, p0, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$VoconSlotUpdateCallbackImpl;->startFromScratch:Z

    .line 137
    iput p4, p0, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$VoconSlotUpdateCallbackImpl;->successChecksumAfterUpdate:I

    .line 138
    iput-object p1, p0, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$VoconSlotUpdateCallbackImpl;->uploadType:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    .line 139
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/lmttvocon/UploadType;ZLcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;ILcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/lmttvocon/UploadType;
    .param p2, "x1"    # Z
    .param p3, "x2"    # Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;
    .param p4, "x3"    # I
    .param p5, "x4"    # Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$1;

    .prologue
    .line 128
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$VoconSlotUpdateCallbackImpl;-><init>(Lcom/vlingo/core/internal/lmttvocon/UploadType;ZLcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;I)V

    return-void
.end method


# virtual methods
.method public onCancelled()V
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$VoconSlotUpdateCallbackImpl;->grammarUpdateCallback:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;

    invoke-interface {v0}, Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;->onCancelled()V

    .line 188
    return-void
.end method

.method public onError()V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$VoconSlotUpdateCallbackImpl;->grammarUpdateCallback:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;

    invoke-interface {v0}, Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;->onError()V

    .line 183
    return-void
.end method

.method public onFinished(Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 148
    .local p1, "failedToAddTerminals":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;>;"
    .local p2, "failedToRemoveTerminals":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;>;"
    if-nez p1, :cond_0

    if-eqz p2, :cond_2

    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 149
    .local v1, "failedGrammarActions":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;>;"
    :goto_0
    if-eqz p1, :cond_3

    .line 150
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;

    .line 151
    .local v4, "slotAction":Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;
    instance-of v5, v4, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarSlotAction;

    if-eqz v5, :cond_1

    move-object v2, v4

    .line 152
    check-cast v2, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarSlotAction;

    .line 153
    .local v2, "grammarSlotAction":Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarSlotAction;
    invoke-virtual {v2}, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarSlotAction;->getGrammarAction()Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 148
    .end local v1    # "failedGrammarActions":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;>;"
    .end local v2    # "grammarSlotAction":Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarSlotAction;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "slotAction":Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 160
    .restart local v1    # "failedGrammarActions":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;>;"
    :cond_3
    if-eqz p2, :cond_5

    .line 161
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_4
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;

    .line 162
    .restart local v4    # "slotAction":Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;
    instance-of v5, v4, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarSlotAction;

    if-eqz v5, :cond_4

    move-object v2, v4

    .line 163
    check-cast v2, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarSlotAction;

    .line 164
    .restart local v2    # "grammarSlotAction":Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarSlotAction;
    invoke-virtual {v2}, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarSlotAction;->getGrammarAction()Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 171
    .end local v2    # "grammarSlotAction":Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$GrammarSlotAction;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "slotAction":Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;
    :cond_5
    if-eqz v1, :cond_6

    .line 172
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;

    .line 173
    .local v0, "failedGrammarAction":Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;
    iget v5, p0, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$VoconSlotUpdateCallbackImpl;->successChecksumAfterUpdate:I

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;->getChecksumDelta()I

    move-result v6

    sub-int/2addr v5, v6

    iput v5, p0, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$VoconSlotUpdateCallbackImpl;->successChecksumAfterUpdate:I

    goto :goto_3

    .line 176
    .end local v0    # "failedGrammarAction":Lcom/vlingo/core/internal/lmttvocon/grammar/GrammarAction;
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_6
    iget-object v5, p0, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$VoconSlotUpdateCallbackImpl;->uploadType:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    # invokes: Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater;->getUploadedChecksumKey(Lcom/vlingo/core/internal/lmttvocon/UploadType;)Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater;->access$300(Lcom/vlingo/core/internal/lmttvocon/UploadType;)Ljava/lang/String;

    move-result-object v5

    iget v6, p0, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$VoconSlotUpdateCallbackImpl;->successChecksumAfterUpdate:I

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 177
    iget-object v5, p0, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$VoconSlotUpdateCallbackImpl;->grammarUpdateCallback:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;

    iget-boolean v6, p0, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$VoconSlotUpdateCallbackImpl;->startFromScratch:Z

    invoke-interface {v5, v6, v1}, Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;->onFinished(ZLjava/util/Collection;)V

    .line 178
    return-void
.end method

.method public onStarted()V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/vocon/VoconGrammarUpdater$VoconSlotUpdateCallbackImpl;->grammarUpdateCallback:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;

    invoke-interface {v0}, Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater$GrammarUpdateCallback;->onStarted()V

    .line 144
    return-void
.end method
