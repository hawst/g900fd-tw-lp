.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetTimerHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "ShowSetTimerHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# static fields
.field private static startTime:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetTimerHandler;->startTime:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method

.method private getCanonicalTimeStringFromParam(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "param"    # Ljava/lang/String;

    .prologue
    .line 108
    if-eqz p1, :cond_0

    const-string/jumbo v0, "+"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "+"

    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .end local p1    # "param":Ljava/lang/String;
    :cond_0
    return-object p1
.end method

.method private save()V
    .locals 3

    .prologue
    .line 102
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SET_ALARM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/SetTimerAction;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetTimerHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/actions/SetTimerAction;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetTimerHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->TIMER_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/SetTimerAction;->time(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SetTimerAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/actions/SetTimerAction;->queue()V

    .line 105
    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 11
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 52
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 53
    const-string/jumbo v6, "time"

    invoke-static {p1, v6, v9}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 54
    .local v4, "time":Ljava/lang/String;
    const-string/jumbo v6, "time.spoken"

    invoke-static {p1, v6, v9}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    .line 56
    .local v5, "timeSpoken":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v6

    const-string/jumbo v7, "timer"

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 58
    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 59
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetTimerHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->TIMER_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v6, v7, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 60
    :cond_0
    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 61
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetTimerHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->TIMER_DATA_SPOKEN:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v6, v7, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 62
    sput-object v5, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetTimerHandler;->startTime:Ljava/lang/String;

    .line 64
    :cond_1
    const/4 v1, 0x0

    .line 66
    .local v1, "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    const-string/jumbo v6, "doit"

    invoke-static {p1, v6, v9, v9}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 67
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeTimerStartCmd()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v1

    .line 68
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SetTimer:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetTimerHandler;->getCanonicalTimeStringFromParam(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {p2, v6, v1, v7, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 98
    :goto_0
    return v9

    .line 72
    :cond_2
    const-string/jumbo v6, "action"

    invoke-static {p1, v6, v9}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "cmd":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 74
    const-string/jumbo v6, "stop"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 75
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeTimerStopCmd()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v1

    .line 97
    :cond_3
    :goto_1
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SetTimer:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetTimerHandler;->getCanonicalTimeStringFromParam(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {p2, v6, v1, v7, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto :goto_0

    .line 76
    :cond_4
    const-string/jumbo v6, "reset"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 77
    const-string/jumbo v6, "action.prompt.spoken"

    invoke-static {p1, v6, v9}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 78
    .local v2, "spokenText":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetTimerHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->TIMER_DATA_SPOKEN:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 79
    .local v3, "spokenTime":Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetTimerHandler;->startTime:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 80
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetTimerHandler;->startTime:Ljava/lang/String;

    .line 81
    :cond_5
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_6

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 82
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v3, v6, v9

    invoke-static {v2, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 83
    invoke-interface {p2, v10, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_6
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeTimerResetCmd()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v1

    .line 86
    goto :goto_1

    .end local v2    # "spokenText":Ljava/lang/String;
    .end local v3    # "spokenTime":Ljava/lang/String;
    :cond_7
    const-string/jumbo v6, "restart"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 87
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeTimerRestartCmd()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v1

    goto :goto_1

    .line 88
    :cond_8
    const-string/jumbo v6, "show"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 89
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeTimerShowCmd()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v1

    goto :goto_1

    .line 90
    :cond_9
    const-string/jumbo v6, "cancel"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 91
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeTimerCancelCmd()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v1

    .line 92
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetTimerHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_TASK_CANCELLED:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v8, v9, [Ljava/lang/Object;

    invoke-static {v7, v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetTimerHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 93
    sput-object v10, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetTimerHandler;->startTime:Ljava/lang/String;

    .line 94
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetTimerHandler;->reset()V

    goto/16 :goto_1
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.Save"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetTimerHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 32
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetTimerHandler;->save()V

    .line 37
    :goto_0
    return-void

    .line 35
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetTimerHandler;->throwUnknownActionException(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/ShowSetTimerHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishDialog()V

    .line 45
    return-void
.end method
