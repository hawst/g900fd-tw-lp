.class public Lcom/vlingo/core/internal/schedule/ScheduleTask;
.super Lcom/vlingo/core/internal/schedule/EventBase;
.source "ScheduleTask.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private accountName:Ljava/lang/String;

.field private bodyLength:I

.field private bodyType:I

.field private completed:Z

.field private groupid:I

.field private importance:I

.field private reminderDateCanonical:Ljava/lang/String;

.field private reminderSet:I

.field private reminderTime:J

.field private reminderTimeCanonical:Ljava/lang/String;

.field private reminderType:I

.field private utcDueDate:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 16
    invoke-direct {p0}, Lcom/vlingo/core/internal/schedule/EventBase;-><init>()V

    .line 22
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderTime:J

    .line 23
    iput v2, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->importance:I

    .line 24
    iput v2, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->groupid:I

    .line 26
    iput v2, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->bodyType:I

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->completed:Z

    return-void
.end method

.method public static clone(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleTask;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleTask;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 35
    .local v0, "c":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .line 36
    .local v4, "t":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    invoke-virtual {v4}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->clone()Lcom/vlingo/core/internal/schedule/EventBase;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .line 37
    .local v1, "copy":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 40
    .end local v0    # "c":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    .end local v1    # "copy":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "t":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    :catch_0
    move-exception v2

    .line 41
    .local v2, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/RuntimeException;

    const-string/jumbo v6, "List cloning unsupported"

    invoke-direct {v5, v6, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 39
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "c":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_0
    return-object v0
.end method


# virtual methods
.method public getAccountName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->accountName:Ljava/lang/String;

    return-object v0
.end method

.method public getBodyLength()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->bodyLength:I

    return v0
.end method

.method public getBodyType()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->bodyType:I

    return v0
.end method

.method public getCompleted()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->completed:Z

    return v0
.end method

.method public getGroupid()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->groupid:I

    return v0
.end method

.method public getImportance()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->importance:I

    return v0
.end method

.method public getReminderDateTime()Ljava/lang/String;
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderDateCanonical:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderTimeCanonical:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderDateCanonical:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderTimeCanonical:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 59
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getReminderSet()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderSet:I

    return v0
.end method

.method public getReminderTime()J
    .locals 2

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderTime:J

    return-wide v0
.end method

.method public getReminderType()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderType:I

    return v0
.end method

.method public getUtcDueDate()J
    .locals 2

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->utcDueDate:J

    return-wide v0
.end method

.method public hasDueDate()Z
    .locals 4

    .prologue
    .line 45
    iget-object v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->begin:Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/EventBase$ScheduleTime;->getTime()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public normalize(Z)V
    .locals 4
    .param p1, "fromCanonical"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 94
    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderDateCanonical:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderTimeCanonical:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 95
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderDateCanonical:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderTimeCanonical:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/schedule/DateUtil;->getDateFromCanonicalDateAndTimeString(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 96
    .local v0, "dateTime":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderTime:J

    .line 101
    .end local v0    # "dateTime":Ljava/util/Date;
    :goto_0
    return-void

    .line 98
    :cond_0
    iget-wide v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderTime:J

    iget-object v3, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderDateCanonical:Ljava/lang/String;

    invoke-virtual {p0, v1, v2, v3}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->determineDate(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderDateCanonical:Ljava/lang/String;

    .line 99
    iget-wide v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderTime:J

    iget-object v3, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderTimeCanonical:Ljava/lang/String;

    invoke-virtual {p0, v1, v2, v3}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->determineTime(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderTimeCanonical:Ljava/lang/String;

    goto :goto_0
.end method

.method setAccountName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->accountName:Ljava/lang/String;

    return-void
.end method

.method setBodyLength(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 89
    iput p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->bodyLength:I

    return-void
.end method

.method setBodyType(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 90
    iput p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->bodyType:I

    return-void
.end method

.method public setCompleted(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 91
    iput-boolean p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->completed:Z

    return-void
.end method

.method setGroupId(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 88
    iput p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->groupid:I

    return-void
.end method

.method setImportance(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 87
    iput p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->importance:I

    return-void
.end method

.method public setReminderDateTime(Ljava/lang/String;)V
    .locals 6
    .param p1, "canonical"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 64
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 65
    iput-object v5, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderDateCanonical:Ljava/lang/String;

    .line 66
    iput-object v5, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderTimeCanonical:Ljava/lang/String;

    .line 67
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderTime:J

    .line 68
    iput v3, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderSet:I

    .line 69
    iput v3, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderType:I

    .line 81
    :goto_0
    return-void

    .line 72
    :cond_0
    const/16 v1, 0x20

    invoke-static {p1, v1}, Lcom/vlingo/core/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "dateTime":[Ljava/lang/String;
    array-length v1, v0

    if-eq v1, v4, :cond_1

    .line 74
    new-instance v1, Ljava/security/InvalidParameterException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Canonical begin must be of format YYYY-MM-DD hh:mm, received: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 76
    :cond_1
    aget-object v1, v0, v3

    iput-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderDateCanonical:Ljava/lang/String;

    .line 77
    aget-object v1, v0, v2

    iput-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderTimeCanonical:Ljava/lang/String;

    .line 78
    iput v2, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderSet:I

    .line 79
    iput v4, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderType:I

    goto :goto_0
.end method

.method setReminderSet(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderSet:I

    return-void
.end method

.method setReminderTime(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 86
    iput-wide p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderTime:J

    return-void
.end method

.method setReminderType(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 84
    iput p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderType:I

    return-void
.end method

.method setUtcDueDate(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 82
    iput-wide p1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->utcDueDate:J

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "ScheduleTask{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lcom/vlingo/core/internal/schedule/EventBase;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "utcDueDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->utcDueDate:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "accountName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->accountName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "reminderType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "reminderSet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderSet:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "reminderTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "importance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->importance:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "groupid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->groupid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "bodyLength="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->bodyLength:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "bodyType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->bodyType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "completed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->completed:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "reminderTimeCanonical="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderTimeCanonical:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "preminderDateCanonical="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/schedule/ScheduleTask;->reminderDateCanonical:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
