.class public final enum Lcom/vlingo/core/internal/lmttvocon/UploadType;
.super Ljava/lang/Enum;
.source "UploadType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/lmttvocon/UploadType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/lmttvocon/UploadType;

.field public static final enum ALBUM:Lcom/vlingo/core/internal/lmttvocon/UploadType;

.field public static final enum APPLICATION:Lcom/vlingo/core/internal/lmttvocon/UploadType;

.field public static final enum ARTIST:Lcom/vlingo/core/internal/lmttvocon/UploadType;

.field public static final enum CONTACT:Lcom/vlingo/core/internal/lmttvocon/UploadType;

.field public static final enum PLAYLIST:Lcom/vlingo/core/internal/lmttvocon/UploadType;

.field public static final enum SONG:Lcom/vlingo/core/internal/lmttvocon/UploadType;


# instance fields
.field private final slotName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4
    new-instance v0, Lcom/vlingo/core/internal/lmttvocon/UploadType;

    const-string/jumbo v1, "CONTACT"

    const-string/jumbo v2, "contacts"

    invoke-direct {v0, v1, v4, v2}, Lcom/vlingo/core/internal/lmttvocon/UploadType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/lmttvocon/UploadType;->CONTACT:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    .line 5
    new-instance v0, Lcom/vlingo/core/internal/lmttvocon/UploadType;

    const-string/jumbo v1, "SONG"

    const-string/jumbo v2, "songs"

    invoke-direct {v0, v1, v5, v2}, Lcom/vlingo/core/internal/lmttvocon/UploadType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/lmttvocon/UploadType;->SONG:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    .line 6
    new-instance v0, Lcom/vlingo/core/internal/lmttvocon/UploadType;

    const-string/jumbo v1, "ARTIST"

    const-string/jumbo v2, "artists"

    invoke-direct {v0, v1, v6, v2}, Lcom/vlingo/core/internal/lmttvocon/UploadType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/lmttvocon/UploadType;->ARTIST:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    .line 7
    new-instance v0, Lcom/vlingo/core/internal/lmttvocon/UploadType;

    const-string/jumbo v1, "ALBUM"

    const-string/jumbo v2, "albums"

    invoke-direct {v0, v1, v7, v2}, Lcom/vlingo/core/internal/lmttvocon/UploadType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/lmttvocon/UploadType;->ALBUM:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    .line 8
    new-instance v0, Lcom/vlingo/core/internal/lmttvocon/UploadType;

    const-string/jumbo v1, "PLAYLIST"

    const-string/jumbo v2, "playlists"

    invoke-direct {v0, v1, v8, v2}, Lcom/vlingo/core/internal/lmttvocon/UploadType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/lmttvocon/UploadType;->PLAYLIST:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    .line 9
    new-instance v0, Lcom/vlingo/core/internal/lmttvocon/UploadType;

    const-string/jumbo v1, "APPLICATION"

    const/4 v2, 0x5

    const-string/jumbo v3, "application_names"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/lmttvocon/UploadType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/lmttvocon/UploadType;->APPLICATION:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    .line 3
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/vlingo/core/internal/lmttvocon/UploadType;

    sget-object v1, Lcom/vlingo/core/internal/lmttvocon/UploadType;->CONTACT:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/lmttvocon/UploadType;->SONG:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/lmttvocon/UploadType;->ARTIST:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/core/internal/lmttvocon/UploadType;->ALBUM:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vlingo/core/internal/lmttvocon/UploadType;->PLAYLIST:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/core/internal/lmttvocon/UploadType;->APPLICATION:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/lmttvocon/UploadType;->$VALUES:[Lcom/vlingo/core/internal/lmttvocon/UploadType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "slotName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 14
    iput-object p3, p0, Lcom/vlingo/core/internal/lmttvocon/UploadType;->slotName:Ljava/lang/String;

    .line 15
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/lmttvocon/UploadType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/vlingo/core/internal/lmttvocon/UploadType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/lmttvocon/UploadType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/lmttvocon/UploadType;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/vlingo/core/internal/lmttvocon/UploadType;->$VALUES:[Lcom/vlingo/core/internal/lmttvocon/UploadType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/lmttvocon/UploadType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/lmttvocon/UploadType;

    return-object v0
.end method


# virtual methods
.method public getSlotName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/UploadType;->slotName:Ljava/lang/String;

    return-object v0
.end method
