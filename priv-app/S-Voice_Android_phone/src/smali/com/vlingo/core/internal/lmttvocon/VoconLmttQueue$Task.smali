.class public abstract Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;
.super Ljava/lang/Object;
.source "VoconLmttQueue.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Task"
.end annotation


# instance fields
.field private queue:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;

.field private final startFromScratch:Z

.field private final uploadType:Lcom/vlingo/core/internal/lmttvocon/UploadType;


# direct methods
.method protected constructor <init>(Lcom/vlingo/core/internal/lmttvocon/UploadType;Z)V
    .locals 1
    .param p1, "uploadType"    # Lcom/vlingo/core/internal/lmttvocon/UploadType;
    .param p2, "startFromScratch"    # Z

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->queue:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;

    .line 116
    iput-object p1, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->uploadType:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    .line 117
    iput-boolean p2, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->startFromScratch:Z

    .line 118
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->removeFromQueue()V

    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;
    .param p1, "x1"    # Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->linkWithQueue(Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;)V

    return-void
.end method

.method private linkWithQueue(Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;)V
    .locals 0
    .param p1, "queueParam"    # Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->queue:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;

    .line 122
    return-void
.end method

.method private removeFromQueue()V
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->queue:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;

    .line 134
    invoke-virtual {p0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->cancel()V

    .line 135
    return-void
.end method


# virtual methods
.method protected abstract cancel()V
.end method

.method protected final getUploadType()Lcom/vlingo/core/internal/lmttvocon/UploadType;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->uploadType:Lcom/vlingo/core/internal/lmttvocon/UploadType;

    return-object v0
.end method

.method protected final isStartFromScratch()Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->startFromScratch:Z

    return v0
.end method

.method protected notifyCompleted()V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->queue:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;->queue:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;

    # invokes: Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->completeCurrentTask()V
    invoke-static {v0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->access$200(Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;)V

    .line 143
    :cond_0
    return-void
.end method
