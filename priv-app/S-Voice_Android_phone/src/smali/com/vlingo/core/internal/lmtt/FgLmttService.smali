.class public Lcom/vlingo/core/internal/lmtt/FgLmttService;
.super Lcom/vlingo/core/internal/lmtt/LmttServiceBase;
.source "FgLmttService.java"

# interfaces
.implements Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$Listener;
.implements Lcom/vlingo/core/internal/lmtt/LMTTManager$Listener;


# instance fields
.field private mLmttManager:Lcom/vlingo/core/internal/lmtt/LMTTManager;

.field private mService:Landroid/app/Service;

.field private mSyncManager:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

.field private mSyncTaskInProgress:Z

.field private mUpdateTaskInProgress:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 13
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;-><init>()V

    .line 15
    iput-object v1, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mLmttManager:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    .line 16
    iput-object v1, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mSyncManager:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    .line 18
    iput-boolean v0, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mUpdateTaskInProgress:Z

    .line 19
    iput-boolean v0, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mSyncTaskInProgress:Z

    return-void
.end method

.method private stopIfIdle()V
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mSyncTaskInProgress:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mUpdateTaskInProgress:Z

    if-nez v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mService:Landroid/app/Service;

    invoke-virtual {v0}, Landroid/app/Service;->stopSelf()V

    .line 116
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/app/Service;)V
    .locals 1
    .param p1, "service"    # Landroid/app/Service;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mService:Landroid/app/Service;

    .line 29
    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTManager;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/lmtt/LMTTManager;-><init>(Lcom/vlingo/core/internal/lmtt/LMTTManager$Listener;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mLmttManager:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    .line 30
    new-instance v0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;-><init>(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$Listener;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mSyncManager:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    .line 31
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 80
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mSyncManager:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mSyncManager:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;->destroy()V

    .line 82
    iput-object v1, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mSyncManager:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mLmttManager:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    if-eqz v0, :cond_1

    .line 86
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mLmttManager:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmtt/LMTTManager;->destroy()V

    .line 87
    iput-object v1, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mLmttManager:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    .line 90
    :cond_1
    iput-object v1, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mService:Landroid/app/Service;

    .line 91
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/FgLmttService;->destroyInstance()V

    .line 92
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 38
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v4, "com.vlingo.lmtt.action.LANGUAGE_CHANGE"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 41
    iput-boolean v6, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mUpdateTaskInProgress:Z

    .line 42
    iget-object v4, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mLmttManager:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    sget-object v5, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_LANGUAGE_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    invoke-virtual {v4, v5, v6, v7}, Lcom/vlingo/core/internal/lmtt/LMTTManager;->fireUpdate(Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;ZZ)V

    .line 72
    :cond_0
    :goto_0
    const/4 v4, 0x2

    return v4

    .line 43
    :cond_1
    const-string/jumbo v4, "com.vlingo.lmtt.action.SEND_UPDATE"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 45
    const-string/jumbo v4, "com.vlingo.lmtt.extra.SKIP_DELAY"

    invoke-virtual {p1, v4, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 46
    .local v2, "skipDelay":Z
    const-string/jumbo v4, "com.vlingo.lmtt.extra.CLEAR_LMTT"

    invoke-virtual {p1, v4, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 47
    .local v1, "clearLMTT":Z
    const-string/jumbo v4, "com.vlingo.lmtt.extra.LMTT_TYPE"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 51
    .local v3, "typeString":Ljava/lang/String;
    iput-boolean v6, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mSyncTaskInProgress:Z

    .line 52
    iget-object v4, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mSyncManager:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    invoke-virtual {v4, v2}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;->fireSync(Z)V

    .line 54
    if-eqz v3, :cond_2

    invoke-static {v3}, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->fromTypeString(Ljava/lang/String;)Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_CONTACT_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    if-ne v4, v5, :cond_3

    .line 57
    :cond_2
    iput-boolean v6, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mUpdateTaskInProgress:Z

    .line 58
    iget-object v4, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mLmttManager:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    sget-object v5, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_CONTACT_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    invoke-virtual {v4, v5, v2, v1}, Lcom/vlingo/core/internal/lmtt/LMTTManager;->fireUpdate(Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;ZZ)V

    .line 61
    :cond_3
    if-eqz v3, :cond_4

    invoke-static {v3}, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->fromTypeString(Ljava/lang/String;)Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_MUSIC_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    if-ne v4, v5, :cond_0

    .line 64
    :cond_4
    iput-boolean v6, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mUpdateTaskInProgress:Z

    .line 65
    iget-object v4, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mLmttManager:Lcom/vlingo/core/internal/lmtt/LMTTManager;

    sget-object v5, Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;->LMTT_MUSIC_UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;

    invoke-virtual {v4, v5, v2, v1}, Lcom/vlingo/core/internal/lmtt/LMTTManager;->fireUpdate(Lcom/vlingo/core/internal/lmtt/LMTTManager$LmttUpdateType;ZZ)V

    goto :goto_0
.end method

.method public onSyncTasksComplete()V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mSyncTaskInProgress:Z

    .line 99
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/FgLmttService;->stopIfIdle()V

    .line 100
    return-void
.end method

.method public onUpdateTasksComplete()V
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/lmtt/FgLmttService;->mUpdateTaskInProgress:Z

    .line 107
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/FgLmttService;->stopIfIdle()V

    .line 108
    return-void
.end method
