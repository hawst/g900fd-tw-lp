.class public final Lcom/vlingo/core/internal/debug/TestWrapper;
.super Ljava/lang/Object;
.source "TestWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/debug/TestWrapper$INormalWrapper;,
        Lcom/vlingo/core/internal/debug/TestWrapper$IAutomationTestWrapper;,
        Lcom/vlingo/core/internal/debug/TestWrapper$IIntegrationTestWrapper;
    }
.end annotation


# static fields
.field private static executeFromAutomationTests:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/core/internal/debug/TestWrapper;->executeFromAutomationTests:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    return-void
.end method

.method public static execute(Lcom/vlingo/core/internal/debug/TestWrapper$IAutomationTestWrapper;)V
    .locals 1
    .param p0, "callback"    # Lcom/vlingo/core/internal/debug/TestWrapper$IAutomationTestWrapper;

    .prologue
    .line 24
    sget-boolean v0, Lcom/vlingo/core/internal/debug/TestWrapper;->executeFromAutomationTests:Z

    if-eqz v0, :cond_0

    .line 25
    invoke-interface {p0}, Lcom/vlingo/core/internal/debug/TestWrapper$IAutomationTestWrapper;->execute()V

    .line 27
    :cond_0
    return-void
.end method

.method public static execute(Lcom/vlingo/core/internal/debug/TestWrapper$IAutomationTestWrapper;Lcom/vlingo/core/internal/debug/TestWrapper$INormalWrapper;)V
    .locals 1
    .param p0, "automationCallback"    # Lcom/vlingo/core/internal/debug/TestWrapper$IAutomationTestWrapper;
    .param p1, "normalCallback"    # Lcom/vlingo/core/internal/debug/TestWrapper$INormalWrapper;

    .prologue
    .line 30
    sget-boolean v0, Lcom/vlingo/core/internal/debug/TestWrapper;->executeFromAutomationTests:Z

    if-eqz v0, :cond_0

    .line 31
    invoke-interface {p0}, Lcom/vlingo/core/internal/debug/TestWrapper$IAutomationTestWrapper;->execute()V

    .line 35
    :goto_0
    return-void

    .line 33
    :cond_0
    invoke-interface {p1}, Lcom/vlingo/core/internal/debug/TestWrapper$INormalWrapper;->execute()V

    goto :goto_0
.end method

.method public static isExecuteFromAutomationTests()Z
    .locals 1

    .prologue
    .line 38
    sget-boolean v0, Lcom/vlingo/core/internal/debug/TestWrapper;->executeFromAutomationTests:Z

    return v0
.end method

.method public static setExecuteFromAutomationTests()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x1

    sput-boolean v0, Lcom/vlingo/core/internal/debug/TestWrapper;->executeFromAutomationTests:Z

    .line 43
    return-void
.end method

.method public static unsetExecuteFromAutomationTests()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/core/internal/debug/TestWrapper;->executeFromAutomationTests:Z

    .line 47
    return-void
.end method
