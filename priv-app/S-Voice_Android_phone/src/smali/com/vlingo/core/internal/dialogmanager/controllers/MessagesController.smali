.class public Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;
.super Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;
.source "MessagesController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# instance fields
.field private COMMA:Ljava/lang/String;

.field private PERIOD:Ljava/lang/String;

.field private SPACE:Ljava/lang/String;

.field private capped:Z

.field protected messageAlertQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation
.end field

.field private messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 43
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    .line 44
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_dot:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->PERIOD:Ljava/lang/String;

    .line 45
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_comma:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->COMMA:Ljava/lang/String;

    .line 48
    const-string/jumbo v0, " "

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->SPACE:Ljava/lang/String;

    .line 49
    return-void
.end method

.method private displayMessage()V
    .locals 14

    .prologue
    const/4 v12, 0x0

    const/4 v13, 0x1

    .line 152
    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-static {v8}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 153
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    check-cast v1, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 154
    .local v1, "messageAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getMessageBody(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/lang/String;

    move-result-object v0

    .line 155
    .local v0, "body":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v3

    .line 156
    .local v3, "name":Ljava/lang/String;
    const/4 v4, 0x0

    .line 157
    .local v4, "nameSpoken":Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 158
    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v3

    .line 160
    :cond_0
    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 162
    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    const-string/jumbo v8, ""

    invoke-direct {v2, v0, v3, v8}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    .local v2, "mtForWidget":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAttachments()I

    move-result v8

    invoke-virtual {v2, v8}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setAttachments(I)V

    .line 164
    new-instance v7, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v3, v8}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    .local v7, "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    invoke-virtual {v2, v7}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->addRecipient(Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;)V

    .line 166
    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getTimeStamp()J

    move-result-wide v8

    invoke-virtual {v2, v8, v9}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setTimeStamp(J)V

    .line 171
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_FROM_INTRO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v10, v13, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getContactName()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v12

    invoke-static {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->PERIOD:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->SPACE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v10, v13, [Ljava/lang/Object;

    aput-object v0, v10, v12

    invoke-static {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 174
    .local v5, "promptShown":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_FROM_INTRO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v10, v13, [Ljava/lang/Object;

    aput-object v4, v10, v12

    invoke-static {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->PERIOD:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->SPACE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->COMMA:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->SPACE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v10, v13, [Ljava/lang/Object;

    aput-object v0, v10, v12

    invoke-static {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 176
    .local v6, "promptSpoken":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v8

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    iget-object v10, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v10}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getId()J

    move-result-wide v10

    iget-object v12, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v12}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getType()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->markAsRead(Landroid/content/Context;JLjava/lang/String;)V

    .line 180
    sget-object v8, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SAFEREADER_POSTMSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v8}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v8

    invoke-virtual {p0, v5, v6, v13, v8}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 187
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v8

    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MessageReadback:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10, v2, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 189
    .end local v0    # "body":Ljava/lang/String;
    .end local v1    # "messageAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v2    # "mtForWidget":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "nameSpoken":Ljava/lang/String;
    .end local v5    # "promptShown":Ljava/lang/String;
    .end local v6    # "promptSpoken":Ljava/lang/String;
    .end local v7    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    :cond_1
    return-void
.end method

.method private displayMultipleMessages(Ljava/util/Queue;Z)V
    .locals 6
    .param p2, "requestCollapse"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p1, "queue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    const/4 v3, 0x1

    .line 246
    if-eqz p2, :cond_1

    const-string/jumbo v2, "multi.widget.client.collapse"

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    move v1, v3

    .line 247
    .local v1, "collapse":Z
    :goto_0
    new-instance v2, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    invoke-direct {v2, p1, v1}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;-><init>(Ljava/util/Queue;Z)V

    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    .line 248
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->getSummaries()Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 250
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->getSummaries()Ljava/util/LinkedList;

    move-result-object v0

    .line 251
    .local v0, "a":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/MessageSenderSummary;>;"
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->getSummaries()Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/messages/MessageSenderSummary;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/MessageSenderSummary;->getAlert()Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 252
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v2, :cond_0

    .line 253
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->getSummaries()Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-ne v2, v3, :cond_2

    .line 254
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->systemTurnForAllFromSender(Ljava/util/Queue;)V

    .line 258
    :goto_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleMessageShowWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->getSummaries()Ljava/util/LinkedList;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 261
    .end local v0    # "a":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/MessageSenderSummary;>;"
    :cond_0
    return-void

    .line 246
    .end local v1    # "collapse":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 256
    .restart local v0    # "a":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/MessageSenderSummary;>;"
    .restart local v1    # "collapse":Z
    :cond_2
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->systemTurnForAllMesages(Ljava/util/Queue;)V

    goto :goto_1
.end method

.method private displayMultipleMessagesFromSender(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)V
    .locals 3
    .param p1, "aSMSMMSMessage"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .prologue
    .line 147
    invoke-virtual {p1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageAlertQueue:Ljava/util/LinkedList;

    invoke-direct {p0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getNewQueueForSender(Ljava/lang/String;Ljava/util/Queue;)Ljava/util/Queue;

    move-result-object v0

    .line 148
    .local v0, "newAlertQueue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->displayMultipleMessages(Ljava/util/Queue;Z)V

    .line 149
    return-void
.end method

.method private getMessageBody(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/lang/String;
    .locals 1
    .param p1, "alert"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .prologue
    .line 303
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getDisplayableMessageText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getMessagesForWidget(Ljava/util/Queue;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;)",
            "Ljava/util/List",
            "<+",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 307
    .local p1, "alerts":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 308
    .local v4, "msgList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageType;>;"
    if-eqz p1, :cond_4

    .line 309
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Queue;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_4

    .line 310
    invoke-interface {p1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 311
    .local v1, "alert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->isSMS()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->isMMS()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 312
    :cond_1
    move-object v3, v1

    .line 314
    .local v3, "messageAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getMessageBody(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/lang/String;

    move-result-object v2

    .line 315
    .local v2, "displayBody":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderName()Ljava/lang/String;

    move-result-object v5

    .line 318
    .local v5, "name":Ljava/lang/String;
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    const-string/jumbo v6, ""

    invoke-direct {v0, v2, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    .local v0, "aMessageType":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAttachments()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setAttachments(I)V

    .line 320
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getId()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setId(J)V

    .line 321
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getType()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "SMS"

    if-ne v6, v7, :cond_3

    .line 322
    const-string/jumbo v6, "SMS"

    invoke-virtual {v0, v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setType(Ljava/lang/String;)V

    .line 327
    :cond_2
    :goto_1
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getTimeStamp()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setTimeStamp(J)V

    .line 329
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 324
    :cond_3
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getType()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "MMS"

    if-ne v6, v7, :cond_2

    .line 325
    const-string/jumbo v6, "MMS"

    invoke-virtual {v0, v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setType(Ljava/lang/String;)V

    goto :goto_1

    .line 333
    .end local v0    # "aMessageType":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    .end local v1    # "alert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v2    # "displayBody":Ljava/lang/String;
    .end local v3    # "messageAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v5    # "name":Ljava/lang/String;
    :cond_4
    return-object v4
.end method

.method private getNewQueueForSender(Ljava/lang/String;Ljava/util/Queue;)Ljava/util/Queue;
    .locals 4
    .param p1, "senderName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Queue",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;)",
            "Ljava/util/Queue",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    .local p2, "oldQueue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    invoke-interface {p2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 136
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 138
    .local v2, "newQueue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 139
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 140
    .local v0, "alert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 141
    invoke-interface {v2, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 143
    .end local v0    # "alert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :cond_1
    return-object v2
.end method

.method private getSenderText(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/lang/String;
    .locals 2
    .param p1, "alert"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .prologue
    .line 289
    if-nez p1, :cond_0

    .line 290
    const-string/jumbo v1, ""

    .line 297
    :goto_0
    return-object v1

    .line 292
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 294
    .local v0, "name":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 295
    invoke-virtual {p1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 297
    :cond_1
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getSenderTtsText(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/lang/String;
    .locals 3
    .param p1, "alert"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .prologue
    .line 275
    if-nez p1, :cond_0

    .line 276
    const-string/jumbo v1, ""

    .line 285
    :goto_0
    return-object v1

    .line 278
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 279
    .local v0, "name":Ljava/lang/String;
    const/4 v1, 0x0

    .line 280
    .local v1, "nameSpoken":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 281
    invoke-virtual {p1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 283
    :cond_1
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 284
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 285
    goto :goto_0
.end method

.method private handleShowUnreadMessagesWidget()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 337
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageAlertQueue:Ljava/util/LinkedList;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageAlertQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 340
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageAlertQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 341
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageAlertQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-le v1, v2, :cond_1

    .line 342
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageAlertQueue:Ljava/util/LinkedList;

    invoke-direct {p0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->displayMultipleMessages(Ljava/util/Queue;Z)V

    .line 359
    :cond_0
    :goto_0
    return-void

    .line 344
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageAlertQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 345
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->displayMessage()V

    goto :goto_0

    .line 349
    :cond_2
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_none:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 350
    .local v0, "errorMsg":Ljava/lang/String;
    invoke-virtual {p0, v0, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private markAsRead(Ljava/util/Queue;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 264
    .local p1, "alertQueue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    invoke-interface {p1}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 266
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v3

    .line 267
    .local v3, "provider":Lcom/vlingo/core/internal/messages/SMSMMSProvider;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 268
    .local v1, "context":Landroid/content/Context;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 269
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 270
    .local v0, "alert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getId()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v1, v4, v5, v6}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->markAsRead(Landroid/content/Context;JLjava/lang/String;)V

    goto :goto_0

    .line 272
    .end local v0    # "alert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :cond_0
    return-void
.end method

.method private systemTurnForAllFromSender(Ljava/util/Queue;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "queue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 195
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->getSummaries()Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    if-gtz v5, :cond_0

    .line 216
    :goto_0
    return-void

    .line 198
    :cond_0
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_multi_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v7, v10, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Queue;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v7, v8

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->getSummaries()Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/messages/MessageSenderSummary;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/messages/MessageSenderSummary;->getAlert()Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getSenderText(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v9

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 201
    .local v1, "displayString":Ljava/lang/String;
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_multi_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v7, v10, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Queue;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v7, v8

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->getSummaries()Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/messages/MessageSenderSummary;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/messages/MessageSenderSummary;->getAlert()Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getSenderTtsText(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v9

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 205
    .local v4, "msgAnnounceString":Ljava/lang/String;
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->getSummaries()Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 208
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/internal/messages/MessageSenderSummary;>;"
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 209
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/messages/MessageSenderSummary;

    .line 210
    .local v3, "messageSenderSummary":Lcom/vlingo/core/internal/messages/MessageSenderSummary;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/messages/MessageSenderSummary;->getAlert()Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    move-result-object v0

    .line 211
    .local v0, "alert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->COMMA:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->SPACE:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->PERIOD:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->SPACE:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 212
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_alert_message:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v7, v8, [Ljava/lang/Object;

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->PERIOD:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->SPACE:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getMessageBody(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 214
    .end local v0    # "alert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v3    # "messageSenderSummary":Lcom/vlingo/core/internal/messages/MessageSenderSummary;
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->PERIOD:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 215
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5, v1, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private systemTurnForAllMesages(Ljava/util/Queue;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "queue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 219
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->getSummaries()Ljava/util/LinkedList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v6

    if-gtz v6, :cond_0

    .line 243
    :goto_0
    return-void

    .line 222
    :cond_0
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_MULTIPLE_NEW_MESSAGES:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v7, v10, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Queue;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 223
    .local v2, "displayString":Ljava/lang/String;
    move-object v5, v2

    .line 225
    .local v5, "msgAnnounceString":Ljava/lang/String;
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageSenderSummaryList:Lcom/vlingo/core/internal/messages/MessageSenderSummaries;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/messages/MessageSenderSummaries;->getSummaries()Ljava/util/LinkedList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 229
    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/internal/messages/MessageSenderSummary;>;"
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 230
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/messages/MessageSenderSummary;

    .line 231
    .local v4, "messageSenderSummary":Lcom/vlingo/core/internal/messages/MessageSenderSummary;
    invoke-virtual {v4}, Lcom/vlingo/core/internal/messages/MessageSenderSummary;->getCount()I

    move-result v1

    .line 232
    .local v1, "count":I
    invoke-virtual {v4}, Lcom/vlingo/core/internal/messages/MessageSenderSummary;->getAlert()Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    move-result-object v0

    .line 233
    .local v0, "alert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->COMMA:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->SPACE:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->PERIOD:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->SPACE:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 234
    if-ne v1, v10, :cond_1

    .line 235
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_single_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v8, v12, [Ljava/lang/Object;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getSenderTtsText(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v7, v8}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 239
    :goto_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->PERIOD:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->SPACE:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->COMMA:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->SPACE:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_preface:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v8, v11, [Ljava/lang/Object;

    invoke-static {v7, v8}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->SPACE:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getMessageBody(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 237
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_multi_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v8, v12, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getSenderTtsText(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v7, v8}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    .line 241
    .end local v0    # "alert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v1    # "count":I
    .end local v4    # "messageSenderSummary":Lcom/vlingo/core/internal/messages/MessageSenderSummary;
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->PERIOD:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 242
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v6

    invoke-interface {v6, v2, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 2
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 65
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/controllers/SafeReaderController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 66
    if-eqz p1, :cond_0

    .line 69
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "ShowUnreadMessagesWidget"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->handleShowUnreadMessagesWidget()V

    .line 80
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 74
    :cond_1
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "ShowWCISWidget"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->handleShowUnreadMessagesWidget()V

    goto :goto_0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 85
    if-eqz p1, :cond_1

    .line 86
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 89
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v7

    invoke-interface {v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 90
    const-string/jumbo v7, "id"

    const-wide/16 v8, -0x1

    invoke-virtual {p1, v7, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 91
    .local v4, "id":J
    const-string/jumbo v7, "item_count"

    const/4 v8, -0x1

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 93
    .local v1, "count":I
    const-string/jumbo v7, "message_type"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 94
    .local v6, "type":Ljava/lang/String;
    const-string/jumbo v7, "from_read_messages"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 96
    .local v3, "fromReadMessagesString":Ljava/lang/String;
    const/4 v2, 0x0

    .line 97
    .local v2, "fromReadMessages":Z
    if-eqz v3, :cond_0

    .line 98
    const-string/jumbo v7, "true"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    .line 101
    :cond_0
    const-string/jumbo v7, "SMS"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string/jumbo v7, "MMS"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 128
    .end local v1    # "count":I
    .end local v2    # "fromReadMessages":Z
    .end local v3    # "fromReadMessagesString":Ljava/lang/String;
    .end local v4    # "id":J
    .end local v6    # "type":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 106
    .restart local v1    # "count":I
    .restart local v2    # "fromReadMessages":Z
    .restart local v3    # "fromReadMessagesString":Ljava/lang/String;
    .restart local v4    # "id":J
    .restart local v6    # "type":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v7

    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageAlertQueue:Ljava/util/LinkedList;

    invoke-virtual {v7, v4, v5, v6, v8}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->findbyId(JLjava/lang/String;Ljava/util/LinkedList;)Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    move-result-object v0

    .line 107
    .local v0, "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    if-eqz v0, :cond_1

    .line 112
    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 113
    const/4 v7, 0x1

    if-le v1, v7, :cond_3

    if-eqz v2, :cond_3

    .line 115
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->displayMultipleMessagesFromSender(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)V

    goto :goto_0

    .line 118
    :cond_3
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->displayMessage()V

    goto :goto_0

    .line 120
    .end local v0    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v1    # "count":I
    .end local v2    # "fromReadMessages":Z
    .end local v3    # "fromReadMessagesString":Ljava/lang/String;
    .end local v4    # "id":J
    .end local v6    # "type":Ljava/lang/String;
    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "com.vlingo.core.internal.dialogmanager.Call"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 121
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v7

    invoke-interface {v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 122
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->callSender()V

    goto :goto_0

    .line 123
    :cond_5
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "com.vlingo.core.internal.dialogmanager.Reply"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 124
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v7

    invoke-interface {v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 125
    const-string/jumbo v7, ""

    invoke-virtual {p0, v7}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->reply(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setCapped(Z)V
    .locals 0
    .param p1, "capped"    # Z

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->capped:Z

    .line 58
    return-void
.end method

.method public setMessageAlertQueue(Ljava/util/LinkedList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p1, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->messageAlertQueue:Ljava/util/LinkedList;

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->capped:Z

    .line 54
    return-void
.end method
