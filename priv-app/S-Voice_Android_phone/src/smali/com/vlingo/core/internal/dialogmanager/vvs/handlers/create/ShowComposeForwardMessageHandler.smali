.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeForwardMessageHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;
.source "ShowComposeForwardMessageHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 2
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeForwardMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeForwardMessageHandler;->messages:Ljava/util/LinkedList;

    .line 14
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v0

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 20
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeMessageHandler;->getText()Ljava/lang/String;

    .line 21
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeForwardMessageHandler;->messages:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/create/ShowComposeForwardMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getDisplayableMessageText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
