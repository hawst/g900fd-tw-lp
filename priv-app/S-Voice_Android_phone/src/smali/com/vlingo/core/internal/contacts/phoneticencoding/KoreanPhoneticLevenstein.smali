.class public Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticLevenstein;
.super Ljava/lang/Object;
.source "KoreanPhoneticLevenstein.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6
    const-class v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticLevenstein;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticLevenstein;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static levenshtein(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 15
    .param p0, "seq1"    # Ljava/lang/String;
    .param p1, "seq2"    # Ljava/lang/String;
    .param p2, "maxDist"    # I

    .prologue
    .line 34
    const/4 v3, -0x2

    .line 39
    .local v3, "dist":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    .line 40
    .local v7, "len1":I
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v8

    .line 42
    .local v8, "len2":I
    if-ge v7, v8, :cond_0

    .line 43
    move-object v10, p0

    .line 44
    .local v10, "tmp":Ljava/lang/String;
    move-object/from16 p0, p1

    .line 45
    move-object/from16 p1, v10

    .line 47
    move v11, v7

    .line 48
    .local v11, "x":I
    move v7, v8

    .line 49
    move v8, v11

    .line 53
    .end local v10    # "tmp":Ljava/lang/String;
    .end local v11    # "x":I
    :cond_0
    if-ltz p2, :cond_2

    sub-int v12, v7, v8

    move/from16 v0, p2

    if-le v12, v0, :cond_2

    .line 54
    const/4 v8, -0x1

    .line 90
    .end local v8    # "len2":I
    :cond_1
    :goto_0
    return v8

    .line 56
    .restart local v8    # "len2":I
    :cond_2
    if-eqz v7, :cond_1

    .line 58
    if-nez v8, :cond_3

    move v8, v7

    .line 59
    goto :goto_0

    .line 62
    :cond_3
    add-int/lit8 v12, v8, 0x1

    new-array v1, v12, [I

    .line 64
    .local v1, "column":[I
    const/4 v5, 0x1

    .local v5, "j":I
    :goto_1
    if-gt v5, v8, :cond_4

    .line 65
    aput v5, v1, v5

    .line 64
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 67
    :cond_4
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_2
    if-gt v4, v7, :cond_8

    .line 68
    const/4 v12, 0x0

    aput v4, v1, v12

    .line 69
    const/4 v5, 0x1

    add-int/lit8 v6, v4, -0x1

    .local v6, "last":I
    :goto_3
    if-gt v5, v8, :cond_6

    .line 70
    aget v9, v1, v5

    .line 71
    .local v9, "old":I
    add-int/lit8 v12, v4, -0x1

    invoke-virtual {p0, v12}, Ljava/lang/String;->charAt(I)C

    move-result v12

    add-int/lit8 v13, v5, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->charAt(I)C

    move-result v13

    if-eq v12, v13, :cond_5

    const/4 v2, 0x1

    .line 73
    .local v2, "cost":I
    :goto_4
    aget v12, v1, v5

    add-int/lit8 v12, v12, 0x1

    add-int/lit8 v13, v5, -0x1

    aget v13, v1, v13

    add-int/lit8 v13, v13, 0x1

    add-int v14, v6, v2

    invoke-static {v12, v13, v14}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticLevenstein;->min3(III)I

    move-result v12

    aput v12, v1, v5

    .line 78
    move v6, v9

    .line 69
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 71
    .end local v2    # "cost":I
    :cond_5
    const/4 v2, 0x0

    goto :goto_4

    .line 80
    .end local v9    # "old":I
    :cond_6
    if-ltz p2, :cond_7

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticLevenstein;->minimum([I)I

    move-result v12

    move/from16 v0, p2

    if-le v12, v0, :cond_7

    .line 81
    const/4 v8, -0x1

    goto :goto_0

    .line 67
    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 85
    .end local v6    # "last":I
    :cond_8
    aget v3, v1, v8

    .line 88
    if-ltz p2, :cond_9

    move/from16 v0, p2

    if-le v3, v0, :cond_9

    .line 89
    const/4 v8, -0x1

    goto :goto_0

    :cond_9
    move v8, v3

    .line 90
    goto :goto_0
.end method

.method public static max3(III)I
    .locals 0
    .param p0, "a"    # I
    .param p1, "b"    # I
    .param p2, "c"    # I

    .prologue
    .line 13
    if-le p0, p1, :cond_1

    if-le p0, p2, :cond_0

    move p2, p0

    .end local p2    # "c":I
    :cond_0
    :goto_0
    return p2

    .restart local p2    # "c":I
    :cond_1
    if-le p1, p2, :cond_0

    move p2, p1

    goto :goto_0
.end method

.method public static min3(III)I
    .locals 0
    .param p0, "a"    # I
    .param p1, "b"    # I
    .param p2, "c"    # I

    .prologue
    .line 9
    if-ge p0, p1, :cond_1

    if-ge p0, p2, :cond_0

    move p2, p0

    .end local p2    # "c":I
    :cond_0
    :goto_0
    return p2

    .restart local p2    # "c":I
    :cond_1
    if-ge p1, p2, :cond_0

    move p2, p1

    goto :goto_0
.end method

.method static minimum([I)I
    .locals 4
    .param p0, "column"    # [I

    .prologue
    .line 19
    sget-boolean v3, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticLevenstein;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    array-length v3, p0

    if-gtz v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 20
    :cond_0
    array-length v0, p0

    .line 21
    .local v0, "len":I
    add-int/lit8 v0, v0, -0x1

    aget v2, p0, v0

    .local v2, "min":I
    move v1, v0

    .line 22
    .end local v0    # "len":I
    .local v1, "len":I
    :goto_0
    add-int/lit8 v0, v1, -0x1

    .end local v1    # "len":I
    .restart local v0    # "len":I
    if-ltz v1, :cond_1

    .line 23
    aget v3, p0, v0

    if-ge v3, v2, :cond_2

    .line 24
    aget v2, p0, v0

    move v1, v0

    .end local v0    # "len":I
    .restart local v1    # "len":I
    goto :goto_0

    .line 27
    .end local v1    # "len":I
    .restart local v0    # "len":I
    :cond_1
    return v2

    :cond_2
    move v1, v0

    .end local v0    # "len":I
    .restart local v1    # "len":I
    goto :goto_0
.end method

.method public static nlevenshtein(Ljava/lang/String;Ljava/lang/String;S)D
    .locals 26
    .param p0, "seq1"    # Ljava/lang/String;
    .param p1, "seq2"    # Ljava/lang/String;
    .param p2, "method"    # S

    .prologue
    .line 109
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v22

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_0

    .line 110
    move-object/from16 v21, p0

    .line 111
    .local v21, "tmp":Ljava/lang/String;
    move-object/from16 p0, p1

    .line 112
    move-object/from16 p1, v21

    .line 114
    .end local v21    # "tmp":Ljava/lang/String;
    :cond_0
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v12

    .local v12, "len1":I
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v13

    .line 116
    .local v13, "len2":I
    sget-boolean v22, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticLevenstein;->$assertionsDisabled:Z

    if-nez v22, :cond_1

    if-ge v12, v13, :cond_1

    new-instance v22, Ljava/lang/AssertionError;

    invoke-direct/range {v22 .. v22}, Ljava/lang/AssertionError;-><init>()V

    throw v22

    .line 118
    :cond_1
    if-nez v12, :cond_2

    .line 119
    const-wide/16 v22, 0x0

    .line 168
    :goto_0
    return-wide v22

    .line 120
    :cond_2
    if-nez v13, :cond_3

    .line 121
    const-wide/high16 v22, 0x3ff0000000000000L    # 1.0

    goto :goto_0

    .line 123
    :cond_3
    const/16 v22, 0x1

    move/from16 v0, p2

    move/from16 v1, v22

    if-ne v0, v1, :cond_5

    .line 124
    const/16 v22, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticLevenstein;->levenshtein(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v5

    .line 125
    .local v5, "fdist":I
    if-gez v5, :cond_4

    .line 126
    int-to-double v0, v5

    move-wide/from16 v22, v0

    goto :goto_0

    .line 127
    :cond_4
    int-to-double v0, v5

    move-wide/from16 v22, v0

    int-to-double v0, v12

    move-wide/from16 v24, v0

    div-double v22, v22, v24

    goto :goto_0

    .line 130
    .end local v5    # "fdist":I
    :cond_5
    add-int/lit8 v22, v13, 0x1

    move/from16 v0, v22

    new-array v3, v0, [I

    .line 131
    .local v3, "column":[I
    add-int/lit8 v22, v13, 0x1

    move/from16 v0, v22

    new-array v14, v0, [I

    .line 134
    .local v14, "length":[I
    const/4 v9, 0x1

    .local v9, "j":I
    :goto_1
    if-gt v9, v13, :cond_6

    .line 135
    aput v9, v14, v9

    .line 136
    aget v22, v14, v9

    aput v22, v3, v9

    .line 134
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 139
    :cond_6
    const/4 v7, 0x1

    .local v7, "i":I
    :goto_2
    if-gt v7, v12, :cond_c

    .line 140
    const/16 v22, 0x0

    aput v7, v14, v22

    .line 141
    const/16 v22, 0x0

    const/16 v23, 0x0

    aget v23, v14, v23

    aput v23, v3, v22

    .line 143
    add-int/lit8 v16, v7, -0x1

    .line 144
    .local v16, "llast":I
    const/4 v9, 0x1

    move/from16 v10, v16

    .local v10, "last":I
    :goto_3
    if-gt v9, v13, :cond_b

    .line 147
    aget v19, v3, v9

    .line 148
    .local v19, "old":I
    add-int/lit8 v22, v9, -0x1

    aget v22, v3, v22

    add-int/lit8 v8, v22, 0x1

    .line 149
    .local v8, "ic":I
    aget v22, v3, v9

    add-int/lit8 v4, v22, 0x1

    .line 150
    .local v4, "dc":I
    add-int/lit8 v22, v7, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v22

    add-int/lit8 v23, v9, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_7

    const/16 v22, 0x1

    :goto_4
    add-int v20, v10, v22

    .line 151
    .local v20, "rc":I
    move/from16 v0, v20

    invoke-static {v8, v4, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticLevenstein;->min3(III)I

    move-result v22

    aput v22, v3, v9

    .line 152
    move/from16 v10, v19

    .line 155
    aget v17, v14, v9

    .line 156
    .local v17, "lold":I
    aget v22, v3, v9

    move/from16 v0, v22

    if-ne v8, v0, :cond_8

    add-int/lit8 v22, v9, -0x1

    aget v22, v14, v22

    add-int/lit8 v15, v22, 0x1

    .line 157
    .local v15, "lic":I
    :goto_5
    aget v22, v3, v9

    move/from16 v0, v22

    if-ne v4, v0, :cond_9

    aget v22, v14, v9

    add-int/lit8 v11, v22, 0x1

    .line 158
    .local v11, "ldc":I
    :goto_6
    aget v22, v3, v9

    move/from16 v0, v20

    move/from16 v1, v22

    if-ne v0, v1, :cond_a

    add-int/lit8 v18, v16, 0x1

    .line 159
    .local v18, "lrc":I
    :goto_7
    move/from16 v0, v18

    invoke-static {v15, v11, v0}, Lcom/vlingo/core/internal/contacts/phoneticencoding/KoreanPhoneticLevenstein;->max3(III)I

    move-result v22

    aput v22, v14, v9

    .line 160
    move/from16 v16, v17

    .line 144
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 150
    .end local v11    # "ldc":I
    .end local v15    # "lic":I
    .end local v17    # "lold":I
    .end local v18    # "lrc":I
    .end local v20    # "rc":I
    :cond_7
    const/16 v22, 0x0

    goto :goto_4

    .line 156
    .restart local v17    # "lold":I
    .restart local v20    # "rc":I
    :cond_8
    const/4 v15, 0x0

    goto :goto_5

    .line 157
    .restart local v15    # "lic":I
    :cond_9
    const/4 v11, 0x0

    goto :goto_6

    .line 158
    .restart local v11    # "ldc":I
    :cond_a
    const/16 v18, 0x0

    goto :goto_7

    .line 139
    .end local v4    # "dc":I
    .end local v8    # "ic":I
    .end local v11    # "ldc":I
    .end local v15    # "lic":I
    .end local v17    # "lold":I
    .end local v19    # "old":I
    .end local v20    # "rc":I
    :cond_b
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2

    .line 164
    .end local v10    # "last":I
    .end local v16    # "llast":I
    :cond_c
    aget v5, v3, v13

    .line 165
    .restart local v5    # "fdist":I
    aget v6, v14, v13

    .line 168
    .local v6, "flen":I
    int-to-double v0, v5

    move-wide/from16 v22, v0

    int-to-double v0, v6

    move-wide/from16 v24, v0

    div-double v22, v22, v24

    goto/16 :goto_0
.end method
