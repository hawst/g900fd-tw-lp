.class final enum Lcom/vlingo/core/internal/lmtt/LMTTChunkState;
.super Ljava/lang/Enum;
.source "LMTTChunkState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/lmtt/LMTTChunkState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

.field public static final enum ERRORED_OUT_STATE:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

.field public static final enum FINISHED_STATE:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

.field public static final enum START_STATE:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

.field public static final enum STATE_RETRY:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    const-string/jumbo v1, "ERRORED_OUT_STATE"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->ERRORED_OUT_STATE:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    .line 5
    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    const-string/jumbo v1, "FINISHED_STATE"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->FINISHED_STATE:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    .line 6
    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    const-string/jumbo v1, "STATE_RETRY"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->STATE_RETRY:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    .line 7
    new-instance v0, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    const-string/jumbo v1, "START_STATE"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->START_STATE:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    .line 3
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->ERRORED_OUT_STATE:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->FINISHED_STATE:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->STATE_RETRY:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->START_STATE:Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->$VALUES:[Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/lmtt/LMTTChunkState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/lmtt/LMTTChunkState;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->$VALUES:[Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/lmtt/LMTTChunkState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    return-object v0
.end method
