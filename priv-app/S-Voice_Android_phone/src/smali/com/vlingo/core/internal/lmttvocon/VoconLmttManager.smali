.class public final Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;
.super Ljava/lang/Object;
.source "VoconLmttManager.java"


# instance fields
.field private final contentChangesObservers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;",
            ">;"
        }
    .end annotation
.end field

.field private final contentProviders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/internal/lmttvocon/UploadType;",
            "Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final grammarUpdater:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater;

.field private final supportedUploadTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/core/internal/lmttvocon/UploadType;",
            ">;"
        }
    .end annotation
.end field

.field private final taskQueue:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;

.field private final uploadContext:Lcom/vlingo/core/internal/lmttvocon/UploadContext;

.field private final uploadedChecksumProvider:Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/Set;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/core/internal/lmttvocon/UploadType;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p2, "supportedUploadTypes":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/lmttvocon/UploadType;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v6, Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    invoke-direct {v6, p1}, Lcom/vlingo/core/internal/lmttvocon/UploadContext;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->uploadContext:Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    .line 43
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->supportedUploadTypes:Ljava/util/Set;

    .line 44
    new-instance v2, Ljava/util/HashSet;

    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v6

    invoke-direct {v2, v6}, Ljava/util/HashSet;-><init>(I)V

    .line 45
    .local v2, "observers":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;>;"
    new-instance v4, Ljava/util/HashMap;

    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v6

    invoke-direct {v4, v6}, Ljava/util/HashMap;-><init>(I)V

    .line 46
    .local v4, "providers":Ljava/util/Map;, "Ljava/util/Map<Lcom/vlingo/core/internal/lmttvocon/UploadType;Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentProvider;>;"
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/lmttvocon/UploadType;

    .line 47
    .local v5, "supportedUploadType":Lcom/vlingo/core/internal/lmttvocon/UploadType;
    iget-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->uploadContext:Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    invoke-static {v6, v5}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserverFactory;->create(Lcom/vlingo/core/internal/lmttvocon/UploadContext;Lcom/vlingo/core/internal/lmttvocon/UploadType;)Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;

    move-result-object v1

    .line 48
    .local v1, "observer":Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;
    if-eqz v1, :cond_1

    .line 49
    new-instance v6, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager$1;

    invoke-direct {v6, p0, v5}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager$1;-><init>(Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;Lcom/vlingo/core/internal/lmttvocon/UploadType;)V

    invoke-interface {v1, v6}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;->watchForContentUpdates(Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserverListener;)V

    .line 55
    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 57
    :cond_1
    iget-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->uploadContext:Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    invoke-static {v6, v5}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentProviderFactory;->create(Lcom/vlingo/core/internal/lmttvocon/UploadContext;Lcom/vlingo/core/internal/lmttvocon/UploadType;)Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentProvider;

    move-result-object v3

    .line 58
    .local v3, "provider":Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentProvider;
    if-eqz v3, :cond_0

    .line 59
    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 62
    .end local v1    # "observer":Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;
    .end local v3    # "provider":Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentProvider;
    .end local v5    # "supportedUploadType":Lcom/vlingo/core/internal/lmttvocon/UploadType;
    :cond_2
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->contentChangesObservers:Ljava/util/Set;

    .line 63
    invoke-static {v4}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->contentProviders:Ljava/util/Map;

    .line 64
    new-instance v6, Lcom/vlingo/core/internal/lmttvocon/vocon/NoRemoveTerminalGrammarUpdaterStrategy;

    invoke-direct {v6}, Lcom/vlingo/core/internal/lmttvocon/vocon/NoRemoveTerminalGrammarUpdaterStrategy;-><init>()V

    invoke-static {v6}, Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdaterFactory;->create(Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdaterStrategy;)Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->grammarUpdater:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater;

    .line 65
    iget-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->uploadContext:Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    invoke-static {v6}, Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProviderFactory;->create(Lcom/vlingo/core/internal/lmttvocon/UploadContext;)Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->uploadedChecksumProvider:Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;

    .line 66
    new-instance v6, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;

    iget-object v7, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->uploadContext:Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/lmttvocon/UploadContext;->getWorkerHandler()Landroid/os/Handler;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;-><init>(Landroid/os/Handler;)V

    iput-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->taskQueue:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;

    .line 67
    return-void
.end method


# virtual methods
.method public getTaskQueue()Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->taskQueue:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;

    return-object v0
.end method

.method public release()V
    .locals 3

    .prologue
    .line 72
    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->taskQueue:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->cancelAll()V

    .line 73
    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->contentChangesObservers:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;

    .line 74
    .local v1, "observer":Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;
    invoke-interface {v1}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;->stopWatchingForContentUpdates()V

    goto :goto_0

    .line 76
    .end local v1    # "observer":Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;
    :cond_0
    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->uploadContext:Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/lmttvocon/UploadContext;->release()V

    .line 77
    return-void
.end method

.method public resetUploadDelays()V
    .locals 3

    .prologue
    .line 94
    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->contentChangesObservers:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;

    .line 95
    .local v1, "observer":Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;
    invoke-interface {v1}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;->resetDelay()V

    goto :goto_0

    .line 97
    .end local v1    # "observer":Lcom/vlingo/core/internal/lmttvocon/contentobserver/ContentChangesObserver;
    :cond_0
    return-void
.end method

.method public upload(Lcom/vlingo/core/internal/lmttvocon/UploadType;Z)V
    .locals 7
    .param p1, "uploadType"    # Lcom/vlingo/core/internal/lmttvocon/UploadType;
    .param p2, "startFromScratch"    # Z

    .prologue
    .line 88
    iget-object v6, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->taskQueue:Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;

    new-instance v0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;

    iget-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->contentProviders:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentProvider;

    iget-object v4, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->uploadedChecksumProvider:Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;

    iget-object v5, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->grammarUpdater:Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater;

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttTask;-><init>(Lcom/vlingo/core/internal/lmttvocon/UploadType;ZLcom/vlingo/core/internal/lmttvocon/contentprovider/ContentProvider;Lcom/vlingo/core/internal/lmttvocon/checksumprovider/UploadedChecksumProvider;Lcom/vlingo/core/internal/lmttvocon/vocon/GrammarUpdater;)V

    invoke-virtual {v6, v0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue;->addTask(Lcom/vlingo/core/internal/lmttvocon/VoconLmttQueue$Task;)V

    .line 89
    return-void
.end method

.method public uploadAll(Z)V
    .locals 3
    .param p1, "startFromScratch"    # Z

    .prologue
    .line 82
    iget-object v2, p0, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->supportedUploadTypes:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/lmttvocon/UploadType;

    .line 83
    .local v1, "uploadType":Lcom/vlingo/core/internal/lmttvocon/UploadType;
    invoke-virtual {p0, v1, p1}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttManager;->upload(Lcom/vlingo/core/internal/lmttvocon/UploadType;Z)V

    goto :goto_0

    .line 85
    .end local v1    # "uploadType":Lcom/vlingo/core/internal/lmttvocon/UploadType;
    :cond_0
    return-void
.end method
