.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;
.super Ljava/lang/Thread;
.source "AnswerQuestionHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ParsingThread"
.end annotation


# instance fields
.field protected mAnswer:Lcom/vlingo/core/internal/questions/parser/ServerResponse;

.field protected mAnswerQuestionHandler:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;",
            ">;"
        }
    .end annotation
.end field

.field protected mHandler:Landroid/os/Handler;

.field protected mListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;Lcom/vlingo/core/internal/questions/parser/ServerResponse;Landroid/os/Handler;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V
    .locals 1
    .param p1, "answerQuestionHandler"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;
    .param p2, "answer"    # Lcom/vlingo/core/internal/questions/parser/ServerResponse;
    .param p3, "handler"    # Landroid/os/Handler;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 107
    const-string/jumbo v0, "Answer.Parsing"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 108
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->mAnswerQuestionHandler:Ljava/lang/ref/WeakReference;

    .line 109
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->mAnswer:Lcom/vlingo/core/internal/questions/parser/ServerResponse;

    .line 110
    iput-object p3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->mHandler:Landroid/os/Handler;

    .line 111
    iput-object p4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->mListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 112
    return-void
.end method

.method private process(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;Lcom/vlingo/core/internal/questions/parser/ProviderResponse;)V
    .locals 6
    .param p1, "answerQuestionHandler"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;
    .param p2, "response"    # Lcom/vlingo/core/internal/questions/parser/ProviderResponse;

    .prologue
    .line 165
    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->mAnswer:Lcom/vlingo/core/internal/questions/parser/ServerResponse;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/questions/parser/ServerResponse;->getQuestion()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->mListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    invoke-static {p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    move-result-object v5

    invoke-direct {v2, p1, v3, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$NoAnswer;-><init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 167
    .local v2, "reaction":Ljava/lang/Runnable;
    invoke-virtual {p2}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->getProvider()Ljava/lang/String;

    move-result-object v1

    .line 169
    .local v1, "provider":Ljava/lang/String;
    const-string/jumbo v3, "WolframAlphaContentProvider"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 171
    new-instance v0, Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;

    invoke-direct {v0, p2}, Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;-><init>(Lcom/vlingo/core/internal/questions/Answer;)V

    .line 173
    .local v0, "answer":Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;->hasAnswer()Z

    move-result v3

    if-nez v3, :cond_1

    .line 202
    .end local v0    # "answer":Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 203
    return-void

    .line 175
    .restart local v0    # "answer":Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;
    :cond_1
    const-string/jumbo v3, "Result"

    invoke-virtual {v0, v3}, Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;->hasSection(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;->getSimpleResponse()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 177
    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$WolframAlpha$ResultAnswer;

    .end local v2    # "reaction":Ljava/lang/Runnable;
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->mListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    invoke-static {p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->access$100(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    move-result-object v4

    invoke-direct {v2, v0, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$WolframAlpha$ResultAnswer;-><init>(Lcom/vlingo/core/internal/questions/Answer;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .restart local v2    # "reaction":Ljava/lang/Runnable;
    goto :goto_0

    .line 178
    :cond_2
    invoke-virtual {v0}, Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;->hasMoreInformation()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 179
    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$WolframAlpha$FullAnswer;

    .end local v2    # "reaction":Ljava/lang/Runnable;
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->mListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    invoke-static {p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->access$200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    move-result-object v4

    invoke-direct {v2, v0, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$WolframAlpha$FullAnswer;-><init>(Lcom/vlingo/core/internal/questions/Answer;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .restart local v2    # "reaction":Ljava/lang/Runnable;
    goto :goto_0

    .line 182
    .end local v0    # "answer":Lcom/vlingo/core/internal/questions/WolframAlphaAnswer;
    :cond_3
    const-string/jumbo v3, "TrueKnowledgeContentProvider"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 184
    new-instance v0, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer;

    invoke-direct {v0, p2}, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer;-><init>(Lcom/vlingo/core/internal/questions/Answer;)V

    .line 186
    .local v0, "answer":Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer;->hasAnswer()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 188
    sget-object v3, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;->SIMPLE_ANSWER:Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;

    invoke-virtual {v0, v3}, Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer;->is(Lcom/vlingo/core/internal/questions/TrueKnowledgeAnswer$Type;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 189
    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$TrueKnowledge$SimpleAnswer;

    .end local v2    # "reaction":Ljava/lang/Runnable;
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->mListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    invoke-static {p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->access$300(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    move-result-object v4

    invoke-direct {v2, v0, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$TrueKnowledge$SimpleAnswer;-><init>(Lcom/vlingo/core/internal/questions/Answer;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .restart local v2    # "reaction":Ljava/lang/Runnable;
    goto :goto_0

    .line 191
    :cond_4
    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$TrueKnowledge$FullAnswer;

    .end local v2    # "reaction":Ljava/lang/Runnable;
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->mListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    invoke-static {p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->access$400(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    move-result-object v4

    invoke-direct {v2, v0, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$TrueKnowledge$FullAnswer;-><init>(Lcom/vlingo/core/internal/questions/Answer;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .restart local v2    # "reaction":Ljava/lang/Runnable;
    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 116
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->mAnswerQuestionHandler:Ljava/lang/ref/WeakReference;

    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;

    .line 117
    .local v0, "answerQuestionHandler":Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;
    if-nez v0, :cond_0

    .line 143
    :goto_0
    return-void

    .line 121
    :cond_0
    :try_start_0
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->mAnswer:Lcom/vlingo/core/internal/questions/parser/ServerResponse;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/questions/parser/ServerResponse;->getXML()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 123
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->mAnswer:Lcom/vlingo/core/internal/questions/parser/ServerResponse;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/questions/parser/ServerResponse;->getXML()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/vlingo/core/internal/questions/parser/AnswerParser;->parse(Ljava/lang/String;)[Lcom/vlingo/core/internal/questions/parser/ProviderResponse;

    move-result-object v5

    .line 127
    .local v5, "responses":[Lcom/vlingo/core/internal/questions/parser/ProviderResponse;
    move-object v1, v5

    .local v1, "arr$":[Lcom/vlingo/core/internal/questions/parser/ProviderResponse;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v4, v1, v2

    .line 128
    .local v4, "response":Lcom/vlingo/core/internal/questions/parser/ProviderResponse;
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->mAnswer:Lcom/vlingo/core/internal/questions/parser/ServerResponse;

    invoke-virtual {v4, v6}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;->setAnswer(Lcom/vlingo/core/internal/questions/parser/ServerResponse;)V

    .line 129
    invoke-direct {p0, v0, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->process(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;Lcom/vlingo/core/internal/questions/parser/ProviderResponse;)V

    .line 127
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 134
    .end local v1    # "arr$":[Lcom/vlingo/core/internal/questions/parser/ProviderResponse;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "response":Lcom/vlingo/core/internal/questions/parser/ProviderResponse;
    .end local v5    # "responses":[Lcom/vlingo/core/internal/questions/parser/ProviderResponse;
    :cond_1
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->mAnswer:Lcom/vlingo/core/internal/questions/parser/ServerResponse;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/questions/parser/ServerResponse;->getAnswer()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 135
    new-instance v6, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;

    const-string/jumbo v7, "null"

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/questions/parser/ProviderResponse;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->process(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;Lcom/vlingo/core/internal/questions/parser/ProviderResponse;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    :cond_2
    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->mListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    invoke-interface {v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 140
    iput-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->mListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 141
    const/4 v0, 0x0

    .line 142
    goto :goto_0

    .line 139
    :catchall_0
    move-exception v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->mListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    invoke-interface {v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 140
    iput-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$ParsingThread;->mListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 141
    const/4 v0, 0x0

    throw v6
.end method
