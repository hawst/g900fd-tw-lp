.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSSharedPreferenceChangeListener;
.super Ljava/lang/Object;
.source "PhraseSpotter.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PSSharedPreferenceChangeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)V
    .locals 0

    .prologue
    .line 420
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSSharedPreferenceChangeListener;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
    .param p2, "x1"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;

    .prologue
    .line 420
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSSharedPreferenceChangeListener;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 424
    if-nez p2, :cond_1

    .line 432
    :cond_0
    :goto_0
    return-void

    .line 425
    :cond_1
    const-string/jumbo v0, "car_word_spotter_when_charging_only"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "car_word_spotter_enabled"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    :cond_2
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSSharedPreferenceChangeListener;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->updatePhraseSpotterState()V
    invoke-static {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$1200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)V

    goto :goto_0
.end method
