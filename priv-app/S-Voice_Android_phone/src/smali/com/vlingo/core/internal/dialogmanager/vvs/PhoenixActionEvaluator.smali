.class public Lcom/vlingo/core/internal/dialogmanager/vvs/PhoenixActionEvaluator;
.super Lcom/vlingo/sdk/recognition/VLActionEvaluator;
.source "PhoenixActionEvaluator.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/vlingo/sdk/recognition/VLActionEvaluator;-><init>()V

    return-void
.end method


# virtual methods
.method protected evaluateExpression(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 3
    .param p1, "expression"    # Ljava/lang/String;
    .param p2, "parameters"    # [Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 15
    const-string/jumbo v1, "setting-equals"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 16
    aget-object v1, p2, v2

    aget-object v0, p2, v0

    invoke-virtual {p0, v1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/PhoenixActionEvaluator;->settingEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 23
    :goto_0
    return v0

    .line 17
    :cond_0
    const-string/jumbo v1, "set-setting"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 18
    aget-object v1, p2, v2

    aget-object v2, p2, v0

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/PhoenixActionEvaluator;->setSetting(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 20
    :cond_1
    const-string/jumbo v1, "test-and-inc"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 21
    aget-object v1, p2, v2

    aget-object v0, p2, v0

    invoke-virtual {p0, v1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/PhoenixActionEvaluator;->testAndIncrement(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 23
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/vlingo/sdk/recognition/VLActionEvaluator;->evaluateExpression(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public setSetting(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "settingName"    # Ljava/lang/String;
    .param p2, "settingValue"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-static {p1, p2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method protected settingEquals(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "settingName"    # Ljava/lang/String;
    .param p2, "settingValue"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-static {p1}, Lcom/vlingo/core/internal/settings/Settings;->hasSetting(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 68
    :cond_0
    :goto_0
    return v1

    .line 64
    :cond_1
    invoke-static {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/PhoenixActionEvaluator;->isBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 65
    invoke-static {p1, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    const-string/jumbo v3, "true"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 67
    :cond_2
    const-string/jumbo v1, ""

    invoke-static {p1, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 68
    .local v0, "actualSettingValue":Ljava/lang/String;
    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public testAndIncrement(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "settingName"    # Ljava/lang/String;
    .param p2, "testValue"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 39
    invoke-static {p1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 40
    .local v1, "val":I
    const/4 v0, 0x0

    .line 41
    .local v0, "testVal":I
    :try_start_0
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 46
    :goto_0
    if-ge v1, v0, :cond_0

    .line 47
    add-int/lit8 v1, v1, 0x1

    .line 48
    invoke-static {p1, v1}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 51
    const/4 v2, 0x1

    .line 56
    :cond_0
    return v2

    .line 41
    :catch_0
    move-exception v3

    goto :goto_0
.end method
