.class public Lcom/vlingo/core/internal/recognition/acceptedtext/AddTaskAcceptedText;
.super Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;
.source "AddTaskAcceptedText.java"


# instance fields
.field private date:Ljava/lang/String;

.field private reminderdate:Ljava/lang/String;

.field private remindertime:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "startdate"    # Ljava/lang/String;
    .param p3, "reminderdate"    # Ljava/lang/String;
    .param p4, "remindertime"    # Ljava/lang/String;

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;-><init>(Ljava/lang/String;)V

    .line 27
    iput-object p1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddTaskAcceptedText;->title:Ljava/lang/String;

    .line 28
    iput-object p2, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddTaskAcceptedText;->date:Ljava/lang/String;

    .line 29
    iput-object p3, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddTaskAcceptedText;->reminderdate:Ljava/lang/String;

    .line 30
    iput-object p4, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddTaskAcceptedText;->remindertime:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method protected getAcceptedTextXML()Ljava/lang/String;
    .locals 2

    .prologue
    .line 35
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 36
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "<AcceptedText pt=\"task:add\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 37
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddTaskAcceptedText;->date:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 38
    const-string/jumbo v1, "<Tag u=\"date\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 39
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddTaskAcceptedText;->date:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 40
    const-string/jumbo v1, "</Tag>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 42
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddTaskAcceptedText;->remindertime:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 43
    const-string/jumbo v1, "<Tag u=\"remindertime\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 44
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddTaskAcceptedText;->remindertime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 45
    const-string/jumbo v1, "</Tag>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 47
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddTaskAcceptedText;->reminderdate:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 48
    const-string/jumbo v1, "<Tag u=\"reminderdate\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 49
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddTaskAcceptedText;->reminderdate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 50
    const-string/jumbo v1, "</Tag>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 52
    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddTaskAcceptedText;->title:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 53
    const-string/jumbo v1, "<Tag u=\"title\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 54
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddTaskAcceptedText;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 55
    const-string/jumbo v1, "</Tag>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 57
    :cond_3
    const-string/jumbo v1, "</AcceptedText>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 58
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "AddCalEventAcceptedText ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddTaskAcceptedText;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", reminderdate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddTaskAcceptedText;->reminderdate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
