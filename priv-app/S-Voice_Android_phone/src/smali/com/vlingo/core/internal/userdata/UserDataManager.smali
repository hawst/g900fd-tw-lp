.class public Lcom/vlingo/core/internal/userdata/UserDataManager;
.super Ljava/lang/Object;
.source "UserDataManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/userdata/UserDataManager$ServerDataDeletionListener;,
        Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;
    }
.end annotation


# static fields
.field private static final DELETE_PERSONAL_DATA_APPLET:Ljava/lang/String; = "voicepad/deletepersonaldata"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 238
    return-void
.end method

.method public static clearLocalData()V
    .locals 11

    .prologue
    .line 75
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->getInstance()Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->onUserDataCleared()V

    .line 77
    const/4 v7, 0x0

    .line 78
    .local v7, "success":Z
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 81
    .local v1, "appContext":Landroid/content/Context;
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x13

    if-lt v8, v9, :cond_0

    .line 86
    const-string/jumbo v8, "activity"

    invoke-virtual {v1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 91
    .local v0, "am":Landroid/app/ActivityManager;
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string/jumbo v9, "clearApplicationUserData"

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Class;

    invoke-virtual {v8, v9, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 92
    .local v5, "method":Ljava/lang/reflect/Method;
    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v5, v0, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    .line 95
    const/4 v7, 0x1

    .line 110
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v5    # "method":Ljava/lang/reflect/Method;
    :cond_0
    :goto_0
    if-nez v7, :cond_1

    .line 113
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    .line 114
    .local v6, "runtime":Ljava/lang/Runtime;
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 115
    .local v2, "appPackageName":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "pm clear "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 119
    .local v3, "cmdLine":Ljava/lang/String;
    :try_start_1
    invoke-virtual {v6, v3}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    .line 127
    .end local v2    # "appPackageName":Ljava/lang/String;
    .end local v3    # "cmdLine":Ljava/lang/String;
    .end local v6    # "runtime":Ljava/lang/Runtime;
    :cond_1
    :goto_1
    return-void

    .line 97
    .restart local v0    # "am":Landroid/app/ActivityManager;
    :catch_0
    move-exception v4

    .line 98
    .local v4, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v4}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 100
    .end local v4    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v4

    .line 101
    .local v4, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v4}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 103
    .end local v4    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v4

    .line 104
    .local v4, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v4}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 106
    .end local v4    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v4

    .line 107
    .local v4, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v4}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    .line 120
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v4    # "e":Ljava/lang/reflect/InvocationTargetException;
    .restart local v2    # "appPackageName":Ljava/lang/String;
    .restart local v3    # "cmdLine":Ljava/lang/String;
    .restart local v6    # "runtime":Ljava/lang/Runtime;
    :catch_4
    move-exception v4

    .line 121
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public static deleteUserDataOnServer(ZLcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;)V
    .locals 7
    .param p0, "deleteLocalData"    # Z
    .param p1, "callback"    # Lcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;

    .prologue
    .line 54
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->getInstance()Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->onUserDataCleared()V

    .line 56
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getServerInfo()Lcom/vlingo/core/internal/util/CoreServerInfo;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/CoreServerInfoUtils;->getAsrUri(Lcom/vlingo/core/internal/util/CoreServerInfo;)Ljava/lang/String;

    move-result-object v2

    .line 57
    .local v2, "asrUri":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "voicepad/deletepersonaldata"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 58
    .local v1, "appletUri":Ljava/lang/String;
    new-instance v0, Lcom/vlingo/sdk/internal/http/URL;

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/http/URL;-><init>(Ljava/lang/String;)V

    .line 59
    .local v0, "appletURL":Lcom/vlingo/sdk/internal/http/URL;
    const-string/jumbo v4, "UserDataRequest-Request"

    new-instance v5, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;

    invoke-direct {v5, p0, p1}, Lcom/vlingo/core/internal/userdata/UserDataManager$UserDataHTTPHandler;-><init>(ZLcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;)V

    const-string/jumbo v6, ""

    invoke-static {v4, v5, v0, v6}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->createVLRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Lcom/vlingo/sdk/internal/http/URL;Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    move-result-object v3

    .line 61
    .local v3, "currentRequest":Lcom/vlingo/sdk/internal/http/HttpRequest;
    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/http/HttpRequest;->start()V

    .line 64
    return-void
.end method
