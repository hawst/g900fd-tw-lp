.class Lcom/vlingo/core/internal/dialogmanager/DialogFlow$5;
.super Ljava/lang/Object;
.source "DialogFlow.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V
    .locals 0

    .prologue
    .line 1688
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$5;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 1722
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$5;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2700(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/display/WakeLockManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1723
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$5;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2700(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/display/WakeLockManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/display/WakeLockManager;->releaseWakeLock()V

    .line 1724
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "wake lock released"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1728
    :goto_0
    return-void

    .line 1726
    :cond_0
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "wake lock not released, wakeLockManager is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 1702
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$5;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2700(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/display/WakeLockManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1703
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$5;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2700(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/display/WakeLockManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/display/WakeLockManager;->releaseWakeLock()V

    .line 1704
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "wake lock released"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1708
    :goto_0
    return-void

    .line 1706
    :cond_0
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "wake lock not released, wakeLockManager is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 1712
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$5;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2700(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/display/WakeLockManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1713
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$5;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2700(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/display/WakeLockManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/display/WakeLockManager;->releaseWakeLock()V

    .line 1714
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "wake lock released"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1718
    :goto_0
    return-void

    .line 1716
    :cond_0
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "wake lock not released, wakeLockManager is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 1692
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$5;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2700(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/display/WakeLockManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1693
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$5;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2700(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/display/WakeLockManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/display/WakeLockManager;->acquireWakeLock()V

    .line 1694
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "wake lock acquired"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1698
    :goto_0
    return-void

    .line 1696
    :cond_0
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "wake lock not acquired, wakeLockManager is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
