.class public Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "SendMessageAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;


# instance fields
.field private addresses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    .line 26
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->addresses:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;
    .param p1, "x1"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->errString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    return-object v0
.end method

.method private errString(I)Ljava/lang/String;
    .locals 2
    .param p1, "code"    # I

    .prologue
    .line 71
    packed-switch p1, :pswitch_data_0

    .line 75
    :pswitch_0
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_unknown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 72
    :pswitch_1
    const-string/jumbo v0, "no-service"

    goto :goto_0

    .line 73
    :pswitch_2
    const-string/jumbo v0, "radio-off"

    goto :goto_0

    .line 71
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private sendSMS(Ljava/lang/String;)V
    .locals 3
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->message:Ljava/lang/String;

    new-instance v2, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction$1;

    invoke-direct {v2, p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction$1;-><init>(Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;)V

    invoke-static {v0, p1, v1, v2}, Lcom/vlingo/core/internal/util/SMSUtil;->sendSMS(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallback;)V

    .line 68
    return-void
.end method


# virtual methods
.method public address(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;
    .locals 1
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->addresses:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    return-object p0
.end method

.method public bridge synthetic address(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->address(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;

    move-result-object v0

    return-object v0
.end method

.method public addresses(Ljava/util/List;)Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;"
        }
    .end annotation

    .prologue
    .line 39
    .local p1, "addressList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->addresses:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 40
    return-object p0
.end method

.method public bridge synthetic addresses(Ljava/util/List;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;
    .locals 1
    .param p1, "x0"    # Ljava/util/List;

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->addresses(Ljava/util/List;)Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;

    move-result-object v0

    return-object v0
.end method

.method protected execute()V
    .locals 4

    .prologue
    .line 45
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->addresses:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 46
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->addresses:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 47
    .local v0, "address":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->sendSMS(Ljava/lang/String;)V

    goto :goto_0

    .line 50
    .end local v0    # "address":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v3, "No address"

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    .line 52
    :cond_1
    return-void
.end method

.method public message(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;
    .locals 0
    .param p1, "messageParam"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->message:Ljava/lang/String;

    .line 35
    return-object p0
.end method

.method public bridge synthetic message(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->message(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;

    move-result-object v0

    return-object v0
.end method
