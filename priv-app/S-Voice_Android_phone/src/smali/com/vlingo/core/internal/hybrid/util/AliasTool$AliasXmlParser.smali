.class Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;
.super Lcom/vlingo/core/internal/xml/SimpleXmlParser;
.source "AliasTool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/hybrid/util/AliasTool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AliasXmlParser"
.end annotation


# instance fields
.field private ALIAS_ELEMENT_TYPE:I

.field private HOLDER_ELEMENT_TYPE:I

.field private LABEL_ELEMENT_TYPE:I

.field private currentAliases:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private currentLabel:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 186
    invoke-direct {p0}, Lcom/vlingo/core/internal/xml/SimpleXmlParser;-><init>()V

    .line 181
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;->currentAliases:Ljava/util/List;

    .line 183
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;->currentLabel:Ljava/lang/String;

    .line 187
    const-string/jumbo v0, "label"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;->LABEL_ELEMENT_TYPE:I

    .line 188
    const-string/jumbo v0, "alias"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;->ALIAS_ELEMENT_TYPE:I

    .line 189
    const-string/jumbo v0, "holder"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;->HOLDER_ELEMENT_TYPE:I

    .line 190
    return-void
.end method


# virtual methods
.method public beginElement(ILcom/vlingo/core/internal/xml/XmlAttributes;[CI)V
    .locals 2
    .param p1, "elementType"    # I
    .param p2, "attributes"    # Lcom/vlingo/core/internal/xml/XmlAttributes;
    .param p3, "cData"    # [C
    .param p4, "elementEndPosition"    # I

    .prologue
    .line 194
    iget v0, p0, Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;->LABEL_ELEMENT_TYPE:I

    if-ne p1, v0, :cond_1

    .line 195
    invoke-static {p3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;->currentLabel:Ljava/lang/String;

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    iget v0, p0, Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;->ALIAS_ELEMENT_TYPE:I

    if-ne p1, v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;->currentAliases:Ljava/util/List;

    invoke-static {p3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public endElement(II)V
    .locals 3
    .param p1, "elementType"    # I
    .param p2, "elementStartPosition"    # I

    .prologue
    .line 202
    iget v1, p0, Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;->HOLDER_ELEMENT_TYPE:I

    if-ne p1, v1, :cond_0

    .line 203
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;->currentAliases:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 204
    .local v0, "aliases":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    # getter for: Lcom/vlingo/core/internal/hybrid/util/AliasTool;->labelsToAliases:Ljava/util/Map;
    invoke-static {}, Lcom/vlingo/core/internal/hybrid/util/AliasTool;->access$100()Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;->currentLabel:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    iget-object v1, p0, Lcom/vlingo/core/internal/hybrid/util/AliasTool$AliasXmlParser;->currentAliases:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 207
    .end local v0    # "aliases":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    return-void
.end method
