.class public final Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
.super Ljava/lang/Object;
.source "PhraseSpotterParameters.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$1;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$PhraseSpotterStats;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;
    }
.end annotation


# instance fields
.field private audioSourceType:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

.field private chunkLengthMs:I

.field private coreSpotterParams:Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

.field private isTesting:Z

.field private language:Ljava/lang/String;

.field private preBufferLengthMs:I

.field private recorderSleepMs:I

.field private seamlessTimeoutMs:I

.field final stats:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$PhraseSpotterStats;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->stats:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$PhraseSpotterStats;

    .line 153
    return-void
.end method

.method private constructor <init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->language:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->access$100(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->language:Ljava/lang/String;

    .line 139
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->chunkLengthMs:I
    invoke-static {p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->access$200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->chunkLengthMs:I

    .line 140
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->recorderSleepMs:I
    invoke-static {p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->access$300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->recorderSleepMs:I

    .line 141
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->preBufferLengthMs:I
    invoke-static {p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->access$400(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->preBufferLengthMs:I

    .line 142
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->seamlessTimeoutMs:I
    invoke-static {p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->seamlessTimeoutMs:I

    .line 143
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->audioSourceType:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;
    invoke-static {p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->access$600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;)Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->audioSourceType:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    .line 144
    iget-object v0, p1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->coreSpotterParams:Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->coreSpotterParams:Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    .line 145
    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->isTesting:Z
    invoke-static {p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->access$700(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->isTesting:Z

    .line 148
    iget-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->isTesting:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$PhraseSpotterStats;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$PhraseSpotterStats;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;)V

    :goto_0
    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->stats:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$PhraseSpotterStats;

    .line 149
    return-void

    .line 148
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;
    .param p2, "x1"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$1;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;)V

    return-void
.end method


# virtual methods
.method public getAudioSourceType()Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->audioSourceType:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    return-object v0
.end method

.method public getChunkLength()I
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->chunkLengthMs:I

    return v0
.end method

.method public getCoreSpotterParams()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->coreSpotterParams:Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->language:Ljava/lang/String;

    return-object v0
.end method

.method public getPreBufferLength()I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->preBufferLengthMs:I

    return v0
.end method

.method public getRecorderSleep()I
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->recorderSleepMs:I

    return v0
.end method

.method public getSeamlessTimeout()I
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->seamlessTimeoutMs:I

    return v0
.end method

.method getStats()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$PhraseSpotterStats;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->stats:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$PhraseSpotterStats;

    return-object v0
.end method

.method isTesting()Z
    .locals 1

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;->isTesting:Z

    return v0
.end method
