.class public Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;
.super Lcom/vlingo/core/internal/dialogmanager/StateController;
.source "AlertReadoutController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# static fields
.field private static final PATTERN_FOR_RUSSIAN_1:Ljava/util/regex/Pattern;

.field private static final PATTERN_FOR_RUSSIAN_2:Ljava/util/regex/Pattern;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected alertQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation
.end field

.field protected currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

.field private currentContactSearch:Ljava/lang/String;

.field private currentOrdinalSearch:Ljava/lang/String;

.field protected currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

.field private isDialingEnabled:Z

.field private isReplyMode:Z

.field private isSilentMode:Z

.field private messagePosition:I

.field protected senderList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;",
            ">;"
        }
    .end annotation
.end field

.field protected senderQueue:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->TAG:Ljava/lang/String;

    .line 60
    const-string/jumbo v0, "[^1]?(2|3|4)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->PATTERN_FOR_RUSSIAN_1:Ljava/util/regex/Pattern;

    .line 62
    const-string/jumbo v0, "^\\d*[^1]1$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->PATTERN_FOR_RUSSIAN_2:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/StateController;-><init>()V

    .line 54
    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 55
    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 64
    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentContactSearch:Ljava/lang/String;

    .line 65
    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentOrdinalSearch:Ljava/lang/String;

    .line 66
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->messagePosition:I

    return-void
.end method

.method private displayInitialSingleMessage(Lcom/vlingo/core/internal/messages/SMSMMSAlert;Lcom/vlingo/core/internal/dialogmanager/types/MessageType;)V
    .locals 11
    .param p1, "message"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .param p2, "mtForWidget"    # Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 496
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeShowMessageBody()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v1

    .line 498
    .local v1, "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    invoke-virtual {p1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 499
    .local v2, "name":Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 500
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 501
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getId()J

    move-result-wide v5

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v0, v5, v6, v7}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->markAsRead(Landroid/content/Context;JLjava/lang/String;)V

    .line 503
    iget-boolean v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->isDialingEnabled:Z

    if-eqz v4, :cond_1

    .line 504
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->isWithoutAskingMsg()Z

    move-result v4

    if-nez v4, :cond_0

    .line 505
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v9, [Ljava/lang/Object;

    aput-object v2, v5, v8

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 517
    .local v3, "spokenSingleText":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SAFEREADER_READBACKMSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v5}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 518
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isPhoneDrivingModeEnabled()Z

    move-result v4

    if-nez v4, :cond_3

    .line 519
    invoke-virtual {p0, v10, v3, v8, v10}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 529
    :goto_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MessageReadback:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {v4, v5, v1, p2, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 530
    return-void

    .line 507
    .end local v3    # "spokenSingleText":Ljava/lang/String;
    :cond_0
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v9, [Ljava/lang/Object;

    aput-object v2, v5, v8

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "spokenSingleText":Ljava/lang/String;
    goto :goto_0

    .line 510
    .end local v3    # "spokenSingleText":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->isWithoutAskingMsg()Z

    move-result v4

    if-nez v4, :cond_2

    .line 511
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v9, [Ljava/lang/Object;

    aput-object v2, v5, v8

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "spokenSingleText":Ljava/lang/String;
    goto :goto_0

    .line 513
    .end local v3    # "spokenSingleText":Ljava/lang/String;
    :cond_2
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v9, [Ljava/lang/Object;

    aput-object v2, v5, v8

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "spokenSingleText":Ljava/lang/String;
    goto :goto_0

    .line 521
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 522
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->ttsAnyway(Ljava/lang/String;)V

    goto :goto_1

    .line 524
    :cond_4
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    .line 525
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->startReco()Z

    goto :goto_1
.end method

.method private displayMultipleMessages()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v6, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 533
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    if-nez v4, :cond_1

    .line 574
    :cond_0
    :goto_0
    return-void

    .line 536
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeShowMessageBody()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    .line 539
    .local v0, "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getDisplayNameOrAddress()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 541
    .local v3, "ttsName":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "ru-RU"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 542
    iget-boolean v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->isDialingEnabled:Z

    if-eqz v4, :cond_2

    .line 543
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v6, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessageCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    aput-object v3, v5, v8

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessageCount()I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getPhraseNewMessagesRussian(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 555
    .local v2, "spokenMultiText":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_READBACK_MSGCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v5}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 556
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isPhoneDrivingModeEnabled()Z

    move-result v4

    if-nez v4, :cond_5

    .line 557
    invoke-virtual {p0, v10, v2, v7, v10}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 558
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 559
    .local v1, "pageSenderList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getMessagesForWidget(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v4, v5, v0, v6, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto :goto_0

    .line 545
    .end local v1    # "pageSenderList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    .end local v2    # "spokenMultiText":Ljava/lang/String;
    :cond_2
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v6, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessageCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    aput-object v3, v5, v8

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessageCount()I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getPhraseNewMessagesRussian(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "spokenMultiText":Ljava/lang/String;
    goto :goto_1

    .line 548
    .end local v2    # "spokenMultiText":Ljava/lang/String;
    :cond_3
    iget-boolean v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->isDialingEnabled:Z

    if-eqz v4, :cond_4

    .line 549
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessageCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    aput-object v3, v5, v8

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "spokenMultiText":Ljava/lang/String;
    goto :goto_1

    .line 551
    .end local v2    # "spokenMultiText":Ljava/lang/String;
    :cond_4
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessageCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    aput-object v3, v5, v8

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "spokenMultiText":Ljava/lang/String;
    goto/16 :goto_1

    .line 561
    :cond_5
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 562
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->ttsAnyway(Ljava/lang/String;)V

    .line 568
    :goto_2
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_7

    .line 569
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleSenderMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-interface {v4, v5, v0, v6, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto/16 :goto_0

    .line 564
    :cond_6
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    .line 565
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->startReco()Z

    goto :goto_2

    .line 571
    :cond_7
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "Alert Queue is not null or empty but senderList is, this should not be possible."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private displayNonInitialSingleMessage(ZLcom/vlingo/core/internal/dialogmanager/types/MessageType;)V
    .locals 9
    .param p1, "hasNext"    # Z
    .param p2, "mtForWidget"    # Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 447
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeShowMessageBody()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v1

    .line 449
    .local v1, "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    if-eqz p1, :cond_3

    .line 450
    iget-boolean v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->isDialingEnabled:Z

    if-eqz v3, :cond_1

    .line 451
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->isWithoutAskingMsg()Z

    move-result v3

    if-nez v3, :cond_0

    .line 452
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 479
    .local v2, "spokenSingleText":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 480
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getId()J

    move-result-wide v4

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v0, v4, v5, v6}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->markAsRead(Landroid/content/Context;JLjava/lang/String;)V

    .line 481
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SAFEREADER_READBACKMSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v4}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 482
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isPhoneDrivingModeEnabled()Z

    move-result v3

    if-nez v3, :cond_7

    .line 483
    invoke-virtual {p0, v8, v2, v7, v8}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 492
    :goto_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MessageReadback:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {v3, v4, v1, p2, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 493
    return-void

    .line 454
    .end local v0    # "context":Landroid/content/Context;
    .end local v2    # "spokenSingleText":Ljava/lang/String;
    :cond_0
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "spokenSingleText":Ljava/lang/String;
    goto :goto_0

    .line 457
    .end local v2    # "spokenSingleText":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->isWithoutAskingMsg()Z

    move-result v3

    if-nez v3, :cond_2

    .line 458
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "spokenSingleText":Ljava/lang/String;
    goto :goto_0

    .line 460
    .end local v2    # "spokenSingleText":Ljava/lang/String;
    :cond_2
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "spokenSingleText":Ljava/lang/String;
    goto :goto_0

    .line 464
    .end local v2    # "spokenSingleText":Ljava/lang/String;
    :cond_3
    iget-boolean v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->isDialingEnabled:Z

    if-eqz v3, :cond_5

    .line 465
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->isWithoutAskingMsg()Z

    move-result v3

    if-nez v3, :cond_4

    .line 466
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "spokenSingleText":Ljava/lang/String;
    goto/16 :goto_0

    .line 468
    .end local v2    # "spokenSingleText":Ljava/lang/String;
    :cond_4
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "spokenSingleText":Ljava/lang/String;
    goto/16 :goto_0

    .line 471
    .end local v2    # "spokenSingleText":Ljava/lang/String;
    :cond_5
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->isWithoutAskingMsg()Z

    move-result v3

    if-nez v3, :cond_6

    .line 472
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "spokenSingleText":Ljava/lang/String;
    goto/16 :goto_0

    .line 474
    .end local v2    # "spokenSingleText":Ljava/lang/String;
    :cond_6
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "spokenSingleText":Ljava/lang/String;
    goto/16 :goto_0

    .line 485
    .restart local v0    # "context":Landroid/content/Context;
    :cond_7
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 486
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->ttsAnyway(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 488
    :cond_8
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    .line 489
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->startReco()Z

    goto/16 :goto_1
.end method

.method private forward()V
    .locals 7

    .prologue
    .line 779
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 780
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionForwardEvent;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getId()J

    move-result-wide v1

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getAddress()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getFieldId()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->getFieldId()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/core/internal/dialogmanager/events/ActionForwardEvent;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionForwardEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 787
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionForwardEvent;
    :cond_0
    :goto_0
    return-void

    .line 782
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 783
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 784
    .local v6, "currentMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionForwardEvent;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getId()J

    move-result-wide v1

    invoke-virtual {v6}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getFieldId()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->getFieldId()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/core/internal/dialogmanager/events/ActionForwardEvent;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    .restart local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionForwardEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    goto :goto_0
.end method

.method private generateSingleList(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/util/LinkedList;
    .locals 1
    .param p1, "alert"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ")",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 816
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 817
    .local v0, "singleAlertList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 818
    return-object v0
.end method

.method private generateVerboseMultiSenderSpoken(Ljava/util/LinkedList;I)Ljava/lang/String;
    .locals 12
    .param p2, "totalMessages"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p1, "senders":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 823
    const-string/jumbo v3, ""

    .line 825
    .local v3, "names":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/LinkedList;->size()I

    move-result v1

    .line 828
    .local v1, "max":I
    const/4 v0, 0x0

    .line 829
    .local v0, "index":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 830
    if-eqz v0, :cond_0

    .line 831
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_comma:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 832
    add-int/lit8 v4, v1, -0x1

    if-ne v0, v4, :cond_0

    .line 833
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 835
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_and:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 838
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 839
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getDisplayNameOrAddress()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 840
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 843
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getDrivingModeWidgetMax()I

    move-result v4

    if-le v1, v4, :cond_4

    .line 844
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getDrivingModeWidgetMax()I

    move-result v1

    .line 845
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "ru-RU"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 846
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_info_verbose_overflow_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p1}, Ljava/util/LinkedList;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    aput-object v3, v6, v10

    invoke-virtual {p0, p2}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getPhraseNewMessagesRussian(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v11

    invoke-interface {v4, v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 858
    .local v2, "multiSenderSpokenText":Ljava/lang/String;
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_dot:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 859
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 860
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->isWithoutAskingMsg()Z

    move-result v4

    if-nez v4, :cond_2

    .line 861
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_command_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 864
    :cond_2
    return-object v2

    .line 848
    .end local v2    # "multiSenderSpokenText":Ljava/lang/String;
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_info_verbose_overflow_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p1}, Ljava/util/LinkedList;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    aput-object v3, v6, v10

    invoke-interface {v4, v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "multiSenderSpokenText":Ljava/lang/String;
    goto :goto_1

    .line 851
    .end local v2    # "multiSenderSpokenText":Ljava/lang/String;
    :cond_4
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "ru-RU"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 852
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_info_verbose_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    aput-object v3, v6, v9

    invoke-virtual {p0, p2}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getPhraseNewMessagesRussian(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-interface {v4, v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "multiSenderSpokenText":Ljava/lang/String;
    goto/16 :goto_1

    .line 854
    .end local v2    # "multiSenderSpokenText":Ljava/lang/String;
    :cond_5
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_info_verbose_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    aput-object v3, v6, v9

    invoke-interface {v4, v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "multiSenderSpokenText":Ljava/lang/String;
    goto/16 :goto_1
.end method

.method private handleContact(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 6
    .param p1, "contactName"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 381
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentContactSearch:Ljava/lang/String;

    .line 383
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderQueue:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move v1, v3

    .line 412
    :goto_0
    return v1

    .line 387
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 388
    .local v0, "filteredSenderQueue":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-static {v1, p1, p2}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->getMessageQueueByContactName(Ljava/util/HashMap;Ljava/lang/String;Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v0

    .line 389
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 392
    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderQueue:Ljava/util/HashMap;

    .line 393
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    new-instance v4, Lcom/vlingo/core/internal/util/ListControlData;

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    invoke-direct {v4, v5}, Lcom/vlingo/core/internal/util/ListControlData;-><init>(I)V

    invoke-static {v1, v4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/util/ListControlData;)V

    .line 394
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    .line 395
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 396
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->sortMessageReadoutList(Ljava/util/List;)Ljava/util/List;

    .line 397
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-le v1, v2, :cond_2

    .line 398
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->displayMultipleSenders()V

    move v1, v2

    .line 399
    goto :goto_0

    .line 401
    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v1, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 402
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessagesFromSender()Ljava/util/LinkedList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    .line 403
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    new-instance v4, Lcom/vlingo/core/internal/util/ListControlData;

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    invoke-direct {v4, v5}, Lcom/vlingo/core/internal/util/ListControlData;-><init>(I)V

    invoke-static {v1, v4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/util/ListControlData;)V

    .line 404
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v1, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 405
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 406
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    check-cast v1, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->generateSingleList(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/util/LinkedList;

    move-result-object v1

    invoke-interface {v4, v5, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 408
    :cond_3
    invoke-virtual {p0, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->displaySingleMessage(Z)V

    move v1, v2

    .line 409
    goto/16 :goto_0

    :cond_4
    move v1, v3

    .line 412
    goto/16 :goto_0
.end method

.method private handleWhich(Ljava/lang/String;)Z
    .locals 9
    .param p1, "which"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 328
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentOrdinalSearch:Ljava/lang/String;

    .line 330
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 332
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v4

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v4, v7}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 335
    .local v2, "ordinalList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isPhoneDrivingModeEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 337
    invoke-static {v2, p1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElementFromList(Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 342
    .local v0, "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :goto_0
    if-nez v0, :cond_1

    .line 344
    invoke-direct {p0, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->noValidMessages(Z)V

    move v4, v5

    .line 376
    .end local v0    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v2    # "ordinalList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    :goto_1
    return v4

    .line 340
    .restart local v2    # "ordinalList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    :cond_0
    invoke-static {v2, p1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElementFromList(Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .restart local v0    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    goto :goto_0

    .line 349
    :cond_1
    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 350
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {p1, v4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->ordinalToInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->messagePosition:I

    .line 351
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->generateSingleList(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/util/LinkedList;

    move-result-object v8

    invoke-interface {v4, v7, v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 352
    invoke-virtual {p0, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->displaySingleMessage(Z)V

    move v4, v5

    .line 353
    goto :goto_1

    .line 355
    .end local v0    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v2    # "ordinalList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    :cond_2
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    .line 357
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v4

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v4, v7}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 358
    .local v1, "ordinalList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    invoke-static {v1, p1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElementFromList(Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 359
    .local v3, "ordinalSender":Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;
    if-nez v3, :cond_3

    .line 361
    invoke-direct {p0, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->noValidMessages(Z)V

    move v4, v5

    .line 362
    goto :goto_1

    .line 366
    :cond_3
    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 367
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessagesFromSender()Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/LinkedList;

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    .line 368
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    new-instance v7, Lcom/vlingo/core/internal/util/ListControlData;

    iget-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v8}, Ljava/util/LinkedList;->size()I

    move-result v8

    invoke-direct {v7, v8}, Lcom/vlingo/core/internal/util/ListControlData;-><init>(I)V

    invoke-static {v4, v7}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/util/ListControlData;)V

    .line 369
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v4, v6}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 370
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-static {v4}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 371
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v7

    sget-object v8, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    check-cast v4, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->generateSingleList(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/util/LinkedList;

    move-result-object v4

    invoke-interface {v7, v8, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 373
    :cond_4
    invoke-virtual {p0, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->displaySingleMessage(Z)V

    move v4, v5

    .line 374
    goto/16 :goto_1

    .end local v1    # "ordinalList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    .end local v3    # "ordinalSender":Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;
    :cond_5
    move v4, v6

    .line 376
    goto/16 :goto_1
.end method

.method private isNewMessagesForRussian(ILjava/util/regex/Pattern;)Z
    .locals 2
    .param p1, "number"    # I
    .param p2, "pattern"    # Ljava/util/regex/Pattern;

    .prologue
    .line 868
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 869
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    return v1
.end method

.method private moveNext(I)V
    .locals 3
    .param p1, "shift"    # I

    .prologue
    .line 717
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 720
    iget v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->messagePosition:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->messagePosition:I

    .line 724
    iget v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->messagePosition:I

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->messagePosition:I

    if-ltz v0, :cond_1

    .line 725
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    iget v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->messagePosition:I

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 726
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 727
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->generateSingleList(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 729
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->displaySingleMessage(Z)V

    .line 734
    :goto_0
    return-void

    .line 733
    :cond_1
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->noValidNextMessage()V

    goto :goto_0
.end method

.method private multipleMessages(Z)V
    .locals 2
    .param p1, "initial"    # Z

    .prologue
    .line 806
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 808
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->displayMultipleMessages()V

    .line 813
    :goto_0
    return-void

    .line 810
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 811
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->displaySingleMessage(Z)V

    goto :goto_0
.end method

.method private next()V
    .locals 1

    .prologue
    .line 739
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->moveNext(I)V

    .line 740
    return-void
.end method

.method private noValidMessages(Z)V
    .locals 11
    .param p1, "initial"    # Z

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 615
    if-eqz p1, :cond_2

    .line 619
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentContactSearch:Ljava/lang/String;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 620
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_none_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentContactSearch:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 621
    .local v1, "displayNoneFound":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_none_from_sender_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentContactSearch:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 622
    .local v4, "spokenNoneFound":Ljava/lang/String;
    iput-object v10, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentContactSearch:Ljava/lang/String;

    .line 632
    :goto_0
    invoke-virtual {p0, v1, v4}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->reset()V

    .line 682
    .end local v1    # "displayNoneFound":Ljava/lang/String;
    .end local v4    # "spokenNoneFound":Ljava/lang/String;
    :goto_1
    return-void

    .line 623
    :cond_0
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentOrdinalSearch:Ljava/lang/String;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 624
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_none_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentOrdinalSearch:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 625
    .restart local v1    # "displayNoneFound":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_none_from_sender_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentOrdinalSearch:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 626
    .restart local v4    # "spokenNoneFound":Ljava/lang/String;
    iput-object v10, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentOrdinalSearch:Ljava/lang/String;

    goto :goto_0

    .line 628
    .end local v1    # "displayNoneFound":Ljava/lang/String;
    .end local v4    # "spokenNoneFound":Ljava/lang/String;
    :cond_1
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_none:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 629
    .restart local v1    # "displayNoneFound":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_none_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "spokenNoneFound":Ljava/lang/String;
    goto :goto_0

    .line 639
    .end local v1    # "displayNoneFound":Ljava/lang/String;
    .end local v4    # "spokenNoneFound":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeShowMessageBody()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    .line 641
    .local v0, "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentContactSearch:Ljava/lang/String;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 642
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_none_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentContactSearch:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 643
    .local v2, "noneFoundShown":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_none_from_sender_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentContactSearch:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 644
    .local v3, "noneFoundSpoken":Ljava/lang/String;
    iput-object v10, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentContactSearch:Ljava/lang/String;

    .line 655
    :goto_2
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isPhoneDrivingModeEnabled()Z

    move-result v5

    if-nez v5, :cond_6

    .line 656
    invoke-virtual {p0, v2, v3, v9, v10}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 667
    :goto_3
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 668
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleSenderMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-interface {v5, v6, v0, v7, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto :goto_1

    .line 645
    .end local v2    # "noneFoundShown":Ljava/lang/String;
    .end local v3    # "noneFoundSpoken":Ljava/lang/String;
    :cond_4
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentOrdinalSearch:Ljava/lang/String;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 646
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_no_match_found_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentOrdinalSearch:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 647
    .restart local v2    # "noneFoundShown":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_no_match_found_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentOrdinalSearch:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 648
    .restart local v3    # "noneFoundSpoken":Ljava/lang/String;
    iput-object v10, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentOrdinalSearch:Ljava/lang/String;

    goto :goto_2

    .line 651
    .end local v2    # "noneFoundShown":Ljava/lang/String;
    .end local v3    # "noneFoundSpoken":Ljava/lang/String;
    :cond_5
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_no_match_found_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    const-string/jumbo v7, ""

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 652
    .restart local v2    # "noneFoundShown":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_no_match_found_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v9, [Ljava/lang/Object;

    const-string/jumbo v7, ""

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "noneFoundSpoken":Ljava/lang/String;
    goto :goto_2

    .line 658
    :cond_6
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 659
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->ttsAnyway(Ljava/lang/String;)V

    goto :goto_3

    .line 661
    :cond_7
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    .line 662
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->startReco()Z

    goto :goto_3

    .line 672
    :cond_8
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isPhoneDrivingModeEnabled()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 673
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_9

    .line 674
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleSenderMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-interface {v5, v6, v0, v7, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto/16 :goto_1

    .line 676
    :cond_9
    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "Alert Queue is not null or empty but senderList is, this should not be possible."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 679
    :cond_a
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {p0, v7}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getMessagesForWidget(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v5, v6, v0, v7, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto/16 :goto_1
.end method

.method private noValidNextMessage()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 689
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isPhoneDrivingModeEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 690
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_verbose:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 696
    .local v0, "displayNoNext":Ljava/lang/String;
    :goto_0
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    if-eqz v4, :cond_1

    .line 697
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getDisplayNameOrAddress()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 698
    .local v3, "ttsName":Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 699
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v3, v5, v7

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 711
    .end local v3    # "ttsName":Ljava/lang/String;
    .local v2, "spokenNoNext":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->DEFAULT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v5}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 712
    const/4 v4, 0x0

    invoke-virtual {p0, v0, v2, v8, v4}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 713
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->reset()V

    .line 714
    return-void

    .line 692
    .end local v0    # "displayNoNext":Ljava/lang/String;
    .end local v2    # "spokenNoNext":Ljava/lang/String;
    :cond_0
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_regular:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "displayNoNext":Ljava/lang/String;
    goto :goto_0

    .line 700
    :cond_1
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v4, :cond_3

    .line 702
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 703
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 707
    .local v1, "name":Ljava/lang/String;
    :goto_2
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 708
    .restart local v2    # "spokenNoNext":Ljava/lang/String;
    goto :goto_1

    .line 705
    .end local v1    # "name":Ljava/lang/String;
    .end local v2    # "spokenNoNext":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "name":Ljava/lang/String;
    goto :goto_2

    .line 709
    .end local v1    # "name":Ljava/lang/String;
    :cond_3
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v8, [Ljava/lang/Object;

    const-string/jumbo v6, ""

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "spokenNoNext":Ljava/lang/String;
    goto :goto_1
.end method

.method private prev()V
    .locals 1

    .prologue
    .line 745
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->moveNext(I)V

    .line 746
    return-void
.end method


# virtual methods
.method call()V
    .locals 5

    .prologue
    .line 765
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 766
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/events/ActionCallEvent;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getAddress()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getFieldId()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->getFieldId()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/events/ActionCallEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 767
    .local v1, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCallEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v2, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 776
    .end local v1    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCallEvent;
    :cond_0
    :goto_0
    return-void

    .line 768
    :cond_1
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 769
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 770
    .local v0, "currentMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/events/ActionCallEvent;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getFieldId()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->getFieldId()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/events/ActionCallEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    .restart local v1    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCallEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v2, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    goto :goto_0
.end method

.method protected displayMultipleSenders()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 577
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 610
    :cond_0
    :goto_0
    return-void

    .line 580
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeShowMessageBody()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    .line 584
    .local v0, "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-static {v4}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->getTotalMessagesFromSenderQueue(Ljava/util/HashMap;)I

    move-result v3

    .line 586
    .local v3, "totalMessages":I
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getRegularWidgetMax()I

    move-result v5

    if-gt v4, v5, :cond_2

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getDrivingModeWidgetMax()I

    move-result v5

    if-le v4, v5, :cond_4

    .line 587
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "ru-RU"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 588
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_overflow_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p0, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getPhraseNewMessagesRussian(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 596
    .local v2, "spokenMultiSenderText":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_READBACK_SENDERCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v5}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 597
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isPhoneDrivingModeEnabled()Z

    move-result v4

    if-nez v4, :cond_5

    .line 598
    invoke-virtual {p0, v10, v2, v7, v10}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 608
    :goto_2
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 609
    .local v1, "pageSenderList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleSenderMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {v4, v5, v0, v1, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto :goto_0

    .line 590
    .end local v1    # "pageSenderList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    .end local v2    # "spokenMultiSenderText":Ljava/lang/String;
    :cond_3
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_overflow_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "spokenMultiSenderText":Ljava/lang/String;
    goto :goto_1

    .line 593
    .end local v2    # "spokenMultiSenderText":Ljava/lang/String;
    :cond_4
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-direct {p0, v4, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->generateVerboseMultiSenderSpoken(Ljava/util/LinkedList;I)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "spokenMultiSenderText":Ljava/lang/String;
    goto :goto_1

    .line 600
    :cond_5
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-direct {p0, v4, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->generateVerboseMultiSenderSpoken(Ljava/util/LinkedList;I)Ljava/lang/String;

    move-result-object v2

    .line 601
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 602
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->ttsAnyway(Ljava/lang/String;)V

    goto :goto_2

    .line 604
    :cond_6
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    .line 605
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->startReco()Z

    goto :goto_2
.end method

.method protected displaySingleMessage(Z)V
    .locals 8
    .param p1, "initial"    # Z

    .prologue
    .line 422
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-static {v4}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 444
    :cond_0
    :goto_0
    return-void

    .line 426
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    check-cast v1, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 427
    .local v1, "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    const/4 v0, 0x0

    .line 428
    .local v0, "hasNext":Z
    const/4 v3, 0x0

    .line 430
    .local v3, "unreadCount":I
    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "displaySingleMessage :  id = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, ", type = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, ", body = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_4

    :cond_2
    const-string/jumbo v4, "empty"

    :goto_1
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 433
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    iget v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->messagePosition:I

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->hasNext(Ljava/util/LinkedList;I)Z

    move-result v0

    .line 434
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    iget v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->messagePosition:I

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->getUnreadCount(Ljava/util/LinkedList;I)I

    move-result v3

    .line 436
    :cond_3
    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->getMessageTypeFromAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;Z)Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    move-result-object v2

    .line 437
    .local v2, "mtForWidget":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->setUnreadCount(I)V

    .line 439
    if-eqz p1, :cond_5

    .line 440
    invoke-direct {p0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->displayInitialSingleMessage(Lcom/vlingo/core/internal/messages/SMSMMSAlert;Lcom/vlingo/core/internal/dialogmanager/types/MessageType;)V

    goto :goto_0

    .line 430
    .end local v2    # "mtForWidget":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    :cond_4
    const-string/jumbo v4, "not empty"

    goto :goto_1

    .line 442
    .restart local v2    # "mtForWidget":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    :cond_5
    invoke-direct {p0, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->displayNonInitialSingleMessage(ZLcom/vlingo/core/internal/dialogmanager/types/MessageType;)V

    goto/16 :goto_0
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 6
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 90
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/StateController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 93
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->CALL:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->isEnabled()Z

    move-result v1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->isDialingEnabled:Z

    .line 94
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "LPAction"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 162
    :goto_0
    return v4

    .line 100
    :cond_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "SafereaderReply"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 101
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->reply()V

    goto :goto_0

    .line 105
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v1, :cond_2

    .line 106
    invoke-virtual {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->displaySingleMessage(Z)V

    goto :goto_0

    .line 110
    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 111
    invoke-direct {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->multipleMessages(Z)V

    goto :goto_0

    .line 115
    :cond_3
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderQueue:Ljava/util/HashMap;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 120
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    .line 121
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 122
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->sortMessageReadoutList(Ljava/util/List;)Ljava/util/List;

    .line 125
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-le v1, v5, :cond_4

    .line 126
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->displayMultipleSenders()V

    goto :goto_0

    .line 132
    :cond_4
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v1, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 133
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessagesFromSender()Ljava/util/LinkedList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    .line 137
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-interface {v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 138
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    new-instance v2, Lcom/vlingo/core/internal/util/ListControlData;

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    invoke-direct {v2, v3}, Lcom/vlingo/core/internal/util/ListControlData;-><init>(I)V

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/util/ListControlData;)V

    .line 139
    iget-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->isReplyMode:Z

    if-eqz v1, :cond_5

    .line 140
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v1, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 144
    .local v0, "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 145
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->generateSingleList(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/util/LinkedList;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 146
    iput-boolean v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->isReplyMode:Z

    .line 147
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->reply()V

    goto/16 :goto_0

    .line 151
    .end local v0    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :cond_5
    invoke-direct {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->multipleMessages(Z)V

    goto/16 :goto_0

    .line 159
    :cond_6
    invoke-direct {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->noValidMessages(Z)V

    goto/16 :goto_0
.end method

.method public getAlertQueue()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 910
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    return-object v0
.end method

.method public getMessagesForWidget(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 790
    .local p1, "alerts":Ljava/util/List;, "Ljava/util/List<+Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 791
    .local v3, "msgList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageType;>;"
    if-eqz p1, :cond_1

    .line 792
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 793
    .local v1, "alert":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 794
    const/4 v4, 0x1

    invoke-static {v1, v4}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->getMessageTypeFromAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;Z)Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    move-result-object v0

    .line 795
    .local v0, "aMessageType":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 799
    .end local v0    # "aMessageType":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    .end local v1    # "alert":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    return-object v3
.end method

.method protected getPhraseNewMessagesRussian(I)Ljava/lang/String;
    .locals 2
    .param p1, "number"    # I

    .prologue
    const/4 v1, 0x0

    .line 873
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->PATTERN_FOR_RUSSIAN_1:Ljava/util/regex/Pattern;

    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->isNewMessagesForRussian(ILjava/util/regex/Pattern;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_new_messages_for_russian_1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 875
    :goto_0
    return-object v0

    .line 874
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->PATTERN_FOR_RUSSIAN_2:Ljava/util/regex/Pattern;

    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->isNewMessagesForRussian(ILjava/util/regex/Pattern;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_new_messages_for_russian_2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 875
    :cond_1
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_new_messages_for_russian_3:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getRuleMappings()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 898
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRules()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 894
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSenderList()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 902
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    return-object v0
.end method

.method public getSenderQueue()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 906
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderQueue:Ljava/util/HashMap;

    return-object v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v7, -0x1

    const/4 v8, 0x0

    .line 167
    if-nez p1, :cond_1

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 171
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "com.vlingo.core.internal.dialogmanager.Readout"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 172
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 174
    invoke-virtual {p0, v8}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->displaySingleMessage(Z)V

    goto :goto_0

    .line 175
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "com.vlingo.core.internal.dialogmanager.Reply"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 176
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 177
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->reply()V

    goto :goto_0

    .line 178
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "com.vlingo.core.internal.dialogmanager.Next"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 179
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 180
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->next()V

    goto :goto_0

    .line 181
    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "com.vlingo.core.internal.dialogmanager.Prev"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 182
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 183
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->prev()V

    goto :goto_0

    .line 184
    :cond_5
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "com.vlingo.core.internal.dialogmanager.Forward"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 185
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 186
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->forward()V

    goto :goto_0

    .line 187
    :cond_6
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "com.vlingo.core.internal.dialogmanager.Call"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 188
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 189
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->call()V

    goto/16 :goto_0

    .line 190
    :cond_7
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_8

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "com.vlingo.core.internal.dialogmanager.DirectReply"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 193
    :cond_8
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 194
    const-string/jumbo v5, "id"

    invoke-virtual {p1, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 195
    .local v2, "position":I
    const-string/jumbo v5, "message_type"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 197
    .local v4, "type":Ljava/lang/String;
    if-eq v2, v7, :cond_0

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 204
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_a

    .line 208
    :try_start_0
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v5, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    .local v0, "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :goto_1
    if-eqz v0, :cond_0

    .line 221
    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 222
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->generateSingleList(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/util/LinkedList;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 223
    iput v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->messagePosition:I

    .line 225
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "com.vlingo.core.internal.dialogmanager.DirectReply"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 226
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->reply()V

    goto/16 :goto_0

    .line 209
    .end local v0    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :catch_0
    move-exception v1

    .line 210
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 211
    const/4 v0, 0x0

    .restart local v0    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    goto :goto_1

    .line 228
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_9
    invoke-virtual {p0, v8}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->displaySingleMessage(Z)V

    goto/16 :goto_0

    .line 231
    .end local v0    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :cond_a
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 233
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v5, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    iput-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 234
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    if-eqz v5, :cond_0

    .line 239
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessagesFromSender()Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/LinkedList;

    .line 240
    .local v3, "selectedMessageQueue":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 245
    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    .line 246
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    new-instance v6, Lcom/vlingo/core/internal/util/ListControlData;

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v7

    invoke-direct {v6, v7}, Lcom/vlingo/core/internal/util/ListControlData;-><init>(I)V

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/util/ListControlData;)V

    .line 247
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v5, v8}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    iput-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 248
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 249
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    check-cast v5, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-direct {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->generateSingleList(Lcom/vlingo/core/internal/messages/SMSMMSAlert;)Ljava/util/LinkedList;

    move-result-object v5

    invoke-interface {v6, v7, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 252
    :cond_b
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "com.vlingo.core.internal.dialogmanager.DirectReply"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 253
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->reply()V

    goto/16 :goto_0

    .line 255
    :cond_c
    invoke-virtual {p0, v8}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->displaySingleMessage(Z)V

    goto/16 :goto_0
.end method

.method public handleLPAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 6
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 266
    const-string/jumbo v3, "Action"

    invoke-static {p1, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 267
    .local v0, "actionValue":Ljava/lang/String;
    if-nez v0, :cond_0

    move v3, v4

    .line 323
    :goto_0
    return v3

    .line 270
    :cond_0
    const-string/jumbo v3, "message:read"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 272
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v3, :cond_2

    .line 273
    invoke-virtual {p0, v4}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->displaySingleMessage(Z)V

    :cond_1
    :goto_1
    move v3, v5

    .line 283
    goto :goto_0

    .line 275
    :cond_2
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 277
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 278
    invoke-virtual {p0, v4}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->displaySingleMessage(Z)V

    goto :goto_1

    .line 280
    :cond_3
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 281
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->displayMultipleSenders()V

    goto :goto_1

    .line 284
    :cond_4
    const-string/jumbo v3, "safereader:reply"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 285
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->reply()V

    move v3, v5

    .line 286
    goto :goto_0

    .line 287
    :cond_5
    const-string/jumbo v3, "message:next"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 289
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->next()V

    move v3, v5

    .line 290
    goto :goto_0

    .line 291
    :cond_6
    const-string/jumbo v3, "message:prev"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 293
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->prev()V

    move v3, v5

    .line 294
    goto :goto_0

    .line 295
    :cond_7
    const-string/jumbo v3, "message:choose"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 297
    const-string/jumbo v3, "Which"

    invoke-static {p1, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 298
    .local v2, "which":Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 299
    invoke-direct {p0, v2}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->handleWhich(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    move v3, v5

    .line 300
    goto/16 :goto_0

    .line 305
    :cond_8
    const-string/jumbo v3, "Contact"

    invoke-static {p1, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 306
    .local v1, "contactName":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 307
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {p0, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->handleContact(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_9

    move v3, v5

    .line 308
    goto/16 :goto_0

    .line 312
    :cond_9
    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->noValidMessages(Z)V

    move v3, v5

    .line 313
    goto/16 :goto_0

    .line 316
    .end local v1    # "contactName":Ljava/lang/String;
    .end local v2    # "which":Ljava/lang/String;
    :cond_a
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v3, :cond_c

    .line 317
    invoke-virtual {p0, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->displaySingleMessage(Z)V

    :cond_b
    :goto_2
    move v3, v5

    .line 323
    goto/16 :goto_0

    .line 318
    :cond_c
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v3, :cond_d

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_d

    .line 319
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->displayMultipleMessages()V

    goto :goto_2

    .line 320
    :cond_d
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_b

    .line 321
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->displayMultipleSenders()V

    goto :goto_2
.end method

.method public isSilentMode()Z
    .locals 1

    .prologue
    .line 880
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->isSilentMode:Z

    return v0
.end method

.method protected isWithoutAskingMsg()Z
    .locals 1

    .prologue
    .line 417
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isPhoneDrivingModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isViewCoverOpened()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method reply()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 749
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->isSMSMMSAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 750
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getId()J

    move-result-wide v1

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getAddress()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getFieldId()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->getFieldId()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 762
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;
    :goto_0
    return-void

    .line 752
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 753
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    invoke-virtual {v1, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 754
    .local v6, "currentMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getId()J

    move-result-wide v1

    invoke-virtual {v6}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getAddress()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getFieldId()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->getFieldId()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 755
    .restart local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    goto :goto_0

    .line 758
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionReplyEvent;
    .end local v6    # "currentMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NO_REPLY:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setCurrentContactSearch(Ljava/lang/String;)V
    .locals 0
    .param p1, "currentContactSearch"    # Ljava/lang/String;

    .prologue
    .line 888
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentContactSearch:Ljava/lang/String;

    .line 889
    return-void
.end method

.method public setReplyMsgMode(Z)V
    .locals 0
    .param p1, "replyMode"    # Z

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->isReplyMode:Z

    .line 83
    return-void
.end method

.method public setSenderQueue(Ljava/util/HashMap;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "senderQueue":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    const/4 v3, 0x0

    .line 70
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderQueue:Ljava/util/HashMap;

    .line 71
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderQueue:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    new-instance v1, Lcom/vlingo/core/internal/util/ListControlData;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-direct {v1, v2}, Lcom/vlingo/core/internal/util/ListControlData;-><init>(I)V

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/util/ListControlData;)V

    .line 75
    :cond_0
    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->alertQueue:Ljava/util/LinkedList;

    .line 76
    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->senderList:Ljava/util/LinkedList;

    .line 77
    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentAlert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 78
    iput-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->currentSender:Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 79
    return-void
.end method

.method public setSilentMode(Z)V
    .locals 0
    .param p1, "silentMode"    # Z

    .prologue
    .line 884
    iput-boolean p1, p0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->isSilentMode:Z

    .line 885
    return-void
.end method
