.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "IntentHandler.java"


# static fields
.field public static final ERROR_RUN_CURRENT_VLINGO_APP:Ljava/lang/String; = "run current vlingo app"


# instance fields
.field private execPackage:Ljava/lang/String;

.field private isOldVvs:Z

.field private launchActivityAction:Lcom/vlingo/core/internal/dialogmanager/DMAction;

.field private packageParam:Ljava/lang/String;

.field private spokenForm:Ljava/lang/String;

.field private systemTurnTTS:Ljava/lang/String;

.field private systemTurnText:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method

.method private tryLaunchApp(Ljava/lang/String;)Z
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 179
    new-instance v0, Lcom/vlingo/core/internal/util/OpenAppUtil;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/OpenAppUtil;-><init>()V

    .line 180
    .local v0, "oau":Lcom/vlingo/core/internal/util/OpenAppUtil;
    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/util/OpenAppUtil;->buildMatchingAppList(Ljava/lang/String;)V

    .line 181
    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/OpenAppUtil;->getAppInfoList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v2, :cond_0

    .line 184
    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/OpenAppUtil;->getAppInfoList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    invoke-virtual {p0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->launchAppAction(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)V

    move v1, v2

    .line 188
    :cond_0
    return v1
.end method


# virtual methods
.method public actionFail(Ljava/lang/String;)V
    .locals 6
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 206
    invoke-super {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->actionFail(Ljava/lang/String;)V

    .line 207
    const-string/jumbo v1, "run current vlingo app"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 210
    iget-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->isOldVvs:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->spokenForm:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 211
    :cond_0
    const-string/jumbo v1, "Spoken Form in Old"

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->spokenForm:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_APPMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 225
    .local v0, "spokenText":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 227
    .end local v0    # "spokenText":Ljava/lang/String;
    :cond_1
    return-void

    .line 216
    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->packageParam:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->packageParam:Ljava/lang/String;

    const-string/jumbo v2, "com.sec.android.app.camera"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 217
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_SPOKEN_APPMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->spokenForm:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "spokenText":Ljava/lang/String;
    goto :goto_0

    .line 220
    .end local v0    # "spokenText":Ljava/lang/String;
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_SPOKEN_APPMATCH_DEMAND_FOR_CAMERA_ONLY:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "spokenText":Ljava/lang/String;
    goto :goto_0
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 21
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 47
    invoke-super/range {p0 .. p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 49
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->launchActivityAction:Lcom/vlingo/core/internal/dialogmanager/DMAction;

    .line 51
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->isOldVvs:Z

    .line 52
    const-string/jumbo v18, "ExecName"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    .line 53
    .local v7, "execName":Ljava/lang/String;
    const-string/jumbo v18, "ExecPackage"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->packageParam:Ljava/lang/String;

    .line 54
    const-string/jumbo v18, "ExecPackage"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->execPackage:Ljava/lang/String;

    .line 55
    const-string/jumbo v18, "action.prompt"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->systemTurnText:Ljava/lang/String;

    .line 56
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->systemTurnText:Ljava/lang/String;

    move-object/from16 v18, v0

    if-nez v18, :cond_0

    .line 57
    const-string/jumbo v18, "TTS"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->systemTurnText:Ljava/lang/String;

    .line 60
    :cond_0
    const-string/jumbo v18, "action.prompt.spoken"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->systemTurnTTS:Ljava/lang/String;

    .line 61
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->systemTurnTTS:Ljava/lang/String;

    move-object/from16 v18, v0

    if-nez v18, :cond_1

    .line 62
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->systemTurnText:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->systemTurnTTS:Ljava/lang/String;

    .line 65
    :cond_1
    const-string/jumbo v18, "SpokenForm"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->spokenForm:Ljava/lang/String;

    .line 67
    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->execPackage:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_9

    .line 68
    const-string/jumbo v18, "AppName"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 69
    .local v4, "appName":Ljava/lang/String;
    const-string/jumbo v18, "Extras"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v10

    .line 70
    .local v10, "extra":Ljava/lang/String;
    const-string/jumbo v18, "ExecAction"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v12

    .line 72
    .local v12, "intentAction":Ljava/lang/String;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 73
    .local v8, "execNameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .local v9, "execPackageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v11, 0x1

    .line 77
    .local v11, "i":I
    :goto_0
    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->execPackage:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_2

    .line 78
    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->execPackage:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "ExecName"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    .line 82
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "ExecPackage"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->execPackage:Ljava/lang/String;

    .line 83
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 86
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "launch "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 93
    const-string/jumbo v18, "validate_launch_intent_version"

    const/16 v19, 0x1

    invoke-static/range {v18 .. v19}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v18

    if-eqz v18, :cond_4

    const/16 v17, 0x1

    .line 95
    .local v17, "versionLimit":I
    :goto_1
    const/4 v11, 0x0

    .line 97
    invoke-interface {v8, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "execName":Ljava/lang/String;
    check-cast v7, Ljava/lang/String;

    .line 98
    .restart local v7    # "execName":Ljava/lang/String;
    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->execPackage:Ljava/lang/String;

    .line 100
    :goto_2
    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->execPackage:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->launchActivityAction:Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-object/from16 v18, v0

    if-nez v18, :cond_6

    .line 101
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->execPackage:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-static {v0, v7, v1}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 102
    sget-object v18, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->LAUNCH_ACTIVITY:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v19, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v18

    check-cast v18, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->execPackage:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->enclosingPackage(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->activity(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->extra(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->action(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->app(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->launchActivityAction:Lcom/vlingo/core/internal/dialogmanager/DMAction;

    .line 109
    :cond_3
    add-int/lit8 v11, v11, 0x1

    .line 110
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    if-le v0, v11, :cond_5

    .line 111
    invoke-interface {v8, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "execName":Ljava/lang/String;
    check-cast v7, Ljava/lang/String;

    .line 112
    .restart local v7    # "execName":Ljava/lang/String;
    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->execPackage:Ljava/lang/String;

    goto :goto_2

    .line 93
    .end local v17    # "versionLimit":I
    :cond_4
    const/high16 v17, -0x80000000

    goto/16 :goto_1

    .line 114
    .restart local v17    # "versionLimit":I
    :cond_5
    const/4 v7, 0x0

    .line 115
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->execPackage:Ljava/lang/String;

    goto/16 :goto_2

    .line 119
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->launchActivityAction:Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-object/from16 v18, v0

    if-nez v18, :cond_d

    .line 120
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->spokenForm:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 121
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->spokenForm:Ljava/lang/String;

    .line 124
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->spokenForm:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->tryLaunchApp(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 125
    const/16 v18, 0x0

    .line 174
    .end local v4    # "appName":Ljava/lang/String;
    .end local v8    # "execNameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v9    # "execPackageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v11    # "i":I
    .end local v12    # "intentAction":Ljava/lang/String;
    .end local v17    # "versionLimit":I
    :goto_3
    return v18

    .line 128
    .restart local v4    # "appName":Ljava/lang/String;
    .restart local v8    # "execNameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v9    # "execPackageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v11    # "i":I
    .restart local v12    # "intentAction":Ljava/lang/String;
    .restart local v17    # "versionLimit":I
    :cond_8
    const-string/jumbo v18, "Not found"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->actionFail(Ljava/lang/String;)V

    .line 130
    const/16 v18, 0x0

    goto :goto_3

    .line 134
    .end local v4    # "appName":Ljava/lang/String;
    .end local v8    # "execNameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v9    # "execPackageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v10    # "extra":Ljava/lang/String;
    .end local v11    # "i":I
    .end local v12    # "intentAction":Ljava/lang/String;
    .end local v17    # "versionLimit":I
    :cond_9
    const-string/jumbo v18, "IntentName"

    const/16 v19, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v15

    .line 135
    .local v15, "name":Ljava/lang/String;
    const-string/jumbo v18, "IntentArgument"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    .line 136
    .local v5, "arg":Ljava/lang/String;
    const-string/jumbo v18, "Extras"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v10

    .line 137
    .restart local v10    # "extra":Ljava/lang/String;
    const-string/jumbo v18, "ClassName"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 138
    .local v6, "className":Ljava/lang/String;
    const-string/jumbo v18, "Type"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v16

    .line 139
    .local v16, "type":Ljava/lang/String;
    const-string/jumbo v18, "broadcast"

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-static {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v13

    .line 140
    .local v13, "isBroadcast":Z
    const-string/jumbo v18, "service"

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-static {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v14

    .line 142
    .local v14, "isService":Z
    sget-object v18, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v19, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v18

    check-cast v18, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->argument(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->extra(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->className(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->broadcast(Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->service(Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->type(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->launchActivityAction:Lcom/vlingo/core/internal/dialogmanager/DMAction;

    .line 151
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->launchActivityAction:Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-object/from16 v18, v0

    check-cast v18, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->isAvailable()Z

    move-result v18

    if-nez v18, :cond_c

    const-string/jumbo v18, "com.sec.android.app.music.musicservicecommand.pause"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_c

    .line 152
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->spokenForm:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_a

    .line 153
    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->spokenForm:Ljava/lang/String;

    .line 154
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->isOldVvs:Z

    .line 156
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->spokenForm:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->tryLaunchApp(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_b

    .line 157
    const/16 v18, 0x0

    goto/16 :goto_3

    .line 159
    :cond_b
    const-string/jumbo v18, "Not found"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->actionFail(Ljava/lang/String;)V

    .line 161
    const/16 v18, 0x0

    goto/16 :goto_3

    .line 164
    :cond_c
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "execute "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 166
    .end local v5    # "arg":Ljava/lang/String;
    .end local v6    # "className":Ljava/lang/String;
    .end local v13    # "isBroadcast":Z
    .end local v14    # "isService":Z
    .end local v15    # "name":Ljava/lang/String;
    .end local v16    # "type":Ljava/lang/String;
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->launchActivityAction:Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-object/from16 v18, v0

    if-eqz v18, :cond_f

    .line 167
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->systemTurnText:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_e

    .line 168
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->systemTurnText:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->systemTurnTTS:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-interface/range {v18 .. v20}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->launchActivityAction:Lcom/vlingo/core/internal/dialogmanager/DMAction;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/dialogmanager/DMAction;->queue()V

    .line 171
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->launchActivityAction:Lcom/vlingo/core/internal/dialogmanager/DMAction;

    .line 172
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishDialog()V

    .line 174
    :cond_f
    const/16 v18, 0x0

    goto/16 :goto_3
.end method

.method public launchAppAction(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)V
    .locals 4
    .param p1, "appInfo"    # Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .prologue
    .line 192
    const-string/jumbo v0, "launch"

    .line 193
    .local v0, "landingPageId":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 194
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 196
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->systemTurnText:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 197
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->systemTurnText:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->systemTurnTTS:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 200
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->OPEN_APP:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v2, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/IntentHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;

    invoke-virtual {v1, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;->appInfo(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;->queue()V

    .line 203
    return-void
.end method
