.class public final enum Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;
.super Ljava/lang/Enum;
.source "ServerHostUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/associatedservice/ServerHostUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HostType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

.field public static final enum ASR:Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

.field public static final enum HELLO:Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

.field public static final enum LOG:Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

.field public static final enum VCS:Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 6
    new-instance v0, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

    const-string/jumbo v1, "ASR"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;->ASR:Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

    new-instance v0, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

    const-string/jumbo v1, "VCS"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;->VCS:Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

    new-instance v0, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

    const-string/jumbo v1, "LOG"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;->LOG:Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

    new-instance v0, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

    const-string/jumbo v1, "HELLO"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;->HELLO:Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

    sget-object v1, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;->ASR:Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;->VCS:Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;->LOG:Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;->HELLO:Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;->$VALUES:[Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 6
    const-class v0, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;->$VALUES:[Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/associatedservice/ServerHostUtil$HostType;

    return-object v0
.end method
