.class public Lcom/vlingo/core/internal/contacts/scoring/DoubleMetaphoneScore;
.super Lcom/vlingo/core/internal/contacts/scoring/ContactScore;
.source "DoubleMetaphoneScore.java"


# instance fields
.field private fallback:I

.field numbersPattern:Ljava/util/regex/Pattern;

.field private score:I


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1, "score"    # I
    .param p2, "fallback"    # I

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/scoring/ContactScore;-><init>()V

    .line 21
    const-string/jumbo v0, "^(\\d+)?.*?(\\d+)?$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/scoring/DoubleMetaphoneScore;->numbersPattern:Ljava/util/regex/Pattern;

    .line 16
    iput p1, p0, Lcom/vlingo/core/internal/contacts/scoring/DoubleMetaphoneScore;->score:I

    .line 17
    iput p2, p0, Lcom/vlingo/core/internal/contacts/scoring/DoubleMetaphoneScore;->fallback:I

    .line 19
    return-void
.end method


# virtual methods
.method public getScore(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactMatch;)I
    .locals 4
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "match"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 26
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/scoring/DoubleMetaphoneScore;->numbersPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 27
    .local v0, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 29
    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 30
    iget v1, p0, Lcom/vlingo/core/internal/contacts/scoring/DoubleMetaphoneScore;->fallback:I

    .line 45
    :goto_0
    return v1

    .line 33
    :cond_0
    invoke-virtual {v0, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 34
    iget v1, p0, Lcom/vlingo/core/internal/contacts/scoring/DoubleMetaphoneScore;->fallback:I

    goto :goto_0

    .line 45
    :cond_1
    iget v1, p0, Lcom/vlingo/core/internal/contacts/scoring/DoubleMetaphoneScore;->score:I

    goto :goto_0
.end method
