.class public Lcom/vlingo/core/internal/dialogmanager/DefaultDialogFlowListener;
.super Ljava/lang/Object;
.source "DefaultDialogFlowListener.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public enteredDomain(Lcom/vlingo/core/internal/domain/DomainName;)V
    .locals 0
    .param p1, "domain"    # Lcom/vlingo/core/internal/domain/DomainName;

    .prologue
    .line 101
    return-void
.end method

.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    return-object v0
.end method

.method public onASRRecorderClosed()V
    .locals 0

    .prologue
    .line 89
    return-void
.end method

.method public onASRRecorderOpened()V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method public onInterceptStartReco()Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return v0
.end method

.method public onRecoCancelled()V
    .locals 0

    .prologue
    .line 58
    return-void
.end method

.method public onRecoToneStarting(Z)J
    .locals 2
    .param p1, "startTone"    # Z

    .prologue
    .line 76
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public onRecoToneStopped(Z)V
    .locals 0
    .param p1, "startTone"    # Z

    .prologue
    .line 95
    return-void
.end method

.method public onResultsNoAction()V
    .locals 0

    .prologue
    .line 54
    return-void
.end method

.method public showDialogFlowStateChange(Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;)V
    .locals 0
    .param p1, "newState"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    .prologue
    .line 42
    return-void
.end method

.method public showError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V
    .locals 0
    .param p1, "error"    # Lcom/vlingo/sdk/recognition/VLRecognitionErrors;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 21
    return-void
.end method

.method public showRMSChange(I)V
    .locals 0
    .param p1, "rmsValue"    # I

    .prologue
    .line 30
    return-void
.end method

.method public showReceivedResults(Lcom/vlingo/core/internal/logging/EventLog;)V
    .locals 0
    .param p1, "results"    # Lcom/vlingo/core/internal/logging/EventLog;

    .prologue
    .line 34
    return-void
.end method

.method public showRecoStateChange(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V
    .locals 0
    .param p1, "newState"    # Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    .prologue
    .line 38
    return-void
.end method

.method public showUserText(Ljava/lang/String;Lcom/vlingo/sdk/recognition/NBestData;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "nbest"    # Lcom/vlingo/sdk/recognition/NBestData;

    .prologue
    .line 50
    return-void
.end method

.method public showVlingoText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 46
    return-void
.end method

.method public showWarning(Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;
    .locals 1
    .param p1, "warning"    # Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 25
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;->Noop:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;

    return-object v0
.end method

.method public userCancel()V
    .locals 0

    .prologue
    .line 71
    return-void
.end method
