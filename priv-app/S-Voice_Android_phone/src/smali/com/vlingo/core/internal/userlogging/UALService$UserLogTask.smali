.class Lcom/vlingo/core/internal/userlogging/UALService$UserLogTask;
.super Lcom/vlingo/core/internal/util/TaskQueue$Task;
.source "UALService.java"

# interfaces
.implements Lcom/vlingo/sdk/services/VLServicesListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/userlogging/UALService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UserLogTask"
.end annotation


# static fields
.field private static final MAX_RETRIES:I = 0x5

.field private static final WAIT_TIME:J = 0x1f4L


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/userlogging/UALService;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/userlogging/UALService;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/vlingo/core/internal/userlogging/UALService$UserLogTask;->this$0:Lcom/vlingo/core/internal/userlogging/UALService;

    invoke-direct {p0}, Lcom/vlingo/core/internal/util/TaskQueue$Task;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/userlogging/UALService;Lcom/vlingo/core/internal/userlogging/UALService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/userlogging/UALService;
    .param p2, "x1"    # Lcom/vlingo/core/internal/userlogging/UALService$1;

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/userlogging/UALService$UserLogTask;-><init>(Lcom/vlingo/core/internal/userlogging/UALService;)V

    return-void
.end method

.method private done()V
    .locals 2

    .prologue
    .line 182
    iget-object v1, p0, Lcom/vlingo/core/internal/userlogging/UALService$UserLogTask;->this$0:Lcom/vlingo/core/internal/userlogging/UALService;

    monitor-enter v1

    .line 183
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/userlogging/UALService$UserLogTask;->this$0:Lcom/vlingo/core/internal/userlogging/UALService;

    # getter for: Lcom/vlingo/core/internal/userlogging/UALService;->mTaskQueue:Lcom/vlingo/core/internal/util/TaskQueue;
    invoke-static {v0}, Lcom/vlingo/core/internal/userlogging/UALService;->access$000(Lcom/vlingo/core/internal/userlogging/UALService;)Lcom/vlingo/core/internal/util/TaskQueue;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/userlogging/UALService$UserLogTask;->this$0:Lcom/vlingo/core/internal/userlogging/UALService;

    # getter for: Lcom/vlingo/core/internal/userlogging/UALService;->mTaskQueue:Lcom/vlingo/core/internal/util/TaskQueue;
    invoke-static {v0}, Lcom/vlingo/core/internal/userlogging/UALService;->access$000(Lcom/vlingo/core/internal/userlogging/UALService;)Lcom/vlingo/core/internal/util/TaskQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/TaskQueue;->hasQueuedTask()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/userlogging/UALService$UserLogTask;->this$0:Lcom/vlingo/core/internal/userlogging/UALService;

    # getter for: Lcom/vlingo/core/internal/userlogging/UALService;->mTransmitTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/vlingo/core/internal/userlogging/UALService;->access$300(Lcom/vlingo/core/internal/userlogging/UALService;)Ljava/util/Timer;

    move-result-object v0

    if-nez v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/vlingo/core/internal/userlogging/UALService$UserLogTask;->this$0:Lcom/vlingo/core/internal/userlogging/UALService;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/userlogging/UALService;->stopSelf()V

    .line 186
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/userlogging/UALService$UserLogTask;->notifyFinished()V

    .line 187
    monitor-exit v1

    .line 188
    return-void

    .line 187
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public onError(Lcom/vlingo/sdk/services/VLServicesErrors;Ljava/lang/String;)V
    .locals 0
    .param p1, "error"    # Lcom/vlingo/sdk/services/VLServicesErrors;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 178
    invoke-direct {p0}, Lcom/vlingo/core/internal/userlogging/UALService$UserLogTask;->done()V

    .line 179
    return-void
.end method

.method public onSuccess(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 170
    .local p1, "actionList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/VLAction;>;"
    invoke-direct {p0}, Lcom/vlingo/core/internal/userlogging/UALService$UserLogTask;->done()V

    .line 171
    return-void
.end method

.method public run()V
    .locals 10

    .prologue
    .line 143
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->flushUserLogRecord()Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;

    move-result-object v4

    .line 144
    .local v4, "logRecord":Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v3

    .line 145
    .local v3, "language":Ljava/lang/String;
    const/4 v5, 0x0

    .line 146
    .local v5, "transmitSuccess":Z
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    const/4 v6, 0x5

    if-gt v2, v6, :cond_0

    if-nez v5, :cond_0

    .line 148
    :try_start_0
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/sdk/VLSdk;->getVLServices()Lcom/vlingo/sdk/services/VLServices;

    move-result-object v6

    invoke-interface {v6, v3, v4, p0}, Lcom/vlingo/sdk/services/VLServices;->sendActivityLog(Ljava/lang/String;Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;Lcom/vlingo/sdk/services/VLServicesListener;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    const/4 v5, 0x1

    .line 146
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 152
    :catch_0
    move-exception v1

    .line 156
    .local v1, "ex":Ljava/lang/IllegalStateException;
    const-wide/16 v6, 0x1f4

    int-to-long v8, v2

    mul-long/2addr v6, v8

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 157
    :catch_1
    move-exception v0

    .line 158
    .local v0, "e":Ljava/lang/InterruptedException;
    const-class v6, Lcom/vlingo/core/internal/userlogging/UALService$UserLogTask;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "waiting did not work, in UserLogTask"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 163
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v1    # "ex":Ljava/lang/IllegalStateException;
    :cond_0
    if-nez v5, :cond_1

    const-class v6, Lcom/vlingo/core/internal/userlogging/UALService$UserLogTask;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "activity log failed to transmit after 5 retries."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    :cond_1
    return-void
.end method
