.class public Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;
.super Lcom/vlingo/core/internal/contacts/scoring/ContactScore;
.source "KoreanPlusPTFContactScore.java"


# static fields
.field private static final SPLIT_REGEXP:Ljava/lang/String; = "[\\+\u00d7\u00f7=%_\u20ac\u00a3\u00a5\u20a9\\!@#\\$/\\^&\\*\\()\\-\'\"\\:;,\\?`~\\\\\\|<>\\{\\}\\[\\]\u00b0\u2022\u25cb\u25cf\u25a1\u25a0\u2664\u2661\u2662\u2667\u2606\u25aa\u00a4\u300a\u300b\u00a1\u00bf]"


# instance fields
.field private baseScore:I

.field private familiarityHonorific:Ljava/lang/String;

.field private hasSuffixes:Z

.field private postProposition:Ljava/lang/String;

.field private regexQuery:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "baseScore"    # I
    .param p2, "regexQuery"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "familiarityHonorific"    # Ljava/lang/String;
    .param p5, "postProposition"    # Ljava/lang/String;
    .param p6, "hasSuffixes"    # Z

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/scoring/ContactScore;-><init>()V

    .line 13
    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->title:Ljava/lang/String;

    .line 14
    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->familiarityHonorific:Ljava/lang/String;

    .line 15
    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->postProposition:Ljava/lang/String;

    .line 22
    iput p1, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->baseScore:I

    .line 23
    iput-object p2, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->regexQuery:Ljava/lang/String;

    .line 24
    iput-object p3, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->title:Ljava/lang/String;

    .line 25
    iput-object p4, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->familiarityHonorific:Ljava/lang/String;

    .line 26
    iput-object p5, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->postProposition:Ljava/lang/String;

    .line 27
    iput-boolean p6, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->hasSuffixes:Z

    .line 28
    return-void
.end method

.method private matchAllowNull(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "a"    # Ljava/lang/String;
    .param p2, "b"    # Ljava/lang/String;

    .prologue
    .line 65
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 66
    :cond_0
    const/4 v0, 0x1

    .line 68
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public getScore(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactMatch;)I
    .locals 13
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "match"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    const/4 v9, 0x0

    const/4 v12, 0x1

    .line 32
    iget-object v0, p2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 33
    .local v0, "companyFilteredName":Ljava/lang/String;
    const-string/jumbo v10, "[\\+\u00d7\u00f7=%_\u20ac\u00a3\u00a5\u20a9\\!@#\\$/\\^&\\*\\()\\-\'\"\\:;,\\?`~\\\\\\|<>\\{\\}\\[\\]\u00b0\u2022\u25cb\u25cf\u25a1\u25a0\u2664\u2661\u2662\u2667\u2606\u25aa\u00a4\u300a\u300b\u00a1\u00bf]"

    invoke-virtual {v0, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    aget-object v0, v10, v9

    .line 34
    iget-object v10, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->regexQuery:Ljava/lang/String;

    invoke-static {v10}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 35
    .local v1, "companyMatcher":Ljava/util/regex/Matcher;
    iget-object v10, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->regexQuery:Ljava/lang/String;

    invoke-static {v10}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v10

    iget-object v11, p2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 37
    .local v3, "regualarMatcher":Ljava/util/regex/Matcher;
    const/4 v2, 0x0

    .line 39
    .local v2, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 40
    move-object v2, v1

    .line 45
    :cond_0
    :goto_0
    if-eqz v2, :cond_4

    .line 46
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v10

    if-gt v10, v12, :cond_7

    move-object v7, v0

    .line 47
    .local v7, "strippedQuery":Ljava/lang/String;
    :goto_1
    const/4 v10, 0x2

    invoke-virtual {v2, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    .line 48
    .local v4, "strippedExtras":Ljava/lang/String;
    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    .line 49
    .local v8, "strippedTitle":Ljava/lang/String;
    const/4 v10, 0x4

    invoke-virtual {v2, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    .line 50
    .local v5, "strippedFamiliarityHonorific":Ljava/lang/String;
    const/4 v10, 0x5

    invoke-virtual {v2, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    .line 51
    .local v6, "strippedPostProposition":Ljava/lang/String;
    if-eqz v7, :cond_1

    iget-object v10, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->regexQuery:Ljava/lang/String;

    invoke-virtual {v7, v10}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_2

    :cond_1
    iget-object v10, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->regexQuery:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    :cond_2
    iget-object v10, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->title:Ljava/lang/String;

    invoke-direct {p0, v10, v8}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->matchAllowNull(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->familiarityHonorific:Ljava/lang/String;

    invoke-direct {p0, v10, v5}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->matchAllowNull(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->postProposition:Ljava/lang/String;

    invoke-direct {p0, v10, v6}, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->matchAllowNull(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    iget-boolean v10, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->hasSuffixes:Z

    if-eqz v10, :cond_3

    invoke-static {v4}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 56
    :cond_3
    iget v9, p0, Lcom/vlingo/core/internal/contacts/scoring/KoreanPlusPTFContactScore;->baseScore:I

    .line 60
    .end local v4    # "strippedExtras":Ljava/lang/String;
    .end local v5    # "strippedFamiliarityHonorific":Ljava/lang/String;
    .end local v6    # "strippedPostProposition":Ljava/lang/String;
    .end local v7    # "strippedQuery":Ljava/lang/String;
    .end local v8    # "strippedTitle":Ljava/lang/String;
    :cond_4
    return v9

    .line 41
    :cond_5
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v10

    if-nez v10, :cond_6

    iget-object v10, p2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    if-gt v10, v12, :cond_0

    .line 42
    :cond_6
    move-object v2, v3

    goto :goto_0

    .line 46
    :cond_7
    invoke-virtual {v2, v12}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_1
.end method
