.class public final Lcom/vlingo/core/internal/safereader/SafeReaderLogger;
.super Ljava/lang/Object;
.source "SafeReaderLogger.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private m_ClassName:Ljava/lang/String;

.field private m_isEnabled:Z

.field private m_prefix:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/vlingo/core/internal/safereader/SafeReaderLogger;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderLogger;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "prefix"    # Ljava/lang/String;
    .param p3, "enabled"    # Z

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderLogger;->m_isEnabled:Z

    .line 17
    const/16 v1, 0x2e

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 18
    .local v0, "i":I
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/safereader/SafeReaderLogger;->m_ClassName:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/vlingo/core/internal/safereader/SafeReaderLogger;->m_prefix:Ljava/lang/String;

    .line 20
    iput-boolean p3, p0, Lcom/vlingo/core/internal/safereader/SafeReaderLogger;->m_isEnabled:Z

    .line 21
    return-void
.end method

.method public static getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/safereader/SafeReaderLogger;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/vlingo/core/internal/safereader/SafeReaderLogger;"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "class1":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/vlingo/core/internal/safereader/SafeReaderLogger;->getLogger(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/core/internal/safereader/SafeReaderLogger;

    move-result-object v0

    return-object v0
.end method

.method public static getLogger(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/core/internal/safereader/SafeReaderLogger;
    .locals 1
    .param p1, "prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/vlingo/core/internal/safereader/SafeReaderLogger;"
        }
    .end annotation

    .prologue
    .line 32
    .local p0, "class1":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/vlingo/core/internal/safereader/SafeReaderLogger;->getLogger(Ljava/lang/Class;Ljava/lang/String;Z)Lcom/vlingo/core/internal/safereader/SafeReaderLogger;

    move-result-object v0

    return-object v0
.end method

.method public static getLogger(Ljava/lang/Class;Ljava/lang/String;Z)Lcom/vlingo/core/internal/safereader/SafeReaderLogger;
    .locals 2
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "enabled"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/vlingo/core/internal/safereader/SafeReaderLogger;"
        }
    .end annotation

    .prologue
    .line 36
    .local p0, "class1":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-nez p0, :cond_0

    .line 37
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 38
    :cond_0
    if-nez p1, :cond_1

    .line 39
    const-string/jumbo p1, "VAC_"

    .line 41
    :cond_1
    new-instance v0, Lcom/vlingo/core/internal/safereader/SafeReaderLogger;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2}, Lcom/vlingo/core/internal/safereader/SafeReaderLogger;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public static getLogger(Ljava/lang/Class;Z)Lcom/vlingo/core/internal/safereader/SafeReaderLogger;
    .locals 2
    .param p1, "enabled"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;Z)",
            "Lcom/vlingo/core/internal/safereader/SafeReaderLogger;"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "class1":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/vlingo/core/internal/safereader/SafeReaderLogger;->getLogger(Ljava/lang/Class;Ljava/lang/String;Z)Lcom/vlingo/core/internal/safereader/SafeReaderLogger;

    move-result-object v0

    return-object v0
.end method

.method private getThread()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public debug(Ljava/lang/String;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 50
    return-void
.end method

.method public error(Ljava/lang/String;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 72
    return-void
.end method

.method public error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "code"    # Ljava/lang/String;
    .param p3, "msg"    # Ljava/lang/String;

    .prologue
    .line 66
    return-void
.end method

.method public info(Ljava/lang/String;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 80
    return-void
.end method

.method public warn(Ljava/lang/String;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 58
    return-void
.end method
