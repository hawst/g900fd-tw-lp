.class Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$3;
.super Ljava/lang/Object;
.source "LMTTChunkComm.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->onUpdateReceived(Ljava/util/HashMap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

.field final synthetic val$serverCounts:Ljava/util/HashMap;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;Ljava/util/HashMap;)V
    .locals 0

    .prologue
    .line 262
    iput-object p1, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$3;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    iput-object p2, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$3;->val$serverCounts:Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 264
    # invokes: Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->updateTransmitTime()V
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->access$500()V

    .line 270
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$3;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mServerCounts:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->access$700(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$3;->val$serverCounts:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 272
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$3;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mActiveChunk:Lcom/vlingo/core/internal/lmtt/LMTTChunk;
    invoke-static {v0}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->access$300(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)Lcom/vlingo/core/internal/lmtt/LMTTChunk;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->updateState(Z)Lcom/vlingo/core/internal/lmtt/LMTTChunkState;

    .line 277
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$3;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    # getter for: Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->mActiveChunk:Lcom/vlingo/core/internal/lmtt/LMTTChunk;
    invoke-static {v0}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->access$300(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)Lcom/vlingo/core/internal/lmtt/LMTTChunk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmtt/LMTTChunk;->getItemList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/lmtt/LMTTDBUtil;->updateSynchedItems(Ljava/util/List;)Z

    .line 282
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm$3;->this$0:Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;

    # invokes: Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->handleNextChunk()V
    invoke-static {v0}, Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;->access$000(Lcom/vlingo/core/internal/lmtt/LMTTChunkComm;)V

    .line 283
    return-void
.end method
