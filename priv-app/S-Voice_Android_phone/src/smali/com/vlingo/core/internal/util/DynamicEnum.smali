.class public abstract Lcom/vlingo/core/internal/util/DynamicEnum;
.super Ljava/lang/Object;
.source "DynamicEnum.java"


# instance fields
.field private final KEY:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/vlingo/core/internal/util/DynamicEnum;->KEY:Ljava/lang/String;

    .line 14
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/util/DynamicEnum;->validate(Ljava/lang/String;)V

    .line 15
    return-void
.end method

.method private validate(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 19
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Key should not be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 21
    :cond_0
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 29
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/vlingo/core/internal/util/DynamicEnum;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/vlingo/core/internal/util/DynamicEnum;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v0, p1, Lcom/vlingo/core/internal/util/DynamicEnum;->KEY:Ljava/lang/String;

    iget-object v1, p0, Lcom/vlingo/core/internal/util/DynamicEnum;->KEY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/core/internal/util/DynamicEnum;->KEY:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/core/internal/util/DynamicEnum;->KEY:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vlingo/core/internal/util/DynamicEnum;->KEY:Ljava/lang/String;

    return-object v0
.end method
