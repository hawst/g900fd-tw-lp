.class Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator$1;
.super Ljava/lang/Object;
.source "SparseArrayMap.java"

# interfaces
.implements Ljava/util/Map$Entry;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;->next()Ljava/util/Map$Entry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Map$Entry",
        "<",
        "Ljava/lang/Integer;",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;)V
    .locals 0

    .prologue
    .line 81
    .local p0, "this":Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator$1;, "Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator.1;"
    iput-object p1, p0, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator$1;->this$1:Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getKey()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 84
    .local p0, "this":Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator$1;, "Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator.1;"
    iget-object v0, p0, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator$1;->this$1:Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;

    iget-object v0, v0, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;->this$0:Lcom/vlingo/core/internal/util/SparseArrayMap;

    iget-object v1, p0, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator$1;->this$1:Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;

    # getter for: Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;->position:I
    invoke-static {v1}, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;->access$100(Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/SparseArrayMap;->keyAt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 81
    .local p0, "this":Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator$1;, "Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator.1;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator$1;->getKey()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getValue()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 89
    .local p0, "this":Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator$1;, "Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator.1;"
    iget-object v0, p0, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator$1;->this$1:Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;

    iget-object v0, v0, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;->this$0:Lcom/vlingo/core/internal/util/SparseArrayMap;

    iget-object v1, p0, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator$1;->this$1:Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;

    # getter for: Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;->position:I
    invoke-static {v1}, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;->access$100(Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/SparseArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 94
    .local p0, "this":Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator$1;, "Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator.1;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator$1;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 95
    .local v0, "result":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator$1;->this$1:Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;

    iget-object v1, v1, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;->this$0:Lcom/vlingo/core/internal/util/SparseArrayMap;

    iget-object v2, p0, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator$1;->this$1:Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;

    # getter for: Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;->position:I
    invoke-static {v2}, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;->access$100(Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;)I

    move-result v2

    invoke-virtual {v1, v2, p1}, Lcom/vlingo/core/internal/util/SparseArrayMap;->setValueAt(ILjava/lang/Object;)V

    .line 96
    return-object v0
.end method
