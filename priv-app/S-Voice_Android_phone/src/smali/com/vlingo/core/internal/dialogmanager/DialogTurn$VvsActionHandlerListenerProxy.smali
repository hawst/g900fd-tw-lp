.class Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;
.super Ljava/lang/Object;
.source "DialogTurn.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VvsActionHandlerListenerProxy"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V
    .locals 0

    .prologue
    .line 334
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;Lcom/vlingo/core/internal/dialogmanager/DialogTurn$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn$1;

    .prologue
    .line 334
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    return-void
.end method


# virtual methods
.method public asyncHandlerDone()V
    .locals 1

    .prologue
    .line 605
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->turnWasCancelled:Z
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->access$300(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 606
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$18;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$18;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 618
    :cond_0
    return-void
.end method

.method public asyncHandlerStarted()V
    .locals 1

    .prologue
    .line 636
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->turnWasCancelled:Z
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->access$300(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 637
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$20;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$20;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 647
    :cond_0
    return-void
.end method

.method public endpointReco()V
    .locals 1

    .prologue
    .line 502
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$12;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$12;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 509
    return-void
.end method

.method public execute(Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;)V
    .locals 1
    .param p1, "action"    # Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    .prologue
    .line 396
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$5;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$5;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 403
    return-void
.end method

.method public finishDialog()V
    .locals 1

    .prologue
    .line 592
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$17;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$17;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 599
    return-void
.end method

.method public finishTurn()V
    .locals 1

    .prologue
    .line 527
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$14;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$14;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 534
    return-void
.end method

.method public getActivityContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 582
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->access$100(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 583
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->access$100(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    .line 585
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0
.end method

.method public getFieldId()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .locals 1

    .prologue
    .line 495
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getFieldId()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    return-object v0
.end method

.method public getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;
    .locals 1
    .param p1, "type"    # Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .prologue
    .line 572
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->access$100(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 573
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->access$100(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    .line 575
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public interruptTurn()V
    .locals 1

    .prologue
    .line 514
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$13;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$13;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 521
    return-void
.end method

.method public queueEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V
    .locals 1
    .param p1, "event"    # Lcom/vlingo/core/internal/dialogmanager/DialogEvent;
    .param p2, "turn"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .prologue
    .line 541
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$15;

    invoke-direct {v0, p0, p2, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$15;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;Lcom/vlingo/core/internal/dialogmanager/DialogEvent;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 554
    return-void
.end method

.method public sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V
    .locals 1
    .param p1, "event"    # Lcom/vlingo/core/internal/dialogmanager/DialogEvent;
    .param p2, "turn"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .prologue
    .line 459
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$9;

    invoke-direct {v0, p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$9;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 467
    return-void
.end method

.method public setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    .locals 3
    .param p1, "id"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .prologue
    .line 485
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setFieldId():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", turn:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getTurnNumber()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$11;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$11;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 491
    return-void
.end method

.method public showUserText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 408
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->showUserText(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 409
    return-void
.end method

.method public showUserText(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "turn"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .prologue
    .line 415
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$6;

    invoke-direct {v0, p0, p2, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$6;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 427
    return-void
.end method

.method public showVlingoText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 433
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$7;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$7;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 440
    return-void
.end method

.method public showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "displayText"    # Ljava/lang/String;
    .param p2, "spokenText"    # Ljava/lang/String;

    .prologue
    .line 446
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$8;

    invoke-direct {v0, p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$8;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 453
    return-void
.end method

.method public showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 6
    .param p1, "key"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p2, "decorators"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            "TT;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 624
    .local p3, "object":Ljava/lang/Object;, "TT;"
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$19;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$19;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 631
    return-void
.end method

.method public startReco()Z
    .locals 1

    .prologue
    .line 473
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$10;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$10;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 480
    const/4 v0, 0x1

    return v0
.end method

.method public storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V
    .locals 1
    .param p1, "type"    # Lcom/vlingo/core/internal/dialogmanager/DialogDataType;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 560
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$16;

    invoke-direct {v0, p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$16;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 567
    return-void
.end method

.method public tts(Ljava/lang/String;)V
    .locals 1
    .param p1, "tts"    # Ljava/lang/String;

    .prologue
    .line 341
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$1;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$1;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 348
    return-void
.end method

.method public tts(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 1
    .param p1, "tts"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    .line 355
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$2;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 362
    return-void
.end method

.method public ttsAnyway(Ljava/lang/String;)V
    .locals 1
    .param p1, "tts"    # Ljava/lang/String;

    .prologue
    .line 369
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$3;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$3;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 376
    return-void
.end method

.method public ttsAnyway(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 1
    .param p1, "tts"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    .line 382
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$4;

    invoke-direct {v0, p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$4;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 389
    return-void
.end method

.method public userCancel()V
    .locals 1

    .prologue
    .line 653
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$21;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$21;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 660
    return-void
.end method
