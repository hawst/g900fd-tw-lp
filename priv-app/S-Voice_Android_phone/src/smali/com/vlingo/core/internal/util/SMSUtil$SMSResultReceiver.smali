.class public Lcom/vlingo/core/internal/util/SMSUtil$SMSResultReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SMSUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/SMSUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SMSResultReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 729
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 735
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "com.vlingo.client.actions.ACTION_SMS_SENT"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 739
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .line 740
    .local v4, "uri":Landroid/net/Uri;
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    const-string/jumbo v7, "id"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 742
    .local v1, "id":J
    # getter for: Lcom/vlingo/core/internal/util/SMSUtil;->smsSendCallback:Ljava/util/Map;
    invoke-static {}, Lcom/vlingo/core/internal/util/SMSUtil;->access$000()Ljava/util/Map;

    move-result-object v6

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;

    .line 744
    .local v0, "cbWrapper":Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/SMSUtil$SMSResultReceiver;->getResultCode()I

    move-result v3

    .line 745
    .local v3, "resultCode":I
    const/4 v5, 0x0

    .line 746
    .local v5, "wasErr":Z
    const/4 v6, -0x1

    if-eq v3, v6, :cond_3

    .line 747
    const-string/jumbo v6, "com.vlingo.client.extras.URI"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 748
    const-string/jumbo v6, "com.vlingo.client.extras.URI"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    .end local v4    # "uri":Landroid/net/Uri;
    check-cast v4, Landroid/net/Uri;

    .line 754
    .restart local v4    # "uri":Landroid/net/Uri;
    :cond_0
    const/4 v5, 0x1

    .line 755
    const/4 v6, 0x5

    invoke-static {p1, v4, v6}, Lcom/vlingo/core/internal/util/SMSUtil;->moveMessageToFolder(Landroid/content/Context;Landroid/net/Uri;I)Z

    .line 768
    :cond_1
    :goto_0
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;->getParts()I

    move-result v6

    const/4 v7, 0x1

    if-le v6, v7, :cond_4

    .line 769
    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;->getParts()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v0, v6}, Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;->setParts(I)V

    .line 778
    .end local v0    # "cbWrapper":Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;
    .end local v1    # "id":J
    .end local v3    # "resultCode":I
    .end local v4    # "uri":Landroid/net/Uri;
    .end local v5    # "wasErr":Z
    :cond_2
    :goto_1
    return-void

    .line 758
    .restart local v0    # "cbWrapper":Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;
    .restart local v1    # "id":J
    .restart local v3    # "resultCode":I
    .restart local v4    # "uri":Landroid/net/Uri;
    .restart local v5    # "wasErr":Z
    :cond_3
    const-string/jumbo v6, "com.vlingo.client.extras.URI"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 759
    const-string/jumbo v6, "com.vlingo.client.extras.URI"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    .end local v4    # "uri":Landroid/net/Uri;
    check-cast v4, Landroid/net/Uri;

    .line 760
    .restart local v4    # "uri":Landroid/net/Uri;
    const/4 v6, 0x2

    invoke-static {p1, v4, v6}, Lcom/vlingo/core/internal/util/SMSUtil;->moveMessageToFolder(Landroid/content/Context;Landroid/net/Uri;I)Z

    goto :goto_0

    .line 771
    :cond_4
    # getter for: Lcom/vlingo/core/internal/util/SMSUtil;->smsSendCallback:Ljava/util/Map;
    invoke-static {}, Lcom/vlingo/core/internal/util/SMSUtil;->access$000()Ljava/util/Map;

    move-result-object v6

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 772
    if-eqz v0, :cond_5

    .line 773
    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallbackWrapper;->getCallback()Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallback;

    move-result-object v6

    invoke-interface {v6, v5, v3}, Lcom/vlingo/core/internal/util/SMSUtil$SMSSendCallback;->onSMSSent(ZI)V

    .line 775
    :cond_5
    # invokes: Lcom/vlingo/core/internal/util/SMSUtil;->releaseSmsSendReceiver(Landroid/content/Context;)V
    invoke-static {p1}, Lcom/vlingo/core/internal/util/SMSUtil;->access$100(Landroid/content/Context;)V

    goto :goto_1
.end method
