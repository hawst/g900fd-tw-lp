.class Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;
.super Lcom/vlingo/core/internal/audio/AudioFocusManager$EclairAudioFocusManager;
.source "AudioFocusManager.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/audio/AudioFocusManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FroyoAudioFocusManager"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager$FocusHandler;
    }
.end annotation


# static fields
.field private static final AUDIO_FOCUS_DELAY:I = 0xc8

.field private static mSetTaskOnAudioFocus:Ljava/lang/Runnable;


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private mHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 141
    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager$EclairAudioFocusManager;-><init>(Lcom/vlingo/core/internal/audio/AudioFocusManager$1;)V

    .line 142
    const-string/jumbo v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mAudioManager:Landroid/media/AudioManager;

    .line 143
    new-instance v0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager$FocusHandler;

    invoke-direct {v0, p0, v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager$FocusHandler;-><init>(Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;Lcom/vlingo/core/internal/audio/AudioFocusManager$1;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mHandler:Landroid/os/Handler;

    .line 144
    return-void
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;

    .prologue
    .line 135
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$500()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mSetTaskOnAudioFocus:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$502(Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0, "x0"    # Ljava/lang/Runnable;

    .prologue
    .line 135
    sput-object p0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mSetTaskOnAudioFocus:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;
    .param p1, "x1"    # I

    .prologue
    .line 135
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->broadcastFocuseChanged(I)V

    return-void
.end method

.method private broadcastFocuseChanged(I)V
    .locals 4
    .param p1, "focusChange"    # I

    .prologue
    .line 214
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "com.vlingo.client.app.action.AUDIO_FOCUS_CHANGED"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 215
    .local v0, "broadcastIntent":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 216
    .local v1, "context":Landroid/content/Context;
    const-string/jumbo v2, "com.vlingo.client.app.extra.FOCUS_CHANGE"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 217
    const-string/jumbo v2, "com.vlingo.client.app.extra.FOCUS_CHANGE_FROM"

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 218
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 219
    return-void
.end method


# virtual methods
.method public declared-synchronized abandonAudioFocus()V
    .locals 4

    .prologue
    .line 176
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mSetTaskOnAudioFocus:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 178
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 179
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    :cond_0
    monitor-exit p0

    return-void

    .line 176
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abandonAudioFocusInSync()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 189
    const/4 v0, 0x0

    .line 190
    .local v0, "result":I
    # getter for: Lcom/vlingo/core/internal/audio/AudioFocusManager;->bAbandonInSync:Z
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->access$200()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 191
    iget-object v2, p0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mHandler:Landroid/os/Handler;

    check-cast v1, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager$FocusHandler;

    invoke-virtual {v2, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    move-result v0

    .line 192
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 193
    # setter for: Lcom/vlingo/core/internal/audio/AudioFocusManager;->bHasFocus:Z
    invoke-static {v3}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->access$302(Z)Z

    .line 194
    const/4 v1, -0x1

    invoke-direct {p0, v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->broadcastFocuseChanged(I)V

    .line 198
    :cond_0
    # setter for: Lcom/vlingo/core/internal/audio/AudioFocusManager;->bAbandonInSync:Z
    invoke-static {v3}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->access$202(Z)Z

    .line 199
    return-void
.end method

.method public onAudioFocusChange(I)V
    .locals 0
    .param p1, "focusChange"    # I

    .prologue
    .line 185
    return-void
.end method

.method public declared-synchronized requestAudioFocus(II)V
    .locals 3
    .param p1, "audioStream"    # I
    .param p2, "focusType"    # I

    .prologue
    .line 165
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 166
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 167
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v1, v2, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    .line 168
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    monitor-exit p0

    return-void

    .line 165
    .end local v0    # "msg":Landroid/os/Message;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public setAbandonInSync(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 204
    # setter for: Lcom/vlingo/core/internal/audio/AudioFocusManager;->bAbandonInSync:Z
    invoke-static {p1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->access$202(Z)Z

    .line 205
    return-void
.end method

.method public setNullTask()V
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mSetTaskOnAudioFocus:Ljava/lang/Runnable;

    .line 211
    return-void
.end method

.method public setTaskOnGetAudioFocus(IILjava/lang/Runnable;)V
    .locals 0
    .param p1, "audioStream"    # I
    .param p2, "focusType"    # I
    .param p3, "task"    # Ljava/lang/Runnable;

    .prologue
    .line 150
    sput-object p3, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->mSetTaskOnAudioFocus:Ljava/lang/Runnable;

    .line 151
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/core/internal/audio/AudioFocusManager$FroyoAudioFocusManager;->requestAudioFocus(II)V

    .line 152
    return-void
.end method
