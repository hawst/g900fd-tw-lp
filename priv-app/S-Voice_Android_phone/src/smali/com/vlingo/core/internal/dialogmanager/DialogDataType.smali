.class public final enum Lcom/vlingo/core/internal/dialogmanager/DialogDataType;
.super Ljava/lang/Enum;
.source "DialogDataType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/dialogmanager/DialogDataType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum ACTIVE_CONTROLLER:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum ALARM_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum ALARM_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum CALENDAR_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum CONTACT_NAME:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum CONTACT_QUERY:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum CURRENT_ACTION:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum LIST_CONTROL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum LOCATION_ITEMS:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum MEMO_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum MESSAGE_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum MUSIC_ITEM_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum OPEN_APP:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum ORDINAL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum ORDINAL_DATA_COUNT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum PARSE_TYPE:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum PREVIOUS_FIELD_ID:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum RECOGNITION_RESULT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum SELECTED_CONTACT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum SOCIAL_UPDATE:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum TASK_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum TIMER_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum TIMER_DATA_SPOKEN:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

.field public static final enum UNDEFINED:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 10
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "ACTIVE_CONTROLLER"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ACTIVE_CONTROLLER:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 11
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "ALARM_DATA"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ALARM_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 12
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "ALARM_MATCHES"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ALARM_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 13
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "CALENDAR_MATCHES"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CALENDAR_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 14
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "CONTACT_MATCHES"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 15
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "CONTACT_QUERY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_QUERY:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 16
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "CURRENT_ACTION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CURRENT_ACTION:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 17
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "LOCATION_ITEMS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->LOCATION_ITEMS:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 18
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "MEMO_DATA"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MEMO_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 19
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "MESSAGE_DATA"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 20
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "MESSAGE_MATCHES"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 21
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "MUSIC_ITEM_MATCHES"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MUSIC_ITEM_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 22
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "OPEN_APP"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->OPEN_APP:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 23
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "ORDINAL_DATA"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 24
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "ORDINAL_DATA_COUNT"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA_COUNT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 25
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "LIST_CONTROL_DATA"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->LIST_CONTROL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 26
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "PARSE_TYPE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->PARSE_TYPE:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 27
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "PREVIOUS_FIELD_ID"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->PREVIOUS_FIELD_ID:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 28
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "RECOGNITION_RESULT"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->RECOGNITION_RESULT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 29
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "SELECTED_CONTACT"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->SELECTED_CONTACT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 30
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "CONTACT_NAME"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_NAME:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 31
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "SOCIAL_UPDATE"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->SOCIAL_UPDATE:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 32
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "TASK_MATCHES"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->TASK_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 33
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "TIMER_DATA"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->TIMER_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 34
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "TIMER_DATA_SPOKEN"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->TIMER_DATA_SPOKEN:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 35
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v1, "UNDEFINED"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->UNDEFINED:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .line 9
    const/16 v0, 0x1a

    new-array v0, v0, [Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ACTIVE_CONTROLLER:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ALARM_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ALARM_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CALENDAR_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_QUERY:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CURRENT_ACTION:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->LOCATION_ITEMS:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MEMO_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MESSAGE_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->MUSIC_ITEM_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->OPEN_APP:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA_COUNT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->LIST_CONTROL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->PARSE_TYPE:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->PREVIOUS_FIELD_ID:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->RECOGNITION_RESULT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->SELECTED_CONTACT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_NAME:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->SOCIAL_UPDATE:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->TASK_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->TIMER_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->TIMER_DATA_SPOKEN:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->UNDEFINED:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogDataType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 9
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/dialogmanager/DialogDataType;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    return-object v0
.end method
