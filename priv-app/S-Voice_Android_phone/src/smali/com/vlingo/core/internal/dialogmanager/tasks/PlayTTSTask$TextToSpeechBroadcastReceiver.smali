.class Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PlayTTSTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TextToSpeechBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$1;

    .prologue
    .line 182
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;-><init>(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 185
    if-eqz p2, :cond_1

    .line 188
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 189
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v1, "android.speech.tts.TTS_QUEUE_PROCESSING_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 190
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTextToSpeechBroadcastReceiver:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->access$600(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;)Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 191
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTextToSpeechBroadcastReceiver:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->access$600(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;)Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 192
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    const/4 v2, 0x0

    # setter for: Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->mTextToSpeechBroadcastReceiver:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->access$602(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;)Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;

    .line 194
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask$TextToSpeechBroadcastReceiver;->this$0:Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->notifyFinished()V
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->access$800(Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;)V

    .line 197
    .end local v0    # "action":Ljava/lang/String;
    :cond_1
    return-void
.end method
