.class abstract Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseUriContentChangesObserver;
.super Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;
.source "BaseUriContentChangesObserver.java"


# instance fields
.field private final contentObserver:Landroid/database/ContentObserver;

.field private final uri:Landroid/net/Uri;


# direct methods
.method protected constructor <init>(Landroid/net/Uri;Lcom/vlingo/core/internal/lmttvocon/UploadContext;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "context"    # Lcom/vlingo/core/internal/lmttvocon/UploadContext;

    .prologue
    .line 13
    invoke-direct {p0, p2}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseContentChangesObserver;-><init>(Lcom/vlingo/core/internal/lmttvocon/UploadContext;)V

    .line 14
    new-instance v0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseUriContentChangesObserver$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseUriContentChangesObserver$1;-><init>(Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseUriContentChangesObserver;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseUriContentChangesObserver;->contentObserver:Landroid/database/ContentObserver;

    .line 27
    iput-object p1, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseUriContentChangesObserver;->uri:Landroid/net/Uri;

    .line 28
    return-void
.end method


# virtual methods
.method protected registerContentObserver(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseUriContentChangesObserver;->uri:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseUriContentChangesObserver;->contentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 38
    return-void
.end method

.method protected unregisterContentObserver(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/lmttvocon/contentobserver/BaseUriContentChangesObserver;->contentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 33
    return-void
.end method
