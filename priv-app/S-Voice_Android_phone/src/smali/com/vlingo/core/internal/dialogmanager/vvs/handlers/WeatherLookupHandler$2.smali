.class Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$2;
.super Ljava/lang/Object;
.source "WeatherLookupHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->showFailure()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)V
    .locals 0

    .prologue
    .line 291
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 294
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_no_results:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$1700(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 295
    .local v0, "prompt":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->access$1800(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 296
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$2;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;

    const-string/jumbo v2, "Weather lookup failed"

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->actionFail(Ljava/lang/String;)V

    .line 297
    return-void
.end method
