.class public final Lcom/vlingo/core/internal/util/LocationUtil;
.super Ljava/lang/Object;
.source "LocationUtil.java"


# static fields
.field private static instance:Lcom/vlingo/core/internal/util/LocationUtil;


# instance fields
.field private context:Landroid/content/Context;

.field private countryCode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/util/LocationUtil;->instance:Lcom/vlingo/core/internal/util/LocationUtil;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/util/LocationUtil;->countryCode:Ljava/lang/String;

    .line 37
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/util/LocationUtil;->context:Landroid/content/Context;

    .line 38
    return-void
.end method

.method public static destroy()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/util/LocationUtil;->instance:Lcom/vlingo/core/internal/util/LocationUtil;

    .line 49
    return-void
.end method

.method public static getCountryCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    invoke-static {}, Lcom/vlingo/core/internal/util/LocationUtil;->getInstance()Lcom/vlingo/core/internal/util/LocationUtil;

    move-result-object v0

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/LocationUtil;->getInstanceCountryCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getInstance()Lcom/vlingo/core/internal/util/LocationUtil;
    .locals 2

    .prologue
    .line 41
    const-class v1, Lcom/vlingo/core/internal/util/LocationUtil;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/util/LocationUtil;->instance:Lcom/vlingo/core/internal/util/LocationUtil;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lcom/vlingo/core/internal/util/LocationUtil;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/LocationUtil;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/util/LocationUtil;->instance:Lcom/vlingo/core/internal/util/LocationUtil;

    .line 44
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/util/LocationUtil;->instance:Lcom/vlingo/core/internal/util/LocationUtil;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getInstanceCountryCode()Ljava/lang/String;
    .locals 15

    .prologue
    .line 56
    iget-object v5, p0, Lcom/vlingo/core/internal/util/LocationUtil;->countryCode:Ljava/lang/String;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 57
    const/4 v5, 0x0

    .line 98
    :goto_0
    return-object v5

    .line 60
    :cond_0
    iget-object v5, p0, Lcom/vlingo/core/internal/util/LocationUtil;->context:Landroid/content/Context;

    const-string/jumbo v14, "phone"

    invoke-virtual {v5, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/telephony/TelephonyManager;

    .line 61
    .local v13, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v13}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/core/internal/util/LocationUtil;->countryCode:Ljava/lang/String;

    .line 62
    iget-object v5, p0, Lcom/vlingo/core/internal/util/LocationUtil;->countryCode:Ljava/lang/String;

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 63
    iget-object v5, p0, Lcom/vlingo/core/internal/util/LocationUtil;->countryCode:Ljava/lang/String;

    goto :goto_0

    .line 65
    :cond_1
    iget-object v5, p0, Lcom/vlingo/core/internal/util/LocationUtil;->context:Landroid/content/Context;

    const-string/jumbo v14, "location"

    invoke-virtual {v5, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/location/LocationManager;

    .line 66
    .local v10, "lm":Landroid/location/LocationManager;
    new-instance v8, Landroid/location/Criteria;

    invoke-direct {v8}, Landroid/location/Criteria;-><init>()V

    .line 67
    .local v8, "cr":Landroid/location/Criteria;
    const/4 v5, 0x2

    invoke-virtual {v8, v5}, Landroid/location/Criteria;->setAccuracy(I)V

    .line 68
    const/4 v5, 0x0

    invoke-virtual {v8, v5}, Landroid/location/Criteria;->setAltitudeRequired(Z)V

    .line 69
    const/4 v5, 0x0

    invoke-virtual {v8, v5}, Landroid/location/Criteria;->setBearingRequired(Z)V

    .line 70
    const/4 v5, 0x0

    invoke-virtual {v8, v5}, Landroid/location/Criteria;->setCostAllowed(Z)V

    .line 71
    const/4 v5, 0x0

    invoke-virtual {v8, v5}, Landroid/location/Criteria;->setSpeedRequired(Z)V

    .line 73
    const/4 v5, 0x1

    invoke-virtual {v10, v8, v5}, Landroid/location/LocationManager;->getBestProvider(Landroid/location/Criteria;Z)Ljava/lang/String;

    move-result-object v12

    .line 75
    .local v12, "provider":Ljava/lang/String;
    if-eqz v12, :cond_2

    .line 76
    invoke-virtual {v10, v12}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v11

    .line 77
    .local v11, "loc":Landroid/location/Location;
    if-eqz v11, :cond_2

    .line 78
    invoke-virtual {v11}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    .line 79
    .local v1, "latitude":D
    invoke-virtual {v11}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    .line 81
    .local v3, "longitude":D
    new-instance v0, Landroid/location/Geocoder;

    iget-object v5, p0, Lcom/vlingo/core/internal/util/LocationUtil;->context:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    .line 83
    .local v0, "geocoder":Landroid/location/Geocoder;
    const/4 v5, 0x1

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v7

    .line 84
    .local v7, "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    if-eqz v7, :cond_2

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_2

    .line 85
    const/4 v5, 0x0

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/location/Address;

    .line 86
    .local v6, "addr":Landroid/location/Address;
    invoke-virtual {v6}, Landroid/location/Address;->getCountryCode()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/core/internal/util/LocationUtil;->countryCode:Ljava/lang/String;

    .line 87
    iget-object v5, p0, Lcom/vlingo/core/internal/util/LocationUtil;->countryCode:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 92
    .end local v6    # "addr":Landroid/location/Address;
    .end local v7    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :catch_0
    move-exception v9

    .line 94
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    .line 98
    .end local v0    # "geocoder":Landroid/location/Geocoder;
    .end local v1    # "latitude":D
    .end local v3    # "longitude":D
    .end local v9    # "e":Ljava/io/IOException;
    .end local v11    # "loc":Landroid/location/Location;
    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method
