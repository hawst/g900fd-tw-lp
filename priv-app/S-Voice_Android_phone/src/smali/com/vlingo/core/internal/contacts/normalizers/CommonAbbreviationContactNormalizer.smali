.class public Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;
.super Lcom/vlingo/core/internal/contacts/normalizers/RegexContactNameNormalizer;
.source "CommonAbbreviationContactNormalizer.java"


# static fields
.field private static final MAPPINGS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/util/regex/Pattern;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static canNormalizePattern:Ljava/util/regex/Pattern;

.field private static canNormalizeStr:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 12
    sput-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->canNormalizeStr:Ljava/lang/String;

    .line 13
    sput-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->canNormalizePattern:Ljava/util/regex/Pattern;

    .line 16
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    .line 17
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "^\\b(Fr|Ms|Mrs|Mme|Madame|Father|Miss|Frau|Sig.na|Sig.ra|Signora|Sr.\u00aa|Sr\u00aa|Sra|Srta|Se\u00f1orita|Se\u00f1ora|Senhora|Mademoiselle|Mlle)\\b\\.?(?!-)"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "#MissusOrFather#"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(Dr|Doctor|Doutor|Doktor|Dott|Docteur|Dottore|\u0414(\\s+)?-?\u0440|\u0434\u043e\u043a\u0442\u043e\u0440)\\b\\.?"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "#Doctor#"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(Dr.\u00aa|Dr\u00aa|Dra|Doutora)\\b\\.?"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "#DoctorWoman#"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "^\\b(Hr|Hrn|Herr|Herrn|M|Monsieur|Mr|Mister|Sig|Signor|Se\u00f1or)\\b\\.?(?!-)"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "#Mister#"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(InG)\\b\\.?"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "#Engineer#"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(Prof.ssa|Profesora|Professora|Professoressa|Profa)\\b\\.?"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "#ProfessorWoman#"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(Profesor|Professor|Prof|\u041f\u0440\u043e\u0444\u0435\u0441\u0441\u043e\u0440|\u041f\u0440\u043e\u0444)\\b\\.?"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "#Professor#"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(\u041d\u0430\u0447\u0430\u043b\u044c\u043d\u0438\u043a|\u041d\u0430\u0447)\\b\\.?"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "#Manager#"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(\u0414\u043e\u0446\u0435\u043d\u0442|\u0414\u043e\u0446)\\b\\.?"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "#ScienceGr#"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(Prim|Primaire|Primary)\\b\\.?"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "#Primary#"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(Sr|Senior|Senhor|(the\\s+)?First)\\b\\.?"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "#Senior#"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(St|Saint)\\b\\.?"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "#Religion#"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(Jr|Junior|II|(the\\s+)?Second)\\b\\.?"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "#Junior#"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(III|(the\\s+)?Third)\\b\\.?"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "#Third#"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(IV|(the\\s+)?Fourth)\\b\\.?"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "#Fourth#"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(V|(the\\s+)?Fifth)\\b\\.?"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "#Fifth#"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(VI|(the\\s+)?Sixth)\\b\\.?"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "#Sixth#"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(\u0434\u043e\u043c|\u0434\u043e\u043c\u0430\u0448\u043d\u0438(\u0438|\u0439))\\b\\.?"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "#Home#"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(\u0440\u0430\u0431|\u0440\u0430\u0431\u043e\u0447\u0438(\u0438|\u0439))\\b\\.?"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "#Work#"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    const-string/jumbo v1, "\\b(\u043c\u043e\u0431|\u043c\u043e\u0431\u0438\u043b\u044c\u043d\u044b(\u0438|\u0439))\\b\\.?"

    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "#Cell#"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/normalizers/RegexContactNameNormalizer;-><init>()V

    return-void
.end method

.method private static createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;
    .locals 2
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 40
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->canNormalizeStr:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->canNormalizeStr:Ljava/lang/String;

    .line 45
    :goto_0
    const/4 v0, 0x2

    invoke-static {p0, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    return-object v0

    .line 43
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->canNormalizeStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "|("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->canNormalizeStr:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected getCanNormalizePattern()Ljava/util/regex/Pattern;
    .locals 2

    .prologue
    .line 56
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->canNormalizePattern:Ljava/util/regex/Pattern;

    if-nez v0, :cond_0

    .line 57
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->canNormalizeStr:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->canNormalizePattern:Ljava/util/regex/Pattern;

    .line 59
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->canNormalizePattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method protected getPatterns()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/util/regex/Pattern;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/CommonAbbreviationContactNormalizer;->MAPPINGS:Ljava/util/Map;

    return-object v0
.end method
