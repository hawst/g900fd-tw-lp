.class public Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;
.super Ljava/lang/Object;
.source "CoreSpotterParameters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private absbeam:F

.field private aoffset:F

.field private beam:F

.field private cgFilename:Ljava/lang/String;

.field private delay:F

.field private deltaD:I

.field private grammarSpec:Ljava/lang/String;

.field private language:Ljava/lang/String;

.field private pronunList:[Ljava/lang/String;

.field private wakeUpExternalStorage:Ljava/lang/String;

.field private wordList:[Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->beam:F

    .line 39
    const/high16 v0, 0x42200000    # 40.0f

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->absbeam:F

    .line 40
    iput v1, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->aoffset:F

    .line 41
    iput v1, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->delay:F

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->deltaD:I

    .line 43
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "cgFilename"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;-><init>()V

    .line 50
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 51
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "language, cgFilename cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_1
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->language:Ljava/lang/String;

    .line 54
    iput-object p2, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->cgFilename:Ljava/lang/String;

    .line 55
    iput-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->grammarSpec:Ljava/lang/String;

    .line 56
    iput-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->wordList:[Ljava/lang/String;

    .line 57
    iput-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->pronunList:[Ljava/lang/String;

    .line 58
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "grammarSpec"    # Ljava/lang/String;
    .param p3, "wordList"    # [Ljava/lang/String;
    .param p4, "pronunList"    # [Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;-><init>()V

    .line 65
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "language cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_0
    invoke-static {p2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "grammarSpec cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_1
    if-eqz p3, :cond_2

    if-eqz p4, :cond_2

    array-length v0, p3

    if-eqz v0, :cond_2

    array-length v0, p4

    if-nez v0, :cond_3

    .line 72
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "wordList and pronunlist cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_3
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->language:Ljava/lang/String;

    .line 75
    iput-object p2, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->grammarSpec:Ljava/lang/String;

    .line 76
    iput-object p3, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->wordList:[Ljava/lang/String;

    .line 77
    iput-object p4, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->pronunList:[Ljava/lang/String;

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->cgFilename:Ljava/lang/String;

    .line 79
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->language:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;

    .prologue
    .line 24
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->deltaD:I

    return v0
.end method

.method static synthetic access$1100(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->wakeUpExternalStorage:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->cgFilename:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->grammarSpec:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->wordList:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->pronunList:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)F
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;

    .prologue
    .line 24
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->beam:F

    return v0
.end method

.method static synthetic access$700(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)F
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;

    .prologue
    .line 24
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->absbeam:F

    return v0
.end method

.method static synthetic access$800(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)F
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;

    .prologue
    .line 24
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->aoffset:F

    return v0
.end method

.method static synthetic access$900(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;)F
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;

    .prologue
    .line 24
    iget v0, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->delay:F

    return v0
.end method


# virtual methods
.method public build()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;
    .locals 2

    .prologue
    .line 102
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;-><init>(Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$1;)V

    return-object v0
.end method

.method public setDeltaD(I)Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;
    .locals 0
    .param p1, "deltaDParam"    # I

    .prologue
    .line 88
    iput p1, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->deltaD:I

    .line 89
    return-object p0
.end method

.method public setSensoryParams(FFFFLjava/lang/String;)Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;
    .locals 0
    .param p1, "beamParam"    # F
    .param p2, "absbeamParam"    # F
    .param p3, "aoffsetParam"    # F
    .param p4, "delayParam"    # F
    .param p5, "wakeUpExternalStorageParam"    # Ljava/lang/String;

    .prologue
    .line 93
    iput p1, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->beam:F

    .line 94
    iput p2, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->absbeam:F

    .line 95
    iput p3, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->aoffset:F

    .line 96
    iput p4, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->delay:F

    .line 97
    iput-object p5, p0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->wakeUpExternalStorage:Ljava/lang/String;

    .line 98
    return-object p0
.end method
