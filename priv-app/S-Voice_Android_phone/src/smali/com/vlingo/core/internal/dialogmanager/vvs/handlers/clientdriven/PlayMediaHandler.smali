.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/PlayMediaHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "PlayMediaHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 6
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v5, 0x0

    .line 30
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 32
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v3

    const-string/jumbo v4, "media"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 34
    const-string/jumbo v3, "Type"

    invoke-static {p1, v3, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 35
    .local v2, "type":Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v3, "Radio:Play"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 36
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 37
    .local v1, "i":Landroid/content/Intent;
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 39
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getFmRadioApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->getIntentNameStart()Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, "fmRadioStartActionName":Ljava/lang/String;
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v4, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/PlayMediaHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {v3, v0}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->argument(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v3

    const-string/jumbo v4, "Extras"

    invoke-static {p1, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->extra(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v3

    const-string/jumbo v4, "broadcast"

    invoke-static {p1, v4, v5, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->broadcast(Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    .line 48
    .end local v0    # "fmRadioStartActionName":Ljava/lang/String;
    .end local v1    # "i":Landroid/content/Intent;
    :cond_0
    return v5
.end method
