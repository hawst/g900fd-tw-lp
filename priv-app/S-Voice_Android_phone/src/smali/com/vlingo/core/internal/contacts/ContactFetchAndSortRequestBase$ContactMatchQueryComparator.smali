.class public Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$ContactMatchQueryComparator;
.super Ljava/lang/Object;
.source "ContactFetchAndSortRequestBase.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ContactMatchQueryComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/vlingo/core/internal/contacts/ContactMatch;",
        ">;"
    }
.end annotation


# instance fields
.field private final mQuery:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 238
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$ContactMatchQueryComparator;->mQuery:Ljava/lang/String;

    .line 239
    return-void
.end method


# virtual methods
.method public compare(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactMatch;)I
    .locals 1
    .param p1, "cm1"    # Lcom/vlingo/core/internal/contacts/ContactMatch;
    .param p2, "cm2"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 243
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$ContactMatchQueryComparator;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, p1, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->compareTo(Lcom/vlingo/core/internal/contacts/ContactMatch;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 234
    check-cast p1, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$ContactMatchQueryComparator;->compare(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/contacts/ContactMatch;)I

    move-result v0

    return v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase$ContactMatchQueryComparator;->mQuery:Ljava/lang/String;

    return-object v0
.end method
