.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SendMessageHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "SendMessageHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SendMessageHandler$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SendMessageHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SendMessageHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 73
    return-void
.end method

.method private getContact(Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactData;
    .locals 5
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SendMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 82
    .local v1, "matches":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-static {v1, p1}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getContactDataFromId(Ljava/util/List;Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactData;

    move-result-object v0

    .line 83
    .local v0, "contact":Lcom/vlingo/core/internal/contacts/ContactData;
    if-nez v0, :cond_0

    .line 84
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SendMessageHandler;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "SendMessage Contact and Address ID doesn\'t match what was sent: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    new-instance v2, Ljava/security/InvalidParameterException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "SendMessage Contact and Address ID doesn\'t match what was sent: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 88
    :cond_0
    return-object v0
.end method


# virtual methods
.method public actionFail(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 103
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;

    invoke-direct {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;-><init>(Ljava/lang/String;)V

    .line 104
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SendMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SendMessageHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 105
    return-void
.end method

.method public actionSuccess()V
    .locals 3

    .prologue
    .line 97
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;-><init>()V

    .line 98
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SendMessageHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SendMessageHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 99
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 10
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 42
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 46
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v6

    const-string/jumbo v7, "car-sms-message"

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 48
    const-string/jumbo v6, "id"

    invoke-static {p1, v6, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 50
    .local v3, "id":Ljava/lang/String;
    const-string/jumbo v6, "subject"

    invoke-static {p1, v6, v9}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    .line 51
    .local v5, "subject":Ljava/lang/String;
    const-string/jumbo v6, "message"

    invoke-static {p1, v6, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 52
    .local v4, "message":Ljava/lang/String;
    const-string/jumbo v6, "capability"

    invoke-static {p1, v6, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 53
    .local v1, "capabilityStr":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SendMessageHandler;->getContact(Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactData;

    move-result-object v2

    .line 55
    .local v2, "contact":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/ContactType;->of(Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v0

    .line 56
    .local v0, "capability":Lcom/vlingo/core/internal/contacts/ContactType;
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SendMessageHandler$1;->$SwitchMap$com$vlingo$core$internal$contacts$ContactType:[I

    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 75
    :goto_0
    return v9

    .line 58
    :pswitch_0
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SEND_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v7, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;

    invoke-virtual {p0, v6, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SendMessageHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;

    iget-object v7, v2, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-interface {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;->address(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;

    move-result-object v6

    invoke-interface {v6, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;->message(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;

    move-result-object v6

    invoke-interface {v6}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendMessageInterface;->queue()V

    goto :goto_0

    .line 64
    :pswitch_1
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SEND_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v7, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;

    invoke-virtual {p0, v6, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SendMessageHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;

    invoke-virtual {v6, v2}, Lcom/vlingo/core/internal/dialogmanager/actions/SendEmailAction;->contact(Lcom/vlingo/core/internal/contacts/ContactData;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;

    move-result-object v6

    invoke-interface {v6, v5}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;->subject(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;

    move-result-object v6

    invoke-interface {v6, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;->message(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;

    move-result-object v6

    invoke-interface {v6}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;->queue()V

    goto :goto_0

    .line 72
    :pswitch_2
    sget-object v6, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SendMessageHandler;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Unexpected target for SendMessage: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactType;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 56
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
