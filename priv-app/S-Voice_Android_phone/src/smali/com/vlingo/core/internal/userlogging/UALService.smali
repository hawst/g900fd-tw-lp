.class public Lcom/vlingo/core/internal/userlogging/UALService;
.super Landroid/app/Service;
.source "UALService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/userlogging/UALService$UserLogTask;,
        Lcom/vlingo/core/internal/userlogging/UALService$TransmitTask;
    }
.end annotation


# static fields
.field public static final EXTRA_SKIP_INITIAL_DELAY:Ljava/lang/String; = "com.vlingo.client.userlogging.skipInitialDelay"

.field private static final TRANSMIT_INTERVAL:J = 0x3a980L


# instance fields
.field private looperThread:Landroid/os/HandlerThread;

.field private volatile mTaskQueue:Lcom/vlingo/core/internal/util/TaskQueue;

.field private mTransmitTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 136
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/userlogging/UALService;)Lcom/vlingo/core/internal/util/TaskQueue;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/userlogging/UALService;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/core/internal/userlogging/UALService;->mTaskQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    return-object v0
.end method

.method static synthetic access$002(Lcom/vlingo/core/internal/userlogging/UALService;Lcom/vlingo/core/internal/util/TaskQueue;)Lcom/vlingo/core/internal/util/TaskQueue;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/userlogging/UALService;
    .param p1, "x1"    # Lcom/vlingo/core/internal/util/TaskQueue;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/vlingo/core/internal/userlogging/UALService;->mTaskQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    return-object p1
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/userlogging/UALService;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/userlogging/UALService;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/core/internal/userlogging/UALService;->mTransmitTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$302(Lcom/vlingo/core/internal/userlogging/UALService;Ljava/util/Timer;)Ljava/util/Timer;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/userlogging/UALService;
    .param p1, "x1"    # Ljava/util/Timer;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/vlingo/core/internal/userlogging/UALService;->mTransmitTimer:Ljava/util/Timer;

    return-object p1
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/userlogging/UALService;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/userlogging/UALService;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/vlingo/core/internal/userlogging/UALService;->transmitData()V

    return-void
.end method

.method private transmitData()V
    .locals 6

    .prologue
    .line 104
    const/16 v1, 0xa

    .line 105
    .local v1, "limitCount":I
    :goto_0
    iget-object v4, p0, Lcom/vlingo/core/internal/userlogging/UALService;->mTaskQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    if-nez v4, :cond_1

    add-int/lit8 v2, v1, -0x1

    .end local v1    # "limitCount":I
    .local v2, "limitCount":I
    if-lez v1, :cond_0

    .line 107
    const-wide/16 v4, 0x32

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v2

    .line 110
    .end local v2    # "limitCount":I
    .restart local v1    # "limitCount":I
    goto :goto_0

    .line 108
    .end local v1    # "limitCount":I
    .restart local v2    # "limitCount":I
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    move v1, v2

    .line 110
    .end local v2    # "limitCount":I
    .restart local v1    # "limitCount":I
    goto :goto_0

    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v1    # "limitCount":I
    .restart local v2    # "limitCount":I
    :cond_0
    move v1, v2

    .line 116
    .end local v2    # "limitCount":I
    .restart local v1    # "limitCount":I
    :cond_1
    iget-object v4, p0, Lcom/vlingo/core/internal/userlogging/UALService;->mTaskQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    if-eqz v4, :cond_2

    .line 117
    new-instance v3, Lcom/vlingo/core/internal/userlogging/UALService$UserLogTask;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/vlingo/core/internal/userlogging/UALService$UserLogTask;-><init>(Lcom/vlingo/core/internal/userlogging/UALService;Lcom/vlingo/core/internal/userlogging/UALService$1;)V

    .line 118
    .local v3, "task":Lcom/vlingo/core/internal/userlogging/UALService$UserLogTask;
    iget-object v4, p0, Lcom/vlingo/core/internal/userlogging/UALService;->mTaskQueue:Lcom/vlingo/core/internal/util/TaskQueue;

    invoke-virtual {v4, v3}, Lcom/vlingo/core/internal/util/TaskQueue;->queueTask(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V

    .line 120
    .end local v3    # "task":Lcom/vlingo/core/internal/userlogging/UALService$UserLogTask;
    :cond_2
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 98
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 43
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 44
    new-instance v0, Lcom/vlingo/core/internal/userlogging/UALService$1;

    const-string/jumbo v1, "UALService"

    invoke-direct {v0, p0, v1}, Lcom/vlingo/core/internal/userlogging/UALService$1;-><init>(Lcom/vlingo/core/internal/userlogging/UALService;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/userlogging/UALService;->looperThread:Landroid/os/HandlerThread;

    .line 50
    iget-object v0, p0, Lcom/vlingo/core/internal/userlogging/UALService;->looperThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 51
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 58
    iget-object v0, p0, Lcom/vlingo/core/internal/userlogging/UALService;->looperThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 59
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 66
    const/4 v0, 0x0

    .line 67
    .local v0, "skipDelay":Z
    if-eqz p1, :cond_0

    .line 68
    const-string/jumbo v1, "com.vlingo.client.userlogging.skipInitialDelay"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 72
    :cond_0
    monitor-enter p0

    .line 73
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/userlogging/UALService;->mTransmitTimer:Ljava/util/Timer;

    if-nez v1, :cond_3

    .line 74
    if-eqz v0, :cond_2

    .line 75
    invoke-direct {p0}, Lcom/vlingo/core/internal/userlogging/UALService;->transmitData()V

    .line 92
    :cond_1
    :goto_0
    monitor-exit p0

    .line 94
    const/4 v1, 0x1

    return v1

    .line 78
    :cond_2
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/vlingo/core/internal/userlogging/UALService;->mTransmitTimer:Ljava/util/Timer;

    .line 79
    iget-object v1, p0, Lcom/vlingo/core/internal/userlogging/UALService;->mTransmitTimer:Ljava/util/Timer;

    new-instance v2, Lcom/vlingo/core/internal/userlogging/UALService$TransmitTask;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/vlingo/core/internal/userlogging/UALService$TransmitTask;-><init>(Lcom/vlingo/core/internal/userlogging/UALService;Lcom/vlingo/core/internal/userlogging/UALService$1;)V

    const-wide/32 v3, 0x3a980

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0

    .line 92
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 85
    :cond_3
    if-eqz v0, :cond_1

    .line 86
    :try_start_1
    iget-object v1, p0, Lcom/vlingo/core/internal/userlogging/UALService;->mTransmitTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 87
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/core/internal/userlogging/UALService;->mTransmitTimer:Ljava/util/Timer;

    .line 88
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/vlingo/core/internal/userlogging/UALService;->mTransmitTimer:Ljava/util/Timer;

    .line 89
    iget-object v1, p0, Lcom/vlingo/core/internal/userlogging/UALService;->mTransmitTimer:Ljava/util/Timer;

    new-instance v2, Lcom/vlingo/core/internal/userlogging/UALService$TransmitTask;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/vlingo/core/internal/userlogging/UALService$TransmitTask;-><init>(Lcom/vlingo/core/internal/userlogging/UALService;Lcom/vlingo/core/internal/userlogging/UALService$1;)V

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
