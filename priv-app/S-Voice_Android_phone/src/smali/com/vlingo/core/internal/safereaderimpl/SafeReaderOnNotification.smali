.class public Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;
.super Ljava/lang/Object;
.source "SafeReaderOnNotification.java"


# instance fields
.field private isSilent:Z

.field final mainNotificationId:I

.field final nm:Landroid/app/NotificationManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mainNotificationId"    # I
    .param p3, "isSilent"    # Z

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;->isSilent:Z

    .line 20
    iput p2, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;->mainNotificationId:I

    .line 21
    const-string/jumbo v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;->nm:Landroid/app/NotificationManager;

    .line 22
    iput-boolean p3, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;->isSilent:Z

    .line 23
    return-void
.end method


# virtual methods
.method public getNotification(Landroid/content/Context;ZZ)Landroid/app/Notification;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isSafeReaderPlaying"    # Z
    .param p3, "showTicker"    # Z

    .prologue
    const/4 v9, 0x0

    .line 30
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v7

    sget-object v8, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_enabled:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v7, v8}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v6

    .line 36
    .local v6, "title":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 37
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v7

    sget-object v8, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_notif_reading_title:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v7, v8}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v4

    .line 38
    .local v4, "text":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v7

    sget-object v8, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_notif_reading_ticker:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v7, v8}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v5

    .line 39
    .local v5, "tickerText":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v7

    sget-object v8, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_stat_safereader_sms:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    invoke-interface {v7, v8}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$drawable;)I

    move-result v1

    .line 40
    .local v1, "icon":I
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v7, "com.vlingo.client.safereader.ACTION_SKIP_CURRENT_ITEM"

    invoke-direct {v3, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 41
    .local v3, "onClickIntent":Landroid/content/Intent;
    new-instance v7, Landroid/content/ComponentName;

    const-class v8, Lcom/vlingo/core/internal/safereader/SafeReaderService;

    invoke-direct {v7, p1, v8}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 42
    invoke-static {p1, v9, v3, v9}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 54
    .local v0, "contentIntent":Landroid/app/PendingIntent;
    :goto_0
    new-instance v2, Landroid/app/Notification;

    const-wide/16 v7, 0x0

    invoke-direct {v2, v1, v6, v7, v8}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 55
    .local v2, "notification":Landroid/app/Notification;
    iget v7, v2, Landroid/app/Notification;->flags:I

    or-int/lit8 v7, v7, 0x22

    iput v7, v2, Landroid/app/Notification;->flags:I

    .line 56
    invoke-virtual {v2, p1, v5, v4, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 57
    return-object v2

    .line 45
    .end local v0    # "contentIntent":Landroid/app/PendingIntent;
    .end local v1    # "icon":I
    .end local v2    # "notification":Landroid/app/Notification;
    .end local v3    # "onClickIntent":Landroid/content/Intent;
    .end local v4    # "text":Ljava/lang/String;
    .end local v5    # "tickerText":Ljava/lang/String;
    :cond_0
    iget-boolean v7, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;->isSilent:Z

    if-eqz v7, :cond_1

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v7

    sget-object v8, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_notif_title_silent:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v7, v8}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v4

    .line 47
    .restart local v4    # "text":Ljava/lang/String;
    :goto_1
    move-object v5, v6

    .line 48
    .restart local v5    # "tickerText":Ljava/lang/String;
    iget-boolean v7, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;->isSilent:Z

    if-eqz v7, :cond_2

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v7

    sget-object v8, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_stat_safereader_silent:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    invoke-interface {v7, v8}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$drawable;)I

    move-result v1

    .line 50
    .restart local v1    # "icon":I
    :goto_2
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v7, "com.vlingo.client.safereader.ACTION_SAFEREADER_OFF"

    invoke-direct {v3, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 51
    .restart local v3    # "onClickIntent":Landroid/content/Intent;
    invoke-static {p1, v9, v3, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .restart local v0    # "contentIntent":Landroid/app/PendingIntent;
    goto :goto_0

    .line 45
    .end local v0    # "contentIntent":Landroid/app/PendingIntent;
    .end local v1    # "icon":I
    .end local v3    # "onClickIntent":Landroid/content/Intent;
    .end local v4    # "text":Ljava/lang/String;
    .end local v5    # "tickerText":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v7

    sget-object v8, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_notif_title:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v7, v8}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 48
    .restart local v4    # "text":Ljava/lang/String;
    .restart local v5    # "tickerText":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v7

    sget-object v8, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_stat_safereader_on:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    invoke-interface {v7, v8}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$drawable;)I

    move-result v1

    goto :goto_2
.end method

.method public isSilent()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;->isSilent:Z

    return v0
.end method

.method public setSilent(Z)V
    .locals 0
    .param p1, "silent"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;->isSilent:Z

    .line 66
    return-void
.end method

.method public updateNotification(Landroid/content/Context;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isSafeReaderPlaying"    # Z

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;->nm:Landroid/app/NotificationManager;

    iget v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;->mainNotificationId:I

    const/4 v2, 0x0

    invoke-virtual {p0, p1, p2, v2}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderOnNotification;->getNotification(Landroid/content/Context;ZZ)Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 27
    return-void
.end method
