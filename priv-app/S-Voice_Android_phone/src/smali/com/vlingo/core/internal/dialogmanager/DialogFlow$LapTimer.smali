.class Lcom/vlingo/core/internal/dialogmanager/DialogFlow$LapTimer;
.super Lcom/vlingo/core/internal/util/PrecisionTimer;
.source "DialogFlow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LapTimer"
.end annotation


# instance fields
.field private laps:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1032
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/PrecisionTimer;-><init>()V

    .line 1053
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$LapTimer;->laps:Ljava/util/LinkedList;

    return-void
.end method


# virtual methods
.method public elapsed()J
    .locals 2

    .prologue
    .line 1036
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$LapTimer;->elapsedInterval()Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->span()J

    move-result-wide v0

    return-wide v0
.end method

.method public elapsedInterval()Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;
    .locals 2

    .prologue
    .line 1041
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$LapTimer;->laps:Ljava/util/LinkedList;

    invoke-super {p0}, Lcom/vlingo/core/internal/util/PrecisionTimer;->elapsedInterval()Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1042
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$LapTimer;->laps:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1047
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1048
    .local v2, "me":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$LapTimer;->laps:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;

    .line 1049
    .local v1, "interval":Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1050
    .end local v1    # "interval":Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
