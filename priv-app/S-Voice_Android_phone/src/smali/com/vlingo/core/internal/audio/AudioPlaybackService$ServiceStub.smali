.class Lcom/vlingo/core/internal/audio/AudioPlaybackService$ServiceStub;
.super Landroid/os/Binder;
.source "AudioPlaybackService.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/audio/AudioPlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ServiceStub"
.end annotation


# instance fields
.field mService:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/core/internal/audio/AudioPlaybackService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/audio/AudioPlaybackService;)V
    .locals 1
    .param p1, "service"    # Lcom/vlingo/core/internal/audio/AudioPlaybackService;

    .prologue
    .line 97
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 98
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    .line 99
    return-void
.end method


# virtual methods
.method public isPlaying()Z
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->pause()V

    .line 115
    return-void
.end method

.method public play(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->play(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 103
    return-void
.end method

.method public play(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "listener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;

    invoke-virtual {v0, p1, p2}, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->play(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 107
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->resume()V

    .line 119
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->stop()V

    .line 123
    return-void
.end method
