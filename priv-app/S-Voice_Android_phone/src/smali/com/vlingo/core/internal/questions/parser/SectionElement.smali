.class public Lcom/vlingo/core/internal/questions/parser/SectionElement;
.super Lcom/vlingo/core/internal/questions/parser/ResponseElement;
.source "SectionElement.java"

# interfaces
.implements Lcom/vlingo/core/internal/questions/Answer$Section;


# instance fields
.field private mSubsections:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/questions/parser/SubsectionElement;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/questions/parser/ResponseElement;-><init>(Ljava/lang/String;)V

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/questions/parser/SectionElement;->mSubsections:Ljava/util/ArrayList;

    .line 18
    return-void
.end method


# virtual methods
.method public add(Lcom/vlingo/core/internal/questions/parser/SubsectionElement;)V
    .locals 1
    .param p1, "subsection"    # Lcom/vlingo/core/internal/questions/parser/SubsectionElement;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/SectionElement;->mSubsections:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 28
    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    const-string/jumbo v0, "Name"

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/questions/parser/SectionElement;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSubsections()[Lcom/vlingo/core/internal/questions/Answer$Subsection;
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/vlingo/core/internal/questions/parser/SectionElement;->mSubsections:Ljava/util/ArrayList;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/vlingo/core/internal/questions/Answer$Subsection;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/questions/Answer$Subsection;

    return-object v0
.end method
