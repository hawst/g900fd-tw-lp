.class public Lcom/vlingo/core/internal/localsearch/LocalSearchChineseResponseParser;
.super Ljava/lang/Object;
.source "LocalSearchChineseResponseParser.java"

# interfaces
.implements Lcom/vlingo/core/internal/localsearch/LocalSearchParser;


# static fields
.field private static final ARRAY_NAME_BUSINESS:Ljava/lang/String; = "businesses"

.field private static final ARRAY_NAME_REVIEWS:Ljava/lang/String; = "reviews"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/vlingo/core/internal/localsearch/LocalSearchChineseResponseParser;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/localsearch/LocalSearchChineseResponseParser;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private addJSONToBusinessItem(Lorg/json/JSONObject;Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;)V
    .locals 4
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .param p2, "bi"    # Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 83
    const-string/jumbo v0, ""

    .line 84
    .local v0, "value":Ljava/lang/String;
    const-string/jumbo v1, "city"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 85
    const-string/jumbo v1, "city"

    invoke-virtual {p2, v1, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->putValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const-string/jumbo v1, "address"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 88
    const-string/jumbo v1, "address"

    invoke-virtual {p2, v1, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->putValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string/jumbo v1, "business_url"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 91
    const-string/jumbo v1, "business_url"

    invoke-virtual {p2, v1, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->putValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string/jumbo v1, "distance"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 94
    const-string/jumbo v1, "distance"

    invoke-virtual {p2, v1, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->putValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string/jumbo v1, "business_id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 97
    const-string/jumbo v1, "business_id"

    invoke-virtual {p2, v1, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->putValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const-string/jumbo v1, "name"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 100
    const-string/jumbo v1, "name"

    invoke-virtual {p2, v1, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->putValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const-string/jumbo v1, "telephone"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 103
    const-string/jumbo v1, "telephone"

    invoke-virtual {p2, v1, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->putValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string/jumbo v1, "avg_rating"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 106
    const-string/jumbo v1, "avg_rating"

    invoke-virtual {p2, v1, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->putValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string/jumbo v1, "review_count"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 109
    const-string/jumbo v1, "review_count"

    invoke-virtual {p2, v1, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->putValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const-string/jumbo v1, "latitude"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112
    const-string/jumbo v1, "latitude"

    invoke-virtual {p2, v1, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->putValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string/jumbo v1, "longitude"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 115
    const-string/jumbo v1, "longitude"

    invoke-virtual {p2, v1, v0}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->putValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string/jumbo v1, "Provider"

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_provider_dianping:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->putValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    return-void
.end method

.method private parseReviews(Lorg/json/JSONObject;Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;
    .locals 2
    .param p1, "review"    # Lorg/json/JSONObject;
    .param p2, "bi"    # Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 121
    new-instance v0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;

    invoke-direct {v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;-><init>()V

    .line 122
    .local v0, "rev":Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;
    const-string/jumbo v1, "user_nickname"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;->author:Ljava/lang/String;

    .line 123
    const-string/jumbo v1, "text_excerpt"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;->body:Ljava/lang/String;

    .line 124
    const-string/jumbo v1, "created_time"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;->date:Ljava/lang/String;

    .line 125
    const-string/jumbo v1, "review_id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;->id:Ljava/lang/String;

    .line 126
    const-string/jumbo v1, "review_rating"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;->rating:Ljava/lang/String;

    .line 127
    return-object v0
.end method


# virtual methods
.method public parseLocalSearchResponse(Lcom/vlingo/sdk/internal/http/HttpResponse;Lcom/vlingo/core/internal/localsearch/LocalSearchListing;)Landroid/util/Pair;
    .locals 18
    .param p1, "response"    # Lcom/vlingo/sdk/internal/http/HttpResponse;
    .param p2, "existingBusinessItem"    # Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/internal/http/HttpResponse;",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchListing;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;",
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchListing;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 33
    new-instance v13, Ljava/util/Vector;

    invoke-direct {v13}, Ljava/util/Vector;-><init>()V

    .line 34
    .local v13, "results":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/vlingo/core/internal/localsearch/LocalSearchListing;>;"
    new-instance v12, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;

    invoke-direct {v12}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;-><init>()V

    .line 36
    .local v12, "requestInfo":Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v15

    invoke-virtual {v15}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getBusinessItemCache()Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;

    move-result-object v10

    .line 38
    .local v10, "listingCache":Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/sdk/internal/http/HttpResponse;->getDataAsString()Ljava/lang/String;

    move-result-object v9

    .line 41
    .local v9, "jsonResponse":Ljava/lang/String;
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 42
    .local v8, "jsonObject":Lorg/json/JSONObject;
    const/4 v6, 0x0

    .line 43
    .local v6, "jsonArray":Lorg/json/JSONArray;
    const/4 v7, 0x0

    .line 44
    .local v7, "jsonItem":Lorg/json/JSONObject;
    const/4 v2, 0x0

    .line 45
    .local v2, "count":I
    const-string/jumbo v15, "status"

    invoke-virtual {v8, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string/jumbo v16, "OK"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 46
    const-string/jumbo v15, "count"

    invoke-virtual {v8, v15}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 47
    const-string/jumbo v15, "businesses"

    invoke-virtual {v8, v15}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 48
    const-string/jumbo v15, "businesses"

    invoke-virtual {v8, v15}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 50
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v2, :cond_1

    .line 52
    invoke-virtual {v6, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 53
    const-string/jumbo v15, "business_id"

    invoke-virtual {v7, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 54
    .local v11, "listingId":Ljava/lang/String;
    const/4 v1, 0x0

    .line 55
    .local v1, "bi":Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;
    if-nez v1, :cond_0

    .line 56
    new-instance v1, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;

    .end local v1    # "bi":Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;
    invoke-direct {v1, v11}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;-><init>(Ljava/lang/String;)V

    .line 58
    .restart local v1    # "bi":Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;
    :cond_0
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchChineseResponseParser;->addJSONToBusinessItem(Lorg/json/JSONObject;Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;)V

    .line 60
    invoke-virtual {v13, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 61
    invoke-virtual {v10, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;->add(Lcom/vlingo/core/internal/localsearch/LocalSearchListing;)V

    .line 50
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 65
    .end local v1    # "bi":Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;
    .end local v4    # "i":I
    .end local v11    # "listingId":Ljava/lang/String;
    :cond_1
    const-string/jumbo v15, "reviews"

    invoke-virtual {v8, v15}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_3

    if-eqz p2, :cond_3

    .line 66
    move-object/from16 v0, p2

    check-cast v0, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;

    move-object v1, v0

    .line 67
    .restart local v1    # "bi":Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_1
    if-ge v5, v2, :cond_2

    .line 68
    const-string/jumbo v15, "reviews"

    invoke-virtual {v8, v15}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v15

    invoke-virtual {v15, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchChineseResponseParser;->parseReviews(Lorg/json/JSONObject;Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;

    move-result-object v14

    .line 69
    .local v14, "rev":Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;
    invoke-virtual {v1, v14}, Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;->addReview(Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;)V

    .line 67
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 71
    .end local v14    # "rev":Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;
    :cond_2
    invoke-virtual {v13, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 72
    invoke-virtual {v10, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;->add(Lcom/vlingo/core/internal/localsearch/LocalSearchListing;)V

    .line 75
    .end local v1    # "bi":Lcom/vlingo/core/internal/localsearch/ChineseLocalSearchListing;
    .end local v5    # "j":I
    :cond_3
    new-instance v15, Landroid/util/Pair;

    invoke-direct {v15, v12, v13}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    .end local v2    # "count":I
    .end local v6    # "jsonArray":Lorg/json/JSONArray;
    .end local v7    # "jsonItem":Lorg/json/JSONObject;
    .end local v8    # "jsonObject":Lorg/json/JSONObject;
    .end local v9    # "jsonResponse":Ljava/lang/String;
    .end local v10    # "listingCache":Lcom/vlingo/core/internal/localsearch/LocalSearchListingCache;
    :goto_2
    return-object v15

    .line 76
    :catch_0
    move-exception v3

    .line 77
    .local v3, "ex":Ljava/lang/Exception;
    sget-object v15, Lcom/vlingo/core/internal/localsearch/LocalSearchChineseResponseParser;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "Exception parsing chinese local search response: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    const/4 v15, 0x0

    goto :goto_2
.end method
