.class public final Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
.super Ljava/lang/Object;
.source "PhraseSpotter.java"

# interfaces
.implements Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;
.implements Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;
.implements Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSAudioPlaybackListener;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSSharedPreferenceChangeListener;,
        Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;
    }
.end annotation


# static fields
.field private static final BATTERY_CHANGED_FILTER:Landroid/content/IntentFilter;

.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;


# instance fields
.field private volatile audioPlaying:Z

.field private isCharging:Z

.field private listener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

.field private final phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

.field private phraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

.field private final psBatteryChangedReceiver:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;

.field private final psSharedPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private volatile recoInProgress:Z

.field private volatile restartWhenStopped:Z

.field private volatile spottingRequested:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    const-class v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->TAG:Ljava/lang/String;

    .line 48
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->BATTERY_CHANGED_FILTER:Landroid/content/IntentFilter;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->restartWhenStopped:Z

    .line 64
    new-instance v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-direct {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;-><init>()V

    iput-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    .line 66
    new-instance v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSSharedPreferenceChangeListener;

    invoke-direct {v1, p0, v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSSharedPreferenceChangeListener;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->psSharedPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 68
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->psSharedPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 71
    new-instance v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;

    invoke-direct {v1, p0, v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;)V

    iput-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->psBatteryChangedReceiver:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;

    .line 72
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->psBatteryChangedReceiver:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;->register(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 73
    .local v0, "i":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 74
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isCharging(Landroid/content/Intent;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isCharging:Z

    .line 80
    :cond_0
    return-void
.end method

.method static synthetic access$1000()Landroid/content/IntentFilter;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->BATTERY_CHANGED_FILTER:Landroid/content/IntentFilter;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Landroid/content/Intent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isCharging(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->updatePhraseSpotterState()V

    return-void
.end method

.method static synthetic access$1302(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->audioPlaying:Z

    return p1
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->spottingRequested:Z

    return v0
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isCharging:Z

    return v0
.end method

.method static synthetic access$402(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isCharging:Z

    return p1
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->recoInProgress:Z

    return v0
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->startPhraseSpottingInternal(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpottingInternal()V

    return-void
.end method

.method static synthetic access$800(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->restartWhenStopped:Z

    return v0
.end method

.method static synthetic access$802(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->restartWhenStopped:Z

    return p1
.end method

.method static synthetic access$900(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->listener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    return-object v0
.end method

.method private declared-synchronized asyncStopPhraseSpottingWithResumingControl(Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)Z
    .locals 1
    .param p1, "control"    # Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .prologue
    .line 277
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->isInIdleState()Z

    move-result v0

    if-nez v0, :cond_0

    .line 280
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->disableSpottingRequest()V

    .line 281
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->stop(Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 282
    const/4 v0, 0x1

    .line 284
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 277
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized destroy()V
    .locals 4

    .prologue
    .line 99
    const-class v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->instance:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    if-eqz v0, :cond_0

    .line 103
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->instance:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 104
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v2, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->instance:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    iget-object v2, v2, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->psSharedPrefChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 105
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->RECOGNITION_START:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    sget-object v3, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->instance:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    invoke-virtual {v0, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->unregisterTaskFlowRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V

    .line 106
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->instance:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    :cond_0
    monitor-exit v1

    return-void

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private disableSpottingRequest()V
    .locals 1

    .prologue
    .line 272
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->spottingRequested:Z

    .line 273
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setDialogFlowTaskRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V

    .line 274
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
    .locals 2

    .prologue
    .line 57
    const-class v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->instance:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    invoke-direct {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->instance:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    .line 60
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->instance:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private isCharging(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 304
    const-string/jumbo v1, "plugged"

    const/4 v2, -0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 305
    .local v0, "plugged":I
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private startPhraseSpottingInternal(Z)V
    .locals 4
    .param p1, "registerTaskRegulator"    # Z

    .prologue
    .line 249
    sget-object v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startPhraseSpottingInternal()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isIUXComplete()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 253
    if-eqz p1, :cond_0

    .line 254
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->RECOGNITION_START:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    invoke-virtual {v1, v2, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->registerTaskRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V

    .line 256
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    invoke-virtual {v1, v2, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->start(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 263
    :cond_1
    :goto_0
    return-void

    .line 258
    :catch_0
    move-exception v0

    .line 259
    .local v0, "e":Ljava/lang/Exception;
    const-class v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Error starting PhraseSpotterControl: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private stopPhraseSpottingInternal()V
    .locals 2

    .prologue
    .line 266
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopPhraseSpottingInternal()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->stop()V

    .line 269
    return-void
.end method

.method private updatePhraseSpotterState()V
    .locals 1

    .prologue
    .line 220
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$1;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 246
    return-void
.end method


# virtual methods
.method public declared-synchronized init(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V
    .locals 1
    .param p1, "spotterParameters"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .param p2, "spotterListener"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    .prologue
    .line 86
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    .line 87
    iput-object p2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->listener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    monitor-exit p0

    return-void

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isListening()Z
    .locals 1

    .prologue
    .line 191
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->isListening()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onPhraseDetected(Ljava/lang/String;)V
    .locals 1
    .param p1, "spottedPhrase"    # Ljava/lang/String;

    .prologue
    .line 340
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$3;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$3;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 346
    return-void
.end method

.method public onPhraseSpotted(Ljava/lang/String;)V
    .locals 1
    .param p1, "spottedPhrase"    # Ljava/lang/String;

    .prologue
    .line 350
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$4;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$4;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 356
    return-void
.end method

.method public onPhraseSpotterStarted()V
    .locals 2

    .prologue
    .line 312
    iget-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->restartWhenStopped:Z

    if-nez v0, :cond_0

    .line 313
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onPhraseSpotterStarted: restartWhenStopped==false, so calling listener."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->listener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;->onPhraseSpotterStarted()V

    .line 316
    :cond_0
    return-void
.end method

.method public onPhraseSpotterStopped()V
    .locals 1

    .prologue
    .line 322
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$2;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$2;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;)V

    .line 336
    return-void
.end method

.method public onSeamlessPhraseSpotted(Ljava/lang/String;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
    .locals 1
    .param p1, "spottedPhrase"    # Ljava/lang/String;
    .param p2, "micStream"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    .line 360
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$5;

    invoke-direct {v0, p0, p1, p2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$5;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Ljava/lang/String;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 366
    return-void
.end method

.method public onTaskWaitingToStart(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)V
    .locals 2
    .param p1, "type"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;
    .param p2, "control"    # Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .prologue
    .line 290
    const/4 v0, 0x1

    .line 291
    .local v0, "resumeControlImmediately":Z
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->RECOGNITION_START:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    invoke-virtual {v1, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 292
    invoke-direct {p0, p2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->asyncStopPhraseSpottingWithResumingControl(Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 293
    const/4 v0, 0x0

    .line 297
    :cond_0
    if-eqz v0, :cond_1

    .line 298
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/ResumeControl;->resume()V

    .line 300
    :cond_1
    return-void
.end method

.method public setRecognitionContext(Lcom/vlingo/sdk/recognition/VLRecognitionContext;)V
    .locals 1
    .param p1, "srContext"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    .prologue
    .line 475
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setRecognitionContext(Lcom/vlingo/sdk/recognition/VLRecognitionContext;)V

    .line 476
    return-void
.end method

.method public declared-synchronized startPhraseSpotting()V
    .locals 2

    .prologue
    .line 145
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startPhraseSpotting()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->supportsSVoiceAssociatedServiceOnly()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    :goto_0
    monitor-exit p0

    return-void

    .line 150
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->spottingRequested:Z

    .line 154
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->restartWhenStopped:Z

    .line 157
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->updatePhraseSpotterState()V

    .line 160
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->psBatteryChangedReceiver:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;->register(Landroid/content/Context;)Landroid/content/Intent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stopPhraseSpotting()V
    .locals 2

    .prologue
    .line 171
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopPhraseSpotting()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->restartWhenStopped:Z

    .line 175
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->psBatteryChangedReceiver:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->psBatteryChangedReceiver:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$PSBatteryChangedReceiver;->unregister(Landroid/content/Context;)V

    .line 178
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->disableSpottingRequest()V

    .line 179
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpottingInternal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180
    monitor-exit p0

    return-void

    .line 171
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public stopPhraseSpottingRightNow()V
    .locals 2

    .prologue
    .line 184
    sget-object v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopPhraseSpottingRightNow()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 187
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 212
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "PS: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; control=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterControl:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; adapter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized updateAudioSettings(Z)V
    .locals 1
    .param p1, "forceSpotterToRun"    # Z

    .prologue
    .line 123
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->spottingRequested:Z

    if-eqz v0, :cond_1

    .line 125
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->restartWhenStopped:Z

    .line 126
    invoke-direct {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpottingInternal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 128
    :cond_1
    if-eqz p1, :cond_0

    .line 129
    :try_start_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->startPhraseSpotting()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized updateParameters(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;)V
    .locals 1
    .param p1, "spotterParameters"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    .prologue
    .line 92
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->phraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    .line 95
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->updateAudioSettings(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    monitor-exit p0

    return-void

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
