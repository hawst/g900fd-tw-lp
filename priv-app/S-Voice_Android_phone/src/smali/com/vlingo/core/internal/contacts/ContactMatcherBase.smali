.class public abstract Lcom/vlingo/core/internal/contacts/ContactMatcherBase;
.super Ljava/lang/Object;
.source "ContactMatcherBase.java"

# interfaces
.implements Lcom/vlingo/core/internal/contacts/AsyncContactSorterCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/contacts/ContactMatcherBase$3;
    }
.end annotation


# static fields
.field public static final ACTION_ADDRESS:Ljava/lang/String; = "address"

.field public static final ACTION_BIRTHDAY:Ljava/lang/String; = "birthday"

.field public static final ACTION_CALL:Ljava/lang/String; = "call"

.field public static final ACTION_EMAIL:Ljava/lang/String; = "email"

.field public static final ACTION_FACEBOOK:Ljava/lang/String; = "facebook"

.field public static final ACTION_MESSAGE:Ljava/lang/String; = "message"

.field public static final ACTION_SMS:Ljava/lang/String; = "sms"

.field private static final TAG:Ljava/lang/String;

.field public static final TYPE_AUTO_ACTION_ALWAYS:I = 0x2

.field public static final TYPE_AUTO_ACTION_IF_CONFIDENT:I = 0x1

.field public static final TYPE_AUTO_ACTION_NEVER:I

.field private static log:Lcom/vlingo/core/internal/logging/Logger;

.field private static sInstanceCounter:I


# instance fields
.field private isAddressBookEmpty:Z

.field private final mActionType:Lcom/vlingo/core/internal/contacts/ContactType;

.field private final mAddressTypes:[I

.field private final mAutoActionType:I

.field private mCheckAutoAction:Z

.field private mContext:Landroid/content/Context;

.field private final mEmailTypes:[I

.field private final mHandler:Landroid/os/Handler;

.field final mListener:Lcom/vlingo/core/internal/contacts/ContactMatchListener;

.field private final mPhoneTypes:[I

.field private mQuery:Ljava/lang/String;

.field private final mRecognizerConfidenceScore:F

.field private final mRequest:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

.field private mRequestThread:Ljava/lang/Thread;

.field private final mSocialTypes:[I

.field private mSuperList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/vlingo/core/internal/contacts/ContactMatcher;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 16
    const-class v0, Lcom/vlingo/core/internal/contacts/ContactMatcher;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->TAG:Ljava/lang/String;

    .line 49
    const/4 v0, 0x0

    sput v0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->sInstanceCounter:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/vlingo/core/internal/contacts/ContactMatchListener;Lcom/vlingo/core/internal/contacts/ContactType;[I[I[I[ILjava/lang/String;FILjava/util/List;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/vlingo/core/internal/contacts/ContactMatchListener;
    .param p3, "actionType"    # Lcom/vlingo/core/internal/contacts/ContactType;
    .param p4, "phoneTypes"    # [I
    .param p5, "emailTypes"    # [I
    .param p6, "socialTypes"    # [I
    .param p7, "addressTypes"    # [I
    .param p8, "query"    # Ljava/lang/String;
    .param p9, "confidenceScore"    # F
    .param p10, "autoActionType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vlingo/core/internal/contacts/ContactMatchListener;",
            "Lcom/vlingo/core/internal/contacts/ContactType;",
            "[I[I[I[I",
            "Ljava/lang/String;",
            "FI",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p11, "superList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mContext:Landroid/content/Context;

    .line 33
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mQuery:Ljava/lang/String;

    .line 45
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->isAddressBookEmpty:Z

    .line 47
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mSuperList:Ljava/util/List;

    .line 51
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mRequestThread:Ljava/lang/Thread;

    .line 71
    if-eqz p8, :cond_0

    invoke-virtual {p8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 72
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "query must not be null or empty"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 75
    :cond_1
    iput-object p2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mListener:Lcom/vlingo/core/internal/contacts/ContactMatchListener;

    .line 79
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mHandler:Landroid/os/Handler;

    .line 81
    iput-object p3, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mActionType:Lcom/vlingo/core/internal/contacts/ContactType;

    .line 82
    iput-object p4, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mPhoneTypes:[I

    .line 83
    iput-object p5, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mEmailTypes:[I

    .line 84
    iput-object p7, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mAddressTypes:[I

    .line 85
    iput-object p6, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mSocialTypes:[I

    .line 86
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mSuperList:Ljava/util/List;

    .line 87
    if-nez p11, :cond_2

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mCheckAutoAction:Z

    .line 88
    iput-object p8, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mQuery:Ljava/lang/String;

    .line 89
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mContext:Landroid/content/Context;

    .line 90
    move/from16 v0, p10

    iput v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mAutoActionType:I

    .line 92
    const-class v3, Lcom/vlingo/core/internal/contacts/ContactMatcher;

    monitor-enter v3

    .line 93
    :try_start_0
    sget v2, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->sInstanceCounter:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->sInstanceCounter:I

    .line 94
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    :try_start_1
    iput p9, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mRecognizerConfidenceScore:F

    .line 102
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->getContactFetchAndSortRequest()Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mRequest:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    .line 103
    new-instance v2, Ljava/lang/Thread;

    iget-object v3, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mRequest:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "ContactSortThread-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget v5, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->sInstanceCounter:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mRequestThread:Ljava/lang/Thread;

    .line 105
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mRequestThread:Ljava/lang/Thread;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 106
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mRequestThread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 112
    return-void

    .line 87
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 94
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 107
    :catch_0
    move-exception v1

    .line 108
    .local v1, "ex":Ljava/lang/RuntimeException;
    sget-object v2, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "ContactMatcher constructor failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-interface {p2}, Lcom/vlingo/core/internal/contacts/ContactMatchListener;->onContactMatchingFailed()V

    .line 110
    throw v1
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/contacts/ContactMatcherBase;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/contacts/ContactMatcherBase;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->attemptAutoAction(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/contacts/ContactMatcherBase;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/contacts/ContactMatcherBase;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->getSortedSublist(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private attemptAutoAction(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 182
    iget-boolean v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mCheckAutoAction:Z

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v5, :cond_2

    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->isRunning()Z

    move-result v2

    if-nez v2, :cond_0

    .line 186
    :cond_2
    iput-boolean v6, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mCheckAutoAction:Z

    .line 188
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 189
    .local v0, "firstContact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    const/4 v1, 0x0

    .line 191
    .local v1, "performAction":Z
    iget v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mAutoActionType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    .line 192
    const/4 v1, 0x1

    .line 212
    :cond_3
    :goto_1
    :pswitch_0
    if-eqz v1, :cond_0

    .line 213
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mListener:Lcom/vlingo/core/internal/contacts/ContactMatchListener;

    invoke-interface {v2, v0}, Lcom/vlingo/core/internal/contacts/ContactMatchListener;->onAutoAction(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    goto :goto_0

    .line 193
    :cond_4
    iget v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mAutoActionType:I

    if-ne v2, v5, :cond_3

    iget v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mRecognizerConfidenceScore:F

    const v3, 0x3dcccccd    # 0.1f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_3

    .line 196
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-eq v2, v5, :cond_5

    invoke-virtual {v0, v5}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getScore(Z)F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v2, v5}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getScore(Z)F

    move-result v2

    add-float/2addr v2, v4

    cmpl-float v2, v3, v2

    if-lez v2, :cond_3

    .line 197
    :cond_5
    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 205
    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/contacts/ContactData;->getScore()F

    move-result v3

    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/contacts/ContactData;->getScore()F

    move-result v2

    sub-float v2, v3, v2

    const v3, 0x3d4ccccd    # 0.05f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    .line 206
    const/4 v1, 0x1

    goto :goto_1

    .line 199
    :pswitch_1
    const/4 v1, 0x1

    .line 200
    goto :goto_1

    .line 197
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private attemptFindContact(Ljava/util/List;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 304
    .local p1, "overlapList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 305
    .local v2, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget-object v3, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 307
    .local v3, "contactName":Ljava/lang/String;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 308
    .local v4, "contactWords":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v11, " "

    invoke-virtual {v3, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v7, :cond_1

    aget-object v10, v0, v6

    .line 309
    .local v10, "singleName":Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v4, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 308
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 312
    .end local v10    # "singleName":Ljava/lang/String;
    :cond_1
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 313
    .local v8, "requestWords":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v11, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mQuery:Ljava/lang/String;

    const-string/jumbo v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v7, v0

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v7, :cond_2

    aget-object v9, v0, v6

    .line 314
    .local v9, "singleContactRequest":Ljava/lang/String;
    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v8, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 313
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 320
    .end local v9    # "singleContactRequest":Ljava/lang/String;
    :cond_2
    invoke-virtual {v4, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 321
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 322
    .local v1, "changedOverlapList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 326
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "changedOverlapList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v2    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v3    # "contactName":Ljava/lang/String;
    .end local v4    # "contactWords":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6    # "i$":I
    .end local v7    # "len$":I
    .end local v8    # "requestWords":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_2
    return-object v1

    :cond_3
    move-object v1, p1

    goto :goto_2
.end method

.method private getSortedSublist(Ljava/util/List;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 274
    .local p1, "subList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    iget-object v5, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mSuperList:Ljava/util/List;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mSuperList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_1

    .line 292
    .end local p1    # "subList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_0
    :goto_0
    return-object p1

    .line 277
    .restart local p1    # "subList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 278
    .local v4, "overlap":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 279
    .local v2, "newMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget-object v5, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mSuperList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 280
    .local v3, "oldMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget-wide v5, v3, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    iget-wide v7, v2, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    cmp-long v5, v5, v7

    if-nez v5, :cond_3

    .line 281
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 285
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "newMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v3    # "oldMatch":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_4
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-eqz v5, :cond_0

    .line 288
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-ne v5, v6, :cond_5

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    iget-object v6, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mSuperList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ne v5, v6, :cond_5

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    if-le v5, v6, :cond_5

    .line 290
    invoke-direct {p0, v4}, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->attemptFindContact(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    :cond_5
    move-object p1, v4

    .line 292
    goto :goto_0
.end method

.method private declared-synchronized isRunning()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 222
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mRequest:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    if-eqz v1, :cond_0

    .line 223
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mRequest:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->hasStarted()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mRequest:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->isDone()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 225
    :cond_0
    monitor-exit p0

    return v0

    .line 222
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public determinePerformAction(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/4 v5, 0x1

    .line 154
    const/4 v2, 0x0

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 155
    .local v0, "firstContact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    const/4 v1, 0x0

    .line 157
    .local v1, "performAction":Z
    iget v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mAutoActionType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 158
    const/4 v1, 0x1

    .line 173
    :cond_0
    :goto_0
    return v1

    .line 159
    :cond_1
    iget v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mAutoActionType:I

    if-ne v2, v5, :cond_0

    .line 160
    iget v2, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mRecognizerConfidenceScore:F

    const v3, 0x3dcccccd    # 0.1f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    .line 162
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-eq v2, v5, :cond_2

    invoke-virtual {v0, v5}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getScore(Z)F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v2, v5}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getScore(Z)F

    move-result v2

    add-float/2addr v2, v4

    cmpl-float v2, v3, v2

    if-lez v2, :cond_0

    .line 163
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 167
    const/4 v1, 0x0

    goto :goto_0

    .line 164
    :pswitch_0
    const/4 v1, 0x1

    .line 165
    goto :goto_0

    .line 163
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getActionType()Lcom/vlingo/core/internal/contacts/ContactType;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mActionType:Lcom/vlingo/core/internal/contacts/ContactType;

    return-object v0
.end method

.method public getAddressTypes()[I
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mAddressTypes:[I

    return-object v0
.end method

.method public getAutoActionType()I
    .locals 1

    .prologue
    .line 218
    iget v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mAutoActionType:I

    return v0
.end method

.method public getConfidenceScore()F
    .locals 1

    .prologue
    .line 371
    iget v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mRecognizerConfidenceScore:F

    return v0
.end method

.method protected abstract getContactFetchAndSortRequest()Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getEmailTypes()[I
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mEmailTypes:[I

    return-object v0
.end method

.method public getNumContacts()I
    .locals 2

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->getSortedContacts()Ljava/util/List;

    move-result-object v0

    .line 178
    .local v0, "sortedContacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPhoneTypes()[I
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mPhoneTypes:[I

    return-object v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mQuery:Ljava/lang/String;

    return-object v0
.end method

.method protected getRequestedTypes()[I
    .locals 3

    .prologue
    .line 125
    const/4 v0, 0x0

    .line 126
    .local v0, "requestedTypes":[I
    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactMatcherBase$3;->$SwitchMap$com$vlingo$core$internal$contacts$ContactType:[I

    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->getActionType()Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/contacts/ContactType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 145
    :goto_0
    return-object v0

    .line 129
    :pswitch_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->getPhoneTypes()[I

    move-result-object v0

    .line 130
    goto :goto_0

    .line 132
    :pswitch_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->getEmailTypes()[I

    move-result-object v0

    .line 133
    goto :goto_0

    .line 135
    :pswitch_2
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->getAddressTypes()[I

    move-result-object v0

    .line 136
    goto :goto_0

    .line 138
    :pswitch_3
    invoke-virtual {p0}, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->getSocialTypes()[I

    move-result-object v0

    .line 139
    goto :goto_0

    .line 126
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getSocialTypes()[I
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mSocialTypes:[I

    return-object v0
.end method

.method public declared-synchronized getSortedContacts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mRequest:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mRequest:Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactFetchAndSortRequestBase;->getSortedContacts()Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 231
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 229
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isAddressBookEmpty()Z
    .locals 1

    .prologue
    .line 235
    iget-boolean v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->isAddressBookEmpty:Z

    return v0
.end method

.method public onAsyncSortingFailed()V
    .locals 1

    .prologue
    .line 334
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mContext:Landroid/content/Context;

    .line 335
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mListener:Lcom/vlingo/core/internal/contacts/ContactMatchListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/contacts/ContactMatchListener;->onContactMatchingFailed()V

    .line 336
    return-void
.end method

.method public onAsyncSortingFinished(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 255
    .local p1, "sortedContacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBUtilManager;->getContactDBUtil()Lcom/vlingo/core/internal/contacts/IContactDBUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/contacts/IContactDBUtil;->isAddressBookEmpty(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->isAddressBookEmpty:Z

    .line 263
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mContext:Landroid/content/Context;

    .line 264
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/vlingo/core/internal/contacts/ContactMatcherBase$2;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/core/internal/contacts/ContactMatcherBase$2;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatcherBase;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 271
    return-void
.end method

.method public onAsyncSortingUpdated(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 244
    .local p1, "sortedContacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/vlingo/core/internal/contacts/ContactMatcherBase$1;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/core/internal/contacts/ContactMatcherBase$1;-><init>(Lcom/vlingo/core/internal/contacts/ContactMatcherBase;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 250
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 343
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactMatcherBase;->mContext:Landroid/content/Context;

    .line 344
    return-void
.end method
