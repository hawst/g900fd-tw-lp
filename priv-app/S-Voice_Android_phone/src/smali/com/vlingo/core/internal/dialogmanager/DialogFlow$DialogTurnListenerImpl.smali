.class Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;
.super Ljava/lang/Object;
.source "DialogFlow.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DialogTurnListenerImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V
    .locals 0

    .prologue
    .line 1269
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Lcom/vlingo/core/internal/dialogmanager/DialogFlow$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow$1;

    .prologue
    .line 1269
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V

    return-void
.end method


# virtual methods
.method public endpointReco()V
    .locals 1

    .prologue
    .line 1357
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->endpointReco()V

    .line 1358
    return-void
.end method

.method public enteredDomain(Lcom/vlingo/core/internal/domain/DomainName;)V
    .locals 1
    .param p1, "domain"    # Lcom/vlingo/core/internal/domain/DomainName;

    .prologue
    .line 1447
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->enteredDomain(Lcom/vlingo/core/internal/domain/DomainName;)V

    .line 1448
    return-void
.end method

.method public execute(Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;)V
    .locals 3
    .param p1, "action"    # Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    .prologue
    .line 1291
    if-eqz p1, :cond_0

    .line 1292
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/tasks/ActionTask;

    invoke-direct {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/tasks/ActionTask;-><init>(Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;)V

    .line 1293
    .local v0, "task":Lcom/vlingo/core/internal/dialogmanager/tasks/ActionTask;
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$500(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 1294
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;
    invoke-static {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$600(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/util/TaskQueue;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/util/TaskQueue;->queueTask(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V

    .line 1295
    monitor-exit v2

    .line 1297
    .end local v0    # "task":Lcom/vlingo/core/internal/dialogmanager/tasks/ActionTask;
    :cond_0
    return-void

    .line 1295
    .restart local v0    # "task":Lcom/vlingo/core/internal/dialogmanager/tasks/ActionTask;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public finishDialog()V
    .locals 1

    .prologue
    .line 1403
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->finishDialog()V

    .line 1404
    return-void
.end method

.method public finishTurn()V
    .locals 1

    .prologue
    .line 1367
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->finishTurn()V
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1200(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V

    .line 1368
    return-void
.end method

.method public getActivityContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1394
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->activityContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1400(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1395
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->activityContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1400(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Landroid/content/Context;

    move-result-object v0

    .line 1397
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0
.end method

.method public getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;
    .locals 1
    .param p1, "type"    # Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    .prologue
    .line 1385
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogData:Ljava/util/Map;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1386
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogData:Ljava/util/Map;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1388
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public interruptTurn()V
    .locals 1

    .prologue
    .line 1362
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->interruptTurn()V

    .line 1363
    return-void
.end method

.method public onAsyncActionStarted()V
    .locals 3

    .prologue
    .line 1424
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$500(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1425
    :try_start_0
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "onAsyncActionStarted()"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1426
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    const/4 v2, 0x1

    # setter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isProcessingActions:Z
    invoke-static {v0, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$802(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Z)Z

    .line 1427
    monitor-exit v1

    .line 1428
    return-void

    .line 1427
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDoneProcessingActions(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V
    .locals 1
    .param p1, "turn"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .prologue
    .line 1271
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->finishActionListProcessing()V
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$400(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V

    .line 1272
    return-void
.end method

.method public playMedia(I)V
    .locals 1
    .param p1, "fileResId"    # I

    .prologue
    .line 1286
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->playMedia(I)V

    .line 1287
    return-void
.end method

.method public sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/vlingo/core/internal/dialogmanager/DialogEvent;

    .prologue
    .line 1336
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$500(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1337
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->createDMTransactionTask()V
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$700(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V

    .line 1338
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isProcessingActions:Z
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$800(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1339
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$600(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/util/TaskQueue;

    move-result-object v0

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$900(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/util/TaskQueue;->queueTask(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V

    .line 1340
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    const/4 v2, 0x0

    # setter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->pendingDMTransactionTask:Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;
    invoke-static {v0, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$902(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;)Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .line 1342
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;->isTerminalState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1343
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->resetState()V
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1000(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V

    .line 1345
    :cond_1
    monitor-exit v1

    .line 1346
    return-void

    .line 1345
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public sendTextRequest(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 1419
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->sendTextRequest(Ljava/lang/String;)Z
    invoke-static {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1800(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Ljava/lang/String;)Z

    .line 1420
    return-void
.end method

.method public showUserText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 1306
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1307
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->showUserText(Ljava/lang/String;Lcom/vlingo/sdk/recognition/NBestData;)V

    .line 1309
    :cond_0
    return-void
.end method

.method public showUserText(Ljava/lang/String;Lcom/vlingo/sdk/recognition/NBestData;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "nbest"    # Lcom/vlingo/sdk/recognition/NBestData;

    .prologue
    .line 1301
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->showUserText(Ljava/lang/String;Lcom/vlingo/sdk/recognition/NBestData;)V

    .line 1302
    return-void
.end method

.method public showVlingoText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 1313
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1314
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogFlowListener:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;->showVlingoText(Ljava/lang/String;)V

    .line 1316
    :cond_0
    return-void
.end method

.method public showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "displayText"    # Ljava/lang/String;
    .param p2, "spokenText"    # Ljava/lang/String;

    .prologue
    .line 1320
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1321
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->showVlingoText(Ljava/lang/String;)V

    .line 1323
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getVlingoApp()Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/util/VlingoApplicationInterface;->isAppInForeground()Z

    move-result v0

    .line 1324
    .local v0, "isAppInForeground":Z
    invoke-static {p2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz v0, :cond_1

    .line 1325
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    iget-object v2, v2, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    const/4 v3, 0x1

    invoke-direct {v1, v2, p2, v3}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;-><init>(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;Ljava/lang/String;Z)V

    .line 1326
    .local v1, "task":Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$500(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 1327
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogQueue:Lcom/vlingo/core/internal/util/TaskQueue;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$600(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/util/TaskQueue;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/vlingo/core/internal/util/TaskQueue;->queueTask(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V

    .line 1328
    monitor-exit v3

    .line 1330
    .end local v1    # "task":Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
    :cond_1
    return-void

    .line 1328
    .restart local v1    # "task":Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p2, "decorators"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            "TT;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1409
    .local p3, "object":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->widgetFactory:Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1500(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1410
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->widgetFactory:Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1500(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 1411
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->EMBEDDED_APPS:Ljava/util/Map;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1600()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1412
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->setLastEmbeddedAppWidgetKey(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)V
    invoke-static {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1700(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)V

    .line 1415
    :cond_0
    return-void
.end method

.method public startReco()V
    .locals 3

    .prologue
    .line 1350
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->taskQueueMutex:Ljava/lang/Object;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$500(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1351
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    const/4 v2, 0x0

    # invokes: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->listen(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z
    invoke-static {v0, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1100(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    .line 1352
    monitor-exit v1

    .line 1353
    return-void

    .line 1352
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V
    .locals 1
    .param p1, "type"    # Lcom/vlingo/core/internal/dialogmanager/DialogDataType;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 1380
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->dialogData:Ljava/util/Map;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1300(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1381
    return-void
.end method

.method public tts(Ljava/lang/String;)V
    .locals 1
    .param p1, "tts"    # Ljava/lang/String;

    .prologue
    .line 1276
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->tts(Ljava/lang/String;)V

    .line 1277
    return-void
.end method

.method public tts(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 1
    .param p1, "tts"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    .line 1442
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    invoke-virtual {v0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->tts(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 1443
    return-void
.end method

.method public ttsAnyway(Ljava/lang/String;)V
    .locals 1
    .param p1, "tts"    # Ljava/lang/String;

    .prologue
    .line 1281
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->ttsAnyway(Ljava/lang/String;)V

    .line 1282
    return-void
.end method

.method public ttsAnyway(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 1
    .param p1, "tts"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    .line 1437
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    invoke-virtual {v0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->ttsAnyway(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 1438
    return-void
.end method

.method public userCancel()V
    .locals 1

    .prologue
    .line 1432
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogTurnListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->userCancel()V

    .line 1433
    return-void
.end method
