.class public final Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;
.super Ljava/lang/Object;
.source "PrecisionTimer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/PrecisionTimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Interval"
.end annotation


# static fields
.field private static final BASE_UNIT:J = 0x1L

.field private static final PER_HOUR:J = 0x34630b8a000L

.field private static final PER_MICRO:J = 0x3e8L

.field private static final PER_MILLI:J = 0xf4240L

.field private static final PER_MINUTE:J = 0xdf8475800L

.field private static final PER_NANO:J = 0x1L

.field private static final PER_SECOND:J = 0x3b9aca00L


# instance fields
.field private hours:J

.field private micros:J

.field private millis:J

.field private minutes:J

.field private nanos:J

.field private precision:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

.field private seconds:J

.field private span:J


# direct methods
.method private constructor <init>(J)V
    .locals 1
    .param p1, "span"    # J

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    sget-object v0, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->NANOSECONDS:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    iput-object v0, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->precision:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    .line 84
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->setSpan(J)V

    .line 85
    return-void
.end method

.method synthetic constructor <init>(JLcom/vlingo/core/internal/util/PrecisionTimer$1;)V
    .locals 0
    .param p1, "x0"    # J
    .param p3, "x1"    # Lcom/vlingo/core/internal/util/PrecisionTimer$1;

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;-><init>(J)V

    return-void
.end method

.method private setSpan(J)V
    .locals 3
    .param p1, "span"    # J

    .prologue
    .line 133
    new-instance v0, Lcom/vlingo/core/internal/util/PrecisionTimer$Remainder;

    invoke-direct {v0, p1, p2}, Lcom/vlingo/core/internal/util/PrecisionTimer$Remainder;-><init>(J)V

    .line 135
    .local v0, "remainder":Lcom/vlingo/core/internal/util/PrecisionTimer$Remainder;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/PrecisionTimer$Remainder;->value()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->span:J

    .line 137
    const-wide v1, 0x34630b8a000L

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/util/PrecisionTimer$Remainder;->dividedBy(J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->hours:J

    .line 138
    const-wide v1, 0xdf8475800L

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/util/PrecisionTimer$Remainder;->dividedBy(J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->minutes:J

    .line 139
    const-wide/32 v1, 0x3b9aca00

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/util/PrecisionTimer$Remainder;->dividedBy(J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->seconds:J

    .line 140
    const-wide/32 v1, 0xf4240

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/util/PrecisionTimer$Remainder;->dividedBy(J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->millis:J

    .line 141
    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/util/PrecisionTimer$Remainder;->dividedBy(J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->micros:J

    .line 142
    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/util/PrecisionTimer$Remainder;->dividedBy(J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->nanos:J

    .line 144
    return-void
.end method


# virtual methods
.method public compare(Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;)I
    .locals 5
    .param p1, "t"    # Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;

    .prologue
    const/4 v0, 0x1

    .line 109
    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v1, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->span:J

    iget-wide v3, p1, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->span:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-wide v1, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->span:J

    iget-wide v3, p1, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->span:J

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public gt(Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;)Z
    .locals 1
    .param p1, "t"    # Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;

    .prologue
    .line 101
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->compare(Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lt(Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;)Z
    .locals 1
    .param p1, "t"    # Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;

    .prologue
    .line 105
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->compare(Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public precision(Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;)Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;
    .locals 0
    .param p1, "precisionParam"    # Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->precision:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    .line 97
    return-object p0
.end method

.method public precision()Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->precision:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    return-object v0
.end method

.method public span()J
    .locals 2

    .prologue
    .line 88
    iget-wide v0, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->span:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 116
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->precision()Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->NANOSECONDS:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    if-ne v0, v1, :cond_0

    .line 117
    const-string/jumbo v0, "%02d:%02d:%02d.%03d.%03d.%03d"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->hours:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->minutes:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v5

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->seconds:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->millis:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v7

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->micros:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v8

    const/4 v2, 0x5

    iget-wide v3, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->nanos:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 126
    :goto_0
    return-object v0

    .line 118
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->precision()Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->MICROSECONDS:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    if-ne v0, v1, :cond_1

    .line 119
    const-string/jumbo v0, "%02d:%02d:%02d.%03d.%03d"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->hours:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->minutes:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v5

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->seconds:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->millis:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v7

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->micros:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 120
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->precision()Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->MILLISECONDS:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    if-ne v0, v1, :cond_2

    .line 121
    const-string/jumbo v0, "%02d:%02d:%02d.%03d"

    new-array v1, v8, [Ljava/lang/Object;

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->hours:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->minutes:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v5

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->seconds:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->millis:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 122
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->precision()Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->SECONDS:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    if-ne v0, v1, :cond_3

    .line 123
    const-string/jumbo v0, "%02d:%02d:%02d"

    new-array v1, v7, [Ljava/lang/Object;

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->hours:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->minutes:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v5

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->seconds:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 124
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->precision()Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;->MINUTES:Lcom/vlingo/core/internal/util/PrecisionTimer$Precision;

    if-ne v0, v1, :cond_4

    .line 125
    const-string/jumbo v0, "%02d:%02d"

    new-array v1, v6, [Ljava/lang/Object;

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->hours:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->minutes:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 126
    :cond_4
    const-string/jumbo v0, "%02d"

    new-array v1, v5, [Ljava/lang/Object;

    iget-wide v2, p0, Lcom/vlingo/core/internal/util/PrecisionTimer$Interval;->hours:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method
