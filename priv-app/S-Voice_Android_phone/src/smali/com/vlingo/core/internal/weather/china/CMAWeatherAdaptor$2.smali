.class Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;
.super Ljava/lang/Object;
.source "CMAWeatherAdaptor.java"

# interfaces
.implements Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->sendRequestWithCurrentLocation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;->this$0:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRequestComplete(ZLjava/lang/Object;)V
    .locals 4
    .param p1, "success"    # Z
    .param p2, "items"    # Ljava/lang/Object;

    .prologue
    .line 223
    iget-object v2, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;->this$0:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    const/4 v3, 0x1

    # setter for: Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestComplete:Z
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->access$002(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;Z)Z

    .line 224
    if-eqz p1, :cond_0

    .line 228
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;->this$0:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    new-instance v3, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    invoke-direct {v3}, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;-><init>()V

    # setter for: Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->access$102(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;)Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    .line 229
    new-instance v1, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;

    iget-object v2, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;->this$0:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    # getter for: Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;
    invoke-static {v2}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->access$100(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;)Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;-><init>(Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;)V

    .line 230
    .local v1, "parser":Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;
    check-cast p2, Ljava/lang/String;

    .end local p2    # "items":Ljava/lang/Object;
    invoke-virtual {v1, p2}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->parseCurrentLocation(Ljava/lang/String;)V

    .line 231
    iget-object v2, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;->this$0:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    iget-object v3, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;->this$0:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    # getter for: Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;
    invoke-static {v3}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->access$100(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;)Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->sendRequestWithAreaID(Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    .end local v1    # "parser":Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;
    :cond_0
    :goto_0
    return-void

    .line 232
    :catch_0
    move-exception v0

    .line 233
    .local v0, "e":Lorg/json/JSONException;
    iget-object v2, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;->this$0:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    # getter for: Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;
    invoke-static {v2}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->access$200(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;)Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 234
    iget-object v2, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;->this$0:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    # getter for: Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;
    invoke-static {v2}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->access$200(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;)Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed()V

    goto :goto_0
.end method

.method public onRequestFailed(Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;)V
    .locals 2
    .param p1, "failureType"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;

    .prologue
    .line 207
    sget-object v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$3;->$SwitchMap$com$vlingo$core$internal$localsearch$LocalSearchRequestListener$LocalSearchFailureType:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 219
    :goto_0
    return-void

    .line 209
    :pswitch_0
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;->this$0:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    # getter for: Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->access$200(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;)Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_bad_response:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0

    .line 212
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;->this$0:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    # getter for: Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->access$200(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;)Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_no_results:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0

    .line 215
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;->this$0:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    # getter for: Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->access$200(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;)Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_bad_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0

    .line 207
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onRequestFailed(Ljava/lang/String;)V
    .locals 2
    .param p1, "failReason"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 198
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;->this$0:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    # setter for: Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestComplete:Z
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->access$002(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;Z)Z

    .line 199
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;->this$0:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    # setter for: Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestFailed:Z
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->access$302(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;Z)Z

    .line 200
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;->this$0:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    # getter for: Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->access$200(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;)Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;->this$0:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    # getter for: Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->access$200(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;)Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed()V

    .line 203
    :cond_0
    return-void
.end method

.method public onRequestScheduled()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 189
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;->this$0:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    # setter for: Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestComplete:Z
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->access$002(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;Z)Z

    .line 190
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;->this$0:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    # setter for: Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestFailed:Z
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->access$302(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;Z)Z

    .line 191
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;->this$0:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    # getter for: Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->access$200(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;)Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;->this$0:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    # getter for: Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->access$200(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;)Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestScheduled()V

    .line 194
    :cond_0
    return-void
.end method
