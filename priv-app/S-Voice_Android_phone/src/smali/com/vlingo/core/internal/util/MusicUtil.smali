.class public Lcom/vlingo/core/internal/util/MusicUtil;
.super Ljava/lang/Object;
.source "MusicUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/util/MusicUtil$1;,
        Lcom/vlingo/core/internal/util/MusicUtil$TitleInfo;,
        Lcom/vlingo/core/internal/util/MusicUtil$AlbumInfo;,
        Lcom/vlingo/core/internal/util/MusicUtil$ArtistInfo;,
        Lcom/vlingo/core/internal/util/MusicUtil$PlaylistInfo;,
        Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;,
        Lcom/vlingo/core/internal/util/MusicUtil$MusicType;
    }
.end annotation


# static fields
.field private static final DEFAULT_MUSIC_ALBUM_ART_URI:Ljava/lang/String; = "content://media/external/audio/albumart/"

.field private static final DEFAULT_MUSIC_ALBUM_URI:Landroid/net/Uri;

.field private static final DEFAULT_MUSIC_ARTIST_URI:Landroid/net/Uri;

.field private static final DEFAULT_MUSIC_GENERAL_URI:Landroid/net/Uri;

.field private static final DEFAULT_MUSIC_PLAYLIST_URI:Landroid/net/Uri;

.field private static final DEFAULT_MUSIC_SQUARE_URL:Ljava/lang/String; = "content://com.sec.music/music_square/music_square/"

.field private static final LOCAL_MUSIC_ALBUM_URI:Landroid/net/Uri;

.field private static final LOCAL_MUSIC_ARTIST_URI:Landroid/net/Uri;

.field private static final LOCAL_MUSIC_GENERAL_URI:Landroid/net/Uri;

.field private static final LOCAL_MUSIC_PLAYLIST_URI:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/vlingo/core/internal/util/MusicUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/MusicUtil;->TAG:Ljava/lang/String;

    .line 31
    sget-object v0, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->PLAYLIST:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/MusicUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicUtil$MusicType;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/MusicUtil;->LOCAL_MUSIC_PLAYLIST_URI:Landroid/net/Uri;

    .line 32
    sget-object v0, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->TITLE:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/MusicUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicUtil$MusicType;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/MusicUtil;->LOCAL_MUSIC_GENERAL_URI:Landroid/net/Uri;

    .line 33
    sget-object v0, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->ARTIST:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/MusicUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicUtil$MusicType;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/MusicUtil;->LOCAL_MUSIC_ARTIST_URI:Landroid/net/Uri;

    .line 34
    sget-object v0, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->ALBUM:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/MusicUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicUtil$MusicType;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/MusicUtil;->LOCAL_MUSIC_ALBUM_URI:Landroid/net/Uri;

    .line 35
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_GENERAL_URI:Landroid/net/Uri;

    .line 36
    sget-object v0, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_PLAYLIST_URI:Landroid/net/Uri;

    .line 37
    sget-object v0, Landroid/provider/MediaStore$Audio$Artists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_ARTIST_URI:Landroid/net/Uri;

    .line 38
    sget-object v0, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_ALBUM_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    return-void
.end method

.method public static findMatchingAlbumList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200
    sget-object v2, Lcom/vlingo/core/internal/util/MusicUtil;->LOCAL_MUSIC_ALBUM_URI:Landroid/net/Uri;

    const-string/jumbo v3, "album"

    const-class v4, Lcom/vlingo/core/internal/util/MusicUtil$AlbumInfo;

    const-string/jumbo v5, "album_key"

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/vlingo/core/internal/util/MusicUtil;->findMusicItems(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static findMatchingArtistList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 204
    sget-object v2, Lcom/vlingo/core/internal/util/MusicUtil;->LOCAL_MUSIC_ARTIST_URI:Landroid/net/Uri;

    const-string/jumbo v3, "artist"

    const-class v4, Lcom/vlingo/core/internal/util/MusicUtil$ArtistInfo;

    const-string/jumbo v5, "artist_key"

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/vlingo/core/internal/util/MusicUtil;->findMusicItems(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static findMatchingPlaylistList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208
    sget-object v2, Lcom/vlingo/core/internal/util/MusicUtil;->LOCAL_MUSIC_PLAYLIST_URI:Landroid/net/Uri;

    const-string/jumbo v3, "name"

    const-class v4, Lcom/vlingo/core/internal/util/MusicUtil$PlaylistInfo;

    const-string/jumbo v5, "name"

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/vlingo/core/internal/util/MusicUtil;->findMusicItems(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static findMatchingTitleList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 196
    sget-object v2, Lcom/vlingo/core/internal/util/MusicUtil;->LOCAL_MUSIC_GENERAL_URI:Landroid/net/Uri;

    const-string/jumbo v3, "title"

    const-class v4, Lcom/vlingo/core/internal/util/MusicUtil$TitleInfo;

    const-string/jumbo v5, "title_key"

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/vlingo/core/internal/util/MusicUtil;->findMusicItems(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static findMusicItems(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/List;
    .locals 16
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "field"    # Ljava/lang/String;
    .param p5, "sortedBy"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 212
    .local p4, "clz":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;>;"
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 213
    .local v14, "list":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;>;"
    const/4 v7, 0x0

    .line 214
    .local v7, "cur":Landroid/database/Cursor;
    const/4 v12, -0x1

    .line 215
    .local v12, "idIndex":I
    const/4 v10, -0x1

    .line 219
    .local v10, "fieldIndex":I
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v2, p2

    move-object/from16 v6, p5

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 221
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_2

    .line 224
    :cond_0
    const/4 v14, 0x0

    .line 250
    .end local v14    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;>;"
    if-eqz v7, :cond_1

    .line 252
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 258
    :cond_1
    :goto_0
    return-object v14

    .line 253
    :catch_0
    move-exception v9

    .line 254
    .local v9, "ex":Ljava/lang/Exception;
    sget-object v1, Lcom/vlingo/core/internal/util/MusicUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "VLG_EXCEPTION "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v9}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 228
    .end local v9    # "ex":Ljava/lang/Exception;
    .restart local v14    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;>;"
    :cond_2
    const/4 v1, -0x1

    if-ne v10, v1, :cond_3

    .line 229
    :try_start_2
    const-string/jumbo v1, "_id"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 230
    move-object/from16 v0, p3

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 232
    :cond_3
    invoke-interface {v7, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 233
    .local v11, "id":I
    invoke-interface {v7, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 234
    .local v15, "value":Ljava/lang/String;
    invoke-static {v15}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 244
    :cond_4
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    if-nez v1, :cond_2

    .line 250
    if-eqz v7, :cond_1

    .line 252
    :try_start_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 253
    :catch_1
    move-exception v9

    .line 254
    .restart local v9    # "ex":Ljava/lang/Exception;
    sget-object v1, Lcom/vlingo/core/internal/util/MusicUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "VLG_EXCEPTION "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v9}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 237
    .end local v9    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_4
    invoke-virtual {v15}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 238
    invoke-virtual/range {p4 .. p4}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;

    .line 239
    .local v13, "info":Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;
    invoke-virtual {v13, v11}, Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;->setId(I)V

    .line 240
    invoke-virtual {v13, v15}, Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;->setTitle(Ljava/lang/String;)V

    .line 241
    invoke-interface {v14, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 245
    .end local v11    # "id":I
    .end local v13    # "info":Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;
    .end local v15    # "value":Ljava/lang/String;
    :catch_2
    move-exception v8

    .line 246
    .local v8, "e":Ljava/lang/Exception;
    :try_start_5
    sget-object v1, Lcom/vlingo/core/internal/util/MusicUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "VLG_EXCEPTION "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v8}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 248
    const/4 v14, 0x0

    .line 250
    .end local v14    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;>;"
    if-eqz v7, :cond_1

    .line 252
    :try_start_6
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_0

    .line 253
    :catch_3
    move-exception v9

    .line 254
    .restart local v9    # "ex":Ljava/lang/Exception;
    sget-object v1, Lcom/vlingo/core/internal/util/MusicUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "VLG_EXCEPTION "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v9}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 250
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v9    # "ex":Ljava/lang/Exception;
    .restart local v14    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;>;"
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_6

    .line 252
    :try_start_7
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    .line 255
    :cond_6
    :goto_2
    throw v1

    .line 253
    :catch_4
    move-exception v9

    .line 254
    .restart local v9    # "ex":Ljava/lang/Exception;
    sget-object v2, Lcom/vlingo/core/internal/util/MusicUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "VLG_EXCEPTION "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v9}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private static getMusicUri(Lcom/vlingo/core/internal/util/MusicUtil$MusicType;)Landroid/net/Uri;
    .locals 8
    .param p0, "key"    # Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    .prologue
    .line 44
    sget-object v5, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v5}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v2

    .line 45
    .local v2, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v3, 0x0

    .line 46
    .local v3, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    const/4 v4, 0x0

    .line 47
    .local v4, "result":Landroid/net/Uri;
    if-eqz v2, :cond_1

    .line 49
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object v3, v0

    .line 50
    if-eqz v3, :cond_0

    .line 51
    sget-object v5, Lcom/vlingo/core/internal/util/MusicUtil$1;->$SwitchMap$com$vlingo$core$internal$util$MusicUtil$MusicType:[I

    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->ordinal()I

    move-result v6

    aget v5, v5, v6
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v5, :pswitch_data_0

    .line 75
    :cond_0
    :goto_0
    if-nez v3, :cond_1

    .line 76
    sget-object v5, Lcom/vlingo/core/internal/util/MusicUtil$1;->$SwitchMap$com$vlingo$core$internal$util$MusicUtil$MusicType:[I

    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_1

    .line 95
    :cond_1
    :goto_1
    return-object v4

    .line 53
    :pswitch_0
    :try_start_1
    sget-object v5, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->PLAYLIST:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-interface {v3, v5}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v4

    .line 54
    goto :goto_0

    .line 56
    :pswitch_1
    sget-object v5, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-interface {v3, v5}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v4

    .line 57
    goto :goto_0

    .line 59
    :pswitch_2
    sget-object v5, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ARTIST:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-interface {v3, v5}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v4

    .line 60
    goto :goto_0

    .line 62
    :pswitch_3
    sget-object v5, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUM:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-interface {v3, v5}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 63
    goto :goto_0

    .line 78
    :pswitch_4
    sget-object v4, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_PLAYLIST_URI:Landroid/net/Uri;

    .line 79
    goto :goto_1

    .line 81
    :pswitch_5
    sget-object v4, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_GENERAL_URI:Landroid/net/Uri;

    .line 82
    goto :goto_1

    .line 84
    :pswitch_6
    sget-object v4, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_ARTIST_URI:Landroid/net/Uri;

    .line 85
    goto :goto_1

    .line 87
    :pswitch_7
    sget-object v4, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_ALBUM_URI:Landroid/net/Uri;

    .line 88
    goto :goto_1

    .line 68
    :catch_0
    move-exception v1

    .line 69
    .local v1, "e":Ljava/lang/InstantiationException;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/InstantiationException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 75
    if-nez v3, :cond_1

    .line 76
    sget-object v5, Lcom/vlingo/core/internal/util/MusicUtil$1;->$SwitchMap$com$vlingo$core$internal$util$MusicUtil$MusicType:[I

    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_2

    goto :goto_1

    .line 78
    :pswitch_8
    sget-object v4, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_PLAYLIST_URI:Landroid/net/Uri;

    .line 79
    goto :goto_1

    .line 81
    :pswitch_9
    sget-object v4, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_GENERAL_URI:Landroid/net/Uri;

    .line 82
    goto :goto_1

    .line 84
    :pswitch_a
    sget-object v4, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_ARTIST_URI:Landroid/net/Uri;

    .line 85
    goto :goto_1

    .line 87
    :pswitch_b
    sget-object v4, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_ALBUM_URI:Landroid/net/Uri;

    .line 88
    goto :goto_1

    .line 70
    .end local v1    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v1

    .line 71
    .local v1, "e":Ljava/lang/IllegalAccessException;
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 75
    if-nez v3, :cond_1

    .line 76
    sget-object v5, Lcom/vlingo/core/internal/util/MusicUtil$1;->$SwitchMap$com$vlingo$core$internal$util$MusicUtil$MusicType:[I

    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_3

    goto :goto_1

    .line 78
    :pswitch_c
    sget-object v4, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_PLAYLIST_URI:Landroid/net/Uri;

    .line 79
    goto :goto_1

    .line 81
    :pswitch_d
    sget-object v4, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_GENERAL_URI:Landroid/net/Uri;

    .line 82
    goto :goto_1

    .line 84
    :pswitch_e
    sget-object v4, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_ARTIST_URI:Landroid/net/Uri;

    .line 85
    goto :goto_1

    .line 87
    :pswitch_f
    sget-object v4, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_ALBUM_URI:Landroid/net/Uri;

    .line 88
    goto :goto_1

    .line 72
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 73
    .local v1, "e":Ljava/lang/NullPointerException;
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 75
    if-nez v3, :cond_1

    .line 76
    sget-object v5, Lcom/vlingo/core/internal/util/MusicUtil$1;->$SwitchMap$com$vlingo$core$internal$util$MusicUtil$MusicType:[I

    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_4

    goto :goto_1

    .line 78
    :pswitch_10
    sget-object v4, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_PLAYLIST_URI:Landroid/net/Uri;

    .line 79
    goto :goto_1

    .line 81
    :pswitch_11
    sget-object v4, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_GENERAL_URI:Landroid/net/Uri;

    .line 82
    goto :goto_1

    .line 84
    :pswitch_12
    sget-object v4, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_ARTIST_URI:Landroid/net/Uri;

    .line 85
    goto :goto_1

    .line 87
    :pswitch_13
    sget-object v4, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_ALBUM_URI:Landroid/net/Uri;

    .line 88
    goto/16 :goto_1

    .line 75
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v5

    if-nez v3, :cond_2

    .line 76
    sget-object v6, Lcom/vlingo/core/internal/util/MusicUtil$1;->$SwitchMap$com$vlingo$core$internal$util$MusicUtil$MusicType:[I

    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_5

    .line 90
    :cond_2
    :goto_2
    throw v5

    .line 78
    :pswitch_14
    sget-object v4, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_PLAYLIST_URI:Landroid/net/Uri;

    .line 79
    goto :goto_2

    .line 81
    :pswitch_15
    sget-object v4, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_GENERAL_URI:Landroid/net/Uri;

    .line 82
    goto :goto_2

    .line 84
    :pswitch_16
    sget-object v4, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_ARTIST_URI:Landroid/net/Uri;

    .line 85
    goto :goto_2

    .line 87
    :pswitch_17
    sget-object v4, Lcom/vlingo/core/internal/util/MusicUtil;->DEFAULT_MUSIC_ALBUM_URI:Landroid/net/Uri;

    .line 88
    goto :goto_2

    .line 51
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 76
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x1
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch
.end method

.method public static getPlayCount(Landroid/content/Context;Ljava/util/List;)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 262
    .local p1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/4 v6, 0x0

    .line 264
    .local v6, "count":I
    const/4 v7, 0x0

    .line 267
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/vlingo/core/internal/util/MusicUtil;->LOCAL_MUSIC_GENERAL_URI:Landroid/net/Uri;

    .line 268
    .local v1, "musicUri":Landroid/net/Uri;
    if-eqz v1, :cond_1

    .line 269
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const-string/jumbo v3, "%s in (%s)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string/jumbo v9, "_id"

    aput-object v9, v4, v5

    const/4 v5, 0x1

    const-string/jumbo v9, ", "

    invoke-static {p1, v9}, Lcom/vlingo/core/internal/util/StringUtils;->join(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 275
    const-string/jumbo v0, "most_played"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 276
    .local v8, "playCountColumnIndex":I
    if-ltz v8, :cond_1

    .line 277
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 279
    :cond_0
    invoke-interface {v7, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    add-int/2addr v6, v0

    .line 280
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 291
    .end local v8    # "playCountColumnIndex":I
    :cond_1
    if-eqz v7, :cond_2

    .line 292
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 296
    :cond_2
    return v6

    .line 291
    .end local v1    # "musicUri":Landroid/net/Uri;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_3

    .line 292
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method
