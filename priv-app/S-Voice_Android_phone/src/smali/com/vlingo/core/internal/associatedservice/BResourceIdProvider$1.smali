.class synthetic Lcom/vlingo/core/internal/associatedservice/BResourceIdProvider$1;
.super Ljava/lang/Object;
.source "BResourceIdProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/associatedservice/BResourceIdProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$vlingo$core$internal$ResourceIdProvider$array:[I

.field static final synthetic $SwitchMap$com$vlingo$core$internal$ResourceIdProvider$raw:[I

.field static final synthetic $SwitchMap$com$vlingo$core$internal$ResourceIdProvider$string:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 78
    invoke-static {}, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->values()[Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/vlingo/core/internal/associatedservice/BResourceIdProvider$1;->$SwitchMap$com$vlingo$core$internal$ResourceIdProvider$raw:[I

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/BResourceIdProvider$1;->$SwitchMap$com$vlingo$core$internal$ResourceIdProvider$raw:[I

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_start_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_7

    :goto_0
    :try_start_1
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/BResourceIdProvider$1;->$SwitchMap$com$vlingo$core$internal$ResourceIdProvider$raw:[I

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_start_tone_bt:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_6

    :goto_1
    :try_start_2
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/BResourceIdProvider$1;->$SwitchMap$com$vlingo$core$internal$ResourceIdProvider$raw:[I

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_start_tone_drv:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_5

    :goto_2
    :try_start_3
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/BResourceIdProvider$1;->$SwitchMap$com$vlingo$core$internal$ResourceIdProvider$raw:[I

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4

    :goto_3
    :try_start_4
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/BResourceIdProvider$1;->$SwitchMap$com$vlingo$core$internal$ResourceIdProvider$raw:[I

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone_bt:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3

    :goto_4
    :try_start_5
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/BResourceIdProvider$1;->$SwitchMap$com$vlingo$core$internal$ResourceIdProvider$raw:[I

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone_drv:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_2

    .line 67
    :goto_5
    invoke-static {}, Lcom/vlingo/core/internal/ResourceIdProvider$array;->values()[Lcom/vlingo/core/internal/ResourceIdProvider$array;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/vlingo/core/internal/associatedservice/BResourceIdProvider$1;->$SwitchMap$com$vlingo$core$internal$ResourceIdProvider$array:[I

    :try_start_6
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/BResourceIdProvider$1;->$SwitchMap$com$vlingo$core$internal$ResourceIdProvider$array:[I

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_languages_iso:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/ResourceIdProvider$array;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1

    .line 45
    :goto_6
    invoke-static {}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->values()[Lcom/vlingo/core/internal/ResourceIdProvider$string;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/vlingo/core/internal/associatedservice/BResourceIdProvider$1;->$SwitchMap$com$vlingo$core$internal$ResourceIdProvider$string:[I

    :try_start_7
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/BResourceIdProvider$1;->$SwitchMap$com$vlingo$core$internal$ResourceIdProvider$string:[I

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_local_required_engine_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_0

    :goto_7
    return-void

    :catch_0
    move-exception v0

    goto :goto_7

    .line 67
    :catch_1
    move-exception v0

    goto :goto_6

    .line 78
    :catch_2
    move-exception v0

    goto :goto_5

    :catch_3
    move-exception v0

    goto :goto_4

    :catch_4
    move-exception v0

    goto :goto_3

    :catch_5
    move-exception v0

    goto :goto_2

    :catch_6
    move-exception v0

    goto :goto_1

    :catch_7
    move-exception v0

    goto :goto_0
.end method
