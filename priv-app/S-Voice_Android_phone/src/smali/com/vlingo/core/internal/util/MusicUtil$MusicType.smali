.class public final enum Lcom/vlingo/core/internal/util/MusicUtil$MusicType;
.super Ljava/lang/Enum;
.source "MusicUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/MusicUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MusicType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/util/MusicUtil$MusicType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

.field public static final enum ALBUM:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

.field public static final enum ARTIST:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

.field public static final enum PLAYLIST:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

.field public static final enum TITLE:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    const-string/jumbo v1, "PLAYLIST"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->PLAYLIST:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    new-instance v0, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    const-string/jumbo v1, "TITLE"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->TITLE:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    new-instance v0, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    const-string/jumbo v1, "ARTIST"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->ARTIST:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    new-instance v0, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    const-string/jumbo v1, "ALBUM"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->ALBUM:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    sget-object v1, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->PLAYLIST:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->TITLE:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->ARTIST:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->ALBUM:Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->$VALUES:[Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/util/MusicUtil$MusicType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 30
    const-class v0, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/util/MusicUtil$MusicType;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->$VALUES:[Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/util/MusicUtil$MusicType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/util/MusicUtil$MusicType;

    return-object v0
.end method
