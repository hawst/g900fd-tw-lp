.class public abstract Lcom/vlingo/core/internal/settings/util/BooleanSetting;
.super Lcom/vlingo/core/internal/settings/util/Setting;
.source "BooleanSetting.java"


# static fields
.field public static LABELS_DISABLED_ENABLED:[Ljava/lang/String;

.field public static LABELS_OFF_ON:[Ljava/lang/String;


# instance fields
.field protected final labels:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 10
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "Off"

    aput-object v1, v0, v2

    const-string/jumbo v1, "On"

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/core/internal/settings/util/BooleanSetting;->LABELS_OFF_ON:[Ljava/lang/String;

    .line 11
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "Disabled"

    aput-object v1, v0, v2

    const-string/jumbo v1, "Enabled"

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/core/internal/settings/util/BooleanSetting;->LABELS_DISABLED_ENABLED:[Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Z
    .param p3, "description"    # Ljava/lang/String;

    .prologue
    .line 16
    sget-object v0, Lcom/vlingo/core/internal/settings/util/BooleanSetting;->LABELS_OFF_ON:[Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/vlingo/core/internal/settings/util/BooleanSetting;-><init>(Ljava/lang/String;ZLjava/lang/String;[Ljava/lang/String;)V

    .line 17
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ZLjava/lang/String;[Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Z
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "labels"    # [Ljava/lang/String;

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-static {p2}, Lcom/vlingo/core/internal/settings/util/BooleanSetting;->getStringValue(Z)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1, p3}, Lcom/vlingo/core/internal/settings/util/Setting;-><init>(Ljava/lang/String;ILjava/lang/Object;Ljava/lang/String;)V

    .line 21
    iput-object p4, p0, Lcom/vlingo/core/internal/settings/util/BooleanSetting;->labels:[Ljava/lang/String;

    .line 22
    return-void
.end method

.method protected static getStringValue(Z)Ljava/lang/String;
    .locals 1
    .param p0, "b"    # Z

    .prologue
    .line 40
    if-eqz p0, :cond_0

    .line 41
    const-string/jumbo v0, "true"

    .line 43
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "false"

    goto :goto_0
.end method

.method public static getValueFromString(Ljava/lang/String;)Z
    .locals 1
    .param p0, "v"    # Ljava/lang/String;

    .prologue
    .line 33
    if-eqz p0, :cond_0

    .line 34
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 36
    :cond_0
    const-string/jumbo v0, "true"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getValue()Z
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/core/internal/settings/util/BooleanSetting;->value:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/settings/util/BooleanSetting;->getValueFromString(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setValue(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 29
    invoke-static {p1}, Lcom/vlingo/core/internal/settings/util/BooleanSetting;->getStringValue(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/settings/util/BooleanSetting;->setValueInternal(Ljava/lang/Object;)V

    .line 30
    return-void
.end method
