.class Lcom/vlingo/core/internal/dialogmanager/DialogFlow$TaskQueueListenerImpl;
.super Ljava/lang/Object;
.source "DialogFlow.java"

# interfaces
.implements Lcom/vlingo/core/internal/util/TaskQueue$TaskQueueListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TaskQueueListenerImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V
    .locals 0

    .prologue
    .line 1637
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$TaskQueueListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Lcom/vlingo/core/internal/dialogmanager/DialogFlow$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow$1;

    .prologue
    .line 1637
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$TaskQueueListenerImpl;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V

    return-void
.end method

.method private handleTaskStartingForRegulators(Ljava/util/Set;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)V
    .locals 4
    .param p2, "eventType"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;
    .param p3, "taskToResume"    # Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;",
            "Lcom/vlingo/core/internal/dialogmanager/ResumeControl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1667
    .local p1, "regulators":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;>;"
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/MultipleResumeControl;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    invoke-direct {v1, p3, v3}, Lcom/vlingo/core/internal/dialogmanager/MultipleResumeControl;-><init>(Lcom/vlingo/core/internal/dialogmanager/ResumeControl;I)V

    .line 1668
    .local v1, "multipleResumeControl":Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;

    .line 1669
    .local v2, "regulator":Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;
    invoke-interface {v2, p2, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;->onTaskWaitingToStart(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)V

    goto :goto_0

    .line 1671
    .end local v2    # "regulator":Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;
    :cond_0
    return-void
.end method


# virtual methods
.method public onQueueCancelled()V
    .locals 2

    .prologue
    .line 1682
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "TaskQueueListenerImpl.onQueueCancelled()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1683
    return-void
.end method

.method public onQueueDone()V
    .locals 2

    .prologue
    .line 1675
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "TaskQueueListenerImpl.onQueueDone()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1676
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "TaskQueueListenerImpl.onQueueDone()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1677
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$TaskQueueListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    const/4 v1, 0x0

    # invokes: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->handleIdle(Z)V
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2600(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;Z)V

    .line 1678
    return-void
.end method

.method public onTaskStarting(Lcom/vlingo/core/internal/util/TaskQueue$Task;)V
    .locals 6
    .param p1, "task"    # Lcom/vlingo/core/internal/util/TaskQueue$Task;

    .prologue
    .line 1640
    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$1900()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "TaskQueueListenerImpl.onTaskStarting() task="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1642
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$TaskQueueListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->notifyDialogBusy()V
    invoke-static {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->access$2500(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V

    .line 1643
    instance-of v3, p1, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    if-eqz v3, :cond_1

    .line 1644
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$TaskQueueListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->RECOGNITION_START:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getTaskRegulators(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;)Ljava/util/Set;

    move-result-object v2

    .line 1645
    .local v2, "regulators":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    move-object v0, p1

    .line 1646
    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;

    .line 1647
    .local v0, "dmServerTask":Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->isPausable()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->isPerformingReco()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1648
    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;->pause()V

    .line 1649
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->RECOGNITION_START:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    invoke-direct {p0, v2, v3, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$TaskQueueListenerImpl;->handleTaskStartingForRegulators(Ljava/util/Set;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)V

    .line 1664
    .end local v0    # "dmServerTask":Lcom/vlingo/core/internal/dialogmanager/tasks/DMServerTask;
    .end local v2    # "regulators":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;>;"
    :cond_0
    :goto_0
    return-void

    .line 1652
    :cond_1
    instance-of v3, p1, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    if-eqz v3, :cond_2

    .line 1653
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$TaskQueueListenerImpl;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->SYSTEM_TEXT_TTS:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getTaskRegulators(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;)Ljava/util/Set;

    move-result-object v2

    .line 1654
    .restart local v2    # "regulators":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    move-object v1, p1

    .line 1655
    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;

    .line 1656
    .local v1, "playTtsTask":Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->isPausable()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->isSystemTts()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1657
    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;->pause()V

    .line 1658
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->SYSTEM_TEXT_TTS:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    invoke-direct {p0, v2, v3, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$TaskQueueListenerImpl;->handleTaskStartingForRegulators(Ljava/util/Set;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)V

    goto :goto_0

    .line 1661
    .end local v1    # "playTtsTask":Lcom/vlingo/core/internal/dialogmanager/tasks/PlayTTSTask;
    .end local v2    # "regulators":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;>;"
    :cond_2
    instance-of v3, p1, Lcom/vlingo/core/internal/dialogmanager/tasks/PlayMediaTask;

    if-eqz v3, :cond_0

    goto :goto_0
.end method
