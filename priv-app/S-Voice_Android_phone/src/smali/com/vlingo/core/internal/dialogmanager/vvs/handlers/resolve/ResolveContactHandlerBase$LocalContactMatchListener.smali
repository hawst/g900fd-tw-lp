.class Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$LocalContactMatchListener;
.super Ljava/lang/Object;
.source "ResolveContactHandlerBase.java"

# interfaces
.implements Lcom/vlingo/core/internal/contacts/ContactMatchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocalContactMatchListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$1;

    .prologue
    .line 175
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$LocalContactMatchListener;-><init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)V

    return-void
.end method


# virtual methods
.method public onAutoAction(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 0
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 177
    return-void
.end method

.method public onContactMatchResultsUpdated(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 180
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    return-void
.end method

.method public onContactMatchingFailed()V
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->access$900(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 254
    return-void
.end method

.method public onContactMatchingFinished(Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "newContacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/4 v6, 0x1

    .line 185
    if-eqz p1, :cond_a

    .line 186
    const/4 v5, 0x0

    .line 189
    .local v5, "contactOffset":I
    :try_start_0
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->access$100(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    move-object v0, v3

    check-cast v0, Ljava/util/List;

    move-object v8, v0

    .line 190
    .local v8, "allContacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    if-nez v8, :cond_5

    .line 191
    new-instance v8, Ljava/util/ArrayList;

    .end local v8    # "allContacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v8, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 205
    .restart local v8    # "allContacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v6, :cond_6

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mCapability:Lcom/vlingo/core/internal/contacts/ContactType;
    invoke-static {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->access$200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactType;->EMAIL:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/contacts/ContactType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getEmailData()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eq v3, v6, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mCapability:Lcom/vlingo/core/internal/contacts/ContactType;
    invoke-static {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->access$200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactType;->SMS:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/contacts/ContactType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x0

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v6, :cond_6

    .line 208
    :cond_1
    const/4 v9, 0x0

    .line 209
    .local v9, "exist":Z
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 210
    .local v12, "oldContact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    const/4 v3, 0x0

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLookupKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getLookupKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 211
    const/4 v9, 0x1

    .line 215
    .end local v12    # "oldContact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    :cond_3
    if-nez v9, :cond_4

    .line 216
    const/4 v3, 0x0

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v8, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    .end local v9    # "exist":Z
    .end local v11    # "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 222
    .local v2, "storeContacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mLimit:I
    invoke-static {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->access$300(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)I

    move-result v4

    if-le v3, v4, :cond_7

    .line 223
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_2
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mLimit:I
    invoke-static {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->access$300(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)I

    move-result v3

    if-ge v10, v3, :cond_8

    .line 224
    invoke-interface {v8, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 193
    .end local v2    # "storeContacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v10    # "i":I
    :cond_5
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v5

    goto/16 :goto_0

    .line 219
    :cond_6
    invoke-interface {v8, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 244
    .end local v8    # "allContacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->access$800(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    throw v3

    .line 227
    .restart local v2    # "storeContacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .restart local v8    # "allContacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_7
    :try_start_1
    invoke-interface {v2, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 229
    :cond_8
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mCapability:Lcom/vlingo/core/internal/contacts/ContactType;
    invoke-static {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->access$200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)Lcom/vlingo/core/internal/contacts/ContactType;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->mDetailed:Z
    invoke-static {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->access$400(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)Z

    move-result v4

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    iget-object v7, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getPhoneTypes()[I

    move-result-object v7

    invoke-direct/range {v1 .. v7}, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;-><init>(Ljava/util/List;Lcom/vlingo/core/internal/contacts/ContactType;ZII[I)V

    .line 231
    .local v1, "event":Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;->getVLDialogEvent()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;

    move-result-object v3

    if-eqz v3, :cond_9

    .line 234
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->access$600(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    invoke-static {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->access$500(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    move-result-object v4

    invoke-interface {v3, v1, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 239
    :cond_9
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->access$700(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v3, v4, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 244
    .end local v1    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ContactResolvedEvent;
    .end local v2    # "storeContacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v5    # "contactOffset":I
    .end local v8    # "allContacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_a
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;->access$800(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveContactHandlerBase;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 246
    return-void
.end method
