.class public Lcom/vlingo/core/internal/dialogmanager/CoreTester;
.super Ljava/lang/Object;
.source "CoreTester.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/recognizer/network/XMLResponseListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayThread;,
        Lcom/vlingo/core/internal/dialogmanager/CoreTester$OnFinishedCallback;,
        Lcom/vlingo/core/internal/dialogmanager/CoreTester$ExtraTypes;,
        Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayTypes;
    }
.end annotation


# static fields
.field private static final LOG:Lcom/vlingo/core/internal/logging/Logger;

.field private static final SINGLE_LINE_STRING_VAL:Ljava/lang/String; = "ONE_LINE"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mDummyRecognizer:Lcom/vlingo/core/internal/dialogmanager/VLDummyRecognizer;

.field private mFlow:Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

.field mInFilePath:Ljava/lang/String;

.field private mLastNanoTime:J

.field private mOutFilePath:Ljava/lang/String;

.field private mReplayThread:Lcom/vlingo/core/internal/dialogmanager/CoreTester$ReplayThread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/CoreTester;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/CoreTester;->TAG:Ljava/lang/String;

    .line 62
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/CoreTester;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/CoreTester;->LOG:Lcom/vlingo/core/internal/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/vlingo/core/internal/dialogmanager/DialogFlow;)V
    .locals 3
    .param p1, "dialogFlow"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    .prologue
    const/4 v2, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/core/internal/dialogmanager/CoreTester;->mLastNanoTime:J

    .line 65
    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/CoreTester;->mOutFilePath:Ljava/lang/String;

    .line 66
    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/CoreTester;->mInFilePath:Ljava/lang/String;

    .line 73
    iput-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/CoreTester;->mDummyRecognizer:Lcom/vlingo/core/internal/dialogmanager/VLDummyRecognizer;

    .line 80
    return-void
.end method

.method private cleanUpRecognizer()V
    .locals 0

    .prologue
    .line 150
    return-void
.end method

.method private getWriter()Ljava/io/OutputStreamWriter;
    .locals 1

    .prologue
    .line 302
    const/4 v0, 0x0

    return-object v0
.end method

.method private recordDelay()V
    .locals 0

    .prologue
    .line 179
    return-void
.end method

.method private recordResponseString(Ljava/lang/String;)V
    .locals 0
    .param p1, "responseXML"    # Ljava/lang/String;

    .prologue
    .line 203
    return-void
.end method


# virtual methods
.method public notifyXMLResponse(Ljava/lang/String;)V
    .locals 0
    .param p1, "responseXML"    # Ljava/lang/String;

    .prologue
    .line 211
    return-void
.end method

.method recordIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "unused"    # Ljava/lang/Object;

    .prologue
    .line 278
    return-void
.end method

.method recordWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)V
    .locals 0
    .param p1, "key"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .prologue
    .line 327
    return-void
.end method

.method public startRecording(Ljava/lang/String;)V
    .locals 0
    .param p1, "outFilePath"    # Ljava/lang/String;

    .prologue
    .line 102
    return-void
.end method

.method public startReplay(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/CoreTester$OnFinishedCallback;)V
    .locals 0
    .param p1, "inFilePath"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/vlingo/core/internal/dialogmanager/CoreTester$OnFinishedCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 132
    return-void
.end method

.method public stopRecording()V
    .locals 0

    .prologue
    .line 109
    return-void
.end method

.method public stopReplay()V
    .locals 0

    .prologue
    .line 141
    return-void
.end method
