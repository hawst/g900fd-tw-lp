.class public Lcom/vlingo/core/internal/xml/XmlParser;
.super Ljava/lang/Object;
.source "XmlParser.java"


# static fields
.field private static final MAX_ESCAPE_LEN:I = 0x6

.field private static final STATE_ADVANCE:I = 0x4

.field private static final STATE_ADVANCE_COMMENTS:I = 0x6

.field private static final STATE_BEGIN:I = 0x0

.field private static final STATE_DONE:I = 0x5

.field private static final STATE_IN_CDATA:I = 0x3

.field private static final STATE_IN_ELEMENT:I = 0x1

.field private static final XML_ESCAPES:Lcom/vlingo/core/internal/util/ToIntHashtable;


# instance fields
.field private attributeNameEnd:I

.field private attributeNameStart:I

.field private attributeValueEnd:I

.field private attributeValueStart:I

.field private attributes:Lcom/vlingo/core/internal/xml/XmlAttributes;

.field protected cDataEnd:I

.field protected cDataStart:I

.field cStartElementEnd:I

.field private cachedAttributes:Lcom/vlingo/core/internal/xml/XmlAttributes;

.field private cachedCData:[C

.field private cachedElementType:I

.field private checkCDataSpacing:Z

.field private checkEscapes:Z

.field private checkForEscapes_AttributeValues:Z

.field private checkForEscapes_CData:Z

.field private currentElementIsBegin:Z

.field private currentElementIsEnd:Z

.field private currentState:I

.field protected elementEnd:I

.field protected elementStart:I

.field protected endIndex:I

.field private handler:Lcom/vlingo/core/internal/xml/XmlHandler;

.field protected index:I

.field private xml:[C

.field private xmlAttributes:Lcom/vlingo/core/internal/util/ToIntHashtable;

.field private xmlElements:Lcom/vlingo/core/internal/util/ToIntHashtable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 68
    invoke-static {}, Lcom/vlingo/core/internal/util/ToIntHashtableFactory;->createNewHashtable()Lcom/vlingo/core/internal/util/ToIntHashtable;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/xml/XmlParser;->XML_ESCAPES:Lcom/vlingo/core/internal/util/ToIntHashtable;

    .line 70
    sget-object v0, Lcom/vlingo/core/internal/xml/XmlParser;->XML_ESCAPES:Lcom/vlingo/core/internal/util/ToIntHashtable;

    const-string/jumbo v1, "quot"

    const/16 v2, 0x22

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/util/ToIntHashtable;->put(Ljava/lang/Object;I)I

    .line 71
    sget-object v0, Lcom/vlingo/core/internal/xml/XmlParser;->XML_ESCAPES:Lcom/vlingo/core/internal/util/ToIntHashtable;

    const-string/jumbo v1, "amp"

    const/16 v2, 0x26

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/util/ToIntHashtable;->put(Ljava/lang/Object;I)I

    .line 72
    sget-object v0, Lcom/vlingo/core/internal/xml/XmlParser;->XML_ESCAPES:Lcom/vlingo/core/internal/util/ToIntHashtable;

    const-string/jumbo v1, "apos"

    const/16 v2, 0x27

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/util/ToIntHashtable;->put(Ljava/lang/Object;I)I

    .line 73
    sget-object v0, Lcom/vlingo/core/internal/xml/XmlParser;->XML_ESCAPES:Lcom/vlingo/core/internal/util/ToIntHashtable;

    const-string/jumbo v1, "lt"

    const/16 v2, 0x3c

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/util/ToIntHashtable;->put(Ljava/lang/Object;I)I

    .line 74
    sget-object v0, Lcom/vlingo/core/internal/xml/XmlParser;->XML_ESCAPES:Lcom/vlingo/core/internal/util/ToIntHashtable;

    const-string/jumbo v1, "gt"

    const/16 v2, 0x3e

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/util/ToIntHashtable;->put(Ljava/lang/Object;I)I

    .line 78
    sget-object v0, Lcom/vlingo/core/internal/xml/XmlParser;->XML_ESCAPES:Lcom/vlingo/core/internal/util/ToIntHashtable;

    const-string/jumbo v1, "nbsp"

    const/16 v2, 0x20

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/util/ToIntHashtable;->put(Ljava/lang/Object;I)I

    .line 80
    sget-object v0, Lcom/vlingo/core/internal/xml/XmlParser;->XML_ESCAPES:Lcom/vlingo/core/internal/util/ToIntHashtable;

    const-string/jumbo v1, "excl"

    const/16 v2, 0x21

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/util/ToIntHashtable;->put(Ljava/lang/Object;I)I

    .line 81
    sget-object v0, Lcom/vlingo/core/internal/xml/XmlParser;->XML_ESCAPES:Lcom/vlingo/core/internal/util/ToIntHashtable;

    const-string/jumbo v1, "sol"

    const/16 v2, 0x2f

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/util/ToIntHashtable;->put(Ljava/lang/Object;I)I

    .line 82
    sget-object v0, Lcom/vlingo/core/internal/xml/XmlParser;->XML_ESCAPES:Lcom/vlingo/core/internal/util/ToIntHashtable;

    const-string/jumbo v1, "equals"

    const/16 v2, 0x3d

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/util/ToIntHashtable;->put(Ljava/lang/Object;I)I

    .line 83
    sget-object v0, Lcom/vlingo/core/internal/xml/XmlParser;->XML_ESCAPES:Lcom/vlingo/core/internal/util/ToIntHashtable;

    const-string/jumbo v1, "lsqb"

    const/16 v2, 0x5b

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/util/ToIntHashtable;->put(Ljava/lang/Object;I)I

    .line 84
    sget-object v0, Lcom/vlingo/core/internal/xml/XmlParser;->XML_ESCAPES:Lcom/vlingo/core/internal/util/ToIntHashtable;

    const-string/jumbo v1, "rsqb"

    const/16 v2, 0x5d

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/util/ToIntHashtable;->put(Ljava/lang/Object;I)I

    .line 86
    sget-object v0, Lcom/vlingo/core/internal/xml/XmlParser;->XML_ESCAPES:Lcom/vlingo/core/internal/util/ToIntHashtable;

    const-string/jumbo v1, "trade"

    const/16 v2, 0x2122

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/util/ToIntHashtable;->put(Ljava/lang/Object;I)I

    .line 87
    return-void
.end method

.method public constructor <init>([CIILcom/vlingo/core/internal/xml/XmlHandler;Lcom/vlingo/core/internal/util/ToIntHashtable;Lcom/vlingo/core/internal/util/ToIntHashtable;ZZ)V
    .locals 3
    .param p1, "xml"    # [C
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .param p4, "handler"    # Lcom/vlingo/core/internal/xml/XmlHandler;
    .param p5, "xmlElements"    # Lcom/vlingo/core/internal/util/ToIntHashtable;
    .param p6, "xmlAttributes"    # Lcom/vlingo/core/internal/util/ToIntHashtable;
    .param p7, "checkEscapes"    # Z
    .param p8, "checkCDataSpacing"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v1, p0, Lcom/vlingo/core/internal/xml/XmlParser;->handler:Lcom/vlingo/core/internal/xml/XmlHandler;

    .line 52
    iput-object v1, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xmlElements:Lcom/vlingo/core/internal/util/ToIntHashtable;

    .line 53
    iput-object v1, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xmlAttributes:Lcom/vlingo/core/internal/util/ToIntHashtable;

    .line 55
    iput-object v1, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributes:Lcom/vlingo/core/internal/xml/XmlAttributes;

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkEscapes:Z

    .line 61
    iput-boolean v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkForEscapes_AttributeValues:Z

    .line 62
    iput-boolean v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkForEscapes_CData:Z

    .line 63
    iput-boolean v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkCDataSpacing:Z

    .line 93
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cachedElementType:I

    .line 94
    iput-object v1, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cachedCData:[C

    .line 95
    iput-object v1, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cachedAttributes:Lcom/vlingo/core/internal/xml/XmlAttributes;

    .line 499
    iput v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cStartElementEnd:I

    .line 99
    iput-object p1, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xml:[C

    .line 100
    iput p2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    .line 101
    iget v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    add-int/2addr v0, p3

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->endIndex:I

    .line 102
    iput-object p4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->handler:Lcom/vlingo/core/internal/xml/XmlHandler;

    .line 103
    iput-object p5, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xmlElements:Lcom/vlingo/core/internal/util/ToIntHashtable;

    .line 104
    iput-boolean p7, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkEscapes:Z

    .line 105
    iput-boolean p8, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkCDataSpacing:Z

    .line 109
    iput v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->currentState:I

    .line 111
    if-nez p6, :cond_0

    .line 112
    invoke-static {}, Lcom/vlingo/core/internal/util/ToIntHashtableFactory;->createNewHashtable()Lcom/vlingo/core/internal/util/ToIntHashtable;

    move-result-object p6

    .line 113
    :cond_0
    iput-object p6, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xmlAttributes:Lcom/vlingo/core/internal/util/ToIntHashtable;

    .line 117
    return-void
.end method

.method private static accountForEscapes(Ljava/util/Vector;[CII)[C
    .locals 10
    .param p0, "escapes"    # Ljava/util/Vector;
    .param p1, "data"    # [C
    .param p2, "relOffset"    # I
    .param p3, "len"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 583
    const/4 v3, 0x0

    .line 588
    .local v3, "newArray":[C
    invoke-static {p0, p3}, Lcom/vlingo/core/internal/xml/XmlParser;->getEscapedArrayLength(Ljava/util/Vector;I)I

    move-result v5

    .line 589
    .local v5, "newArrayLength":I
    new-array v3, v5, [C

    .line 590
    const/4 v4, 0x0

    .line 592
    .local v4, "newArrayIndex":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v6

    if-ge v2, v6, :cond_0

    .line 593
    invoke-virtual {p0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [I

    move-object v0, v6

    check-cast v0, [I

    .line 597
    .local v0, "escape":[I
    aget v6, v0, v8

    sub-int/2addr v6, p2

    invoke-static {p1, p2, v3, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 598
    aget v6, v0, v8

    sub-int/2addr v6, p2

    add-int/2addr v4, v6

    .line 601
    aget v6, v0, v8

    aget v7, v0, v9

    invoke-static {p1, v6, v7}, Lcom/vlingo/core/internal/xml/XmlParser;->getEscapedByte([CII)C

    move-result v1

    .line 606
    .local v1, "escapedByte":C
    aget v6, v0, v8

    aget v7, v0, v9

    add-int/2addr v6, v7

    add-int/lit8 p2, v6, -0x1

    .line 607
    aput-char v1, p1, p2

    .line 592
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 611
    .end local v0    # "escape":[I
    .end local v1    # "escapedByte":C
    :cond_0
    if-ge v4, v5, :cond_1

    .line 613
    sub-int v6, v5, v4

    invoke-static {p1, p2, v3, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 616
    :cond_1
    return-object v3
.end method

.method private addAttribute()V
    .locals 7

    .prologue
    .line 279
    iget v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeNameStart:I

    if-ltz v4, :cond_0

    iget v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeNameEnd:I

    if-gez v4, :cond_1

    .line 306
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    iget v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeNameEnd:I

    iget v5, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeNameStart:I

    sub-int v2, v4, v5

    .line 284
    .local v2, "nameLen":I
    iget v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeValueEnd:I

    iget v5, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeValueStart:I

    sub-int v3, v4, v5

    .line 288
    .local v3, "valueLen":I
    iget v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeValueStart:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_2

    .line 290
    const/4 v3, 0x0

    .line 291
    iput v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeValueStart:I

    .line 292
    iget v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeValueStart:I

    iput v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeValueEnd:I

    .line 295
    :cond_2
    iget-object v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributes:Lcom/vlingo/core/internal/xml/XmlAttributes;

    if-nez v4, :cond_3

    .line 296
    new-instance v4, Lcom/vlingo/core/internal/xml/XmlAttributes;

    iget-object v5, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xmlAttributes:Lcom/vlingo/core/internal/util/ToIntHashtable;

    invoke-direct {v4, v5}, Lcom/vlingo/core/internal/xml/XmlAttributes;-><init>(Lcom/vlingo/core/internal/util/ToIntHashtable;)V

    iput-object v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributes:Lcom/vlingo/core/internal/xml/XmlAttributes;

    .line 298
    :cond_3
    iget-object v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributes:Lcom/vlingo/core/internal/xml/XmlAttributes;

    iget-object v5, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xml:[C

    iget v6, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeNameStart:I

    invoke-virtual {v4, v5, v6, v2}, Lcom/vlingo/core/internal/xml/XmlAttributes;->getAttributeType([CII)B

    move-result v0

    .line 299
    .local v0, "attributeType":B
    iget-object v5, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xml:[C

    iget v6, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeValueStart:I

    iget-boolean v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkEscapes:Z

    if-nez v4, :cond_4

    iget-boolean v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkForEscapes_AttributeValues:Z

    if-eqz v4, :cond_5

    :cond_4
    const/4 v4, 0x1

    :goto_1
    invoke-static {v5, v6, v3, v4}, Lcom/vlingo/core/internal/xml/XmlParser;->createString([CIIZ)Ljava/lang/String;

    move-result-object v1

    .line 305
    .local v1, "attributeValue":Ljava/lang/String;
    iget-object v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributes:Lcom/vlingo/core/internal/xml/XmlAttributes;

    invoke-virtual {v4, v0, v1}, Lcom/vlingo/core/internal/xml/XmlAttributes;->add(BLjava/lang/String;)V

    goto :goto_0

    .line 299
    .end local v1    # "attributeValue":Ljava/lang/String;
    :cond_5
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private static addEscape(Ljava/util/Vector;II)Ljava/util/Vector;
    .locals 2
    .param p0, "escapes"    # Ljava/util/Vector;
    .param p1, "index"    # I
    .param p2, "length"    # I

    .prologue
    .line 635
    const/4 v1, 0x2

    new-array v0, v1, [I

    .line 636
    .local v0, "escape":[I
    const/4 v1, 0x0

    aput p1, v0, v1

    .line 637
    const/4 v1, 0x1

    aput p2, v0, v1

    .line 640
    if-nez p0, :cond_0

    .line 642
    new-instance p0, Ljava/util/Vector;

    .end local p0    # "escapes":Ljava/util/Vector;
    invoke-direct {p0}, Ljava/util/Vector;-><init>()V

    .line 644
    .restart local p0    # "escapes":Ljava/util/Vector;
    :cond_0
    invoke-virtual {p0, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 646
    return-object p0
.end method

.method private advanceIndex()I
    .locals 4

    .prologue
    const/4 v1, 0x5

    .line 194
    :goto_0
    iget v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    iget v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->endIndex:I

    if-gt v2, v3, :cond_0

    .line 196
    iget-object v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xml:[C

    iget v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    aget-char v0, v2, v3

    .line 197
    .local v0, "b":C
    const/16 v2, 0x3c

    if-ne v0, v2, :cond_1

    .line 198
    const/4 v1, 0x1

    .line 206
    .end local v0    # "b":C
    :cond_0
    :goto_1
    return v1

    .line 199
    .restart local v0    # "b":C
    :cond_1
    if-eqz v0, :cond_0

    .line 201
    const/16 v2, 0x20

    if-lt v0, v2, :cond_2

    .line 202
    const/4 v1, 0x3

    goto :goto_1

    .line 204
    :cond_2
    iget v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    goto :goto_0
.end method

.method public static createByteArray([CIIZZ)[C
    .locals 6
    .param p0, "data"    # [C
    .param p1, "offset"    # I
    .param p2, "len"    # I
    .param p3, "checkEscapes"    # Z
    .param p4, "bailOutIfNoEscapes"    # Z

    .prologue
    const/4 v5, -0x1

    .line 538
    const/4 v2, 0x0

    .line 541
    .local v2, "escapes":Ljava/util/Vector;
    const/4 v3, 0x0

    .line 543
    .local v3, "newArray":[C
    if-eqz p3, :cond_4

    .line 548
    move v0, p1

    .line 549
    .local v0, "begEscapeIndex":I
    :cond_0
    :goto_0
    if-eq v0, v5, :cond_2

    .line 550
    invoke-static {p0, p1, p2, v0}, Lcom/vlingo/core/internal/xml/XmlParser;->findBegEscape([CIII)I

    move-result v0

    .line 551
    if-eq v0, v5, :cond_0

    .line 553
    add-int/lit8 v4, v0, 0x1

    invoke-static {p0, p1, p2, v4}, Lcom/vlingo/core/internal/xml/XmlParser;->findEndEscape([CIII)I

    move-result v1

    .line 554
    .local v1, "endEscapeIndex":I
    if-eq v1, v5, :cond_1

    .line 556
    sub-int v4, v1, v0

    add-int/lit8 v4, v4, 0x1

    invoke-static {v2, v0, v4}, Lcom/vlingo/core/internal/xml/XmlParser;->addEscape(Ljava/util/Vector;II)Ljava/util/Vector;

    move-result-object v2

    .line 557
    move v0, v1

    goto :goto_0

    .line 559
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 565
    .end local v1    # "endEscapeIndex":I
    :cond_2
    if-eqz v2, :cond_3

    .line 566
    invoke-static {v2, p0, p1, p2}, Lcom/vlingo/core/internal/xml/XmlParser;->accountForEscapes(Ljava/util/Vector;[CII)[C

    move-result-object v4

    .line 574
    .end local v0    # "begEscapeIndex":I
    :goto_1
    return-object v4

    .line 567
    .restart local v0    # "begEscapeIndex":I
    :cond_3
    if-eqz p4, :cond_4

    .line 568
    const/4 v4, 0x0

    goto :goto_1

    .line 572
    .end local v0    # "begEscapeIndex":I
    :cond_4
    new-array v3, p2, [C

    .line 573
    const/4 v4, 0x0

    invoke-static {p0, p1, v3, v4, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v4, v3

    .line 574
    goto :goto_1
.end method

.method public static createString([CIIZ)Ljava/lang/String;
    .locals 2
    .param p0, "data"    # [C
    .param p1, "offset"    # I
    .param p2, "len"    # I
    .param p3, "checkEscapes"    # Z

    .prologue
    .line 521
    if-eqz p3, :cond_0

    .line 522
    const/4 v0, 0x0

    .line 523
    .local v0, "newArray":[C
    const/4 v1, 0x1

    invoke-static {p0, p1, p2, p3, v1}, Lcom/vlingo/core/internal/xml/XmlParser;->createByteArray([CIIZZ)[C

    move-result-object v0

    .line 524
    if-eqz v0, :cond_0

    .line 525
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v1

    .line 528
    .end local v0    # "newArray":[C
    :goto_0
    return-object v1

    :cond_0
    invoke-static {p0, p1, p2}, Ljava/lang/String;->valueOf([CII)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static findBegEscape([CIII)I
    .locals 5
    .param p0, "data"    # [C
    .param p1, "offset"    # I
    .param p2, "len"    # I
    .param p3, "start"    # I

    .prologue
    const/4 v2, -0x1

    .line 674
    move v1, p3

    .line 675
    .local v1, "i":I
    add-int v0, p1, p2

    .line 677
    .local v0, "end":I
    if-lt p3, v0, :cond_2

    .line 686
    :cond_0
    :goto_0
    return v2

    .line 684
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 680
    :cond_2
    if-ge v1, v0, :cond_0

    .line 681
    aget-char v3, p0, v1

    const/16 v4, 0x26

    if-ne v3, v4, :cond_1

    move v2, v1

    .line 682
    goto :goto_0
.end method

.method private static findEndEscape([CIII)I
    .locals 5
    .param p0, "data"    # [C
    .param p1, "offset"    # I
    .param p2, "len"    # I
    .param p3, "start"    # I

    .prologue
    const/4 v3, -0x1

    .line 651
    add-int v1, p1, p2

    .line 652
    .local v1, "end":I
    move v2, p3

    .line 653
    .local v2, "i":I
    const/4 v0, 0x0

    .line 655
    .local v0, "count":I
    if-lt p3, v1, :cond_1

    .line 669
    :cond_0
    :goto_0
    :sswitch_0
    return v3

    .line 658
    :cond_1
    :goto_1
    if-ge v2, v1, :cond_0

    const/4 v4, 0x6

    if-ge v0, v4, :cond_0

    .line 659
    aget-char v4, p0, v2

    sparse-switch v4, :sswitch_data_0

    .line 665
    add-int/lit8 v2, v2, 0x1

    .line 666
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :sswitch_1
    move v3, v2

    .line 661
    goto :goto_0

    .line 659
    nop

    :sswitch_data_0
    .sparse-switch
        0x26 -> :sswitch_0
        0x3b -> :sswitch_1
    .end sparse-switch
.end method

.method private getElementCode([CII)I
    .locals 3
    .param p1, "data"    # [C
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    .line 707
    const/4 v1, 0x0

    .line 708
    .local v1, "elementType":I
    invoke-static {p1, p2, p3}, Ljava/lang/String;->valueOf([CII)Ljava/lang/String;

    move-result-object v0

    .line 711
    .local v0, "elementName":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xmlElements:Lcom/vlingo/core/internal/util/ToIntHashtable;

    invoke-interface {v2, v0}, Lcom/vlingo/core/internal/util/ToIntHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 712
    iget-object v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xmlElements:Lcom/vlingo/core/internal/util/ToIntHashtable;

    invoke-interface {v2, v0}, Lcom/vlingo/core/internal/util/ToIntHashtable;->get(Ljava/lang/Object;)I

    move-result v1

    .line 715
    :cond_0
    return v1
.end method

.method private static getEscapedArrayLength(Ljava/util/Vector;I)I
    .locals 3
    .param p0, "escapes"    # Ljava/util/Vector;
    .param p1, "currentLen"    # I

    .prologue
    .line 624
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 625
    invoke-virtual {p0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    move-object v0, v2

    check-cast v0, [I

    .line 626
    .local v0, "escape":[I
    const/4 v2, 0x1

    aget v2, v0, v2

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr p1, v2

    .line 624
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 630
    .end local v0    # "escape":[I
    :cond_0
    return p1
.end method

.method private static getEscapedByte([CII)C
    .locals 5
    .param p0, "data"    # [C
    .param p1, "escapeStart"    # I
    .param p2, "escapeLen"    # I

    .prologue
    .line 691
    const/16 v2, 0x20

    .line 692
    .local v2, "escapedByte":C
    add-int/lit8 v3, p1, 0x1

    add-int/lit8 v4, p2, -0x2

    invoke-static {p0, v3, v4}, Ljava/lang/String;->valueOf([CII)Ljava/lang/String;

    move-result-object v1

    .line 695
    .local v1, "escapeName":Ljava/lang/String;
    const-string/jumbo v3, "#"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 696
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 697
    .local v0, "escapeInt":I
    int-to-char v2, v0

    .line 702
    .end local v0    # "escapeInt":I
    :cond_0
    :goto_0
    return v2

    .line 698
    :cond_1
    sget-object v3, Lcom/vlingo/core/internal/xml/XmlParser;->XML_ESCAPES:Lcom/vlingo/core/internal/util/ToIntHashtable;

    invoke-interface {v3, v1}, Lcom/vlingo/core/internal/util/ToIntHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 699
    sget-object v3, Lcom/vlingo/core/internal/xml/XmlParser;->XML_ESCAPES:Lcom/vlingo/core/internal/util/ToIntHashtable;

    invoke-interface {v3, v1}, Lcom/vlingo/core/internal/util/ToIntHashtable;->get(Ljava/lang/Object;)I

    move-result v3

    int-to-char v2, v3

    goto :goto_0
.end method

.method private handleAttributes()V
    .locals 4

    .prologue
    .line 311
    const/4 v0, 0x0

    .line 312
    .local v0, "inQuotes":Z
    const/4 v1, 0x0

    .line 314
    .local v1, "usingDoubleQuotes":Z
    invoke-direct {p0}, Lcom/vlingo/core/internal/xml/XmlParser;->resetAttributeIndicies()V

    .line 316
    :goto_0
    iget v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    iget v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->endIndex:I

    if-gt v2, v3, :cond_6

    .line 319
    iget-object v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xml:[C

    iget v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    aget-char v2, v2, v3

    sparse-switch v2, :sswitch_data_0

    .line 364
    :cond_0
    if-nez v0, :cond_7

    iget v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeNameStart:I

    if-gez v2, :cond_7

    .line 365
    iget v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    iput v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeNameStart:I

    .line 375
    :cond_1
    :goto_1
    :sswitch_0
    iget v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    goto :goto_0

    .line 321
    :sswitch_1
    if-nez v0, :cond_1

    .line 322
    iget v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    iput v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeNameEnd:I

    goto :goto_1

    .line 331
    :sswitch_2
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 332
    iget v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    iput v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeValueEnd:I

    .line 333
    invoke-direct {p0}, Lcom/vlingo/core/internal/xml/XmlParser;->addAttribute()V

    .line 334
    invoke-direct {p0}, Lcom/vlingo/core/internal/xml/XmlParser;->resetAttributeIndicies()V

    .line 335
    const/4 v0, 0x0

    goto :goto_1

    .line 337
    :cond_2
    if-eqz v0, :cond_3

    if-eqz v1, :cond_1

    .line 338
    :cond_3
    const/4 v0, 0x1

    .line 339
    const/4 v1, 0x1

    goto :goto_1

    .line 344
    :sswitch_3
    if-eqz v0, :cond_4

    if-nez v1, :cond_4

    iget v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeValueStart:I

    if-lez v2, :cond_4

    .line 345
    iget v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    iput v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeValueEnd:I

    .line 346
    invoke-direct {p0}, Lcom/vlingo/core/internal/xml/XmlParser;->addAttribute()V

    .line 347
    invoke-direct {p0}, Lcom/vlingo/core/internal/xml/XmlParser;->resetAttributeIndicies()V

    .line 348
    const/4 v0, 0x0

    goto :goto_1

    .line 350
    :cond_4
    if-eqz v0, :cond_5

    if-nez v1, :cond_1

    .line 351
    :cond_5
    const/4 v0, 0x1

    .line 352
    const/4 v1, 0x0

    goto :goto_1

    .line 358
    :sswitch_4
    if-nez v0, :cond_0

    .line 359
    iget v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    .line 377
    :cond_6
    return-void

    .line 368
    :cond_7
    if-eqz v0, :cond_1

    iget v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeValueStart:I

    if-gez v2, :cond_1

    .line 369
    iget v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    iput v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeValueStart:I

    goto :goto_1

    .line 319
    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_0
        0x22 -> :sswitch_2
        0x27 -> :sswitch_3
        0x2f -> :sswitch_4
        0x3d -> :sswitch_1
        0x3e -> :sswitch_4
    .end sparse-switch
.end method

.method private handleBegin()I
    .locals 2

    .prologue
    .line 157
    :goto_0
    iget v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    iget v1, p0, Lcom/vlingo/core/internal/xml/XmlParser;->endIndex:I

    if-gt v0, v1, :cond_1

    .line 159
    iget-object v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xml:[C

    iget v1, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    aget-char v0, v0, v1

    const/16 v1, 0x3c

    if-ne v0, v1, :cond_0

    .line 160
    const/4 v0, 0x1

    .line 164
    :goto_1
    return v0

    .line 162
    :cond_0
    iget v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    goto :goto_0

    .line 164
    :cond_1
    const/4 v0, 0x5

    goto :goto_1
.end method

.method private handleCData()I
    .locals 9

    .prologue
    const/16 v8, 0x20

    const/4 v3, 0x5

    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 382
    const/4 v2, -0x1

    .line 383
    .local v2, "lastSpaceIndex":I
    const/4 v1, 0x1

    .line 387
    .local v1, "foundOnlyBlanks":Z
    iget-boolean v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkCDataSpacing:Z

    if-eqz v4, :cond_0

    .line 388
    :goto_0
    iget v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    iget v5, p0, Lcom/vlingo/core/internal/xml/XmlParser;->endIndex:I

    if-gt v4, v5, :cond_0

    .line 391
    iget-object v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xml:[C

    iget v5, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    aget-char v4, v4, v5

    if-le v4, v8, :cond_2

    .line 397
    :cond_0
    iget v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    iput v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cDataEnd:I

    .line 398
    iget v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cDataEnd:I

    iput v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cDataStart:I

    .line 401
    :goto_1
    iget v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    iget v5, p0, Lcom/vlingo/core/internal/xml/XmlParser;->endIndex:I

    if-gt v4, v5, :cond_6

    .line 403
    iget-object v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xml:[C

    iget v5, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    aget-char v0, v4, v5

    .line 404
    .local v0, "b":C
    sparse-switch v0, :sswitch_data_0

    .line 443
    const/4 v1, 0x0

    .line 444
    const/4 v2, -0x1

    .line 447
    :cond_1
    :goto_2
    iget v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    goto :goto_1

    .line 394
    .end local v0    # "b":C
    :cond_2
    iget v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    goto :goto_0

    .line 406
    .restart local v0    # "b":C
    :sswitch_0
    iget-boolean v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkCDataSpacing:Z

    if-eqz v3, :cond_7

    if-le v2, v6, :cond_7

    .line 407
    iput v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cDataEnd:I

    .line 411
    :goto_3
    iget-boolean v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkCDataSpacing:Z

    if-eqz v3, :cond_3

    if-nez v1, :cond_4

    .line 412
    :cond_3
    invoke-direct {p0}, Lcom/vlingo/core/internal/xml/XmlParser;->outputChars()V

    .line 413
    :cond_4
    iget-boolean v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkForEscapes_CData:Z

    if-eqz v3, :cond_5

    .line 414
    iput-boolean v7, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkForEscapes_CData:Z

    .line 415
    :cond_5
    const/4 v3, 0x1

    .line 449
    .end local v0    # "b":C
    :cond_6
    :goto_4
    return v3

    .line 409
    .restart local v0    # "b":C
    :cond_7
    iget v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    iput v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cDataEnd:I

    goto :goto_3

    .line 418
    :sswitch_1
    iget-boolean v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkCDataSpacing:Z

    if-eqz v4, :cond_a

    if-le v2, v6, :cond_a

    .line 419
    iput v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cDataEnd:I

    .line 423
    :goto_5
    iget-boolean v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkCDataSpacing:Z

    if-eqz v4, :cond_8

    if-nez v1, :cond_9

    .line 424
    :cond_8
    invoke-direct {p0}, Lcom/vlingo/core/internal/xml/XmlParser;->outputChars()V

    .line 425
    :cond_9
    iget-boolean v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkForEscapes_CData:Z

    if-eqz v4, :cond_6

    .line 426
    iput-boolean v7, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkForEscapes_CData:Z

    goto :goto_4

    .line 421
    :cond_a
    iget v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    iput v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cDataEnd:I

    goto :goto_5

    .line 435
    :sswitch_2
    iget-object v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xml:[C

    iget v5, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    aput-char v8, v4, v5

    .line 437
    :sswitch_3
    if-ne v2, v6, :cond_1

    .line 438
    iget v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    goto :goto_2

    .line 404
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xb -> :sswitch_2
        0xc -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_3
        0x3c -> :sswitch_0
    .end sparse-switch
.end method

.method private handleElement()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, -0x1

    const/4 v2, 0x0

    .line 211
    iput-boolean v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->currentElementIsBegin:Z

    .line 212
    iput-boolean v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->currentElementIsEnd:Z

    .line 213
    iput v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->elementStart:I

    .line 214
    iput v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->elementEnd:I

    .line 216
    :goto_0
    iget v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    iget v1, p0, Lcom/vlingo/core/internal/xml/XmlParser;->endIndex:I

    if-gt v0, v1, :cond_3

    .line 218
    iget-object v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xml:[C

    iget v1, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    aget-char v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 261
    iget v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->elementStart:I

    if-gez v0, :cond_0

    .line 262
    iget v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    iput v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->elementStart:I

    .line 266
    :cond_0
    :goto_1
    :sswitch_0
    iget v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    goto :goto_0

    .line 221
    :sswitch_1
    const/4 v0, 0x6

    .line 268
    :goto_2
    return v0

    .line 223
    :sswitch_2
    iget v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->elementStart:I

    if-gez v0, :cond_1

    .line 224
    iput-boolean v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->currentElementIsBegin:Z

    .line 226
    :cond_1
    iput-boolean v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->currentElementIsEnd:Z

    .line 227
    iget v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->elementEnd:I

    if-gez v0, :cond_0

    iget v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->elementStart:I

    if-lez v0, :cond_0

    .line 228
    iget v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    iput v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->elementEnd:I

    goto :goto_1

    .line 233
    :sswitch_3
    iget v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->elementEnd:I

    if-gez v0, :cond_0

    iget v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->elementStart:I

    if-ltz v0, :cond_0

    .line 234
    iget v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    iput v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->elementEnd:I

    .line 235
    invoke-direct {p0}, Lcom/vlingo/core/internal/xml/XmlParser;->handleAttributes()V

    .line 236
    iget-boolean v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkForEscapes_AttributeValues:Z

    if-eqz v0, :cond_0

    .line 237
    iput-boolean v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkForEscapes_AttributeValues:Z

    goto :goto_1

    .line 247
    :sswitch_4
    iget v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->elementEnd:I

    if-gez v0, :cond_2

    iget v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->elementStart:I

    if-ltz v0, :cond_2

    .line 248
    iget v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    iput v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->elementEnd:I

    .line 250
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/core/internal/xml/XmlParser;->outputElement()V

    .line 251
    iget v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    .line 252
    const/4 v0, 0x4

    goto :goto_2

    .line 268
    :cond_3
    const/4 v0, 0x5

    goto :goto_2

    .line 218
    nop

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_3
        0x21 -> :sswitch_1
        0x22 -> :sswitch_0
        0x27 -> :sswitch_0
        0x2f -> :sswitch_2
        0x3c -> :sswitch_0
        0x3e -> :sswitch_4
    .end sparse-switch
.end method

.method private outputChars()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 503
    const/4 v0, 0x0

    .line 504
    .local v0, "cData":[C
    iget v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cDataEnd:I

    iget v5, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cDataStart:I

    sub-int v1, v4, v5

    .line 506
    .local v1, "len":I
    iget v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cachedElementType:I

    const/16 v5, 0xff

    if-eq v4, v5, :cond_3

    .line 510
    iget-object v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xml:[C

    iget v5, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cDataStart:I

    iget-boolean v6, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkEscapes:Z

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkForEscapes_CData:Z

    if-eqz v6, :cond_2

    :cond_0
    :goto_0
    invoke-static {v4, v5, v1, v2, v3}, Lcom/vlingo/core/internal/xml/XmlParser;->createByteArray([CIIZZ)[C

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cachedCData:[C

    .line 517
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v2, v3

    .line 510
    goto :goto_0

    .line 513
    :cond_3
    iget-object v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xml:[C

    iget v5, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cDataStart:I

    iget-boolean v6, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkEscapes:Z

    if-nez v6, :cond_4

    iget-boolean v6, p0, Lcom/vlingo/core/internal/xml/XmlParser;->checkForEscapes_CData:Z

    if-eqz v6, :cond_5

    :cond_4
    :goto_2
    invoke-static {v4, v5, v1, v2, v3}, Lcom/vlingo/core/internal/xml/XmlParser;->createByteArray([CIIZZ)[C

    move-result-object v0

    .line 514
    if-eqz v0, :cond_1

    .line 515
    iget-object v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->handler:Lcom/vlingo/core/internal/xml/XmlHandler;

    invoke-interface {v2, v0}, Lcom/vlingo/core/internal/xml/XmlHandler;->characters([C)V

    goto :goto_1

    :cond_5
    move v2, v3

    .line 513
    goto :goto_2
.end method

.method private outputElement()V
    .locals 10

    .prologue
    const/16 v9, 0xff

    const/4 v8, 0x0

    .line 455
    iget v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->elementEnd:I

    iget v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->elementStart:I

    sub-int v2, v3, v4

    .line 456
    .local v2, "len":I
    const/4 v1, 0x0

    .line 457
    .local v1, "elementType":I
    const/4 v0, 0x0

    .line 460
    .local v0, "elementName":[C
    iget v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cachedElementType:I

    if-eq v3, v9, :cond_0

    .line 462
    iget-object v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->handler:Lcom/vlingo/core/internal/xml/XmlHandler;

    iget v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cachedElementType:I

    iget-object v5, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cachedAttributes:Lcom/vlingo/core/internal/xml/XmlAttributes;

    iget-object v6, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cachedCData:[C

    iget v7, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cStartElementEnd:I

    invoke-interface {v3, v4, v5, v6, v7}, Lcom/vlingo/core/internal/xml/XmlHandler;->beginElement(ILcom/vlingo/core/internal/xml/XmlAttributes;[CI)V

    .line 464
    iput v9, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cachedElementType:I

    .line 465
    iput-object v8, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cachedAttributes:Lcom/vlingo/core/internal/xml/XmlAttributes;

    .line 466
    iput-object v8, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cachedCData:[C

    .line 470
    :cond_0
    iget v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->elementStart:I

    if-lez v3, :cond_2

    .line 471
    new-array v0, v2, [C

    .line 472
    iget-object v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xml:[C

    iget v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->elementStart:I

    const/4 v5, 0x0

    invoke-static {v3, v4, v0, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 473
    iget-object v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xml:[C

    iget v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->elementStart:I

    invoke-direct {p0, v3, v4, v2}, Lcom/vlingo/core/internal/xml/XmlParser;->getElementCode([CII)I

    move-result v1

    .line 474
    iget-boolean v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->currentElementIsEnd:Z

    if-eqz v3, :cond_3

    .line 476
    iget-boolean v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->currentElementIsBegin:Z

    if-eqz v3, :cond_1

    .line 479
    iget-object v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->handler:Lcom/vlingo/core/internal/xml/XmlHandler;

    iget-object v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributes:Lcom/vlingo/core/internal/xml/XmlAttributes;

    iget v5, p0, Lcom/vlingo/core/internal/xml/XmlParser;->elementEnd:I

    invoke-interface {v3, v1, v4, v8, v5}, Lcom/vlingo/core/internal/xml/XmlHandler;->beginElement(ILcom/vlingo/core/internal/xml/XmlAttributes;[CI)V

    .line 484
    :cond_1
    iget-object v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->handler:Lcom/vlingo/core/internal/xml/XmlHandler;

    iget v4, p0, Lcom/vlingo/core/internal/xml/XmlParser;->elementStart:I

    add-int/lit8 v4, v4, -0x2

    invoke-interface {v3, v1, v4}, Lcom/vlingo/core/internal/xml/XmlHandler;->endElement(II)V

    .line 497
    :cond_2
    :goto_0
    iput-object v8, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributes:Lcom/vlingo/core/internal/xml/XmlAttributes;

    .line 498
    return-void

    .line 485
    :cond_3
    iget-boolean v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->currentElementIsBegin:Z

    if-eqz v3, :cond_2

    .line 489
    iput v1, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cachedElementType:I

    .line 490
    iget v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cStartElementEnd:I

    .line 491
    iget-object v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributes:Lcom/vlingo/core/internal/xml/XmlAttributes;

    if-eqz v3, :cond_2

    .line 493
    iget-object v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributes:Lcom/vlingo/core/internal/xml/XmlAttributes;

    iput-object v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->cachedAttributes:Lcom/vlingo/core/internal/xml/XmlAttributes;

    goto :goto_0
.end method

.method private resetAttributeIndicies()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 272
    iput v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeNameStart:I

    .line 273
    iput v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeNameEnd:I

    .line 274
    iput v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeValueStart:I

    .line 275
    iput v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->attributeValueEnd:I

    .line 276
    return-void
.end method

.method private skipComments()I
    .locals 4

    .prologue
    .line 169
    const/4 v1, 0x0

    .line 171
    .local v1, "dashCount":I
    :goto_0
    iget v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    iget v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->endIndex:I

    if-gt v2, v3, :cond_1

    .line 173
    iget-object v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->xml:[C

    iget v3, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    aget-char v0, v2, v3

    .line 174
    .local v0, "b":C
    sparse-switch v0, :sswitch_data_0

    .line 183
    const/4 v1, 0x0

    .line 186
    :cond_0
    :goto_1
    iget v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    goto :goto_0

    .line 176
    :sswitch_0
    add-int/lit8 v1, v1, 0x1

    .line 177
    goto :goto_1

    .line 179
    :sswitch_1
    const/4 v2, 0x2

    if-lt v1, v2, :cond_0

    .line 180
    const/4 v2, 0x4

    .line 188
    .end local v0    # "b":C
    :goto_2
    return v2

    :cond_1
    const/4 v2, 0x5

    goto :goto_2

    .line 174
    :sswitch_data_0
    .sparse-switch
        0x2d -> :sswitch_0
        0x3e -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public parseXml()V
    .locals 2

    .prologue
    .line 122
    :goto_0
    iget v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->currentState:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 124
    :try_start_0
    iget v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->currentState:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 126
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->handler:Lcom/vlingo/core/internal/xml/XmlHandler;

    invoke-interface {v0}, Lcom/vlingo/core/internal/xml/XmlHandler;->beginDocument()V

    .line 127
    invoke-direct {p0}, Lcom/vlingo/core/internal/xml/XmlParser;->handleBegin()I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->currentState:I

    goto :goto_0

    .line 146
    :catch_0
    move-exception v0

    goto :goto_0

    .line 131
    :pswitch_2
    invoke-direct {p0}, Lcom/vlingo/core/internal/xml/XmlParser;->handleElement()I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->currentState:I

    goto :goto_0

    .line 135
    :pswitch_3
    invoke-direct {p0}, Lcom/vlingo/core/internal/xml/XmlParser;->handleCData()I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->currentState:I

    goto :goto_0

    .line 139
    :pswitch_4
    invoke-direct {p0}, Lcom/vlingo/core/internal/xml/XmlParser;->advanceIndex()I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->currentState:I

    goto :goto_0

    .line 143
    :pswitch_5
    invoke-direct {p0}, Lcom/vlingo/core/internal/xml/XmlParser;->skipComments()I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->currentState:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->handler:Lcom/vlingo/core/internal/xml/XmlHandler;

    invoke-interface {v0}, Lcom/vlingo/core/internal/xml/XmlHandler;->endDocument()V

    .line 153
    return-void

    .line 124
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public stopParsing()V
    .locals 1

    .prologue
    .line 721
    iget v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->endIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/core/internal/xml/XmlParser;->index:I

    .line 722
    return-void
.end method
