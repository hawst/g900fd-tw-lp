.class public Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "VoiceDialAction.java"


# instance fields
.field private address:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;->address:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public address(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;
    .locals 0
    .param p1, "newAddress"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;->address:Ljava/lang/String;

    .line 30
    return-object p0
.end method

.method protected execute()V
    .locals 4

    .prologue
    .line 35
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;->address:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 38
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->clearPendingSafeReaderTurn()V

    .line 39
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v0

    .line 40
    .local v0, "bluetoothOn":Z
    if-eqz v0, :cond_0

    .line 41
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopScoOnIdle()V

    .line 44
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->setAbandonInSync(Z)V

    .line 46
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction$1;

    invoke-direct {v1, p0, v0}, Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction$1;-><init>(Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;Z)V

    const-wide/16 v2, 0x258

    invoke-static {v1, v2, v3}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V

    .line 77
    .end local v0    # "bluetoothOn":Z
    :goto_0
    return-void

    .line 75
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    goto :goto_0
.end method
