.class public Lcom/vlingo/core/internal/util/OpenAppUtil;
.super Ljava/lang/Object;
.source "OpenAppUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/util/OpenAppUtil$IOpenAppNameManager;,
        Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    }
.end annotation


# static fields
.field private static iOpenAppNameManager:Lcom/vlingo/core/internal/util/OpenAppUtil$IOpenAppNameManager;

.field private static log:Lcom/vlingo/core/internal/logging/Logger;

.field private static p:Ljava/util/regex/Pattern;


# instance fields
.field protected appInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/vlingo/core/internal/util/OpenAppUtil;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/OpenAppUtil;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 38
    const-string/jumbo v0, "(?<=[^\\p{Upper}])(?=\\p{Upper})|\\s"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/OpenAppUtil;->p:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 237
    return-void
.end method

.method public static setOpenAppNameManager(Lcom/vlingo/core/internal/util/OpenAppUtil$IOpenAppNameManager;)V
    .locals 0
    .param p0, "openAppNameManager"    # Lcom/vlingo/core/internal/util/OpenAppUtil$IOpenAppNameManager;

    .prologue
    .line 242
    sput-object p0, Lcom/vlingo/core/internal/util/OpenAppUtil;->iOpenAppNameManager:Lcom/vlingo/core/internal/util/OpenAppUtil$IOpenAppNameManager;

    .line 243
    return-void
.end method


# virtual methods
.method public buildMatchingAppList(Ljava/lang/String;)V
    .locals 1
    .param p1, "searchStr"    # Ljava/lang/String;

    .prologue
    .line 104
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/util/OpenAppUtil;->buildMatchingAppList(Ljava/lang/String;Ljava/util/List;)V

    .line 105
    return-void
.end method

.method public buildMatchingAppList(Ljava/lang/String;Ljava/util/List;)V
    .locals 24
    .param p1, "searchStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 108
    .local p2, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    if-eqz p2, :cond_0

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v22

    if-eqz v22, :cond_4

    .line 109
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/core/internal/util/OpenAppUtil;->createAppList()V

    .line 113
    :goto_0
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 114
    .local v17, "matchList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 115
    .local v16, "looseMatchList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    if-eqz p1, :cond_c

    .line 116
    sget-object v22, Lcom/vlingo/core/internal/util/OpenAppUtil;->p:Ljava/util/regex/Pattern;

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v21

    .line 117
    .local v21, "searchWords":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p1}, Lcom/vlingo/core/internal/util/OpenAppUtil;->cleanAppName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 118
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v19

    .line 120
    .local v19, "searchLen":I
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 121
    sget-object v22, Lcom/vlingo/core/internal/util/OpenAppUtil;->iOpenAppNameManager:Lcom/vlingo/core/internal/util/OpenAppUtil$IOpenAppNameManager;

    if-eqz v22, :cond_1

    .line 122
    sget-object v22, Lcom/vlingo/core/internal/util/OpenAppUtil;->iOpenAppNameManager:Lcom/vlingo/core/internal/util/OpenAppUtil$IOpenAppNameManager;

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/util/OpenAppUtil$IOpenAppNameManager;->normalizeOpenAppName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 126
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/util/OpenAppUtil;->appInfoList:Ljava/util/List;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .line 127
    .local v13, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    :cond_2
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_c

    .line 128
    const/4 v10, 0x0

    .line 129
    .local v10, "found":Z
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .line 130
    .local v2, "appInfo":Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    # getter for: Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->appName:Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->access$000(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/OpenAppUtil;->cleanAppName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 131
    .local v4, "appNameLc":Ljava/lang/String;
    # getter for: Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->appNameLabel:Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->access$100(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/OpenAppUtil;->cleanAppName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 132
    .local v3, "appNameLabel":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    .line 133
    .local v5, "appNameLen":I
    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 134
    :cond_3
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->clear()V

    .line 135
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/util/OpenAppUtil;->appInfoList:Ljava/util/List;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->clear()V

    .line 137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/util/OpenAppUtil;->appInfoList:Ljava/util/List;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 170
    .end local v2    # "appInfo":Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    .end local v3    # "appNameLabel":Ljava/lang/String;
    .end local v4    # "appNameLc":Ljava/lang/String;
    .end local v5    # "appNameLen":I
    .end local v10    # "found":Z
    .end local v13    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    .end local v19    # "searchLen":I
    .end local v21    # "searchWords":[Ljava/lang/String;
    :goto_2
    return-void

    .line 111
    .end local v16    # "looseMatchList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    .end local v17    # "matchList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    :cond_4
    new-instance v22, Ljava/util/ArrayList;

    move-object/from16 v0, v22

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/util/OpenAppUtil;->appInfoList:Ljava/util/List;

    goto/16 :goto_0

    .line 141
    .restart local v2    # "appInfo":Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    .restart local v3    # "appNameLabel":Ljava/lang/String;
    .restart local v4    # "appNameLc":Ljava/lang/String;
    .restart local v5    # "appNameLen":I
    .restart local v10    # "found":Z
    .restart local v13    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    .restart local v16    # "looseMatchList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    .restart local v17    # "matchList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    .restart local v19    # "searchLen":I
    .restart local v21    # "searchWords":[Ljava/lang/String;
    :cond_5
    sget-object v23, Lcom/vlingo/core/internal/util/OpenAppUtil;->p:Ljava/util/regex/Pattern;

    # getter for: Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->appNameLabel:Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->access$100(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_7

    # getter for: Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->appNameLabel:Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->access$100(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)Ljava/lang/String;

    move-result-object v22

    :goto_3
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v6

    .line 142
    .local v6, "appNameWords":[Ljava/lang/String;
    const/16 v18, 0x0

    .line 143
    .local v18, "matchedCnt":I
    move-object/from16 v8, v21

    .local v8, "arr$":[Ljava/lang/String;
    array-length v14, v8

    .local v14, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    move v12, v11

    .end local v8    # "arr$":[Ljava/lang/String;
    .end local v11    # "i$":I
    .end local v14    # "len$":I
    .local v12, "i$":I
    :goto_4
    if-ge v12, v14, :cond_9

    aget-object v20, v8, v12

    .line 144
    .local v20, "searchWord":Ljava/lang/String;
    move-object v9, v6

    .local v9, "arr$":[Ljava/lang/String;
    array-length v15, v9

    .local v15, "len$":I
    const/4 v11, 0x0

    .end local v12    # "i$":I
    .restart local v11    # "i$":I
    :goto_5
    if-ge v11, v15, :cond_6

    aget-object v7, v9, v11

    .line 145
    .local v7, "appWord":Ljava/lang/String;
    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_8

    invoke-static/range {v20 .. v20}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_8

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_8

    .line 146
    add-int/lit8 v18, v18, 0x1

    .line 143
    .end local v7    # "appWord":Ljava/lang/String;
    :cond_6
    add-int/lit8 v11, v12, 0x1

    move v12, v11

    .end local v11    # "i$":I
    .restart local v12    # "i$":I
    goto :goto_4

    .line 141
    .end local v6    # "appNameWords":[Ljava/lang/String;
    .end local v9    # "arr$":[Ljava/lang/String;
    .end local v12    # "i$":I
    .end local v15    # "len$":I
    .end local v18    # "matchedCnt":I
    .end local v20    # "searchWord":Ljava/lang/String;
    :cond_7
    # getter for: Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->appName:Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->access$000(Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;)Ljava/lang/String;

    move-result-object v22

    goto :goto_3

    .line 144
    .restart local v6    # "appNameWords":[Ljava/lang/String;
    .restart local v7    # "appWord":Ljava/lang/String;
    .restart local v9    # "arr$":[Ljava/lang/String;
    .restart local v11    # "i$":I
    .restart local v15    # "len$":I
    .restart local v18    # "matchedCnt":I
    .restart local v20    # "searchWord":Ljava/lang/String;
    :cond_8
    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    .line 151
    .end local v7    # "appWord":Ljava/lang/String;
    .end local v9    # "arr$":[Ljava/lang/String;
    .end local v11    # "i$":I
    .end local v15    # "len$":I
    .end local v20    # "searchWord":Ljava/lang/String;
    .restart local v12    # "i$":I
    :cond_9
    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v22, v0

    move/from16 v0, v18

    move/from16 v1, v22

    if-lt v0, v1, :cond_a

    .line 152
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    const/4 v10, 0x1

    .line 156
    :cond_a
    if-nez v10, :cond_2

    .line 158
    if-lez v5, :cond_2

    mul-int/lit8 v22, v19, 0xa

    div-int v22, v22, v5

    const/16 v23, 0x4

    move/from16 v0, v22

    move/from16 v1, v23

    if-lt v0, v1, :cond_2

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_b

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 159
    :cond_b
    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 164
    .end local v2    # "appInfo":Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    .end local v3    # "appNameLabel":Ljava/lang/String;
    .end local v4    # "appNameLc":Ljava/lang/String;
    .end local v5    # "appNameLen":I
    .end local v6    # "appNameWords":[Ljava/lang/String;
    .end local v10    # "found":Z
    .end local v12    # "i$":I
    .end local v13    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    .end local v18    # "matchedCnt":I
    .end local v19    # "searchLen":I
    .end local v21    # "searchWords":[Ljava/lang/String;
    :cond_c
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->isEmpty()Z

    move-result v22

    if-eqz v22, :cond_d

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->isEmpty()Z

    move-result v22

    if-nez v22, :cond_d

    .line 165
    move-object/from16 v17, v16

    .line 167
    :cond_d
    invoke-static/range {v17 .. v17}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 168
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/util/OpenAppUtil;->appInfoList:Ljava/util/List;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->clear()V

    .line 169
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/util/OpenAppUtil;->appInfoList:Ljava/util/List;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2
.end method

.method protected cleanAppName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 173
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "\\W"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected createAppList()V
    .locals 23

    .prologue
    .line 48
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v15

    .line 49
    .local v15, "pm":Landroid/content/pm/PackageManager;
    new-instance v16, Landroid/content/Intent;

    const-string/jumbo v19, "android.intent.action.MAIN"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 50
    .local v16, "queryIntent":Landroid/content/Intent;
    const-string/jumbo v19, "android.intent.category.LAUNCHER"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    const/16 v19, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v13

    .line 53
    .local v13, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v19, Ljava/util/ArrayList;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v20

    invoke-direct/range {v19 .. v20}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/core/internal/util/OpenAppUtil;->appInfoList:Ljava/util/List;

    .line 55
    new-instance v6, Landroid/content/res/Configuration;

    invoke-direct {v6}, Landroid/content/res/Configuration;-><init>()V

    .line 56
    .local v6, "config":Landroid/content/res/Configuration;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage()Ljava/lang/String;

    move-result-object v18

    .line 57
    .local v18, "vlingoLanguage":Ljava/lang/String;
    const-string/jumbo v19, "-"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 58
    .local v5, "codes":[Ljava/lang/String;
    new-instance v19, Ljava/util/Locale;

    const/16 v20, 0x0

    aget-object v20, v5, v20

    const/16 v21, 0x1

    aget-object v21, v5, v21

    invoke-direct/range {v19 .. v21}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v19

    iput-object v0, v6, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 59
    new-instance v7, Landroid/content/res/Configuration;

    invoke-direct {v7}, Landroid/content/res/Configuration;-><init>()V

    .line 60
    .local v7, "configEng":Landroid/content/res/Configuration;
    sget-object v19, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move-object/from16 v0, v19

    iput-object v0, v7, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 62
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v19

    move-object/from16 v0, v19

    iget v8, v0, Landroid/content/res/Configuration;->fontScale:F

    .line 63
    .local v8, "fontScale":F
    iput v8, v7, Landroid/content/res/Configuration;->fontScale:F

    .line 64
    iput v8, v6, Landroid/content/res/Configuration;->fontScale:F

    .line 66
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/pm/ResolveInfo;

    .line 67
    .local v11, "info":Landroid/content/pm/ResolveInfo;
    iget-object v0, v11, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v10, v0, Landroid/content/pm/ActivityInfo;->labelRes:I

    .line 68
    .local v10, "id":I
    iget-object v0, v11, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v14, v0, Landroid/content/pm/ActivityInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    check-cast v14, Ljava/lang/String;

    .line 69
    .local v14, "name":Ljava/lang/String;
    if-nez v10, :cond_1

    if-nez v14, :cond_1

    .line 70
    iget-object v0, v11, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v10, v0, Landroid/content/pm/ApplicationInfo;->labelRes:I

    .line 71
    iget-object v0, v11, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v14, v0, Landroid/content/pm/ApplicationInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    .end local v14    # "name":Ljava/lang/String;
    check-cast v14, Ljava/lang/String;

    .line 72
    .restart local v14    # "name":Ljava/lang/String;
    if-nez v10, :cond_1

    if-nez v14, :cond_1

    .line 73
    iget-object v0, v11, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v14, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 79
    :cond_1
    :try_start_0
    iget-object v0, v11, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v17

    .line 80
    .local v17, "res":Landroid/content/res/Resources;
    const/16 v19, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v7, v1}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 81
    invoke-virtual {v11, v15}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v12

    .line 82
    .local v12, "label":Ljava/lang/CharSequence;
    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 83
    .local v3, "appNameEng":Ljava/lang/String;
    const-string/jumbo v4, ""

    .line 84
    .local v4, "appNameLabel":Ljava/lang/String;
    if-eqz v10, :cond_2

    .line 85
    const/16 v19, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v6, v1}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 86
    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 89
    :cond_2
    if-nez v3, :cond_3

    if-eqz v4, :cond_0

    .line 90
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/util/OpenAppUtil;->appInfoList:Ljava/util/List;

    move-object/from16 v19, v0

    new-instance v20, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    iget-object v0, v11, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v21, v0

    iget-object v0, v11, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v3, v4, v1, v2}, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v19 .. v20}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    .line 92
    .end local v3    # "appNameEng":Ljava/lang/String;
    .end local v4    # "appNameLabel":Ljava/lang/String;
    .end local v12    # "label":Ljava/lang/CharSequence;
    .end local v17    # "res":Landroid/content/res/Resources;
    :catch_0
    move-exception v19

    goto/16 :goto_0

    .line 101
    .end local v10    # "id":I
    .end local v11    # "info":Landroid/content/pm/ResolveInfo;
    .end local v14    # "name":Ljava/lang/String;
    :cond_4
    return-void

    .line 95
    .restart local v10    # "id":I
    .restart local v11    # "info":Landroid/content/pm/ResolveInfo;
    .restart local v14    # "name":Ljava/lang/String;
    :catch_1
    move-exception v19

    goto/16 :goto_0
.end method

.method public getAppInfoList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/vlingo/core/internal/util/OpenAppUtil;->appInfoList:Ljava/util/List;

    return-object v0
.end method

.method public hasApplicationInstalled(Ljava/lang/String;)Z
    .locals 5
    .param p1, "appPackage"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 177
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 179
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/16 v4, 0x80

    :try_start_0
    invoke-virtual {v2, p1, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 180
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_0

    const/4 v3, 0x1

    .line 182
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    return v3

    .line 181
    :catch_0
    move-exception v0

    .line 182
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method
