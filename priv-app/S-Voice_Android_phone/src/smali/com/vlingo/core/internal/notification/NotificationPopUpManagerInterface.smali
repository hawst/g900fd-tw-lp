.class public interface abstract Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;
.super Ljava/lang/Object;
.source "NotificationPopUpManagerInterface.java"


# virtual methods
.method public abstract dismissNotifications()V
.end method

.method public abstract existDialog()Z
.end method

.method public abstract hasNotifications(Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)Z
.end method

.method public abstract needNotifications()Z
.end method

.method public abstract sendResult(I)V
.end method

.method public abstract setContext(Landroid/app/Activity;)V
.end method

.method public abstract showNextNotification()V
.end method

.method public abstract showNotification(Lcom/vlingo/core/internal/notification/NotificationPopUp;Landroid/app/Activity;)V
.end method

.method public abstract showingNotifications()Z
.end method
