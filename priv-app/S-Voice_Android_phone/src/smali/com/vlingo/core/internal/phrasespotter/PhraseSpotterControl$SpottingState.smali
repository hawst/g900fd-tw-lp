.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpottingState;
.super Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
.source "PhraseSpotterControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SpottingState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V
    .locals 0

    .prologue
    .line 308
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpottingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V

    return-void
.end method


# virtual methods
.method public start(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V
    .locals 0
    .param p1, "phraseSpotterParams"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .param p2, "listener"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    .prologue
    .line 312
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->start(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V

    .line 313
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 317
    invoke-super {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->stop()V

    .line 318
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpottingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpottingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStoppingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    invoke-static {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$600(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v1

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V

    .line 319
    return-void
.end method
