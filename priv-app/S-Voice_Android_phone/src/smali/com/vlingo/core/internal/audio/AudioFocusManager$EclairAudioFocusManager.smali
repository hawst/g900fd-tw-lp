.class Lcom/vlingo/core/internal/audio/AudioFocusManager$EclairAudioFocusManager;
.super Lcom/vlingo/core/internal/audio/AudioFocusManager;
.source "AudioFocusManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/audio/AudioFocusManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EclairAudioFocusManager"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/audio/AudioFocusManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/audio/AudioFocusManager$1;

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/AudioFocusManager$EclairAudioFocusManager;-><init>()V

    return-void
.end method


# virtual methods
.method public abandonAudioFocus()V
    .locals 0

    .prologue
    .line 107
    return-void
.end method

.method public abandonAudioFocusInSync()V
    .locals 0

    .prologue
    .line 120
    return-void
.end method

.method public requestAudioFocus(II)V
    .locals 0
    .param p1, "audioStream"    # I
    .param p2, "focusType"    # I

    .prologue
    .line 103
    return-void
.end method

.method public setAbandonInSync(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 126
    return-void
.end method

.method public setNullTask()V
    .locals 0

    .prologue
    .line 132
    return-void
.end method

.method public setTaskOnGetAudioFocus(IILjava/lang/Runnable;)V
    .locals 0
    .param p1, "audioStream"    # I
    .param p2, "focusType"    # I
    .param p3, "task"    # Ljava/lang/Runnable;

    .prologue
    .line 114
    return-void
.end method
