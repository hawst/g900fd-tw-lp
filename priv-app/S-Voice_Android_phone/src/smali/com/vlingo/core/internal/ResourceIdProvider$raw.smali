.class public final enum Lcom/vlingo/core/internal/ResourceIdProvider$raw;
.super Ljava/lang/Enum;
.source "ResourceIdProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/ResourceIdProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "raw"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/ResourceIdProvider$raw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/ResourceIdProvider$raw;

.field public static final enum core_app_aliases:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

.field public static final enum core_processing_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

.field public static final enum core_start_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

.field public static final enum core_start_tone_bt:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

.field public static final enum core_start_tone_drv:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

.field public static final enum core_stop_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

.field public static final enum core_stop_tone_bt:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

.field public static final enum core_stop_tone_drv:Lcom/vlingo/core/internal/ResourceIdProvider$raw;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 433
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    const-string/jumbo v1, "core_start_tone"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/ResourceIdProvider$raw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_start_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    .line 434
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    const-string/jumbo v1, "core_start_tone_drv"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/ResourceIdProvider$raw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_start_tone_drv:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    .line 435
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    const-string/jumbo v1, "core_stop_tone"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/core/internal/ResourceIdProvider$raw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    .line 436
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    const-string/jumbo v1, "core_stop_tone_drv"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/core/internal/ResourceIdProvider$raw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone_drv:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    .line 437
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    const-string/jumbo v1, "core_start_tone_bt"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/core/internal/ResourceIdProvider$raw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_start_tone_bt:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    .line 438
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    const-string/jumbo v1, "core_stop_tone_bt"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$raw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone_bt:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    .line 439
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    const-string/jumbo v1, "core_processing_tone"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$raw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_processing_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    .line 440
    new-instance v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    const-string/jumbo v1, "core_app_aliases"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider$raw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_app_aliases:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    .line 432
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_start_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_start_tone_drv:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone_drv:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_start_tone_bt:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone_bt:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_processing_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_app_aliases:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->$VALUES:[Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 432
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/ResourceIdProvider$raw;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 432
    const-class v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/ResourceIdProvider$raw;
    .locals 1

    .prologue
    .line 432
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->$VALUES:[Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/ResourceIdProvider$raw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    return-object v0
.end method
