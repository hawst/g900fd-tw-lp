.class public Lcom/vlingo/core/internal/util/ListControlData;
.super Ljava/lang/Object;
.source "ListControlData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    }
.end annotation


# instance fields
.field private m_currentPage:I

.field private m_maxPages:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "listSize"    # I

    .prologue
    .line 13
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/util/ListControlData;-><init>(II)V

    .line 14
    return-void
.end method

.method public constructor <init>(II)V
    .locals 2
    .param p1, "listSize"    # I
    .param p2, "currentPage"    # I

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput p2, p0, Lcom/vlingo/core/internal/util/ListControlData;->m_currentPage:I

    .line 18
    add-int/lit8 v0, p1, -0x1

    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getMultiWidgetItemsInitialMax()I

    move-result v1

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/core/internal/util/ListControlData;->m_maxPages:I

    .line 19
    return-void
.end method


# virtual methods
.method public decCurrentPage()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
        }
    .end annotation

    .prologue
    .line 30
    iget v0, p0, Lcom/vlingo/core/internal/util/ListControlData;->m_currentPage:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 31
    iget v0, p0, Lcom/vlingo/core/internal/util/ListControlData;->m_currentPage:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/vlingo/core/internal/util/ListControlData;->m_currentPage:I

    .line 35
    return-void

    .line 33
    :cond_0
    new-instance v0, Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;-><init>(Lcom/vlingo/core/internal/util/ListControlData;)V

    throw v0
.end method

.method public filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 51
    .local p1, "sourceList":Ljava/util/List;, "Ljava/util/List<TT;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 52
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<TT;>;"
    iget v2, p0, Lcom/vlingo/core/internal/util/ListControlData;->m_currentPage:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/ListControlData;->getPageSize()I

    move-result v3

    mul-int v0, v2, v3

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget v2, p0, Lcom/vlingo/core/internal/util/ListControlData;->m_currentPage:I

    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/ListControlData;->getPageSize()I

    move-result v3

    mul-int/2addr v2, v3

    if-ge v0, v2, :cond_0

    .line 53
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 55
    :cond_0
    return-object v1
.end method

.method public getCurrentPage()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/vlingo/core/internal/util/ListControlData;->m_currentPage:I

    return v0
.end method

.method public getPageSize()I
    .locals 1

    .prologue
    .line 47
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getMultiWidgetItemsInitialMax()I

    move-result v0

    return v0
.end method

.method public incCurrentPage()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
        }
    .end annotation

    .prologue
    .line 39
    iget v0, p0, Lcom/vlingo/core/internal/util/ListControlData;->m_maxPages:I

    iget v1, p0, Lcom/vlingo/core/internal/util/ListControlData;->m_currentPage:I

    if-le v0, v1, :cond_0

    .line 40
    iget v0, p0, Lcom/vlingo/core/internal/util/ListControlData;->m_currentPage:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/core/internal/util/ListControlData;->m_currentPage:I

    .line 44
    return-void

    .line 42
    :cond_0
    new-instance v0, Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;-><init>(Lcom/vlingo/core/internal/util/ListControlData;)V

    throw v0
.end method

.method public setCurrentPage(I)V
    .locals 0
    .param p1, "currentPage"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/vlingo/core/internal/util/ListControlData;->m_currentPage:I

    .line 27
    return-void
.end method
