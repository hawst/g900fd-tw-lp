.class public Lcom/vlingo/core/internal/debug/actions/AudioFileUtt;
.super Lcom/vlingo/core/internal/debug/actions/Utt;
.source "AudioFileUtt.java"


# instance fields
.field private audioType:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

.field private fileName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 12
    sget-object v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;->PCM_16KHZ_16BIT:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    invoke-direct {p0, p1, v0}, Lcom/vlingo/core/internal/debug/actions/AudioFileUtt;-><init>(Ljava/lang/String;Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;)V

    .line 13
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;)V
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "audioType"    # Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    .prologue
    .line 16
    sget-object v0, Lcom/vlingo/core/internal/debug/actions/Utt$Type;->AUDIO_FILE:Lcom/vlingo/core/internal/debug/actions/Utt$Type;

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/debug/actions/Utt;-><init>(Lcom/vlingo/core/internal/debug/actions/Utt$Type;)V

    .line 17
    iput-object p1, p0, Lcom/vlingo/core/internal/debug/actions/AudioFileUtt;->fileName:Ljava/lang/String;

    .line 18
    iput-object p2, p0, Lcom/vlingo/core/internal/debug/actions/AudioFileUtt;->audioType:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    .line 19
    return-void
.end method


# virtual methods
.method public getAudioType()Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/actions/AudioFileUtt;->audioType:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vlingo/core/internal/debug/actions/AudioFileUtt;->fileName:Ljava/lang/String;

    return-object v0
.end method
