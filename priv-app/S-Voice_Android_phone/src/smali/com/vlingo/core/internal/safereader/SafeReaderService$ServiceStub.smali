.class Lcom/vlingo/core/internal/safereader/SafeReaderService$ServiceStub;
.super Landroid/os/Binder;
.source "SafeReaderService.java"

# interfaces
.implements Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/safereader/SafeReaderService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ServiceStub"
.end annotation


# instance fields
.field mService:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/core/internal/safereader/SafeReaderService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/safereader/SafeReaderService;)V
    .locals 1
    .param p1, "service"    # Lcom/vlingo/core/internal/safereader/SafeReaderService;

    .prologue
    .line 325
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 326
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    .line 327
    return-void
.end method


# virtual methods
.method public broadcastStatusUpdate()V
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderService;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->broadcastStatusUpdate()V

    .line 357
    return-void
.end method

.method public doesSupportNotifications()Z
    .locals 1

    .prologue
    .line 388
    const/4 v0, 0x0

    return v0
.end method

.method public getNotification()Landroid/app/Notification;
    .locals 1

    .prologue
    .line 392
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNotificationId()I
    .locals 1

    .prologue
    .line 396
    const/4 v0, 0x0

    return v0
.end method

.method public isSafeReaderOn()Z
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderService;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->isSafeReaderOn()Z

    move-result v0

    return v0
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderService;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->pause()V

    .line 342
    return-void
.end method

.method public registerAlertHandler(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 361
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderService;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->registerListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 362
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderService;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->resume()V

    .line 347
    return-void
.end method

.method public safeReaderDeinit()V
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderService;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderDeinit()V

    .line 378
    return-void
.end method

.method public safeReaderInit(Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;)V
    .locals 1
    .param p1, "engine"    # Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    .prologue
    .line 372
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderService;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->safeReaderInit(Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;)V

    .line 373
    return-void
.end method

.method public skipCurrentlyPlayingItem()V
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderService;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->skipCurrentlyPlayingItem()V

    .line 352
    return-void
.end method

.method public startSafeReading()V
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderService;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->startSafeReading()V

    .line 332
    return-void
.end method

.method public stopSafeReading()V
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderService;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->stopSafeReading()V

    .line 337
    return-void
.end method

.method public unregisterAlertHandler(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 366
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderService;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/safereader/SafeReaderService;->unregisterListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 367
    return-void
.end method

.method public updateNotification()V
    .locals 0

    .prologue
    .line 400
    return-void
.end method
