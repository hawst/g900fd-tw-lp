.class abstract Lcom/vlingo/core/internal/settings/SettingsImpl;
.super Ljava/lang/Object;
.source "SettingsImpl.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/vlingo/core/internal/settings/SettingsImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/settings/SettingsImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static clearDataValuesDatabase(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 129
    const/4 v1, 0x0

    .line 131
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string/jumbo v3, "VlingoDataSettings"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p0, v3, v4, v5}, Landroid/content/Context;->openOrCreateDatabase(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 132
    const-string/jumbo v0, "DROP TABLE IF EXISTS dataValues"

    .line 133
    .local v0, "createDB":Ljava/lang/String;
    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    if-eqz v1, :cond_0

    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 141
    .end local v0    # "createDB":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 135
    :catch_0
    move-exception v2

    .line 136
    .local v2, "ex":Ljava/lang/Exception;
    :try_start_2
    sget-object v3, Lcom/vlingo/core/internal/settings/SettingsImpl;->TAG:Ljava/lang/String;

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 139
    if-eqz v1, :cond_0

    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v3

    goto :goto_0

    .end local v2    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    :try_start_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :cond_1
    :goto_1
    throw v3

    .restart local v0    # "createDB":Ljava/lang/String;
    :catch_2
    move-exception v3

    goto :goto_0

    .end local v0    # "createDB":Ljava/lang/String;
    :catch_3
    move-exception v4

    goto :goto_1
.end method

.method static getDataImpl(Ljava/lang/String;)[B
    .locals 11
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 97
    const/4 v0, 0x0

    .line 98
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v8, 0x0

    .line 100
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/settings/SettingsImpl;->getDataValuesDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 101
    const-string/jumbo v1, "dataValues"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "setting_data"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setting_key LIKE \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 102
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 103
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 104
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 109
    if-eqz v8, :cond_0

    .line 110
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 112
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 113
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 116
    :cond_1
    :goto_1
    return-object v1

    .line 109
    :cond_2
    if-eqz v8, :cond_3

    .line 110
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 112
    :cond_3
    :goto_2
    if-eqz v0, :cond_4

    .line 113
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_4
    :goto_3
    move-object v1, v10

    .line 116
    goto :goto_1

    .line 106
    :catch_0
    move-exception v9

    .line 107
    .local v9, "ex":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 109
    if-eqz v8, :cond_5

    .line 110
    :try_start_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 112
    :cond_5
    :goto_4
    if-eqz v0, :cond_4

    .line 113
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_3

    .line 109
    .end local v9    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_6

    .line 110
    :try_start_5
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 112
    :cond_6
    :goto_5
    if-eqz v0, :cond_7

    .line 113
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_7
    throw v1

    .line 110
    :catch_1
    move-exception v2

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_2

    .restart local v9    # "ex":Ljava/lang/Exception;
    :catch_3
    move-exception v1

    goto :goto_4

    .end local v9    # "ex":Ljava/lang/Exception;
    :catch_4
    move-exception v2

    goto :goto_5
.end method

.method private static getDataValuesDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 6

    .prologue
    .line 120
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 121
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v3, "VlingoDataSettings"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Context;->openOrCreateDatabase(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 123
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string/jumbo v1, "CREATE TABLE IF NOT EXISTS dataValues (setting_key VARCHAR(255), setting_data BLOB)"

    .line 124
    .local v1, "createDB":Ljava/lang/String;
    invoke-virtual {v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 125
    return-object v2
.end method

.method static getImageImpl(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 26
    const/4 v1, 0x0

    .line 27
    .local v1, "image":Landroid/graphics/Bitmap;
    invoke-static {p0}, Lcom/vlingo/core/internal/settings/Settings;->getData(Ljava/lang/String;)[B

    move-result-object v0

    .line 28
    .local v0, "bytes":[B
    if-eqz v0, :cond_0

    .line 29
    const/4 v2, 0x0

    array-length v3, v0

    invoke-static {v0, v2, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 31
    :cond_0
    return-object v1
.end method

.method static setDataImpl(Ljava/lang/String;[B)V
    .locals 11
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "data"    # [B

    .prologue
    .line 56
    if-eqz p1, :cond_6

    array-length v1, p1

    if-lez v1, :cond_6

    .line 57
    const/4 v0, 0x0

    .line 58
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v8, 0x0

    .line 60
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 61
    .local v9, "cv":Landroid/content/ContentValues;
    const-string/jumbo v1, "setting_data"

    invoke-virtual {v9, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 62
    const-string/jumbo v1, "setting_key"

    invoke-virtual {v9, v1, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    invoke-static {}, Lcom/vlingo/core/internal/settings/SettingsImpl;->getDataValuesDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 64
    const-string/jumbo v1, "dataValues"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "setting_data"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setting_key LIKE \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 65
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_2

    .line 66
    const-string/jumbo v1, "dataValues"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v9}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    :goto_0
    if-eqz v8, :cond_0

    .line 75
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    .line 77
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    .line 78
    :try_start_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6

    .line 94
    .end local v8    # "c":Landroid/database/Cursor;
    .end local v9    # "cv":Landroid/content/ContentValues;
    :cond_1
    :goto_2
    return-void

    .line 69
    .restart local v8    # "c":Landroid/database/Cursor;
    .restart local v9    # "cv":Landroid/content/ContentValues;
    :cond_2
    :try_start_3
    const-string/jumbo v1, "dataValues"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setting_key LIKE \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 71
    .end local v9    # "cv":Landroid/content/ContentValues;
    :catch_0
    move-exception v10

    .line 72
    .local v10, "ex":Landroid/database/SQLException;
    :try_start_4
    invoke-virtual {v10}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 74
    if-eqz v8, :cond_3

    .line 75
    :try_start_5
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7

    .line 77
    :cond_3
    :goto_3
    if-eqz v0, :cond_1

    .line 78
    :try_start_6
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_2

    .line 74
    .end local v10    # "ex":Landroid/database/SQLException;
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_4

    .line 75
    :try_start_7
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_8

    .line 77
    :cond_4
    :goto_4
    if-eqz v0, :cond_5

    .line 78
    :try_start_8
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_9

    :cond_5
    :goto_5
    throw v1

    .line 82
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v8    # "c":Landroid/database/Cursor;
    :cond_6
    const/4 v0, 0x0

    .line 84
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_9
    invoke-static {}, Lcom/vlingo/core/internal/settings/SettingsImpl;->getDataValuesDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 85
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "DELETE FROM dataValues WHERE setting_key LIKE \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_9
    .catch Landroid/database/SQLException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 89
    if-eqz v0, :cond_1

    .line 90
    :try_start_a
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_2

    .line 86
    :catch_3
    move-exception v10

    .line 87
    .restart local v10    # "ex":Landroid/database/SQLException;
    :try_start_b
    invoke-virtual {v10}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 89
    if-eqz v0, :cond_1

    .line 90
    :try_start_c
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_4

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_2

    .line 89
    .end local v10    # "ex":Landroid/database/SQLException;
    :catchall_1
    move-exception v1

    if-eqz v0, :cond_7

    .line 90
    :try_start_d
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_a

    :cond_7
    :goto_6
    throw v1

    .line 75
    .restart local v8    # "c":Landroid/database/Cursor;
    .restart local v9    # "cv":Landroid/content/ContentValues;
    :catch_5
    move-exception v1

    goto/16 :goto_1

    .line 78
    :catch_6
    move-exception v1

    goto/16 :goto_2

    .line 75
    .end local v9    # "cv":Landroid/content/ContentValues;
    .restart local v10    # "ex":Landroid/database/SQLException;
    :catch_7
    move-exception v1

    goto :goto_3

    .end local v10    # "ex":Landroid/database/SQLException;
    :catch_8
    move-exception v2

    goto :goto_4

    .line 78
    :catch_9
    move-exception v2

    goto :goto_5

    .line 90
    .end local v8    # "c":Landroid/database/Cursor;
    :catch_a
    move-exception v2

    goto :goto_6
.end method

.method static setImageImpl(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 35
    if-eqz p1, :cond_0

    .line 36
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 38
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {p1, v2, v3, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 39
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    :try_start_1
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 48
    :goto_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-static {p0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setData(Ljava/lang/String;[B)V

    .line 53
    .end local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    :goto_1
    return-void

    .line 40
    .restart local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    :catch_0
    move-exception v1

    .line 41
    .local v1, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 44
    :try_start_3
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 45
    :catch_1
    move-exception v2

    goto :goto_0

    .line 43
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    .line 44
    :try_start_4
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 46
    :goto_2
    throw v2

    .line 51
    .end local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    :cond_0
    const/4 v2, 0x0

    invoke-static {p0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setData(Ljava/lang/String;[B)V

    goto :goto_1

    .line 45
    .restart local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    :catch_2
    move-exception v2

    goto :goto_0

    :catch_3
    move-exception v3

    goto :goto_2
.end method
