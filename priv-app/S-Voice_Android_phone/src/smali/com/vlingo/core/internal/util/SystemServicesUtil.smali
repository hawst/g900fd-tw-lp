.class public Lcom/vlingo/core/internal/util/SystemServicesUtil;
.super Ljava/lang/Object;
.source "SystemServicesUtil.java"


# instance fields
.field private context:Landroid/content/Context;

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mLocationManger:Landroid/location/LocationManager;

.field private mWifiManger:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/vlingo/core/internal/util/SystemServicesUtil;->context:Landroid/content/Context;

    .line 33
    return-void
.end method

.method private getBluetoothAdapter()Landroid/bluetooth/BluetoothAdapter;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/vlingo/core/internal/util/SystemServicesUtil;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_0

    .line 107
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/util/SystemServicesUtil;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/util/SystemServicesUtil;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method

.method private getConnectivityManager()Landroid/net/ConnectivityManager;
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/vlingo/core/internal/util/SystemServicesUtil;->mConnectivityManager:Landroid/net/ConnectivityManager;

    if-nez v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/vlingo/core/internal/util/SystemServicesUtil;->context:Landroid/content/Context;

    const-string/jumbo v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/vlingo/core/internal/util/SystemServicesUtil;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/util/SystemServicesUtil;->mConnectivityManager:Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method private getLocationManager()Landroid/location/LocationManager;
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/vlingo/core/internal/util/SystemServicesUtil;->mLocationManger:Landroid/location/LocationManager;

    if-nez v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/vlingo/core/internal/util/SystemServicesUtil;->context:Landroid/content/Context;

    const-string/jumbo v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/vlingo/core/internal/util/SystemServicesUtil;->mLocationManger:Landroid/location/LocationManager;

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/util/SystemServicesUtil;->mLocationManger:Landroid/location/LocationManager;

    return-object v0
.end method

.method private getWifiManager()Landroid/net/wifi/WifiManager;
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/vlingo/core/internal/util/SystemServicesUtil;->mWifiManger:Landroid/net/wifi/WifiManager;

    if-nez v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/vlingo/core/internal/util/SystemServicesUtil;->context:Landroid/content/Context;

    const-string/jumbo v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/vlingo/core/internal/util/SystemServicesUtil;->mWifiManger:Landroid/net/wifi/WifiManager;

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/util/SystemServicesUtil;->mWifiManger:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method private gpsToggle(Landroid/content/Context;)V
    .locals 4
    .param p1, "contextParam"    # Landroid/content/Context;

    .prologue
    .line 94
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "location_providers_allowed"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 96
    .local v1, "provider":Ljava/lang/String;
    const-string/jumbo v2, "gps"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 97
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 98
    .local v0, "poke":Landroid/content/Intent;
    const-string/jumbo v2, "com.android.settings"

    const-string/jumbo v3, "com.android.settings.widget.SettingsAppWidgetProvider"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    const-string/jumbo v2, "android.intent.category.ALTERNATIVE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 100
    const-string/jumbo v2, "3"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 101
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 103
    .end local v0    # "poke":Landroid/content/Intent;
    :cond_0
    return-void
.end method


# virtual methods
.method public isBluetoothEnabled()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 36
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->getBluetoothAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    .line 37
    .local v0, "bluetoothAdapter":Landroid/bluetooth/BluetoothAdapter;
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v1

    .line 38
    .local v1, "state":I
    packed-switch v1, :pswitch_data_0

    .line 46
    :goto_0
    :pswitch_0
    return v2

    .line 43
    :pswitch_1
    const/4 v2, 0x1

    goto :goto_0

    .line 38
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isGpsEnabled()Z
    .locals 2

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->getLocationManager()Landroid/location/LocationManager;

    move-result-object v0

    .line 65
    .local v0, "locationManger":Landroid/location/LocationManager;
    const-string/jumbo v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public isWifiConnected()Z
    .locals 3

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->getConnectivityManager()Landroid/net/ConnectivityManager;

    move-result-object v0

    .line 88
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 89
    .local v1, "mWifi":Landroid/net/NetworkInfo;
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    return v2
.end method

.method public isWifiEnabled()Z
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->getWifiManager()Landroid/net/wifi/WifiManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    return v0
.end method

.method public setBluetoothEnabled(Z)Z
    .locals 2
    .param p1, "bluetoothEnabled"    # Z

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->getBluetoothAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    .line 57
    .local v0, "bluetoothAdapter":Landroid/bluetooth/BluetoothAdapter;
    if-eqz p1, :cond_0

    .line 58
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    move-result v1

    .line 60
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    move-result v1

    goto :goto_0
.end method

.method public setGpsEnabled(Z)V
    .locals 2
    .param p1, "gpsEnabled"    # Z

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vlingo/core/internal/util/SystemServicesUtil;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "gps"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 75
    return-void
.end method

.method public setWifiEnabled(Z)Z
    .locals 2
    .param p1, "wifiEnabled"    # Z

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->getWifiManager()Landroid/net/wifi/WifiManager;

    move-result-object v0

    .line 83
    .local v0, "wifiManger":Landroid/net/wifi/WifiManager;
    invoke-virtual {v0, p1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    move-result v1

    return v1
.end method
