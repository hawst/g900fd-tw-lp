.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IdleState;
.super Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
.source "PhraseSpotterControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "IdleState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V
    .locals 0

    .prologue
    .line 251
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IdleState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V

    return-void
.end method


# virtual methods
.method public onEnter()V
    .locals 3

    .prologue
    .line 265
    invoke-super {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->onEnter()V

    .line 266
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IdleState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStateTransitionLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 267
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IdleState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRestartWhenIdle:Z
    invoke-static {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$000(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "Idle.onEnter: mRestartWhenIdle==true, so restarting."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IdleState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    const/4 v2, 0x0

    # setter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRestartWhenIdle:Z
    invoke-static {v0, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$002(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Z)Z

    .line 270
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IdleState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    invoke-static {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IdleState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mListener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    invoke-static {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IdleState;->start(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V

    .line 272
    :cond_0
    monitor-exit v1

    .line 273
    return-void

    .line 272
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public start(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V
    .locals 3
    .param p1, "phraseSpotterParams"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .param p2, "listener"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    .prologue
    .line 255
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->start(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V

    .line 256
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IdleState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStateTransitionLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 257
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IdleState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # setter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mPhraseSpotterParams:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    invoke-static {v0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$202(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    .line 258
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IdleState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # setter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mListener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    invoke-static {v0, p2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$302(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    .line 259
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IdleState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$IdleState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStartingState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    invoke-static {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$400(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v2

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V
    invoke-static {v0, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V

    .line 260
    monitor-exit v1

    .line 261
    return-void

    .line 260
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
