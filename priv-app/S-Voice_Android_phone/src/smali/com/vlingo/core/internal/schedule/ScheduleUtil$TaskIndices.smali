.class Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;
.super Ljava/lang/Object;
.source "ScheduleUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/schedule/ScheduleUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TaskIndices"
.end annotation


# instance fields
.field final TASK_ACCOUNT_NAME_COL:I

.field final TASK_BODY_COL:I

.field final TASK_COMPLETE_COL:I

.field final TASK_DUE_DATE_COL:I

.field final TASK_GROUPID_COL:I

.field final TASK_ID_COL:I

.field final TASK_IMPORTANCE_COL:I

.field final TASK_REMINDER_SET_COL:I

.field final TASK_REMINDER_TIME_COL:I

.field final TASK_REMINDER_TYPE_COL:I

.field final TASK_SUBJECT_COL:I

.field final TASK_UTC_DUE_DATE_COL:I


# direct methods
.method constructor <init>(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cur"    # Landroid/database/Cursor;

    .prologue
    .line 1392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1393
    const-string/jumbo v0, "subject"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_SUBJECT_COL:I

    .line 1394
    const-string/jumbo v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_ID_COL:I

    .line 1395
    const-string/jumbo v0, "due_date"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_DUE_DATE_COL:I

    .line 1396
    const-string/jumbo v0, "utc_due_date"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_UTC_DUE_DATE_COL:I

    .line 1397
    const-string/jumbo v0, "accountName"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_ACCOUNT_NAME_COL:I

    .line 1398
    const-string/jumbo v0, "reminder_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_REMINDER_TYPE_COL:I

    .line 1399
    const-string/jumbo v0, "reminder_set"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_REMINDER_SET_COL:I

    .line 1400
    const-string/jumbo v0, "reminder_time"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_REMINDER_TIME_COL:I

    .line 1401
    const-string/jumbo v0, "importance"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_IMPORTANCE_COL:I

    .line 1402
    const-string/jumbo v0, "groupId"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_GROUPID_COL:I

    .line 1403
    const-string/jumbo v0, "body"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_BODY_COL:I

    .line 1404
    const-string/jumbo v0, "complete"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/core/internal/schedule/ScheduleUtil$TaskIndices;->TASK_COMPLETE_COL:I

    .line 1405
    return-void
.end method
