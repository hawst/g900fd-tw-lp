.class final enum Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;
.super Ljava/lang/Enum;
.source "MicrophoneStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/audio/MicrophoneStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "RecorderState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

.field public static final enum CLOSED:Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

.field public static final enum STARTED:Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 50
    new-instance v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    const-string/jumbo v1, "STARTED"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;->STARTED:Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    new-instance v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    const-string/jumbo v1, "CLOSED"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;->CLOSED:Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    sget-object v1, Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;->STARTED:Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;->CLOSED:Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;->$VALUES:[Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 50
    const-class v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;->$VALUES:[Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/audio/MicrophoneStream$RecorderState;

    return-object v0
.end method
