.class public final enum Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;
.super Ljava/lang/Enum;
.source "CMAWeatherLookupHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CMADateType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

.field public static final enum OTHER:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

.field public static final enum TODAY:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

    const-string/jumbo v1, "TODAY"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;->TODAY:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

    const-string/jumbo v1, "OTHER"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;->OTHER:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

    .line 37
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;->TODAY:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;->OTHER:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;->$VALUES:[Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

    return-object v0
.end method
