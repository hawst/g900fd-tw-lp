.class public final enum Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;
.super Ljava/lang/Enum;
.source "EndpointTimeWithSpeech.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

.field public static final enum DEFAULT:Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

.field public static final enum LONG:Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

.field public static final enum MEDIUM:Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

.field public static final enum SHORT:Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;


# instance fields
.field private endpointTimeWithSpeechMilliseconds:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 16
    new-instance v0, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    const-string/jumbo v1, "SHORT"

    const/16 v2, 0x190

    invoke-direct {v0, v1, v3, v2}, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;->SHORT:Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    new-instance v0, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    const-string/jumbo v1, "MEDIUM"

    const/16 v2, 0x2ee

    invoke-direct {v0, v1, v4, v2}, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;->MEDIUM:Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    new-instance v0, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    const-string/jumbo v1, "LONG"

    const/16 v2, 0x6d6

    invoke-direct {v0, v1, v5, v2}, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;->LONG:Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    new-instance v0, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    const-string/jumbo v1, "DEFAULT"

    sget-object v2, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;->MEDIUM:Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;->getEndpointTimeWithSpeechMilliseconds()I

    move-result v2

    invoke-direct {v0, v1, v6, v2}, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;->DEFAULT:Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    .line 15
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    sget-object v1, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;->SHORT:Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;->MEDIUM:Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;->LONG:Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;->DEFAULT:Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;->$VALUES:[Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "endpointTimeWithSpeechMilliseconds"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 21
    iput p3, p0, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;->endpointTimeWithSpeechMilliseconds:I

    .line 22
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 15
    const-class v0, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;->$VALUES:[Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;

    return-object v0
.end method


# virtual methods
.method public getEndpointTimeWithSpeechMilliseconds()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/vlingo/core/internal/audio/EndpointTimeWithSpeech;->endpointTimeWithSpeechMilliseconds:I

    return v0
.end method
