.class final Lcom/vlingo/core/internal/vlservice/VlingoApplicationService$ServiceHandler;
.super Landroid/os/Handler;
.source "VlingoApplicationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService$ServiceHandler;->this$0:Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;

    .line 83
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 84
    return-void
.end method

.method private processIntent(Landroid/content/Intent;I)V
    .locals 22
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "intentId"    # I

    .prologue
    .line 99
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 101
    .local v3, "action":Ljava/lang/String;
    const-string/jumbo v20, "com.vlingo.client.app.action.ACTIVITY_STATE_CHANGED"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 103
    const-string/jumbo v20, "com.vlingo.client.app.extra.STATE"

    const/16 v21, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v19

    .line 108
    .local v19, "state":I
    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_1

    if-eqz v19, :cond_1

    .line 199
    .end local v19    # "state":I
    :cond_0
    :goto_0
    return-void

    .line 114
    .restart local v19    # "state":I
    :cond_1
    # getter for: Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->mLastState:I
    invoke-static {}, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->access$000()I

    move-result v20

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_2

    .line 117
    # invokes: Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->setLastState(I)V
    invoke-static/range {v19 .. v19}, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->access$100(I)V

    .line 120
    :cond_2
    # getter for: Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->mLastState:I
    invoke-static {}, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->access$000()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_3

    .line 123
    const/16 v20, 0x0

    invoke-static/range {v20 .. v20}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->considerRightBeforeForeground(Z)V

    goto :goto_0

    .line 127
    :cond_3
    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    const/16 v17, 0x1

    .line 128
    .local v17, "newStateIsForeground":Z
    :goto_1
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_2
    const/16 v20, 0x1e

    move/from16 v0, v20

    if-ge v14, v0, :cond_0

    .line 130
    const-wide/16 v20, 0x32

    :try_start_0
    invoke-static/range {v20 .. v21}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    :goto_3
    const/16 v20, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService$ServiceHandler;->hasMessages(I)Z

    move-result v20

    if-nez v20, :cond_0

    .line 141
    # invokes: Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->isAppInForegroundInternal()Z
    invoke-static {}, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->access$200()Z

    move-result v12

    .line 142
    .local v12, "currentStateIsForeground":Z
    move/from16 v0, v17

    if-ne v12, v0, :cond_6

    .line 143
    # invokes: Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->setLastState(I)V
    invoke-static/range {v19 .. v19}, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->access$100(I)V

    .line 144
    new-instance v6, Landroid/content/Intent;

    const-string/jumbo v20, "com.vlingo.client.app.action.APPLICATION_STATE_CHANGED"

    move-object/from16 v0, v20

    invoke-direct {v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 146
    .local v6, "broadcastIntent":Landroid/content/Intent;
    const-string/jumbo v21, "com.vlingo.client.app.extra.STATE"

    if-eqz v17, :cond_5

    const/16 v20, 0x1

    :goto_4
    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 147
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService$ServiceHandler;->this$0:Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->sendBroadcast(Landroid/content/Intent;)V

    .line 148
    if-nez v12, :cond_0

    .line 150
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService$ServiceHandler;->this$0:Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->stopSelf(I)V

    goto :goto_0

    .line 127
    .end local v6    # "broadcastIntent":Landroid/content/Intent;
    .end local v12    # "currentStateIsForeground":Z
    .end local v14    # "i":I
    .end local v17    # "newStateIsForeground":Z
    :cond_4
    const/16 v17, 0x0

    goto :goto_1

    .line 131
    .restart local v14    # "i":I
    .restart local v17    # "newStateIsForeground":Z
    :catch_0
    move-exception v13

    .line 132
    .local v13, "e":Ljava/lang/Exception;
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 146
    .end local v13    # "e":Ljava/lang/Exception;
    .restart local v6    # "broadcastIntent":Landroid/content/Intent;
    .restart local v12    # "currentStateIsForeground":Z
    :cond_5
    const/16 v20, 0x0

    goto :goto_4

    .line 128
    .end local v6    # "broadcastIntent":Landroid/content/Intent;
    :cond_6
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 160
    .end local v12    # "currentStateIsForeground":Z
    .end local v14    # "i":I
    .end local v17    # "newStateIsForeground":Z
    .end local v19    # "state":I
    :cond_7
    const-string/jumbo v20, "com.vlingo.client.app.action.CLOSE_APPLICATION"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 165
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    .line 166
    .local v11, "context":Landroid/content/Context;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 167
    .local v4, "activitiesToClose":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Class<*>;>;"
    const-string/jumbo v20, "activity"

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager;

    .line 168
    .local v5, "am":Landroid/app/ActivityManager;
    const/16 v20, 0x2

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v16

    .line 169
    .local v16, "listOfRti":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v16, :cond_9

    .line 171
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :cond_8
    :goto_5
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_9

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 172
    .local v18, "rti":Landroid/app/ActivityManager$RunningTaskInfo;
    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v8

    .line 173
    .local v8, "className":Ljava/lang/String;
    const-string/jumbo v20, "com.vlingo"

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 175
    :try_start_1
    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_5

    .line 177
    :catch_1
    move-exception v10

    .line 178
    .local v10, "cnfe":Ljava/lang/ClassNotFoundException;
    const-string/jumbo v20, "VlingoApplicationService"

    invoke-static {v10}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 189
    .end local v8    # "className":Ljava/lang/String;
    .end local v10    # "cnfe":Ljava/lang/ClassNotFoundException;
    .end local v15    # "i$":Ljava/util/Iterator;
    .end local v18    # "rti":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_9
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .restart local v15    # "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_0

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Class;

    .line 191
    .local v7, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9, v11, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 193
    .local v9, "closeIntent":Landroid/content/Intent;
    const/high16 v20, 0x24000000

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 194
    const/high16 v20, 0x10000000

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 195
    const-string/jumbo v20, "com.vlingo.core.internal.vlservice.FINISH_ACTIVITY"

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 196
    invoke-virtual {v11, v9}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_6
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 88
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    .line 90
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 93
    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, v0, v1}, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService$ServiceHandler;->processIntent(Landroid/content/Intent;I)V

    .line 95
    :cond_0
    return-void
.end method
