.class public Lcom/vlingo/core/internal/audio/AudioPlaybackService;
.super Landroid/app/Service;
.source "AudioPlaybackService.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/audio/AudioPlaybackService$ServiceStub;
    }
.end annotation


# static fields
.field private static isRunning:Z


# instance fields
.field private NumClients:I

.field private NumRequests:I

.field private final mBinder:Landroid/os/IBinder;

.field private player:Lcom/vlingo/core/internal/audio/AudioPlayer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->isRunning:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 25
    iput v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->NumClients:I

    .line 26
    iput v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->NumRequests:I

    .line 29
    new-instance v0, Lcom/vlingo/core/internal/audio/AudioPlaybackService$ServiceStub;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/audio/AudioPlaybackService$ServiceStub;-><init>(Lcom/vlingo/core/internal/audio/AudioPlaybackService;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->mBinder:Landroid/os/IBinder;

    .line 30
    return-void
.end method

.method public static isRunning()Z
    .locals 1

    .prologue
    .line 171
    sget-boolean v0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->isRunning:Z

    return v0
.end method

.method private static setRunning(Z)V
    .locals 0
    .param p0, "running"    # Z

    .prologue
    .line 59
    sput-boolean p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->isRunning:Z

    .line 60
    return-void
.end method

.method private stopServiceIfPossible()V
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->NumClients:I

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->NumRequests:I

    if-gtz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->player:Lcom/vlingo/core/internal/audio/AudioPlayer;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/AudioPlayer;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    invoke-virtual {p0}, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->stopSelf()V

    .line 38
    :cond_0
    return-void
.end method


# virtual methods
.method public isPlaying()Z
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->player:Lcom/vlingo/core/internal/audio/AudioPlayer;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/AudioPlayer;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 66
    iget v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->NumClients:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->NumClients:I

    .line 67
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 45
    new-instance v0, Lcom/vlingo/core/internal/audio/AudioPlayer;

    invoke-direct {v0, p0, p0}, Lcom/vlingo/core/internal/audio/AudioPlayer;-><init>(Landroid/content/Context;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->player:Lcom/vlingo/core/internal/audio/AudioPlayer;

    .line 46
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->setRunning(Z)V

    .line 47
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->player:Lcom/vlingo/core/internal/audio/AudioPlayer;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/AudioPlayer;->onDestroy()V

    .line 54
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 55
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->setRunning(Z)V

    .line 56
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 74
    iget v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->NumClients:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->NumClients:I

    .line 75
    return-void
.end method

.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 152
    iget v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->NumRequests:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->NumRequests:I

    .line 153
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->stopServiceIfPossible()V

    .line 154
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 157
    iget v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->NumRequests:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->NumRequests:I

    .line 158
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->stopServiceIfPossible()V

    .line 159
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 162
    iget v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->NumRequests:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->NumRequests:I

    .line 163
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->stopServiceIfPossible()V

    .line 164
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 167
    iget v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->NumRequests:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->NumRequests:I

    .line 168
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 90
    const/4 v0, 0x1

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 81
    iget v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->NumClients:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->NumClients:I

    .line 82
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->stopServiceIfPossible()V

    .line 83
    const/4 v0, 0x0

    return v0
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->player:Lcom/vlingo/core/internal/audio/AudioPlayer;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/AudioPlayer;->pause()V

    .line 129
    return-void
.end method

.method public play(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->player:Lcom/vlingo/core/internal/audio/AudioPlayer;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/audio/AudioPlayer;->play(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 133
    return-void
.end method

.method public play(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "listener"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->player:Lcom/vlingo/core/internal/audio/AudioPlayer;

    invoke-virtual {v0, p1, p2}, Lcom/vlingo/core/internal/audio/AudioPlayer;->play(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 137
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->player:Lcom/vlingo/core/internal/audio/AudioPlayer;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/AudioPlayer;->resume()V

    .line 141
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->player:Lcom/vlingo/core/internal/audio/AudioPlayer;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/AudioPlayer;->stop()V

    .line 145
    return-void
.end method
