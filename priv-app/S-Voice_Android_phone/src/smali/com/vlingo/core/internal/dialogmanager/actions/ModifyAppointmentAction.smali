.class public Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "ModifyAppointmentAction.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private modifiedEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

.field private origEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method


# virtual methods
.method protected execute()V
    .locals 4

    .prologue
    .line 36
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;->modifiedEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    if-eqz v1, :cond_0

    .line 38
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;->origEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;->modifiedEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-static {v1, v2, v3}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->updateEvent(Landroid/content/Context;Lcom/vlingo/core/internal/schedule/ScheduleEvent;Lcom/vlingo/core/internal/schedule/ScheduleEvent;)V

    .line 39
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V
    :try_end_0
    .catch Lcom/vlingo/core/internal/schedule/ScheduleUtilException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    :goto_0
    return-void

    .line 40
    :catch_0
    move-exception v0

    .line 41
    .local v0, "e":Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unable to modify event: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unable to modify event: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    .end local v0    # "e":Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    const-string/jumbo v2, "No event to update"

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public modified(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;
    .locals 0
    .param p1, "event"    # Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;->modifiedEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 31
    return-object p0
.end method

.method public original(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;
    .locals 0
    .param p1, "event"    # Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ModifyAppointmentAction;->origEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 26
    return-object p0
.end method
