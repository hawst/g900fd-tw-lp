.class Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;
.super Ljava/lang/Object;
.source "SparseArrayMap.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/SparseArrayMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EntryIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/util/Map$Entry",
        "<",
        "Ljava/lang/Integer;",
        "TT;>;>;"
    }
.end annotation


# instance fields
.field private position:I

.field final synthetic this$0:Lcom/vlingo/core/internal/util/SparseArrayMap;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/util/SparseArrayMap;)V
    .locals 1

    .prologue
    .line 69
    .local p0, "this":Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;, "Lcom/vlingo/core/internal/util/SparseArrayMap<TT;>.EntryIterator;"
    iput-object p1, p0, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;->this$0:Lcom/vlingo/core/internal/util/SparseArrayMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;->position:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/util/SparseArrayMap;Lcom/vlingo/core/internal/util/SparseArrayMap$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/util/SparseArrayMap;
    .param p2, "x1"    # Lcom/vlingo/core/internal/util/SparseArrayMap$1;

    .prologue
    .line 69
    .local p0, "this":Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;, "Lcom/vlingo/core/internal/util/SparseArrayMap<TT;>.EntryIterator;"
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;-><init>(Lcom/vlingo/core/internal/util/SparseArrayMap;)V

    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;

    .prologue
    .line 69
    iget v0, p0, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;->position:I

    return v0
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 74
    .local p0, "this":Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;, "Lcom/vlingo/core/internal/util/SparseArrayMap<TT;>.EntryIterator;"
    iget v0, p0, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;->position:I

    iget-object v1, p0, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;->this$0:Lcom/vlingo/core/internal/util/SparseArrayMap;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/SparseArrayMap;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 69
    .local p0, "this":Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;, "Lcom/vlingo/core/internal/util/SparseArrayMap<TT;>.EntryIterator;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;->next()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public next()Ljava/util/Map$Entry;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Integer;",
            "TT;>;"
        }
    .end annotation

    .prologue
    .line 79
    .local p0, "this":Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;, "Lcom/vlingo/core/internal/util/SparseArrayMap<TT;>.EntryIterator;"
    iget v0, p0, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;->position:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;->position:I

    .line 80
    iget v0, p0, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;->position:I

    iget-object v1, p0, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;->this$0:Lcom/vlingo/core/internal/util/SparseArrayMap;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/SparseArrayMap;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 81
    new-instance v0, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator$1;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator$1;-><init>(Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;)V

    return-object v0

    .line 100
    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 106
    .local p0, "this":Lcom/vlingo/core/internal/util/SparseArrayMap$EntryIterator;, "Lcom/vlingo/core/internal/util/SparseArrayMap<TT;>.EntryIterator;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "Remove is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
