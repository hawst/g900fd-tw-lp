.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;
.super Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
.source "PhraseSpotterControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "StoppingState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V
    .locals 0

    .prologue
    .line 326
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;-><init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)V

    return-void
.end method


# virtual methods
.method public onEnter()V
    .locals 4

    .prologue
    .line 362
    invoke-super {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->onEnter()V

    .line 363
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStateTransitionLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 364
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;
    invoke-static {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$700(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;

    move-result-object v0

    if-nez v0, :cond_0

    .line 365
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "  mRecordThread is null!"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mIdleState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    invoke-static {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1000(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v2

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V
    invoke-static {v0, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V

    .line 370
    :goto_0
    monitor-exit v1

    .line 371
    return-void

    .line 368
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "  Record thread state is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRecordThread:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;
    invoke-static {v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$700(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$RecordThread;->getState()Ljava/lang/Thread$State;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 370
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onExit()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 346
    invoke-super {p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->onExit()V

    .line 347
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->resumeControlOnPhraseSpotterStopped:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    invoke-static {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1100(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 348
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->resumeControlOnPhraseSpotterStopped:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    invoke-static {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1100(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/ResumeControl;->resume()V

    .line 349
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # setter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->resumeControlOnPhraseSpotterStopped:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    invoke-static {v1, v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1102(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .line 351
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getDialogFlowTaskRegulator()Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;

    move-result-object v0

    .line 352
    .local v0, "regulator":Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;
    if-eqz v0, :cond_1

    .line 353
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->RECOGNITION_START:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    invoke-virtual {v1, v2, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->unregisterTaskFlowRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V

    .line 354
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v1, v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setDialogFlowTaskRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V

    .line 356
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mListener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    invoke-static {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$300(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;->onPhraseSpotterStopped()V

    .line 357
    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mAudioLogger:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;
    invoke-static {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1200(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$AudioLoggerWrapper;->release()V

    .line 358
    return-void
.end method

.method public start(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V
    .locals 3
    .param p1, "phraseSpotterParams"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .param p2, "listener"    # Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    .prologue
    .line 330
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;->start(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V

    .line 331
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "  Attempting to start while in state \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\', will restart after fully stopped."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mStateTransitionLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$100(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 334
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    const/4 v2, 0x1

    # setter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mRestartWhenIdle:Z
    invoke-static {v0, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$002(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Z)Z

    .line 335
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->isInIdleState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "  mRecordThread is null."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$StoppingState;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->mIdleState:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;
    invoke-static {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$1000(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;

    move-result-object v2

    # invokes: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->setState(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V
    invoke-static {v0, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;->access$500(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterControl$SpotterState;)V

    .line 341
    :cond_0
    monitor-exit v1

    .line 342
    return-void

    .line 341
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
