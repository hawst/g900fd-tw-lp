.class public Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
.super Ljava/lang/Object;
.source "CMAWeatherAdaptor.java"

# interfaces
.implements Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$3;
    }
.end annotation


# static fields
.field private static final CMA_APP_ID:Ljava/lang/String;

.field private static final CMA_APP_ID_FULL:Ljava/lang/String;

.field private static final CMA_PRIVATE_KEY:Ljava/lang/String;


# instance fields
.field private date:Ljava/lang/String;

.field private isDatePlusSeven:Z

.field private isRequestComplete:Z

.field private isRequestFailed:Z

.field private isToday:Z

.field private lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

.field private weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

.field private widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    const-string/jumbo v0, "cma_private_key"

    const-string/jumbo v1, "sanx_data_99"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->CMA_PRIVATE_KEY:Ljava/lang/String;

    .line 37
    const-string/jumbo v0, "cma_app_id_full"

    const-string/jumbo v1, "f63d329270a44900"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->CMA_APP_ID_FULL:Ljava/lang/String;

    .line 38
    const-string/jumbo v0, "cma_app_id"

    const-string/jumbo v1, "f63d32"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->CMA_APP_ID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-boolean v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestComplete:Z

    .line 42
    iput-boolean v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestFailed:Z

    .line 43
    iput-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .line 49
    iput-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    .line 50
    iput-boolean v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestComplete:Z

    .line 51
    new-instance v0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    invoke-direct {v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    .line 52
    return-void
.end method

.method static synthetic access$002(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestComplete:Z

    return p1
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;)Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    return-object v0
.end method

.method static synthetic access$102(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;)Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
    .param p1, "x1"    # Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    return-object p1
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;)Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    return-object v0
.end method

.method static synthetic access$302(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestFailed:Z

    return p1
.end method

.method private getCurrentLocationURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "latitude"    # Ljava/lang/String;
    .param p2, "longitude"    # Ljava/lang/String;

    .prologue
    .line 246
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyyMMddhhmm"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 247
    .local v0, "formattedDate":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->CMA_APP_ID:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0, v2}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getLocationDataURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "&key="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->CMA_APP_ID_FULL:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0, v2}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getLocationDataURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->CMA_PRIVATE_KEY:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getHMACKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getDataURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "areaID"    # Ljava/lang/String;
    .param p2, "dateParam"    # Ljava/lang/String;
    .param p3, "appID"    # Ljava/lang/String;

    .prologue
    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "http://webapi.weather.com.cn/data/?areaid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "&type=forecast&date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "&appid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getDataURLLocation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "location"    # Ljava/lang/String;
    .param p2, "dateParam"    # Ljava/lang/String;
    .param p3, "appID"    # Ljava/lang/String;

    .prologue
    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "http://webapi.weather.com.cn/data/citysearch/?city="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "&type=observe&date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "&appid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getHMACKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "data"    # Ljava/lang/String;
    .param p1, "privateKey"    # Ljava/lang/String;

    .prologue
    .line 159
    const/4 v1, 0x0

    .line 161
    .local v1, "key":Ljava/lang/String;
    :try_start_0
    const-string/jumbo v3, "HmacSHA1"

    invoke-static {v3}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v2

    .line 162
    .local v2, "mac":Ljavax/crypto/Mac;
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    const-string/jumbo v5, "HmacSHA1"

    invoke-direct {v3, v4, v5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {v2, v3}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 163
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 169
    .end local v2    # "mac":Ljavax/crypto/Mac;
    :goto_0
    invoke-static {v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "%0A"

    const-string/jumbo v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 164
    :catch_0
    move-exception v0

    .line 165
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0

    .line 166
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v0

    .line 167
    .local v0, "e":Ljava/security/InvalidKeyException;
    invoke-virtual {v0}, Ljava/security/InvalidKeyException;->printStackTrace()V

    goto :goto_0
.end method

.method private getLocationDataURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "latitude"    # Ljava/lang/String;
    .param p2, "longitude"    # Ljava/lang/String;
    .param p3, "formattedDate"    # Ljava/lang/String;
    .param p4, "appId"    # Ljava/lang/String;

    .prologue
    .line 252
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "http://geo.weather.com.cn/ag9/?lon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "&lat="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "&date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "&appid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getURL(Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;)Ljava/lang/String;
    .locals 4
    .param p1, "cmaWeatherElement"    # Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    .prologue
    .line 149
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyyMMddhhmm"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iget-object v2, p1, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->lastUpdate:Ljava/util/Date;

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 150
    .local v0, "formattedDate":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->location:Lcom/vlingo/core/internal/weather/china/CMALocationElement;

    iget-object v2, v2, Lcom/vlingo/core/internal/weather/china/CMALocationElement;->areaID:Ljava/lang/String;

    sget-object v3, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->CMA_APP_ID:Ljava/lang/String;

    invoke-direct {p0, v2, v0, v3}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getDataURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "&key="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->location:Lcom/vlingo/core/internal/weather/china/CMALocationElement;

    iget-object v2, v2, Lcom/vlingo/core/internal/weather/china/CMALocationElement;->areaID:Ljava/lang/String;

    sget-object v3, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->CMA_APP_ID_FULL:Ljava/lang/String;

    invoke-direct {p0, v2, v0, v3}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getDataURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->CMA_PRIVATE_KEY:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getHMACKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getURLLocation(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 131
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyyMMddhhmm"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 132
    .local v0, "formattedDate":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->CMA_APP_ID:Ljava/lang/String;

    invoke-direct {p0, v2, v0, v3}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getDataURLLocation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "&key="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->CMA_APP_ID_FULL:Ljava/lang/String;

    invoke-direct {p0, v2, v0, v3}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getDataURLLocation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->CMA_PRIVATE_KEY:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getHMACKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private declared-synchronized setWeatherElement(Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;)V
    .locals 1
    .param p1, "weatherElement"    # Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    .prologue
    .line 334
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 335
    monitor-exit p0

    return-void

    .line 334
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public getCurrentTempForToday()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 401
    const/4 v0, 0x0

    .line 402
    .local v0, "currentTemp":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v1, v1, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherCurrent:Lcom/vlingo/core/internal/weather/china/WeatherCurrent;

    if-eqz v1, :cond_0

    .line 403
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v1, v1, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherCurrent:Lcom/vlingo/core/internal/weather/china/WeatherCurrent;

    iget-object v0, v1, Lcom/vlingo/core/internal/weather/china/WeatherCurrent;->temperature:Ljava/lang/String;

    .line 405
    :cond_0
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 406
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v1, v1, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/weather/china/WeatherItem;

    iget-object v1, v1, Lcom/vlingo/core/internal/weather/china/WeatherItem;->day:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v0, v1, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->temperature:Ljava/lang/String;

    .line 407
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 408
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v1, v1, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/weather/china/WeatherItem;

    iget-object v1, v1, Lcom/vlingo/core/internal/weather/china/WeatherItem;->night:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v0, v1, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->temperature:Ljava/lang/String;

    .line 411
    :cond_1
    return-object v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->date:Ljava/lang/String;

    return-object v0
.end method

.method public getLocationCity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->nameCity:Ljava/lang/String;

    return-object v0
.end method

.method public getLocationState()Ljava/lang/String;
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->nameProvince:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->nameProvince:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->nameProvince:Ljava/lang/String;

    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v1, v1, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->nameCity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 372
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->nameCountry:Ljava/lang/String;

    .line 374
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->nameProvince:Ljava/lang/String;

    goto :goto_0
.end method

.method public getMaxTempByDate(Ljava/lang/Integer;)Ljava/lang/String;
    .locals 3
    .param p1, "indexDay"    # Ljava/lang/Integer;

    .prologue
    .line 415
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v1, v1, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/weather/china/WeatherItem;

    iget-object v1, v1, Lcom/vlingo/core/internal/weather/china/WeatherItem;->day:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v0, v1, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->temperature:Ljava/lang/String;

    .line 416
    .local v0, "maxTemp":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 417
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v1, v1, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/weather/china/WeatherItem;

    iget-object v1, v1, Lcom/vlingo/core/internal/weather/china/WeatherItem;->night:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v0, v1, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->temperature:Ljava/lang/String;

    .line 419
    :cond_0
    return-object v0
.end method

.method public getWeatherCodeByDate(Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 3
    .param p1, "indexDay"    # Ljava/lang/Integer;

    .prologue
    .line 423
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v1, v1, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/weather/china/WeatherItem;

    iget-object v1, v1, Lcom/vlingo/core/internal/weather/china/WeatherItem;->day:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v0, v1, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->weatherCode:Ljava/lang/String;

    .line 424
    .local v0, "weatherCode":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 425
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v1, v1, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/weather/china/WeatherItem;

    iget-object v1, v1, Lcom/vlingo/core/internal/weather/china/WeatherItem;->night:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v0, v1, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->weatherCode:Ljava/lang/String;

    .line 426
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 427
    const-string/jumbo v0, "99"

    .line 435
    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public getWeatherElement()Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    return-object v0
.end method

.method public getWeatherPhenomenonByDate(Ljava/lang/Integer;)Ljava/lang/String;
    .locals 3
    .param p1, "indexDay"    # Ljava/lang/Integer;

    .prologue
    .line 439
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v1, v1, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/weather/china/WeatherItem;

    iget-object v1, v1, Lcom/vlingo/core/internal/weather/china/WeatherItem;->day:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v0, v1, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->weatherPhenomenon:Ljava/lang/String;

    .line 440
    .local v0, "weatherPhenomenon":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 441
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v1, v1, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/weather/china/WeatherItem;

    iget-object v1, v1, Lcom/vlingo/core/internal/weather/china/WeatherItem;->night:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v0, v1, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->weatherPhenomenon:Ljava/lang/String;

    .line 442
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 443
    const-string/jumbo v0, ""

    .line 446
    :cond_0
    return-object v0
.end method

.method public getWidgetListener()Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    return-object v0
.end method

.method public isDatePlusSeven()Z
    .locals 1

    .prologue
    .line 393
    iget-boolean v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isDatePlusSeven:Z

    return v0
.end method

.method public isRequestComplete()Z
    .locals 1

    .prologue
    .line 320
    iget-boolean v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestComplete:Z

    return v0
.end method

.method public isRequestFailed()Z
    .locals 1

    .prologue
    .line 327
    iget-boolean v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestFailed:Z

    return v0
.end method

.method public isToday()Z
    .locals 1

    .prologue
    .line 385
    iget-boolean v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isToday:Z

    return v0
.end method

.method public onRequestComplete(ZLjava/lang/Object;)V
    .locals 4
    .param p1, "success"    # Z
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    .line 260
    iput-boolean v3, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestComplete:Z

    .line 261
    if-eqz p1, :cond_0

    .line 265
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    if-eqz v2, :cond_3

    .line 266
    new-instance v1, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;

    iget-object v2, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    invoke-direct {v1, v2}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;-><init>(Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;)V

    .line 267
    .local v1, "parser":Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;
    check-cast p2, Ljava/lang/String;

    .end local p2    # "object":Ljava/lang/Object;
    invoke-virtual {v1, p2}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->parseForecasts(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 281
    .end local v1    # "parser":Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestFailed:Z

    if-eqz v2, :cond_4

    .line 284
    :cond_1
    iput-boolean v3, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestFailed:Z

    .line 285
    iget-object v2, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v2}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed()V

    .line 293
    :cond_2
    :goto_1
    return-void

    .line 269
    .restart local p2    # "object":Ljava/lang/Object;
    :cond_3
    :try_start_1
    new-instance v2, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    invoke-direct {v2}, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;-><init>()V

    iput-object v2, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    .line 270
    new-instance v1, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;

    iget-object v2, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    invoke-direct {v1, v2}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;-><init>(Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;)V

    .line 271
    .restart local v1    # "parser":Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;
    check-cast p2, Ljava/lang/String;

    .end local p2    # "object":Ljava/lang/Object;
    invoke-virtual {v1, p2}, Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;->parse(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 273
    .end local v1    # "parser":Lcom/vlingo/core/internal/weather/china/CMAWeatherParser;
    :catch_0
    move-exception v0

    .line 274
    .local v0, "e":Lorg/json/JSONException;
    iput-boolean v3, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestFailed:Z

    goto :goto_0

    .line 289
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_4
    iget-object v2, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    if-eqz v2, :cond_2

    .line 290
    iget-object v2, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v2}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onResponseReceived()V

    goto :goto_1
.end method

.method public onRequestFailed(Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;)V
    .locals 0
    .param p1, "failureType"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;

    .prologue
    .line 474
    return-void
.end method

.method public onRequestFailed(Ljava/lang/String;)V
    .locals 1
    .param p1, "failReason"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 297
    iput-boolean v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestComplete:Z

    .line 298
    iput-boolean v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestFailed:Z

    .line 300
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed()V

    .line 303
    :cond_0
    return-void
.end method

.method public onRequestScheduled()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 308
    iput-boolean v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestComplete:Z

    .line 309
    iput-boolean v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestFailed:Z

    .line 311
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestScheduled()V

    .line 314
    :cond_0
    return-void
.end method

.method protected sendRequestWithAreaID(Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;)V
    .locals 3
    .param p1, "cmaWeatherElement"    # Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getURL(Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;)Ljava/lang/String;

    move-result-object v0

    .line 145
    .local v0, "url":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    const-string/jumbo v2, "ChineseWeatherRequest-Request"

    invoke-virtual {v1, v2, v0, p0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sendRequest(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V

    .line 146
    return-void
.end method

.method protected sendRequestWithCurrentLocation()V
    .locals 6

    .prologue
    .line 176
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getUserLat()Ljava/lang/String;

    move-result-object v0

    .line 177
    .local v0, "latitude":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getUserLong()Ljava/lang/String;

    move-result-object v1

    .line 178
    .local v1, "longitude":Ljava/lang/String;
    const-string/jumbo v2, "0.0"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "0.0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 181
    iget-object v2, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    .line 243
    :goto_0
    return-void

    .line 185
    :cond_0
    iget-object v2, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    const-string/jumbo v3, "SearchLocationChineseWeather-Request"

    invoke-direct {p0, v0, v1}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getCurrentLocationURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;

    invoke-direct {v5, p0}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$2;-><init>(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;)V

    invoke-virtual {v2, v3, v4, v5}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sendRequest(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V

    goto :goto_0
.end method

.method public sendWeatherRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "dateParam"    # Ljava/lang/String;
    .param p2, "spokenDate"    # Ljava/lang/String;
    .param p3, "location"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 55
    iput-boolean v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestComplete:Z

    .line 56
    iput-boolean v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isRequestFailed:Z

    .line 57
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->date:Ljava/lang/String;

    .line 63
    invoke-static {p3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 64
    invoke-direct {p0, p3}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getURLLocation(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 65
    .local v0, "url":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    const-string/jumbo v2, "SearchCityChineseWeatherRequest-Request"

    new-instance v3, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$1;

    invoke-direct {v3, p0}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor$1;-><init>(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;)V

    invoke-virtual {v1, v2, v0, v3}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sendRequest(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V

    .line 128
    .end local v0    # "url":Ljava/lang/String;
    :goto_0
    return-void

    .line 126
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->sendRequestWithCurrentLocation()V

    goto :goto_0
.end method

.method public setDatePlusSeven(Z)V
    .locals 0
    .param p1, "datePlusSeven"    # Z

    .prologue
    .line 397
    iput-boolean p1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isDatePlusSeven:Z

    .line 398
    return-void
.end method

.method public setToday(Z)V
    .locals 0
    .param p1, "today"    # Z

    .prologue
    .line 389
    iput-boolean p1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isToday:Z

    .line 390
    return-void
.end method

.method public setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V
    .locals 0
    .param p1, "widgetListener"    # Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .prologue
    .line 348
    iput-object p1, p0, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .line 349
    return-void
.end method
