.class public Lcom/vlingo/core/internal/util/ServiceBindingUtil;
.super Ljava/lang/Object;
.source "ServiceBindingUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceBinder;,
        Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceToken;
    }
.end annotation


# static fields
.field private static sConnectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/Context;",
            "Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceBinder;",
            ">;"
        }
    .end annotation
.end field

.field private static sServiceMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/util/ServiceBindingUtil;->sServiceMap:Ljava/util/HashMap;

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/util/ServiceBindingUtil;->sConnectionMap:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    return-void
.end method

.method static synthetic access$000()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/vlingo/core/internal/util/ServiceBindingUtil;->sServiceMap:Ljava/util/HashMap;

    return-object v0
.end method

.method public static bindToService(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Class;Landroid/content/ServiceConnection;)Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceToken;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "serviceClass"    # Ljava/lang/Class;
    .param p2, "interfaceClass"    # Ljava/lang/Class;
    .param p3, "callback"    # Landroid/content/ServiceConnection;

    .prologue
    .line 37
    instance-of v3, p0, Landroid/app/Activity;

    if-eqz v3, :cond_0

    move-object v3, p0

    .line 38
    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getParent()Landroid/app/Activity;

    move-result-object v1

    .line 39
    .local v1, "realActivity":Landroid/app/Activity;
    if-eqz v1, :cond_0

    .line 40
    move-object p0, v1

    .line 43
    .end local v1    # "realActivity":Landroid/app/Activity;
    :cond_0
    new-instance v0, Landroid/content/ContextWrapper;

    invoke-direct {v0, p0}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 44
    .local v0, "cw":Landroid/content/ContextWrapper;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v3}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 45
    new-instance v2, Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceBinder;

    invoke-direct {v2, p3, p2}, Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceBinder;-><init>(Landroid/content/ServiceConnection;Ljava/lang/Class;)V

    .line 46
    .local v2, "sb":Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceBinder;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v3, v0, p1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v2, v4}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 47
    sget-object v3, Lcom/vlingo/core/internal/util/ServiceBindingUtil;->sConnectionMap:Ljava/util/HashMap;

    invoke-virtual {v3, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    new-instance v3, Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceToken;

    invoke-direct {v3, v0, p2}, Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceToken;-><init>(Landroid/content/ContextWrapper;Ljava/lang/Class;)V

    .line 52
    :goto_0
    return-object v3

    .line 51
    :cond_1
    const-string/jumbo v3, "ServiceBindingUtil"

    const-string/jumbo v4, "Failed to bind to service"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static unbindFromService(Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceToken;)V
    .locals 4
    .param p0, "token"    # Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceToken;

    .prologue
    .line 56
    if-nez p0, :cond_1

    .line 57
    const-string/jumbo v2, "ServiceBindingUtil"

    const-string/jumbo v3, "Trying to unbind with null token"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceToken;->mWrappedContext:Landroid/content/ContextWrapper;

    .line 63
    .local v0, "cw":Landroid/content/ContextWrapper;
    sget-object v2, Lcom/vlingo/core/internal/util/ServiceBindingUtil;->sConnectionMap:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceBinder;

    .line 64
    .local v1, "sb":Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceBinder;
    if-nez v1, :cond_2

    .line 65
    const-string/jumbo v2, "ServiceBindingUtil"

    const-string/jumbo v3, "Trying to unbind for unknown Context"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 68
    :cond_2
    invoke-virtual {v0, v1}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    .line 69
    sget-object v2, Lcom/vlingo/core/internal/util/ServiceBindingUtil;->sConnectionMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 74
    sget-object v2, Lcom/vlingo/core/internal/util/ServiceBindingUtil;->sServiceMap:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceToken;->interfaceClass:Ljava/lang/Class;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
