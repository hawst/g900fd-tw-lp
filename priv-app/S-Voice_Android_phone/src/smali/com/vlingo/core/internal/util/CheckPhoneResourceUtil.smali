.class public final Lcom/vlingo/core/internal/util/CheckPhoneResourceUtil;
.super Ljava/lang/Object;
.source "CheckPhoneResourceUtil.java"


# static fields
.field public static final SIZE_GB:J = 0x40000000L

.field public static final SIZE_KB:J = 0x400L

.field public static final SIZE_MB:J = 0x100000L

.field private static mCheckPhoneResourceUtil:Lcom/vlingo/core/internal/util/CheckPhoneResourceUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/util/CheckPhoneResourceUtil;->mCheckPhoneResourceUtil:Lcom/vlingo/core/internal/util/CheckPhoneResourceUtil;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    return-void
.end method

.method private getExternalAvailableSpaceInBytes()J
    .locals 8

    .prologue
    .line 32
    const-wide/16 v0, -0x1

    .line 34
    .local v0, "availableSpace":J
    :try_start_0
    new-instance v3, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 35
    .local v3, "stat":Landroid/os/StatFs;
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x12

    if-ge v4, v5, :cond_0

    .line 36
    invoke-virtual {v3}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v6, v6

    mul-long v0, v4, v6

    .line 46
    .end local v3    # "stat":Landroid/os/StatFs;
    :goto_0
    return-wide v0

    .line 38
    .restart local v3    # "stat":Landroid/os/StatFs;
    :cond_0
    invoke-virtual {v3}, Landroid/os/StatFs;->getAvailableBytes()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    .line 40
    .end local v3    # "stat":Landroid/os/StatFs;
    :catch_0
    move-exception v2

    .line 41
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getInstance()Lcom/vlingo/core/internal/util/CheckPhoneResourceUtil;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/vlingo/core/internal/util/CheckPhoneResourceUtil;->mCheckPhoneResourceUtil:Lcom/vlingo/core/internal/util/CheckPhoneResourceUtil;

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Lcom/vlingo/core/internal/util/CheckPhoneResourceUtil;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/CheckPhoneResourceUtil;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/util/CheckPhoneResourceUtil;->mCheckPhoneResourceUtil:Lcom/vlingo/core/internal/util/CheckPhoneResourceUtil;

    .line 28
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/util/CheckPhoneResourceUtil;->mCheckPhoneResourceUtil:Lcom/vlingo/core/internal/util/CheckPhoneResourceUtil;

    return-object v0
.end method


# virtual methods
.method public getExternalAvailableSpaceInGB()J
    .locals 4

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/CheckPhoneResourceUtil;->getExternalAvailableSpaceInBytes()J

    move-result-wide v0

    const-wide/32 v2, 0x40000000

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public getExternalAvailableSpaceInKB()J
    .locals 4

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/CheckPhoneResourceUtil;->getExternalAvailableSpaceInBytes()J

    move-result-wide v0

    const-wide/16 v2, 0x400

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public getExternalAvailableSpaceInMB()J
    .locals 4

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/CheckPhoneResourceUtil;->getExternalAvailableSpaceInBytes()J

    move-result-wide v0

    const-wide/32 v2, 0x100000

    div-long/2addr v0, v2

    return-wide v0
.end method
