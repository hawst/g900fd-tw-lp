.class Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;
.super Ljava/lang/Object;
.source "VlingoVoiceRecognitionService.java"

# interfaces
.implements Lcom/vlingo/core/internal/recognition/service/AndroidRecognitionStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RecognitionStateListenerImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;
    .param p2, "x1"    # Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$1;

    .prologue
    .line 280
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;-><init>(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)V

    return-void
.end method


# virtual methods
.method public onRecognitionEvent(Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;IILjava/lang/String;)V
    .locals 5
    .param p1, "manager"    # Lcom/vlingo/core/internal/recognition/AndroidRecognitionManager;
    .param p2, "state"    # I
    .param p3, "type"    # I
    .param p4, "message"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 285
    iget-object v2, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    monitor-enter v2

    .line 286
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->serviceCallbackListener:Landroid/speech/RecognitionService$Callback;
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$400(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)Landroid/speech/RecognitionService$Callback;

    move-result-object v1

    if-nez v1, :cond_0

    .line 287
    monitor-exit v2

    .line 424
    :goto_0
    return-void

    .line 290
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 423
    :cond_1
    :goto_1
    :sswitch_0
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 294
    :sswitch_1
    :try_start_1
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_state:I
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$200(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)I

    move-result v1

    if-eq v1, v3, :cond_1

    .line 295
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    const/4 v3, 0x1

    # setter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_state:I
    invoke-static {v1, v3}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$202(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 297
    :try_start_2
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->serviceCallbackListener:Landroid/speech/RecognitionService$Callback;
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$400(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)Landroid/speech/RecognitionService$Callback;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/speech/RecognitionService$Callback;->readyForSpeech(Landroid/os/Bundle;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 302
    :goto_2
    :try_start_3
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->serviceCallbackListener:Landroid/speech/RecognitionService$Callback;
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$400(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)Landroid/speech/RecognitionService$Callback;

    move-result-object v1

    invoke-virtual {v1}, Landroid/speech/RecognitionService$Callback;->beginningOfSpeech()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 303
    :catch_0
    move-exception v0

    .line 304
    .local v0, "ex":Landroid/os/RemoteException;
    :try_start_4
    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$500()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "VLG_EXCEPTION "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 298
    .end local v0    # "ex":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 299
    .restart local v0    # "ex":Landroid/os/RemoteException;
    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$500()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "VLG_EXCEPTION "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 319
    .end local v0    # "ex":Landroid/os/RemoteException;
    :sswitch_2
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_state:I
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$200(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 320
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    const/4 v3, 0x2

    # setter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_state:I
    invoke-static {v1, v3}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$202(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;I)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 322
    :try_start_5
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->serviceCallbackListener:Landroid/speech/RecognitionService$Callback;
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$400(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)Landroid/speech/RecognitionService$Callback;

    move-result-object v1

    invoke-virtual {v1}, Landroid/speech/RecognitionService$Callback;->endOfSpeech()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 323
    :catch_2
    move-exception v0

    .line 324
    .restart local v0    # "ex":Landroid/os/RemoteException;
    :try_start_6
    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$500()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "VLG_EXCEPTION "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 348
    .end local v0    # "ex":Landroid/os/RemoteException;
    :sswitch_3
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_state:I
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$200(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 350
    :try_start_7
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->serviceCallbackListener:Landroid/speech/RecognitionService$Callback;
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$400(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)Landroid/speech/RecognitionService$Callback;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/speech/RecognitionService$Callback;->error(I)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 354
    :goto_3
    :try_start_8
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # invokes: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->endRecognition()V
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$300(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)V

    goto/16 :goto_1

    .line 351
    :catch_3
    move-exception v0

    .line 352
    .restart local v0    # "ex":Landroid/os/RemoteException;
    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$500()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "VLG_EXCEPTION "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 361
    .end local v0    # "ex":Landroid/os/RemoteException;
    :sswitch_4
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_state:I
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$200(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 363
    :try_start_9
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->serviceCallbackListener:Landroid/speech/RecognitionService$Callback;
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$400(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)Landroid/speech/RecognitionService$Callback;

    move-result-object v1

    const/4 v3, 0x6

    invoke-virtual {v1, v3}, Landroid/speech/RecognitionService$Callback;->error(I)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 367
    :goto_4
    :try_start_a
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # invokes: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->endRecognition()V
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$300(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)V

    goto/16 :goto_1

    .line 364
    :catch_4
    move-exception v0

    .line 365
    .restart local v0    # "ex":Landroid/os/RemoteException;
    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$500()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "VLG_EXCEPTION "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 373
    .end local v0    # "ex":Landroid/os/RemoteException;
    :sswitch_5
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_state:I
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$200(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 375
    :try_start_b
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->serviceCallbackListener:Landroid/speech/RecognitionService$Callback;
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$400(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)Landroid/speech/RecognitionService$Callback;

    move-result-object v1

    const/4 v3, 0x7

    invoke-virtual {v1, v3}, Landroid/speech/RecognitionService$Callback;->error(I)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_5
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 379
    :goto_5
    :try_start_c
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # invokes: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->endRecognition()V
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$300(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)V

    goto/16 :goto_1

    .line 376
    :catch_5
    move-exception v0

    .line 377
    .restart local v0    # "ex":Landroid/os/RemoteException;
    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$500()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "VLG_EXCEPTION "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 385
    .end local v0    # "ex":Landroid/os/RemoteException;
    :sswitch_6
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_state:I
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$200(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 387
    :try_start_d
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->serviceCallbackListener:Landroid/speech/RecognitionService$Callback;
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$400(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)Landroid/speech/RecognitionService$Callback;

    move-result-object v1

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Landroid/speech/RecognitionService$Callback;->error(I)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_d} :catch_6
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 391
    :goto_6
    :try_start_e
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # invokes: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->endRecognition()V
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$300(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)V

    goto/16 :goto_1

    .line 388
    :catch_6
    move-exception v0

    .line 389
    .restart local v0    # "ex":Landroid/os/RemoteException;
    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$500()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "VLG_EXCEPTION "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 397
    .end local v0    # "ex":Landroid/os/RemoteException;
    :sswitch_7
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_state:I
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$200(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 399
    :try_start_f
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->serviceCallbackListener:Landroid/speech/RecognitionService$Callback;
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$400(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)Landroid/speech/RecognitionService$Callback;

    move-result-object v1

    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Landroid/speech/RecognitionService$Callback;->error(I)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_f} :catch_7
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 403
    :goto_7
    :try_start_10
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # invokes: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->endRecognition()V
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$300(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)V

    goto/16 :goto_1

    .line 400
    :catch_7
    move-exception v0

    .line 401
    .restart local v0    # "ex":Landroid/os/RemoteException;
    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$500()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "VLG_EXCEPTION "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 409
    .end local v0    # "ex":Landroid/os/RemoteException;
    :sswitch_8
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->m_state:I
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$200(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)I
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 411
    :try_start_11
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->serviceCallbackListener:Landroid/speech/RecognitionService$Callback;
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$400(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)Landroid/speech/RecognitionService$Callback;

    move-result-object v1

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/speech/RecognitionService$Callback;->error(I)V
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_11} :catch_8
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    .line 415
    :goto_8
    :try_start_12
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService$RecognitionStateListenerImpl;->this$0:Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;

    # invokes: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->endRecognition()V
    invoke-static {v1}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$300(Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;)V

    goto/16 :goto_1

    .line 412
    :catch_8
    move-exception v0

    .line 413
    .restart local v0    # "ex":Landroid/os/RemoteException;
    # getter for: Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/recognition/service/VlingoVoiceRecognitionService;->access$500()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "VLG_EXCEPTION "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    goto :goto_8

    .line 290
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
        0x66 -> :sswitch_0
        0x67 -> :sswitch_2
        0x68 -> :sswitch_0
        0x69 -> :sswitch_8
        0x6a -> :sswitch_6
        0x6b -> :sswitch_3
        0x6c -> :sswitch_7
        0x6d -> :sswitch_5
        0x6e -> :sswitch_4
        0x6f -> :sswitch_0
        0x70 -> :sswitch_0
        0xc8 -> :sswitch_0
    .end sparse-switch
.end method
