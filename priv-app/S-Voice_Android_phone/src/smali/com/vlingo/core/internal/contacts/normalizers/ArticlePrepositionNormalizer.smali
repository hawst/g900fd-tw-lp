.class public Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;
.super Lcom/vlingo/core/internal/contacts/normalizers/RegexContactNameNormalizer;
.source "ArticlePrepositionNormalizer.java"


# static fields
.field private static final PATTERNS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/util/regex/Pattern;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static canNormalizePattern:Ljava/util/regex/Pattern;

.field private static canNormalizeStr:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11
    sput-object v0, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->canNormalizeStr:Ljava/lang/StringBuilder;

    .line 12
    sput-object v0, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->canNormalizePattern:Ljava/util/regex/Pattern;

    .line 14
    new-instance v0, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer$1;

    invoke-direct {v0}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer$1;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->PATTERNS:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/normalizers/RegexContactNameNormalizer;-><init>()V

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)Ljava/util/regex/Pattern;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 9
    invoke-static {p0}, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    return-object v0
.end method

.method private static createPattern(Ljava/lang/String;)Ljava/util/regex/Pattern;
    .locals 2
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 43
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->canNormalizeStr:Ljava/lang/StringBuilder;

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->canNormalizeStr:Ljava/lang/StringBuilder;

    .line 48
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "\\b"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\\b\\s?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    return-object v0

    .line 46
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->canNormalizeStr:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "(\\b"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\\b\\s?)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->canNormalizeStr:Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method public canNormalize(Ljava/lang/String;)Z
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 32
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "fr-FR"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_0

    .line 39
    :goto_0
    return v1

    .line 35
    :catch_0
    move-exception v0

    .line 37
    .local v0, "e":Ljava/lang/NullPointerException;
    goto :goto_0

    .line 39
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_0
    invoke-super {p0, p1}, Lcom/vlingo/core/internal/contacts/normalizers/RegexContactNameNormalizer;->canNormalize(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method protected getCanNormalizePattern()Ljava/util/regex/Pattern;
    .locals 2

    .prologue
    .line 58
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->canNormalizePattern:Ljava/util/regex/Pattern;

    if-nez v0, :cond_0

    .line 59
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->canNormalizeStr:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->canNormalizePattern:Ljava/util/regex/Pattern;

    .line 61
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->canNormalizePattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method protected getPatterns()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/util/regex/Pattern;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    sget-object v0, Lcom/vlingo/core/internal/contacts/normalizers/ArticlePrepositionNormalizer;->PATTERNS:Ljava/util/Map;

    return-object v0
.end method
