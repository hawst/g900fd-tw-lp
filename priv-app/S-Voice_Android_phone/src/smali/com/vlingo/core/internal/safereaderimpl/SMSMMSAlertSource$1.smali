.class Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$1;
.super Landroid/os/Handler;
.source "SMSMMSAlertSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$1;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 121
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTutorialMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 126
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$1;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    iget-object v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$1;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    # getter for: Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->safeReaderMMSAlertChangeListener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;
    invoke-static {v2}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->access$100(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;)Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

    move-result-object v2

    # setter for: Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->safeReaderAlertChangeListener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->access$002(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;)Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

    .line 131
    :goto_1
    # getter for: Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "[MMS readout] MMSReceivingBroadcastReceiver"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getNewSafereaderAlerts(IZ)Ljava/util/LinkedList;

    move-result-object v0

    .line 135
    .local v0, "smsmmsAlerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 138
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$1;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    # getter for: Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->safeReaderAlertChangeListener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;
    invoke-static {v1}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->access$000(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;)Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 139
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$1;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    # getter for: Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->safeReaderAlertChangeListener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;
    invoke-static {v1}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->access$000(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;)Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;->onNewSafeReaderAlert(Ljava/util/LinkedList;)V

    .line 142
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isViewCoverOpened()Z

    move-result v1

    if-nez v1, :cond_0

    .line 143
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->showViewCoverUi()V

    goto :goto_0

    .line 128
    .end local v0    # "smsmmsAlerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    :cond_3
    iget-object v1, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$1;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    iget-object v2, p0, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource$1;->this$0:Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;

    # getter for: Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->safeReaderSMSAlertChangeListener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;
    invoke-static {v2}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->access$200(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;)Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

    move-result-object v2

    # setter for: Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->safeReaderAlertChangeListener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;->access$002(Lcom/vlingo/core/internal/safereaderimpl/SMSMMSAlertSource;Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;)Lcom/vlingo/core/internal/safereader/ISafeReaderAlertChangeListener;

    goto :goto_1
.end method
