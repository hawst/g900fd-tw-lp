.class public interface abstract Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;
.super Ljava/lang/Object;
.source "SendEmailInterface.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;


# virtual methods
.method public abstract contact(Lcom/vlingo/core/internal/contacts/ContactData;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;
.end method

.method public abstract contacts(Ljava/util/List;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)",
            "Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;"
        }
    .end annotation
.end method

.method public abstract message(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;
.end method

.method public abstract subject(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;
.end method
