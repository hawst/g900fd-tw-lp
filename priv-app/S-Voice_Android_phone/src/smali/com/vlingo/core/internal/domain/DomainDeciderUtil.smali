.class public Lcom/vlingo/core/internal/domain/DomainDeciderUtil;
.super Ljava/lang/Object;
.source "DomainDeciderUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/domain/DomainDecider;


# static fields
.field private static final ACTIONNAME_TO_DOMAIN_MAPPING:Ljava/lang/String;

.field private static final FIELD_ID_TO_DOMAIN_MAPPING:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    const-string/jumbo v0, "DECIDER_FIELDID_TO_DOMAIN_MAPPING"

    const-string/jumbo v1, "dm_dial_contact=voicedial,dm_dial_contact_disambig=voicedial,dm_dial_type=voicedial"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/domain/DomainDeciderUtil;->FIELD_ID_TO_DOMAIN_MAPPING:Ljava/lang/String;

    .line 24
    const-string/jumbo v0, "DECIDER_ACTIONNAME_TO_DOMAIN_MAPPING"

    const-string/jumbo v1, "ShowCallWidget=voicedial"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/domain/DomainDeciderUtil;->ACTIONNAME_TO_DOMAIN_MAPPING:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getDomainMap(Ljava/lang/String;)Ljava/util/Map;
    .locals 8
    .param p1, "mapping"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    :try_start_0
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 68
    .local v4, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v6, ","

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v5, v0, v1

    .line 69
    .local v5, "pair":Ljava/lang/String;
    const-string/jumbo v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 70
    .local v2, "keyValue":[Ljava/lang/String;
    const/4 v6, 0x0

    aget-object v6, v2, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    aget-object v7, v2, v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 73
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "keyValue":[Ljava/lang/String;
    .end local v3    # "len$":I
    .end local v4    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5    # "pair":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 77
    const/4 v4, 0x0

    :cond_0
    return-object v4
.end method


# virtual methods
.method public getDomain(Ljava/util/List;)Lcom/vlingo/core/internal/domain/DomainName;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            ">;)",
            "Lcom/vlingo/core/internal/domain/DomainName;"
        }
    .end annotation

    .prologue
    .local p1, "actionList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/VLAction;>;"
    const/4 v7, 0x0

    const/4 v10, 0x0

    .line 30
    sget-object v8, Lcom/vlingo/core/internal/domain/DomainDeciderUtil;->FIELD_ID_TO_DOMAIN_MAPPING:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/vlingo/core/internal/domain/DomainDeciderUtil;->getDomainMap(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v5

    .line 31
    .local v5, "fieldIdToDomainMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v8, Lcom/vlingo/core/internal/domain/DomainDeciderUtil;->ACTIONNAME_TO_DOMAIN_MAPPING:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/vlingo/core/internal/domain/DomainDeciderUtil;->getDomainMap(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 33
    .local v1, "actionNameToDomainMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v5, :cond_0

    if-nez v1, :cond_1

    .line 62
    :cond_0
    :goto_0
    return-object v7

    .line 37
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/recognition/VLAction;

    .line 38
    .local v0, "action":Lcom/vlingo/sdk/recognition/VLAction;
    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "SetTurnParams"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 39
    const-string/jumbo v8, "fieldid"

    invoke-static {v0, v8, v10}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 40
    .local v4, "fieldId":Ljava/lang/String;
    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 41
    .local v3, "domain":Ljava/lang/String;
    if-eqz v3, :cond_3

    .line 42
    invoke-static {v3}, Lcom/vlingo/core/internal/domain/DomainName;->get(Ljava/lang/String;)Lcom/vlingo/core/internal/domain/DomainName;

    move-result-object v7

    goto :goto_0

    .line 46
    .end local v3    # "domain":Ljava/lang/String;
    .end local v4    # "fieldId":Ljava/lang/String;
    :cond_3
    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "ResolveContact"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 47
    const-string/jumbo v8, "capability"

    invoke-static {v0, v8, v10}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 48
    .local v2, "capability":Ljava/lang/String;
    if-eqz v2, :cond_5

    .line 49
    const-string/jumbo v8, "call"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 50
    sget-object v7, Lcom/vlingo/core/internal/domain/DomainName;->VOICEDIAL:Lcom/vlingo/core/internal/domain/DomainName;

    goto :goto_0

    .line 51
    :cond_4
    const-string/jumbo v8, "sms"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 57
    .end local v2    # "capability":Ljava/lang/String;
    :cond_5
    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 58
    .restart local v3    # "domain":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 59
    invoke-static {v3}, Lcom/vlingo/core/internal/domain/DomainName;->get(Ljava/lang/String;)Lcom/vlingo/core/internal/domain/DomainName;

    move-result-object v7

    goto :goto_0
.end method
