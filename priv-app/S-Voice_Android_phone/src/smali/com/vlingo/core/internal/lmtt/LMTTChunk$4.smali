.class synthetic Lcom/vlingo/core/internal/lmtt/LMTTChunk$4;
.super Ljava/lang/Object;
.source "LMTTChunk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/lmtt/LMTTChunk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$vlingo$core$internal$lmtt$LMTTItem$ChangeType:[I

.field static final synthetic $SwitchMap$com$vlingo$core$internal$lmtt$LMTTItem$LmttItemType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 168
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->values()[Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTChunk$4;->$SwitchMap$com$vlingo$core$internal$lmtt$LMTTItem$ChangeType:[I

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/lmtt/LMTTChunk$4;->$SwitchMap$com$vlingo$core$internal$lmtt$LMTTItem$ChangeType:[I

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->DELETE:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_0
    :try_start_1
    sget-object v0, Lcom/vlingo/core/internal/lmtt/LMTTChunk$4;->$SwitchMap$com$vlingo$core$internal$lmtt$LMTTItem$ChangeType:[I

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->INSERT:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_1
    :try_start_2
    sget-object v0, Lcom/vlingo/core/internal/lmtt/LMTTChunk$4;->$SwitchMap$com$vlingo$core$internal$lmtt$LMTTItem$ChangeType:[I

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->UPDATE:Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/lmtt/LMTTItem$ChangeType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    .line 170
    :goto_2
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->values()[Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/vlingo/core/internal/lmtt/LMTTChunk$4;->$SwitchMap$com$vlingo$core$internal$lmtt$LMTTItem$LmttItemType:[I

    :try_start_3
    sget-object v0, Lcom/vlingo/core/internal/lmtt/LMTTChunk$4;->$SwitchMap$com$vlingo$core$internal$lmtt$LMTTItem$LmttItemType:[I

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->TYPE_CONTACT:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_3
    :try_start_4
    sget-object v0, Lcom/vlingo/core/internal/lmtt/LMTTChunk$4;->$SwitchMap$com$vlingo$core$internal$lmtt$LMTTItem$LmttItemType:[I

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->TYPE_SONG:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_4
    :try_start_5
    sget-object v0, Lcom/vlingo/core/internal/lmtt/LMTTChunk$4;->$SwitchMap$com$vlingo$core$internal$lmtt$LMTTItem$LmttItemType:[I

    sget-object v1, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->TYPE_PLAYLIST:Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/lmtt/LMTTItem$LmttItemType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_5
    return-void

    :catch_0
    move-exception v0

    goto :goto_5

    :catch_1
    move-exception v0

    goto :goto_4

    :catch_2
    move-exception v0

    goto :goto_3

    .line 168
    :catch_3
    move-exception v0

    goto :goto_2

    :catch_4
    move-exception v0

    goto :goto_1

    :catch_5
    move-exception v0

    goto :goto_0
.end method
