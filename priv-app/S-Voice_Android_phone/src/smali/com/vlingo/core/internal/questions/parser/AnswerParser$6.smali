.class Lcom/vlingo/core/internal/questions/parser/AnswerParser$6;
.super Ljava/lang/Object;
.source "AnswerParser.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/questions/parser/AnswerParser;->loadImage(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/questions/parser/AnswerParser;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/questions/parser/AnswerParser;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$6;->this$0:Lcom/vlingo/core/internal/questions/parser/AnswerParser;

    iput-object p2, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$6;->val$url:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Landroid/graphics/drawable/Drawable;
    .locals 8

    .prologue
    .line 362
    const/4 v2, 0x0

    .line 369
    .local v2, "d":Landroid/graphics/drawable/Drawable;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/HttpKeepAlive;->on()Lcom/vlingo/core/internal/util/HttpKeepAlive;

    .line 370
    new-instance v4, Ljava/net/URL;

    iget-object v5, p0, Lcom/vlingo/core/internal/questions/parser/AnswerParser$6;->val$url:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 371
    .local v4, "theURL":Ljava/net/URL;
    invoke-virtual {v4}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    .line 372
    .local v1, "connection":Ljava/net/URLConnection;
    invoke-virtual {v1}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    .line 373
    .local v3, "is":Ljava/io/InputStream;
    const-string/jumbo v5, "src"

    invoke-static {v3, v5}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 374
    move-object v0, v2

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    move-object v5, v0

    invoke-virtual {v5}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    const/16 v6, 0xa0

    invoke-virtual {v5, v6}, Landroid/graphics/Bitmap;->setDensity(I)V

    .line 375
    move-object v0, v2

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    move-object v5, v0

    const/high16 v6, 0x43200000    # 160.0f

    invoke-static {}, Lcom/vlingo/sdk/internal/util/Screen;->getMagnification()F

    move-result v7

    mul-float/2addr v6, v7

    float-to-int v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/BitmapDrawable;->setTargetDensity(I)V

    .line 376
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 381
    .end local v1    # "connection":Ljava/net/URLConnection;
    .end local v3    # "is":Ljava/io/InputStream;
    .end local v4    # "theURL":Ljava/net/URL;
    :goto_0
    return-object v2

    .line 377
    :catch_0
    move-exception v5

    goto :goto_0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 360
    invoke-virtual {p0}, Lcom/vlingo/core/internal/questions/parser/AnswerParser$6;->call()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
