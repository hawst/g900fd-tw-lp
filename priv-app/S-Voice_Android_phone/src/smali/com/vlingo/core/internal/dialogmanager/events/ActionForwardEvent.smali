.class public Lcom/vlingo/core/internal/dialogmanager/events/ActionForwardEvent;
.super Lcom/vlingo/core/internal/dialogmanager/DialogEvent;
.source "ActionForwardEvent.java"


# instance fields
.field private final FIELDID_FIELD_NAME:Ljava/lang/String;

.field private final ID_FIELD_NAME:Ljava/lang/String;

.field private final NAME:Ljava/lang/String;

.field private final SENDER_NAME_FIELD_NAME:Ljava/lang/String;

.field private final SENDER_PHONE_FIELD_NAME:Ljava/lang/String;

.field private fieldId:Ljava/lang/String;

.field private id:J

.field private senderName:Ljava/lang/String;

.field private senderPhone:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # J
    .param p3, "senderName"    # Ljava/lang/String;
    .param p4, "senderPhone"    # Ljava/lang/String;
    .param p5, "fieldId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;-><init>(ZZZ)V

    .line 12
    const-string/jumbo v0, "action-forward"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionForwardEvent;->NAME:Ljava/lang/String;

    .line 13
    const-string/jumbo v0, "id"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionForwardEvent;->ID_FIELD_NAME:Ljava/lang/String;

    .line 14
    const-string/jumbo v0, "sender.name"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionForwardEvent;->SENDER_NAME_FIELD_NAME:Ljava/lang/String;

    .line 15
    const-string/jumbo v0, "sender.phone"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionForwardEvent;->SENDER_PHONE_FIELD_NAME:Ljava/lang/String;

    .line 16
    const-string/jumbo v0, "fieldid"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionForwardEvent;->FIELDID_FIELD_NAME:Ljava/lang/String;

    .line 26
    iput-wide p1, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionForwardEvent;->id:J

    .line 27
    iput-object p3, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionForwardEvent;->senderName:Ljava/lang/String;

    .line 28
    iput-object p4, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionForwardEvent;->senderPhone:Ljava/lang/String;

    .line 29
    iput-object p5, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionForwardEvent;->fieldId:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    const-string/jumbo v0, "action-forward"

    return-object v0
.end method

.method public getVLDialogEvent()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    .locals 4

    .prologue
    .line 39
    new-instance v0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    const-string/jumbo v1, "action-forward"

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;-><init>(Ljava/lang/String;)V

    .line 40
    .local v0, "builder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;
    const-string/jumbo v1, "id"

    iget-wide v2, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionForwardEvent;->id:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 41
    const-string/jumbo v1, "sender.name"

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionForwardEvent;->senderName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 42
    const-string/jumbo v1, "sender.phone"

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionForwardEvent;->senderPhone:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 43
    const-string/jumbo v1, "fieldid"

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/events/ActionForwardEvent;->fieldId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 45
    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;

    move-result-object v1

    return-object v1
.end method
