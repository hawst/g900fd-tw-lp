.class public Lcom/vlingo/core/internal/util/ContactUtil;
.super Ljava/lang/Object;
.source "ContactUtil.java"


# static fields
.field private static final MAX_RECENTS:I = 0xf

.field private static final MAX_STARRED:I = 0x1e

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/vlingo/core/internal/util/ContactUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/util/ContactUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getContactDisplayNameByAddress(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 154
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 157
    .local v1, "uri":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v0, "display_name"

    aput-object v0, v2, v4

    .line 160
    .local v2, "projection":[Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBUtilManager;->getContactDBUtil()Lcom/vlingo/core/internal/contacts/IContactDBUtil;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/vlingo/core/internal/contacts/IContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 161
    const-string/jumbo p1, ""

    .line 179
    .end local p1    # "address":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 164
    .restart local p1    # "address":Ljava/lang/String;
    :cond_1
    const/4 v6, 0x0

    .line 166
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 167
    if-eqz v6, :cond_2

    .line 168
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 169
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object p1

    .line 177
    .end local p1    # "address":Ljava/lang/String;
    if-eqz v6, :cond_0

    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    .restart local p1    # "address":Ljava/lang/String;
    :cond_2
    if-eqz v6, :cond_0

    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    .line 173
    :catch_2
    move-exception v7

    .line 174
    .local v7, "ex":Ljava/lang/Exception;
    :try_start_3
    sget-object v0, Lcom/vlingo/core/internal/util/ContactUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 177
    if-eqz v6, :cond_0

    :try_start_4
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_0

    :catch_3
    move-exception v0

    goto :goto_0

    .end local v7    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    :try_start_5
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    :cond_3
    :goto_1
    throw v0

    :catch_4
    move-exception v3

    goto :goto_1
.end method

.method public static getContactFullName(JLandroid/content/Context;)Ljava/lang/String;
    .locals 9
    .param p0, "contactId"    # J
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 25
    const/4 v6, 0x0

    .line 26
    .local v6, "cur":Landroid/database/Cursor;
    const-wide/16 v4, 0x0

    cmp-long v0, p0, v4

    if-eqz v0, :cond_1

    .line 28
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBUtilManager;->getContactDBUtil()Lcom/vlingo/core/internal/contacts/IContactDBUtil;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/contacts/IContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 29
    const-string/jumbo v7, ""

    .line 48
    :goto_0
    return-object v7

    .line 31
    :cond_0
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p0, p1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 32
    .local v1, "contactUri":Landroid/net/Uri;
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "display_name"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 34
    .end local v1    # "contactUri":Landroid/net/Uri;
    :cond_1
    if-eqz v6, :cond_3

    .line 36
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 37
    const-string/jumbo v0, "display_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 38
    .local v8, "nameColumn":I
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 39
    .local v7, "name":Ljava/lang/String;
    if-eqz v7, :cond_2

    .line 40
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .end local v7    # "name":Ljava/lang/String;
    .end local v8    # "nameColumn":I
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 48
    :cond_3
    const-string/jumbo v7, ""

    goto :goto_0

    .line 45
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static getContactFullNameFromPhoneNumber(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 9
    .param p0, "address"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 208
    const/4 v6, 0x0

    .line 209
    .local v6, "cur":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 210
    .local v7, "name":Ljava/lang/String;
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 212
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBUtilManager;->getContactDBUtil()Lcom/vlingo/core/internal/contacts/IContactDBUtil;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/contacts/IContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 213
    const-string/jumbo v0, ""

    .line 234
    :goto_0
    return-object v0

    .line 215
    :cond_0
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {v0, p0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 216
    .local v1, "contactUri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "display_name"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 218
    .end local v1    # "contactUri":Landroid/net/Uri;
    :cond_1
    invoke-static {v6}, Lcom/vlingo/core/internal/util/CursorUtil;->isValid(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 220
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 221
    const-string/jumbo v0, "display_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 222
    .local v8, "nameColumn":I
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 225
    .end local v8    # "nameColumn":I
    :cond_2
    if-eqz v6, :cond_3

    .line 226
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 231
    :cond_3
    if-nez v7, :cond_4

    .line 232
    const-string/jumbo v7, ""

    :cond_4
    move-object v0, v7

    .line 234
    goto :goto_0

    .line 225
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_5

    .line 226
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method public static getLastOutgoingCall(Landroid/content/Context;)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x0

    .line 183
    const/4 v7, 0x0

    .line 185
    .local v7, "cur":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    .line 186
    .local v1, "contacts":Landroid/net/Uri;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v4, "_id"

    aput-object v4, v2, v0

    const/4 v0, 0x1

    const-string/jumbo v4, "number"

    aput-object v4, v2, v0

    .line 187
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v3, "type=2"

    .line 188
    .local v3, "where":Ljava/lang/String;
    const-string/jumbo v5, "date DESC"

    .line 189
    .local v5, "orderBy":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 190
    if-eqz v7, :cond_3

    .line 191
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 192
    const-string/jumbo v0, "number"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 194
    .local v8, "numberColumn":I
    :cond_0
    invoke-interface {v7, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 195
    .local v6, "address":Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 203
    if-eqz v7, :cond_1

    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 204
    .end local v1    # "contacts":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "where":Ljava/lang/String;
    .end local v5    # "orderBy":Ljava/lang/String;
    .end local v6    # "address":Ljava/lang/String;
    .end local v8    # "numberColumn":I
    :cond_1
    :goto_0
    return-object v6

    .line 198
    .restart local v1    # "contacts":Landroid/net/Uri;
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v3    # "where":Ljava/lang/String;
    .restart local v5    # "orderBy":Ljava/lang/String;
    .restart local v6    # "address":Ljava/lang/String;
    .restart local v8    # "numberColumn":I
    :cond_2
    :try_start_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 203
    .end local v6    # "address":Ljava/lang/String;
    .end local v8    # "numberColumn":I
    :cond_3
    if-eqz v7, :cond_4

    :try_start_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .end local v1    # "contacts":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "where":Ljava/lang/String;
    .end local v5    # "orderBy":Ljava/lang/String;
    :cond_4
    :goto_1
    move-object v6, v9

    .line 204
    goto :goto_0

    .line 201
    :catch_0
    move-exception v0

    .line 203
    if-eqz v7, :cond_4

    :try_start_4
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_1

    :catchall_0
    move-exception v0

    if-eqz v7, :cond_5

    :try_start_5
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    :cond_5
    :goto_2
    throw v0

    .restart local v1    # "contacts":Landroid/net/Uri;
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v3    # "where":Ljava/lang/String;
    .restart local v5    # "orderBy":Ljava/lang/String;
    .restart local v6    # "address":Ljava/lang/String;
    .restart local v8    # "numberColumn":I
    :catch_2
    move-exception v0

    goto :goto_0

    .end local v6    # "address":Ljava/lang/String;
    .end local v8    # "numberColumn":I
    :catch_3
    move-exception v0

    goto :goto_1

    .end local v1    # "contacts":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "where":Ljava/lang/String;
    .end local v5    # "orderBy":Ljava/lang/String;
    :catch_4
    move-exception v4

    goto :goto_2
.end method

.method public static getLookupKeyFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 130
    if-nez p1, :cond_1

    .line 150
    :cond_0
    :goto_0
    return-object v3

    .line 134
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBUtilManager;->getContactDBUtil()Lcom/vlingo/core/internal/contacts/IContactDBUtil;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/contacts/IContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v4, "lookup"

    aput-object v4, v2, v5

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 139
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 141
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 142
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 143
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 147
    .local v7, "lookupKey":Ljava/lang/String;
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v3, v7

    goto :goto_0

    .end local v7    # "lookupKey":Ljava/lang/String;
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static getPersonIdFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)J
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const-wide/16 v8, -0x1

    const/4 v3, 0x0

    .line 106
    if-nez p1, :cond_0

    move-wide v0, v8

    .line 126
    :goto_0
    return-wide v0

    .line 110
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBUtilManager;->getContactDBUtil()Lcom/vlingo/core/internal/contacts/IContactDBUtil;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/contacts/IContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v0

    if-nez v0, :cond_1

    move-wide v0, v8

    .line 111
    goto :goto_0

    .line 113
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v4, "_id"

    aput-object v4, v2, v5

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 115
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_3

    .line 117
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 118
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 119
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 120
    .local v7, "id":Ljava/lang/Long;
    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 123
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .end local v7    # "id":Ljava/lang/Long;
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    move-wide v0, v8

    .line 126
    goto :goto_0

    .line 123
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static getTypeStringEN(I)Ljava/lang/String;
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 53
    packed-switch p0, :pswitch_data_0

    .line 97
    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    .line 55
    :pswitch_0
    const-string/jumbo v0, "home"

    goto :goto_0

    .line 57
    :pswitch_1
    const-string/jumbo v0, "mobile"

    goto :goto_0

    .line 59
    :pswitch_2
    const-string/jumbo v0, "work"

    goto :goto_0

    .line 61
    :pswitch_3
    const-string/jumbo v0, "fax work"

    goto :goto_0

    .line 63
    :pswitch_4
    const-string/jumbo v0, "fax home"

    goto :goto_0

    .line 65
    :pswitch_5
    const-string/jumbo v0, "pager"

    goto :goto_0

    .line 67
    :pswitch_6
    const-string/jumbo v0, "other"

    goto :goto_0

    .line 69
    :pswitch_7
    const-string/jumbo v0, "custom"

    goto :goto_0

    .line 71
    :pswitch_8
    const-string/jumbo v0, "callback"

    goto :goto_0

    .line 73
    :pswitch_9
    const-string/jumbo v0, "car"

    goto :goto_0

    .line 75
    :pswitch_a
    const-string/jumbo v0, "company main"

    goto :goto_0

    .line 77
    :pswitch_b
    const-string/jumbo v0, "isdn"

    goto :goto_0

    .line 79
    :pswitch_c
    const-string/jumbo v0, "main"

    goto :goto_0

    .line 81
    :pswitch_d
    const-string/jumbo v0, "other fax"

    goto :goto_0

    .line 83
    :pswitch_e
    const-string/jumbo v0, "radio"

    goto :goto_0

    .line 85
    :pswitch_f
    const-string/jumbo v0, "telex"

    goto :goto_0

    .line 87
    :pswitch_10
    const-string/jumbo v0, "tty tdd"

    goto :goto_0

    .line 89
    :pswitch_11
    const-string/jumbo v0, "work mobile"

    goto :goto_0

    .line 91
    :pswitch_12
    const-string/jumbo v0, "work pager"

    goto :goto_0

    .line 93
    :pswitch_13
    const-string/jumbo v0, "assistant"

    goto :goto_0

    .line 95
    :pswitch_14
    const-string/jumbo v0, "mms"

    goto :goto_0

    .line 53
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
    .end packed-switch
.end method
