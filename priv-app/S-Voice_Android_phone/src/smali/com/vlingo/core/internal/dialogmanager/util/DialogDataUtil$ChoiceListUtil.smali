.class public Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;
.super Ljava/lang/Object;
.source "DialogDataUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChoiceListUtil"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public choiceIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public displayedChoices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .local p0, "this":Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;, "Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil<TT;>;"
    const/4 v0, 0x0

    .line 230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 231
    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->displayedChoices:Ljava/util/List;

    .line 232
    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->choiceIds:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getIdOfSelection(Landroid/content/Intent;)Ljava/lang/String;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 313
    .local p0, "this":Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;, "Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil<TT;>;"
    const-string/jumbo v2, "choice"

    const/4 v3, -0x1

    invoke-static {p1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->extractParamInt(Landroid/content/Intent;Ljava/lang/String;I)I

    move-result v1

    .line 314
    .local v1, "position":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->choiceIds:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 315
    .local v0, "idString":Ljava/lang/String;
    return-object v0
.end method

.method public showChoiceListWidget(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;Ljava/util/Map;)Z
    .locals 12
    .param p1, "vvsListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .param p2, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p3, "stateKey"    # Lcom/vlingo/core/internal/dialogmanager/DialogDataType;
    .param p4, "widgetKeyList"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p5, "widgetKeySingle"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p6, "widgetListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogDataType;",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 275
    .local p0, "this":Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;, "Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil<TT;>;"
    .local p7, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v8, 0x0

    .line 276
    .local v8, "tempChoiceIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const-string/jumbo v9, "ListPosition"

    invoke-interface {p2}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 277
    const-string/jumbo v9, "Start"

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-static {p2, v9, v10, v11}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamInt(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;IZ)I

    move-result v7

    .line 278
    .local v7, "start":I
    const-string/jumbo v9, "End"

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-static {p2, v9, v10, v11}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamInt(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;IZ)I

    move-result v5

    .line 279
    .local v5, "end":I
    new-instance v8, Ljava/util/ArrayList;

    .end local v8    # "tempChoiceIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 280
    .restart local v8    # "tempChoiceIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    move v6, v7

    .local v6, "i":I
    :goto_0
    if-ge v6, v5, :cond_0

    .line 281
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 280
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 285
    .end local v5    # "end":I
    .end local v6    # "i":I
    .end local v7    # "start":I
    :cond_0
    const-string/jumbo v9, "choices"

    const/4 v10, 0x0

    invoke-static {p2, v9, v10}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_1

    if-eqz v8, :cond_5

    .line 287
    :cond_1
    if-eqz v8, :cond_2

    .line 288
    :try_start_0
    iput-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->choiceIds:Ljava/util/List;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    :goto_1
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->choiceIds:Ljava/util/List;

    if-nez v9, :cond_3

    .line 296
    const/4 v9, 0x0

    .line 309
    :goto_2
    return v9

    .line 290
    :cond_2
    :try_start_1
    invoke-static {p2}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getChoicesAsInts(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;

    move-result-object v9

    iput-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->choiceIds:Ljava/util/List;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 292
    :catch_0
    move-exception v4

    .line 293
    .local v4, "e":Lorg/json/JSONException;
    const-string/jumbo v9, "DialogDataUtil.ChoiceUtil"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "displayList: JSONException"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v4}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 299
    .end local v4    # "e":Lorg/json/JSONException;
    :cond_3
    invoke-interface {p1, p3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 300
    .local v2, "allObjects":Ljava/util/List;, "Ljava/util/List<TT;>;"
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->choiceIds:Ljava/util/List;

    invoke-static {v2, v9}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->extractByPosition(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v9

    iput-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->displayedChoices:Ljava/util/List;

    .line 301
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->displayedChoices:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_4

    .line 302
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->displayedChoices:Ljava/util/List;

    const/4 v10, 0x0

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 303
    .local v3, "choice":Ljava/lang/Object;, "TT;"
    const/4 v9, 0x0

    move-object/from16 v0, p5

    move-object/from16 v1, p6

    invoke-interface {p1, v0, v9, v3, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 307
    .end local v3    # "choice":Ljava/lang/Object;, "TT;"
    :goto_3
    const/4 v9, 0x1

    goto :goto_2

    .line 305
    :cond_4
    const/4 v9, 0x0

    iget-object v10, p0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->displayedChoices:Ljava/util/List;

    move-object/from16 v0, p4

    move-object/from16 v1, p6

    invoke-interface {p1, v0, v9, v10, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto :goto_3

    .line 309
    .end local v2    # "allObjects":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :cond_5
    const/4 v9, 0x0

    goto :goto_2
.end method

.method public showChoiceListWidget(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;Ljava/util/Map;)Z
    .locals 13
    .param p1, "vvsListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .param p2, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p3, "stateKey"    # Lcom/vlingo/core/internal/dialogmanager/DialogDataType;
    .param p4, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p5, "widgetListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogDataType;",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 236
    .local p0, "this":Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;, "Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil<TT;>;"
    .local p6, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v8, 0x0

    .line 237
    .local v8, "tempChoiceIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    .line 238
    .local v3, "decorator":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    const-string/jumbo v9, "ListPosition"

    invoke-interface {p2}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 239
    const-string/jumbo v9, "Start"

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-static {p2, v9, v10, v11}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamInt(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;IZ)I

    move-result v7

    .line 240
    .local v7, "start":I
    const-string/jumbo v9, "End"

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-static {p2, v9, v10, v11}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamInt(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;IZ)I

    move-result v5

    .line 241
    .local v5, "end":I
    new-instance v8, Ljava/util/ArrayList;

    .end local v8    # "tempChoiceIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 242
    .restart local v8    # "tempChoiceIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    move v6, v7

    .local v6, "i":I
    :goto_0
    if-ge v6, v5, :cond_0

    .line 243
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 242
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 245
    :cond_0
    sget-object v9, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string/jumbo v10, "Redraw"

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {p2, v10, v11, v12}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 246
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeReplaceable()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v3

    .line 247
    const-string/jumbo v9, "DialogDataUtil.ChoiceUtil"

    const-string/jumbo v10, "displayList: replaceable decorator."

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    .end local v5    # "end":I
    .end local v6    # "i":I
    .end local v7    # "start":I
    :cond_1
    const-string/jumbo v9, "choices"

    const/4 v10, 0x0

    invoke-static {p2, v9, v10}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_2

    if-eqz v8, :cond_6

    .line 253
    :cond_2
    if-eqz v8, :cond_4

    .line 254
    :try_start_0
    iput-object v8, p0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->choiceIds:Ljava/util/List;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 262
    :goto_1
    move-object/from16 v0, p3

    invoke-interface {p1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 263
    .local v2, "allObjects":Ljava/util/List;, "Ljava/util/List<TT;>;"
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->choiceIds:Ljava/util/List;

    if-eqz v9, :cond_3

    if-nez v2, :cond_5

    .line 264
    :cond_3
    const/4 v9, 0x0

    .line 270
    .end local v2    # "allObjects":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :goto_2
    return v9

    .line 256
    :cond_4
    :try_start_1
    invoke-static {p2}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->getChoicesAsInts(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;

    move-result-object v9

    iput-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->choiceIds:Ljava/util/List;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 258
    :catch_0
    move-exception v4

    .line 259
    .local v4, "e":Lorg/json/JSONException;
    const-string/jumbo v9, "DialogDataUtil.ChoiceUtil"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "displayList: JSONException"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v4}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 266
    .end local v4    # "e":Lorg/json/JSONException;
    .restart local v2    # "allObjects":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :cond_5
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->choiceIds:Ljava/util/List;

    invoke-static {v2, v9}, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil;->extractByPosition(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v9

    iput-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->displayedChoices:Ljava/util/List;

    .line 267
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/util/DialogDataUtil$ChoiceListUtil;->displayedChoices:Ljava/util/List;

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    invoke-interface {p1, v0, v3, v9, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 268
    const/4 v9, 0x1

    goto :goto_2

    .line 270
    .end local v2    # "allObjects":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :cond_6
    const/4 v9, 0x0

    goto :goto_2
.end method
