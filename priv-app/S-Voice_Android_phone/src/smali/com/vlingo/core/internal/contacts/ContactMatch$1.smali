.class Lcom/vlingo/core/internal/contacts/ContactMatch$1;
.super Ljava/lang/Object;
.source "ContactMatch.java"

# interfaces
.implements Lcom/vlingo/sdk/util/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/contacts/ContactMatch;->filterPhonesByType([ILjava/util/List;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/vlingo/sdk/util/Predicate",
        "<",
        "Lcom/vlingo/core/internal/contacts/ContactData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/contacts/ContactMatch;

.field final synthetic val$requestedTypes:[I


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/contacts/ContactMatch;[I)V
    .locals 0

    .prologue
    .line 578
    iput-object p1, p0, Lcom/vlingo/core/internal/contacts/ContactMatch$1;->this$0:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iput-object p2, p0, Lcom/vlingo/core/internal/contacts/ContactMatch$1;->val$requestedTypes:[I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/vlingo/core/internal/contacts/ContactData;)Z
    .locals 5
    .param p1, "object"    # Lcom/vlingo/core/internal/contacts/ContactData;

    .prologue
    .line 581
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/ContactMatch$1;->val$requestedTypes:[I

    .local v0, "arr$":[I
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget v3, v0, v1

    .line 582
    .local v3, "type":I
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactData;->getType()I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 583
    const/4 v4, 0x1

    .line 586
    .end local v3    # "type":I
    :goto_1
    return v4

    .line 581
    .restart local v3    # "type":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 586
    .end local v3    # "type":I
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 578
    check-cast p1, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/contacts/ContactMatch$1;->apply(Lcom/vlingo/core/internal/contacts/ContactData;)Z

    move-result v0

    return v0
.end method
