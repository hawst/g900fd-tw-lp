.class public final Lcom/vlingo/core/internal/util/HttpKeepAlive;
.super Ljava/lang/Object;
.source "HttpKeepAlive.java"


# static fields
.field public static final HTTP_KEEP_ALIVE:Ljava/lang/String; = "http.keepAlive"

.field private static httpKeepAlive:Lcom/vlingo/core/internal/util/HttpKeepAlive;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getInstance()Lcom/vlingo/core/internal/util/HttpKeepAlive;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/vlingo/core/internal/util/HttpKeepAlive;->httpKeepAlive:Lcom/vlingo/core/internal/util/HttpKeepAlive;

    if-nez v0, :cond_0

    new-instance v0, Lcom/vlingo/core/internal/util/HttpKeepAlive;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/HttpKeepAlive;-><init>()V

    :goto_0
    sput-object v0, Lcom/vlingo/core/internal/util/HttpKeepAlive;->httpKeepAlive:Lcom/vlingo/core/internal/util/HttpKeepAlive;

    .line 38
    sget-object v0, Lcom/vlingo/core/internal/util/HttpKeepAlive;->httpKeepAlive:Lcom/vlingo/core/internal/util/HttpKeepAlive;

    return-object v0

    .line 37
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/util/HttpKeepAlive;->httpKeepAlive:Lcom/vlingo/core/internal/util/HttpKeepAlive;

    goto :goto_0
.end method

.method public static isOn()Z
    .locals 2

    .prologue
    .line 15
    const-string/jumbo v0, "true"

    const-string/jumbo v1, "http.keepAlive"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isSet()Z
    .locals 1

    .prologue
    .line 19
    const-string/jumbo v0, "http.keepAlive"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static off()Lcom/vlingo/core/internal/util/HttpKeepAlive;
    .locals 2

    .prologue
    .line 32
    const-string/jumbo v0, "http.keepAlive"

    const-string/jumbo v1, "false"

    invoke-static {v0, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 33
    invoke-static {}, Lcom/vlingo/core/internal/util/HttpKeepAlive;->getInstance()Lcom/vlingo/core/internal/util/HttpKeepAlive;

    move-result-object v0

    return-object v0
.end method

.method public static on()Lcom/vlingo/core/internal/util/HttpKeepAlive;
    .locals 2

    .prologue
    .line 27
    const-string/jumbo v0, "http.keepAlive"

    const-string/jumbo v1, "true"

    invoke-static {v0, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 28
    invoke-static {}, Lcom/vlingo/core/internal/util/HttpKeepAlive;->getInstance()Lcom/vlingo/core/internal/util/HttpKeepAlive;

    move-result-object v0

    return-object v0
.end method

.method public static toggle(Z)V
    .locals 2
    .param p0, "toggle"    # Z

    .prologue
    .line 23
    const-string/jumbo v1, "http.keepAlive"

    if-eqz p0, :cond_0

    const-string/jumbo v0, "true"

    :goto_0
    invoke-static {v1, v0}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 24
    return-void

    .line 23
    :cond_0
    const-string/jumbo v0, "false"

    goto :goto_0
.end method
