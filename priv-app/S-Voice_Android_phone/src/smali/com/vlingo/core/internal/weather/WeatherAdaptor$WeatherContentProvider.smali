.class public final enum Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;
.super Ljava/lang/Enum;
.source "WeatherAdaptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/weather/WeatherAdaptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WeatherContentProvider"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

.field public static final enum ACCUWEATHER:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

.field public static final enum WEATHERNEWS:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32
    new-instance v0, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    const-string/jumbo v1, "WEATHERNEWS"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;->WEATHERNEWS:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    new-instance v0, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    const-string/jumbo v1, "ACCUWEATHER"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;->ACCUWEATHER:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    .line 31
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    sget-object v1, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;->WEATHERNEWS:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;->ACCUWEATHER:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;->$VALUES:[Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const-class v0, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;->$VALUES:[Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    return-object v0
.end method
