.class public Lcom/vlingo/core/internal/naver/NaverResponseParser;
.super Ljava/lang/Object;
.source "NaverResponseParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;
    }
.end annotation


# static fields
.field public static final DETAILS:Ljava/lang/String; = "Details"

.field public static final MESSAGE:Ljava/lang/String; = "Message"

.field public static final NAVER_PASS_THROUGH_RESPONSE:Ljava/lang/String; = "NaverPassThroughResponse"

.field public static final QUERY_USED:Ljava/lang/String; = "QueryUsed"

.field public static final RAW_RESPONSE_XML:Ljava/lang/String; = "RawResponseXML"


# instance fields
.field private error:Ljava/lang/String;

.field private queryUsed:Ljava/lang/String;

.field private rawResponseXML:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/naver/NaverResponseParser;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/naver/NaverResponseParser;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/naver/NaverResponseParser;->setError(Ljava/lang/String;)V

    return-void
.end method

.method private setError(Ljava/lang/String;)V
    .locals 0
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser;->error:Ljava/lang/String;

    .line 53
    return-void
.end method


# virtual methods
.method public getError()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser;->error:Ljava/lang/String;

    return-object v0
.end method

.method public getQueryUsed()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser;->queryUsed:Ljava/lang/String;

    return-object v0
.end method

.method public getRawResponseXML()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser;->rawResponseXML:Ljava/lang/String;

    return-object v0
.end method

.method public parse(Ljava/lang/String;)V
    .locals 7
    .param p1, "xml"    # Ljava/lang/String;

    .prologue
    .line 60
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v3

    .line 61
    .local v3, "saxPF":Ljavax/xml/parsers/SAXParserFactory;
    invoke-virtual {v3}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v2

    .line 62
    .local v2, "saxP":Ljavax/xml/parsers/SAXParser;
    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v4

    .line 67
    .local v4, "xmlR":Lorg/xml/sax/XMLReader;
    new-instance v1, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;

    invoke-direct {v1, p0}, Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;-><init>(Lcom/vlingo/core/internal/naver/NaverResponseParser;)V

    .line 68
    .local v1, "myXMLHandler":Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;
    invoke-interface {v4, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 69
    new-instance v5, Lorg/xml/sax/InputSource;

    new-instance v6, Ljava/io/StringReader;

    invoke-direct {v6, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v6}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface {v4, v5}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    .end local v1    # "myXMLHandler":Lcom/vlingo/core/internal/naver/NaverResponseParser$XMLHandler;
    .end local v2    # "saxP":Ljavax/xml/parsers/SAXParser;
    .end local v3    # "saxPF":Ljavax/xml/parsers/SAXParserFactory;
    .end local v4    # "xmlR":Lorg/xml/sax/XMLReader;
    :goto_0
    return-void

    .line 71
    :catch_0
    move-exception v0

    .line 72
    .local v0, "e":Ljava/lang/Exception;
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v5, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public setQueryUsed(Ljava/lang/String;)V
    .locals 0
    .param p1, "queryUsed"    # Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser;->queryUsed:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public setRawResponseXML(Ljava/lang/String;)V
    .locals 0
    .param p1, "rawResponseXML"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/vlingo/core/internal/naver/NaverResponseParser;->rawResponseXML:Ljava/lang/String;

    .line 45
    return-void
.end method
