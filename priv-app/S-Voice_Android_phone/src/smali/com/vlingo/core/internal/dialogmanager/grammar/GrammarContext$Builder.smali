.class public Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;
.super Ljava/lang/Object;
.source "GrammarContext.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private generalSpeakables:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private listItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;->generalSpeakables:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;->listItems:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 2

    .prologue
    .line 76
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;-><init>(Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$1;)V

    return-object v0
.end method

.method public generalSpeakables(Ljava/util/List;)Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;"
        }
    .end annotation

    .prologue
    .line 66
    .local p1, "speakables":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;->generalSpeakables:Ljava/util/List;

    .line 67
    return-object p0
.end method

.method public listItems(Ljava/util/List;)Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;"
        }
    .end annotation

    .prologue
    .line 71
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext$Builder;->listItems:Ljava/util/List;

    .line 72
    return-object p0
.end method
