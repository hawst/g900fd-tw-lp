.class public Lcom/vlingo/core/internal/recognition/acceptedtext/EmailAcceptedText;
.super Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;
.source "EmailAcceptedText.java"


# instance fields
.field private contact:Ljava/lang/String;

.field private subject:Ljava/lang/String;

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "contact"    # Ljava/lang/String;
    .param p2, "subject"    # Ljava/lang/String;
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;-><init>(Ljava/lang/String;)V

    .line 25
    iput-object p3, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EmailAcceptedText;->text:Ljava/lang/String;

    .line 26
    iput-object p1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EmailAcceptedText;->contact:Ljava/lang/String;

    .line 27
    iput-object p2, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EmailAcceptedText;->subject:Ljava/lang/String;

    .line 28
    return-void
.end method


# virtual methods
.method protected getAcceptedTextXML()Ljava/lang/String;
    .locals 2

    .prologue
    .line 32
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 33
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "<AcceptedText pt=\"sms:def\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 34
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EmailAcceptedText;->contact:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 35
    const-string/jumbo v1, "<Tag u=\"contact\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 36
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EmailAcceptedText;->contact:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 37
    const-string/jumbo v1, "</Tag>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 39
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EmailAcceptedText;->text:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 40
    const-string/jumbo v1, "<Tag u=\"text\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 41
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EmailAcceptedText;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 42
    const-string/jumbo v1, "</Tag>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 44
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EmailAcceptedText;->subject:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 45
    const-string/jumbo v1, "<Tag u=\"subj\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 46
    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EmailAcceptedText;->subject:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 47
    const-string/jumbo v1, "</Tag>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 49
    :cond_2
    const-string/jumbo v1, "</AcceptedText>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 50
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "SMSAcceptedText ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", subject = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EmailAcceptedText;->subject:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EmailAcceptedText;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", contact="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/recognition/acceptedtext/EmailAcceptedText;->contact:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
