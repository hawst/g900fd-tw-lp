.class public final Lcom/vlingo/core/internal/messages/SMSMMSProvider;
.super Ljava/lang/Object;
.source "SMSMMSProvider.java"


# static fields
.field private static final BODY:Ljava/lang/String; = "body"

.field private static final CONTENT_URL_MESSAGE_CONVERSATION:Ljava/lang/String; = "content://mms-sms/conversations"

.field private static final CONTENT_URL_MMS:Ljava/lang/String; = "content://mms/"

.field private static final CONTENT_URL_SMS:Ljava/lang/String; = "content://sms/"

.field private static final DATE:Ljava/lang/String; = "date"

.field private static final ID:Ljava/lang/String; = "_id"

.field private static final INSTANCE:Lcom/vlingo/core/internal/messages/SMSMMSProvider;

.field private static final MESSAGE_CONVERSATION_URI:Landroid/net/Uri;

.field private static final MESSAGE_MARK_AS_READ_INTENT:Ljava/lang/String; = "com.samsung.accessory.intent.action.CHECK_NOTIFICATION_ITEM"

.field private static final MESSAGE_MARK_AS_READ_NOTIFICATION_ID:Ljava/lang/String; = "NOTIFICATION_ITEM_ID"

.field private static final MESSAGE_MARK_AS_READ_NOTIFICATION_URI:Ljava/lang/String; = "NOTIFICATION_ITEM_URI"

.field private static final MESSAGE_MARK_AS_READ_PACKAGE_NAME_KEY:Ljava/lang/String; = "NOTIFICATION_PACKAGE_NAME"

.field private static final MESSAGE_MARK_AS_READ_PACKAGE_NAME_VALUE:Ljava/lang/String; = "com.android.mms"

.field private static final MESSAGE_REQ_MARK_AS_READ_INTENT:Ljava/lang/String; = "com.samsung.accessory.intent.action.UPDATE_NOTIFICATION_ITEM"

.field private static final MIME_TYPE:Ljava/lang/String; = "ct_t"

.field private static final MMS_DATE_CORRECTION_FACTOR:J = 0x3e8L

.field private static final MMS_PROJECTION:[Ljava/lang/String;

.field public static final MMS_TYPE:Ljava/lang/String; = "MMS"

.field public static final MMS_URI:Landroid/net/Uri;

.field private static final MSG_BOX:Ljava/lang/String; = "msg_box"

.field private static final MSG_BOX_INBOX:I = 0x1

.field private static final NAME:Ljava/lang/String; = "name"

.field private static final PHONE:Ljava/lang/String; = "address"

.field private static final QUERY_FRAGMENT_NOT_READ:Ljava/lang/String; = "read = 0"

.field private static final QUERY_FRAGMENT_NOT_SEEN:Ljava/lang/String; = "seen = 0"

.field private static final QUERY_FRAGMENT_NOT_SEEN_AND_NOT_READ:Ljava/lang/String; = "seen = 0 AND read = 0"

.field private static final QUERY_FRAGMENT_SMS_RECEIVED_FLAG:Ljava/lang/String; = "type = 1"

.field private static final READ:Ljava/lang/String; = "read"

.field private static final SEEN:Ljava/lang/String; = "seen"

.field private static final SMS_PROJECTION:[Ljava/lang/String;

.field public static final SMS_TYPE:Ljava/lang/String; = "SMS"

.field private static final SMS_URI:Landroid/net/Uri;

.field private static final SORT_ORDER:Ljava/lang/String; = "date DESC"

.field public static final SUBJECT:Ljava/lang/String; = "sub"

.field private static final TAG:Ljava/lang/String;

.field private static final TYPE:Ljava/lang/String; = "type"

.field private static final VALUE:Ljava/lang/String; = "value"

.field private static currentSenderUnreadCount:I


# instance fields
.field mContext:Landroid/content/Context;

.field private mostRecentTimestamp:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 47
    const-class v0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->TAG:Ljava/lang/String;

    .line 49
    new-instance v0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    invoke-direct {v0}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->INSTANCE:Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    .line 61
    const-string/jumbo v0, "content://sms/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->SMS_URI:Landroid/net/Uri;

    .line 62
    const-string/jumbo v0, "content://mms/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->MMS_URI:Landroid/net/Uri;

    .line 63
    const-string/jumbo v0, "content://mms-sms/conversations"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->MESSAGE_CONVERSATION_URI:Landroid/net/Uri;

    .line 92
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "*"

    aput-object v1, v0, v2

    sput-object v0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->SMS_PROJECTION:[Ljava/lang/String;

    .line 93
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "*"

    aput-object v1, v0, v2

    sput-object v0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->MMS_PROJECTION:[Ljava/lang/String;

    .line 105
    sput v2, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->currentSenderUnreadCount:I

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mContext:Landroid/content/Context;

    .line 103
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mostRecentTimestamp:J

    .line 112
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mContext:Landroid/content/Context;

    .line 113
    return-void
.end method

.method private extractAttachmentBodyFromCursor(Landroid/database/Cursor;)I
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 884
    const/4 v1, 0x0

    .line 885
    .local v1, "attachments":I
    sget-object v5, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MsgUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v5}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v3

    .line 886
    .local v3, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v4, 0x0

    .line 887
    .local v4, "mmsUtil":Lcom/vlingo/core/internal/util/MsgUtil;
    if-eqz v3, :cond_0

    .line 889
    :try_start_0
    invoke-virtual {v3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/vlingo/core/internal/util/MsgUtil;

    move-object v4, v0

    .line 890
    if-eqz v4, :cond_0

    .line 891
    invoke-interface {v4, p1}, Lcom/vlingo/core/internal/util/MsgUtil;->countAttachmentMMS(Landroid/database/Cursor;)I
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 899
    :cond_0
    :goto_0
    return v1

    .line 893
    :catch_0
    move-exception v2

    .line 894
    .local v2, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v2}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 895
    .end local v2    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v2

    .line 896
    .local v2, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0
.end method

.method private extractBodyFromCursor(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "body"    # Ljava/lang/String;

    .prologue
    .line 702
    sget-object v4, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MsgUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v4}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v2

    .line 703
    .local v2, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v3, 0x0

    .line 704
    .local v3, "mmsUtil":Lcom/vlingo/core/internal/util/MsgUtil;
    if-eqz v2, :cond_0

    .line 706
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/vlingo/core/internal/util/MsgUtil;

    move-object v3, v0

    .line 707
    if-eqz v3, :cond_0

    .line 708
    invoke-interface {v3, p1}, Lcom/vlingo/core/internal/util/MsgUtil;->getMmsTextByCursor(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object p2

    .line 709
    if-nez p2, :cond_0

    .line 710
    const-string/jumbo v4, "body"

    invoke-direct {p0, p1, v4}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object p2

    .line 719
    :cond_0
    :goto_0
    return-object p2

    .line 713
    :catch_0
    move-exception v1

    .line 714
    .local v1, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v1}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 715
    .end local v1    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v1

    .line 716
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0
.end method

.method private getAddress(Landroid/database/Cursor;I)Ljava/lang/String;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "id"    # I

    .prologue
    .line 585
    const-string/jumbo v1, "address"

    invoke-direct {p0, p1, v1}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 586
    .local v0, "address":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 587
    invoke-direct {p0, p2}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getAddressNumber(I)Ljava/lang/String;

    move-result-object v0

    .line 588
    if-nez v0, :cond_0

    .line 593
    :cond_0
    return-object v0
.end method

.method private getAddressNumber(I)Ljava/lang/String;
    .locals 11
    .param p1, "id"    # I

    .prologue
    .line 745
    new-instance v3, Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "msg_id="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 746
    .local v3, "selectionAdd":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "content://mms/"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "/addr"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 747
    .local v10, "uriStr":Ljava/lang/String;
    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 748
    .local v1, "uriAddress":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 749
    .local v6, "cAdd":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 751
    .local v7, "name":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 753
    const/4 v9, 0x0

    .line 754
    .local v9, "number":Ljava/lang/String;
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 756
    :cond_0
    const-string/jumbo v0, "address"

    invoke-direct {p0, v6, v0}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 757
    if-eqz v9, :cond_3

    .line 759
    :try_start_1
    const-string/jumbo v0, "-"

    const-string/jumbo v2, ""

    invoke-virtual {v9, v0, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    .line 760
    move-object v7, v9

    .line 761
    const-string/jumbo v0, "getAddressNumber"

    invoke-direct {p0, v0, v6}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->printColumns(Ljava/lang/String;Landroid/database/Cursor;)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 772
    :cond_1
    :goto_0
    if-eqz v6, :cond_2

    .line 773
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 776
    :cond_2
    return-object v7

    .line 763
    :catch_0
    move-exception v8

    .line 764
    .local v8, "nfe":Ljava/lang/NumberFormatException;
    if-nez v7, :cond_3

    .line 765
    move-object v7, v9

    .line 769
    .end local v8    # "nfe":Ljava/lang/NumberFormatException;
    :cond_3
    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 772
    .end local v9    # "number":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 773
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public static getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->INSTANCE:Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    return-object v0
.end method

.method private getInt(Landroid/database/Cursor;Ljava/lang/String;)I
    .locals 2
    .param p1, "cur"    # Landroid/database/Cursor;
    .param p2, "columnName"    # Ljava/lang/String;

    .prologue
    .line 791
    const/4 v1, -0x1

    .line 792
    .local v1, "val":I
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 793
    .local v0, "columnIndex":I
    if-ltz v0, :cond_0

    .line 794
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 796
    :cond_0
    return v1
.end method

.method private getLong(Landroid/database/Cursor;Ljava/lang/String;)J
    .locals 3
    .param p1, "cur"    # Landroid/database/Cursor;
    .param p2, "columnName"    # Ljava/lang/String;

    .prologue
    .line 800
    const-wide/16 v1, 0x0

    .line 801
    .local v1, "val":J
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 802
    .local v0, "columnIndex":I
    if-ltz v0, :cond_0

    .line 803
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 805
    :cond_0
    return-wide v1
.end method

.method private getMMSCursor(JLjava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 13
    .param p1, "queryTimeMS"    # J
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 830
    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MsgUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v1}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v11

    .line 831
    .local v11, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v9, 0x0

    .line 832
    .local v9, "cursor":Landroid/database/Cursor;
    const/4 v12, 0x0

    .line 833
    .local v12, "mmsUtil":Lcom/vlingo/core/internal/util/MsgUtil;
    if-eqz v11, :cond_1

    .line 835
    :try_start_0
    invoke-virtual {v11}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/vlingo/core/internal/util/MsgUtil;

    move-object v12, v0

    .line 836
    if-eqz v12, :cond_0

    .line 837
    invoke-interface {v12, p1, p2}, Lcom/vlingo/core/internal/util/MsgUtil;->getMmsQueryCursor(J)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v9

    .line 860
    :cond_0
    :goto_0
    return-object v9

    .line 839
    :catch_0
    move-exception v10

    .line 840
    .local v10, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v10}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 841
    .end local v10    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v10

    .line 842
    .local v10, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v10}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 845
    .end local v10    # "e":Ljava/lang/IllegalAccessException;
    :cond_1
    const-wide/16 v1, 0x0

    cmp-long v1, p1, v1

    if-lez v1, :cond_2

    .line 846
    const-wide/16 v1, 0x3e8

    div-long v7, p1, v1

    .line 847
    .local v7, "adjustedQueryTimeMS":J
    invoke-static/range {p3 .. p3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 848
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " AND date > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 856
    .end local v7    # "adjustedQueryTimeMS":J
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->MMS_URI:Landroid/net/Uri;

    sget-object v3, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->MMS_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v4, p3

    move-object/from16 v6, p4

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    goto :goto_0

    .line 850
    .restart local v7    # "adjustedQueryTimeMS":J
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "date > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    goto :goto_1
.end method

.method private getNewAlertsOfType(Landroid/database/Cursor;Ljava/lang/String;Z)Ljava/util/LinkedList;
    .locals 12
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "messageType"    # Ljava/lang/String;
    .param p3, "updateQueryTime"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 597
    new-instance v10, Ljava/util/LinkedList;

    invoke-direct {v10}, Ljava/util/LinkedList;-><init>()V

    .line 599
    .local v10, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_2

    .line 692
    :cond_0
    if-eqz p1, :cond_1

    .line 693
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 698
    :cond_1
    :goto_0
    return-object v10

    .line 606
    :cond_2
    :try_start_1
    const-string/jumbo v1, "_id"

    invoke-direct {p0, p1, v1}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v11

    .line 607
    .local v11, "id":I
    const-string/jumbo v1, "MMS"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 630
    :cond_3
    const-string/jumbo v1, "MMS"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 631
    int-to-long v1, v11

    invoke-direct {p0, v10, v1, v2}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->isInAlertList(Ljava/util/LinkedList;J)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 687
    :cond_4
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_2

    .line 692
    if-eqz p1, :cond_1

    .line 693
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 645
    :cond_5
    :try_start_2
    invoke-direct {p0, p1, v11}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getAddress(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v5

    .line 647
    .local v5, "address":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mContext:Landroid/content/Context;

    invoke-static {v1, v5}, Lcom/vlingo/core/internal/util/ContactUtil;->getContactDisplayNameByAddress(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 648
    .local v6, "senderDisplayName":Ljava/lang/String;
    if-nez v6, :cond_6

    .line 649
    const-string/jumbo v6, ""

    .line 652
    :cond_6
    const/4 v7, 0x0

    .line 653
    .local v7, "body":Ljava/lang/String;
    const-string/jumbo v1, "MMS"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 654
    invoke-direct {p0, p1, v7}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->extractBodyFromCursor(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 659
    :goto_2
    const-string/jumbo v1, "date"

    invoke-direct {p0, p1, v1}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v3

    .line 660
    .local v3, "dateMs":J
    const-string/jumbo v1, "MMS"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 662
    const-wide/16 v1, 0x3e8

    mul-long/2addr v3, v1

    .line 665
    :cond_7
    iget-wide v1, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mostRecentTimestamp:J

    cmp-long v1, v3, v1

    if-lez v1, :cond_8

    if-eqz p3, :cond_8

    .line 668
    iput-wide v3, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mostRecentTimestamp:J

    .line 671
    :cond_8
    const/4 v8, 0x0

    .line 672
    .local v8, "subject":Ljava/lang/String;
    const-string/jumbo v1, "MMS"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 673
    invoke-static {p1}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMmsSubject(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v8

    .line 676
    :cond_9
    new-instance v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    int-to-long v1, v11

    move-object v9, p2

    invoke-direct/range {v0 .. v9}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 678
    .local v0, "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    const-string/jumbo v1, "MMS"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 679
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->extractAttachmentBodyFromCursor(Landroid/database/Cursor;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->setAttachments(I)V

    .line 682
    :cond_a
    invoke-virtual {v10, v0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 683
    invoke-virtual {v10, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 688
    .end local v0    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v3    # "dateMs":J
    .end local v5    # "address":Ljava/lang/String;
    .end local v6    # "senderDisplayName":Ljava/lang/String;
    .end local v7    # "body":Ljava/lang/String;
    .end local v8    # "subject":Ljava/lang/String;
    .end local v11    # "id":I
    :catch_0
    move-exception v1

    .line 692
    if-eqz p1, :cond_1

    .line 693
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 656
    .restart local v5    # "address":Ljava/lang/String;
    .restart local v6    # "senderDisplayName":Ljava/lang/String;
    .restart local v7    # "body":Ljava/lang/String;
    .restart local v11    # "id":I
    :cond_b
    :try_start_3
    const-string/jumbo v1, "body"

    invoke-direct {p0, p1, v1}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v7

    goto :goto_2

    .line 692
    .end local v5    # "address":Ljava/lang/String;
    .end local v6    # "senderDisplayName":Ljava/lang/String;
    .end local v7    # "body":Ljava/lang/String;
    .end local v11    # "id":I
    :catchall_0
    move-exception v1

    if-eqz p1, :cond_c

    .line 693
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_c
    throw v1
.end method

.method private getSMSCursor(JLjava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "queryTimeMS"    # J
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 810
    invoke-static {p3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 811
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " AND type = 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 815
    :goto_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 816
    invoke-static {p3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 817
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " AND date > "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 824
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->SMS_URI:Landroid/net/Uri;

    sget-object v2, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->SMS_PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    move-object v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 813
    :cond_1
    const-string/jumbo p3, "type = 1"

    goto :goto_0

    .line 819
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "date > "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    goto :goto_1
.end method

.method private getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "cur"    # Landroid/database/Cursor;
    .param p2, "columnName"    # Ljava/lang/String;

    .prologue
    .line 782
    const/4 v1, 0x0

    .line 783
    .local v1, "val":Ljava/lang/String;
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 784
    .local v0, "columnIndex":I
    if-ltz v0, :cond_0

    .line 785
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 787
    :cond_0
    return-object v1
.end method

.method private isInAlertList(Ljava/util/LinkedList;J)Z
    .locals 5
    .param p2, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;J)Z"
        }
    .end annotation

    .prologue
    .line 723
    .local p1, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    const/4 v2, 0x0

    .line 724
    .local v2, "size":I
    const/4 v0, 0x0

    .line 725
    .local v0, "alert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    if-eqz p1, :cond_1

    .line 726
    invoke-virtual {p1}, Ljava/util/LinkedList;->size()I

    move-result v2

    .line 727
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 728
    invoke-virtual {p1, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "alert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 729
    .restart local v0    # "alert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getId()J

    move-result-wide v3

    cmp-long v3, v3, p2

    if-nez v3, :cond_0

    .line 730
    const/4 v3, 0x1

    .line 735
    .end local v1    # "i":I
    :goto_1
    return v3

    .line 727
    .restart local v1    # "i":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 735
    .end local v1    # "i":I
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private printColumns(Ljava/lang/String;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "preface"    # Ljava/lang/String;
    .param p2, "c"    # Landroid/database/Cursor;

    .prologue
    .line 875
    return-void
.end method


# virtual methods
.method public findbyId(JLjava/lang/String;Ljava/util/LinkedList;)Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .locals 19
    .param p1, "id"    # J
    .param p3, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;)",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;"
        }
    .end annotation

    .prologue
    .line 466
    .local p4, "smsMmsAlerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    const/4 v3, 0x0

    .line 468
    .local v3, "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    const-string/jumbo v12, "SMS"

    .line 470
    .local v12, "msgType":Ljava/lang/String;
    const-string/jumbo v4, "MMS"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 471
    const-string/jumbo v12, "MMS"

    .line 478
    :cond_0
    :goto_0
    invoke-virtual/range {p4 .. p4}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-lez v4, :cond_3

    .line 479
    invoke-virtual/range {p4 .. p4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 480
    .local v13, "aMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-virtual {v13}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getId()J

    move-result-wide v4

    cmp-long v4, v4, p1

    if-nez v4, :cond_1

    invoke-virtual {v13}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getType()Ljava/lang/String;

    move-result-object v4

    if-ne v4, v12, :cond_1

    .line 483
    move-object v3, v13

    goto :goto_1

    .line 473
    .end local v13    # "aMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v15    # "i$":Ljava/util/Iterator;
    :cond_2
    const-string/jumbo v4, "SMS"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 474
    const-string/jumbo v12, "SMS"

    goto :goto_0

    :cond_3
    move-object v14, v3

    .line 493
    .end local v3    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .local v14, "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    const/16 v16, 0x0

    .line 495
    .local v16, "myCursor":Landroid/database/Cursor;
    :try_start_0
    const-string/jumbo v4, "SMS"

    invoke-virtual {v4, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 496
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->SMS_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "_id = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 501
    :cond_4
    :goto_2
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    .line 502
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-nez v4, :cond_a

    .line 505
    move-wide/from16 v0, p1

    long-to-int v4, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getAddress(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v8

    .line 506
    .local v8, "address":Ljava/lang/String;
    const-string/jumbo v4, "body"

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 507
    .local v10, "body":Ljava/lang/String;
    const-string/jumbo v4, "date"

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v6

    .line 508
    .local v6, "dateMs":J
    const/4 v11, 0x0

    .line 510
    .local v11, "subject":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mContext:Landroid/content/Context;

    invoke-static {v4, v8}, Lcom/vlingo/core/internal/util/ContactUtil;->getContactDisplayNameByAddress(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 511
    .local v9, "senderDisplayName":Ljava/lang/String;
    const-string/jumbo v4, "MMS"

    invoke-virtual {v4, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 512
    invoke-static/range {v16 .. v16}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMmsSubject(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v11

    .line 515
    :cond_5
    new-instance v3, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    move-wide/from16 v4, p1

    invoke-direct/range {v3 .. v12}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 517
    .end local v14    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .restart local v3    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :try_start_1
    const-string/jumbo v4, "MMS"

    invoke-virtual {v4, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 518
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->extractAttachmentBodyFromCursor(Landroid/database/Cursor;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->setAttachments(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 525
    .end local v6    # "dateMs":J
    .end local v8    # "address":Ljava/lang/String;
    .end local v9    # "senderDisplayName":Ljava/lang/String;
    .end local v10    # "body":Ljava/lang/String;
    .end local v11    # "subject":Ljava/lang/String;
    :cond_6
    :goto_3
    if-eqz v16, :cond_7

    .line 526
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 530
    :cond_7
    :goto_4
    return-object v3

    .line 498
    .end local v3    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .restart local v14    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :cond_8
    :try_start_2
    const-string/jumbo v4, "MMS"

    invoke-virtual {v4, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 499
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->MMS_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "_id = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v16

    goto/16 :goto_2

    .line 521
    :catch_0
    move-exception v4

    move-object v3, v14

    .line 525
    .end local v14    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .restart local v3    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :goto_5
    if-eqz v16, :cond_7

    .line 526
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    goto :goto_4

    .line 525
    .end local v3    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .restart local v14    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :catchall_0
    move-exception v4

    move-object v3, v14

    .end local v14    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .restart local v3    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :goto_6
    if-eqz v16, :cond_9

    .line 526
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v4

    .line 525
    .restart local v6    # "dateMs":J
    .restart local v8    # "address":Ljava/lang/String;
    .restart local v9    # "senderDisplayName":Ljava/lang/String;
    .restart local v10    # "body":Ljava/lang/String;
    .restart local v11    # "subject":Ljava/lang/String;
    :catchall_1
    move-exception v4

    goto :goto_6

    .line 521
    :catch_1
    move-exception v4

    goto :goto_5

    .end local v3    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v6    # "dateMs":J
    .end local v8    # "address":Ljava/lang/String;
    .end local v9    # "senderDisplayName":Ljava/lang/String;
    .end local v10    # "body":Ljava/lang/String;
    .end local v11    # "subject":Ljava/lang/String;
    .restart local v14    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :cond_a
    move-object v3, v14

    .end local v14    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .restart local v3    # "aSMSMMSMessage":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    goto :goto_3
.end method

.method public getAlerts(JILjava/lang/String;Ljava/lang/String;Z)Ljava/util/LinkedList;
    .locals 8
    .param p1, "queryTimeMS"    # J
    .param p3, "maxToReturn"    # I
    .param p4, "query"    # Ljava/lang/String;
    .param p5, "orderCol"    # Ljava/lang/String;
    .param p6, "ascending"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 154
    .local v0, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    move-object v3, p5

    .line 155
    .local v3, "sortOrder":Ljava/lang/String;
    if-eqz p6, :cond_2

    .line 156
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " ASC"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 163
    :goto_0
    invoke-direct {p0, p1, p2, p4, v3}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getMMSCursor(JLjava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 164
    .local v1, "mmsCursor":Landroid/database/Cursor;
    const-string/jumbo v5, "MMS"

    const/4 v6, 0x0

    invoke-direct {p0, v1, v5, v6}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getNewAlertsOfType(Landroid/database/Cursor;Ljava/lang/String;Z)Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 166
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v4

    .line 167
    .local v4, "tmpAlertSize":I
    if-lez v4, :cond_0

    .line 168
    sget-object v5, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "New MMS Alert count is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    :cond_0
    invoke-direct {p0, p1, p2, p4, v3}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getSMSCursor(JLjava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 174
    .local v2, "smsCursor":Landroid/database/Cursor;
    const-string/jumbo v5, "SMS"

    const/4 v6, 0x1

    invoke-direct {p0, v2, v5, v6}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getNewAlertsOfType(Landroid/database/Cursor;Ljava/lang/String;Z)Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 175
    new-instance v5, Lcom/vlingo/core/internal/messages/SMSMMSProvider$1;

    invoke-direct {v5, p0, p6}, Lcom/vlingo/core/internal/messages/SMSMMSProvider$1;-><init>(Lcom/vlingo/core/internal/messages/SMSMMSProvider;Z)V

    invoke-static {v0, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 188
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v5

    if-ge v4, v5, :cond_1

    .line 189
    sget-object v5, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "New SMS Alert count is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v7

    sub-int/2addr v7, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    :cond_1
    if-lez p3, :cond_3

    .line 195
    :goto_1
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v5

    if-le v5, p3, :cond_3

    .line 196
    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    goto :goto_1

    .line 158
    .end local v1    # "mmsCursor":Landroid/database/Cursor;
    .end local v2    # "smsCursor":Landroid/database/Cursor;
    .end local v4    # "tmpAlertSize":I
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " DESC"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 199
    .restart local v1    # "mmsCursor":Landroid/database/Cursor;
    .restart local v2    # "smsCursor":Landroid/database/Cursor;
    .restart local v4    # "tmpAlertSize":I
    :cond_3
    return-object v0
.end method

.method public getAllNewAlerts()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 275
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getAllNewAlerts(I)Ljava/util/LinkedList;

    move-result-object v0

    return-object v0
.end method

.method public getAllNewAlerts(I)Ljava/util/LinkedList;
    .locals 7
    .param p1, "maxToReturn"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 290
    const-wide/16 v1, 0x0

    .line 291
    .local v1, "queryTimeMS":J
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-le v0, v3, :cond_0

    .line 292
    const-string/jumbo v4, "read = 0"

    const-string/jumbo v5, "date"

    move-object v0, p0

    move v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getAlerts(JILjava/lang/String;Ljava/lang/String;Z)Ljava/util/LinkedList;

    move-result-object v0

    .line 294
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v4, "seen = 0 AND read = 0"

    const-string/jumbo v5, "date"

    move-object v0, p0

    move v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getAlerts(JILjava/lang/String;Ljava/lang/String;Z)Ljava/util/LinkedList;

    move-result-object v0

    goto :goto_0
.end method

.method public getMMSAlerts(JILjava/lang/String;Ljava/lang/String;Z)Ljava/util/LinkedList;
    .locals 5
    .param p1, "queryTimeMS"    # J
    .param p3, "maxToReturn"    # I
    .param p4, "query"    # Ljava/lang/String;
    .param p5, "orderCol"    # Ljava/lang/String;
    .param p6, "ascending"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 204
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 206
    .local v0, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    move-object v2, p5

    .line 207
    .local v2, "sortOrder":Ljava/lang/String;
    if-eqz p6, :cond_0

    .line 208
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " ASC"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 215
    :goto_0
    invoke-direct {p0, p1, p2, p4, v2}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getMMSCursor(JLjava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 216
    .local v1, "mmsCursor":Landroid/database/Cursor;
    const-string/jumbo v3, "MMS"

    const/4 v4, 0x0

    invoke-direct {p0, v1, v3, v4}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getNewAlertsOfType(Landroid/database/Cursor;Ljava/lang/String;Z)Ljava/util/LinkedList;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 220
    if-lez p3, :cond_1

    .line 221
    :goto_1
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-le v3, p3, :cond_1

    .line 222
    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    goto :goto_1

    .line 210
    .end local v1    # "mmsCursor":Landroid/database/Cursor;
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " DESC"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 225
    .restart local v1    # "mmsCursor":Landroid/database/Cursor;
    :cond_1
    return-object v0
.end method

.method public getNewMMSSafereaderAlerts(I)Ljava/util/LinkedList;
    .locals 8
    .param p1, "maxToReturn"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 376
    const-string/jumbo v0, "car_safereader_last_saferead_time"

    const-wide/16 v1, 0x0

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mostRecentTimestamp:J

    .line 379
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-le v0, v1, :cond_0

    .line 380
    iget-wide v1, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mostRecentTimestamp:J

    const-string/jumbo v4, "read = 0"

    const-string/jumbo v5, "date"

    move-object v0, p0

    move v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getMMSAlerts(JILjava/lang/String;Ljava/lang/String;Z)Ljava/util/LinkedList;

    move-result-object v7

    .line 391
    .local v7, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    :goto_0
    return-object v7

    .line 382
    .end local v7    # "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    :cond_0
    iget-wide v1, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mostRecentTimestamp:J

    const-string/jumbo v4, "seen = 0"

    const-string/jumbo v5, "date"

    move-object v0, p0

    move v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getMMSAlerts(JILjava/lang/String;Ljava/lang/String;Z)Ljava/util/LinkedList;

    move-result-object v7

    .restart local v7    # "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    goto :goto_0
.end method

.method public getNewSMSSafereaderAlerts(I)Ljava/util/LinkedList;
    .locals 11
    .param p1, "maxToReturn"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v9, 0x0

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 308
    const-string/jumbo v0, "car_safereader_last_saferead_time"

    invoke-static {v0, v9, v10}, Lcom/vlingo/core/internal/settings/Settings;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mostRecentTimestamp:J

    .line 311
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-le v0, v1, :cond_1

    .line 312
    iget-wide v1, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mostRecentTimestamp:J

    const-string/jumbo v4, "read = 0"

    const-string/jumbo v5, "date"

    move-object v0, p0

    move v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getSMSAlerts(JILjava/lang/String;Ljava/lang/String;Z)Ljava/util/LinkedList;

    move-result-object v7

    .line 317
    .local v7, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    :goto_0
    invoke-virtual {v7}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v8

    :goto_1
    iget-wide v1, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mostRecentTimestamp:J

    cmp-long v1, v1, v9

    if-eqz v1, :cond_3

    :goto_2
    and-int/2addr v0, v8

    if-eqz v0, :cond_0

    .line 320
    const-string/jumbo v0, "car_safereader_last_saferead_time"

    iget-wide v1, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mostRecentTimestamp:J

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Ljava/lang/String;J)V

    .line 323
    :cond_0
    return-object v7

    .line 314
    .end local v7    # "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    :cond_1
    iget-wide v1, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mostRecentTimestamp:J

    const-string/jumbo v4, "seen = 0"

    const-string/jumbo v5, "date"

    move-object v0, p0

    move v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getSMSAlerts(JILjava/lang/String;Ljava/lang/String;Z)Ljava/util/LinkedList;

    move-result-object v7

    .restart local v7    # "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    goto :goto_0

    :cond_2
    move v0, v6

    .line 317
    goto :goto_1

    :cond_3
    move v8, v6

    goto :goto_2
.end method

.method protected getNewSafereaderAlerts()Ljava/util/LinkedList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 332
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getNewSafereaderAlerts(IZ)Ljava/util/LinkedList;

    move-result-object v0

    return-object v0
.end method

.method public getNewSafereaderAlerts(IZ)Ljava/util/LinkedList;
    .locals 10
    .param p1, "maxToReturn"    # I
    .param p2, "updateQueryTime"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x1

    .line 345
    const-string/jumbo v0, "car_safereader_last_saferead_time"

    invoke-static {v0, v8, v9}, Lcom/vlingo/core/internal/settings/Settings;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mostRecentTimestamp:J

    .line 348
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-le v0, v1, :cond_1

    .line 349
    iget-wide v1, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mostRecentTimestamp:J

    const-string/jumbo v4, "read = 0"

    const-string/jumbo v5, "date"

    move-object v0, p0

    move v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getAlerts(JILjava/lang/String;Ljava/lang/String;Z)Ljava/util/LinkedList;

    move-result-object v7

    .line 355
    .local v7, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    :goto_0
    if-eqz p2, :cond_0

    .line 356
    invoke-virtual {v7}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mostRecentTimestamp:J

    cmp-long v0, v0, v8

    if-eqz v0, :cond_0

    .line 359
    const-string/jumbo v0, "car_safereader_last_saferead_time"

    iget-wide v1, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mostRecentTimestamp:J

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Ljava/lang/String;J)V

    .line 363
    :cond_0
    return-object v7

    .line 351
    .end local v7    # "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    :cond_1
    iget-wide v1, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mostRecentTimestamp:J

    const-string/jumbo v4, "seen = 0"

    const-string/jumbo v5, "date"

    move-object v0, p0

    move v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getAlerts(JILjava/lang/String;Ljava/lang/String;Z)Ljava/util/LinkedList;

    move-result-object v7

    .restart local v7    # "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    goto :goto_0
.end method

.method public getSMSAlerts(JILjava/lang/String;Ljava/lang/String;Z)Ljava/util/LinkedList;
    .locals 5
    .param p1, "queryTimeMS"    # J
    .param p3, "maxToReturn"    # I
    .param p4, "query"    # Ljava/lang/String;
    .param p5, "orderCol"    # Ljava/lang/String;
    .param p6, "ascending"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 231
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 233
    .local v0, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    move-object v2, p5

    .line 234
    .local v2, "sortOrder":Ljava/lang/String;
    if-eqz p6, :cond_0

    .line 235
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " ASC"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 242
    :goto_0
    invoke-direct {p0, p1, p2, p4, v2}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getSMSCursor(JLjava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 243
    .local v1, "smsCursor":Landroid/database/Cursor;
    const-string/jumbo v3, "SMS"

    const/4 v4, 0x0

    invoke-direct {p0, v1, v3, v4}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getNewAlertsOfType(Landroid/database/Cursor;Ljava/lang/String;Z)Ljava/util/LinkedList;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 244
    new-instance v3, Lcom/vlingo/core/internal/messages/SMSMMSProvider$2;

    invoke-direct {v3, p0, p6}, Lcom/vlingo/core/internal/messages/SMSMMSProvider$2;-><init>(Lcom/vlingo/core/internal/messages/SMSMMSProvider;Z)V

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 259
    if-lez p3, :cond_1

    .line 260
    :goto_1
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-le v3, p3, :cond_1

    .line 261
    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    goto :goto_1

    .line 237
    .end local v1    # "smsCursor":Landroid/database/Cursor;
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " DESC"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 264
    .restart local v1    # "smsCursor":Landroid/database/Cursor;
    :cond_1
    return-object v0
.end method

.method public getUnreadSMSAndMMSMessagesJayceeStyle()Ljava/util/LinkedList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/messages/SMSMMSAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 545
    const-string/jumbo v2, "car_safereader_last_saferead_time"

    const-wide/16 v3, 0x0

    invoke-static {v2, v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    const-wide/16 v4, 0x2710

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mostRecentTimestamp:J

    .line 547
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 549
    .local v0, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    const/4 v1, 0x0

    .line 554
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-wide v2, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mostRecentTimestamp:J

    const-string/jumbo v4, "seen = 0"

    const-string/jumbo v5, "date DESC"

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getMMSCursor(JLjava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 555
    const-string/jumbo v2, "MMS"

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getNewAlertsOfType(Landroid/database/Cursor;Ljava/lang/String;Z)Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 557
    if-eqz v1, :cond_0

    .line 558
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 565
    :cond_0
    :try_start_1
    iget-wide v2, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mostRecentTimestamp:J

    const-string/jumbo v4, "seen = 0"

    const-string/jumbo v5, "date DESC"

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getSMSCursor(JLjava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 566
    const-string/jumbo v2, "SMS"

    const/4 v3, 0x1

    invoke-direct {p0, v1, v2, v3}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getNewAlertsOfType(Landroid/database/Cursor;Ljava/lang/String;Z)Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 568
    if-eqz v1, :cond_1

    .line 569
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 575
    :cond_1
    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 578
    const-string/jumbo v2, "car_safereader_last_saferead_time"

    iget-wide v3, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mostRecentTimestamp:J

    invoke-static {v2, v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Ljava/lang/String;J)V

    .line 581
    :cond_2
    return-object v0

    .line 557
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_3

    .line 558
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2

    .line 568
    :catchall_1
    move-exception v2

    if-eqz v1, :cond_4

    .line 569
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v2
.end method

.method public markAsRead(Landroid/content/Context;JLjava/lang/String;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "smsID"    # J
    .param p4, "messageType"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 404
    const/16 v2, 0x13

    .line 406
    .local v2, "kitkat":I
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v7, v2, :cond_3

    .line 407
    sget-object v5, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->SMS_URI:Landroid/net/Uri;

    .line 408
    .local v5, "uri":Landroid/net/Uri;
    const-string/jumbo v7, "MMS"

    invoke-virtual {p4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 409
    sget-object v5, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->MMS_URI:Landroid/net/Uri;

    .line 412
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 413
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 414
    .local v6, "values":Landroid/content/ContentValues;
    const-string/jumbo v7, "read"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 416
    const-string/jumbo v7, "seen"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 417
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "_id = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 418
    .local v4, "updateCount":I
    if-eq v4, v9, :cond_1

    .line 423
    :cond_1
    new-array v3, v9, [I

    long-to-int v7, p2

    aput v7, v3, v10

    .line 425
    .local v3, "msgID":[I
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v7, "com.samsung.accessory.intent.action.CHECK_NOTIFICATION_ITEM"

    invoke-direct {v1, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 426
    .local v1, "i":Landroid/content/Intent;
    const-string/jumbo v7, "NOTIFICATION_PACKAGE_NAME"

    const-string/jumbo v8, "com.android.mms"

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 427
    const-string/jumbo v7, "NOTIFICATION_ITEM_ID"

    invoke-virtual {v1, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 428
    const-string/jumbo v7, "MMS"

    invoke-virtual {v7, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 429
    const-string/jumbo v7, "NOTIFICATION_ITEM_URI"

    const-string/jumbo v8, "content://mms/"

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 434
    :goto_0
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 451
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v4    # "updateCount":I
    .end local v5    # "uri":Landroid/net/Uri;
    .end local v6    # "values":Landroid/content/ContentValues;
    :goto_1
    return-void

    .line 431
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    .restart local v4    # "updateCount":I
    .restart local v5    # "uri":Landroid/net/Uri;
    .restart local v6    # "values":Landroid/content/ContentValues;
    :cond_2
    const-string/jumbo v7, "NOTIFICATION_ITEM_URI"

    const-string/jumbo v8, "content://sms/"

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 438
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v1    # "i":Landroid/content/Intent;
    .end local v3    # "msgID":[I
    .end local v4    # "updateCount":I
    .end local v5    # "uri":Landroid/net/Uri;
    .end local v6    # "values":Landroid/content/ContentValues;
    :cond_3
    new-array v3, v9, [I

    long-to-int v7, p2

    aput v7, v3, v10

    .line 440
    .restart local v3    # "msgID":[I
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v7, "com.samsung.accessory.intent.action.UPDATE_NOTIFICATION_ITEM"

    invoke-direct {v1, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 441
    .restart local v1    # "i":Landroid/content/Intent;
    const-string/jumbo v7, "NOTIFICATION_PACKAGE_NAME"

    const-string/jumbo v8, "com.android.mms"

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 442
    const-string/jumbo v7, "NOTIFICATION_ITEM_ID"

    invoke-virtual {v1, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 443
    const-string/jumbo v7, "MMS"

    invoke-virtual {v7, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 444
    const-string/jumbo v7, "NOTIFICATION_ITEM_URI"

    const-string/jumbo v8, "content://mms"

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 449
    :goto_2
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 446
    :cond_4
    const-string/jumbo v7, "NOTIFICATION_ITEM_URI"

    const-string/jumbo v8, "content://sms/"

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2
.end method

.method public registerSMSMMSContentObserver(Landroid/database/ContentObserver;)V
    .locals 3
    .param p1, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->MESSAGE_CONVERSATION_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 121
    return-void
.end method

.method public resetMsgReadoutTimestamp()V
    .locals 4

    .prologue
    .line 878
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 879
    .local v0, "lastSafereaderTime":Ljava/util/Date;
    const-string/jumbo v1, "car_safereader_last_saferead_time"

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Ljava/lang/String;J)V

    .line 880
    const-string/jumbo v1, "car_safereader_last_saferead_time"

    const-wide/16 v2, 0x0

    invoke-static {v1, v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mostRecentTimestamp:J

    .line 881
    return-void
.end method

.method setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mContext:Landroid/content/Context;

    .line 130
    return-void
.end method

.method public unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 125
    return-void
.end method
