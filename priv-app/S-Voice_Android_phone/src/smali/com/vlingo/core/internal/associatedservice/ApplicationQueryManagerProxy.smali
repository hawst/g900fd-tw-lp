.class public Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManagerProxy;
.super Ljava/lang/Object;
.source "ApplicationQueryManagerProxy.java"

# interfaces
.implements Lcom/vlingo/core/facade/associatedservice/IApplicationQueryManagerProxy;


# instance fields
.field private final proxyVersion:I

.field protected successor:Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManagerProxy;


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "proxyVersion"    # I

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput p1, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManagerProxy;->proxyVersion:I

    .line 15
    return-void
.end method


# virtual methods
.method protected getCompatibilityValue(Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "queryManager"    # Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManagerProxy;->successor:Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManagerProxy;

    if-eqz v0, :cond_0

    .line 23
    iget-object v0, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManagerProxy;->successor:Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManagerProxy;

    invoke-virtual {v0, p1, p2}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManagerProxy;->getValue(Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 25
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManagerProxy;

    iget v1, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManagerProxy;->proxyVersion:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManagerProxy;-><init>(I)V

    invoke-virtual {v0, p1, p2}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManagerProxy;->getValue(Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getValue(Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "queryManager"    # Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    iget v1, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManagerProxy;->proxyVersion:I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getProviderVersion()I

    move-result v2

    if-le v1, v2, :cond_0

    .line 32
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManagerProxy;->getCompatibilityValue(Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 35
    :goto_0
    return-object v1

    .line 34
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getApplicationContentProviderBase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v1, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManagerProxy;->proxyVersion:I

    if-lez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "?version="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManagerProxy;->proxyVersion:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 35
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getValueFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 34
    .end local v0    # "uri":Landroid/net/Uri;
    :cond_1
    const-string/jumbo v1, ""

    goto :goto_1
.end method

.method public setSuccessor(Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManagerProxy;)V
    .locals 0
    .param p1, "successor"    # Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManagerProxy;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManagerProxy;->successor:Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManagerProxy;

    .line 19
    return-void
.end method
