.class Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$5;
.super Ljava/lang/Object;
.source "PhraseSpotter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->onSeamlessPhraseSpotted(Ljava/lang/String;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

.field final synthetic val$micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

.field final synthetic val$spottedPhrase:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;Ljava/lang/String;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$5;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    iput-object p2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$5;->val$spottedPhrase:Ljava/lang/String;

    iput-object p3, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$5;->val$micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 362
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->stop()V

    .line 363
    iget-object v0, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$5;->this$0:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    # getter for: Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->listener:Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->access$900(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$5;->val$spottedPhrase:Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter$5;->val$micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;->onSeamlessPhraseSpotted(Ljava/lang/String;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    .line 364
    return-void
.end method
