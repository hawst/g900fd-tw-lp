.class final Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;
.super Landroid/os/Handler;
.source "ContactDBSyncManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SyncHandler"
.end annotation


# static fields
.field private static final HANDLE_END_TASK:I = 0x3

.field private static final HANDLE_NEW_TASK:I = 0x1

.field private static final INITIAL_DELAY:J = 0x1388L

.field private static final MAKE_TASK_READY:I = 0x2

.field private static final PARAM_REQUEST_ID:Ljava/lang/String; = "request_id"

.field private static final PARAM_SKIP_DELAY:Ljava/lang/String; = "skip_delay"

.field private static final WAIT_TIME_MS:J = 0x3a98L


# instance fields
.field private mLastHandledRequestId:I

.field private mListener:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$Listener;

.field private mReadyTask:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

.field private mRunningTask:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

.field private mWaitingTask:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

.field final synthetic this$0:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;Landroid/os/Looper;Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$Listener;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;
    .param p3, "listener"    # Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$Listener;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->this$0:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;

    .line 75
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 76
    iput-object p3, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->mListener:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$Listener;

    .line 77
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;Landroid/os/Looper;Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$Listener;Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;
    .param p2, "x1"    # Landroid/os/Looper;
    .param p3, "x2"    # Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$Listener;
    .param p4, "x3"    # Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$1;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;-><init>(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager;Landroid/os/Looper;Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$Listener;)V

    return-void
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;)Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->mListener:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$Listener;

    return-object v0
.end method

.method private handleNewTask(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;ZI)V
    .locals 3
    .param p1, "newTask"    # Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;
    .param p2, "skipDelay"    # Z
    .param p3, "requestId"    # I

    .prologue
    const/4 v2, 0x2

    .line 105
    iput p3, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->mLastHandledRequestId:I

    .line 107
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->mReadyTask:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

    if-eqz v1, :cond_0

    .line 119
    :goto_0
    return-void

    .line 111
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->mWaitingTask:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

    if-eqz v1, :cond_1

    .line 113
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->mWaitingTask:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

    invoke-virtual {p0, v2, v1}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->removeMessages(ILjava/lang/Object;)V

    .line 115
    :cond_1
    iput-object p1, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->mWaitingTask:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

    .line 116
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->mWaitingTask:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

    invoke-virtual {p0, v2, v1}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 117
    .local v0, "msg":Landroid/os/Message;
    if-eqz p2, :cond_2

    const-wide/16 v1, 0x1388

    :goto_1
    invoke-virtual {p0, v0, v1, v2}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_2
    const-wide/16 v1, 0x3a98

    goto :goto_1
.end method

.method private makeReady(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;)V
    .locals 1
    .param p1, "waitingTask"    # Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

    .prologue
    .line 130
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->mWaitingTask:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

    if-ne v0, p1, :cond_0

    .line 131
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->mWaitingTask:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

    .line 132
    iput-object p1, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->mReadyTask:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

    .line 133
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->runNextTask()V

    .line 135
    :cond_0
    return-void
.end method

.method private notifyComplete()V
    .locals 2

    .prologue
    .line 162
    iget v0, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->mLastHandledRequestId:I

    .line 163
    .local v0, "lastHandledRequestId":I
    new-instance v1, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler$1;

    invoke-direct {v1, p0, v0}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler$1;-><init>(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;I)V

    invoke-static {v1}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 170
    return-void
.end method

.method private runNextTask()V
    .locals 2

    .prologue
    .line 144
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->mRunningTask:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

    if-nez v1, :cond_0

    .line 145
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->mReadyTask:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

    if-eqz v1, :cond_1

    .line 146
    iget-object v0, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->mReadyTask:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

    .line 147
    .local v0, "nextTask":Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->mReadyTask:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

    .line 152
    iput-object v0, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->mRunningTask:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

    .line 153
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->mRunningTask:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

    # invokes: Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;->runTask()V
    invoke-static {v1}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;->access$200(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;)V

    .line 159
    .end local v0    # "nextTask":Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->mWaitingTask:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

    if-nez v1, :cond_0

    .line 156
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->notifyComplete()V

    goto :goto_0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 82
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 99
    :goto_0
    return-void

    .line 84
    :pswitch_0
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

    .line 85
    .local v3, "task":Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 86
    .local v0, "data":Landroid/os/Bundle;
    const-string/jumbo v4, "skip_delay"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 87
    .local v2, "skipDelay":Z
    const-string/jumbo v4, "request_id"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 88
    .local v1, "requestId":I
    invoke-direct {p0, v3, v2, v1}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->handleNewTask(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;ZI)V

    goto :goto_0

    .line 91
    .end local v0    # "data":Landroid/os/Bundle;
    .end local v1    # "requestId":I
    .end local v2    # "skipDelay":Z
    .end local v3    # "task":Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;
    :pswitch_1
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

    .line 92
    .restart local v3    # "task":Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;
    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->makeReady(Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;)V

    goto :goto_0

    .line 95
    .end local v3    # "task":Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;
    :pswitch_2
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->mRunningTask:Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncTask;

    .line 96
    invoke-direct {p0}, Lcom/vlingo/core/internal/lmtt/ContactDBSyncManager$SyncHandler;->runNextTask()V

    goto :goto_0

    .line 82
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
