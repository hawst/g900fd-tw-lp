.class public Lcom/vlingo/core/internal/dialogmanager/events/ChoiceSelectedEvent;
.super Lcom/vlingo/core/internal/dialogmanager/DialogEvent;
.source "ChoiceSelectedEvent.java"


# instance fields
.field private final NAME:Ljava/lang/String;

.field private final id:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;-><init>(ZZZ)V

    .line 16
    const-string/jumbo v0, "choice-selected"

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/ChoiceSelectedEvent;->NAME:Ljava/lang/String;

    .line 21
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/events/ChoiceSelectedEvent;->id:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    const-string/jumbo v0, "choice-selected"

    return-object v0
.end method

.method public getVLDialogEvent()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    .locals 3

    .prologue
    .line 31
    new-instance v0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    const-string/jumbo v1, "choice-selected"

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;-><init>(Ljava/lang/String;)V

    .line 32
    .local v0, "builder":Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;
    const-string/jumbo v1, "choice-uid"

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/events/ChoiceSelectedEvent;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 34
    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->build()Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;

    move-result-object v1

    return-object v1
.end method
