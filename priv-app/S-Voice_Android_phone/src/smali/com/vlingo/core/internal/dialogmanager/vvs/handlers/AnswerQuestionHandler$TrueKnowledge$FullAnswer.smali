.class public Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$TrueKnowledge$FullAnswer;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$Reaction;
.source "AnswerQuestionHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$TrueKnowledge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FullAnswer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$Reaction",
        "<",
        "Lcom/vlingo/core/internal/questions/Answer;",
        ">;",
        "Ljava/lang/Runnable;",
        "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/questions/Answer;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V
    .locals 0
    .param p1, "answer"    # Lcom/vlingo/core/internal/questions/Answer;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .param p3, "turn"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .prologue
    .line 367
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$Reaction;-><init>(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 368
    return-void
.end method


# virtual methods
.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 383
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$TrueKnowledge$FullAnswer;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowTrueKnowledgeWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$TrueKnowledge$FullAnswer;->getAnswer()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v3, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 385
    return-void
.end method

.method public run()V
    .locals 4

    .prologue
    .line 372
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$TrueKnowledge$FullAnswer;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 378
    :goto_0
    return-void

    .line 374
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$TrueKnowledge$FullAnswer;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$TrueKnowledge$FullAnswer;->getAnswer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/questions/Answer;

    invoke-interface {v0}, Lcom/vlingo/core/internal/questions/Answer;->getSimpleResponse()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$TrueKnowledge$FullAnswer;->getAnswer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/questions/Answer;

    invoke-interface {v0}, Lcom/vlingo/core/internal/questions/Answer;->getSimpleResponse()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler$TrueKnowledge$FullAnswer;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowButton:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v2, 0x0

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->moreButtonText:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto :goto_0
.end method
