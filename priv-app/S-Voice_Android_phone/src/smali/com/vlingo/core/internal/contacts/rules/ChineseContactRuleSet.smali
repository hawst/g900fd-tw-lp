.class public Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;
.super Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;
.source "ChineseContactRuleSet.java"


# static fields
.field private static final CHINESE_TITLES:Ljava/lang/String; = "\u7238|\u5988|\u5144|\u7237|\u5976|\u59e5|\u53d4|\u4f84|\u5ac2|\u8205|\u592a|\u8463|\u7239|\u5a18|\u603b|\u5de5|\u5bfc|\u526f|\u53a8|\u59e8|\u5a76|\u5c40|\u68c0|\u961f|\u59d0|\u54e5|\u5f1f|\u59b9|\u5c0f\u59d0|\u592a\u592a|\u5973\u58eb|\u8001\u7237 |\u5148\u751f|\u8001\u5e08 |\u592b\u4eba|\u7239\u5730|\u5927\u6b3e|\u571f\u8c6a|\u5bcc\u5a46|\u5e05\u54e5|\u5e08\u516c|\u5f92\u5f1f|\u54e5\u4eec|\u59d0\u4eec|\u95fa\u871c|\u9753\u5973|\u9753\u4ed4|\u670b\u53cb|\u5934\u513f|\u9886\u5bfc|\u8001\u677f|\u8001\u5916|\u4e3b\u4efb|\u6821\u957f|\u535a\u58eb|\u7855\u58eb|\u6559\u6388|\u8bb2\u5e08|\u79d1\u957f|\u957f\u5b98|\u5904\u957f|\u7ecf\u7406|\u5e97\u957f|\u5bb6\u957f|\u540c\u5b66|\u8001\u5f1f|\u5c0f\u5f1f|\u53f8\u673a|\u4e0a\u5c06|\u4e0a\u5c09|\u4e0a\u6821|\u4e13\u5458|\u4e2d\u533b|\u4e2d\u5c06|\u4e2d\u5c09|\u4e2d\u6821|\u4e3b\u5e2d|\u4e3b\u6559|\u4e3b\u7ba1|\u4e61\u957f|\u4ee3\u8868|\u4f1a\u957f|\u4f2f\u4f2f|\u4fa6\u63a2|\u4fee\u5973|\u4fee\u7a97|\u4fee\u8f66|\u5144\u5f1f|\u515a\u5458|\u516c\u4e3b|\u516c\u5b89|\u51c6\u5c06|\u5236\u7247|\u526f\u53bf|\u526f\u56e2|\u526f\u5e08|\u526f\u603b|\u526f\u65c5|\u526f\u73ed|\u526f\u8425|\u52a9\u7406|\u5382\u957f|\u5385\u957f|\u53a8\u5b50|\u53a8\u5e08|\u53bf\u957f|\u53f8\u4ee4|\u53f8\u957f|\u540c\u5fd7|\u56e2\u957f|\u5927\u4eba|\u5927\u4f7f|\u5927\u53d4|\u5927\u5988|\u5927\u5a18|\u5927\u5e08|\u5927\u7237|\u59b9\u59b9|\u59b9\u5b50|\u59ca\u59b9|\u59d0\u59b9|\u59d4\u5458|\u59e5\u59e5|\u59e5\u7237|\u59e8\u59e8|\u5a76\u5a76|\u5b66\u751f|\u5b98\u5458|\u5bfc\u6f14|\u5c06\u519b|\u5c0f\u59b9|\u5c11\u5c06|\u5c11\u5c09|\u5c11\u6821|\u5dde\u957f|\u5e02\u957f|\u5e08\u5085|\u5e08\u7236|\u5e08\u957f|\u5e38\u59d4|\u5f1f\u5144|\u5f8b\u5e08|\u603b\u5de5|\u603b\u7406|\u603b\u76d1|\u603b\u7763|\u603b\u7edf|\u603b\u88c1|\u6240\u957f|\u62a4\u58eb|\u62a4\u7532|\u63a5\u673a|\u642c\u8fd0|\u652f\u4e66|\u65c5\u957f|\u6751\u6c11|\u6751\u957f|\u6821\u76d1|\u6821\u8463|\u6cd5\u5b98|\u6cd5\u5e08|\u6f14\u5458|\u7267\u5e08|\u73ed\u957f|\u7406\u53d1|\u7535\u5de5|\u76d1\u5236|\u76d1\u7763|\u7701\u957f|\u7763\u5bdf|\u793e\u957f|\u795e\u7236|\u798f\u68c0|\u79d8\u4e66|\u7ba1\u5bb6|\u7f16\u5267|\u7f8e\u5973|\u7f8e\u5bb9|\u7f8e\u7709|\u8001\u5e08|\u8205\u8205|\u8239\u957f|\u8425\u957f|\u897f\u533b|\u8b66\u53f8|\u8b66\u5458|\u8b66\u5bdf|\u8b66\u957f|\u9053\u58eb|\u9053\u957f|\u9500\u552e|\u9547\u957f|\u961f\u957f|\u963f\u7238|\u9662\u58eb|\u9753\u59b9|\u9886\u8896|\u9996\u5e2d|\u9996\u76f8|\u52a9\u6559|\u5e72\u5988|\u5e72\u7239|\u5e08\u59d0|\u5e08\u54e5|\u5e08\u5144|\u5e08\u5f1f|\u5e08\u59b9|\u4ee3\u7406|\u53d4\u53d4|\u963f\u59e8|\u5e08\u6bcd|\u8bd7\u4e08|\u4e66\u8bb0|\u5c40\u957f|\u9662\u957f|\u533b\u5e08|\u4f1a\u8ba1|\u90e8\u957f|\u8205\u6bcd|\u8205\u7236|\u8868\u5f1f|\u8868\u54e5|\u8868\u59d0|\u8868\u59b9|\u987e\u95ee|\u7968\u53cb|\u4e0a\u53f8|\u5927\u592b|\u5bfc\u6e38|\u9886\u961f|\u6559\u7ec3|\u5988\u54aa|\u5ab3\u5987|\u5a92\u5a46|\u8b66\u5b98|\u7ea2\u5a18|\u592a\u592a|\u9a74\u53cb|\u5c0f\u4e09|\u8001\u5927|\u8001\u4e8c|\u8001\u516c|\u8001\u5a46|\u8001\u603b|\u8001\u4e61|\u8001\u7238|\u8001\u5988|\u7231\u4eba|\u516c\u516c|\u5a46\u5a46|\u5988\u5988|\u5b9d\u8d1d|\u5b9d\u5b9d|\u7238\u7238|\u7237\u7237|\u5976\u5976|\u5916\u516c|\u5916\u5a46|\u987e\u5ba2|\u5ba2\u6237|\u5927\u59d0|\u5927\u54e5|\u59d0\u59d0|\u54e5\u54e5|\u59d0\u592b|\u5ac2\u5b50|\u5f1f\u5f1f|\u5f1f\u59b9|\u5f92\u5f1f|\u8001\u4f34|\u90bb\u5c45|\u9886\u73ed|\u5e97\u4e3b|\u5ba2\u4eba|\u623f\u4e1c|\u753b\u5bb6|\u96c7\u4e3b|\u4fdd\u59c6|\u533b\u751f|\u603b\u7ecf\u7406|\u8463\u4e8b\u957f|\u4e3b\u6301\u4eba|\u4e8c\u5e08\u5f1f|\u5236\u7247\u4eba|\u8d5e\u52a9\u5546|\u9886\u5bfc\u4eba|\u5927\u5e08\u5144|\u5c0f\u8001\u677f|\u88c5\u4fee\u5de5|\u8f85\u5bfc\u5458|\u9547\u4e66\u8bb0|\u7cfb\u4e3b\u4efb|\u6536\u85cf\u5bb6|\u751f\u4ea7\u5546|\u6444\u5f71\u5e08|\u8001\u670b\u53cb|\u8001\u592a\u592a|\u8001\u7237\u5b50|\u8001\u540c\u5b66|\u8001\u90e8\u4e0b|\u8001\u4e0a\u7ea7|\u8001\u9886\u5bfc|\u8001\u9996\u957f|\u5f00\u53d1\u5546|\u91d1\u878d\u5bb6|\u5e7f\u544a\u5546|\u4f9b\u5e94\u5546|\u5c0f\u5e08\u59b9|\u5c0f\u5e08\u5f1f|\u4e8c\u628a\u624b|\u4e00\u628a\u624b|\u623f\u4ea7\u5546|\u5927\u5e08\u59d0|\u51fa\u7248\u5546|\u4e8c\u5e08\u5144|\u5927\u8001\u677f|\u5927\u660e\u661f|\u627f\u5305\u4eba|\u627f\u5305\u5546|\u7f8e\u5bb9\u5e08|\u4e3b\u8bb2\u4eba|\u88c1\u5224\u5b98|\u7ecf\u7eaa\u4eba|\u4f01\u4e1a\u5bb6|\u751f\u4ea7\u5546|\u4f1a\u8ba1\u5e08|\u5de5\u7a0b\u5e08|\u5927\u5f53\u5bb6|\u4e8c\u5f53\u5bb6|\u4fdd\u7ba1\u5458|\u515a\u652f\u4e66|\u526f\u4e3b\u4efb|\u526f\u4e3b\u5e2d|\u526f\u4e61\u957f|\u526f\u4e66\u8bb0|\u526f\u4f1a\u957f|\u526f\u5382\u957f|\u526f\u5385\u957f|\u526f\u53f8\u4ee4|\u526f\u5904\u957f|\u526f\u5dde\u957f|\u526f\u5e02\u957f|\u526f\u603b\u7406|\u526f\u603b\u7edf|\u526f\u603b\u88c1|\u526f\u6240\u957f|\u526f\u6559\u6388|\u526f\u6751\u957f|\u526f\u6821\u957f|\u526f\u7701\u957f|\u526f\u793e\u957f|\u526f\u7ecf\u7406|\u526f\u90e8\u957f|\u526f\u9547\u957f|\u526f\u9662\u957f|\u5305\u5de5\u5934|\u53c2\u8bae\u5458|\u53c2\u8c0b\u957f|\u54a8\u8be2\u5e08|\u56e2\u652f\u4e66|\u5927\u5e08\u7236|\u5927\u5f8b\u5e08|\u5927\u6cd5\u5b98|\u5973\u4e3b\u4eba|\u59d4\u5458\u957f|\u5efa\u7b51\u5e08|\u603b\u4e66\u8bb0|\u603b\u53f8\u4ee4|\u603b\u987e\u95ee|\u62a4\u58eb\u957f|\u642c\u8fd0\u5de5|\u68c0\u5bdf\u5b98|\u68c0\u5bdf\u957f|\u68c0\u67e5\u957f|\u6c7d\u4fee\u5de5|\u6e05\u6d01\u5de5|\u7406\u53d1\u5e08|\u7535\u710a\u5de5|\u79d8\u4e66\u957f|\u7a0e\u52a1\u957f|\u7ba1\u7406\u5458|\u4e13\u79d1\u533b\u751f|\u4ea4\u901a\u90e8\u957f|\u4eba\u5927\u4ee3\u8868|\u515a\u59d4\u4e66\u8bb0|\u526f\u603b\u4e66\u8bb0|\u526f\u603b\u53f8\u4ee4|\u526f\u603b\u7ecf\u7406|\u526f\u68c0\u5bdf\u957f|\u526f\u8463\u4e8b\u957f|\u52a9\u7406\u6559\u6388|\u56fd\u9632\u90e8\u957f|\u5916\u4ea4\u90e8\u957f|\u5916\u79d1\u533b\u751f|\u59d4\u5458\u4e13\u5458|\u5bb6\u5ead\u533b\u751f|\u5dde\u53c2\u8bae\u5458|\u603b\u5de5\u7a0b\u5e08|\u603b\u68c0\u5bdf\u957f|\u6267\u884c\u8463\u4e8b|\u653f\u52a1\u52a9\u7406|\u6559\u5b66\u79d8\u4e66|\u6559\u80b2\u90e8\u957f|\u7a7a\u519b\u4e0a\u5c09|\u7ecf\u7406\u52a9\u7406|\u8363\u8a89\u535a\u58eb|\u8363\u8a89\u6559\u6388|\u884c\u653f\u52a9\u7406|\u884c\u653f\u6cd5\u5b98|\u884c\u653f\u79d8\u4e66|\u884c\u653f\u957f\u5b98|\u8f66\u95f4\u4e3b\u4efb|\u9500\u552e\u7ecf\u7406|\u9879\u76ee\u7ecf\u7406|\u9996\u5e2d\u6cd5\u5b98|\u9ad8\u7ea7\u4e13\u5458"

.field private static final CHINESE_TITLES_PATTERN:Ljava/util/regex/Pattern;

.field private static DEFAULT_FIELDS:[Ljava/lang/String;

.field private static FAMILY_NAME_FIELDS:[Ljava/lang/String;

.field private static GIVEN_NAME_FIELDS:[Ljava/lang/String;

.field private static final HONORIFIC_POSTFIXES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private criteria:Lcom/vlingo/core/internal/contacts/ContactExitCriteria;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 48
    const-string/jumbo v0, "^(.{1,}?)(\u7238|\u5988|\u5144|\u7237|\u5976|\u59e5|\u53d4|\u4f84|\u5ac2|\u8205|\u592a|\u8463|\u7239|\u5a18|\u603b|\u5de5|\u5bfc|\u526f|\u53a8|\u59e8|\u5a76|\u5c40|\u68c0|\u961f|\u59d0|\u54e5|\u5f1f|\u59b9|\u5c0f\u59d0|\u592a\u592a|\u5973\u58eb|\u8001\u7237 |\u5148\u751f|\u8001\u5e08 |\u592b\u4eba|\u7239\u5730|\u5927\u6b3e|\u571f\u8c6a|\u5bcc\u5a46|\u5e05\u54e5|\u5e08\u516c|\u5f92\u5f1f|\u54e5\u4eec|\u59d0\u4eec|\u95fa\u871c|\u9753\u5973|\u9753\u4ed4|\u670b\u53cb|\u5934\u513f|\u9886\u5bfc|\u8001\u677f|\u8001\u5916|\u4e3b\u4efb|\u6821\u957f|\u535a\u58eb|\u7855\u58eb|\u6559\u6388|\u8bb2\u5e08|\u79d1\u957f|\u957f\u5b98|\u5904\u957f|\u7ecf\u7406|\u5e97\u957f|\u5bb6\u957f|\u540c\u5b66|\u8001\u5f1f|\u5c0f\u5f1f|\u53f8\u673a|\u4e0a\u5c06|\u4e0a\u5c09|\u4e0a\u6821|\u4e13\u5458|\u4e2d\u533b|\u4e2d\u5c06|\u4e2d\u5c09|\u4e2d\u6821|\u4e3b\u5e2d|\u4e3b\u6559|\u4e3b\u7ba1|\u4e61\u957f|\u4ee3\u8868|\u4f1a\u957f|\u4f2f\u4f2f|\u4fa6\u63a2|\u4fee\u5973|\u4fee\u7a97|\u4fee\u8f66|\u5144\u5f1f|\u515a\u5458|\u516c\u4e3b|\u516c\u5b89|\u51c6\u5c06|\u5236\u7247|\u526f\u53bf|\u526f\u56e2|\u526f\u5e08|\u526f\u603b|\u526f\u65c5|\u526f\u73ed|\u526f\u8425|\u52a9\u7406|\u5382\u957f|\u5385\u957f|\u53a8\u5b50|\u53a8\u5e08|\u53bf\u957f|\u53f8\u4ee4|\u53f8\u957f|\u540c\u5fd7|\u56e2\u957f|\u5927\u4eba|\u5927\u4f7f|\u5927\u53d4|\u5927\u5988|\u5927\u5a18|\u5927\u5e08|\u5927\u7237|\u59b9\u59b9|\u59b9\u5b50|\u59ca\u59b9|\u59d0\u59b9|\u59d4\u5458|\u59e5\u59e5|\u59e5\u7237|\u59e8\u59e8|\u5a76\u5a76|\u5b66\u751f|\u5b98\u5458|\u5bfc\u6f14|\u5c06\u519b|\u5c0f\u59b9|\u5c11\u5c06|\u5c11\u5c09|\u5c11\u6821|\u5dde\u957f|\u5e02\u957f|\u5e08\u5085|\u5e08\u7236|\u5e08\u957f|\u5e38\u59d4|\u5f1f\u5144|\u5f8b\u5e08|\u603b\u5de5|\u603b\u7406|\u603b\u76d1|\u603b\u7763|\u603b\u7edf|\u603b\u88c1|\u6240\u957f|\u62a4\u58eb|\u62a4\u7532|\u63a5\u673a|\u642c\u8fd0|\u652f\u4e66|\u65c5\u957f|\u6751\u6c11|\u6751\u957f|\u6821\u76d1|\u6821\u8463|\u6cd5\u5b98|\u6cd5\u5e08|\u6f14\u5458|\u7267\u5e08|\u73ed\u957f|\u7406\u53d1|\u7535\u5de5|\u76d1\u5236|\u76d1\u7763|\u7701\u957f|\u7763\u5bdf|\u793e\u957f|\u795e\u7236|\u798f\u68c0|\u79d8\u4e66|\u7ba1\u5bb6|\u7f16\u5267|\u7f8e\u5973|\u7f8e\u5bb9|\u7f8e\u7709|\u8001\u5e08|\u8205\u8205|\u8239\u957f|\u8425\u957f|\u897f\u533b|\u8b66\u53f8|\u8b66\u5458|\u8b66\u5bdf|\u8b66\u957f|\u9053\u58eb|\u9053\u957f|\u9500\u552e|\u9547\u957f|\u961f\u957f|\u963f\u7238|\u9662\u58eb|\u9753\u59b9|\u9886\u8896|\u9996\u5e2d|\u9996\u76f8|\u52a9\u6559|\u5e72\u5988|\u5e72\u7239|\u5e08\u59d0|\u5e08\u54e5|\u5e08\u5144|\u5e08\u5f1f|\u5e08\u59b9|\u4ee3\u7406|\u53d4\u53d4|\u963f\u59e8|\u5e08\u6bcd|\u8bd7\u4e08|\u4e66\u8bb0|\u5c40\u957f|\u9662\u957f|\u533b\u5e08|\u4f1a\u8ba1|\u90e8\u957f|\u8205\u6bcd|\u8205\u7236|\u8868\u5f1f|\u8868\u54e5|\u8868\u59d0|\u8868\u59b9|\u987e\u95ee|\u7968\u53cb|\u4e0a\u53f8|\u5927\u592b|\u5bfc\u6e38|\u9886\u961f|\u6559\u7ec3|\u5988\u54aa|\u5ab3\u5987|\u5a92\u5a46|\u8b66\u5b98|\u7ea2\u5a18|\u592a\u592a|\u9a74\u53cb|\u5c0f\u4e09|\u8001\u5927|\u8001\u4e8c|\u8001\u516c|\u8001\u5a46|\u8001\u603b|\u8001\u4e61|\u8001\u7238|\u8001\u5988|\u7231\u4eba|\u516c\u516c|\u5a46\u5a46|\u5988\u5988|\u5b9d\u8d1d|\u5b9d\u5b9d|\u7238\u7238|\u7237\u7237|\u5976\u5976|\u5916\u516c|\u5916\u5a46|\u987e\u5ba2|\u5ba2\u6237|\u5927\u59d0|\u5927\u54e5|\u59d0\u59d0|\u54e5\u54e5|\u59d0\u592b|\u5ac2\u5b50|\u5f1f\u5f1f|\u5f1f\u59b9|\u5f92\u5f1f|\u8001\u4f34|\u90bb\u5c45|\u9886\u73ed|\u5e97\u4e3b|\u5ba2\u4eba|\u623f\u4e1c|\u753b\u5bb6|\u96c7\u4e3b|\u4fdd\u59c6|\u533b\u751f|\u603b\u7ecf\u7406|\u8463\u4e8b\u957f|\u4e3b\u6301\u4eba|\u4e8c\u5e08\u5f1f|\u5236\u7247\u4eba|\u8d5e\u52a9\u5546|\u9886\u5bfc\u4eba|\u5927\u5e08\u5144|\u5c0f\u8001\u677f|\u88c5\u4fee\u5de5|\u8f85\u5bfc\u5458|\u9547\u4e66\u8bb0|\u7cfb\u4e3b\u4efb|\u6536\u85cf\u5bb6|\u751f\u4ea7\u5546|\u6444\u5f71\u5e08|\u8001\u670b\u53cb|\u8001\u592a\u592a|\u8001\u7237\u5b50|\u8001\u540c\u5b66|\u8001\u90e8\u4e0b|\u8001\u4e0a\u7ea7|\u8001\u9886\u5bfc|\u8001\u9996\u957f|\u5f00\u53d1\u5546|\u91d1\u878d\u5bb6|\u5e7f\u544a\u5546|\u4f9b\u5e94\u5546|\u5c0f\u5e08\u59b9|\u5c0f\u5e08\u5f1f|\u4e8c\u628a\u624b|\u4e00\u628a\u624b|\u623f\u4ea7\u5546|\u5927\u5e08\u59d0|\u51fa\u7248\u5546|\u4e8c\u5e08\u5144|\u5927\u8001\u677f|\u5927\u660e\u661f|\u627f\u5305\u4eba|\u627f\u5305\u5546|\u7f8e\u5bb9\u5e08|\u4e3b\u8bb2\u4eba|\u88c1\u5224\u5b98|\u7ecf\u7eaa\u4eba|\u4f01\u4e1a\u5bb6|\u751f\u4ea7\u5546|\u4f1a\u8ba1\u5e08|\u5de5\u7a0b\u5e08|\u5927\u5f53\u5bb6|\u4e8c\u5f53\u5bb6|\u4fdd\u7ba1\u5458|\u515a\u652f\u4e66|\u526f\u4e3b\u4efb|\u526f\u4e3b\u5e2d|\u526f\u4e61\u957f|\u526f\u4e66\u8bb0|\u526f\u4f1a\u957f|\u526f\u5382\u957f|\u526f\u5385\u957f|\u526f\u53f8\u4ee4|\u526f\u5904\u957f|\u526f\u5dde\u957f|\u526f\u5e02\u957f|\u526f\u603b\u7406|\u526f\u603b\u7edf|\u526f\u603b\u88c1|\u526f\u6240\u957f|\u526f\u6559\u6388|\u526f\u6751\u957f|\u526f\u6821\u957f|\u526f\u7701\u957f|\u526f\u793e\u957f|\u526f\u7ecf\u7406|\u526f\u90e8\u957f|\u526f\u9547\u957f|\u526f\u9662\u957f|\u5305\u5de5\u5934|\u53c2\u8bae\u5458|\u53c2\u8c0b\u957f|\u54a8\u8be2\u5e08|\u56e2\u652f\u4e66|\u5927\u5e08\u7236|\u5927\u5f8b\u5e08|\u5927\u6cd5\u5b98|\u5973\u4e3b\u4eba|\u59d4\u5458\u957f|\u5efa\u7b51\u5e08|\u603b\u4e66\u8bb0|\u603b\u53f8\u4ee4|\u603b\u987e\u95ee|\u62a4\u58eb\u957f|\u642c\u8fd0\u5de5|\u68c0\u5bdf\u5b98|\u68c0\u5bdf\u957f|\u68c0\u67e5\u957f|\u6c7d\u4fee\u5de5|\u6e05\u6d01\u5de5|\u7406\u53d1\u5e08|\u7535\u710a\u5de5|\u79d8\u4e66\u957f|\u7a0e\u52a1\u957f|\u7ba1\u7406\u5458|\u4e13\u79d1\u533b\u751f|\u4ea4\u901a\u90e8\u957f|\u4eba\u5927\u4ee3\u8868|\u515a\u59d4\u4e66\u8bb0|\u526f\u603b\u4e66\u8bb0|\u526f\u603b\u53f8\u4ee4|\u526f\u603b\u7ecf\u7406|\u526f\u68c0\u5bdf\u957f|\u526f\u8463\u4e8b\u957f|\u52a9\u7406\u6559\u6388|\u56fd\u9632\u90e8\u957f|\u5916\u4ea4\u90e8\u957f|\u5916\u79d1\u533b\u751f|\u59d4\u5458\u4e13\u5458|\u5bb6\u5ead\u533b\u751f|\u5dde\u53c2\u8bae\u5458|\u603b\u5de5\u7a0b\u5e08|\u603b\u68c0\u5bdf\u957f|\u6267\u884c\u8463\u4e8b|\u653f\u52a1\u52a9\u7406|\u6559\u5b66\u79d8\u4e66|\u6559\u80b2\u90e8\u957f|\u7a7a\u519b\u4e0a\u5c09|\u7ecf\u7406\u52a9\u7406|\u8363\u8a89\u535a\u58eb|\u8363\u8a89\u6559\u6388|\u884c\u653f\u52a9\u7406|\u884c\u653f\u6cd5\u5b98|\u884c\u653f\u79d8\u4e66|\u884c\u653f\u957f\u5b98|\u8f66\u95f4\u4e3b\u4efb|\u9500\u552e\u7ecf\u7406|\u9879\u76ee\u7ecf\u7406|\u9996\u5e2d\u6cd5\u5b98|\u9ad8\u7ea7\u4e13\u5458)?$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->CHINESE_TITLES_PATTERN:Ljava/util/regex/Pattern;

    .line 50
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "data1"

    aput-object v1, v0, v2

    const-string/jumbo v1, "data9"

    aput-object v1, v0, v3

    const-string/jumbo v1, "data7"

    aput-object v1, v0, v4

    const-string/jumbo v1, "data9 || data7"

    aput-object v1, v0, v5

    const-string/jumbo v1, "data7 || data9"

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->DEFAULT_FIELDS:[Ljava/lang/String;

    .line 58
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "data1"

    aput-object v1, v0, v2

    const-string/jumbo v1, "phonetic_name"

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    .line 63
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "data3"

    aput-object v1, v0, v2

    const-string/jumbo v1, "data9"

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->FAMILY_NAME_FIELDS:[Ljava/lang/String;

    .line 92
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "\u5c0f\u59d0"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\u592a\u592a"

    aput-object v1, v0, v3

    const-string/jumbo v1, "\u5973\u58eb"

    aput-object v1, v0, v4

    const-string/jumbo v1, "\u8001\u7237"

    aput-object v1, v0, v5

    const-string/jumbo v1, "\u5148\u751f"

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string/jumbo v2, "\u8001\u5e08"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "\u592b\u4eba"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->HONORIFIC_POSTFIXES:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/rules/ContactRuleSet;-><init>()V

    .line 69
    new-instance v0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet$1;-><init>(Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;Z)V

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->criteria:Lcom/vlingo/core/internal/contacts/ContactExitCriteria;

    .line 75
    return-void
.end method

.method private generateRulesQueryLength1(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 6
    .param p1, "query"    # Ljava/lang/String;
    .param p3, "normalizedQuery"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactRule;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 277
    .local p2, "rules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactRule;>;"
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Partial Match only first name"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, p1, v3}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "__"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "___"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;

    const/16 v4, 0x64

    invoke-direct {p0, p1, p3}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->prepareRegExForQuery(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5, p3}, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    return-void
.end method

.method private generateRulesQueryLength2(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "query"    # Ljava/lang/String;
    .param p3, "strippedQuery"    # Ljava/lang/String;
    .param p4, "normalizedQuery"    # Ljava/lang/String;
    .param p5, "normalizedStripprdQuery"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactRule;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "rules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactRule;>;"
    const/16 v7, 0x3c

    const/16 v6, 0x64

    const/4 v5, 0x0

    .line 248
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Partial Match on First Name"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, p1, v3}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "__"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;

    invoke-direct {p0, p1, p4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->prepareRegExForQuery(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v6, v4, p4}, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    invoke-virtual {p3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 255
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Partial Match only first name"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->FAMILY_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, p3, v3}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->FAMILY_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "__"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->FAMILY_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "___"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->FAMILY_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;

    invoke-direct {p0, p3, p5}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->prepareRegExForQuery(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v7, v4, p5}, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 261
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Partial Match only last name"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, p3, v3}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "__"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "__"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;

    invoke-direct {p0, p3, p5}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->prepareRegExForQuery(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v7, v4, p5}, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 268
    :cond_0
    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v0, v1, :cond_1

    .line 271
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Partial Match doubling first or second hieroglyph"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_;_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v3, v6}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 272
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Partial Match first name with last name"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v3, v6}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 274
    :cond_1
    return-void
.end method

.method private generateRulesQueryLength3(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 8
    .param p1, "query"    # Ljava/lang/String;
    .param p3, "strippedQuery"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactRule;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "rules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactRule;>;"
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/16 v5, 0x64

    .line 207
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Full Match"

    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v3, v5}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    invoke-virtual {p3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Partial Match on last Name"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->FAMILY_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, p1, v3}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->FAMILY_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "__"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->FAMILY_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v3, v5}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->HONORIFIC_POSTFIXES:Ljava/util/List;

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 219
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Partial Match"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v3, v5}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    :cond_1
    sget-object v0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->HONORIFIC_POSTFIXES:Ljava/util/List;

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 232
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Partial Match w/o honorific"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v3, v5}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 235
    :cond_2
    invoke-virtual {p3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 236
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Partial Match only first name"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->FAMILY_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, p3, v3}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->FAMILY_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "__"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->FAMILY_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "___"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->FAMILY_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v3, v5}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    :cond_3
    return-void
.end method

.method private generateRulesQueryLength4(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 7
    .param p1, "query"    # Ljava/lang/String;
    .param p3, "strippedQuery"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactRule;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "rules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactRule;>;"
    const/4 v6, 0x2

    const/16 v5, 0x64

    .line 181
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Full Match"

    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v3, v5}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 182
    invoke-virtual {p3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 183
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Full Match"

    invoke-virtual {p0, p3}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v3, v5}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Partial Match on First Name"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, p3, v3}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "__"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v3, v5}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Partial Match on last Name"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->FAMILY_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, p1, v3}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->FAMILY_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "__"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->FAMILY_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v3, v5}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->HONORIFIC_POSTFIXES:Ljava/util/List;

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 198
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Partial Match"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {p1, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v3, v5}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    :cond_1
    return-void
.end method

.method private generateRulesQueryLengthGt4(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 6
    .param p1, "query"    # Ljava/lang/String;
    .param p3, "strippedQuery"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactRule;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "rules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactRule;>;"
    const/16 v5, 0x64

    .line 162
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Full Match"

    sget-object v2, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, p1, v2}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v3, v5}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    invoke-virtual {p3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Full Match"

    invoke-virtual {p0, p3}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v3, v5}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 167
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Partial Match on First Name"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, p3, v3}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "__"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->GIVEN_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v3, v5}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Partial Match on last Name"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->FAMILY_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, p1, v3}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->FAMILY_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "__"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->FAMILY_NAME_FIELDS:[Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;

    invoke-direct {v3, v5}, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;-><init>(I)V

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 177
    :cond_0
    return-void
.end method

.method private prepareRegExForQuery(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "samePhonetics"    # Ljava/lang/String;

    .prologue
    .line 285
    move-object v0, p1

    .line 288
    .local v0, "regex":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 299
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "^"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 302
    :goto_0
    return-object v0

    .line 290
    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "^.*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".*?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\u7238|\u5988|\u5144|\u7237|\u5976|\u59e5|\u53d4|\u4f84|\u5ac2|\u8205|\u592a|\u8463|\u7239|\u5a18|\u603b|\u5de5|\u5bfc|\u526f|\u53a8|\u59e8|\u5a76|\u5c40|\u68c0|\u961f|\u59d0|\u54e5|\u5f1f|\u59b9|\u5c0f\u59d0|\u592a\u592a|\u5973\u58eb|\u8001\u7237 |\u5148\u751f|\u8001\u5e08 |\u592b\u4eba|\u7239\u5730|\u5927\u6b3e|\u571f\u8c6a|\u5bcc\u5a46|\u5e05\u54e5|\u5e08\u516c|\u5f92\u5f1f|\u54e5\u4eec|\u59d0\u4eec|\u95fa\u871c|\u9753\u5973|\u9753\u4ed4|\u670b\u53cb|\u5934\u513f|\u9886\u5bfc|\u8001\u677f|\u8001\u5916|\u4e3b\u4efb|\u6821\u957f|\u535a\u58eb|\u7855\u58eb|\u6559\u6388|\u8bb2\u5e08|\u79d1\u957f|\u957f\u5b98|\u5904\u957f|\u7ecf\u7406|\u5e97\u957f|\u5bb6\u957f|\u540c\u5b66|\u8001\u5f1f|\u5c0f\u5f1f|\u53f8\u673a|\u4e0a\u5c06|\u4e0a\u5c09|\u4e0a\u6821|\u4e13\u5458|\u4e2d\u533b|\u4e2d\u5c06|\u4e2d\u5c09|\u4e2d\u6821|\u4e3b\u5e2d|\u4e3b\u6559|\u4e3b\u7ba1|\u4e61\u957f|\u4ee3\u8868|\u4f1a\u957f|\u4f2f\u4f2f|\u4fa6\u63a2|\u4fee\u5973|\u4fee\u7a97|\u4fee\u8f66|\u5144\u5f1f|\u515a\u5458|\u516c\u4e3b|\u516c\u5b89|\u51c6\u5c06|\u5236\u7247|\u526f\u53bf|\u526f\u56e2|\u526f\u5e08|\u526f\u603b|\u526f\u65c5|\u526f\u73ed|\u526f\u8425|\u52a9\u7406|\u5382\u957f|\u5385\u957f|\u53a8\u5b50|\u53a8\u5e08|\u53bf\u957f|\u53f8\u4ee4|\u53f8\u957f|\u540c\u5fd7|\u56e2\u957f|\u5927\u4eba|\u5927\u4f7f|\u5927\u53d4|\u5927\u5988|\u5927\u5a18|\u5927\u5e08|\u5927\u7237|\u59b9\u59b9|\u59b9\u5b50|\u59ca\u59b9|\u59d0\u59b9|\u59d4\u5458|\u59e5\u59e5|\u59e5\u7237|\u59e8\u59e8|\u5a76\u5a76|\u5b66\u751f|\u5b98\u5458|\u5bfc\u6f14|\u5c06\u519b|\u5c0f\u59b9|\u5c11\u5c06|\u5c11\u5c09|\u5c11\u6821|\u5dde\u957f|\u5e02\u957f|\u5e08\u5085|\u5e08\u7236|\u5e08\u957f|\u5e38\u59d4|\u5f1f\u5144|\u5f8b\u5e08|\u603b\u5de5|\u603b\u7406|\u603b\u76d1|\u603b\u7763|\u603b\u7edf|\u603b\u88c1|\u6240\u957f|\u62a4\u58eb|\u62a4\u7532|\u63a5\u673a|\u642c\u8fd0|\u652f\u4e66|\u65c5\u957f|\u6751\u6c11|\u6751\u957f|\u6821\u76d1|\u6821\u8463|\u6cd5\u5b98|\u6cd5\u5e08|\u6f14\u5458|\u7267\u5e08|\u73ed\u957f|\u7406\u53d1|\u7535\u5de5|\u76d1\u5236|\u76d1\u7763|\u7701\u957f|\u7763\u5bdf|\u793e\u957f|\u795e\u7236|\u798f\u68c0|\u79d8\u4e66|\u7ba1\u5bb6|\u7f16\u5267|\u7f8e\u5973|\u7f8e\u5bb9|\u7f8e\u7709|\u8001\u5e08|\u8205\u8205|\u8239\u957f|\u8425\u957f|\u897f\u533b|\u8b66\u53f8|\u8b66\u5458|\u8b66\u5bdf|\u8b66\u957f|\u9053\u58eb|\u9053\u957f|\u9500\u552e|\u9547\u957f|\u961f\u957f|\u963f\u7238|\u9662\u58eb|\u9753\u59b9|\u9886\u8896|\u9996\u5e2d|\u9996\u76f8|\u52a9\u6559|\u5e72\u5988|\u5e72\u7239|\u5e08\u59d0|\u5e08\u54e5|\u5e08\u5144|\u5e08\u5f1f|\u5e08\u59b9|\u4ee3\u7406|\u53d4\u53d4|\u963f\u59e8|\u5e08\u6bcd|\u8bd7\u4e08|\u4e66\u8bb0|\u5c40\u957f|\u9662\u957f|\u533b\u5e08|\u4f1a\u8ba1|\u90e8\u957f|\u8205\u6bcd|\u8205\u7236|\u8868\u5f1f|\u8868\u54e5|\u8868\u59d0|\u8868\u59b9|\u987e\u95ee|\u7968\u53cb|\u4e0a\u53f8|\u5927\u592b|\u5bfc\u6e38|\u9886\u961f|\u6559\u7ec3|\u5988\u54aa|\u5ab3\u5987|\u5a92\u5a46|\u8b66\u5b98|\u7ea2\u5a18|\u592a\u592a|\u9a74\u53cb|\u5c0f\u4e09|\u8001\u5927|\u8001\u4e8c|\u8001\u516c|\u8001\u5a46|\u8001\u603b|\u8001\u4e61|\u8001\u7238|\u8001\u5988|\u7231\u4eba|\u516c\u516c|\u5a46\u5a46|\u5988\u5988|\u5b9d\u8d1d|\u5b9d\u5b9d|\u7238\u7238|\u7237\u7237|\u5976\u5976|\u5916\u516c|\u5916\u5a46|\u987e\u5ba2|\u5ba2\u6237|\u5927\u59d0|\u5927\u54e5|\u59d0\u59d0|\u54e5\u54e5|\u59d0\u592b|\u5ac2\u5b50|\u5f1f\u5f1f|\u5f1f\u59b9|\u5f92\u5f1f|\u8001\u4f34|\u90bb\u5c45|\u9886\u73ed|\u5e97\u4e3b|\u5ba2\u4eba|\u623f\u4e1c|\u753b\u5bb6|\u96c7\u4e3b|\u4fdd\u59c6|\u533b\u751f|\u603b\u7ecf\u7406|\u8463\u4e8b\u957f|\u4e3b\u6301\u4eba|\u4e8c\u5e08\u5f1f|\u5236\u7247\u4eba|\u8d5e\u52a9\u5546|\u9886\u5bfc\u4eba|\u5927\u5e08\u5144|\u5c0f\u8001\u677f|\u88c5\u4fee\u5de5|\u8f85\u5bfc\u5458|\u9547\u4e66\u8bb0|\u7cfb\u4e3b\u4efb|\u6536\u85cf\u5bb6|\u751f\u4ea7\u5546|\u6444\u5f71\u5e08|\u8001\u670b\u53cb|\u8001\u592a\u592a|\u8001\u7237\u5b50|\u8001\u540c\u5b66|\u8001\u90e8\u4e0b|\u8001\u4e0a\u7ea7|\u8001\u9886\u5bfc|\u8001\u9996\u957f|\u5f00\u53d1\u5546|\u91d1\u878d\u5bb6|\u5e7f\u544a\u5546|\u4f9b\u5e94\u5546|\u5c0f\u5e08\u59b9|\u5c0f\u5e08\u5f1f|\u4e8c\u628a\u624b|\u4e00\u628a\u624b|\u623f\u4ea7\u5546|\u5927\u5e08\u59d0|\u51fa\u7248\u5546|\u4e8c\u5e08\u5144|\u5927\u8001\u677f|\u5927\u660e\u661f|\u627f\u5305\u4eba|\u627f\u5305\u5546|\u7f8e\u5bb9\u5e08|\u4e3b\u8bb2\u4eba|\u88c1\u5224\u5b98|\u7ecf\u7eaa\u4eba|\u4f01\u4e1a\u5bb6|\u751f\u4ea7\u5546|\u4f1a\u8ba1\u5e08|\u5de5\u7a0b\u5e08|\u5927\u5f53\u5bb6|\u4e8c\u5f53\u5bb6|\u4fdd\u7ba1\u5458|\u515a\u652f\u4e66|\u526f\u4e3b\u4efb|\u526f\u4e3b\u5e2d|\u526f\u4e61\u957f|\u526f\u4e66\u8bb0|\u526f\u4f1a\u957f|\u526f\u5382\u957f|\u526f\u5385\u957f|\u526f\u53f8\u4ee4|\u526f\u5904\u957f|\u526f\u5dde\u957f|\u526f\u5e02\u957f|\u526f\u603b\u7406|\u526f\u603b\u7edf|\u526f\u603b\u88c1|\u526f\u6240\u957f|\u526f\u6559\u6388|\u526f\u6751\u957f|\u526f\u6821\u957f|\u526f\u7701\u957f|\u526f\u793e\u957f|\u526f\u7ecf\u7406|\u526f\u90e8\u957f|\u526f\u9547\u957f|\u526f\u9662\u957f|\u5305\u5de5\u5934|\u53c2\u8bae\u5458|\u53c2\u8c0b\u957f|\u54a8\u8be2\u5e08|\u56e2\u652f\u4e66|\u5927\u5e08\u7236|\u5927\u5f8b\u5e08|\u5927\u6cd5\u5b98|\u5973\u4e3b\u4eba|\u59d4\u5458\u957f|\u5efa\u7b51\u5e08|\u603b\u4e66\u8bb0|\u603b\u53f8\u4ee4|\u603b\u987e\u95ee|\u62a4\u58eb\u957f|\u642c\u8fd0\u5de5|\u68c0\u5bdf\u5b98|\u68c0\u5bdf\u957f|\u68c0\u67e5\u957f|\u6c7d\u4fee\u5de5|\u6e05\u6d01\u5de5|\u7406\u53d1\u5e08|\u7535\u710a\u5de5|\u79d8\u4e66\u957f|\u7a0e\u52a1\u957f|\u7ba1\u7406\u5458|\u4e13\u79d1\u533b\u751f|\u4ea4\u901a\u90e8\u957f|\u4eba\u5927\u4ee3\u8868|\u515a\u59d4\u4e66\u8bb0|\u526f\u603b\u4e66\u8bb0|\u526f\u603b\u53f8\u4ee4|\u526f\u603b\u7ecf\u7406|\u526f\u68c0\u5bdf\u957f|\u526f\u8463\u4e8b\u957f|\u52a9\u7406\u6559\u6388|\u56fd\u9632\u90e8\u957f|\u5916\u4ea4\u90e8\u957f|\u5916\u79d1\u533b\u751f|\u59d4\u5458\u4e13\u5458|\u5bb6\u5ead\u533b\u751f|\u5dde\u53c2\u8bae\u5458|\u603b\u5de5\u7a0b\u5e08|\u603b\u68c0\u5bdf\u957f|\u6267\u884c\u8463\u4e8b|\u653f\u52a1\u52a9\u7406|\u6559\u5b66\u79d8\u4e66|\u6559\u80b2\u90e8\u957f|\u7a7a\u519b\u4e0a\u5c09|\u7ecf\u7406\u52a9\u7406|\u8363\u8a89\u535a\u58eb|\u8363\u8a89\u6559\u6388|\u884c\u653f\u52a9\u7406|\u884c\u653f\u6cd5\u5b98|\u884c\u653f\u79d8\u4e66|\u884c\u653f\u957f\u5b98|\u8f66\u95f4\u4e3b\u4efb|\u9500\u552e\u7ecf\u7406|\u9879\u76ee\u7ecf\u7406|\u9996\u5e2d\u6cd5\u5b98|\u9ad8\u7ea7\u4e13\u5458"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")?$"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 291
    goto :goto_0

    .line 293
    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "^.*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".*?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\u7238|\u5988|\u5144|\u7237|\u5976|\u59e5|\u53d4|\u4f84|\u5ac2|\u8205|\u592a|\u8463|\u7239|\u5a18|\u603b|\u5de5|\u5bfc|\u526f|\u53a8|\u59e8|\u5a76|\u5c40|\u68c0|\u961f|\u59d0|\u54e5|\u5f1f|\u59b9|\u5c0f\u59d0|\u592a\u592a|\u5973\u58eb|\u8001\u7237 |\u5148\u751f|\u8001\u5e08 |\u592b\u4eba|\u7239\u5730|\u5927\u6b3e|\u571f\u8c6a|\u5bcc\u5a46|\u5e05\u54e5|\u5e08\u516c|\u5f92\u5f1f|\u54e5\u4eec|\u59d0\u4eec|\u95fa\u871c|\u9753\u5973|\u9753\u4ed4|\u670b\u53cb|\u5934\u513f|\u9886\u5bfc|\u8001\u677f|\u8001\u5916|\u4e3b\u4efb|\u6821\u957f|\u535a\u58eb|\u7855\u58eb|\u6559\u6388|\u8bb2\u5e08|\u79d1\u957f|\u957f\u5b98|\u5904\u957f|\u7ecf\u7406|\u5e97\u957f|\u5bb6\u957f|\u540c\u5b66|\u8001\u5f1f|\u5c0f\u5f1f|\u53f8\u673a|\u4e0a\u5c06|\u4e0a\u5c09|\u4e0a\u6821|\u4e13\u5458|\u4e2d\u533b|\u4e2d\u5c06|\u4e2d\u5c09|\u4e2d\u6821|\u4e3b\u5e2d|\u4e3b\u6559|\u4e3b\u7ba1|\u4e61\u957f|\u4ee3\u8868|\u4f1a\u957f|\u4f2f\u4f2f|\u4fa6\u63a2|\u4fee\u5973|\u4fee\u7a97|\u4fee\u8f66|\u5144\u5f1f|\u515a\u5458|\u516c\u4e3b|\u516c\u5b89|\u51c6\u5c06|\u5236\u7247|\u526f\u53bf|\u526f\u56e2|\u526f\u5e08|\u526f\u603b|\u526f\u65c5|\u526f\u73ed|\u526f\u8425|\u52a9\u7406|\u5382\u957f|\u5385\u957f|\u53a8\u5b50|\u53a8\u5e08|\u53bf\u957f|\u53f8\u4ee4|\u53f8\u957f|\u540c\u5fd7|\u56e2\u957f|\u5927\u4eba|\u5927\u4f7f|\u5927\u53d4|\u5927\u5988|\u5927\u5a18|\u5927\u5e08|\u5927\u7237|\u59b9\u59b9|\u59b9\u5b50|\u59ca\u59b9|\u59d0\u59b9|\u59d4\u5458|\u59e5\u59e5|\u59e5\u7237|\u59e8\u59e8|\u5a76\u5a76|\u5b66\u751f|\u5b98\u5458|\u5bfc\u6f14|\u5c06\u519b|\u5c0f\u59b9|\u5c11\u5c06|\u5c11\u5c09|\u5c11\u6821|\u5dde\u957f|\u5e02\u957f|\u5e08\u5085|\u5e08\u7236|\u5e08\u957f|\u5e38\u59d4|\u5f1f\u5144|\u5f8b\u5e08|\u603b\u5de5|\u603b\u7406|\u603b\u76d1|\u603b\u7763|\u603b\u7edf|\u603b\u88c1|\u6240\u957f|\u62a4\u58eb|\u62a4\u7532|\u63a5\u673a|\u642c\u8fd0|\u652f\u4e66|\u65c5\u957f|\u6751\u6c11|\u6751\u957f|\u6821\u76d1|\u6821\u8463|\u6cd5\u5b98|\u6cd5\u5e08|\u6f14\u5458|\u7267\u5e08|\u73ed\u957f|\u7406\u53d1|\u7535\u5de5|\u76d1\u5236|\u76d1\u7763|\u7701\u957f|\u7763\u5bdf|\u793e\u957f|\u795e\u7236|\u798f\u68c0|\u79d8\u4e66|\u7ba1\u5bb6|\u7f16\u5267|\u7f8e\u5973|\u7f8e\u5bb9|\u7f8e\u7709|\u8001\u5e08|\u8205\u8205|\u8239\u957f|\u8425\u957f|\u897f\u533b|\u8b66\u53f8|\u8b66\u5458|\u8b66\u5bdf|\u8b66\u957f|\u9053\u58eb|\u9053\u957f|\u9500\u552e|\u9547\u957f|\u961f\u957f|\u963f\u7238|\u9662\u58eb|\u9753\u59b9|\u9886\u8896|\u9996\u5e2d|\u9996\u76f8|\u52a9\u6559|\u5e72\u5988|\u5e72\u7239|\u5e08\u59d0|\u5e08\u54e5|\u5e08\u5144|\u5e08\u5f1f|\u5e08\u59b9|\u4ee3\u7406|\u53d4\u53d4|\u963f\u59e8|\u5e08\u6bcd|\u8bd7\u4e08|\u4e66\u8bb0|\u5c40\u957f|\u9662\u957f|\u533b\u5e08|\u4f1a\u8ba1|\u90e8\u957f|\u8205\u6bcd|\u8205\u7236|\u8868\u5f1f|\u8868\u54e5|\u8868\u59d0|\u8868\u59b9|\u987e\u95ee|\u7968\u53cb|\u4e0a\u53f8|\u5927\u592b|\u5bfc\u6e38|\u9886\u961f|\u6559\u7ec3|\u5988\u54aa|\u5ab3\u5987|\u5a92\u5a46|\u8b66\u5b98|\u7ea2\u5a18|\u592a\u592a|\u9a74\u53cb|\u5c0f\u4e09|\u8001\u5927|\u8001\u4e8c|\u8001\u516c|\u8001\u5a46|\u8001\u603b|\u8001\u4e61|\u8001\u7238|\u8001\u5988|\u7231\u4eba|\u516c\u516c|\u5a46\u5a46|\u5988\u5988|\u5b9d\u8d1d|\u5b9d\u5b9d|\u7238\u7238|\u7237\u7237|\u5976\u5976|\u5916\u516c|\u5916\u5a46|\u987e\u5ba2|\u5ba2\u6237|\u5927\u59d0|\u5927\u54e5|\u59d0\u59d0|\u54e5\u54e5|\u59d0\u592b|\u5ac2\u5b50|\u5f1f\u5f1f|\u5f1f\u59b9|\u5f92\u5f1f|\u8001\u4f34|\u90bb\u5c45|\u9886\u73ed|\u5e97\u4e3b|\u5ba2\u4eba|\u623f\u4e1c|\u753b\u5bb6|\u96c7\u4e3b|\u4fdd\u59c6|\u533b\u751f|\u603b\u7ecf\u7406|\u8463\u4e8b\u957f|\u4e3b\u6301\u4eba|\u4e8c\u5e08\u5f1f|\u5236\u7247\u4eba|\u8d5e\u52a9\u5546|\u9886\u5bfc\u4eba|\u5927\u5e08\u5144|\u5c0f\u8001\u677f|\u88c5\u4fee\u5de5|\u8f85\u5bfc\u5458|\u9547\u4e66\u8bb0|\u7cfb\u4e3b\u4efb|\u6536\u85cf\u5bb6|\u751f\u4ea7\u5546|\u6444\u5f71\u5e08|\u8001\u670b\u53cb|\u8001\u592a\u592a|\u8001\u7237\u5b50|\u8001\u540c\u5b66|\u8001\u90e8\u4e0b|\u8001\u4e0a\u7ea7|\u8001\u9886\u5bfc|\u8001\u9996\u957f|\u5f00\u53d1\u5546|\u91d1\u878d\u5bb6|\u5e7f\u544a\u5546|\u4f9b\u5e94\u5546|\u5c0f\u5e08\u59b9|\u5c0f\u5e08\u5f1f|\u4e8c\u628a\u624b|\u4e00\u628a\u624b|\u623f\u4ea7\u5546|\u5927\u5e08\u59d0|\u51fa\u7248\u5546|\u4e8c\u5e08\u5144|\u5927\u8001\u677f|\u5927\u660e\u661f|\u627f\u5305\u4eba|\u627f\u5305\u5546|\u7f8e\u5bb9\u5e08|\u4e3b\u8bb2\u4eba|\u88c1\u5224\u5b98|\u7ecf\u7eaa\u4eba|\u4f01\u4e1a\u5bb6|\u751f\u4ea7\u5546|\u4f1a\u8ba1\u5e08|\u5de5\u7a0b\u5e08|\u5927\u5f53\u5bb6|\u4e8c\u5f53\u5bb6|\u4fdd\u7ba1\u5458|\u515a\u652f\u4e66|\u526f\u4e3b\u4efb|\u526f\u4e3b\u5e2d|\u526f\u4e61\u957f|\u526f\u4e66\u8bb0|\u526f\u4f1a\u957f|\u526f\u5382\u957f|\u526f\u5385\u957f|\u526f\u53f8\u4ee4|\u526f\u5904\u957f|\u526f\u5dde\u957f|\u526f\u5e02\u957f|\u526f\u603b\u7406|\u526f\u603b\u7edf|\u526f\u603b\u88c1|\u526f\u6240\u957f|\u526f\u6559\u6388|\u526f\u6751\u957f|\u526f\u6821\u957f|\u526f\u7701\u957f|\u526f\u793e\u957f|\u526f\u7ecf\u7406|\u526f\u90e8\u957f|\u526f\u9547\u957f|\u526f\u9662\u957f|\u5305\u5de5\u5934|\u53c2\u8bae\u5458|\u53c2\u8c0b\u957f|\u54a8\u8be2\u5e08|\u56e2\u652f\u4e66|\u5927\u5e08\u7236|\u5927\u5f8b\u5e08|\u5927\u6cd5\u5b98|\u5973\u4e3b\u4eba|\u59d4\u5458\u957f|\u5efa\u7b51\u5e08|\u603b\u4e66\u8bb0|\u603b\u53f8\u4ee4|\u603b\u987e\u95ee|\u62a4\u58eb\u957f|\u642c\u8fd0\u5de5|\u68c0\u5bdf\u5b98|\u68c0\u5bdf\u957f|\u68c0\u67e5\u957f|\u6c7d\u4fee\u5de5|\u6e05\u6d01\u5de5|\u7406\u53d1\u5e08|\u7535\u710a\u5de5|\u79d8\u4e66\u957f|\u7a0e\u52a1\u957f|\u7ba1\u7406\u5458|\u4e13\u79d1\u533b\u751f|\u4ea4\u901a\u90e8\u957f|\u4eba\u5927\u4ee3\u8868|\u515a\u59d4\u4e66\u8bb0|\u526f\u603b\u4e66\u8bb0|\u526f\u603b\u53f8\u4ee4|\u526f\u603b\u7ecf\u7406|\u526f\u68c0\u5bdf\u957f|\u526f\u8463\u4e8b\u957f|\u52a9\u7406\u6559\u6388|\u56fd\u9632\u90e8\u957f|\u5916\u4ea4\u90e8\u957f|\u5916\u79d1\u533b\u751f|\u59d4\u5458\u4e13\u5458|\u5bb6\u5ead\u533b\u751f|\u5dde\u53c2\u8bae\u5458|\u603b\u5de5\u7a0b\u5e08|\u603b\u68c0\u5bdf\u957f|\u6267\u884c\u8463\u4e8b|\u653f\u52a1\u52a9\u7406|\u6559\u5b66\u79d8\u4e66|\u6559\u80b2\u90e8\u957f|\u7a7a\u519b\u4e0a\u5c09|\u7ecf\u7406\u52a9\u7406|\u8363\u8a89\u535a\u58eb|\u8363\u8a89\u6559\u6388|\u884c\u653f\u52a9\u7406|\u884c\u653f\u6cd5\u5b98|\u884c\u653f\u79d8\u4e66|\u884c\u653f\u957f\u5b98|\u8f66\u95f4\u4e3b\u4efb|\u9500\u552e\u7ecf\u7406|\u9879\u76ee\u7ecf\u7406|\u9996\u5e2d\u6cd5\u5b98|\u9ad8\u7ea7\u4e13\u5458"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")?$"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 294
    goto :goto_0

    .line 296
    :pswitch_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "^"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\u7238|\u5988|\u5144|\u7237|\u5976|\u59e5|\u53d4|\u4f84|\u5ac2|\u8205|\u592a|\u8463|\u7239|\u5a18|\u603b|\u5de5|\u5bfc|\u526f|\u53a8|\u59e8|\u5a76|\u5c40|\u68c0|\u961f|\u59d0|\u54e5|\u5f1f|\u59b9|\u5c0f\u59d0|\u592a\u592a|\u5973\u58eb|\u8001\u7237 |\u5148\u751f|\u8001\u5e08 |\u592b\u4eba|\u7239\u5730|\u5927\u6b3e|\u571f\u8c6a|\u5bcc\u5a46|\u5e05\u54e5|\u5e08\u516c|\u5f92\u5f1f|\u54e5\u4eec|\u59d0\u4eec|\u95fa\u871c|\u9753\u5973|\u9753\u4ed4|\u670b\u53cb|\u5934\u513f|\u9886\u5bfc|\u8001\u677f|\u8001\u5916|\u4e3b\u4efb|\u6821\u957f|\u535a\u58eb|\u7855\u58eb|\u6559\u6388|\u8bb2\u5e08|\u79d1\u957f|\u957f\u5b98|\u5904\u957f|\u7ecf\u7406|\u5e97\u957f|\u5bb6\u957f|\u540c\u5b66|\u8001\u5f1f|\u5c0f\u5f1f|\u53f8\u673a|\u4e0a\u5c06|\u4e0a\u5c09|\u4e0a\u6821|\u4e13\u5458|\u4e2d\u533b|\u4e2d\u5c06|\u4e2d\u5c09|\u4e2d\u6821|\u4e3b\u5e2d|\u4e3b\u6559|\u4e3b\u7ba1|\u4e61\u957f|\u4ee3\u8868|\u4f1a\u957f|\u4f2f\u4f2f|\u4fa6\u63a2|\u4fee\u5973|\u4fee\u7a97|\u4fee\u8f66|\u5144\u5f1f|\u515a\u5458|\u516c\u4e3b|\u516c\u5b89|\u51c6\u5c06|\u5236\u7247|\u526f\u53bf|\u526f\u56e2|\u526f\u5e08|\u526f\u603b|\u526f\u65c5|\u526f\u73ed|\u526f\u8425|\u52a9\u7406|\u5382\u957f|\u5385\u957f|\u53a8\u5b50|\u53a8\u5e08|\u53bf\u957f|\u53f8\u4ee4|\u53f8\u957f|\u540c\u5fd7|\u56e2\u957f|\u5927\u4eba|\u5927\u4f7f|\u5927\u53d4|\u5927\u5988|\u5927\u5a18|\u5927\u5e08|\u5927\u7237|\u59b9\u59b9|\u59b9\u5b50|\u59ca\u59b9|\u59d0\u59b9|\u59d4\u5458|\u59e5\u59e5|\u59e5\u7237|\u59e8\u59e8|\u5a76\u5a76|\u5b66\u751f|\u5b98\u5458|\u5bfc\u6f14|\u5c06\u519b|\u5c0f\u59b9|\u5c11\u5c06|\u5c11\u5c09|\u5c11\u6821|\u5dde\u957f|\u5e02\u957f|\u5e08\u5085|\u5e08\u7236|\u5e08\u957f|\u5e38\u59d4|\u5f1f\u5144|\u5f8b\u5e08|\u603b\u5de5|\u603b\u7406|\u603b\u76d1|\u603b\u7763|\u603b\u7edf|\u603b\u88c1|\u6240\u957f|\u62a4\u58eb|\u62a4\u7532|\u63a5\u673a|\u642c\u8fd0|\u652f\u4e66|\u65c5\u957f|\u6751\u6c11|\u6751\u957f|\u6821\u76d1|\u6821\u8463|\u6cd5\u5b98|\u6cd5\u5e08|\u6f14\u5458|\u7267\u5e08|\u73ed\u957f|\u7406\u53d1|\u7535\u5de5|\u76d1\u5236|\u76d1\u7763|\u7701\u957f|\u7763\u5bdf|\u793e\u957f|\u795e\u7236|\u798f\u68c0|\u79d8\u4e66|\u7ba1\u5bb6|\u7f16\u5267|\u7f8e\u5973|\u7f8e\u5bb9|\u7f8e\u7709|\u8001\u5e08|\u8205\u8205|\u8239\u957f|\u8425\u957f|\u897f\u533b|\u8b66\u53f8|\u8b66\u5458|\u8b66\u5bdf|\u8b66\u957f|\u9053\u58eb|\u9053\u957f|\u9500\u552e|\u9547\u957f|\u961f\u957f|\u963f\u7238|\u9662\u58eb|\u9753\u59b9|\u9886\u8896|\u9996\u5e2d|\u9996\u76f8|\u52a9\u6559|\u5e72\u5988|\u5e72\u7239|\u5e08\u59d0|\u5e08\u54e5|\u5e08\u5144|\u5e08\u5f1f|\u5e08\u59b9|\u4ee3\u7406|\u53d4\u53d4|\u963f\u59e8|\u5e08\u6bcd|\u8bd7\u4e08|\u4e66\u8bb0|\u5c40\u957f|\u9662\u957f|\u533b\u5e08|\u4f1a\u8ba1|\u90e8\u957f|\u8205\u6bcd|\u8205\u7236|\u8868\u5f1f|\u8868\u54e5|\u8868\u59d0|\u8868\u59b9|\u987e\u95ee|\u7968\u53cb|\u4e0a\u53f8|\u5927\u592b|\u5bfc\u6e38|\u9886\u961f|\u6559\u7ec3|\u5988\u54aa|\u5ab3\u5987|\u5a92\u5a46|\u8b66\u5b98|\u7ea2\u5a18|\u592a\u592a|\u9a74\u53cb|\u5c0f\u4e09|\u8001\u5927|\u8001\u4e8c|\u8001\u516c|\u8001\u5a46|\u8001\u603b|\u8001\u4e61|\u8001\u7238|\u8001\u5988|\u7231\u4eba|\u516c\u516c|\u5a46\u5a46|\u5988\u5988|\u5b9d\u8d1d|\u5b9d\u5b9d|\u7238\u7238|\u7237\u7237|\u5976\u5976|\u5916\u516c|\u5916\u5a46|\u987e\u5ba2|\u5ba2\u6237|\u5927\u59d0|\u5927\u54e5|\u59d0\u59d0|\u54e5\u54e5|\u59d0\u592b|\u5ac2\u5b50|\u5f1f\u5f1f|\u5f1f\u59b9|\u5f92\u5f1f|\u8001\u4f34|\u90bb\u5c45|\u9886\u73ed|\u5e97\u4e3b|\u5ba2\u4eba|\u623f\u4e1c|\u753b\u5bb6|\u96c7\u4e3b|\u4fdd\u59c6|\u533b\u751f|\u603b\u7ecf\u7406|\u8463\u4e8b\u957f|\u4e3b\u6301\u4eba|\u4e8c\u5e08\u5f1f|\u5236\u7247\u4eba|\u8d5e\u52a9\u5546|\u9886\u5bfc\u4eba|\u5927\u5e08\u5144|\u5c0f\u8001\u677f|\u88c5\u4fee\u5de5|\u8f85\u5bfc\u5458|\u9547\u4e66\u8bb0|\u7cfb\u4e3b\u4efb|\u6536\u85cf\u5bb6|\u751f\u4ea7\u5546|\u6444\u5f71\u5e08|\u8001\u670b\u53cb|\u8001\u592a\u592a|\u8001\u7237\u5b50|\u8001\u540c\u5b66|\u8001\u90e8\u4e0b|\u8001\u4e0a\u7ea7|\u8001\u9886\u5bfc|\u8001\u9996\u957f|\u5f00\u53d1\u5546|\u91d1\u878d\u5bb6|\u5e7f\u544a\u5546|\u4f9b\u5e94\u5546|\u5c0f\u5e08\u59b9|\u5c0f\u5e08\u5f1f|\u4e8c\u628a\u624b|\u4e00\u628a\u624b|\u623f\u4ea7\u5546|\u5927\u5e08\u59d0|\u51fa\u7248\u5546|\u4e8c\u5e08\u5144|\u5927\u8001\u677f|\u5927\u660e\u661f|\u627f\u5305\u4eba|\u627f\u5305\u5546|\u7f8e\u5bb9\u5e08|\u4e3b\u8bb2\u4eba|\u88c1\u5224\u5b98|\u7ecf\u7eaa\u4eba|\u4f01\u4e1a\u5bb6|\u751f\u4ea7\u5546|\u4f1a\u8ba1\u5e08|\u5de5\u7a0b\u5e08|\u5927\u5f53\u5bb6|\u4e8c\u5f53\u5bb6|\u4fdd\u7ba1\u5458|\u515a\u652f\u4e66|\u526f\u4e3b\u4efb|\u526f\u4e3b\u5e2d|\u526f\u4e61\u957f|\u526f\u4e66\u8bb0|\u526f\u4f1a\u957f|\u526f\u5382\u957f|\u526f\u5385\u957f|\u526f\u53f8\u4ee4|\u526f\u5904\u957f|\u526f\u5dde\u957f|\u526f\u5e02\u957f|\u526f\u603b\u7406|\u526f\u603b\u7edf|\u526f\u603b\u88c1|\u526f\u6240\u957f|\u526f\u6559\u6388|\u526f\u6751\u957f|\u526f\u6821\u957f|\u526f\u7701\u957f|\u526f\u793e\u957f|\u526f\u7ecf\u7406|\u526f\u90e8\u957f|\u526f\u9547\u957f|\u526f\u9662\u957f|\u5305\u5de5\u5934|\u53c2\u8bae\u5458|\u53c2\u8c0b\u957f|\u54a8\u8be2\u5e08|\u56e2\u652f\u4e66|\u5927\u5e08\u7236|\u5927\u5f8b\u5e08|\u5927\u6cd5\u5b98|\u5973\u4e3b\u4eba|\u59d4\u5458\u957f|\u5efa\u7b51\u5e08|\u603b\u4e66\u8bb0|\u603b\u53f8\u4ee4|\u603b\u987e\u95ee|\u62a4\u58eb\u957f|\u642c\u8fd0\u5de5|\u68c0\u5bdf\u5b98|\u68c0\u5bdf\u957f|\u68c0\u67e5\u957f|\u6c7d\u4fee\u5de5|\u6e05\u6d01\u5de5|\u7406\u53d1\u5e08|\u7535\u710a\u5de5|\u79d8\u4e66\u957f|\u7a0e\u52a1\u957f|\u7ba1\u7406\u5458|\u4e13\u79d1\u533b\u751f|\u4ea4\u901a\u90e8\u957f|\u4eba\u5927\u4ee3\u8868|\u515a\u59d4\u4e66\u8bb0|\u526f\u603b\u4e66\u8bb0|\u526f\u603b\u53f8\u4ee4|\u526f\u603b\u7ecf\u7406|\u526f\u68c0\u5bdf\u957f|\u526f\u8463\u4e8b\u957f|\u52a9\u7406\u6559\u6388|\u56fd\u9632\u90e8\u957f|\u5916\u4ea4\u90e8\u957f|\u5916\u79d1\u533b\u751f|\u59d4\u5458\u4e13\u5458|\u5bb6\u5ead\u533b\u751f|\u5dde\u53c2\u8bae\u5458|\u603b\u5de5\u7a0b\u5e08|\u603b\u68c0\u5bdf\u957f|\u6267\u884c\u8463\u4e8b|\u653f\u52a1\u52a9\u7406|\u6559\u5b66\u79d8\u4e66|\u6559\u80b2\u90e8\u957f|\u7a7a\u519b\u4e0a\u5c09|\u7ecf\u7406\u52a9\u7406|\u8363\u8a89\u535a\u58eb|\u8363\u8a89\u6559\u6388|\u884c\u653f\u52a9\u7406|\u884c\u653f\u6cd5\u5b98|\u884c\u653f\u79d8\u4e66|\u884c\u653f\u957f\u5b98|\u8f66\u95f4\u4e3b\u4efb|\u9500\u552e\u7ecf\u7406|\u9879\u76ee\u7ecf\u7406|\u9996\u5e2d\u6cd5\u5b98|\u9ad8\u7ea7\u4e13\u5458"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")?$"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 297
    goto/16 :goto_0

    .line 288
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public canProcess(Ljava/lang/String;)Z
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isChineseString(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public generateRules(Ljava/lang/String;)Ljava/util/List;
    .locals 13
    .param p1, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactRule;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v12, 0x64

    const/16 v11, 0x3c

    const/4 v1, 0x4

    const/4 v8, 0x1

    .line 103
    const/4 v2, 0x0

    .line 106
    .local v2, "rules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactRule;>;"
    sget-object v0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->CHINESE_TITLES_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    .line 108
    .local v6, "chineseTitleMatcher":Ljava/util/regex/Matcher;
    move-object v3, p1

    .line 109
    .local v3, "strippedQuery":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {v6, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 113
    :cond_0
    new-instance v7, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;

    invoke-direct {v7}, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;-><init>()V

    .line 114
    .local v7, "normalizer":Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;
    invoke-virtual {v7, p1}, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;->allPossiblePhonetic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 115
    .local v4, "normalizedQuery":Ljava/lang/String;
    move-object v5, v4

    .line 116
    .local v5, "normalizedStrippedQuery":Ljava/lang/String;
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 117
    invoke-virtual {v7, v3}, Lcom/vlingo/core/internal/contacts/normalizers/PhoneticNormalizer;->allPossiblePhonetic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 120
    :cond_1
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 121
    new-instance v2, Ljava/util/ArrayList;

    .end local v2    # "rules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactRule;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 123
    .restart local v2    # "rules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactRule;>;"
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_2

    .line 124
    invoke-direct {p0, p1, v2, v3}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->generateRulesQueryLengthGt4(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    .line 126
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v1, :cond_5

    .line 127
    invoke-direct {p0, p1, v2, v3}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->generateRulesQueryLength4(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    .line 146
    :cond_3
    :goto_0
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Search With Titles"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "%"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "%"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;

    invoke-direct {p0, p1, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->prepareRegExForQuery(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v12, v10, v4}, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, v8, v9}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Search With Titles"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "%"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;

    invoke-direct {p0, p1, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->prepareRegExForQuery(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v12, v10, v4}, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, v8, v9}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 150
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Search With Titles"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "%"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "%"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;

    invoke-direct {p0, v3, v5}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->prepareRegExForQuery(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v11, v10, v5}, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, v8, v9}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactRule;

    const-string/jumbo v1, "Search With Titles"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "%"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;

    invoke-direct {p0, v3, v5}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->prepareRegExForQuery(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v11, v10, v5}, Lcom/vlingo/core/internal/contacts/scoring/ChineseTitleContactScore;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, v8, v9}, Lcom/vlingo/core/internal/contacts/ContactRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/contacts/scoring/ContactScore;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    :cond_4
    return-object v2

    .line 131
    :cond_5
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_6

    .line 132
    invoke-direct {p0, p1, v2, v3}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->generateRulesQueryLength3(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 136
    :cond_6
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    move-object v0, p0

    move-object v1, p1

    .line 137
    invoke-direct/range {v0 .. v5}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->generateRulesQueryLength2(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 139
    :cond_7
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v8, :cond_3

    .line 141
    invoke-direct {p0, p1, v2, v4}, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->generateRulesQueryLength1(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public getExitCriteria()Lcom/vlingo/core/internal/contacts/ContactExitCriteria;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->criteria:Lcom/vlingo/core/internal/contacts/ContactExitCriteria;

    return-object v0
.end method

.method public queryToWhereClause(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 307
    const-string/jumbo v0, "vnd.android.cursor.item/name"

    sget-object v1, Lcom/vlingo/core/internal/contacts/rules/ChineseContactRuleSet;->DEFAULT_FIELDS:[Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcom/vlingo/core/internal/contacts/ContactDBQueryUtil;->queryToWhereClauseWithNormalization(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public varargs queryToWhereClause(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "fields"    # [Ljava/lang/String;

    .prologue
    .line 311
    const-string/jumbo v0, "vnd.android.cursor.item/name"

    invoke-static {p1, v0, p2}, Lcom/vlingo/core/internal/contacts/ContactDBQueryUtil;->queryToWhereClauseWithNormalization(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public skipExtraData()Z
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return v0
.end method
