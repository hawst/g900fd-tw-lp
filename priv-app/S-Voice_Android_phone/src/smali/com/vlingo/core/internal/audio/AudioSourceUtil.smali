.class public interface abstract Lcom/vlingo/core/internal/audio/AudioSourceUtil;
.super Ljava/lang/Object;
.source "AudioSourceUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/CoreAdapter;


# virtual methods
.method public abstract chooseAudioSource(Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)I
.end method

.method public abstract chooseChannelConfig(I)I
.end method

.method public abstract chooseMicSampleRate()I
.end method

.method public abstract notifyAudioSourceSet(I)V
.end method
