.class public Lcom/vlingo/core/internal/associatedservice/ResourceIdProviderImpl;
.super Ljava/lang/Object;
.source "ResourceIdProviderImpl.java"

# interfaces
.implements Lcom/vlingo/core/internal/ResourceIdProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/associatedservice/ResourceIdProviderImpl$1;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method


# virtual methods
.method public getDrawable(Lcom/vlingo/core/internal/ResourceIdProvider$drawable;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    .prologue
    .line 17
    const/4 v0, 0x0

    return-object v0
.end method

.method public getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$array;)I
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$array;

    .prologue
    .line 57
    const/4 v0, 0x0

    return v0
.end method

.method public getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$drawable;)I
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    .prologue
    .line 11
    const/4 v0, 0x0

    return v0
.end method

.method public getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$id;)I
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$id;

    .prologue
    .line 23
    const/4 v0, 0x0

    return v0
.end method

.method public getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$layout;)I
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$layout;

    .prologue
    .line 29
    const/4 v0, 0x0

    return v0
.end method

.method public getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$raw;)I
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    .prologue
    .line 74
    const/4 v0, 0x0

    return v0
.end method

.method public getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$string;)I
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method

.method public getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .prologue
    .line 40
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/ResourceIdProviderImpl$1;->$SwitchMap$com$vlingo$core$internal$ResourceIdProvider$string:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/ResourceIdProvider$string;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 44
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 42
    :pswitch_0
    const-string/jumbo v0, "com.samsung.SMT"

    goto :goto_0

    .line 40
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public varargs getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 51
    const/4 v0, 0x0

    return-object v0
.end method

.method public getStringArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;)[Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Lcom/vlingo/core/internal/ResourceIdProvider$array;

    .prologue
    .line 62
    sget-object v0, Lcom/vlingo/core/internal/associatedservice/ResourceIdProviderImpl$1;->$SwitchMap$com$vlingo$core$internal$ResourceIdProvider$array:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/ResourceIdProvider$array;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 67
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 65
    :pswitch_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    goto :goto_0

    .line 62
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
