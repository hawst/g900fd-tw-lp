.class Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler$AppointmentQueryRunnable;
.super Ljava/lang/Object;
.source "ResolveAppointmentHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AppointmentQueryRunnable"
.end annotation


# instance fields
.field action:Lcom/vlingo/sdk/recognition/VLAction;

.field resolveAppointmentHandler:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;Lcom/vlingo/sdk/recognition/VLAction;)V
    .locals 0
    .param p1, "resolveAppointmentHandler"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;
    .param p2, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler$AppointmentQueryRunnable;->resolveAppointmentHandler:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;

    .line 92
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler$AppointmentQueryRunnable;->action:Lcom/vlingo/sdk/recognition/VLAction;

    .line 93
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 97
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler$AppointmentQueryRunnable;->resolveAppointmentHandler:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler$AppointmentQueryRunnable;->action:Lcom/vlingo/sdk/recognition/VLAction;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->getAppointmentsByQuery(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;
    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->access$000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;

    move-result-object v0

    .line 99
    .local v0, "queriedAppointments":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    if-eqz v0, :cond_0

    .line 100
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler$AppointmentQueryRunnable;->resolveAppointmentHandler:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->getStoredAppointments()Ljava/util/List;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->access$100(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;)Ljava/util/List;

    move-result-object v1

    .line 102
    .local v1, "storedAppointments":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler$AppointmentQueryRunnable;->resolveAppointmentHandler:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->sendScheduleResolvedEvent(Ljava/util/List;I)V
    invoke-static {v2, v0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->access$200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;Ljava/util/List;I)V

    .line 103
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler$AppointmentQueryRunnable;->resolveAppointmentHandler:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->addQueriedAppointmentsToStore(Ljava/util/List;Ljava/util/List;)V
    invoke-static {v2, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->access$300(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;Ljava/util/List;Ljava/util/List;)V

    .line 106
    .end local v1    # "storedAppointments":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    :cond_0
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler$AppointmentQueryRunnable;->resolveAppointmentHandler:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;->access$400(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 108
    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler$AppointmentQueryRunnable;->resolveAppointmentHandler:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler;

    .line 109
    iput-object v4, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveAppointmentHandler$AppointmentQueryRunnable;->action:Lcom/vlingo/sdk/recognition/VLAction;

    .line 110
    return-void
.end method
