.class Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;
.super Landroid/os/Handler;
.source "AudioPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/audio/AudioPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AudioPlayerHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

.field thread:Landroid/os/HandlerThread;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/audio/AudioPlayer;Landroid/os/HandlerThread;)V
    .locals 1
    .param p2, "thread"    # Landroid/os/HandlerThread;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    .line 184
    invoke-virtual {p2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 185
    iput-object p2, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->thread:Landroid/os/HandlerThread;

    .line 186
    return-void
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;
    .param p1, "x1"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 181
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->handleStartSub(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    return-void
.end method

.method private handleError(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    const/4 v2, 0x0

    .line 413
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->playingRequest:Lcom/vlingo/core/internal/audio/AudioRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->playingRequest:Lcom/vlingo/core/internal/audio/AudioRequest;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/audio/AudioRequest;->hasFlag(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 414
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->releaseMediaPlayer()V

    .line 416
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->playingRequest:Lcom/vlingo/core/internal/audio/AudioRequest;

    .line 417
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iput-boolean v2, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isSupposedToBePlaying:Z

    .line 418
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->serviceListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    sget-object v1, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;->ERROR:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    invoke-interface {v0, p1, v1}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V

    .line 419
    sget-object v0, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;->ERROR:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    invoke-virtual {p1, p1, v0}, Lcom/vlingo/core/internal/audio/AudioRequest;->onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V

    .line 420
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iput-boolean v2, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isWorking:Z

    .line 421
    return-void
.end method

.method private handleStart(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 5
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 305
    iget-boolean v0, p1, Lcom/vlingo/core/internal/audio/AudioRequest;->requestAudioFocus:Z

    if-eqz v0, :cond_1

    .line 307
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->audioFocusManager:Lcom/vlingo/core/internal/audio/AudioFocusManager;

    iget v1, p1, Lcom/vlingo/core/internal/audio/AudioRequest;->audioStream:I

    iget v2, p1, Lcom/vlingo/core/internal/audio/AudioRequest;->audioFocusType:I

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    .line 309
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    const-wide/16 v1, 0x7d0

    new-instance v3, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler$1;

    invoke-direct {v3, p0, p1}, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler$1;-><init>(Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;Lcom/vlingo/core/internal/audio/AudioRequest;)V

    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v4, v4, Lcom/vlingo/core/internal/audio/AudioPlayer;->handler:Landroid/os/Handler;

    invoke-static {v1, v2, v3, v4}, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->runTaskOnBluetoothAudioOn(JLjava/lang/Runnable;Landroid/os/Handler;)Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    move-result-object v1

    # setter for: Lcom/vlingo/core/internal/audio/AudioPlayer;->onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;
    invoke-static {v0, v1}, Lcom/vlingo/core/internal/audio/AudioPlayer;->access$102(Lcom/vlingo/core/internal/audio/AudioPlayer;Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;)Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    .line 324
    :goto_0
    return-void

    .line 318
    :cond_0
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->handleStartSub(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    goto :goto_0

    .line 321
    :cond_1
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->handleStartSub(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    goto :goto_0
.end method

.method private handleStartSub(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 13
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    .line 328
    iget-object v7, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iput-boolean v11, v7, Lcom/vlingo/core/internal/audio/AudioPlayer;->isSupposedToBePlaying:Z

    .line 329
    iget-object v7, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iput-object p1, v7, Lcom/vlingo/core/internal/audio/AudioPlayer;->playingRequest:Lcom/vlingo/core/internal/audio/AudioRequest;

    .line 331
    iget-object v7, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v7, v7, Lcom/vlingo/core/internal/audio/AudioPlayer;->serviceListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-interface {v7, p1}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 333
    const/4 v2, 0x1

    .line 334
    .local v2, "fireCallbacks":Z
    instance-of v7, p1, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;

    if-eqz v7, :cond_0

    move-object v6, p1

    .line 335
    check-cast v6, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;

    .line 336
    .local v6, "mreq":Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;
    invoke-virtual {v6}, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->isOnFirstPart()Z

    move-result v2

    .line 339
    .end local v6    # "mreq":Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;
    :cond_0
    if-eqz v2, :cond_1

    .line 340
    invoke-virtual {p1, p1}, Lcom/vlingo/core/internal/audio/AudioRequest;->onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 343
    :cond_1
    instance-of v7, p1, Lcom/vlingo/core/internal/audio/NullAudioRequest;

    if-eqz v7, :cond_2

    .line 344
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->skipAudioStart(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 402
    :goto_0
    return-void

    .line 348
    :cond_2
    iget-object v7, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v7, v7, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    if-nez v7, :cond_3

    .line 349
    iget-object v7, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    # invokes: Lcom/vlingo/core/internal/audio/AudioPlayer;->createMediaPlayer()V
    invoke-static {v7}, Lcom/vlingo/core/internal/audio/AudioPlayer;->access$300(Lcom/vlingo/core/internal/audio/AudioPlayer;)V

    .line 352
    :cond_3
    iget-object v7, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    # invokes: Lcom/vlingo/core/internal/audio/AudioPlayer;->setDataSource(Lcom/vlingo/core/internal/audio/AudioRequest;)Z
    invoke-static {v7, p1}, Lcom/vlingo/core/internal/audio/AudioPlayer;->access$400(Lcom/vlingo/core/internal/audio/AudioPlayer;Lcom/vlingo/core/internal/audio/AudioRequest;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 353
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->skipAudioStart(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    goto :goto_0

    .line 357
    :cond_4
    iget-object v7, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    # invokes: Lcom/vlingo/core/internal/audio/AudioPlayer;->checkMuteVolume()V
    invoke-static {v7}, Lcom/vlingo/core/internal/audio/AudioPlayer;->access$500(Lcom/vlingo/core/internal/audio/AudioPlayer;)V

    .line 359
    new-instance v5, Lcom/vlingo/core/internal/audio/AudioPlayer$MediaPlayerListener;

    iget-object v7, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    invoke-direct {v5, v7, p1}, Lcom/vlingo/core/internal/audio/AudioPlayer$MediaPlayerListener;-><init>(Lcom/vlingo/core/internal/audio/AudioPlayer;Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 360
    .local v5, "listener":Lcom/vlingo/core/internal/audio/AudioPlayer$MediaPlayerListener;
    iget-object v7, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v7, v7, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v7, v5}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 361
    iget-object v7, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v7, v7, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v7, v5}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 362
    instance-of v7, p1, Lcom/vlingo/core/internal/audio/ToneAudioRequest;

    if-eqz v7, :cond_5

    .line 369
    iget-object v7, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v7, v7, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v7}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v1

    .line 370
    .local v1, "duration":I
    iget-object v7, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v7, v7, Lcom/vlingo/core/internal/audio/AudioPlayer;->handler:Landroid/os/Handler;

    iget-object v8, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v8, v8, Lcom/vlingo/core/internal/audio/AudioPlayer;->handler:Landroid/os/Handler;

    invoke-virtual {v8, v11, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v8

    int-to-long v9, v1

    invoke-virtual {v7, v8, v9, v10}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 374
    .end local v1    # "duration":I
    :cond_5
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const-string/jumbo v8, "audio"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 375
    .local v0, "am":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v4

    .line 376
    .local v4, "isScoOn":Z
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v7

    invoke-virtual {v7, v11}, Landroid/bluetooth/BluetoothAdapter;->getProfileConnectionState(I)I

    move-result v3

    .line 377
    .local v3, "isHeadsetConnected":I
    if-eqz v4, :cond_8

    if-ne v3, v12, :cond_8

    .line 394
    :cond_6
    :goto_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 401
    :cond_7
    iget-object v7, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v7, v7, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v7}, Landroid/media/MediaPlayer;->start()V

    goto :goto_0

    .line 381
    :cond_8
    if-nez v4, :cond_9

    .line 386
    :cond_9
    if-eq v3, v12, :cond_6

    goto :goto_1
.end method

.method private handleStop(Z)V
    .locals 5
    .param p1, "forceStop"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 424
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->playingRequest:Lcom/vlingo/core/internal/audio/AudioRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->playingRequest:Lcom/vlingo/core/internal/audio/AudioRequest;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/audio/AudioRequest;->hasFlag(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 446
    :goto_0
    return-void

    .line 427
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    # getter for: Lcom/vlingo/core/internal/audio/AudioPlayer;->onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioPlayer;->access$100(Lcom/vlingo/core/internal/audio/AudioPlayer;)Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 428
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    # getter for: Lcom/vlingo/core/internal/audio/AudioPlayer;->onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioPlayer;->access$100(Lcom/vlingo/core/internal/audio/AudioPlayer;)Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;->cancel()V

    .line 429
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    # setter for: Lcom/vlingo/core/internal/audio/AudioPlayer;->onBtOnTask:Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;
    invoke-static {v0, v4}, Lcom/vlingo/core/internal/audio/AudioPlayer;->access$102(Lcom/vlingo/core/internal/audio/AudioPlayer;Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;)Lcom/vlingo/core/internal/bluetooth/OnBluetoothAudioOnTimeoutTask;

    .line 431
    :cond_1
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-boolean v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isSupposedToBePlaying:Z

    if-eqz v0, :cond_3

    .line 432
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-boolean v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isInitialized:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->playingRequest:Lcom/vlingo/core/internal/audio/AudioRequest;

    if-eqz v0, :cond_2

    .line 434
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 435
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iput-boolean v3, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isInitialized:Z

    .line 436
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->playingRequest:Lcom/vlingo/core/internal/audio/AudioRequest;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/audio/AudioRequest;->hasFlag(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 437
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->releaseMediaPlayer()V

    .line 440
    :cond_2
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->serviceListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v1, v1, Lcom/vlingo/core/internal/audio/AudioPlayer;->playingRequest:Lcom/vlingo/core/internal/audio/AudioRequest;

    sget-object v2, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;->STOPPED:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V

    .line 441
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->playingRequest:Lcom/vlingo/core/internal/audio/AudioRequest;

    iget-object v1, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v1, v1, Lcom/vlingo/core/internal/audio/AudioPlayer;->playingRequest:Lcom/vlingo/core/internal/audio/AudioRequest;

    sget-object v2, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;->STOPPED:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/audio/AudioRequest;->onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V

    .line 443
    :cond_3
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iput-object v4, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->playingRequest:Lcom/vlingo/core/internal/audio/AudioRequest;

    .line 444
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iput-boolean v3, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isSupposedToBePlaying:Z

    .line 445
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iput-boolean v3, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isWorking:Z

    goto :goto_0
.end method

.method private releaseMediaPlayer()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 293
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 297
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 298
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 299
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iput-object v1, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 301
    :cond_0
    return-void
.end method

.method private skipAudioStart(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    const/4 v2, 0x0

    .line 405
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v0, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->serviceListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 406
    invoke-virtual {p1, p1}, Lcom/vlingo/core/internal/audio/AudioRequest;->onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 407
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->playingRequest:Lcom/vlingo/core/internal/audio/AudioRequest;

    .line 408
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iput-boolean v2, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isSupposedToBePlaying:Z

    .line 409
    iget-object v0, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iput-boolean v2, v0, Lcom/vlingo/core/internal/audio/AudioPlayer;->isWorking:Z

    .line 410
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 189
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    # getter for: Lcom/vlingo/core/internal/audio/AudioPlayer;->isCalledMute:Z
    invoke-static {v4}, Lcom/vlingo/core/internal/audio/AudioPlayer;->access$000(Lcom/vlingo/core/internal/audio/AudioPlayer;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, p1, Landroid/os/Message;->what:I

    if-eq v4, v7, :cond_0

    iget v4, p1, Landroid/os/Message;->what:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 190
    :cond_0
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v4, v4, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v4, :cond_1

    .line 191
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    # setter for: Lcom/vlingo/core/internal/audio/AudioPlayer;->isCalledMute:Z
    invoke-static {v4, v6}, Lcom/vlingo/core/internal/audio/AudioPlayer;->access$002(Lcom/vlingo/core/internal/audio/AudioPlayer;Z)Z

    .line 195
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v4, v4, Lcom/vlingo/core/internal/audio/AudioPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4, v8, v8}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 198
    :cond_1
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-boolean v4, v4, Lcom/vlingo/core/internal/audio/AudioPlayer;->isShutDown:Z

    if-eqz v4, :cond_3

    .line 290
    :cond_2
    :goto_0
    return-void

    .line 199
    :cond_3
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/vlingo/core/internal/audio/AudioRequest;

    .line 200
    .local v3, "request":Lcom/vlingo/core/internal/audio/AudioRequest;
    iget v4, p1, Landroid/os/Message;->what:I

    sparse-switch v4, :sswitch_data_0

    goto :goto_0

    .line 223
    :sswitch_0
    invoke-virtual {v3, v7}, Lcom/vlingo/core/internal/audio/AudioRequest;->hasFlag(I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 224
    invoke-direct {p0, v7}, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->handleStop(Z)V

    .line 226
    :cond_4
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/audio/AudioPlayer;->isPlaying()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 227
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v4, v4, Lcom/vlingo/core/internal/audio/AudioPlayer;->serviceListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    sget-object v5, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;->OTHER_REQUEST_PLAYING:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    invoke-interface {v4, v3, v5}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V

    .line 228
    sget-object v4, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;->OTHER_REQUEST_PLAYING:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    invoke-virtual {v3, v3, v4}, Lcom/vlingo/core/internal/audio/AudioRequest;->onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V

    goto :goto_0

    .line 205
    :sswitch_1
    :try_start_0
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v4, v4, Lcom/vlingo/core/internal/audio/AudioPlayer;->ttsEngine:Lcom/vlingo/core/internal/audio/TTSEngine;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/audio/TTSEngine;->prepare()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 206
    :catch_0
    move-exception v0

    .line 207
    .local v0, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v4, v4, Lcom/vlingo/core/internal/audio/AudioPlayer;->context:Landroid/content/Context;

    instance-of v4, v4, Lcom/vlingo/core/internal/audio/AudioPlaybackService;

    if-eqz v4, :cond_2

    .line 208
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v4, v4, Lcom/vlingo/core/internal/audio/AudioPlayer;->context:Landroid/content/Context;

    check-cast v4, Lcom/vlingo/core/internal/audio/AudioPlaybackService;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->stopSelf()V

    goto :goto_0

    .line 231
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_5
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v4, v4, Lcom/vlingo/core/internal/audio/AudioPlayer;->context:Landroid/content/Context;

    iget-object v5, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    invoke-virtual {v3, v4, v5}, Lcom/vlingo/core/internal/audio/AudioRequest;->prepareForPlayback(Landroid/content/Context;Lcom/vlingo/core/internal/audio/AudioPlayer;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 232
    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->handleError(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    goto :goto_0

    .line 235
    :cond_6
    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->handleStart(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 236
    iget-boolean v4, v3, Lcom/vlingo/core/internal/audio/AudioRequest;->requestAudioFocus:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-boolean v4, v4, Lcom/vlingo/core/internal/audio/AudioPlayer;->existNotProcessedStopRequest:Z

    if-eqz v4, :cond_2

    .line 237
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getRecoState()Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    move-result-object v2

    .line 238
    .local v2, "recoState":Lcom/vlingo/sdk/recognition/VLRecognitionStates;
    sget-object v4, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->CONNECTING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    if-eq v2, v4, :cond_2

    sget-object v4, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->LISTENING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    if-eq v2, v4, :cond_2

    .line 241
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v4, v4, Lcom/vlingo/core/internal/audio/AudioPlayer;->audioFocusManager:Lcom/vlingo/core/internal/audio/AudioFocusManager;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    goto :goto_0

    .line 252
    .end local v2    # "recoState":Lcom/vlingo/sdk/recognition/VLRecognitionStates;
    :sswitch_2
    instance-of v4, v3, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;

    if-eqz v4, :cond_7

    move-object v1, v3

    .line 253
    check-cast v1, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;

    .line 254
    .local v1, "mreq":Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;->areMorePartsWaiting()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 255
    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->handleStart(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    goto/16 :goto_0

    .line 259
    .end local v1    # "mreq":Lcom/vlingo/core/internal/audio/MultiPartAudioRequest;
    :cond_7
    iget-boolean v4, v3, Lcom/vlingo/core/internal/audio/AudioRequest;->requestAudioFocus:Z

    if-eqz v4, :cond_8

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getVlingoApp()Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/util/VlingoApplicationInterface;->isAppInForeground()Z

    move-result v4

    if-nez v4, :cond_8

    .line 260
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v4, v4, Lcom/vlingo/core/internal/audio/AudioPlayer;->audioFocusManager:Lcom/vlingo/core/internal/audio/AudioFocusManager;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 262
    :cond_8
    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/audio/AudioRequest;->hasFlag(I)Z

    move-result v4

    if-nez v4, :cond_9

    .line 263
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->releaseMediaPlayer()V

    .line 265
    :cond_9
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    const/4 v5, 0x0

    iput-object v5, v4, Lcom/vlingo/core/internal/audio/AudioPlayer;->playingRequest:Lcom/vlingo/core/internal/audio/AudioRequest;

    .line 266
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iput-boolean v6, v4, Lcom/vlingo/core/internal/audio/AudioPlayer;->isSupposedToBePlaying:Z

    .line 267
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iget-object v4, v4, Lcom/vlingo/core/internal/audio/AudioPlayer;->serviceListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-interface {v4, v3}, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;->onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 268
    invoke-virtual {v3, v3}, Lcom/vlingo/core/internal/audio/AudioRequest;->onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 269
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iput-boolean v6, v4, Lcom/vlingo/core/internal/audio/AudioPlayer;->isWorking:Z

    goto/16 :goto_0

    .line 274
    :sswitch_3
    invoke-direct {p0, v6}, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->handleStop(Z)V

    .line 275
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iput-boolean v6, v4, Lcom/vlingo/core/internal/audio/AudioPlayer;->existNotProcessedStopRequest:Z

    goto/16 :goto_0

    .line 280
    :sswitch_4
    invoke-direct {p0, v3}, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->handleError(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    goto/16 :goto_0

    .line 283
    :sswitch_5
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iput-boolean v7, v4, Lcom/vlingo/core/internal/audio/AudioPlayer;->isShutDown:Z

    .line 284
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->releaseMediaPlayer()V

    .line 285
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->thread:Landroid/os/HandlerThread;

    invoke-virtual {v4}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/Looper;->quit()V

    .line 287
    iget-object v4, p0, Lcom/vlingo/core/internal/audio/AudioPlayer$AudioPlayerHandler;->this$0:Lcom/vlingo/core/internal/audio/AudioPlayer;

    iput-boolean v6, v4, Lcom/vlingo/core/internal/audio/AudioPlayer;->isWorking:Z

    goto/16 :goto_0

    .line 200
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_5
        0x4 -> :sswitch_1
        0x69 -> :sswitch_4
    .end sparse-switch
.end method
