.class public abstract Lcom/vlingo/core/internal/util/ServiceProxyBase;
.super Ljava/lang/Object;
.source "ServiceProxyBase.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<IS:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field protected static final IDLE_DELAY:I = 0x5

.field private static final IDLE_DELAY_INTERVAL:I = 0x7530

.field protected static final MSG_BASE:I = 0x14

.field protected static final RUN_QUEUE:I = 0x4

.field protected static final SERVICE_CONNECTED:I = 0x1

.field protected static final SERVICE_DISCONNECTED:I = 0x2


# instance fields
.field protected final context:Landroid/content/Context;

.field private final handler:Landroid/os/Handler;

.field private isConnected:Z

.field private isConnecting:Z

.field private lastCommandTime:J

.field private pendingRequestQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field private service:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TIS;"
        }
    .end annotation
.end field

.field private serviceToken:Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceToken;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .local p0, "this":Lcom/vlingo/core/internal/util/ServiceProxyBase;, "Lcom/vlingo/core/internal/util/ServiceProxyBase<TIS;TS;>;"
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-boolean v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->isConnecting:Z

    .line 36
    iput-boolean v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->isConnected:Z

    .line 38
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->lastCommandTime:J

    .line 41
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->pendingRequestQueue:Ljava/util/LinkedList;

    .line 42
    iput-object p1, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->context:Landroid/content/Context;

    .line 43
    new-instance v0, Lcom/vlingo/core/internal/util/ServiceProxyBase$1;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/vlingo/core/internal/util/ServiceProxyBase$1;-><init>(Lcom/vlingo/core/internal/util/ServiceProxyBase;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->handler:Landroid/os/Handler;

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/core/internal/util/ServiceProxyBase;Landroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/util/ServiceProxyBase;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/util/ServiceProxyBase;->handleBase(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/util/ServiceProxyBase;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/util/ServiceProxyBase;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private declared-synchronized handleBase(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .local p0, "this":Lcom/vlingo/core/internal/util/ServiceProxyBase;, "Lcom/vlingo/core/internal/util/ServiceProxyBase<TIS;TS;>;"
    const-wide/16 v6, 0x7530

    const/4 v5, 0x5

    .line 191
    monitor-enter p0

    :try_start_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 218
    :pswitch_0
    iget-boolean v1, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->isConnected:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 220
    :try_start_1
    iget-object v1, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->service:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 223
    iget-object v1, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->service:Ljava/lang/Object;

    invoke-virtual {p0, p1, v1}, Lcom/vlingo/core/internal/util/ServiceProxyBase;->handleMessageForService(Landroid/os/Message;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 241
    :cond_0
    :goto_0
    :try_start_2
    iget-boolean v1, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->isConnected:Z

    if-eqz v1, :cond_1

    iget v1, p1, Landroid/os/Message;->what:I

    if-eq v1, v5, :cond_1

    .line 242
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->lastCommandTime:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 245
    :cond_1
    monitor-exit p0

    return-void

    .line 193
    :pswitch_1
    :try_start_3
    iget-boolean v1, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->isConnecting:Z

    if-eqz v1, :cond_0

    .line 194
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iput-object v1, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->service:Ljava/lang/Object;

    .line 195
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->isConnected:Z

    .line 196
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->isConnecting:Z

    .line 197
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/ServiceProxyBase;->executeQueue()V

    .line 198
    iget-object v1, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->handler:Landroid/os/Handler;

    const/4 v2, 0x5

    const-wide/16 v3, 0x7530

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 191
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 202
    :pswitch_2
    :try_start_4
    iget-boolean v1, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->isConnected:Z

    if-eqz v1, :cond_0

    .line 203
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/ServiceProxyBase;->executeQueue()V

    goto :goto_0

    .line 206
    :pswitch_3
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/ServiceProxyBase;->release()V

    goto :goto_0

    .line 209
    :pswitch_4
    iget-boolean v1, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->isConnected:Z

    if-eqz v1, :cond_0

    .line 210
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/ServiceProxyBase;->allowsDelayDisconnect()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->lastCommandTime:J

    sub-long/2addr v1, v3

    cmp-long v1, v1, v6

    if-lez v1, :cond_2

    .line 211
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/ServiceProxyBase;->release()V

    goto :goto_0

    .line 213
    :cond_2
    iget-object v1, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->handler:Landroid/os/Handler;

    const/4 v2, 0x5

    const-wide/16 v3, 0x7530

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 229
    :catch_0
    move-exception v0

    .line 230
    .local v0, "ex":Ljava/lang/Exception;
    const-string/jumbo v1, "ServiceProxyBase"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 191
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method private declared-synchronized queue(IIILjava/lang/Object;)V
    .locals 1
    .param p1, "what"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    .line 99
    .local p0, "this":Lcom/vlingo/core/internal/util/ServiceProxyBase;, "Lcom/vlingo/core/internal/util/ServiceProxyBase<TIS;TS;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->handler:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/util/ServiceProxyBase;->queue(Landroid/os/Message;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    monitor-exit p0

    return-void

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized queue(Landroid/os/Message;)V
    .locals 1
    .param p1, "m"    # Landroid/os/Message;

    .prologue
    .line 103
    .local p0, "this":Lcom/vlingo/core/internal/util/ServiceProxyBase;, "Lcom/vlingo/core/internal/util/ServiceProxyBase<TIS;TS;>;"
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 104
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->pendingRequestQueue:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    :cond_0
    monitor-exit p0

    return-void

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected allowsDelayDisconnect()Z
    .locals 1

    .prologue
    .line 253
    .local p0, "this":Lcom/vlingo/core/internal/util/ServiceProxyBase;, "Lcom/vlingo/core/internal/util/ServiceProxyBase<TIS;TS;>;"
    const/4 v0, 0x1

    return v0
.end method

.method public declared-synchronized connect()V
    .locals 4

    .prologue
    .line 112
    .local p0, "this":Lcom/vlingo/core/internal/util/ServiceProxyBase;, "Lcom/vlingo/core/internal/util/ServiceProxyBase<TIS;TS;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->serviceToken:Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceToken;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->service:Ljava/lang/Object;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->isConnecting:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->isConnected:Z

    if-nez v0, :cond_0

    .line 113
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->isConnecting:Z

    .line 114
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->context:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/ServiceProxyBase;->getServiceClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/ServiceProxyBase;->getInterfaceClass()Ljava/lang/Class;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/util/ServiceProxyBase$2;

    invoke-direct {v3, p0}, Lcom/vlingo/core/internal/util/ServiceProxyBase$2;-><init>(Lcom/vlingo/core/internal/util/ServiceProxyBase;)V

    invoke-static {v0, v1, v2, v3}, Lcom/vlingo/core/internal/util/ServiceBindingUtil;->bindToService(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Class;Landroid/content/ServiceConnection;)Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceToken;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->serviceToken:Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceToken;

    .line 129
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->serviceToken:Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceToken;

    if-nez v0, :cond_0

    .line 130
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->isConnecting:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    :cond_0
    monitor-exit p0

    return-void

    .line 112
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized execute(I)V
    .locals 3
    .param p1, "what"    # I

    .prologue
    .line 58
    .local p0, "this":Lcom/vlingo/core/internal/util/ServiceProxyBase;, "Lcom/vlingo/core/internal/util/ServiceProxyBase<TIS;TS;>;"
    monitor-enter p0

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/vlingo/core/internal/util/ServiceProxyBase;->execute(IIILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    monitor-exit p0

    return-void

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized execute(IIILjava/lang/Object;)V
    .locals 2
    .param p1, "what"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    .line 75
    .local p0, "this":Lcom/vlingo/core/internal/util/ServiceProxyBase;, "Lcom/vlingo/core/internal/util/ServiceProxyBase<TIS;TS;>;"
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->isConnected:Z

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->handler:Landroid/os/Handler;

    invoke-virtual {v1, p1, p2, p3, p4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 83
    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->isConnecting:Z

    if-nez v0, :cond_2

    .line 87
    invoke-virtual {p0}, Lcom/vlingo/core/internal/util/ServiceProxyBase;->connect()V

    .line 90
    :cond_2
    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    .line 94
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/vlingo/core/internal/util/ServiceProxyBase;->queue(IIILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized execute(ILjava/lang/Object;)V
    .locals 2
    .param p1, "what"    # I
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 62
    .local p0, "this":Lcom/vlingo/core/internal/util/ServiceProxyBase;, "Lcom/vlingo/core/internal/util/ServiceProxyBase<TIS;TS;>;"
    monitor-enter p0

    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0, v1, p2}, Lcom/vlingo/core/internal/util/ServiceProxyBase;->execute(IIILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    monitor-exit p0

    return-void

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized executeQueue()V
    .locals 3

    .prologue
    .line 161
    .local p0, "this":Lcom/vlingo/core/internal/util/ServiceProxyBase;, "Lcom/vlingo/core/internal/util/ServiceProxyBase<TIS;TS;>;"
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->service:Ljava/lang/Object;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->pendingRequestQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 162
    iget-object v2, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->pendingRequestQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Message;

    .line 163
    .local v1, "queuedMessage":Landroid/os/Message;
    iget-object v2, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 161
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "queuedMessage":Landroid/os/Message;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 165
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->pendingRequestQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 167
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    monitor-exit p0

    return-void
.end method

.method protected abstract getInterfaceClass()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<TIS;>;"
        }
    .end annotation
.end method

.method protected abstract getMessageName(Landroid/os/Message;)Ljava/lang/String;
.end method

.method protected getMessageNameBase(Landroid/os/Message;)Ljava/lang/String;
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 170
    .local p0, "this":Lcom/vlingo/core/internal/util/ServiceProxyBase;, "Lcom/vlingo/core/internal/util/ServiceProxyBase<TIS;TS;>;"
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 180
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/util/ServiceProxyBase;->getMessageName(Landroid/os/Message;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 172
    :pswitch_1
    const-string/jumbo v0, "SERVICE_CONNECTED"

    goto :goto_0

    .line 174
    :pswitch_2
    const-string/jumbo v0, "SERVICE_DISCONNECTED"

    goto :goto_0

    .line 176
    :pswitch_3
    const-string/jumbo v0, "RUN_QUEUE"

    goto :goto_0

    .line 178
    :pswitch_4
    const-string/jumbo v0, "IDLE_DELAY"

    goto :goto_0

    .line 170
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected declared-synchronized getService()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TIS;"
        }
    .end annotation

    .prologue
    .line 136
    .local p0, "this":Lcom/vlingo/core/internal/util/ServiceProxyBase;, "Lcom/vlingo/core/internal/util/ServiceProxyBase<TIS;TS;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->service:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract getServiceClass()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<TS;>;"
        }
    .end annotation
.end method

.method protected abstract handleMessageForService(Landroid/os/Message;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Message;",
            "TIS;)V"
        }
    .end annotation
.end method

.method public declared-synchronized release()V
    .locals 2

    .prologue
    .line 143
    .local p0, "this":Lcom/vlingo/core/internal/util/ServiceProxyBase;, "Lcom/vlingo/core/internal/util/ServiceProxyBase<TIS;TS;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->serviceToken:Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceToken;

    if-eqz v0, :cond_1

    .line 144
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->isConnected:Z

    .line 145
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->isConnecting:Z

    .line 146
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->pendingRequestQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->pendingRequestQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->handler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 149
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->handler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 150
    iget-object v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->serviceToken:Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceToken;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ServiceBindingUtil;->unbindFromService(Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceToken;)V

    .line 151
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->serviceToken:Lcom/vlingo/core/internal/util/ServiceBindingUtil$ServiceToken;

    .line 152
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->service:Ljava/lang/Object;

    .line 153
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/core/internal/util/ServiceProxyBase;->lastCommandTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    :cond_1
    monitor-exit p0

    return-void

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
