.class public Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;
.super Lcom/vlingo/core/internal/contacts/scoring/ContactScore;
.source "ConstantContactScore.java"


# instance fields
.field private final score:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "score"    # I

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/scoring/ContactScore;-><init>()V

    .line 10
    iput p1, p0, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;->score:I

    .line 11
    return-void
.end method


# virtual methods
.method public getScore()I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;->score:I

    return v0
.end method

.method public getScore(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactMatch;)I
    .locals 1
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "match"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/core/internal/contacts/scoring/ConstantContactScore;->score:I

    return v0
.end method
