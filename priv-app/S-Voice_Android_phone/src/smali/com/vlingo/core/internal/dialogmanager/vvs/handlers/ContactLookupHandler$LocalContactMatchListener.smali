.class Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;
.super Ljava/lang/Object;
.source "ContactLookupHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/contacts/ContactMatchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocalContactMatchListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;


# direct methods
.method private constructor <init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)V
    .locals 0

    .prologue
    .line 358
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$1;

    .prologue
    .line 358
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;-><init>(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)V

    return-void
.end method


# virtual methods
.method public onAutoAction(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 0
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 361
    return-void
.end method

.method public onContactMatchResultsUpdated(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 364
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    return-void
.end method

.method public onContactMatchingFailed()V
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$1600(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 425
    return-void
.end method

.method public onContactMatchingFinished(Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/4 v12, 0x1

    .line 369
    :try_start_0
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$800(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const/4 v11, 0x0

    invoke-interface {v9, v10, v11}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 370
    if-eqz p1, :cond_0

    .line 372
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v9

    if-ne v9, v12, :cond_8

    .line 373
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    iget-object v9, v9, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v10, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v9, v10, :cond_2

    .line 374
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$900(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-interface {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const/4 v9, 0x0

    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v9

    iget-object v11, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    iget-object v12, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    iget-object v12, v12, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getPhoneType(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Ljava/lang/String;
    invoke-static {v11, v12}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$1000(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v9, v11}, Lcom/vlingo/core/internal/util/DialUtil;->getPhoneTypeMap(Landroid/content/res/Resources;Ljava/util/List;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v7

    .line 375
    .local v7, "phones":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-interface {v7}, Ljava/util/Map;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 376
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->setProcessingActions()V

    .line 377
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->additionalMatchContacts()V
    invoke-static {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$1100(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 415
    .end local v7    # "phones":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :cond_0
    :goto_0
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$1500(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-interface {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 417
    return-void

    .line 379
    .restart local v7    # "phones":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :cond_1
    :try_start_1
    iget-object v10, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    const/4 v9, 0x0

    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v10, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->openContact(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 415
    .end local v7    # "phones":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :catchall_0
    move-exception v9

    iget-object v10, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$1500(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v10

    invoke-interface {v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    throw v9

    .line 381
    :cond_2
    :try_start_2
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    iget-object v9, v9, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v10, Lcom/vlingo/core/internal/contacts/ContactType;->EMAIL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v9, v10, :cond_7

    .line 382
    const/4 v9, 0x0

    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getEmailData()Ljava/util/List;

    move-result-object v2

    .line 383
    .local v2, "data":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const/4 v3, 0x0

    .line 384
    .local v3, "hasDataToDisplay":Z
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 385
    .local v1, "d":Lcom/vlingo/core/internal/contacts/ContactData;
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->emailTypes:[I
    invoke-static {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$1200(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)[I

    move-result-object v0

    .local v0, "arr$":[I
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_2
    if-ge v5, v6, :cond_3

    aget v8, v0, v5

    .line 386
    .local v8, "typeEmail":I
    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/ContactData;->getType()I

    move-result v9

    if-ne v9, v8, :cond_4

    .line 387
    const/4 v3, 0x1

    .line 388
    goto :goto_1

    .line 385
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 392
    .end local v0    # "arr$":[I
    .end local v1    # "d":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v8    # "typeEmail":I
    :cond_5
    if-eqz v3, :cond_6

    .line 393
    iget-object v10, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    const/4 v9, 0x0

    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v10, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->openContact(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    goto :goto_0

    .line 395
    :cond_6
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->additionalMatchContacts()V
    invoke-static {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$1100(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)V

    goto :goto_0

    .line 398
    .end local v2    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .end local v3    # "hasDataToDisplay":Z
    :cond_7
    iget-object v10, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    const/4 v9, 0x0

    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v10, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->openContact(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    goto :goto_0

    .line 400
    :cond_8
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v9

    if-le v9, v12, :cond_9

    .line 401
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$1300(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-static {v9, p1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/util/List;)V

    .line 402
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    invoke-virtual {v9, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->multipleContacts(Ljava/util/List;)V

    goto/16 :goto_0

    .line 403
    :cond_9
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    iget-object v9, v9, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v10, Lcom/vlingo/core/internal/contacts/ContactType;->EMAIL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-eq v9, v10, :cond_a

    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    iget-object v9, v9, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v10, Lcom/vlingo/core/internal/contacts/ContactType;->ADDRESS:Lcom/vlingo/core/internal/contacts/ContactType;

    if-eq v9, v10, :cond_a

    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    iget-object v9, v9, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v10, Lcom/vlingo/core/internal/contacts/ContactType;->BIRTHDAY:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v9, v10, :cond_b

    .line 407
    :cond_a
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->setProcessingActions()V

    .line 408
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->additionalMatchContacts()V
    invoke-static {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$1100(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)V

    goto/16 :goto_0

    .line 410
    :cond_b
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    # invokes: Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->access$1400(Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_NAME:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const/4 v11, 0x0

    invoke-interface {v9, v10, v11}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 411
    iget-object v9, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    iget-object v10, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    iget-object v11, p0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;

    iget-object v11, v11, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->name:Ljava/lang/String;

    invoke-virtual {v10, v11}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->getContactNotFoundString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->showSystemTurn(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method
