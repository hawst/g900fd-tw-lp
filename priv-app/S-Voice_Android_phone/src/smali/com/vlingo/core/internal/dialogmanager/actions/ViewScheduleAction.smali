.class public Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "ViewScheduleAction.java"


# instance fields
.field private mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method


# virtual methods
.method protected execute()V
    .locals 4

    .prologue
    .line 24
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 25
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.android.calendar"

    const-string/jumbo v2, "com.android.calendar.EventInfoActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 26
    const-string/jumbo v1, "vnd.android.cursor.item/event"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 27
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEventID()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 28
    const-string/jumbo v1, "title"

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 29
    const-string/jumbo v1, "beginTime"

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBegin()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 30
    const-string/jumbo v1, "endTime"

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEnd()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 31
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 32
    return-void
.end method

.method public readyToExec()V
    .locals 0

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;->execute()V

    .line 40
    return-void
.end method

.method public scheduleEvent(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;
    .locals 0
    .param p1, "scheduleEvent"    # Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/ViewScheduleAction;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 19
    return-object p0
.end method
