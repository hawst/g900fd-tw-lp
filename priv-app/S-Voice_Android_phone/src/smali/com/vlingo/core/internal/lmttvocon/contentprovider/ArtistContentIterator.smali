.class public Lcom/vlingo/core/internal/lmttvocon/contentprovider/ArtistContentIterator;
.super Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;
.source "ArtistContentIterator.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator",
        "<",
        "Lcom/vlingo/core/internal/lmttvocon/contentprovider/ArtistContentItem;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 10
    sget-object v2, Landroid/provider/MediaStore$Audio$Artists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "_id"

    aput-object v1, v3, v0

    const/4 v0, 0x1

    const-string/jumbo v1, "artist"

    aput-object v1, v3, v0

    move-object v0, p0

    move-object v1, p1

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    return-void
.end method


# virtual methods
.method public bridge synthetic hasNext()Z
    .locals 1

    .prologue
    .line 8
    invoke-super {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method protected parseItem(Landroid/database/Cursor;)Lcom/vlingo/core/internal/lmttvocon/contentprovider/ArtistContentItem;
    .locals 7
    .param p1, "cursorParam"    # Landroid/database/Cursor;

    .prologue
    .line 17
    const-string/jumbo v4, "_id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 18
    .local v1, "idColumnIndex":I
    const-string/jumbo v4, "artist"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 20
    .local v3, "nameColumnIndex":I
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 21
    .local v0, "id":Ljava/lang/Long;
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 23
    .local v2, "name":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    :goto_0
    return-object v4

    :cond_0
    new-instance v4, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ArtistContentItem;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-direct {v4, v5, v6, v2}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ArtistContentItem;-><init>(JLjava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic parseItem(Landroid/database/Cursor;)Lcom/vlingo/core/internal/lmttvocon/contentprovider/ContentItem;
    .locals 1
    .param p1, "x0"    # Landroid/database/Cursor;

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/ArtistContentIterator;->parseItem(Landroid/database/Cursor;)Lcom/vlingo/core/internal/lmttvocon/contentprovider/ArtistContentItem;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic remove()V
    .locals 0

    .prologue
    .line 8
    invoke-super {p0}, Lcom/vlingo/core/internal/lmttvocon/contentprovider/BaseUriContentIterator;->remove()V

    return-void
.end method
