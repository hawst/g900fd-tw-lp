.class public interface abstract Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;
.super Ljava/lang/Object;
.source "IAudioPlaybackService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/audio/IAudioPlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AudioPlaybackListener"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;,
        Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;
    }
.end annotation


# virtual methods
.method public abstract onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
.end method

.method public abstract onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
.end method

.method public abstract onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
.end method

.method public abstract onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
.end method
