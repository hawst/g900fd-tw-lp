.class public Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
.super Ljava/lang/Object;
.source "DialogTurn.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/dialogmanager/DialogTurn$1;,
        Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;,
        Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;
    }
.end annotation


# static fields
.field static final TAG:Ljava/lang/String;


# instance fields
.field private actionHandlerProxy:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

.field private alerts:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;"
        }
    .end annotation
.end field

.field private asyncHandlersFinished:I

.field private asyncHandlersStarted:I

.field private dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

.field private doesEventResolveQuery:Z

.field private fieldId:Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

.field private pendingConcatenatedEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/DialogEvent;",
            ">;"
        }
    .end annotation
.end field

.field private pendingUniqueEvents:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogEvent;",
            ">;"
        }
    .end annotation
.end field

.field private result:Lcom/vlingo/sdk/recognition/VLRecognitionResult;

.field private turnWasCancelled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;Lcom/vlingo/sdk/recognition/VLRecognitionResult;Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;)V
    .locals 2
    .param p1, "previousTurn"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    .param p2, "result"    # Lcom/vlingo/sdk/recognition/VLRecognitionResult;
    .param p3, "dialogTurnListener"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    .prologue
    const/4 v1, 0x0

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->pendingUniqueEvents:Ljava/util/Map;

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->pendingConcatenatedEvents:Ljava/util/List;

    .line 92
    iput-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->doesEventResolveQuery:Z

    .line 95
    iput-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->turnWasCancelled:Z

    .line 96
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;Lcom/vlingo/core/internal/dialogmanager/DialogTurn$1;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->actionHandlerProxy:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    .line 113
    if-nez p1, :cond_0

    .line 114
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "previousTurn is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116
    :cond_0
    iget-object v0, p1, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->fieldId:Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->fieldId:Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .line 117
    invoke-direct {p0, p2, p3}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->init(Lcom/vlingo/sdk/recognition/VLRecognitionResult;Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;)V

    .line 118
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->pendingUniqueEvents:Ljava/util/Map;

    iget-object v1, p1, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->pendingUniqueEvents:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 119
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->pendingConcatenatedEvents:Ljava/util/List;

    iget-object v1, p1, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->pendingConcatenatedEvents:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 120
    return-void
.end method

.method constructor <init>(Lcom/vlingo/sdk/recognition/VLRecognitionResult;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;)V
    .locals 2
    .param p1, "result"    # Lcom/vlingo/sdk/recognition/VLRecognitionResult;
    .param p2, "fieldId"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .param p3, "dialogTurnListener"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    .prologue
    const/4 v1, 0x0

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->pendingUniqueEvents:Ljava/util/Map;

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->pendingConcatenatedEvents:Ljava/util/List;

    .line 92
    iput-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->doesEventResolveQuery:Z

    .line 95
    iput-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->turnWasCancelled:Z

    .line 96
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;Lcom/vlingo/core/internal/dialogmanager/DialogTurn$1;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->actionHandlerProxy:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    .line 99
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->fieldId:Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .line 100
    invoke-direct {p0, p1, p3}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->init(Lcom/vlingo/sdk/recognition/VLRecognitionResult;Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;)V

    .line 101
    return-void
.end method

.method constructor <init>(Ljava/util/LinkedList;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;)V
    .locals 3
    .param p2, "fieldId"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .param p3, "dialogTurnListener"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->pendingUniqueEvents:Ljava/util/Map;

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->pendingConcatenatedEvents:Ljava/util/List;

    .line 92
    iput-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->doesEventResolveQuery:Z

    .line 95
    iput-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->turnWasCancelled:Z

    .line 96
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    invoke-direct {v0, p0, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;-><init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;Lcom/vlingo/core/internal/dialogmanager/DialogTurn$1;)V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->actionHandlerProxy:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    .line 104
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "alerts is null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_1
    invoke-direct {p0, v2, p3}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->init(Lcom/vlingo/sdk/recognition/VLRecognitionResult;Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;)V

    .line 108
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->alerts:Ljava/util/LinkedList;

    .line 109
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->fieldId:Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .line 110
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;
    .param p1, "x1"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    return-void
.end method

.method static synthetic access$300(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->turnWasCancelled:Z

    return v0
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .prologue
    .line 45
    iget v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->asyncHandlersFinished:I

    return v0
.end method

.method static synthetic access$408(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)I
    .locals 2
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .prologue
    .line 45
    iget v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->asyncHandlersFinished:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->asyncHandlersFinished:I

    return v0
.end method

.method static synthetic access$500(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .prologue
    .line 45
    iget v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->asyncHandlersStarted:I

    return v0
.end method

.method static synthetic access$508(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)I
    .locals 2
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .prologue
    .line 45
    iget v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->asyncHandlersStarted:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->asyncHandlersStarted:I

    return v0
.end method

.method static synthetic access$600(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->finishActionListProcessing()V

    return-void
.end method

.method private enterDomain(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 218
    .local p1, "actionList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/VLAction;>;"
    const/4 v1, 0x0

    .line 219
    .local v1, "decider":Lcom/vlingo/core/internal/domain/DomainDecider;
    sget-object v5, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->DomainDecider:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v5}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v4

    .line 220
    .local v4, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    if-eqz v4, :cond_0

    .line 222
    :try_start_0
    invoke-virtual {v4}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/vlingo/core/internal/domain/DomainDecider;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 229
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 230
    invoke-interface {v1, p1}, Lcom/vlingo/core/internal/domain/DomainDecider;->getDomain(Ljava/util/List;)Lcom/vlingo/core/internal/domain/DomainName;

    move-result-object v2

    .line 233
    .local v2, "domain":Lcom/vlingo/core/internal/domain/DomainName;
    if-eqz v2, :cond_1

    .line 234
    iget-object v5, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    invoke-interface {v5, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;->enteredDomain(Lcom/vlingo/core/internal/domain/DomainName;)V

    .line 237
    .end local v2    # "domain":Lcom/vlingo/core/internal/domain/DomainName;
    :cond_1
    return-void

    .line 223
    :catch_0
    move-exception v3

    .line 224
    .local v3, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v3}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 225
    .end local v3    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v3

    .line 226
    .local v3, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v3}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0
.end method

.method private finishActionListProcessing()V
    .locals 1

    .prologue
    .line 322
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->turnWasCancelled:Z

    if-nez v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    invoke-interface {v0, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;->onDoneProcessingActions(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 325
    :cond_0
    return-void
.end method

.method private init(Lcom/vlingo/sdk/recognition/VLRecognitionResult;Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;)V
    .locals 3
    .param p1, "resultParam"    # Lcom/vlingo/sdk/recognition/VLRecognitionResult;
    .param p2, "dialogTurnListenerParam"    # Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    .prologue
    .line 123
    if-nez p2, :cond_0

    .line 124
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "dialogTurnListener is null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 126
    :cond_0
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->result:Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    .line 127
    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    .line 128
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->result:Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->result:Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    invoke-interface {v1}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getFieldId()Ljava/lang/String;

    move-result-object v0

    .line 129
    .local v0, "newID":Ljava/lang/String;
    :goto_0
    invoke-static {v0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 130
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->from(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->fieldId:Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .line 132
    :cond_1
    return-void

    .line 128
    .end local v0    # "newID":Ljava/lang/String;
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    .locals 0
    .param p1, "fieldId"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .prologue
    .line 243
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->fieldId:Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    .line 244
    return-void
.end method


# virtual methods
.method addEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/vlingo/core/internal/dialogmanager/DialogEvent;

    .prologue
    .line 271
    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;->isUnique()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 272
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->pendingUniqueEvents:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    :cond_0
    :goto_0
    return-void

    .line 280
    :cond_1
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->pendingConcatenatedEvents:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 281
    iget-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->doesEventResolveQuery:Z

    if-nez v1, :cond_0

    instance-of v1, p1, Lcom/vlingo/core/internal/dialogmanager/events/QueryEvent;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 282
    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/events/QueryEvent;

    .line 283
    .local v0, "qEvent":Lcom/vlingo/core/internal/dialogmanager/events/QueryEvent;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/events/QueryEvent;->isMeaningful()Z

    move-result v1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->doesEventResolveQuery:Z

    goto :goto_0
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->turnWasCancelled:Z

    .line 139
    return-void
.end method

.method public clearEvents()V
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->pendingUniqueEvents:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 301
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->pendingConcatenatedEvents:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 302
    return-void
.end method

.method public doesEventResolveQuery()Z
    .locals 1

    .prologue
    .line 305
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->doesEventResolveQuery:Z

    return v0
.end method

.method public getFieldId()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->fieldId:Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    return-object v0
.end method

.method public getGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->result:Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->result:Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getDialogGUID()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGUttId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->result:Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->result:Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getGUttId()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNBestData()Lcom/vlingo/sdk/recognition/NBestData;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->result:Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getNBestData()Lcom/vlingo/sdk/recognition/NBestData;

    move-result-object v0

    return-object v0
.end method

.method public getPendingEvents()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/DialogEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 289
    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->pendingConcatenatedEvents:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 290
    .local v0, "allEvents":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/DialogEvent;>;"
    iget-object v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->pendingUniqueEvents:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/DialogEvent;

    .line 291
    .local v1, "event":Lcom/vlingo/core/internal/dialogmanager/DialogEvent;
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 293
    .end local v1    # "event":Lcom/vlingo/core/internal/dialogmanager/DialogEvent;
    :cond_0
    return-object v0
.end method

.method public getServerState()[B
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->result:Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->result:Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getDialogState()[B

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTurnNumber()I
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->result:Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->result:Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getDialogTurn()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getUserText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->result:Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->result:Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getResultString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->turnWasCancelled:Z

    return v0
.end method

.method public isFromEDM()Z
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->result:Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->result:Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->isFromEDM()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method processActions()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 195
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->result:Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    invoke-interface {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getActions()Ljava/util/List;

    move-result-object v0

    .line 197
    .local v0, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/VLAction;>;"
    invoke-direct {p0, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->enterDomain(Ljava/util/List;)V

    .line 202
    iput v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->asyncHandlersStarted:I

    .line 203
    iput v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->asyncHandlersFinished:I

    .line 205
    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->actionHandlerProxy:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    invoke-static {v0, v2, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->processActionList(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)I

    move-result v1

    .line 210
    .local v1, "count":I
    iput v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->asyncHandlersStarted:I

    .line 211
    iget v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->asyncHandlersStarted:I

    iget v3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->asyncHandlersFinished:I

    if-ne v2, v3, :cond_0

    .line 213
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->finishActionListProcessing()V

    .line 215
    :cond_0
    return-void
.end method

.method processClientFlow()V
    .locals 0

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->finishActionListProcessing()V

    .line 192
    return-void
.end method

.method processReplyMessage(Lcom/vlingo/core/internal/messages/SMSMMSAlert;Z)V
    .locals 3
    .param p1, "message"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .param p2, "isSilentHandler"    # Z

    .prologue
    .line 180
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/MessageReplyHandler;

    invoke-direct {v0, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/MessageReplyHandler;-><init>(Z)V

    .local v0, "srh":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
    move-object v1, v0

    .line 181
    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/MessageReplyHandler;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/MessageReplyHandler;->init(Ljava/util/LinkedList;)V

    move-object v1, v0

    .line 182
    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/MessageReplyHandler;

    invoke-virtual {v1, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/MessageReplyHandler;->init(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)V

    .line 183
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->actionHandlerProxy:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->setListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 184
    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->setTurn(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 185
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->actionHandlerProxy:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 187
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->finishActionListProcessing()V

    .line 188
    return-void
.end method

.method processSafeReaderMessage(Lcom/vlingo/core/internal/messages/SMSMMSAlert;Z)V
    .locals 3
    .param p1, "message"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .param p2, "isSilentHandler"    # Z

    .prologue
    .line 163
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->alerts:Ljava/util/LinkedList;

    invoke-static {p2, v1}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getSafeReaderHandler(ZLjava/util/LinkedList;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;

    move-result-object v0

    .line 164
    .local v0, "srh":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
    if-nez v0, :cond_0

    .line 165
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SafeReaderHandler;

    .end local v0    # "srh":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
    invoke-direct {v0, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SafeReaderHandler;-><init>(Z)V

    .restart local v0    # "srh":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
    move-object v1, v0

    .line 166
    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SafeReaderHandler;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SafeReaderHandler;->init(Ljava/util/LinkedList;)V

    move-object v1, v0

    .line 167
    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SafeReaderHandler;

    invoke-virtual {v1, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/SafeReaderHandler;->init(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)V

    .line 174
    :goto_0
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->finishActionListProcessing()V

    .line 175
    return-void

    .line 169
    :cond_0
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->actionHandlerProxy:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->setListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 170
    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->setTurn(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 171
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->actionHandlerProxy:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    goto :goto_0
.end method

.method processSafeReaderMessage(Z)V
    .locals 3
    .param p1, "isSilentHandler"    # Z

    .prologue
    .line 153
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->alerts:Ljava/util/LinkedList;

    invoke-static {p1, v1}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getSafeReaderHandler(ZLjava/util/LinkedList;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;

    move-result-object v0

    .line 154
    .local v0, "srh":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->actionHandlerProxy:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->setListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 155
    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->setTurn(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 156
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->actionHandlerProxy:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 157
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->finishActionListProcessing()V

    .line 158
    return-void
.end method

.method public setResult(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)V
    .locals 0
    .param p1, "result"    # Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->result:Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    .line 147
    return-void
.end method
