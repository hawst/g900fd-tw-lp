.class public final enum Lcom/vlingo/core/internal/util/Alarm$WeekendType;
.super Ljava/lang/Enum;
.source "Alarm.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/core/internal/util/Alarm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WeekendType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/internal/util/Alarm$WeekendType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/internal/util/Alarm$WeekendType;

.field public static final enum FRI_SAT:Lcom/vlingo/core/internal/util/Alarm$WeekendType;

.field public static final enum SAT_SUN:Lcom/vlingo/core/internal/util/Alarm$WeekendType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 71
    new-instance v0, Lcom/vlingo/core/internal/util/Alarm$WeekendType;

    const-string/jumbo v1, "SAT_SUN"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/util/Alarm$WeekendType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/Alarm$WeekendType;->SAT_SUN:Lcom/vlingo/core/internal/util/Alarm$WeekendType;

    .line 72
    new-instance v0, Lcom/vlingo/core/internal/util/Alarm$WeekendType;

    const-string/jumbo v1, "FRI_SAT"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/internal/util/Alarm$WeekendType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/internal/util/Alarm$WeekendType;->FRI_SAT:Lcom/vlingo/core/internal/util/Alarm$WeekendType;

    .line 70
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/core/internal/util/Alarm$WeekendType;

    sget-object v1, Lcom/vlingo/core/internal/util/Alarm$WeekendType;->SAT_SUN:Lcom/vlingo/core/internal/util/Alarm$WeekendType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/internal/util/Alarm$WeekendType;->FRI_SAT:Lcom/vlingo/core/internal/util/Alarm$WeekendType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/core/internal/util/Alarm$WeekendType;->$VALUES:[Lcom/vlingo/core/internal/util/Alarm$WeekendType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/util/Alarm$WeekendType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 70
    const-class v0, Lcom/vlingo/core/internal/util/Alarm$WeekendType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/util/Alarm$WeekendType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/internal/util/Alarm$WeekendType;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/vlingo/core/internal/util/Alarm$WeekendType;->$VALUES:[Lcom/vlingo/core/internal/util/Alarm$WeekendType;

    invoke-virtual {v0}, [Lcom/vlingo/core/internal/util/Alarm$WeekendType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/internal/util/Alarm$WeekendType;

    return-object v0
.end method
