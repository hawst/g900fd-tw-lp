.class Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$6;
.super Ljava/lang/Object;
.source "DialogTurn.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->showUserText(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

.field final synthetic val$text:Ljava/lang/String;

.field final synthetic val$turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;


# direct methods
.method constructor <init>(Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 415
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$6;->this$1:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    iput-object p2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$6;->val$turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    iput-object p3, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$6;->val$text:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 417
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$6;->this$1:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->access$100(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 418
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->getInstance()Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->onUserUtt()V

    .line 419
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$6;->val$turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    if-nez v0, :cond_1

    .line 420
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$6;->this$1:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->access$100(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$6;->val$text:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;->showUserText(Ljava/lang/String;)V

    .line 425
    :cond_0
    :goto_0
    return-void

    .line 422
    :cond_1
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$6;->this$1:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;

    iget-object v0, v0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy;->this$0:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    # getter for: Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->dialogTurnListener:Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;
    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->access$100(Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$6;->val$text:Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$VvsActionHandlerListenerProxy$6;->val$turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn;->getNBestData()Lcom/vlingo/sdk/recognition/NBestData;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogTurn$DialogTurnListener;->showUserText(Ljava/lang/String;Lcom/vlingo/sdk/recognition/NBestData;)V

    goto :goto_0
.end method
