.class public Lcom/vlingo/core/internal/dialogmanager/actions/SetTimerAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "SetTimerAction.java"


# instance fields
.field private time:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method


# virtual methods
.method protected execute()V
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SetTimerAction;->time:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 23
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/SetTimerAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 25
    :cond_0
    return-void
.end method

.method public time(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SetTimerAction;
    .locals 0
    .param p1, "timeParam"    # Ljava/lang/String;

    .prologue
    .line 15
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/actions/SetTimerAction;->time:Ljava/lang/String;

    .line 16
    return-object p0
.end method
