.class public final Lcom/vlingo/core/internal/logging/Logger;
.super Ljava/lang/Object;
.source "Logger.java"

# interfaces
.implements Lcom/vlingo/core/facade/logging/ILogger;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/core/internal/logging/Logger$LogMatcher;,
        Lcom/vlingo/core/internal/logging/Logger$LogListener;
    }
.end annotation


# static fields
.field private static listeners:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Lcom/vlingo/core/internal/logging/Logger$LogMatcher;",
            "Lcom/vlingo/core/internal/logging/Logger$LogListener;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private m_ClassName:Ljava/lang/String;

.field private m_isEnabled:Z

.field private m_prefix:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/logging/Logger;->listeners:Ljava/util/Hashtable;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "prefix"    # Ljava/lang/String;
    .param p3, "enabled"    # Z

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/core/internal/logging/Logger;->m_isEnabled:Z

    .line 52
    const/16 v1, 0x2e

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 53
    .local v0, "i":I
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/core/internal/logging/Logger;->m_ClassName:Ljava/lang/String;

    .line 54
    iput-object p2, p0, Lcom/vlingo/core/internal/logging/Logger;->m_prefix:Ljava/lang/String;

    .line 55
    iput-boolean p3, p0, Lcom/vlingo/core/internal/logging/Logger;->m_isEnabled:Z

    .line 56
    return-void
.end method

.method public static clearLogListeners()V
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/vlingo/core/internal/logging/Logger;->listeners:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 45
    return-void
.end method

.method public static getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/vlingo/core/internal/logging/Logger;"
        }
    .end annotation

    .prologue
    .line 59
    .local p0, "class1":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    return-object v0
.end method

.method public static getLogger(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/core/internal/logging/Logger;
    .locals 1
    .param p1, "prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/vlingo/core/internal/logging/Logger;"
        }
    .end annotation

    .prologue
    .line 67
    .local p0, "class1":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;Ljava/lang/String;Z)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    return-object v0
.end method

.method public static getLogger(Ljava/lang/Class;Ljava/lang/String;Z)Lcom/vlingo/core/internal/logging/Logger;
    .locals 2
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "enabled"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/vlingo/core/internal/logging/Logger;"
        }
    .end annotation

    .prologue
    .line 71
    .local p0, "class1":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-nez p0, :cond_0

    .line 72
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 73
    :cond_0
    if-nez p1, :cond_1

    .line 74
    const-string/jumbo p1, "VAC_"

    .line 76
    :cond_1
    new-instance v0, Lcom/vlingo/core/internal/logging/Logger;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2}, Lcom/vlingo/core/internal/logging/Logger;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public static getLogger(Ljava/lang/Class;Z)Lcom/vlingo/core/internal/logging/Logger;
    .locals 1
    .param p1, "enabled"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;Z)",
            "Lcom/vlingo/core/internal/logging/Logger;"
        }
    .end annotation

    .prologue
    .line 63
    .local p0, "class1":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;Ljava/lang/String;Z)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    return-object v0
.end method

.method private getThread()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static setLogListener(Lcom/vlingo/core/internal/logging/Logger$LogMatcher;Lcom/vlingo/core/internal/logging/Logger$LogListener;)V
    .locals 1
    .param p0, "matcher"    # Lcom/vlingo/core/internal/logging/Logger$LogMatcher;
    .param p1, "listener"    # Lcom/vlingo/core/internal/logging/Logger$LogListener;

    .prologue
    .line 40
    sget-object v0, Lcom/vlingo/core/internal/logging/Logger;->listeners:Ljava/util/Hashtable;

    invoke-virtual {v0, p0, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    return-void
.end method


# virtual methods
.method public debug(Ljava/lang/String;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 92
    return-void
.end method

.method public error(Ljava/lang/String;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 128
    return-void
.end method

.method public error(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "code"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 121
    return-void
.end method

.method public error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "code"    # Ljava/lang/String;
    .param p3, "msg"    # Ljava/lang/String;

    .prologue
    .line 109
    return-void
.end method

.method public info(Ljava/lang/String;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 137
    return-void
.end method

.method public warn(Ljava/lang/String;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 101
    return-void
.end method
