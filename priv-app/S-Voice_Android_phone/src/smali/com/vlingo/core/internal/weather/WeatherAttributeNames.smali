.class public Lcom/vlingo/core/internal/weather/WeatherAttributeNames;
.super Ljava/lang/Object;
.source "WeatherAttributeNames.java"


# static fields
.field public static final FORECAST_DATE:Ljava/lang/String; = "ForecastDate"

.field public static final LOCATION:Ljava/lang/String; = "Location"

.field public static final LONG_TEXT:Ljava/lang/String; = "LongText"

.field public static final SHORT_TEXT:Ljava/lang/String; = "ShortText"

.field public static final TEMPERATURE:Ljava/lang/String; = "Temperature"

.field public static final TEMP_MAX:Ljava/lang/String; = "TempMax"

.field public static final TEMP_MAX_C:Ljava/lang/String; = "TempMaxC"

.field public static final TEMP_MIN:Ljava/lang/String; = "TempMin"

.field public static final TEMP_MIN_C:Ljava/lang/String; = "TempMinC"

.field public static final WEATHER_CODE:Ljava/lang/String; = "WeatherCode"

.field public static final WEATHER_TEXT:Ljava/lang/String; = "WeatherText"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
