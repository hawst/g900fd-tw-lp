.class public abstract Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;
.super Lcom/vlingo/core/internal/dialogmanager/events/QueryEvent;
.source "CountQueryEvent.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/vlingo/core/internal/dialogmanager/events/QueryEvent;"
    }
.end annotation


# instance fields
.field private final items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final offset:I

.field private final totalCount:I


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    .local p0, "this":Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;, "Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent<TT;>;"
    const/4 v1, 0x0

    .line 24
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/events/QueryEvent;-><init>()V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;->items:Ljava/util/List;

    .line 26
    iput v1, p0, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;->offset:I

    .line 27
    iput v1, p0, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;->totalCount:I

    .line 28
    return-void
.end method

.method protected constructor <init>(Ljava/util/List;II)V
    .locals 0
    .param p2, "offset"    # I
    .param p3, "totalCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;II)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p0, "this":Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;, "Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent<TT;>;"
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/events/QueryEvent;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;->items:Ljava/util/List;

    .line 20
    iput p2, p0, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;->offset:I

    .line 21
    iput p3, p0, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;->totalCount:I

    .line 22
    return-void
.end method


# virtual methods
.method public getItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "this":Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;, "Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent<TT;>;"
    iget-object v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;->items:Ljava/util/List;

    return-object v0
.end method

.method public getOffset()I
    .locals 1

    .prologue
    .line 35
    .local p0, "this":Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;, "Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent<TT;>;"
    iget v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;->offset:I

    return v0
.end method

.method public getTotalCount()I
    .locals 1

    .prologue
    .line 39
    .local p0, "this":Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;, "Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent<TT;>;"
    iget v0, p0, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;->totalCount:I

    return v0
.end method

.method public isMeaningful()Z
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;, "Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent<TT;>;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;->getItems()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected writeTotalCount(Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .prologue
    .line 43
    .local p0, "this":Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;, "Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent<TT;>;"
    invoke-virtual {p0}, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;->getTotalCount()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;->writeTotalCount(Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;I)V

    .line 44
    return-void
.end method

.method protected writeTotalCount(Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;I)V
    .locals 2
    .param p1, "builder"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;
    .param p2, "count"    # I

    .prologue
    .line 48
    .local p0, "this":Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent;, "Lcom/vlingo/core/internal/dialogmanager/events/CountQueryEvent<TT;>;"
    const-string/jumbo v0, "matchcount"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->eventField(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .line 49
    return-void
.end method
