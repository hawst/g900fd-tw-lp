.class public final Lcom/vlingo/core/internal/contacts/mru/MRU;
.super Ljava/lang/Object;
.source "MRU.java"


# static fields
.field private static final APPLICATION_NAMES:[Ljava/lang/String;

.field private static final COLUMNS:[Ljava/lang/String;

.field private static final COUNT_INCREMENT_FOR_RESCALE:I = 0x5

.field private static ImplClass:Ljava/lang/Class; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final MRU_MAX_SIZE:I = 0x32

.field private static final RESCALE_FACTOR:F = 0.98f

.field private static final TAG:Ljava/lang/String;

.field private static m_instance:Lcom/vlingo/core/internal/contacts/mru/MRU;


# instance fields
.field private m_store:Lcom/vlingo/core/internal/contacts/mru/MRUStore;

.field m_tables:[Lcom/vlingo/core/internal/contacts/mru/MRUTable;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 28
    const-class v0, Lcom/vlingo/core/internal/contacts/mru/MRU;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/core/internal/contacts/mru/MRU;->TAG:Ljava/lang/String;

    .line 32
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "sms"

    aput-object v1, v0, v3

    const-string/jumbo v1, "call"

    aput-object v1, v0, v4

    const-string/jumbo v1, "email"

    aput-object v1, v0, v5

    const-string/jumbo v1, "message"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string/jumbo v2, "facebook"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/core/internal/contacts/mru/MRU;->APPLICATION_NAMES:[Ljava/lang/String;

    .line 43
    new-array v0, v6, [Ljava/lang/String;

    const-string/jumbo v1, "contactID"

    aput-object v1, v0, v3

    const-string/jumbo v1, "address"

    aput-object v1, v0, v4

    const-string/jumbo v1, "count"

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/core/internal/contacts/mru/MRU;->COLUMNS:[Ljava/lang/String;

    .line 48
    sput-object v7, Lcom/vlingo/core/internal/contacts/mru/MRU;->m_instance:Lcom/vlingo/core/internal/contacts/mru/MRU;

    .line 51
    sput-object v7, Lcom/vlingo/core/internal/contacts/mru/MRU;->ImplClass:Ljava/lang/Class;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    invoke-direct {p0}, Lcom/vlingo/core/internal/contacts/mru/MRU;->createMRUStore()Lcom/vlingo/core/internal/contacts/mru/MRUStore;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/mru/MRU;->m_store:Lcom/vlingo/core/internal/contacts/mru/MRUStore;

    .line 70
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/mru/MRU;->m_store:Lcom/vlingo/core/internal/contacts/mru/MRUStore;

    invoke-interface {v0}, Lcom/vlingo/core/internal/contacts/mru/MRUStore;->loadMRUTables()[Lcom/vlingo/core/internal/contacts/mru/MRUTable;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/core/internal/contacts/mru/MRU;->m_tables:[Lcom/vlingo/core/internal/contacts/mru/MRUTable;

    .line 71
    return-void
.end method

.method private createMRUStore()Lcom/vlingo/core/internal/contacts/mru/MRUStore;
    .locals 5

    .prologue
    .line 74
    sget-object v2, Lcom/vlingo/core/internal/contacts/mru/MRU;->ImplClass:Ljava/lang/Class;

    if-nez v2, :cond_0

    .line 75
    new-instance v2, Ljava/lang/RuntimeException;

    const-string/jumbo v3, "MRUStore implementation class is not set"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 77
    :cond_0
    :try_start_0
    sget-object v2, Lcom/vlingo/core/internal/contacts/mru/MRU;->ImplClass:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/mru/MRUStore;

    .line 78
    .local v1, "store":Lcom/vlingo/core/internal/contacts/mru/MRUStore;
    sget-object v2, Lcom/vlingo/core/internal/contacts/mru/MRU;->APPLICATION_NAMES:[Ljava/lang/String;

    sget-object v3, Lcom/vlingo/core/internal/contacts/mru/MRU;->COLUMNS:[Ljava/lang/String;

    const/16 v4, 0x32

    invoke-interface {v1, v2, v3, v4}, Lcom/vlingo/core/internal/contacts/mru/MRUStore;->init([Ljava/lang/String;[Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 79
    return-object v1

    .line 80
    .end local v1    # "store":Lcom/vlingo/core/internal/contacts/mru/MRUStore;
    :catch_0
    move-exception v0

    .line 83
    .local v0, "ex":Ljava/lang/InstantiationException;
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "MRUStore InstantiationException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 84
    .end local v0    # "ex":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v0

    .line 87
    .local v0, "ex":Ljava/lang/IllegalAccessException;
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "MRUStore IllegalAccessException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public static declared-synchronized getMRU()Lcom/vlingo/core/internal/contacts/mru/MRU;
    .locals 2

    .prologue
    .line 62
    const-class v1, Lcom/vlingo/core/internal/contacts/mru/MRU;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/contacts/mru/MRU;->m_instance:Lcom/vlingo/core/internal/contacts/mru/MRU;

    if-nez v0, :cond_0

    .line 63
    new-instance v0, Lcom/vlingo/core/internal/contacts/mru/MRU;

    invoke-direct {v0}, Lcom/vlingo/core/internal/contacts/mru/MRU;-><init>()V

    sput-object v0, Lcom/vlingo/core/internal/contacts/mru/MRU;->m_instance:Lcom/vlingo/core/internal/contacts/mru/MRU;

    .line 65
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/contacts/mru/MRU;->m_instance:Lcom/vlingo/core/internal/contacts/mru/MRU;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getMRUTable(Lcom/vlingo/core/internal/contacts/ContactType;)Lcom/vlingo/core/internal/contacts/mru/MRUTable;
    .locals 3
    .param p1, "type"    # Lcom/vlingo/core/internal/contacts/ContactType;

    .prologue
    .line 172
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/vlingo/core/internal/contacts/mru/MRU;->APPLICATION_NAMES:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 173
    sget-object v1, Lcom/vlingo/core/internal/contacts/mru/MRU;->APPLICATION_NAMES:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 174
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/mru/MRU;->m_tables:[Lcom/vlingo/core/internal/contacts/mru/MRUTable;

    aget-object v1, v1, v0

    .line 177
    :goto_1
    return-object v1

    .line 172
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 177
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private getMRUTable(Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/mru/MRUTable;
    .locals 2
    .param p1, "applicationName"    # Ljava/lang/String;

    .prologue
    .line 163
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/vlingo/core/internal/contacts/mru/MRU;->APPLICATION_NAMES:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 164
    sget-object v1, Lcom/vlingo/core/internal/contacts/mru/MRU;->APPLICATION_NAMES:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 165
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/mru/MRU;->m_tables:[Lcom/vlingo/core/internal/contacts/mru/MRUTable;

    aget-object v1, v1, v0

    .line 168
    :goto_1
    return-object v1

    .line 163
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 168
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private pruneTable(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/mru/MRUTable;)V
    .locals 3
    .param p1, "applicationName"    # Ljava/lang/String;
    .param p2, "table"    # Lcom/vlingo/core/internal/contacts/mru/MRUTable;

    .prologue
    .line 147
    :cond_0
    :goto_0
    invoke-virtual {p2}, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->getNumItems()I

    move-result v1

    const/16 v2, 0x32

    if-le v1, v2, :cond_1

    .line 148
    invoke-virtual {p2}, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->removeLowestScoringContact()Ljava/lang/Integer;

    move-result-object v0

    .line 149
    .local v0, "contactToRemove":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 150
    iget-object v1, p0, Lcom/vlingo/core/internal/contacts/mru/MRU;->m_store:Lcom/vlingo/core/internal/contacts/mru/MRUStore;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, p1, v2}, Lcom/vlingo/core/internal/contacts/mru/MRUStore;->removeEntry(Ljava/lang/String;I)V

    goto :goto_0

    .line 153
    .end local v0    # "contactToRemove":Ljava/lang/Integer;
    :cond_1
    return-void
.end method

.method private rescaleCounts(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/mru/MRUTable;)V
    .locals 2
    .param p1, "applicationName"    # Ljava/lang/String;
    .param p2, "table"    # Lcom/vlingo/core/internal/contacts/mru/MRUTable;

    .prologue
    const v1, 0x3f7ae148    # 0.98f

    .line 157
    iget-object v0, p0, Lcom/vlingo/core/internal/contacts/mru/MRU;->m_store:Lcom/vlingo/core/internal/contacts/mru/MRUStore;

    invoke-interface {v0, p1, v1}, Lcom/vlingo/core/internal/contacts/mru/MRUStore;->rescaleAllCounts(Ljava/lang/String;F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    invoke-virtual {p2, v1}, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->scaleValues(F)V

    .line 160
    :cond_0
    return-void
.end method

.method public static declared-synchronized setMRUStoreImpl(Ljava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 54
    .local p0, "implClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v1, Lcom/vlingo/core/internal/contacts/mru/MRU;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 55
    :try_start_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "MRUStore class null"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 56
    :cond_0
    :try_start_1
    const-class v0, Lcom/vlingo/core/internal/contacts/mru/MRUStore;

    invoke-virtual {v0, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 57
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "MRUStore invalid impl: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_1
    sput-object p0, Lcom/vlingo/core/internal/contacts/mru/MRU;->ImplClass:Ljava/lang/Class;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 59
    monitor-exit v1

    return-void
.end method


# virtual methods
.method public getNormalizedCount(Lcom/vlingo/core/internal/contacts/ContactType;I)F
    .locals 4
    .param p1, "type"    # Lcom/vlingo/core/internal/contacts/ContactType;
    .param p2, "contactID"    # I

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/contacts/mru/MRU;->getMRUTable(Lcom/vlingo/core/internal/contacts/ContactType;)Lcom/vlingo/core/internal/contacts/mru/MRUTable;

    move-result-object v0

    .line 112
    .local v0, "table":Lcom/vlingo/core/internal/contacts/mru/MRUTable;
    if-nez v0, :cond_0

    .line 113
    sget-object v1, Lcom/vlingo/core/internal/contacts/mru/MRU;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "asking for MRU for missing app "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " returning 0 "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    const/4 v1, 0x0

    .line 117
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0, p2}, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->getNormalizedCount(I)F

    move-result v1

    goto :goto_0
.end method

.method public getNormalizedCount(Lcom/vlingo/core/internal/contacts/ContactType;ILjava/lang/String;)F
    .locals 4
    .param p1, "type"    # Lcom/vlingo/core/internal/contacts/ContactType;
    .param p2, "contactID"    # I
    .param p3, "address"    # Ljava/lang/String;

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/contacts/mru/MRU;->getMRUTable(Lcom/vlingo/core/internal/contacts/ContactType;)Lcom/vlingo/core/internal/contacts/mru/MRUTable;

    move-result-object v0

    .line 102
    .local v0, "table":Lcom/vlingo/core/internal/contacts/mru/MRUTable;
    if-nez v0, :cond_0

    .line 103
    sget-object v1, Lcom/vlingo/core/internal/contacts/mru/MRU;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "asking for MRU for missing app "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " returning 0 "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    const/4 v1, 0x0

    .line 106
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0, p2, p3}, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->getNormalizedCount(ILjava/lang/String;)F

    move-result v1

    goto :goto_0
.end method

.method public incrementCount(Ljava/lang/String;ILjava/lang/String;)F
    .locals 5
    .param p1, "applicationName"    # Ljava/lang/String;
    .param p2, "contactID"    # I
    .param p3, "address"    # Ljava/lang/String;

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/contacts/mru/MRU;->getMRUTable(Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/mru/MRUTable;

    move-result-object v1

    .line 124
    .local v1, "table":Lcom/vlingo/core/internal/contacts/mru/MRUTable;
    if-nez v1, :cond_1

    .line 125
    sget-object v2, Lcom/vlingo/core/internal/contacts/mru/MRU;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "incrementing count for unknown application "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " skipping"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    const/4 v0, 0x0

    .line 141
    :cond_0
    :goto_0
    return v0

    .line 129
    :cond_1
    invoke-virtual {v1, p2, p3}, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->incrementCount(ILjava/lang/String;)F

    move-result v0

    .line 130
    .local v0, "newCount":F
    iget-object v2, p0, Lcom/vlingo/core/internal/contacts/mru/MRU;->m_store:Lcom/vlingo/core/internal/contacts/mru/MRUStore;

    invoke-interface {v2, p1, p2, p3, v0}, Lcom/vlingo/core/internal/contacts/mru/MRUStore;->setEntryCount(Ljava/lang/String;ILjava/lang/String;F)V

    .line 132
    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->getCountSinceLastUdate()I

    move-result v2

    const/4 v3, 0x5

    if-le v2, v3, :cond_2

    .line 133
    invoke-direct {p0, p1, v1}, Lcom/vlingo/core/internal/contacts/mru/MRU;->rescaleCounts(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/mru/MRUTable;)V

    .line 137
    :cond_2
    invoke-virtual {v1}, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->getNumItems()I

    move-result v2

    const/16 v3, 0x32

    if-le v2, v3, :cond_0

    .line 138
    invoke-direct {p0, p1, v1}, Lcom/vlingo/core/internal/contacts/mru/MRU;->pruneTable(Ljava/lang/String;Lcom/vlingo/core/internal/contacts/mru/MRUTable;)V

    goto :goto_0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "applicationName"    # Ljava/lang/String;

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/contacts/mru/MRU;->getMRUTable(Ljava/lang/String;)Lcom/vlingo/core/internal/contacts/mru/MRUTable;

    move-result-object v0

    .line 93
    .local v0, "table":Lcom/vlingo/core/internal/contacts/mru/MRUTable;
    if-nez v0, :cond_0

    .line 94
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No MRU Table for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 96
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "MRU for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/mru/MRUTable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
