.class public Lcom/vlingo/core/internal/safereader/SafeReaderProxy;
.super Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;
.source "SafeReaderProxy.java"


# static fields
.field private static final REGISTER_MSG_LISTENER:I = 0x1b

.field private static final SAFEREADER_DEINIT:I = 0x1e

.field private static final SAFEREADER_GET_LAST_ALERT:I = 0x1a

.field private static final SAFEREADER_INIT:I = 0x1d

.field private static final SAFEREADER_STATUS:I = 0x19

.field private static final SAFEREADING_START:I = 0x17

.field private static final SAFEREADING_STOP:I = 0x18

.field private static final UNREGISTER_MSG_LISTENER:I = 0x1c

.field private static instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;


# instance fields
.field private safeReaderListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/safereader/SafeReaderServiceProxyBase;-><init>(Landroid/content/Context;)V

    .line 31
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->safeReaderListeners:Ljava/util/List;

    .line 100
    return-void
.end method

.method public static deinit()V
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    if-eqz v0, :cond_0

    .line 41
    sget-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->release()V

    .line 42
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    .line 44
    :cond_0
    return-void
.end method

.method public static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    if-eqz v0, :cond_0

    .line 48
    sget-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    iget-object v0, v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->context:Landroid/content/Context;

    .line 50
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    sget-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    .line 37
    :cond_0
    return-void
.end method

.method private declared-synchronized registerListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 123
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->safeReaderListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->safeReaderListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    :cond_0
    const/16 v0, 0x1b

    invoke-virtual {p0, v0, p1}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->execute(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    monitor-exit p0

    return-void

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static registerSafeReaderListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p0, "listener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 85
    sget-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    if-eqz v0, :cond_0

    .line 86
    sget-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->registerListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 88
    :cond_0
    return-void
.end method

.method private declared-synchronized safeReaderDeinit()V
    .locals 1

    .prologue
    .line 119
    monitor-enter p0

    const/16 v0, 0x1e

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->execute(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    monitor-exit p0

    return-void

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized safeReaderInit(Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;)V
    .locals 1
    .param p1, "engine"    # Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    .prologue
    .line 111
    monitor-enter p0

    const/16 v0, 0x1d

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->execute(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    monitor-exit p0

    return-void

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static safeReadingDeinit()V
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    if-eqz v0, :cond_0

    .line 74
    sget-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    invoke-direct {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->safeReaderDeinit()V

    .line 76
    :cond_0
    return-void
.end method

.method public static safeReadingInit(Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;)V
    .locals 1
    .param p0, "engine"    # Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    .prologue
    .line 61
    sget-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    if-eqz v0, :cond_0

    .line 62
    sget-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->safeReaderInit(Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;)V

    .line 64
    :cond_0
    return-void
.end method

.method private declared-synchronized startSafeReader()V
    .locals 1

    .prologue
    .line 107
    monitor-enter p0

    const/16 v0, 0x17

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->execute(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    monitor-exit p0

    return-void

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static startSafeReading()V
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    if-eqz v0, :cond_0

    .line 56
    sget-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    invoke-direct {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->startSafeReader()V

    .line 58
    :cond_0
    return-void
.end method

.method private declared-synchronized stopSafeReader()V
    .locals 1

    .prologue
    .line 115
    monitor-enter p0

    const/16 v0, 0x18

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->execute(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    monitor-exit p0

    return-void

    .line 115
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static stopSafeReading()V
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    if-eqz v0, :cond_0

    .line 68
    sget-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    invoke-direct {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->stopSafeReader()V

    .line 70
    :cond_0
    return-void
.end method

.method private declared-synchronized unregisterListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 130
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->safeReaderListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 131
    const/16 v0, 0x1c

    invoke-virtual {p0, v0, p1}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->execute(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    monitor-exit p0

    return-void

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static unregisterSafeReaderListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p0, "listener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 91
    sget-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    if-eqz v0, :cond_0

    .line 92
    sget-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->unregisterListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 94
    :cond_0
    return-void
.end method

.method private declared-synchronized updateSafeReaderStatus()V
    .locals 1

    .prologue
    .line 103
    monitor-enter p0

    const/16 v0, 0x19

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->execute(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    monitor-exit p0

    return-void

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static updateStatus()V
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    if-eqz v0, :cond_0

    .line 80
    sget-object v0, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->instance:Lcom/vlingo/core/internal/safereader/SafeReaderProxy;

    invoke-direct {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->updateSafeReaderStatus()V

    .line 82
    :cond_0
    return-void
.end method


# virtual methods
.method protected allowsDelayDisconnect()Z
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x0

    return v0
.end method

.method protected getMessageName(Landroid/os/Message;)Ljava/lang/String;
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 137
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 153
    :pswitch_0
    const-string/jumbo v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 139
    :pswitch_1
    const-string/jumbo v0, "SAFEREADER_ON"

    goto :goto_0

    .line 141
    :pswitch_2
    const-string/jumbo v0, "SAFEREADER_OFF"

    goto :goto_0

    .line 143
    :pswitch_3
    const-string/jumbo v0, "SAFEREADER_STATUS"

    goto :goto_0

    .line 145
    :pswitch_4
    const-string/jumbo v0, "REGISTER_MSG_LISTENER"

    goto :goto_0

    .line 147
    :pswitch_5
    const-string/jumbo v0, "UNREGISTER_MSG_LISTENER"

    goto :goto_0

    .line 149
    :pswitch_6
    const-string/jumbo v0, "SAFEREADER_INIT"

    goto :goto_0

    .line 151
    :pswitch_7
    const-string/jumbo v0, "SAFEREADER_DEINIT"

    goto :goto_0

    .line 137
    nop

    :pswitch_data_0
    .packed-switch 0x17
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method protected handleMessageForService(Landroid/os/Message;Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;
    .param p2, "safeReaderServiceEngine"    # Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    .prologue
    .line 158
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 185
    :goto_0
    :sswitch_0
    return-void

    .line 162
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    invoke-interface {p2, v0}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->registerAlertHandler(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    goto :goto_0

    .line 165
    :sswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    invoke-interface {p2, v0}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->unregisterAlertHandler(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    goto :goto_0

    .line 168
    :sswitch_3
    invoke-interface {p2}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->stopSafeReading()V

    goto :goto_0

    .line 171
    :sswitch_4
    invoke-interface {p2}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->startSafeReading()V

    goto :goto_0

    .line 174
    :sswitch_5
    invoke-interface {p2}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->broadcastStatusUpdate()V

    goto :goto_0

    .line 179
    :sswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;

    invoke-interface {p2, v0}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->safeReaderInit(Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;)V

    goto :goto_0

    .line 182
    :sswitch_7
    invoke-interface {p2}, Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;->safeReaderDeinit()V

    goto :goto_0

    .line 158
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x17 -> :sswitch_4
        0x18 -> :sswitch_3
        0x19 -> :sswitch_5
        0x1a -> :sswitch_0
        0x1b -> :sswitch_1
        0x1c -> :sswitch_2
        0x1d -> :sswitch_6
        0x1e -> :sswitch_7
    .end sparse-switch
.end method
