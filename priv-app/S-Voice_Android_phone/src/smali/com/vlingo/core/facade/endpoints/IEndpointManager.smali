.class public interface abstract Lcom/vlingo/core/facade/endpoints/IEndpointManager;
.super Ljava/lang/Object;
.source "IEndpointManager.java"


# virtual methods
.method public abstract getFieldId(Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
.end method

.method public abstract getFieldId(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
.end method

.method public abstract setFieldIdSilenceDuration(Lcom/vlingo/core/facade/endpoints/FieldIdsInterface;I)I
.end method

.method public abstract setSilenceDurationNoSpeech(I)I
.end method

.method public abstract setSilenceDurationWithSpeech(Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;I)I
.end method
