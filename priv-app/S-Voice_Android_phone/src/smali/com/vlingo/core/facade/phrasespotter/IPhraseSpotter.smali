.class public interface abstract Lcom/vlingo/core/facade/phrasespotter/IPhraseSpotter;
.super Ljava/lang/Object;
.source "IPhraseSpotter.java"


# virtual methods
.method public abstract init(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V
.end method

.method public abstract isListening()Z
.end method

.method public abstract setRecognitionContext(Lcom/vlingo/sdk/recognition/VLRecognitionContext;)V
.end method

.method public abstract startPhraseSpotting()V
.end method

.method public abstract stopPhraseSpotting()V
.end method

.method public abstract stopPhraseSpottingRightNow()V
.end method

.method public abstract updateParameters(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;)V
.end method
