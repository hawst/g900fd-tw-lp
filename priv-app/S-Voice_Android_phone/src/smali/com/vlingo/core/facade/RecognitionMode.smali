.class public final enum Lcom/vlingo/core/facade/RecognitionMode;
.super Ljava/lang/Enum;
.source "RecognitionMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/core/facade/RecognitionMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/core/facade/RecognitionMode;

.field public static final enum CLOUD:Lcom/vlingo/core/facade/RecognitionMode;

.field public static final enum EMBEDDED:Lcom/vlingo/core/facade/RecognitionMode;

.field public static final enum HYBRID:Lcom/vlingo/core/facade/RecognitionMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11
    new-instance v0, Lcom/vlingo/core/facade/RecognitionMode;

    const-string/jumbo v1, "CLOUD"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/facade/RecognitionMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/facade/RecognitionMode;->CLOUD:Lcom/vlingo/core/facade/RecognitionMode;

    .line 15
    new-instance v0, Lcom/vlingo/core/facade/RecognitionMode;

    const-string/jumbo v1, "EMBEDDED"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/core/facade/RecognitionMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/facade/RecognitionMode;->EMBEDDED:Lcom/vlingo/core/facade/RecognitionMode;

    .line 21
    new-instance v0, Lcom/vlingo/core/facade/RecognitionMode;

    const-string/jumbo v1, "HYBRID"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/core/facade/RecognitionMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/core/facade/RecognitionMode;->HYBRID:Lcom/vlingo/core/facade/RecognitionMode;

    .line 7
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vlingo/core/facade/RecognitionMode;

    sget-object v1, Lcom/vlingo/core/facade/RecognitionMode;->CLOUD:Lcom/vlingo/core/facade/RecognitionMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/core/facade/RecognitionMode;->EMBEDDED:Lcom/vlingo/core/facade/RecognitionMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/core/facade/RecognitionMode;->HYBRID:Lcom/vlingo/core/facade/RecognitionMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/core/facade/RecognitionMode;->$VALUES:[Lcom/vlingo/core/facade/RecognitionMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/core/facade/RecognitionMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/vlingo/core/facade/RecognitionMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/facade/RecognitionMode;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/core/facade/RecognitionMode;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/vlingo/core/facade/RecognitionMode;->$VALUES:[Lcom/vlingo/core/facade/RecognitionMode;

    invoke-virtual {v0}, [Lcom/vlingo/core/facade/RecognitionMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/core/facade/RecognitionMode;

    return-object v0
.end method
