.class public Lcom/vlingo/core/facade/lmtt/LmttReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LmttReceiver.java"


# static fields
.field public static final ACTION_HEART_BEAT:Ljava/lang/String; = "com.vlingo.lmtt.action.CONNECTED_APP_HEART_BIT"

.field public static final ACTION_SLAVE_EVENT:Ljava/lang/String; = "com.vlingo.mdso.action.SLAVE_EVENT"

.field public static final EXTRA_APP_LAUNCHED:Ljava/lang/String; = "com.vlingo.lmtt.extra.APP_LAUNCHED"

.field public static final EXTRA_EVENT_NAME:Ljava/lang/String; = "com.vlingo.mdso.extra.EVENT_NAME"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 20
    if-nez p2, :cond_1

    .line 42
    :cond_0
    :goto_0
    return-void

    .line 23
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 24
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v3, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 25
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->getInstance()Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->onBootCompleted()V

    .line 26
    new-instance v3, Lcom/vlingo/core/internal/lmttvocon/VoconLmttServiceBootStart;

    invoke-direct {v3}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttServiceBootStart;-><init>()V

    invoke-virtual {v3, p1, p2}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttServiceBootStart;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 27
    :cond_2
    const-string/jumbo v3, "com.vlingo.lmtt.action.CONNECTED_APP_HEART_BIT"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 30
    const-string/jumbo v3, "com.vlingo.lmtt.extra.APP_LAUNCHED"

    const/4 v4, 0x0

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 31
    .local v2, "isAppLaunched":Z
    if-eqz v2, :cond_3

    .line 32
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->getInstance()Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;

    move-result-object v3

    const-string/jumbo v4, "app_launched"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->onSlaveEvent(Ljava/lang/String;)V

    goto :goto_0

    .line 34
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->getInstance()Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;

    move-result-object v3

    const-string/jumbo v4, "user_utt"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->onSlaveEvent(Ljava/lang/String;)V

    goto :goto_0

    .line 36
    .end local v2    # "isAppLaunched":Z
    :cond_4
    const-string/jumbo v3, "com.vlingo.mdso.action.SLAVE_EVENT"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 38
    const-string/jumbo v3, "com.vlingo.mdso.extra.EVENT_NAME"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 39
    .local v1, "eventName":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->getInstance()Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/vlingo/core/internal/lmtt/event/LmttEventHandler;->onSlaveEvent(Ljava/lang/String;)V

    goto :goto_0
.end method
