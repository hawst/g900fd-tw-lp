.class public interface abstract Lcom/vlingo/core/facade/IClientInfo;
.super Ljava/lang/Object;
.source "IClientInfo.java"


# virtual methods
.method public abstract getAppId()Ljava/lang/String;
.end method

.method public abstract getAppName()Ljava/lang/String;
.end method

.method public abstract getAppVersion()Ljava/lang/String;
.end method

.method public abstract getRecognitionMode()Lcom/vlingo/core/facade/RecognitionMode;
.end method

.method public abstract getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;
.end method

.method public abstract getSalesCode()Ljava/lang/String;
.end method

.method public abstract getServerDetails()Lcom/vlingo/core/internal/util/CoreServerInfo;
.end method

.method public abstract getVlingoApp()Lcom/vlingo/core/internal/util/VlingoApplicationInterface;
.end method

.method public abstract isUseEmbeddedDialogManager()Z
.end method
