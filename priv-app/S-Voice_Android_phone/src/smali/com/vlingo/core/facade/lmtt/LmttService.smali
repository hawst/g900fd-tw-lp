.class public Lcom/vlingo/core/facade/lmtt/LmttService;
.super Landroid/app/Service;
.source "LmttService.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 41
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 22
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;->getInstance()Lcom/vlingo/core/internal/lmtt/LmttServiceBase;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;->onCreate(Landroid/app/Service;)V

    .line 23
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;->getInstance()Lcom/vlingo/core/internal/lmtt/LmttServiceBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;->onDestroy()V

    .line 37
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 29
    invoke-static {}, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;->getInstance()Lcom/vlingo/core/internal/lmtt/LmttServiceBase;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/vlingo/core/internal/lmtt/LmttServiceBase;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method
