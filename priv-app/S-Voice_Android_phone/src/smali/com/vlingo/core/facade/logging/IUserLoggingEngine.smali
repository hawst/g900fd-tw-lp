.class public interface abstract Lcom/vlingo/core/facade/logging/IUserLoggingEngine;
.super Ljava/lang/Object;
.source "IUserLoggingEngine.java"


# virtual methods
.method public abstract errorDisplayed(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract landingPageAction(Ljava/lang/String;Ljava/util/List;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;",
            ">;Z)V"
        }
    .end annotation
.end method

.method public abstract landingPageViewed(Ljava/lang/String;)V
.end method
