.class public interface abstract Lcom/vlingo/core/facade/tts/ITTSEngine;
.super Ljava/lang/Object;
.source "ITTSEngine.java"


# virtual methods
.method public abstract destroy()V
.end method

.method public abstract getFilePathForTTSRequest(Landroid/content/Context;Lcom/vlingo/core/internal/audio/TTSRequest;)Ljava/lang/String;
.end method

.method public abstract getTTS()Landroid/speech/tts/TextToSpeech;
.end method

.method public abstract onApplicationLanguageChanged()V
.end method

.method public abstract prepare()V
.end method

.method public abstract setTtsSpeechRate(F)V
.end method
