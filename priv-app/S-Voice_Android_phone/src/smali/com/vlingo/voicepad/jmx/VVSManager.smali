.class public Lcom/vlingo/voicepad/jmx/VVSManager;
.super Ljava/lang/Object;
.source "VVSManager.java"


# static fields
.field private static ivManager:Lcom/vlingo/voicepad/jmx/VVSManager;


# instance fields
.field private volatile ivHelloStatus:Lcom/vlingo/voicepad/jmx/VVSStatus;

.field private volatile ivSRStatus:Lcom/vlingo/voicepad/jmx/VVSStatus;

.field private volatile ivTagToActionConfigLoaded:Z

.field private ivWorkflowHelper:Lcom/bzbyte/event/PropertyChangeHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/vlingo/voicepad/jmx/VVSManager;

    invoke-direct {v0}, Lcom/vlingo/voicepad/jmx/VVSManager;-><init>()V

    sput-object v0, Lcom/vlingo/voicepad/jmx/VVSManager;->ivManager:Lcom/vlingo/voicepad/jmx/VVSManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lcom/bzbyte/event/PropertyChangeHelper;

    invoke-direct {v0}, Lcom/bzbyte/event/PropertyChangeHelper;-><init>()V

    iput-object v0, p0, Lcom/vlingo/voicepad/jmx/VVSManager;->ivWorkflowHelper:Lcom/bzbyte/event/PropertyChangeHelper;

    .line 24
    sget-object v0, Lcom/vlingo/voicepad/jmx/VVSStatus;->STARTING:Lcom/vlingo/voicepad/jmx/VVSStatus;

    iput-object v0, p0, Lcom/vlingo/voicepad/jmx/VVSManager;->ivSRStatus:Lcom/vlingo/voicepad/jmx/VVSStatus;

    .line 25
    sget-object v0, Lcom/vlingo/voicepad/jmx/VVSStatus;->STARTING:Lcom/vlingo/voicepad/jmx/VVSStatus;

    iput-object v0, p0, Lcom/vlingo/voicepad/jmx/VVSManager;->ivHelloStatus:Lcom/vlingo/voicepad/jmx/VVSStatus;

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/voicepad/jmx/VVSManager;->ivTagToActionConfigLoaded:Z

    .line 30
    return-void
.end method

.method public static getInstance()Lcom/vlingo/voicepad/jmx/VVSManager;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/vlingo/voicepad/jmx/VVSManager;->ivManager:Lcom/vlingo/voicepad/jmx/VVSManager;

    return-object v0
.end method


# virtual methods
.method public addPropertyChangeListener(Lcom/bzbyte/event/PropertyChangeListener;)V
    .locals 1
    .param p1, "l"    # Lcom/bzbyte/event/PropertyChangeListener;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/vlingo/voicepad/jmx/VVSManager;->ivWorkflowHelper:Lcom/bzbyte/event/PropertyChangeHelper;

    invoke-virtual {v0, p1}, Lcom/bzbyte/event/PropertyChangeHelper;->addPropertyChangeListener(Lcom/bzbyte/event/PropertyChangeListener;)V

    .line 71
    return-void
.end method

.method public removePropertyChangeListener(Lcom/bzbyte/event/PropertyChangeListener;)V
    .locals 1
    .param p1, "l"    # Lcom/bzbyte/event/PropertyChangeListener;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/vlingo/voicepad/jmx/VVSManager;->ivWorkflowHelper:Lcom/bzbyte/event/PropertyChangeHelper;

    invoke-virtual {v0, p1}, Lcom/bzbyte/event/PropertyChangeHelper;->removePropertyChangeListener(Lcom/bzbyte/event/PropertyChangeListener;)V

    .line 76
    return-void
.end method
