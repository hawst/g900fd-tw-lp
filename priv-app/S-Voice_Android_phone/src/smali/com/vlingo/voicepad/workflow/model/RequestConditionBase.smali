.class public Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;
.super Ljava/lang/Object;
.source "RequestConditionBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_AppChannel:Ljava/lang/String; = "AppChannel"

.field public static final PROP_AppID:Ljava/lang/String; = "AppID"

.field public static final PROP_AutoTag:Ljava/lang/String; = "AutoTag"

.field public static final PROP_DeviceCarrier:Ljava/lang/String; = "DeviceCarrier"

.field public static final PROP_DeviceCarrierCountry:Ljava/lang/String; = "DeviceCarrierCountry"

.field public static final PROP_DeviceID:Ljava/lang/String; = "DeviceID"

.field public static final PROP_DeviceMake:Ljava/lang/String; = "DeviceMake"

.field public static final PROP_DeviceModel:Ljava/lang/String; = "DeviceModel"

.field public static final PROP_DeviceOS:Ljava/lang/String; = "DeviceOS"

.field public static final PROP_DeviceOSName:Ljava/lang/String; = "DeviceOSName"

.field public static final PROP_FeatureLevel:Ljava/lang/String; = "FeatureLevel"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Language:Ljava/lang/String; = "Language"

.field public static final PROP_LicenseClass:Ljava/lang/String; = "LicenseClass"

.field public static final PROP_MinSoftwareVersion:Ljava/lang/String; = "MinSoftwareVersion"

.field public static final PROP_MinVVSVersion:Ljava/lang/String; = "MinVVSVersion"

.field public static final PROP_ServletName:Ljava/lang/String; = "ServletName"

.field public static final PROP_SoftwareName:Ljava/lang/String; = "SoftwareName"


# instance fields
.field private AppChannel:Ljava/lang/String;

.field private AppID:Ljava/lang/String;

.field private AutoTag:Ljava/lang/Boolean;

.field private DeviceCarrier:Ljava/lang/String;

.field private DeviceCarrierCountry:Ljava/lang/String;

.field private DeviceID:Ljava/lang/String;

.field private DeviceMake:Ljava/lang/String;

.field private DeviceModel:Ljava/lang/String;

.field private DeviceOS:Ljava/lang/String;

.field private DeviceOSName:Ljava/lang/String;

.field private FeatureLevel:Ljava/lang/Integer;

.field private ID:J

.field private Language:Ljava/lang/String;

.field private LicenseClass:Ljava/lang/String;

.field private MinSoftwareVersion:Ljava/lang/String;

.field private MinVVSVersion:Ljava/lang/String;

.field private ServletName:Ljava/lang/String;

.field private SoftwareName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 172
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/voicepad/workflow/model/RequestCondition;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAppChannel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->AppChannel:Ljava/lang/String;

    return-object v0
.end method

.method public getAppID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->AppID:Ljava/lang/String;

    return-object v0
.end method

.method public getAutoTag()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->AutoTag:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 176
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/voicepad/workflow/model/RequestCondition;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceCarrier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->DeviceCarrier:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceCarrierCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->DeviceCarrierCountry:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->DeviceID:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceMake()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->DeviceMake:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->DeviceModel:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceOS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->DeviceOS:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceOSName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->DeviceOSName:Ljava/lang/String;

    return-object v0
.end method

.method public getFeatureLevel()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->FeatureLevel:Ljava/lang/Integer;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 164
    iget-wide v0, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->ID:J

    return-wide v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->Language:Ljava/lang/String;

    return-object v0
.end method

.method public getLicenseClass()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->LicenseClass:Ljava/lang/String;

    return-object v0
.end method

.method public getMinSoftwareVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->MinSoftwareVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getMinVVSVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->MinVVSVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getServletName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->ServletName:Ljava/lang/String;

    return-object v0
.end method

.method public getSoftwareName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->SoftwareName:Ljava/lang/String;

    return-object v0
.end method

.method public setAppChannel(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->AppChannel:Ljava/lang/String;

    .line 77
    return-void
.end method

.method public setAppID(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->AppID:Ljava/lang/String;

    .line 140
    return-void
.end method

.method public setAutoTag(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Boolean;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->AutoTag:Ljava/lang/Boolean;

    .line 161
    return-void
.end method

.method public setDeviceCarrier(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->DeviceCarrier:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public setDeviceCarrierCountry(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->DeviceCarrierCountry:Ljava/lang/String;

    .line 91
    return-void
.end method

.method public setDeviceID(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->DeviceID:Ljava/lang/String;

    .line 133
    return-void
.end method

.method public setDeviceMake(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->DeviceMake:Ljava/lang/String;

    .line 105
    return-void
.end method

.method public setDeviceModel(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->DeviceModel:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public setDeviceOS(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->DeviceOS:Ljava/lang/String;

    .line 126
    return-void
.end method

.method public setDeviceOSName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->DeviceOSName:Ljava/lang/String;

    .line 119
    return-void
.end method

.method public setFeatureLevel(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Integer;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->FeatureLevel:Ljava/lang/Integer;

    .line 154
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 167
    iput-wide p1, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->ID:J

    .line 168
    return-void
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->Language:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public setLicenseClass(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->LicenseClass:Ljava/lang/String;

    .line 147
    return-void
.end method

.method public setMinSoftwareVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->MinSoftwareVersion:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public setMinVVSVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->MinVVSVersion:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public setServletName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->ServletName:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public setSoftwareName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/RequestConditionBase;->SoftwareName:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
