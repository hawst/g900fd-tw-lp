.class public Lcom/vlingo/voicepad/workflow/model/Workflow;
.super Lcom/vlingo/voicepad/workflow/model/WorkflowBase;
.source "Workflow.java"


# static fields
.field public static final PARAM_IgnoreContentCookies:Ljava/lang/String; = "IgnoreContentCookies"

.field public static final PARAM_InlineContent:Ljava/lang/String; = "InlineContent"

.field public static final PARAM_ShowUserErrors:Ljava/lang/String; = "ShowUserErrors"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/vlingo/voicepad/workflow/model/WorkflowBase;-><init>()V

    return-void
.end method

.method public static getTaskParamBoolean(Lcom/vlingo/voicepad/workflow/model/Workflow;Ljava/lang/String;Z)Z
    .locals 1
    .param p0, "workflow"    # Lcom/vlingo/voicepad/workflow/model/Workflow;
    .param p1, "paramName"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Z

    .prologue
    .line 30
    if-eqz p0, :cond_0

    .line 32
    invoke-virtual {p0}, Lcom/vlingo/voicepad/workflow/model/Workflow;->getParams()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/vlingo/voicepad/workflow/model/Workflow;->getTaskParamBoolean(Ljava/util/Set;Ljava/lang/String;Z)Z

    move-result p2

    .line 34
    .end local p2    # "defaultValue":Z
    :cond_0
    return p2
.end method

.method public static getTaskParamBoolean(Ljava/util/Set;Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "paramName"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/workflow/model/TaskParam;",
            ">;",
            "Ljava/lang/String;",
            "Z)Z"
        }
    .end annotation

    .prologue
    .line 39
    .local p0, "params":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/workflow/model/TaskParam;>;"
    if-eqz p0, :cond_1

    .line 41
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/workflow/model/TaskParam;

    .line 43
    .local v1, "param":Lcom/vlingo/workflow/model/TaskParam;
    invoke-virtual {v1}, Lcom/vlingo/workflow/model/TaskParam;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 45
    invoke-virtual {v1}, Lcom/vlingo/workflow/model/TaskParam;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    .line 49
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "param":Lcom/vlingo/workflow/model/TaskParam;
    .end local p2    # "defaultValue":Z
    :cond_1
    return p2
.end method
