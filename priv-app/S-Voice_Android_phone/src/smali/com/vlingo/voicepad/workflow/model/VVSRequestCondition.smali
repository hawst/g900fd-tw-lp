.class public Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;
.super Lcom/vlingo/voicepad/workflow/model/VVSRequestConditionBase;
.source "VVSRequestCondition.java"


# static fields
.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/vlingo/voicepad/workflow/model/VVSRequestConditionBase;-><init>()V

    .line 16
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->evaluators:Ljava/util/ArrayList;

    new-instance v1, Lcom/vlingo/voicepad/workflow/model/MinVvsVersionEvaluator;

    invoke-direct {v1, p0}, Lcom/vlingo/voicepad/workflow/model/MinVvsVersionEvaluator;-><init>(Lcom/vlingo/workflow/model/RequestCondition;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 17
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->evaluators:Ljava/util/ArrayList;

    new-instance v1, Lcom/vlingo/voicepad/workflow/model/AutoTagEvaluator;

    invoke-direct {v1, p0}, Lcom/vlingo/voicepad/workflow/model/AutoTagEvaluator;-><init>(Lcom/vlingo/workflow/model/RequestCondition;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 18
    return-void
.end method


# virtual methods
.method public evaluateConditions(Ljava/lang/String;Lcom/vlingo/voicepad/model/LicenseInfo;Lcom/vlingo/workflow/model/VRequest;)Z
    .locals 7
    .param p1, "servletName"    # Ljava/lang/String;
    .param p2, "license"    # Lcom/vlingo/voicepad/model/LicenseInfo;
    .param p3, "request"    # Lcom/vlingo/workflow/model/VRequest;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 39
    invoke-virtual {p0}, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->getLicenseClass()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 41
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/vlingo/voicepad/model/LicenseInfo;->getEffectiveLicenseClass()Ljava/lang/String;

    move-result-object v1

    .line 42
    .local v1, "licenseClass":Ljava/lang/String;
    :goto_0
    sget-object v4, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Evaluating LicenseClass: expected "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->getLicenseClass()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ", found "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->trace(Ljava/lang/Object;)V

    .line 43
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->getLicenseClass()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->matchesRegexIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    :cond_0
    move v2, v3

    .line 55
    .end local v1    # "licenseClass":Ljava/lang/String;
    :goto_1
    return v2

    :cond_1
    move-object v1, v2

    .line 41
    goto :goto_0

    .line 47
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->getFeatureLevel()Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 49
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/vlingo/voicepad/model/LicenseInfo;->getEffectiveFeatureLevel()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 50
    .local v0, "featureLevel":Ljava/lang/Integer;
    :goto_2
    sget-object v2, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Evaluating FeatureLevel: expected "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->getFeatureLevel()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", found "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/vlingo/common/log4j/VLogger;->trace(Ljava/lang/Object;)V

    .line 51
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0}, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->getFeatureLevel()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v2, v4, :cond_5

    :cond_3
    move v2, v3

    .line 52
    goto :goto_1

    .end local v0    # "featureLevel":Ljava/lang/Integer;
    :cond_4
    move-object v0, v2

    .line 49
    goto :goto_2

    .line 55
    :cond_5
    invoke-virtual {p0, p1, p3}, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->evaluateConditions(Ljava/lang/String;Lcom/vlingo/workflow/model/VRequest;)Z

    move-result v2

    goto :goto_1
.end method

.method public evaluateConditions_old(Ljava/lang/String;Lcom/vlingo/workflow/model/VRequest;)Z
    .locals 6
    .param p1, "servletName"    # Ljava/lang/String;
    .param p2, "request"    # Lcom/vlingo/workflow/model/VRequest;

    .prologue
    const/4 v2, 0x0

    .line 22
    invoke-virtual {p0}, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->getMinVVSVersion()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 23
    invoke-static {p2}, Lcom/vlingo/voicepad/vvs/VVServletUtil;->getVVSRequestVersion(Lcom/vlingo/workflow/model/VRequest;)Ljava/lang/String;

    move-result-object v1

    .line 24
    .local v1, "vvsVersion":Ljava/lang/String;
    sget-object v3, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Evaluating MinVVSVersion: expected "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->getMinVVSVersion()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " or higher, found "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->trace(Ljava/lang/Object;)V

    .line 25
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->getMinVVSVersion()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/vlingo/voicepad/workflow/WorkflowManager;->compareVersions(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    if-gez v3, :cond_1

    .line 35
    .end local v1    # "vvsVersion":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 29
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->getAutoTag()Ljava/lang/Boolean;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 30
    invoke-static {p2}, Lcom/vlingo/voicepad/vvs/VVServletUtil;->getAutoTag(Lcom/vlingo/workflow/model/VRequest;)Ljava/lang/Boolean;

    move-result-object v0

    .line 31
    .local v0, "doAutoTag":Ljava/lang/Boolean;
    sget-object v3, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Evaluating AutoTag: expected "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->getAutoTag()Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", found "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->trace(Ljava/lang/Object;)V

    .line 32
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 35
    .end local v0    # "doAutoTag":Ljava/lang/Boolean;
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/vlingo/voicepad/workflow/model/VVSRequestConditionBase;->evaluateConditions(Ljava/lang/String;Lcom/vlingo/workflow/model/VRequest;)Z

    move-result v2

    goto :goto_0
.end method
