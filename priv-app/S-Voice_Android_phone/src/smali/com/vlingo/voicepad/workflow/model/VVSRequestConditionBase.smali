.class public Lcom/vlingo/voicepad/workflow/model/VVSRequestConditionBase;
.super Lcom/vlingo/workflow/model/RequestCondition;
.source "VVSRequestConditionBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_AutoTag:Ljava/lang/String; = "AutoTag"

.field public static final PROP_FeatureLevel:Ljava/lang/String; = "FeatureLevel"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_LicenseClass:Ljava/lang/String; = "LicenseClass"

.field public static final PROP_MinVVSVersion:Ljava/lang/String; = "MinVVSVersion"


# instance fields
.field private AutoTag:Ljava/lang/Boolean;

.field private FeatureLevel:Ljava/lang/Integer;

.field private ID:J

.field private LicenseClass:Ljava/lang/String;

.field private MinVVSVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/workflow/model/RequestCondition;-><init>()V

    .line 17
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 55
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAutoTag()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/VVSRequestConditionBase;->AutoTag:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 59
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getFeatureLevel()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/VVSRequestConditionBase;->FeatureLevel:Ljava/lang/Integer;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/vlingo/voicepad/workflow/model/VVSRequestConditionBase;->ID:J

    return-wide v0
.end method

.method public getLicenseClass()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/VVSRequestConditionBase;->LicenseClass:Ljava/lang/String;

    return-object v0
.end method

.method public getMinVVSVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vlingo/voicepad/workflow/model/VVSRequestConditionBase;->MinVVSVersion:Ljava/lang/String;

    return-object v0
.end method

.method public setAutoTag(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Boolean;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/VVSRequestConditionBase;->AutoTag:Ljava/lang/Boolean;

    .line 44
    return-void
.end method

.method public setFeatureLevel(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Integer;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/VVSRequestConditionBase;->FeatureLevel:Ljava/lang/Integer;

    .line 37
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 50
    iput-wide p1, p0, Lcom/vlingo/voicepad/workflow/model/VVSRequestConditionBase;->ID:J

    .line 51
    return-void
.end method

.method public setLicenseClass(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/VVSRequestConditionBase;->LicenseClass:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setMinVVSVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/vlingo/voicepad/workflow/model/VVSRequestConditionBase;->MinVVSVersion:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
