.class public Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;
.super Ljava/lang/Object;
.source "FeatureSetManager.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager$1;,
        Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager$FeatureComparator;
    }
.end annotation


# static fields
.field private static final VALIDATE_PATTERN:Ljava/util/regex/Pattern;

.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;

.field private static final serialVersionUID:J = -0x6d59a95e137c0ad6L


# instance fields
.field private final ivActionTemplates:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;",
            ">;"
        }
    .end annotation
.end field

.field private ivConfigLoader:Lcom/vlingo/mda/cfgloader/ConfigLoader;

.field private ivFeatureMaps:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;",
            ">;"
        }
    .end annotation
.end field

.field private final ivFeatureSets:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;",
            ">;"
        }
    .end annotation
.end field

.field private ivOK:Z

.field private transient ivResourceManager:Lcom/vlingo/voicepad/config/ResourceManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    .line 54
    const-string/jumbo v0, "\\$\\{[^}]+\\}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->VALIDATE_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;Lcom/vlingo/voicepad/config/ResourceManager;)V
    .locals 1
    .param p1, "loader"    # Lcom/vlingo/mda/cfgloader/ConfigLoader;
    .param p2, "rootConfigFilePath"    # Ljava/lang/String;
    .param p3, "resourceManager"    # Lcom/vlingo/voicepad/config/ResourceManager;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivFeatureSets:Ljava/util/Map;

    .line 58
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivActionTemplates:Ljava/util/Map;

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivConfigLoader:Lcom/vlingo/mda/cfgloader/ConfigLoader;

    .line 67
    iput-object p3, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivResourceManager:Lcom/vlingo/voicepad/config/ResourceManager;

    .line 68
    invoke-direct {p0, p1, p2}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->initFromLoader(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method public constructor <init>(Ljava/io/File;Ljava/lang/String;Lcom/vlingo/voicepad/config/ResourceManager;)V
    .locals 1
    .param p1, "file"    # Ljava/io/File;
    .param p2, "rootConfigFilePath"    # Ljava/lang/String;
    .param p3, "resourceManager"    # Lcom/vlingo/voicepad/config/ResourceManager;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivFeatureSets:Ljava/util/Map;

    .line 58
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivActionTemplates:Ljava/util/Map;

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivConfigLoader:Lcom/vlingo/mda/cfgloader/ConfigLoader;

    .line 73
    iput-object p3, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivResourceManager:Lcom/vlingo/voicepad/config/ResourceManager;

    .line 74
    invoke-direct {p0, p1, p2}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->initFromFile(Ljava/io/File;Ljava/lang/String;)V

    .line 75
    return-void
.end method

.method private addInheritedFeatures(Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;Ljava/util/List;)V
    .locals 4
    .param p1, "featureSet"    # Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/tagtoaction/model/Feature;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 380
    .local p2, "features":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/tagtoaction/model/Feature;>;"
    invoke-virtual {p1}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->getFeatures()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/voicepad/tagtoaction/model/Feature;

    .line 381
    .local v0, "feature":Lcom/vlingo/voicepad/tagtoaction/model/Feature;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 385
    .end local v0    # "feature":Lcom/vlingo/voicepad/tagtoaction/model/Feature;
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->getParents()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;

    .line 386
    .local v2, "parent":Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    invoke-direct {p0, v2, p2}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->addInheritedFeatures(Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;Ljava/util/List;)V

    goto :goto_1

    .line 388
    .end local v2    # "parent":Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    :cond_1
    return-void
.end method

.method private getFeatureMapByName(Ljava/lang/String;)Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;
    .locals 3
    .param p1, "featureMapName"    # Ljava/lang/String;

    .prologue
    .line 346
    invoke-direct {p0}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->getFeatureMapSet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;

    .line 347
    .local v1, "map":Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;
    invoke-virtual {v1}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 351
    .end local v1    # "map":Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getFeatureMapSet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 329
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivFeatureMaps:Ljava/util/Set;

    return-object v0
.end method

.method private getFeaturesForMap(Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;)Ljava/util/List;
    .locals 4
    .param p1, "featureMap"    # Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/tagtoaction/model/Feature;",
            ">;"
        }
    .end annotation

    .prologue
    .line 362
    invoke-virtual {p1}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;->getCachedFeatureSet()Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;

    move-result-object v1

    .line 364
    .local v1, "leafSet":Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 365
    .local v0, "features":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/tagtoaction/model/Feature;>;"
    invoke-direct {p0, v1, v0}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->addInheritedFeatures(Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;Ljava/util/List;)V

    .line 367
    new-instance v2, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager$FeatureComparator;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager$FeatureComparator;-><init>(Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager$1;)V

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 369
    return-object v0
.end method

.method private initFromFile(Ljava/io/File;Ljava/lang/String;)V
    .locals 3
    .param p1, "file"    # Ljava/io/File;
    .param p2, "rootConfigFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;
        }
    .end annotation

    .prologue
    .line 82
    new-instance v0, Lcom/vlingo/mda/cfgloader/JarMDAConfigLoader;

    const-class v1, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionConfig;

    invoke-direct {v0, p1, v1}, Lcom/vlingo/mda/cfgloader/JarMDAConfigLoader;-><init>(Ljava/io/File;Ljava/lang/Class;)V

    .line 84
    .local v0, "loader":Lcom/vlingo/mda/cfgloader/ConfigLoader;
    :try_start_0
    invoke-direct {p0, v0, p2}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->initFromLoader(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    :try_start_1
    invoke-interface {v0}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 92
    :goto_0
    return-void

    .line 86
    :catchall_0
    move-exception v1

    .line 87
    :try_start_2
    invoke-interface {v0}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 90
    :goto_1
    throw v1

    .line 88
    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method private declared-synchronized initFromLoader(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;)V
    .locals 2
    .param p1, "loader"    # Lcom/vlingo/mda/cfgloader/ConfigLoader;
    .param p2, "rootConfigFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;
        }
    .end annotation

    .prologue
    .line 97
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivConfigLoader:Lcom/vlingo/mda/cfgloader/ConfigLoader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 99
    :try_start_1
    invoke-direct {p0, p2}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->loadRoot(Ljava/lang/String;)V

    .line 100
    invoke-direct {p0}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->precacheConfig()V

    .line 101
    invoke-direct {p0}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->validateConfig()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 103
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivConfigLoader:Lcom/vlingo/mda/cfgloader/ConfigLoader;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 106
    monitor-exit p0

    return-void

    .line 103
    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    :try_start_3
    iput-object v1, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivConfigLoader:Lcom/vlingo/mda/cfgloader/ConfigLoader;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 97
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private loadConfig(Ljava/lang/String;Ljava/util/Set;)Lcom/vlingo/voicepad/tagtoaction/model/TagToActionConfig;
    .locals 8
    .param p1, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/vlingo/voicepad/tagtoaction/model/TagToActionConfig;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;
        }
    .end annotation

    .prologue
    .line 140
    .local p2, "filenameSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    sget-object v5, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "loading "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 141
    invoke-interface {p2, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 143
    new-instance v5, Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Already loaded/imported filename: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 145
    :cond_0
    iget-object v5, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivConfigLoader:Lcom/vlingo/mda/cfgloader/ConfigLoader;

    invoke-interface {v5, p1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->load(Ljava/lang/String;)Lcom/vlingo/mda/util/MDAObject;

    move-result-object v1

    check-cast v1, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionConfig;

    .line 148
    .local v1, "config":Lcom/vlingo/voicepad/tagtoaction/model/TagToActionConfig;
    invoke-virtual {v1}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionConfig;->getFeatureSets()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;

    .line 149
    .local v2, "fs":Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    iget-object v5, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivFeatureSets:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 150
    new-instance v5, Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Duplicate FeatureSet with Name: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ", found in filename: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 151
    :cond_1
    iget-object v5, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivFeatureSets:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 155
    .end local v2    # "fs":Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    :cond_2
    invoke-virtual {v1}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionConfig;->getActionTemplates()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;

    .line 156
    .local v0, "at":Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;
    iget-object v5, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivActionTemplates:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 157
    new-instance v5, Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Duplicate ActionTemplate with Name: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ", found in filename: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 158
    :cond_3
    iget-object v5, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivActionTemplates:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 162
    .end local v0    # "at":Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;
    :cond_4
    invoke-virtual {v1}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionConfig;->getImports()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/voicepad/tagtoaction/model/Import;

    .line 163
    .local v4, "myImport":Lcom/vlingo/voicepad/tagtoaction/model/Import;
    invoke-virtual {v4}, Lcom/vlingo/voicepad/tagtoaction/model/Import;->getFile()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5, p2}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->loadConfig(Ljava/lang/String;Ljava/util/Set;)Lcom/vlingo/voicepad/tagtoaction/model/TagToActionConfig;

    goto :goto_2

    .line 166
    .end local v4    # "myImport":Lcom/vlingo/voicepad/tagtoaction/model/Import;
    :cond_5
    return-object v1
.end method

.method private loadRoot(Ljava/lang/String;)V
    .locals 4
    .param p1, "rootConfigFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;
        }
    .end annotation

    .prologue
    .line 133
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-direct {p0, p1, v1}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->loadConfig(Ljava/lang/String;Ljava/util/Set;)Lcom/vlingo/voicepad/tagtoaction/model/TagToActionConfig;

    move-result-object v0

    .line 134
    .local v0, "config":Lcom/vlingo/voicepad/tagtoaction/model/TagToActionConfig;
    invoke-virtual {v0}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionConfig;->getFeatureMaps()Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivFeatureMaps:Ljava/util/Set;

    .line 135
    sget-object v1, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "loaded "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivFeatureSets:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " feature sets, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->getFeatureMapSet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " feature maps"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 137
    return-void
.end method

.method private precacheConfig()V
    .locals 14

    .prologue
    .line 173
    iget-object v11, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivFeatureSets:Ljava/util/Map;

    invoke-interface {v11}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;

    .line 175
    .local v2, "fs":Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    invoke-virtual {v2}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->getExtends()Ljava/lang/String;

    move-result-object v8

    .line 176
    .local v8, "parentList":Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 178
    const-string/jumbo v11, ","

    invoke-virtual {v8, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 179
    .local v10, "parentNames":[Ljava/lang/String;
    move-object v0, v10

    .local v0, "arr$":[Ljava/lang/String;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_0

    aget-object v9, v0, v5

    .line 180
    .local v9, "parentName":Ljava/lang/String;
    iget-object v11, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivFeatureSets:Ljava/util/Map;

    invoke-interface {v11, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;

    .line 181
    .local v7, "parent":Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    if-nez v7, :cond_1

    .line 183
    new-instance v11, Ljava/lang/RuntimeException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "FeatureSet \'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "\' extends unknown FeatureSet \'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "\'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 185
    :cond_1
    invoke-virtual {v2, v7}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->addParent(Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;)V

    .line 179
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 191
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "fs":Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "parent":Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    .end local v8    # "parentList":Ljava/lang/String;
    .end local v9    # "parentName":Ljava/lang/String;
    .end local v10    # "parentNames":[Ljava/lang/String;
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->getFeatureMapSet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;

    .line 193
    .local v1, "fm":Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;
    invoke-virtual {v1}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;->getFeatureSet()Ljava/lang/String;

    move-result-object v3

    .line 194
    .local v3, "fsName":Ljava/lang/String;
    if-nez v3, :cond_3

    .line 196
    new-instance v11, Lcom/vlingo/common/message/MessageException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "FeatureMap \'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v1}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "\' does not reference any FeatureSet"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionError;->getBadMeta(Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;

    move-result-object v12

    invoke-direct {v11, v12}, Lcom/vlingo/common/message/MessageException;-><init>(Lcom/vlingo/common/message/VMessage;)V

    throw v11

    .line 198
    :cond_3
    iget-object v11, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivFeatureSets:Ljava/util/Map;

    invoke-interface {v11, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;

    .line 199
    .restart local v2    # "fs":Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    if-nez v2, :cond_4

    .line 201
    new-instance v11, Lcom/vlingo/common/message/MessageException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "FeatureMap \'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v1}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "\' references unknown FeatureSet \'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "\'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionError;->getBadMeta(Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;

    move-result-object v12

    invoke-direct {v11, v12}, Lcom/vlingo/common/message/MessageException;-><init>(Lcom/vlingo/common/message/VMessage;)V

    throw v11

    .line 203
    :cond_4
    invoke-virtual {v1, v2}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;->setCachedFeatureSet(Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;)V

    goto :goto_1

    .line 205
    .end local v1    # "fm":Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;
    .end local v2    # "fs":Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    .end local v3    # "fsName":Ljava/lang/String;
    :cond_5
    return-void
.end method

.method private validateActionParameter(Ljava/lang/String;)V
    .locals 6
    .param p1, "param"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;
        }
    .end annotation

    .prologue
    .line 263
    sget-object v3, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->VALIDATE_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 266
    .local v0, "matcher":Ljava/util/regex/Matcher;
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 267
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v2

    .line 269
    .local v2, "variable":Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->isVPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 270
    invoke-static {v2}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->validateVPath(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 271
    new-instance v3, Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;

    invoke-static {v2}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionError;->getInvalidRecognitionReference(Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;-><init>(Lcom/vlingo/common/message/VMessage;)V

    throw v3

    .line 274
    :cond_1
    invoke-static {v2}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->isResource(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 276
    iget-object v3, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivResourceManager:Lcom/vlingo/voicepad/config/ResourceManager;

    if-nez v3, :cond_2

    .line 277
    new-instance v3, Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Null resource manager for resource:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 280
    :cond_2
    iget-object v3, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivResourceManager:Lcom/vlingo/voicepad/config/ResourceManager;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-static {v3, v2, v4}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->getResourceValueForLocale(Lcom/vlingo/voicepad/config/ResourceManager;Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 282
    .local v1, "resource":Ljava/lang/String;
    if-nez v1, :cond_3

    .line 283
    new-instance v3, Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Null resource:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 285
    :cond_3
    const-string/jumbo v3, "${res:"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 286
    new-instance v3, Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Resource:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " contains another resource:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 288
    :cond_4
    invoke-direct {p0, v1}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->validateActionParameter(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 291
    .end local v1    # "resource":Ljava/lang/String;
    .end local v2    # "variable":Ljava/lang/String;
    :cond_5
    return-void
.end method

.method private validateActionTemplate(Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;)V
    .locals 5
    .param p1, "template"    # Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;
        }
    .end annotation

    .prologue
    .line 232
    invoke-virtual {p1}, Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;->getActions()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/voicepad/tagtoaction/model/Action;

    .line 233
    .local v0, "action":Lcom/vlingo/voicepad/tagtoaction/model/Action;
    invoke-virtual {v0}, Lcom/vlingo/voicepad/tagtoaction/model/Action;->getParams()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;

    .line 234
    .local v3, "param":Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;
    invoke-virtual {v3}, Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->validateActionParameter(Ljava/lang/String;)V

    goto :goto_0

    .line 237
    .end local v0    # "action":Lcom/vlingo/voicepad/tagtoaction/model/Action;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "param":Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;
    :cond_1
    return-void
.end method

.method private validateConfig()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;
        }
    .end annotation

    .prologue
    .line 209
    iget-object v3, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivFeatureSets:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 211
    new-instance v3, Lcom/vlingo/common/message/MessageException;

    const-string/jumbo v4, "No FeatureSets defined"

    invoke-static {v4}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionError;->getBadMeta(Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/vlingo/common/message/MessageException;-><init>(Lcom/vlingo/common/message/VMessage;)V

    throw v3

    .line 213
    :cond_0
    iget-object v3, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivResourceManager:Lcom/vlingo/voicepad/config/ResourceManager;

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->getFeatureMapSet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 215
    new-instance v3, Lcom/vlingo/common/message/MessageException;

    const-string/jumbo v4, "No FeatureMaps defined"

    invoke-static {v4}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionError;->getBadMeta(Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/vlingo/common/message/MessageException;-><init>(Lcom/vlingo/common/message/VMessage;)V

    throw v3

    .line 218
    :cond_1
    iget-object v3, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivActionTemplates:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;

    .line 220
    .local v2, "template":Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;
    invoke-direct {p0, v2}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->validateActionTemplate(Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;)V

    goto :goto_0

    .line 223
    .end local v2    # "template":Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;
    :cond_2
    iget-object v3, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivFeatureSets:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;

    .line 225
    .local v0, "fs":Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    invoke-direct {p0, v0}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->validateFeatureSet(Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;)V

    goto :goto_1

    .line 228
    .end local v0    # "fs":Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    :cond_3
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivOK:Z

    .line 229
    return-void
.end method

.method private validateFeatureSet(Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;)V
    .locals 9
    .param p1, "featureSet"    # Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;
        }
    .end annotation

    .prologue
    .line 242
    invoke-virtual {p1}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->getFeatures()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/voicepad/tagtoaction/model/Feature;

    .line 244
    .local v1, "feature":Lcom/vlingo/voicepad/tagtoaction/model/Feature;
    invoke-virtual {v1}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getActions()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/voicepad/tagtoaction/model/Action;

    .line 246
    .local v0, "action":Lcom/vlingo/voicepad/tagtoaction/model/Action;
    instance-of v6, v0, Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplateRef;

    if-eqz v6, :cond_2

    .line 248
    invoke-virtual {v0}, Lcom/vlingo/voicepad/tagtoaction/model/Action;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->getActionTemplateByName(Ljava/lang/String;)Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;

    move-result-object v6

    if-nez v6, :cond_2

    .line 249
    new-instance v6, Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Undefined action template \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/vlingo/voicepad/tagtoaction/model/Action;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\' referenced in feature \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 253
    :cond_2
    invoke-virtual {v0}, Lcom/vlingo/voicepad/tagtoaction/model/Action;->getParams()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;

    .line 255
    .local v5, "param":Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;
    invoke-virtual {v5}, Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->validateActionParameter(Ljava/lang/String;)V

    goto :goto_0

    .line 259
    .end local v0    # "action":Lcom/vlingo/voicepad/tagtoaction/model/Action;
    .end local v1    # "feature":Lcom/vlingo/voicepad/tagtoaction/model/Feature;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "param":Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;
    :cond_3
    return-void
.end method


# virtual methods
.method public findFeatureSetForRequest(Lcom/vlingo/voicepad/model/LicenseInfo;Lcom/vlingo/workflow/model/VRequest;)Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    .locals 6
    .param p1, "license"    # Lcom/vlingo/voicepad/model/LicenseInfo;
    .param p2, "request"    # Lcom/vlingo/workflow/model/VRequest;

    .prologue
    const/4 v2, 0x0

    .line 301
    invoke-direct {p0}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->getFeatureMapSet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;

    .line 303
    .local v1, "map":Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;
    sget-object v3, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "checking feature map "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->trace(Ljava/lang/Object;)V

    .line 305
    invoke-virtual {v1, v2, p1, p2}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;->evaluateConditions(Ljava/lang/String;Lcom/vlingo/voicepad/model/LicenseInfo;Lcom/vlingo/workflow/model/VRequest;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 307
    sget-object v2, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "FeatureMap MATCH! Name="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 309
    invoke-virtual {v1}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;->getCachedFeatureSet()Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;

    move-result-object v2

    .line 313
    .end local v1    # "map":Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;
    :cond_1
    return-object v2
.end method

.method public findSingleFeatureSet()Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 317
    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->getFeatureSets()Ljava/util/List;

    move-result-object v0

    .line 318
    .local v0, "featureSets":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 319
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "no single FeatureSet"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 321
    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;

    return-object v1
.end method

.method public getActionTemplateByName(Ljava/lang/String;)Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;
    .locals 1
    .param p1, "templateName"    # Ljava/lang/String;

    .prologue
    .line 342
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivActionTemplates:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;

    return-object v0
.end method

.method public getFeatureMaps()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {p0}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->getFeatureMapSet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getFeatureSets()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivFeatureSets:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getFeaturesForFeatureMap(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1, "featureMapName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/tagtoaction/model/Feature;",
            ">;"
        }
    .end annotation

    .prologue
    .line 337
    invoke-direct {p0, p1}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->getFeatureMapByName(Ljava/lang/String;)Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;

    move-result-object v0

    .line 338
    .local v0, "map":Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;
    invoke-direct {p0, v0}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->getFeaturesForMap(Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public isOK()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivOK:Z

    return v0
.end method

.method public setResourceManager(Lcom/vlingo/voicepad/config/ResourceManager;)V
    .locals 0
    .param p1, "resourceManager"    # Lcom/vlingo/voicepad/config/ResourceManager;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->ivResourceManager:Lcom/vlingo/voicepad/config/ResourceManager;

    .line 79
    return-void
.end method
