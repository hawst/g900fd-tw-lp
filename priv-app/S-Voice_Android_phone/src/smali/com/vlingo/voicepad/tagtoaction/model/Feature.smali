.class public Lcom/vlingo/voicepad/tagtoaction/model/Feature;
.super Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;
.source "Feature.java"


# static fields
.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private ivCapabilityPattern:Ljava/util/regex/Pattern;

.field private ivVPath2Pattern:Ljava/util/regex/Pattern;

.field private ivVPathPattern:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/vlingo/voicepad/tagtoaction/model/Feature;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;-><init>()V

    return-void
.end method

.method private checkVPath2Criteria(Lcom/vlingo/common/message/MNode;)Z
    .locals 3
    .param p1, "asrResponseRoot"    # Lcom/vlingo/common/message/MNode;

    .prologue
    const/4 v1, 0x0

    .line 154
    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getVPath2Exists()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 155
    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getVPath2Exists()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->getValueFromVPath(Ljava/lang/String;Lcom/vlingo/common/message/MNode;)Ljava/lang/String;

    move-result-object v0

    .line 156
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->ivVPath2Pattern:Ljava/util/regex/Pattern;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->ivVPath2Pattern:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-nez v2, :cond_1

    .line 166
    .end local v0    # "value":Ljava/lang/String;
    :cond_0
    :goto_0
    return v1

    .line 160
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getVPath2NotExists()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 161
    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getVPath2NotExists()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->getValueFromVPath(Ljava/lang/String;Lcom/vlingo/common/message/MNode;)Ljava/lang/String;

    move-result-object v0

    .line 162
    .restart local v0    # "value":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 166
    .end local v0    # "value":Ljava/lang/String;
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private checkVPathCriteria(Lcom/vlingo/common/message/MNode;)Z
    .locals 3
    .param p1, "asrResponseRoot"    # Lcom/vlingo/common/message/MNode;

    .prologue
    const/4 v1, 0x0

    .line 126
    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getVPathExists()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 127
    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getVPathExists()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->getValueFromVPath(Ljava/lang/String;Lcom/vlingo/common/message/MNode;)Ljava/lang/String;

    move-result-object v0

    .line 128
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->ivVPathPattern:Ljava/util/regex/Pattern;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->ivVPathPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-nez v2, :cond_1

    .line 138
    .end local v0    # "value":Ljava/lang/String;
    :cond_0
    :goto_0
    return v1

    .line 132
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getVPathNotExists()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 133
    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getVPathNotExists()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->getValueFromVPath(Ljava/lang/String;Lcom/vlingo/common/message/MNode;)Ljava/lang/String;

    move-result-object v0

    .line 134
    .restart local v0    # "value":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 138
    .end local v0    # "value":Ljava/lang/String;
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private checkVPathCriteria(Lcom/vlingo/dialog/goal/model/Goal;)Z
    .locals 4
    .param p1, "goal"    # Lcom/vlingo/dialog/goal/model/Goal;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 182
    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getVPathExists()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    move v1, v2

    .line 188
    :cond_0
    :goto_0
    return v1

    .line 186
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getVPathExists()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->evaluateJexlExpression(Lcom/vlingo/dialog/goal/model/Goal;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 187
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 188
    iget-object v3, p0, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->ivVPathPattern:Ljava/util/regex/Pattern;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->ivVPathPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method private evaluateCapability(Ljava/util/Map;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "capabilityMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 106
    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getCapabilityName()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 121
    :goto_0
    return v1

    .line 107
    :cond_0
    iget-object v2, p0, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->ivCapabilityPattern:Ljava/util/regex/Pattern;

    if-nez v2, :cond_1

    .line 108
    sget-object v2, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "returning false, tag-to-action specifying Feature with CapabilityName but no valid CapabilityValue. Feature: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    goto :goto_0

    .line 111
    :cond_1
    sget-object v2, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "looking for capability name: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getCapabilityName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 112
    if-nez p1, :cond_2

    .line 113
    sget-object v2, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v3, "returning false, no capability map (might be legacy request like suggest)"

    invoke-virtual {v2, v3}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    goto :goto_0

    .line 116
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getCapabilityName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 117
    .local v0, "mapValue":Ljava/lang/String;
    if-nez v0, :cond_3

    .line 118
    sget-object v2, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "returning false, tag-to-action specifying Feature with CapabilityName not found in config. Feature: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    goto :goto_0

    .line 121
    :cond_3
    iget-object v1, p0, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->ivCapabilityPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    goto :goto_0
.end method

.method private static optionalMatchString(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "featureString"    # Ljava/lang/String;
    .param p1, "checkString"    # Ljava/lang/String;

    .prologue
    .line 67
    if-eqz p0, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p1, p0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public isMatch(Lcom/vlingo/dialog/goal/model/Goal;)Z
    .locals 2
    .param p1, "goal"    # Lcom/vlingo/dialog/goal/model/Goal;

    .prologue
    .line 101
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getGoalClass()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->checkVPathCriteria(Lcom/vlingo/dialog/goal/model/Goal;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMatch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/voicepad/model/LicenseInfo;Lcom/vlingo/workflow/model/VRequest;Lcom/vlingo/common/message/MNode;Ljava/util/Map;)Z
    .locals 3
    .param p1, "responseParseType"    # Ljava/lang/String;
    .param p2, "requestFieldID"    # Ljava/lang/String;
    .param p3, "requestFieldContext"    # Ljava/lang/String;
    .param p4, "license"    # Lcom/vlingo/voicepad/model/LicenseInfo;
    .param p5, "request"    # Lcom/vlingo/workflow/model/VRequest;
    .param p6, "asrResponseRoot"    # Lcom/vlingo/common/message/MNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/vlingo/voicepad/model/LicenseInfo;",
            "Lcom/vlingo/workflow/model/VRequest;",
            "Lcom/vlingo/common/message/MNode;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 83
    .local p7, "capabilityMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getParseType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getFieldID()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->optionalMatchString(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getFieldContext()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->optionalMatchString(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p4, p5}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->evaluateConditions(Ljava/lang/String;Lcom/vlingo/voicepad/model/LicenseInfo;Lcom/vlingo/workflow/model/VRequest;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p6}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->checkVPathCriteria(Lcom/vlingo/common/message/MNode;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p6}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->checkVPath2Criteria(Lcom/vlingo/common/message/MNode;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p7}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->evaluateCapability(Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    sget-object v0, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Feature MATCH! Name="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 92
    const/4 v0, 0x1

    .line 96
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCapabilityValue(Ljava/lang/String;)V
    .locals 1
    .param p1, "regex"    # Ljava/lang/String;

    .prologue
    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->ivCapabilityPattern:Ljava/util/regex/Pattern;

    .line 48
    invoke-super {p0, p1}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->setCapabilityValue(Ljava/lang/String;)V

    .line 49
    if-nez p1, :cond_0

    .line 51
    :goto_0
    return-void

    .line 50
    :cond_0
    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->ivCapabilityPattern:Ljava/util/regex/Pattern;

    goto :goto_0
.end method

.method public setVPath2Equals(Ljava/lang/String;)V
    .locals 1
    .param p1, "regex"    # Ljava/lang/String;

    .prologue
    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->ivVPath2Pattern:Ljava/util/regex/Pattern;

    .line 40
    invoke-super {p0, p1}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->setVPath2Equals(Ljava/lang/String;)V

    .line 41
    if-nez p1, :cond_0

    .line 43
    :goto_0
    return-void

    .line 42
    :cond_0
    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->ivVPath2Pattern:Ljava/util/regex/Pattern;

    goto :goto_0
.end method

.method public setVPathEquals(Ljava/lang/String;)V
    .locals 1
    .param p1, "regex"    # Ljava/lang/String;

    .prologue
    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->ivVPathPattern:Ljava/util/regex/Pattern;

    .line 32
    invoke-super {p0, p1}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->setVPathEquals(Ljava/lang/String;)V

    .line 33
    if-nez p1, :cond_0

    .line 35
    :goto_0
    return-void

    .line 34
    :cond_0
    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->ivVPathPattern:Ljava/util/regex/Pattern;

    goto :goto_0
.end method
