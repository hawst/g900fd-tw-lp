.class public Lcom/vlingo/voicepad/tagtoaction/model/Action;
.super Lcom/vlingo/voicepad/tagtoaction/model/ActionBase;
.source "Action.java"


# static fields
.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private ivCapabilityPattern:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/vlingo/voicepad/tagtoaction/model/Action;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/model/Action;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/vlingo/voicepad/tagtoaction/model/ActionBase;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/Action;->ivCapabilityPattern:Ljava/util/regex/Pattern;

    return-void
.end method


# virtual methods
.method public evaluateCapability(Ljava/util/Map;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "capabilityMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 23
    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/Action;->getCapabilityName()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 38
    :goto_0
    return v1

    .line 24
    :cond_0
    iget-object v2, p0, Lcom/vlingo/voicepad/tagtoaction/model/Action;->ivCapabilityPattern:Ljava/util/regex/Pattern;

    if-nez v2, :cond_1

    .line 25
    sget-object v2, Lcom/vlingo/voicepad/tagtoaction/model/Action;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "returning false, tag-to-action specifying Action with CapabilityName but no valid CapabilityValue. Feature: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    goto :goto_0

    .line 28
    :cond_1
    sget-object v2, Lcom/vlingo/voicepad/tagtoaction/model/Action;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v2}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/vlingo/voicepad/tagtoaction/model/Action;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "looking for capability name: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/Action;->getCapabilityName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 29
    :cond_2
    if-nez p1, :cond_3

    .line 30
    sget-object v2, Lcom/vlingo/voicepad/tagtoaction/model/Action;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v3, "returning false, no capability map (might be legacy request like suggest)"

    invoke-virtual {v2, v3}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    goto :goto_0

    .line 33
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/voicepad/tagtoaction/model/Action;->getCapabilityName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 34
    .local v0, "mapValue":Ljava/lang/String;
    if-nez v0, :cond_4

    .line 35
    sget-object v2, Lcom/vlingo/voicepad/tagtoaction/model/Action;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "returning false, tag-to-action specifying Action with CapabilityName not found in config. Feature: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    goto :goto_0

    .line 38
    :cond_4
    iget-object v1, p0, Lcom/vlingo/voicepad/tagtoaction/model/Action;->ivCapabilityPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    goto :goto_0
.end method

.method public setCapabilityValue(Ljava/lang/String;)V
    .locals 1
    .param p1, "regex"    # Ljava/lang/String;

    .prologue
    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/Action;->ivCapabilityPattern:Ljava/util/regex/Pattern;

    .line 17
    invoke-super {p0, p1}, Lcom/vlingo/voicepad/tagtoaction/model/ActionBase;->setCapabilityValue(Ljava/lang/String;)V

    .line 18
    if-nez p1, :cond_0

    .line 20
    :goto_0
    return-void

    .line 19
    :cond_0
    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/Action;->ivCapabilityPattern:Ljava/util/regex/Pattern;

    goto :goto_0
.end method
