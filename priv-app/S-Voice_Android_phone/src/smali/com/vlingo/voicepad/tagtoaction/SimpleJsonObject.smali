.class public Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;
.super Ljava/lang/Object;
.source "SimpleJsonObject.java"


# instance fields
.field private map:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->map:Ljava/util/Map;

    .line 13
    return-void
.end method

.method private static append(Ljava/lang/StringBuilder;Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;)V
    .locals 1
    .param p0, "sb"    # Ljava/lang/StringBuilder;
    .param p1, "simpleJsonObject"    # Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;

    .prologue
    .line 84
    invoke-virtual {p1}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    return-void
.end method

.method private static append(Ljava/lang/StringBuilder;Ljava/lang/Number;)V
    .locals 1
    .param p0, "sb"    # Ljava/lang/StringBuilder;
    .param p1, "number"    # Ljava/lang/Number;

    .prologue
    .line 80
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    return-void
.end method

.method private static append(Ljava/lang/StringBuilder;Ljava/lang/Object;)V
    .locals 3
    .param p0, "sb"    # Ljava/lang/StringBuilder;
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 26
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 27
    check-cast p1, Ljava/lang/String;

    .end local p1    # "object":Ljava/lang/Object;
    invoke-static {p0, p1}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->append(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 39
    :goto_0
    return-void

    .line 28
    .restart local p1    # "object":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Ljava/lang/Number;

    if-eqz v0, :cond_1

    .line 29
    check-cast p1, Ljava/lang/Number;

    .end local p1    # "object":Ljava/lang/Object;
    invoke-static {p0, p1}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->append(Ljava/lang/StringBuilder;Ljava/lang/Number;)V

    goto :goto_0

    .line 30
    .restart local p1    # "object":Ljava/lang/Object;
    :cond_1
    instance-of v0, p1, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;

    if-eqz v0, :cond_2

    .line 31
    check-cast p1, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;

    .end local p1    # "object":Ljava/lang/Object;
    invoke-static {p0, p1}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->append(Ljava/lang/StringBuilder;Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;)V

    goto :goto_0

    .line 32
    .restart local p1    # "object":Ljava/lang/Object;
    :cond_2
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_3

    .line 33
    check-cast p1, Ljava/util/Collection;

    .end local p1    # "object":Ljava/lang/Object;
    invoke-static {p0, p1}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->append(Ljava/lang/StringBuilder;Ljava/util/Collection;)V

    goto :goto_0

    .line 34
    .restart local p1    # "object":Ljava/lang/Object;
    :cond_3
    instance-of v0, p1, Ljava/util/Map;

    if-eqz v0, :cond_4

    .line 35
    check-cast p1, Ljava/util/Map;

    .end local p1    # "object":Ljava/lang/Object;
    invoke-static {p0, p1}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->append(Ljava/lang/StringBuilder;Ljava/util/Map;)V

    goto :goto_0

    .line 37
    .restart local p1    # "object":Ljava/lang/Object;
    :cond_4
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unsupported object: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static append(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .locals 7
    .param p0, "sb"    # Ljava/lang/StringBuilder;
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0x22

    .line 42
    invoke-virtual {p0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 43
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 44
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 45
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 46
    .local v0, "c":C
    sparse-switch v0, :sswitch_data_0

    .line 68
    const/16 v2, 0x20

    if-ge v0, v2, :cond_0

    .line 69
    const-string/jumbo v2, "\\u%04x"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 49
    :sswitch_0
    const/16 v2, 0x5c

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 50
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 53
    :sswitch_1
    const-string/jumbo v2, "\\b"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 56
    :sswitch_2
    const-string/jumbo v2, "\\f"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 59
    :sswitch_3
    const-string/jumbo v2, "\\n"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 62
    :sswitch_4
    const-string/jumbo v2, "\\r"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 65
    :sswitch_5
    const-string/jumbo v2, "\\t"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 71
    :cond_0
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 76
    .end local v0    # "c":C
    .end local v1    # "i":I
    :cond_1
    invoke-virtual {p0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 77
    return-void

    .line 46
    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_1
        0x9 -> :sswitch_5
        0xa -> :sswitch_3
        0xc -> :sswitch_2
        0xd -> :sswitch_4
        0x22 -> :sswitch_0
        0x5c -> :sswitch_0
    .end sparse-switch
.end method

.method private static append(Ljava/lang/StringBuilder;Ljava/util/Collection;)V
    .locals 4
    .param p0, "sb"    # Ljava/lang/StringBuilder;
    .param p1, "collection"    # Ljava/util/Collection;

    .prologue
    .line 88
    const/16 v3, 0x5b

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 89
    const/4 v0, 0x1

    .line 90
    .local v0, "first":Z
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 91
    .local v2, "object":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 92
    const/16 v3, 0x2c

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 94
    :cond_0
    invoke-static {p0, v2}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->append(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    .line 95
    const/4 v0, 0x0

    goto :goto_0

    .line 97
    .end local v2    # "object":Ljava/lang/Object;
    :cond_1
    const/16 v3, 0x5d

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 98
    return-void
.end method

.method private static append(Ljava/lang/StringBuilder;Ljava/util/Map;)V
    .locals 7
    .param p0, "sb"    # Ljava/lang/StringBuilder;
    .param p1, "map"    # Ljava/util/Map;

    .prologue
    .line 101
    const/16 v4, 0x7b

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 102
    const/4 v1, 0x1

    .line 103
    .local v1, "first":Z
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 104
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 105
    .local v0, "entry":Ljava/util/Map$Entry;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    .line 106
    .local v3, "key":Ljava/lang/Object;
    instance-of v4, v3, Ljava/lang/String;

    if-nez v4, :cond_0

    .line 107
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "only string keys supported, found "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 109
    :cond_0
    if-nez v1, :cond_1

    .line 110
    const/16 v4, 0x2c

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 112
    :cond_1
    invoke-static {p0, v3}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->append(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    .line 113
    const/16 v4, 0x3a

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 114
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->append(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    .line 115
    const/4 v1, 0x0

    .line 116
    goto :goto_0

    .line 117
    .end local v0    # "entry":Ljava/util/Map$Entry;
    .end local v3    # "key":Ljava/lang/Object;
    :cond_2
    const/16 v4, 0x7d

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 118
    return-void
.end method


# virtual methods
.method public put(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->map:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 20
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->map:Ljava/util/Map;

    invoke-static {v0, v1}, Lcom/vlingo/voicepad/tagtoaction/SimpleJsonObject;->append(Ljava/lang/StringBuilder;Ljava/util/Map;)V

    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
