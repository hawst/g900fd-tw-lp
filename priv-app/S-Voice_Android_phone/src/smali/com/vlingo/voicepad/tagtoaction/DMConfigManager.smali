.class public Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;
.super Ljava/lang/Object;
.source "DMConfigManager.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;

.field private static final serialVersionUID:J = -0x6e587404c363c951L


# instance fields
.field private transient ivConfigLoader:Lcom/vlingo/mda/cfgloader/ConfigLoader;

.field private ivDMConfigMaps:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/dialog/manager/model/DialogManagerConfigMap;",
            ">;"
        }
    .end annotation
.end field

.field private ivDMConfigs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/dialog/manager/model/DialogManagerConfig;",
            ">;"
        }
    .end annotation
.end field

.field private ivOK:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;)V
    .locals 1
    .param p1, "loader"    # Lcom/vlingo/mda/cfgloader/ConfigLoader;
    .param p2, "rootConfigFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->ivDMConfigs:Ljava/util/Map;

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->ivConfigLoader:Lcom/vlingo/mda/cfgloader/ConfigLoader;

    .line 40
    invoke-direct {p0, p1, p2}, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->initFromLoader(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;)V

    .line 41
    return-void
.end method

.method private getDMConfigMapSet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/dialog/manager/model/DialogManagerConfigMap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->ivDMConfigMaps:Ljava/util/Set;

    return-object v0
.end method

.method private declared-synchronized initFromLoader(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;)V
    .locals 2
    .param p1, "loader"    # Lcom/vlingo/mda/cfgloader/ConfigLoader;
    .param p2, "rootConfigFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->ivConfigLoader:Lcom/vlingo/mda/cfgloader/ConfigLoader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 47
    :try_start_1
    invoke-direct {p0, p2}, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->loadRoot(Ljava/lang/String;)V

    .line 48
    invoke-direct {p0}, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->precacheConfig()V

    .line 49
    invoke-direct {p0}, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->validateConfig()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 51
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->ivConfigLoader:Lcom/vlingo/mda/cfgloader/ConfigLoader;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 54
    monitor-exit p0

    return-void

    .line 51
    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    :try_start_3
    iput-object v1, p0, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->ivConfigLoader:Lcom/vlingo/mda/cfgloader/ConfigLoader;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 45
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private loadConfig(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/DialogManagerConfig;
    .locals 6
    .param p1, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    sget-object v3, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "loading "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 67
    iget-object v3, p0, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->ivConfigLoader:Lcom/vlingo/mda/cfgloader/ConfigLoader;

    invoke-interface {v3, p1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->load(Ljava/lang/String;)Lcom/vlingo/mda/util/MDAObject;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    .line 69
    .local v0, "config":Lcom/vlingo/dialog/manager/model/DialogManagerConfig;
    invoke-virtual {v0}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->postDeserialize()V

    .line 72
    iget-object v3, p0, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->ivDMConfigs:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    invoke-virtual {v0}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getImports()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/dialog/manager/model/Import;

    .line 76
    .local v2, "myImport":Lcom/vlingo/dialog/manager/model/Import;
    invoke-virtual {v2}, Lcom/vlingo/dialog/manager/model/Import;->getFile()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->loadConfig(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    goto :goto_0

    .line 79
    .end local v2    # "myImport":Lcom/vlingo/dialog/manager/model/Import;
    :cond_0
    return-object v0
.end method

.method private loadRoot(Ljava/lang/String;)V
    .locals 4
    .param p1, "rootConfigFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->loadConfig(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    move-result-object v0

    .line 59
    .local v0, "config":Lcom/vlingo/dialog/manager/model/DialogManagerConfig;
    invoke-virtual {v0}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getDialogManagerConfigMaps()Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->ivDMConfigMaps:Ljava/util/Set;

    .line 60
    sget-object v1, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "loaded "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->ivDMConfigs:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " feature sets, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->getDMConfigMapSet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " feature maps"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 62
    return-void
.end method

.method private precacheConfig()V
    .locals 7

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->getDMConfigMapSet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/dialog/manager/model/DialogManagerConfigMap;

    .line 96
    .local v2, "dmcm":Lcom/vlingo/dialog/manager/model/DialogManagerConfigMap;
    invoke-virtual {v2}, Lcom/vlingo/dialog/manager/model/DialogManagerConfigMap;->getDialogManagerConfig()Ljava/lang/String;

    move-result-object v1

    .line 97
    .local v1, "dmcName":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 99
    new-instance v4, Lcom/vlingo/common/message/MessageException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "DialogManagerConfigMap \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/vlingo/dialog/manager/model/DialogManagerConfigMap;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\' does not reference any DialogManagerConfig"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionError;->getBadMeta(Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/vlingo/common/message/MessageException;-><init>(Lcom/vlingo/common/message/VMessage;)V

    throw v4

    .line 101
    :cond_0
    iget-object v4, p0, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->ivDMConfigs:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    .line 102
    .local v0, "dmc":Lcom/vlingo/dialog/manager/model/DialogManagerConfig;
    if-nez v0, :cond_1

    .line 104
    new-instance v4, Lcom/vlingo/common/message/MessageException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "DialogManagerConfigMap \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/vlingo/dialog/manager/model/DialogManagerConfigMap;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\' references unknown DialogManagerConfig \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionError;->getBadMeta(Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/vlingo/common/message/MessageException;-><init>(Lcom/vlingo/common/message/VMessage;)V

    throw v4

    .line 106
    :cond_1
    invoke-virtual {v2, v0}, Lcom/vlingo/dialog/manager/model/DialogManagerConfigMap;->setCachedDialogManagerConfig(Lcom/vlingo/dialog/manager/model/DialogManagerConfig;)V

    goto :goto_0

    .line 108
    .end local v0    # "dmc":Lcom/vlingo/dialog/manager/model/DialogManagerConfig;
    .end local v1    # "dmcName":Ljava/lang/String;
    .end local v2    # "dmcm":Lcom/vlingo/dialog/manager/model/DialogManagerConfigMap;
    :cond_2
    return-void
.end method

.method private validateConfig()V
    .locals 4

    .prologue
    .line 153
    iget-object v2, p0, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->ivDMConfigs:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 155
    new-instance v2, Lcom/vlingo/common/message/MessageException;

    const-string/jumbo v3, "No FeatureSets defined"

    invoke-static {v3}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionError;->getBadMeta(Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/vlingo/common/message/MessageException;-><init>(Lcom/vlingo/common/message/VMessage;)V

    throw v2

    .line 157
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->getDMConfigMapSet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 163
    :cond_1
    iget-object v2, p0, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->ivDMConfigs:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    .line 165
    .local v0, "dmc":Lcom/vlingo/dialog/manager/model/DialogManagerConfig;
    invoke-direct {p0, v0}, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->validateDMConfig(Lcom/vlingo/dialog/manager/model/DialogManagerConfig;)V

    goto :goto_0

    .line 168
    .end local v0    # "dmc":Lcom/vlingo/dialog/manager/model/DialogManagerConfig;
    :cond_2
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->ivOK:Z

    .line 169
    return-void
.end method

.method private validateDMConfig(Lcom/vlingo/dialog/manager/model/DialogManagerConfig;)V
    .locals 0
    .param p1, "dmconfig"    # Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    .prologue
    .line 173
    return-void
.end method


# virtual methods
.method public findDMConfigForRequest(Lcom/vlingo/workflow/model/VRequest;)Lcom/vlingo/dialog/manager/model/DialogManagerConfig;
    .locals 6
    .param p1, "request"    # Lcom/vlingo/workflow/model/VRequest;

    .prologue
    const/4 v2, 0x0

    .line 117
    invoke-direct {p0}, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->getDMConfigMapSet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/manager/model/DialogManagerConfigMap;

    .line 119
    .local v1, "map":Lcom/vlingo/dialog/manager/model/DialogManagerConfigMap;
    sget-object v3, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "checking feature map "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/vlingo/dialog/manager/model/DialogManagerConfigMap;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 121
    invoke-virtual {v1, v2, p1}, Lcom/vlingo/dialog/manager/model/DialogManagerConfigMap;->evaluateConditions(Ljava/lang/String;Lcom/vlingo/workflow/model/VRequest;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 123
    sget-object v2, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "DMConfig MATCH! Name="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/vlingo/dialog/manager/model/DialogManagerConfigMap;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 125
    invoke-virtual {v1}, Lcom/vlingo/dialog/manager/model/DialogManagerConfigMap;->getCachedDialogManagerConfig()Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    move-result-object v2

    .line 129
    .end local v1    # "map":Lcom/vlingo/dialog/manager/model/DialogManagerConfigMap;
    :cond_1
    return-object v2
.end method

.method public findSingleDMConfig()Lcom/vlingo/dialog/manager/model/DialogManagerConfig;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 133
    const/4 v3, 0x0

    .line 134
    .local v3, "result":Lcom/vlingo/dialog/manager/model/DialogManagerConfig;
    iget-object v4, p0, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->ivDMConfigs:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 135
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/vlingo/dialog/manager/model/DialogManagerConfig;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    .line 136
    .local v0, "config":Lcom/vlingo/dialog/manager/model/DialogManagerConfig;
    const-string/jumbo v4, "Main"

    invoke-virtual {v0}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 138
    if-eqz v3, :cond_1

    .line 139
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string/jumbo v5, "more than one DMConfig found"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 141
    :cond_1
    move-object v3, v0

    goto :goto_0

    .line 144
    .end local v0    # "config":Lcom/vlingo/dialog/manager/model/DialogManagerConfig;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/vlingo/dialog/manager/model/DialogManagerConfig;>;"
    :cond_2
    if-nez v3, :cond_3

    .line 145
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string/jumbo v5, "no DMConfig not found"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 147
    :cond_3
    sget-object v4, Lcom/vlingo/voicepad/tagtoaction/DMConfigManager;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "found DMConfig: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 148
    return-object v3
.end method
