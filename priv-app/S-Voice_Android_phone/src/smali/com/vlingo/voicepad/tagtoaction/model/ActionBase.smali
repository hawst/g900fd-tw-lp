.class public Lcom/vlingo/voicepad/tagtoaction/model/ActionBase;
.super Ljava/lang/Object;
.source "ActionBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_CapabilityName:Ljava/lang/String; = "CapabilityName"

.field public static final PROP_CapabilityValue:Ljava/lang/String; = "CapabilityValue"

.field public static final PROP_Else:Ljava/lang/String; = "Else"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_If:Ljava/lang/String; = "If"

.field public static final PROP_Name:Ljava/lang/String; = "Name"

.field public static final PROP_Params:Ljava/lang/String; = "Params"


# instance fields
.field private CapabilityName:Ljava/lang/String;

.field private CapabilityValue:Ljava/lang/String;

.field private Else:Ljava/lang/String;

.field private ID:J

.field private If:Ljava/lang/String;

.field private Name:Ljava/lang/String;

.field private Params:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/ActionBase;->Params:Ljava/util/Set;

    .line 21
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 68
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/voicepad/tagtoaction/model/Action;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCapabilityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/ActionBase;->CapabilityName:Ljava/lang/String;

    return-object v0
.end method

.method public getCapabilityValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/ActionBase;->CapabilityValue:Ljava/lang/String;

    return-object v0
.end method

.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 72
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/voicepad/tagtoaction/model/Action;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getElse()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/ActionBase;->Else:Ljava/lang/String;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/ActionBase;->ID:J

    return-wide v0
.end method

.method public getIf()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/ActionBase;->If:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/ActionBase;->Name:Ljava/lang/String;

    return-object v0
.end method

.method public getParams()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/ActionBase;->Params:Ljava/util/Set;

    return-object v0
.end method

.method public setCapabilityName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/ActionBase;->CapabilityName:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public setCapabilityValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/ActionBase;->CapabilityValue:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public setElse(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/ActionBase;->Else:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 63
    iput-wide p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/ActionBase;->ID:J

    .line 64
    return-void
.end method

.method public setIf(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/ActionBase;->If:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/ActionBase;->Name:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
