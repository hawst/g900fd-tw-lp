.class public abstract Lcom/vlingo/voicepad/tagtoaction/VPathUtil;
.super Ljava/lang/Object;
.source "VPathUtil.java"


# static fields
.field public static final ATT_FIELDCONTEXT:Ljava/lang/String; = "FieldContext"

.field public static final ATT_FIELDID:Ljava/lang/String; = "FieldID"

.field public static final ATT_NAME:Ljava/lang/String; = "nm"

.field public static final ATT_PARSETYPE:Ljava/lang/String; = "t"

.field private static final BASE_NAME:Ljava/lang/String; = "TagToActionMessage"

.field public static final ELEM_CODE:Ljava/lang/String; = "Code"

.field public static final ELEM_DETAILS:Ljava/lang/String; = "Details"

.field public static final ELEM_ERROR:Ljava/lang/String; = "Error"

.field public static final ELEM_MESSAGE:Ljava/lang/String; = "Message"

.field public static final ELEM_PARSEGROUP:Ljava/lang/String; = "pg"

.field public static final ELEM_WARNING:Ljava/lang/String; = "Warning"

.field private static final PROP_EXT:Ljava/lang/String; = ".properties"

.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;

.field private static final sConstantVariables:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    const-class v0, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->sConstantVariables:Ljava/util/HashMap;

    .line 43
    sget-object v0, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->sConstantVariables:Ljava/util/HashMap;

    const-string/jumbo v1, "ClientWhereAmI"

    const-string/jumbo v2, "${whereami}"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->sConstantVariables:Ljava/util/HashMap;

    const-string/jumbo v1, "ClientLocation"

    const-string/jumbo v2, "${location}"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->sConstantVariables:Ljava/util/HashMap;

    const-string/jumbo v1, "ClientTime"

    const-string/jumbo v2, "${time}"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->sConstantVariables:Ljava/util/HashMap;

    const-string/jumbo v1, "ClientDate"

    const-string/jumbo v2, "${date}"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static assembleStringFromWords(Lcom/vlingo/common/message/MNode;)Ljava/lang/String;
    .locals 10
    .param p0, "referentNode"    # Lcom/vlingo/common/message/MNode;

    .prologue
    .line 295
    const-string/jumbo v9, "w"

    invoke-virtual {p0, v9}, Lcom/vlingo/common/message/MNode;->findChildrenRecursive(Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    .line 296
    .local v8, "wordNodes":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/common/message/MNode;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 298
    .local v0, "buff":Ljava/lang/StringBuilder;
    const/4 v5, 0x0

    .line 299
    .local v5, "prevWordWasDigit":Z
    const/4 v1, 0x1

    .line 301
    .local v1, "firstWord":Z
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/common/message/MNode;

    .line 302
    .local v7, "wordNode":Lcom/vlingo/common/message/MNode;
    invoke-virtual {v7}, Lcom/vlingo/common/message/MNode;->getTextChildNodeValue()Ljava/lang/String;

    move-result-object v6

    .line 303
    .local v6, "word":Ljava/lang/String;
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_0

    .line 305
    const-string/jumbo v9, "ns"

    invoke-virtual {v7, v9}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 306
    .local v3, "nsAtt":Ljava/lang/String;
    const-string/jumbo v9, "nsd"

    invoke-virtual {v7, v9}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 307
    .local v4, "nsdAtt":Ljava/lang/String;
    if-nez v1, :cond_2

    if-nez v3, :cond_2

    if-eqz v4, :cond_1

    if-nez v5, :cond_2

    .line 310
    :cond_1
    const-string/jumbo v9, " "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    :cond_2
    const/4 v1, 0x0

    .line 314
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-static {v9}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    goto :goto_0

    .line 320
    .end local v3    # "nsAtt":Ljava/lang/String;
    .end local v4    # "nsdAtt":Ljava/lang/String;
    .end local v6    # "word":Ljava/lang/String;
    .end local v7    # "wordNode":Lcom/vlingo/common/message/MNode;
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    return-object v9
.end method

.method public static deepCopyNode(Lcom/vlingo/common/message/MNode;)Lcom/vlingo/common/message/MNode;
    .locals 1
    .param p0, "node"    # Lcom/vlingo/common/message/MNode;

    .prologue
    .line 428
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->deepCopyNodeExceptName(Lcom/vlingo/common/message/MNode;Ljava/lang/String;)Lcom/vlingo/common/message/MNode;

    move-result-object v0

    return-object v0
.end method

.method public static deepCopyNodeExceptName(Lcom/vlingo/common/message/MNode;Ljava/lang/String;)Lcom/vlingo/common/message/MNode;
    .locals 8
    .param p0, "node"    # Lcom/vlingo/common/message/MNode;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 413
    new-instance v5, Lcom/vlingo/common/message/MNode;

    invoke-virtual {p0}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/vlingo/common/message/MNode;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/vlingo/common/message/MNode;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    .local v5, "newNode":Lcom/vlingo/common/message/MNode;
    invoke-virtual {p0}, Lcom/vlingo/common/message/MNode;->getAttributes()Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    .line 416
    .local v2, "entries":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 417
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lcom/vlingo/common/message/MNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 420
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/common/message/MNode;->getChildren()Ljava/util/List;

    move-result-object v1

    .line 421
    .local v1, "children":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/common/message/MNode;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/common/message/MNode;

    .line 422
    .local v0, "child":Lcom/vlingo/common/message/MNode;
    if-eqz p1, :cond_2

    invoke-virtual {v0}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 423
    :cond_2
    invoke-static {v0, p1}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->deepCopyNodeExceptName(Lcom/vlingo/common/message/MNode;Ljava/lang/String;)Lcom/vlingo/common/message/MNode;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vlingo/common/message/MNode;->add(Lcom/vlingo/common/message/MNode;)V

    goto :goto_1

    .line 425
    .end local v0    # "child":Lcom/vlingo/common/message/MNode;
    :cond_3
    return-object v5
.end method

.method public static doesVPathReferentExist(Ljava/lang/String;Lcom/vlingo/common/message/MNode;)Z
    .locals 5
    .param p0, "vpath"    # Ljava/lang/String;
    .param p1, "treeRoot"    # Lcom/vlingo/common/message/MNode;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 275
    invoke-static {p0, p1}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->findNodeFromVPath(Ljava/lang/String;Lcom/vlingo/common/message/MNode;)Lcom/vlingo/common/message/MNode;

    move-result-object v1

    .line 276
    .local v1, "referentNode":Lcom/vlingo/common/message/MNode;
    if-nez v1, :cond_1

    move v2, v3

    .line 284
    :cond_0
    :goto_0
    return v2

    .line 279
    :cond_1
    invoke-static {p0}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->getReferencedAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 280
    .local v0, "attrName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 282
    invoke-virtual {v1, v0}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    move v2, v3

    goto :goto_0
.end method

.method public static findNodeFromVPath(Ljava/lang/String;Lcom/vlingo/common/message/MNode;)Lcom/vlingo/common/message/MNode;
    .locals 6
    .param p0, "vpath"    # Ljava/lang/String;
    .param p1, "treeRoot"    # Lcom/vlingo/common/message/MNode;

    .prologue
    .line 143
    const/4 v1, 0x0

    .line 147
    .local v1, "node":Lcom/vlingo/common/message/MNode;
    const/4 v4, 0x2

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 149
    .local v2, "path":Ljava/lang/String;
    const-string/jumbo v4, "\\."

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 150
    .local v3, "pathSegments":[Ljava/lang/String;
    const/4 v4, 0x0

    invoke-static {v3, v4, p1}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->findNodeFromVPathAux([Ljava/lang/String;ILcom/vlingo/common/message/MNode;)Lcom/vlingo/common/message/MNode;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 157
    .end local v2    # "path":Ljava/lang/String;
    .end local v3    # "pathSegments":[Ljava/lang/String;
    :goto_0
    return-object v1

    .line 152
    :catch_0
    move-exception v0

    .line 154
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    invoke-static {p0}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionError;->getInvalidRecognitionReference(Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Lcom/vlingo/common/log4j/VLogger;->error(Lcom/vlingo/common/message/VMessage;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static findNodeFromVPathAux([Ljava/lang/String;ILcom/vlingo/common/message/MNode;)Lcom/vlingo/common/message/MNode;
    .locals 13
    .param p0, "pathSegments"    # [Ljava/lang/String;
    .param p1, "depth"    # I
    .param p2, "treeRoot"    # Lcom/vlingo/common/message/MNode;

    .prologue
    const/4 v10, 0x0

    const/4 v12, -0x1

    .line 162
    array-length v11, p0

    if-lt p1, v11, :cond_0

    .line 213
    .end local p2    # "treeRoot":Lcom/vlingo/common/message/MNode;
    :goto_0
    return-object p2

    .line 168
    .restart local p2    # "treeRoot":Lcom/vlingo/common/message/MNode;
    :cond_0
    aget-object v7, p0, p1

    .line 173
    .local v7, "pathSegment":Ljava/lang/String;
    const-string/jumbo v11, "["

    invoke-virtual {v7, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    .local v8, "pos":I
    if-eq v8, v12, :cond_2

    .line 176
    const-string/jumbo v11, "]"

    invoke-virtual {v7, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 177
    .local v2, "closePos":I
    add-int/lit8 v11, v8, 0x1

    invoke-virtual {v7, v11, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 178
    .local v4, "index":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 180
    .local v5, "indexVal":I
    invoke-virtual {p2}, Lcom/vlingo/common/message/MNode;->getChildren()Ljava/util/List;

    move-result-object v1

    .line 181
    .local v1, "children":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/common/message/MNode;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v11

    if-lt v5, v11, :cond_1

    move-object p2, v10

    .line 182
    goto :goto_0

    .line 184
    :cond_1
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/common/message/MNode;

    .line 186
    .local v9, "target":Lcom/vlingo/common/message/MNode;
    add-int/lit8 v10, p1, 0x1

    invoke-static {p0, v10, v9}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->findNodeFromVPathAux([Ljava/lang/String;ILcom/vlingo/common/message/MNode;)Lcom/vlingo/common/message/MNode;

    move-result-object p2

    goto :goto_0

    .line 188
    .end local v1    # "children":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/common/message/MNode;>;"
    .end local v2    # "closePos":I
    .end local v4    # "index":Ljava/lang/String;
    .end local v5    # "indexVal":I
    .end local v9    # "target":Lcom/vlingo/common/message/MNode;
    :cond_2
    const-string/jumbo v11, "("

    invoke-virtual {v7, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    if-eq v8, v12, :cond_4

    .line 191
    const-string/jumbo v11, ")"

    invoke-virtual {v7, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 192
    .restart local v2    # "closePos":I
    add-int/lit8 v11, v8, 0x1

    invoke-virtual {v7, v11, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 194
    .restart local v4    # "index":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/vlingo/common/message/MNode;->getChildren()Ljava/util/List;

    move-result-object v1

    .line 195
    .restart local v1    # "children":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/common/message/MNode;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/common/message/MNode;

    .line 197
    .local v0, "child":Lcom/vlingo/common/message/MNode;
    const-string/jumbo v11, "nm"

    invoke-virtual {v0, v11}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 198
    .local v6, "name":Ljava/lang/String;
    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 201
    add-int/lit8 v10, p1, 0x1

    invoke-static {p0, v10, v0}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->findNodeFromVPathAux([Ljava/lang/String;ILcom/vlingo/common/message/MNode;)Lcom/vlingo/common/message/MNode;

    move-result-object p2

    goto :goto_0

    .line 207
    .end local v0    # "child":Lcom/vlingo/common/message/MNode;
    .end local v1    # "children":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/common/message/MNode;>;"
    .end local v2    # "closePos":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "index":Ljava/lang/String;
    .end local v6    # "name":Ljava/lang/String;
    :cond_4
    invoke-virtual {p2, v7}, Lcom/vlingo/common/message/MNode;->findFirstChild(Ljava/lang/String;)Lcom/vlingo/common/message/MNode;

    move-result-object v9

    .line 208
    .restart local v9    # "target":Lcom/vlingo/common/message/MNode;
    if-eqz v9, :cond_5

    .line 210
    add-int/lit8 v10, p1, 0x1

    invoke-static {p0, v10, v9}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->findNodeFromVPathAux([Ljava/lang/String;ILcom/vlingo/common/message/MNode;)Lcom/vlingo/common/message/MNode;

    move-result-object p2

    goto :goto_0

    .end local v9    # "target":Lcom/vlingo/common/message/MNode;
    :cond_5
    move-object p2, v10

    .line 213
    goto/16 :goto_0
.end method

.method public static getReferencedAttribute(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "vpath"    # Ljava/lang/String;

    .prologue
    .line 223
    const/16 v1, 0x3a

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 224
    .local v0, "index":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v1, v0

    const/4 v2, 0x3

    if-ge v1, v2, :cond_1

    .line 225
    :cond_0
    const/4 v1, 0x0

    .line 227
    :goto_0
    return-object v1

    :cond_1
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getResourceValue(Lcom/vlingo/voicepad/config/ResourceManager;Ljava/lang/String;Lcom/vlingo/workflow/task/TaskEnv;)Ljava/lang/String;
    .locals 10
    .param p0, "resourceManager"    # Lcom/vlingo/voicepad/config/ResourceManager;
    .param p1, "resourceVariable"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/voicepad/config/ResourceManager;",
            "Ljava/lang/String;",
            "Lcom/vlingo/workflow/task/TaskEnv",
            "<",
            "Lcom/vlingo/voicepad/vvs/VVEnv;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 378
    .local p2, "taskEnv":Lcom/vlingo/workflow/task/TaskEnv;, "Lcom/vlingo/workflow/task/TaskEnv<Lcom/vlingo/voicepad/vvs/VVEnv;>;"
    invoke-virtual {p2}, Lcom/vlingo/workflow/task/TaskEnv;->getEnv()Lcom/vlingo/workflow/ServletEnvironmentInterface;

    move-result-object v7

    check-cast v7, Lcom/vlingo/voicepad/vvs/VVEnv;

    invoke-virtual {v7}, Lcom/vlingo/voicepad/vvs/VVEnv;->getBaseServletEnv()Lcom/vlingo/servlet/util/BaseServletEnv;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/servlet/util/BaseServletEnv;->getActiveLocale()Ljava/util/Locale;

    move-result-object v3

    .line 379
    .local v3, "locale":Ljava/util/Locale;
    const/4 v5, 0x0

    .line 380
    .local v5, "ret":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/vlingo/workflow/task/TaskEnv;->getEnv()Lcom/vlingo/workflow/ServletEnvironmentInterface;

    move-result-object v7

    check-cast v7, Lcom/vlingo/voicepad/vvs/VVEnv;

    invoke-virtual {v7}, Lcom/vlingo/voicepad/vvs/VVEnv;->getServletEnv()Lcom/vlingo/servlet/util/VLServletEnv;

    move-result-object v7

    invoke-interface {v7}, Lcom/vlingo/servlet/util/VLServletEnv;->getSoftwareMeta()Lcom/vlingo/message/model/request/SoftwareMeta;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/message/model/request/SoftwareMeta;->getAppChannel()Ljava/lang/String;

    move-result-object v0

    .line 381
    .local v0, "appChannel":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/vlingo/workflow/task/TaskEnv;->getEnv()Lcom/vlingo/workflow/ServletEnvironmentInterface;

    move-result-object v7

    check-cast v7, Lcom/vlingo/voicepad/vvs/VVEnv;

    invoke-virtual {v7}, Lcom/vlingo/voicepad/vvs/VVEnv;->getServletEnv()Lcom/vlingo/servlet/util/VLServletEnv;

    move-result-object v7

    invoke-interface {v7}, Lcom/vlingo/servlet/util/VLServletEnv;->getSoftwareMeta()Lcom/vlingo/message/model/request/SoftwareMeta;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/message/model/request/SoftwareMeta;->getName()Ljava/lang/String;

    move-result-object v6

    .line 382
    .local v6, "softwareName":Ljava/lang/String;
    const/4 v7, 0x6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {p1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 383
    .local v2, "key":Ljava/lang/String;
    const-string/jumbo v7, "TagToActionMessage"

    invoke-virtual {p0, v7, v3}, Lcom/vlingo/voicepad/config/ResourceManager;->getBundle(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/ResourceBundle;

    move-result-object v4

    .line 385
    .local v4, "resourceBundle":Ljava/util/ResourceBundle;
    :try_start_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 386
    sget-object v7, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v7}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v7

    if-eqz v7, :cond_0

    sget-object v7, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "*** "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 390
    :cond_0
    :goto_0
    if-eqz v5, :cond_1

    move-object v7, v5

    .line 402
    :goto_1
    return-object v7

    .line 387
    :catch_0
    move-exception v1

    .line 388
    .local v1, "ex":Ljava/util/MissingResourceException;
    sget-object v7, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v7}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v7

    if-eqz v7, :cond_0

    sget-object v7, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "No resource key:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    goto :goto_0

    .line 393
    .end local v1    # "ex":Ljava/util/MissingResourceException;
    :cond_1
    :try_start_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 394
    sget-object v7, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v7}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v7

    if-eqz v7, :cond_2

    sget-object v7, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "*** "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/util/MissingResourceException; {:try_start_1 .. :try_end_1} :catch_1

    .line 399
    :cond_2
    :goto_2
    if-eqz v5, :cond_3

    move-object v7, v5

    .line 400
    goto/16 :goto_1

    .line 395
    :catch_1
    move-exception v1

    .line 396
    .restart local v1    # "ex":Ljava/util/MissingResourceException;
    sget-object v7, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v7}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v7

    if-eqz v7, :cond_2

    sget-object v7, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "No resource key:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    goto :goto_2

    .line 402
    .end local v1    # "ex":Ljava/util/MissingResourceException;
    :cond_3
    invoke-virtual {v4, v2}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_1
.end method

.method public static getResourceValueForLocale(Lcom/vlingo/voicepad/config/ResourceManager;Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;
    .locals 4
    .param p0, "resourceManager"    # Lcom/vlingo/voicepad/config/ResourceManager;
    .param p1, "resourceVariable"    # Ljava/lang/String;
    .param p2, "locale"    # Ljava/util/Locale;

    .prologue
    .line 406
    const/4 v2, 0x6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 407
    .local v0, "keyName":Ljava/lang/String;
    const-string/jumbo v2, "TagToActionMessage"

    invoke-virtual {p0, v2, p2}, Lcom/vlingo/voicepad/config/ResourceManager;->getBundle(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/ResourceBundle;

    move-result-object v1

    .line 408
    .local v1, "resourceBundle":Ljava/util/ResourceBundle;
    invoke-virtual {v1, v0}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getValueFromVPath(Ljava/lang/String;Lcom/vlingo/common/message/MNode;)Ljava/lang/String;
    .locals 11
    .param p0, "vpath"    # Ljava/lang/String;
    .param p1, "treeRoot"    # Lcom/vlingo/common/message/MNode;

    .prologue
    .line 238
    invoke-static {p0, p1}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->findNodeFromVPath(Ljava/lang/String;Lcom/vlingo/common/message/MNode;)Lcom/vlingo/common/message/MNode;

    move-result-object v7

    .line 239
    .local v7, "referentNode":Lcom/vlingo/common/message/MNode;
    if-nez v7, :cond_1

    .line 240
    const/4 v3, 0x0

    .line 264
    :cond_0
    :goto_0
    return-object v3

    .line 243
    :cond_1
    invoke-static {p0}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->getReferencedAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 244
    .local v2, "attrNames":Ljava/lang/String;
    if-eqz v2, :cond_3

    .line 246
    const-string/jumbo v9, "?:"

    invoke-virtual {p0, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    const/4 v10, -0x1

    if-eq v9, v10, :cond_2

    const/4 v6, 0x1

    .line 247
    .local v6, "optional":Z
    :goto_1
    const-string/jumbo v9, ","

    invoke-virtual {v2, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_2
    if-ge v4, v5, :cond_3

    aget-object v1, v0, v4

    .line 250
    .local v1, "attrName":Ljava/lang/String;
    invoke-virtual {v7, v1}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 251
    .local v3, "attributeValue":Ljava/lang/String;
    if-nez v3, :cond_0

    if-eqz v6, :cond_0

    .line 247
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 246
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "attrName":Ljava/lang/String;
    .end local v3    # "attributeValue":Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "optional":Z
    :cond_2
    const/4 v6, 0x0

    goto :goto_1

    .line 259
    :cond_3
    invoke-virtual {v7}, Lcom/vlingo/common/message/MNode;->getTextChildNodeValue()Ljava/lang/String;

    move-result-object v8

    .line 260
    .local v8, "value":Ljava/lang/String;
    if-eqz v8, :cond_4

    move-object v3, v8

    .line 261
    goto :goto_0

    .line 264
    :cond_4
    invoke-static {v7}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->assembleStringFromWords(Lcom/vlingo/common/message/MNode;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static getVariableValue(Ljava/lang/String;Lcom/vlingo/common/message/MNode;)Ljava/lang/String;
    .locals 8
    .param p0, "variableName"    # Ljava/lang/String;
    .param p1, "asrResponseRoot"    # Lcom/vlingo/common/message/MNode;

    .prologue
    .line 325
    const/4 v5, 0x0

    .line 326
    .local v5, "urlEncode":Z
    const/4 v3, -0x1

    .line 327
    .local v3, "pos":I
    const/4 v1, 0x0

    .line 330
    .local v1, "nullAllowable":Z
    const-string/jumbo v6, "@%"

    invoke-virtual {p0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 331
    const/4 v5, 0x1

    .line 332
    const/4 v3, 0x2

    .line 339
    :cond_0
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-lt v3, v6, :cond_3

    .line 340
    new-instance v6, Lcom/vlingo/common/message/MessageException;

    invoke-static {p0}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionError;->getInvalidVariableReference(Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/vlingo/common/message/MessageException;-><init>(Lcom/vlingo/common/message/VMessage;)V

    throw v6

    .line 333
    :cond_1
    const-string/jumbo v6, "@?"

    invoke-virtual {p0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 334
    const/4 v1, 0x1

    .line 335
    const/4 v3, 0x2

    goto :goto_0

    .line 336
    :cond_2
    const-string/jumbo v6, "@"

    invoke-virtual {p0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 337
    const/4 v3, 0x1

    goto :goto_0

    .line 342
    :cond_3
    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 344
    const/4 v4, 0x0

    .line 345
    .local v4, "replaceValue":Ljava/lang/String;
    sget-object v6, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->sConstantVariables:Ljava/util/HashMap;

    invoke-virtual {v6, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 346
    sget-object v6, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->sConstantVariables:Ljava/util/HashMap;

    invoke-virtual {v6, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 374
    :goto_1
    return-object v6

    .line 349
    :cond_4
    const-string/jumbo v6, "ParseType"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 352
    const-string/jumbo v6, "${Alternates.T.pg[0]}"

    invoke-static {v6, p1}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->findNodeFromVPath(Ljava/lang/String;Lcom/vlingo/common/message/MNode;)Lcom/vlingo/common/message/MNode;

    move-result-object v2

    .line 353
    .local v2, "pg":Lcom/vlingo/common/message/MNode;
    const-string/jumbo v6, "t"

    invoke-virtual {v2, v6}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 361
    .end local v2    # "pg":Lcom/vlingo/common/message/MNode;
    :cond_5
    :goto_2
    if-nez v4, :cond_8

    .line 362
    if-eqz v1, :cond_7

    const/4 v6, 0x0

    goto :goto_1

    .line 355
    :cond_6
    invoke-static {p0}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->isVPath(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 358
    invoke-static {p0, p1}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->getValueFromVPath(Ljava/lang/String;Lcom/vlingo/common/message/MNode;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 363
    :cond_7
    const-string/jumbo v4, ""

    .line 366
    :cond_8
    if-eqz v5, :cond_9

    .line 368
    :try_start_0
    const-string/jumbo v6, "UTF-8"

    invoke-static {v4, v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    :cond_9
    :goto_3
    move-object v6, v4

    .line 374
    goto :goto_1

    .line 369
    :catch_0
    move-exception v0

    .line 370
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v6, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v6, v0}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    goto :goto_3
.end method

.method public static isResource(Ljava/lang/String;)Z
    .locals 1
    .param p0, "variableName"    # Ljava/lang/String;

    .prologue
    .line 86
    const-string/jumbo v0, "${res:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isVPath(Ljava/lang/String;)Z
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 68
    const-string/jumbo v0, "${Alternates"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isVariable(Ljava/lang/String;)Z
    .locals 1
    .param p0, "variableName"    # Ljava/lang/String;

    .prologue
    .line 77
    const-string/jumbo v0, "@"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static validateVPath(Ljava/lang/String;)Z
    .locals 5
    .param p0, "vpath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 96
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x7d

    if-eq v3, v4, :cond_0

    .line 104
    :goto_0
    return v2

    .line 101
    :cond_0
    const/4 v3, 0x2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 102
    .local v0, "path":Ljava/lang/String;
    const-string/jumbo v3, "\\."

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 104
    .local v1, "pathSegments":[Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->validateVPathAux([Ljava/lang/String;I)Z

    move-result v2

    goto :goto_0
.end method

.method static validateVPathAux([Ljava/lang/String;I)Z
    .locals 5
    .param p0, "pathSegments"    # [Ljava/lang/String;
    .param p1, "depth"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v4, -0x1

    .line 109
    array-length v3, p0

    if-lt p1, v3, :cond_1

    .line 111
    const/4 v2, 0x1

    .line 131
    :cond_0
    :goto_0
    return v2

    .line 114
    :cond_1
    aget-object v0, p0, p1

    .line 117
    .local v0, "pathSegment":Ljava/lang/String;
    const-string/jumbo v3, "["

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .local v1, "pos":I
    if-eq v1, v4, :cond_3

    .line 119
    const/16 v3, 0x5d

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-le v3, v1, :cond_0

    .line 131
    :cond_2
    add-int/lit8 v2, p1, 0x1

    invoke-static {p0, v2}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->validateVPathAux([Ljava/lang/String;I)Z

    move-result v2

    goto :goto_0

    .line 124
    :cond_3
    const-string/jumbo v3, "("

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v4, :cond_2

    .line 126
    const/16 v3, 0x29

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-gt v3, v1, :cond_2

    goto :goto_0
.end method
