.class public Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;
.super Ljava/lang/Object;
.source "TagToActionGenerator.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final PARAM_VALUE_PATTERN:Ljava/util/regex/Pattern;

.field public static final TAGTOACTION_CONFIG_FILE:Ljava/lang/String; = "TagToAction.main.xml"

.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;

.field private static final serialVersionUID:J = -0x15c7c83663456dadL


# instance fields
.field private ivFeatureSetMgr:Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;

.field private transient ivResourceManager:Lcom/vlingo/voicepad/config/ResourceManager;

.field private volatile ivVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    .line 59
    const-string/jumbo v0, "((@[%\\?]?)?\\$\\{[^}]+\\}|@[%\\?]?\\w+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->PARAM_VALUE_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method private constructor <init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;Lcom/vlingo/voicepad/config/ResourceManager;)V
    .locals 2
    .param p1, "loader"    # Lcom/vlingo/mda/cfgloader/ConfigLoader;
    .param p2, "resourceManager"    # Lcom/vlingo/voicepad/config/ResourceManager;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;
        }
    .end annotation

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    const-string/jumbo v0, "unknown"

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->ivVersion:Ljava/lang/String;

    .line 83
    iput-object p2, p0, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->ivResourceManager:Lcom/vlingo/voicepad/config/ResourceManager;

    .line 85
    :try_start_0
    invoke-direct {p0, p1}, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->init(Lcom/vlingo/mda/cfgloader/ConfigLoader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    :try_start_1
    invoke-interface {p1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 93
    :goto_0
    return-void

    .line 87
    :catchall_0
    move-exception v0

    .line 88
    :try_start_2
    invoke-interface {p1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 91
    :goto_1
    throw v0

    .line 89
    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/io/File;Lcom/vlingo/voicepad/config/ResourceManager;)V
    .locals 2
    .param p1, "config"    # Ljava/io/File;
    .param p2, "resourceManager"    # Lcom/vlingo/voicepad/config/ResourceManager;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;
        }
    .end annotation

    .prologue
    .line 69
    new-instance v0, Lcom/vlingo/mda/cfgloader/JarMDAConfigLoader;

    const-class v1, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionConfig;

    invoke-direct {v0, p1, v1}, Lcom/vlingo/mda/cfgloader/JarMDAConfigLoader;-><init>(Ljava/io/File;Ljava/lang/Class;)V

    invoke-direct {p0, v0, p2}, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;-><init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;Lcom/vlingo/voicepad/config/ResourceManager;)V

    .line 70
    return-void
.end method

.method private buildActionResult(Lcom/vlingo/common/message/MNode;Lcom/vlingo/workflow/task/TaskEnv;ZLjava/util/List;Lcom/vlingo/voicepad/tagtoaction/model/Action;Ljava/util/Map;)V
    .locals 7
    .param p1, "asrResponseRoot"    # Lcom/vlingo/common/message/MNode;
    .param p2, "taskEnv"    # Lcom/vlingo/workflow/task/TaskEnv;
    .param p3, "evaluateAllVPath"    # Z
    .param p5, "action"    # Lcom/vlingo/voicepad/tagtoaction/model/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/common/message/MNode;",
            "Lcom/vlingo/workflow/task/TaskEnv;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/model/VLActionResult;",
            ">;",
            "Lcom/vlingo/voicepad/tagtoaction/model/Action;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 273
    .local p4, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/model/VLActionResult;>;"
    .local p6, "capabilityMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p5, p6}, Lcom/vlingo/voicepad/tagtoaction/model/Action;->evaluateCapability(Ljava/util/Map;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 294
    :goto_0
    return-void

    .line 278
    :cond_0
    new-instance v0, Lcom/vlingo/voicepad/util/ActionResult;

    invoke-direct {v0, p5}, Lcom/vlingo/voicepad/util/ActionResult;-><init>(Lcom/vlingo/voicepad/tagtoaction/model/Action;)V

    .line 280
    .local v0, "actionResult":Lcom/vlingo/voicepad/util/ActionResult;
    sget-object v4, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "generating ActionResult "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/vlingo/voicepad/util/ActionResult;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 283
    invoke-virtual {p5}, Lcom/vlingo/voicepad/tagtoaction/model/Action;->getParams()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;

    .line 284
    .local v3, "param":Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;
    invoke-virtual {v3}, Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4, p1, p2, p3}, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->validateAndReplaceVariables(Ljava/lang/String;Lcom/vlingo/common/message/MNode;Lcom/vlingo/workflow/task/TaskEnv;Z)Ljava/lang/String;

    move-result-object v1

    .line 285
    .local v1, "finalParamValue":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 290
    invoke-virtual {v3}, Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4, v1}, Lcom/vlingo/voicepad/util/ActionResult;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 293
    .end local v1    # "finalParamValue":Ljava/lang/String;
    .end local v3    # "param":Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;
    :cond_2
    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private findTopParseGroup(Lcom/vlingo/common/message/MNode;)Lcom/vlingo/common/message/MNode;
    .locals 2
    .param p1, "asrResponseRoot"    # Lcom/vlingo/common/message/MNode;

    .prologue
    .line 241
    const-string/jumbo v1, "pg"

    invoke-virtual {p1, v1}, Lcom/vlingo/common/message/MNode;->findChildrenRecursive(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 242
    .local v0, "pgs":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/common/message/MNode;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 244
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/common/message/MNode;

    .line 246
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private generateActionForMessage(Lcom/vlingo/common/message/MNode;)Lcom/vlingo/voicepad/util/ActionResult;
    .locals 9
    .param p1, "messageRoot"    # Lcom/vlingo/common/message/MNode;

    .prologue
    .line 176
    new-instance v0, Lcom/vlingo/voicepad/util/ActionResult;

    const-string/jumbo v7, "ShowMessage"

    invoke-direct {v0, v7}, Lcom/vlingo/voicepad/util/ActionResult;-><init>(Ljava/lang/String;)V

    .line 177
    .local v0, "action":Lcom/vlingo/voicepad/util/ActionResult;
    invoke-virtual {p1}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "Error"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 179
    const-string/jumbo v7, "Type"

    const-string/jumbo v8, "error"

    invoke-virtual {v0, v7, v8}, Lcom/vlingo/voicepad/util/ActionResult;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    :cond_0
    :goto_0
    const-string/jumbo v7, "Message"

    invoke-virtual {p1, v7}, Lcom/vlingo/common/message/MNode;->findFirstChild(Ljava/lang/String;)Lcom/vlingo/common/message/MNode;

    move-result-object v6

    .line 187
    .local v6, "msgTextRoot":Lcom/vlingo/common/message/MNode;
    const-string/jumbo v7, "Message"

    invoke-virtual {v6}, Lcom/vlingo/common/message/MNode;->getTextChildNodeValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Lcom/vlingo/voicepad/util/ActionResult;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const-string/jumbo v7, "Code"

    invoke-virtual {p1, v7}, Lcom/vlingo/common/message/MNode;->findFirstChild(Ljava/lang/String;)Lcom/vlingo/common/message/MNode;

    move-result-object v2

    .line 190
    .local v2, "codeTextRoot":Lcom/vlingo/common/message/MNode;
    if-eqz v2, :cond_1

    .line 192
    const-string/jumbo v7, "Code"

    invoke-virtual {v2}, Lcom/vlingo/common/message/MNode;->getTextChildNodeValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Lcom/vlingo/voicepad/util/ActionResult;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :cond_1
    const-string/jumbo v7, "Details"

    invoke-virtual {p1, v7}, Lcom/vlingo/common/message/MNode;->findFirstChild(Ljava/lang/String;)Lcom/vlingo/common/message/MNode;

    move-result-object v4

    .line 196
    .local v4, "detailsRoot":Lcom/vlingo/common/message/MNode;
    if-eqz v4, :cond_2

    .line 198
    const-string/jumbo v7, "Code"

    invoke-virtual {v4, v7}, Lcom/vlingo/common/message/MNode;->findFirstChild(Ljava/lang/String;)Lcom/vlingo/common/message/MNode;

    move-result-object v1

    .line 199
    .local v1, "code":Lcom/vlingo/common/message/MNode;
    const-string/jumbo v7, "Message"

    invoke-virtual {v4, v7}, Lcom/vlingo/common/message/MNode;->findFirstChild(Ljava/lang/String;)Lcom/vlingo/common/message/MNode;

    move-result-object v5

    .line 200
    .local v5, "msg":Lcom/vlingo/common/message/MNode;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/vlingo/common/message/MNode;->getTextChildNodeValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ": "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/vlingo/common/message/MNode;->getTextChildNodeValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 201
    .local v3, "details":Ljava/lang/String;
    const-string/jumbo v7, "Details"

    invoke-virtual {v0, v7, v3}, Lcom/vlingo/voicepad/util/ActionResult;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    .end local v1    # "code":Lcom/vlingo/common/message/MNode;
    .end local v3    # "details":Ljava/lang/String;
    .end local v5    # "msg":Lcom/vlingo/common/message/MNode;
    :cond_2
    return-object v0

    .line 181
    .end local v2    # "codeTextRoot":Lcom/vlingo/common/message/MNode;
    .end local v4    # "detailsRoot":Lcom/vlingo/common/message/MNode;
    .end local v6    # "msgTextRoot":Lcom/vlingo/common/message/MNode;
    :cond_3
    invoke-virtual {p1}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "Warning"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 183
    const-string/jumbo v7, "Type"

    const-string/jumbo v8, "warning"

    invoke-virtual {v0, v7, v8}, Lcom/vlingo/voicepad/util/ActionResult;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private generateActionsForFeature(Lcom/vlingo/voicepad/tagtoaction/model/Feature;Lcom/vlingo/common/message/MNode;Lcom/vlingo/workflow/task/TaskEnv;ZLjava/util/Map;)Ljava/util/List;
    .locals 18
    .param p1, "feature"    # Lcom/vlingo/voicepad/tagtoaction/model/Feature;
    .param p2, "asrResponseRoot"    # Lcom/vlingo/common/message/MNode;
    .param p3, "taskEnv"    # Lcom/vlingo/workflow/task/TaskEnv;
    .param p4, "evaluateAllVPath"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/voicepad/tagtoaction/model/Feature;",
            "Lcom/vlingo/common/message/MNode;",
            "Lcom/vlingo/workflow/task/TaskEnv;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/model/VLActionResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251
    .local p5, "capabilityMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 253
    .local v5, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/model/VLActionResult;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getActions()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_0
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/vlingo/voicepad/tagtoaction/model/Action;

    .line 254
    .local v12, "action":Lcom/vlingo/voicepad/tagtoaction/model/Action;
    instance-of v1, v12, Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplateRef;

    if-eqz v1, :cond_1

    .line 256
    invoke-virtual {v12}, Lcom/vlingo/voicepad/tagtoaction/model/Action;->getName()Ljava/lang/String;

    move-result-object v17

    .line 257
    .local v17, "templateName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->ivFeatureSetMgr:Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->getActionTemplateByName(Ljava/lang/String;)Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;

    move-result-object v16

    .line 259
    .local v16, "template":Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;->getActions()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/voicepad/tagtoaction/model/Action;

    .local v6, "templateAction":Lcom/vlingo/voicepad/tagtoaction/model/Action;
    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move-object/from16 v7, p5

    .line 260
    invoke-direct/range {v1 .. v7}, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->buildActionResult(Lcom/vlingo/common/message/MNode;Lcom/vlingo/workflow/task/TaskEnv;ZLjava/util/List;Lcom/vlingo/voicepad/tagtoaction/model/Action;Ljava/util/Map;)V

    goto :goto_1

    .end local v6    # "templateAction":Lcom/vlingo/voicepad/tagtoaction/model/Action;
    .end local v15    # "i$":Ljava/util/Iterator;
    .end local v16    # "template":Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;
    .end local v17    # "templateName":Ljava/lang/String;
    :cond_1
    move-object/from16 v7, p0

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move/from16 v10, p4

    move-object v11, v5

    move-object/from16 v13, p5

    .line 264
    invoke-direct/range {v7 .. v13}, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->buildActionResult(Lcom/vlingo/common/message/MNode;Lcom/vlingo/workflow/task/TaskEnv;ZLjava/util/List;Lcom/vlingo/voicepad/tagtoaction/model/Action;Ljava/util/Map;)V

    goto :goto_0

    .line 268
    .end local v12    # "action":Lcom/vlingo/voicepad/tagtoaction/model/Action;
    :cond_2
    return-object v5
.end method

.method private generateActionsForMessages(Lcom/vlingo/common/message/MNode;)Ljava/util/List;
    .locals 9
    .param p1, "asrResponseRoot"    # Lcom/vlingo/common/message/MNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/common/message/MNode;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/model/VLActionResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 149
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 151
    .local v1, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/model/VLActionResult;>;"
    invoke-virtual {p1}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "Error"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 153
    invoke-direct {p0, p1}, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->generateActionForMessage(Lcom/vlingo/common/message/MNode;)Lcom/vlingo/voicepad/util/ActionResult;

    move-result-object v0

    .line 154
    .local v0, "action":Lcom/vlingo/voicepad/util/ActionResult;
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    .end local v0    # "action":Lcom/vlingo/voicepad/util/ActionResult;
    :cond_0
    const-string/jumbo v7, "Error"

    invoke-virtual {p1, v7}, Lcom/vlingo/common/message/MNode;->findChildrenRecursive(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 158
    .local v3, "errors":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/common/message/MNode;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/common/message/MNode;

    .line 160
    .local v2, "error":Lcom/vlingo/common/message/MNode;
    invoke-direct {p0, v2}, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->generateActionForMessage(Lcom/vlingo/common/message/MNode;)Lcom/vlingo/voicepad/util/ActionResult;

    move-result-object v0

    .line 161
    .restart local v0    # "action":Lcom/vlingo/voicepad/util/ActionResult;
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 164
    .end local v0    # "action":Lcom/vlingo/voicepad/util/ActionResult;
    .end local v2    # "error":Lcom/vlingo/common/message/MNode;
    :cond_1
    const-string/jumbo v7, "Warning"

    invoke-virtual {p1, v7}, Lcom/vlingo/common/message/MNode;->findChildrenRecursive(Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    .line 165
    .local v6, "warnings":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/common/message/MNode;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/common/message/MNode;

    .line 167
    .local v5, "warning":Lcom/vlingo/common/message/MNode;
    invoke-direct {p0, v5}, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->generateActionForMessage(Lcom/vlingo/common/message/MNode;)Lcom/vlingo/voicepad/util/ActionResult;

    move-result-object v0

    .line 168
    .restart local v0    # "action":Lcom/vlingo/voicepad/util/ActionResult;
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 171
    .end local v0    # "action":Lcom/vlingo/voicepad/util/ActionResult;
    .end local v5    # "warning":Lcom/vlingo/common/message/MNode;
    :cond_2
    return-object v1
.end method

.method private generateActionsForTags(Lcom/vlingo/workflow/task/TaskEnv;Lcom/vlingo/common/message/MNode;Lcom/vlingo/common/message/MNode;Lcom/vlingo/voicepad/model/LicenseInfo;ZLjava/util/Map;)Ljava/util/List;
    .locals 13
    .param p1, "taskEnv"    # Lcom/vlingo/workflow/task/TaskEnv;
    .param p2, "requestMetaRoot"    # Lcom/vlingo/common/message/MNode;
    .param p3, "asrResponseRoot"    # Lcom/vlingo/common/message/MNode;
    .param p4, "license"    # Lcom/vlingo/voicepad/model/LicenseInfo;
    .param p5, "evaluateAllVPath"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/workflow/task/TaskEnv;",
            "Lcom/vlingo/common/message/MNode;",
            "Lcom/vlingo/common/message/MNode;",
            "Lcom/vlingo/voicepad/model/LicenseInfo;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/model/VLActionResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209
    .local p6, "capabilityMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/vlingo/workflow/task/TaskEnv;->getRequest()Lcom/vlingo/workflow/model/VRequest;

    move-result-object v5

    const-string/jumbo v7, "x-vlclient"

    const-string/jumbo v8, "DeviceOSName"

    invoke-interface {v5, v7, v8}, Lcom/vlingo/workflow/model/VRequest;->parseVLHeaderField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 211
    .local v11, "platform":Ljava/lang/String;
    iget-object v5, p0, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->ivFeatureSetMgr:Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;

    invoke-virtual {p1}, Lcom/vlingo/workflow/task/TaskEnv;->getRequest()Lcom/vlingo/workflow/model/VRequest;

    move-result-object v7

    move-object/from16 v0, p4

    invoke-virtual {v5, v0, v7}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->findFeatureSetForRequest(Lcom/vlingo/voicepad/model/LicenseInfo;Lcom/vlingo/workflow/model/VRequest;)Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;

    move-result-object v1

    .line 212
    .local v1, "featureSet":Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    if-nez v1, :cond_0

    .line 215
    new-instance v5, Lcom/vlingo/common/message/MessageException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/voicepad/model/LicenseInfo;->getLicenseClass()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/voicepad/model/LicenseInfo;->getFeatureLevel()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v11, v7}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionError;->getNoMatchingFeatureMap(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;

    move-result-object v7

    invoke-direct {v5, v7}, Lcom/vlingo/common/message/MessageException;-><init>(Lcom/vlingo/common/message/VMessage;)V

    throw v5

    .line 218
    :cond_0
    move-object/from16 v0, p3

    invoke-direct {p0, v0}, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->findTopParseGroup(Lcom/vlingo/common/message/MNode;)Lcom/vlingo/common/message/MNode;

    move-result-object v12

    .line 219
    .local v12, "topParseGroup":Lcom/vlingo/common/message/MNode;
    if-nez v12, :cond_1

    .line 222
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 236
    :goto_0
    return-object v5

    .line 226
    :cond_1
    const-string/jumbo v5, "t"

    invoke-virtual {v12, v5}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 227
    .local v2, "responseParseType":Ljava/lang/String;
    const-string/jumbo v5, "FieldID"

    invoke-direct {p0, p2, v5}, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->getAttrFromReqMeta(Lcom/vlingo/common/message/MNode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 228
    .local v3, "requestFieldID":Ljava/lang/String;
    const-string/jumbo v5, "FieldContext"

    invoke-direct {p0, p2, v5}, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->getAttrFromReqMeta(Lcom/vlingo/common/message/MNode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 229
    .local v4, "requestFieldContext":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vlingo/workflow/task/TaskEnv;->getRequest()Lcom/vlingo/workflow/model/VRequest;

    move-result-object v6

    move-object/from16 v5, p4

    move-object/from16 v7, p3

    move-object/from16 v8, p6

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->findFirstMatchingFeature(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/voicepad/model/LicenseInfo;Lcom/vlingo/workflow/model/VRequest;Lcom/vlingo/common/message/MNode;Ljava/util/Map;)Lcom/vlingo/voicepad/tagtoaction/model/Feature;

    move-result-object v6

    .line 230
    .local v6, "featureMatch":Lcom/vlingo/voicepad/tagtoaction/model/Feature;
    if-nez v6, :cond_2

    .line 233
    new-instance v5, Lcom/vlingo/common/message/MessageException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/voicepad/model/LicenseInfo;->getLicenseClass()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/voicepad/model/LicenseInfo;->getFeatureLevel()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v11, v7, v2}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionError;->getNoMatchingFeature(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;

    move-result-object v7

    invoke-direct {v5, v7}, Lcom/vlingo/common/message/MessageException;-><init>(Lcom/vlingo/common/message/VMessage;)V

    throw v5

    :cond_2
    move-object v5, p0

    move-object/from16 v7, p3

    move-object v8, p1

    move/from16 v9, p5

    move-object/from16 v10, p6

    .line 236
    invoke-direct/range {v5 .. v10}, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->generateActionsForFeature(Lcom/vlingo/voicepad/tagtoaction/model/Feature;Lcom/vlingo/common/message/MNode;Lcom/vlingo/workflow/task/TaskEnv;ZLjava/util/Map;)Ljava/util/List;

    move-result-object v5

    goto :goto_0
.end method

.method private getAttrFromReqMeta(Lcom/vlingo/common/message/MNode;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "requestMeta"    # Lcom/vlingo/common/message/MNode;
    .param p2, "attr"    # Ljava/lang/String;

    .prologue
    .line 347
    if-nez p1, :cond_0

    .line 348
    const-string/jumbo v0, ""

    .line 349
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1, p2}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private init(Lcom/vlingo/mda/cfgloader/ConfigLoader;)V
    .locals 3
    .param p1, "loader"    # Lcom/vlingo/mda/cfgloader/ConfigLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;
        }
    .end annotation

    .prologue
    .line 105
    invoke-interface {p1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->getVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->ivVersion:Ljava/lang/String;

    .line 106
    new-instance v0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;

    const-string/jumbo v1, "TagToAction.main.xml"

    iget-object v2, p0, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->ivResourceManager:Lcom/vlingo/voicepad/config/ResourceManager;

    invoke-direct {v0, p1, v1, v2}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;-><init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;Lcom/vlingo/voicepad/config/ResourceManager;)V

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->ivFeatureSetMgr:Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;

    .line 107
    sget-object v0, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Initialized TagToActionGenerator from configuration version: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->ivVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 108
    return-void
.end method

.method public static loadFromDirectory(Ljava/io/File;Lcom/vlingo/voicepad/config/ResourceManager;)Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;
    .locals 3
    .param p0, "dir"    # Ljava/io/File;
    .param p1, "resourceManager"    # Lcom/vlingo/voicepad/config/ResourceManager;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;
        }
    .end annotation

    .prologue
    .line 79
    new-instance v0, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;

    new-instance v1, Lcom/vlingo/mda/cfgloader/DirectoryMDAConfigLoader;

    const-class v2, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionConfig;

    invoke-direct {v1, p0, v2}, Lcom/vlingo/mda/cfgloader/DirectoryMDAConfigLoader;-><init>(Ljava/io/File;Ljava/lang/Class;)V

    invoke-direct {v0, v1, p1}, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;-><init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;Lcom/vlingo/voicepad/config/ResourceManager;)V

    return-object v0
.end method

.method public static loadFromJar(Ljava/io/File;Lcom/vlingo/voicepad/config/ResourceManager;)Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;
    .locals 3
    .param p0, "config"    # Ljava/io/File;
    .param p1, "resourceManager"    # Lcom/vlingo/voicepad/config/ResourceManager;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;
        }
    .end annotation

    .prologue
    .line 74
    new-instance v0, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;

    new-instance v1, Lcom/vlingo/mda/cfgloader/JarMDAConfigLoader;

    const-class v2, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionConfig;

    invoke-direct {v1, p0, v2}, Lcom/vlingo/mda/cfgloader/JarMDAConfigLoader;-><init>(Ljava/io/File;Ljava/lang/Class;)V

    invoke-direct {v0, v1, p1}, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;-><init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;Lcom/vlingo/voicepad/config/ResourceManager;)V

    return-object v0
.end method

.method private validateAndReplaceVariables(Ljava/lang/String;Lcom/vlingo/common/message/MNode;Lcom/vlingo/workflow/task/TaskEnv;Z)Ljava/lang/String;
    .locals 6
    .param p1, "paramValue"    # Ljava/lang/String;
    .param p2, "asrResponseRoot"    # Lcom/vlingo/common/message/MNode;
    .param p3, "env"    # Lcom/vlingo/workflow/task/TaskEnv;
    .param p4, "evaluateAllVPath"    # Z

    .prologue
    const/4 v4, 0x0

    .line 297
    sget-object v5, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->PARAM_VALUE_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v5, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 298
    .local v0, "matcher":Ljava/util/regex/Matcher;
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 301
    .local v1, "replacedParamValue":Ljava/lang/StringBuffer;
    :goto_0
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 302
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v2

    .line 305
    .local v2, "var":Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->isVPath(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 308
    invoke-static {v2, p2}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->doesVPathReferentExist(Ljava/lang/String;Lcom/vlingo/common/message/MNode;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 342
    .end local v2    # "var":Ljava/lang/String;
    :cond_0
    :goto_1
    return-object v4

    .line 311
    .restart local v2    # "var":Ljava/lang/String;
    :cond_1
    if-eqz p4, :cond_3

    .line 313
    invoke-static {v2, p2}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->getValueFromVPath(Ljava/lang/String;Lcom/vlingo/common/message/MNode;)Ljava/lang/String;

    move-result-object v3

    .line 337
    .local v3, "varValue":Ljava/lang/String;
    :cond_2
    :goto_2
    if-eqz v3, :cond_0

    .line 338
    invoke-static {v3}, Ljava/util/regex/Matcher;->quoteReplacement(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    goto :goto_0

    .line 316
    .end local v3    # "varValue":Ljava/lang/String;
    :cond_3
    move-object v3, v2

    .restart local v3    # "varValue":Ljava/lang/String;
    goto :goto_2

    .line 319
    .end local v3    # "varValue":Ljava/lang/String;
    :cond_4
    invoke-static {v2}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->isVariable(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 322
    invoke-static {v2, p2}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->getVariableValue(Ljava/lang/String;Lcom/vlingo/common/message/MNode;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "varValue":Ljava/lang/String;
    goto :goto_2

    .line 324
    .end local v3    # "varValue":Ljava/lang/String;
    :cond_5
    invoke-static {v2}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->isResource(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 326
    iget-object v5, p0, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->ivResourceManager:Lcom/vlingo/voicepad/config/ResourceManager;

    invoke-static {v5, v2, p3}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->getResourceValue(Lcom/vlingo/voicepad/config/ResourceManager;Ljava/lang/String;Lcom/vlingo/workflow/task/TaskEnv;)Ljava/lang/String;

    move-result-object v3

    .line 329
    .restart local v3    # "varValue":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 330
    invoke-direct {p0, v3, p2, p3, p4}, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->validateAndReplaceVariables(Ljava/lang/String;Lcom/vlingo/common/message/MNode;Lcom/vlingo/workflow/task/TaskEnv;Z)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 334
    .end local v3    # "varValue":Ljava/lang/String;
    :cond_6
    move-object v3, v2

    .restart local v3    # "varValue":Ljava/lang/String;
    goto :goto_2

    .line 341
    .end local v2    # "var":Ljava/lang/String;
    .end local v3    # "varValue":Ljava/lang/String;
    :cond_7
    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 342
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method


# virtual methods
.method public generateActionList(Lcom/vlingo/workflow/task/TaskEnv;Lcom/vlingo/common/message/MNode;Lcom/vlingo/common/message/MNode;Lcom/vlingo/voicepad/model/LicenseInfo;ZZLjava/util/Map;)Ljava/util/List;
    .locals 10
    .param p2, "requestMetaRoot"    # Lcom/vlingo/common/message/MNode;
    .param p3, "asrResponseRoot"    # Lcom/vlingo/common/message/MNode;
    .param p4, "license"    # Lcom/vlingo/voicepad/model/LicenseInfo;
    .param p5, "evaluateAllVPath"    # Z
    .param p6, "showUserErrors"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/workflow/task/TaskEnv",
            "<",
            "Lcom/vlingo/voicepad/vvs/VVEnv;",
            ">;",
            "Lcom/vlingo/common/message/MNode;",
            "Lcom/vlingo/common/message/MNode;",
            "Lcom/vlingo/voicepad/model/LicenseInfo;",
            "ZZ",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/model/VLActionResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    .local p1, "taskEnv":Lcom/vlingo/workflow/task/TaskEnv;, "Lcom/vlingo/workflow/task/TaskEnv<Lcom/vlingo/voicepad/vvs/VVEnv;>;"
    .local p7, "capabilityMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .local v7, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/model/VLActionResult;>;"
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object/from16 v6, p7

    .line 135
    invoke-direct/range {v0 .. v6}, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->generateActionsForTags(Lcom/vlingo/workflow/task/TaskEnv;Lcom/vlingo/common/message/MNode;Lcom/vlingo/common/message/MNode;Lcom/vlingo/voicepad/model/LicenseInfo;ZLjava/util/Map;)Ljava/util/List;

    move-result-object v9

    .line 136
    .local v9, "tagActions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/model/VLActionResult;>;"
    invoke-interface {v7, v9}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 139
    if-eqz p6, :cond_0

    invoke-virtual {p1}, Lcom/vlingo/workflow/task/TaskEnv;->getRequest()Lcom/vlingo/workflow/model/VRequest;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/voicepad/vvs/VVSRServlet;->isDMRequest(Lcom/vlingo/workflow/model/VRequest;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 140
    invoke-direct {p0, p3}, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->generateActionsForMessages(Lcom/vlingo/common/message/MNode;)Ljava/util/List;

    move-result-object v8

    .line 141
    .local v8, "msgActions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/model/VLActionResult;>;"
    invoke-interface {v7, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 144
    .end local v8    # "msgActions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/model/VLActionResult;>;"
    :cond_0
    return-object v7
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->ivVersion:Ljava/lang/String;

    return-object v0
.end method

.method public setResourceManager(Lcom/vlingo/voicepad/config/ResourceManager;)V
    .locals 2
    .param p1, "resourceManager"    # Lcom/vlingo/voicepad/config/ResourceManager;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->ivResourceManager:Lcom/vlingo/voicepad/config/ResourceManager;

    .line 97
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->ivFeatureSetMgr:Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;

    iget-object v1, p0, Lcom/vlingo/voicepad/tagtoaction/TagToActionGenerator;->ivResourceManager:Lcom/vlingo/voicepad/config/ResourceManager;

    invoke-virtual {v0, v1}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->setResourceManager(Lcom/vlingo/voicepad/config/ResourceManager;)V

    .line 98
    return-void
.end method
