.class public Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;
.super Ljava/lang/Object;
.source "GoalToActionGenerator.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator$GoalToActionFeatureTester;
    }
.end annotation


# static fields
.field public static final GOALTOACTION_CONFIG_FILE:Ljava/lang/String; = "GoalToAction.main.xml"

.field public static final GOALTOACTION_CONFIG_REGEX:Ljava/lang/String; = "GoalToAction\\..*\\.xml"

.field private static final JEXL_PATTERN:Ljava/util/regex/Pattern;

.field private static final engine:Lorg/apache/commons/jexl2/JexlEngine;

.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;

.field private static final serialVersionUID:J = 0x39d527458eee5a69L


# instance fields
.field private ivFeatureSetMgr:Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;

.field private transient ivResourceManager:Lcom/vlingo/voicepad/config/ResourceManager;

.field private ivVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 51
    const-class v0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    .line 53
    const-string/jumbo v0, "\\$\\{jexl:[^\\}]+\\}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->JEXL_PATTERN:Ljava/util/regex/Pattern;

    .line 58
    new-instance v0, Lorg/apache/commons/jexl2/JexlEngine;

    invoke-direct {v0}, Lorg/apache/commons/jexl2/JexlEngine;-><init>()V

    sput-object v0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->engine:Lorg/apache/commons/jexl2/JexlEngine;

    .line 60
    sget-object v0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->engine:Lorg/apache/commons/jexl2/JexlEngine;

    const-string/jumbo v1, "gta"

    const-class v2, Lcom/vlingo/voicepad/tagtoaction/GoalToActionJexlFunctions;

    invoke-static {v1, v2}, Lcom/google/common/collect/ImmutableMap;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/JexlEngine;->setFunctions(Ljava/util/Map;)V

    .line 61
    sget-object v0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->engine:Lorg/apache/commons/jexl2/JexlEngine;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/JexlEngine;->setCache(I)V

    .line 62
    sget-object v0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->engine:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v0, v3}, Lorg/apache/commons/jexl2/JexlEngine;->setSilent(Z)V

    .line 63
    sget-object v0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->engine:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v0, v3}, Lorg/apache/commons/jexl2/JexlEngine;->setLenient(Z)V

    .line 64
    sget-object v0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v1, "GoalToActionGenerator: static initializer"

    invoke-virtual {v0, v1}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 65
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    const-string/jumbo v0, "unknown"

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivVersion:Ljava/lang/String;

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivResourceManager:Lcom/vlingo/voicepad/config/ResourceManager;

    .line 75
    return-void
.end method

.method private constructor <init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;Lcom/vlingo/voicepad/config/ResourceManager;)V
    .locals 2
    .param p1, "loader"    # Lcom/vlingo/mda/cfgloader/ConfigLoader;
    .param p2, "resourceManager"    # Lcom/vlingo/voicepad/config/ResourceManager;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    const-string/jumbo v0, "unknown"

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivVersion:Ljava/lang/String;

    .line 92
    iput-object p2, p0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivResourceManager:Lcom/vlingo/voicepad/config/ResourceManager;

    .line 94
    :try_start_0
    invoke-direct {p0, p1}, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->init(Lcom/vlingo/mda/cfgloader/ConfigLoader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    :try_start_1
    invoke-interface {p1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 102
    :goto_0
    return-void

    .line 96
    :catchall_0
    move-exception v0

    .line 97
    :try_start_2
    invoke-interface {p1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 100
    :goto_1
    throw v0

    .line 98
    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/io/File;Lcom/vlingo/voicepad/config/ResourceManager;)V
    .locals 2
    .param p1, "config"    # Ljava/io/File;
    .param p2, "resourceManager"    # Lcom/vlingo/voicepad/config/ResourceManager;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;
        }
    .end annotation

    .prologue
    .line 79
    new-instance v0, Lcom/vlingo/mda/cfgloader/JarMDAConfigLoader;

    const-class v1, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionConfig;

    invoke-direct {v0, p1, v1}, Lcom/vlingo/mda/cfgloader/JarMDAConfigLoader;-><init>(Ljava/io/File;Ljava/lang/Class;)V

    invoke-direct {p0, v0, p2}, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;-><init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;Lcom/vlingo/voicepad/config/ResourceManager;)V

    .line 80
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;Lcom/vlingo/dialog/goal/model/Goal;ZLjava/util/List;Lcom/vlingo/voicepad/tagtoaction/model/Action;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;
    .param p1, "x1"    # Lcom/vlingo/dialog/goal/model/Goal;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Ljava/util/List;
    .param p4, "x4"    # Lcom/vlingo/voicepad/tagtoaction/model/Action;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->buildActionResult(Lcom/vlingo/dialog/goal/model/Goal;ZLjava/util/List;Lcom/vlingo/voicepad/tagtoaction/model/Action;)V

    return-void
.end method

.method private buildActionResult(Lcom/vlingo/dialog/goal/model/Goal;ZLjava/util/List;Lcom/vlingo/voicepad/tagtoaction/model/Action;)V
    .locals 7
    .param p1, "goal"    # Lcom/vlingo/dialog/goal/model/Goal;
    .param p2, "evaluateAllVPath"    # Z
    .param p4, "action"    # Lcom/vlingo/voicepad/tagtoaction/model/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/goal/model/Goal;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/model/VLActionResult;",
            ">;",
            "Lcom/vlingo/voicepad/tagtoaction/model/Action;",
            ")V"
        }
    .end annotation

    .prologue
    .line 262
    .local p3, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/model/VLActionResult;>;"
    new-instance v0, Lcom/vlingo/voicepad/util/ActionResult;

    invoke-direct {v0, p4}, Lcom/vlingo/voicepad/util/ActionResult;-><init>(Lcom/vlingo/voicepad/tagtoaction/model/Action;)V

    .line 264
    .local v0, "actionResult":Lcom/vlingo/voicepad/util/ActionResult;
    sget-object v4, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "generating ActionResult "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/vlingo/voicepad/util/ActionResult;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 267
    invoke-virtual {p4}, Lcom/vlingo/voicepad/tagtoaction/model/Action;->getParams()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;

    .line 268
    .local v3, "param":Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;
    invoke-virtual {v3}, Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4, p1, p2}, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->validateAndReplaceVariables(Ljava/lang/String;Lcom/vlingo/dialog/goal/model/Goal;Z)Ljava/lang/String;

    move-result-object v1

    .line 269
    .local v1, "finalParamValue":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    if-lt v4, v5, :cond_0

    .line 274
    invoke-virtual {v3}, Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4, v1}, Lcom/vlingo/voicepad/util/ActionResult;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 277
    .end local v1    # "finalParamValue":Ljava/lang/String;
    .end local v3    # "param":Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;
    :cond_1
    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 278
    return-void
.end method

.method public static evaluateJexlExpression(Lcom/vlingo/dialog/goal/model/Goal;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "goal"    # Lcom/vlingo/dialog/goal/model/Goal;
    .param p1, "expressionIn"    # Ljava/lang/String;

    .prologue
    .line 337
    invoke-static {p1}, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->stripPrefixAndSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 338
    .local v3, "expressionString":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 339
    sget-object v4, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->engine:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v4, v3}, Lorg/apache/commons/jexl2/JexlEngine;->createExpression(Ljava/lang/String;)Lorg/apache/commons/jexl2/Expression;

    move-result-object v2

    .line 340
    .local v2, "expression":Lorg/apache/commons/jexl2/Expression;
    new-instance v0, Lorg/apache/commons/jexl2/MapContext;

    const-string/jumbo v4, "goal"

    invoke-static {v4, p0}, Lcom/google/common/collect/ImmutableMap;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v4

    invoke-direct {v0, v4}, Lorg/apache/commons/jexl2/MapContext;-><init>(Ljava/util/Map;)V

    .line 342
    .local v0, "context":Lorg/apache/commons/jexl2/JexlContext;
    :try_start_0
    invoke-interface {v2, v0}, Lorg/apache/commons/jexl2/Expression;->evaluate(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;
    :try_end_0
    .catch Lorg/apache/commons/jexl2/JexlException; {:try_start_0 .. :try_end_0} :catch_0

    .line 347
    .end local v0    # "context":Lorg/apache/commons/jexl2/JexlContext;
    .end local v2    # "expression":Lorg/apache/commons/jexl2/Expression;
    :goto_0
    return-object v4

    .line 343
    .restart local v0    # "context":Lorg/apache/commons/jexl2/JexlContext;
    .restart local v2    # "expression":Lorg/apache/commons/jexl2/Expression;
    :catch_0
    move-exception v1

    .line 344
    .local v1, "e":Lorg/apache/commons/jexl2/JexlException;
    sget-object v4, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v5, "JEXL error"

    invoke-virtual {v4, v5, v1}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 347
    .end local v0    # "context":Lorg/apache/commons/jexl2/JexlContext;
    .end local v1    # "e":Lorg/apache/commons/jexl2/JexlException;
    .end local v2    # "expression":Lorg/apache/commons/jexl2/Expression;
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private generateActionsForFeature(Lcom/vlingo/voicepad/tagtoaction/model/Feature;Lcom/vlingo/dialog/goal/model/Goal;Z)Ljava/util/List;
    .locals 8
    .param p1, "feature"    # Lcom/vlingo/voicepad/tagtoaction/model/Feature;
    .param p2, "goal"    # Lcom/vlingo/dialog/goal/model/Goal;
    .param p3, "evaluateAllVPath"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/voicepad/tagtoaction/model/Feature;",
            "Lcom/vlingo/dialog/goal/model/Goal;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/model/VLActionResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 240
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 242
    .local v1, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/model/VLActionResult;>;"
    invoke-virtual {p1}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getActions()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/voicepad/tagtoaction/model/Action;

    .line 243
    .local v0, "action":Lcom/vlingo/voicepad/tagtoaction/model/Action;
    instance-of v7, v0, Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplateRef;

    if-eqz v7, :cond_1

    .line 245
    invoke-virtual {v0}, Lcom/vlingo/voicepad/tagtoaction/model/Action;->getName()Ljava/lang/String;

    move-result-object v6

    .line 246
    .local v6, "templateName":Ljava/lang/String;
    iget-object v7, p0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivFeatureSetMgr:Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;

    invoke-virtual {v7, v6}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->getActionTemplateByName(Ljava/lang/String;)Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;

    move-result-object v4

    .line 248
    .local v4, "template":Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;
    invoke-virtual {v4}, Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;->getActions()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/voicepad/tagtoaction/model/Action;

    .line 249
    .local v5, "templateAction":Lcom/vlingo/voicepad/tagtoaction/model/Action;
    invoke-direct {p0, p2, p3, v1, v5}, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->buildActionResult(Lcom/vlingo/dialog/goal/model/Goal;ZLjava/util/List;Lcom/vlingo/voicepad/tagtoaction/model/Action;)V

    goto :goto_1

    .line 253
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "template":Lcom/vlingo/voicepad/tagtoaction/model/ActionTemplate;
    .end local v5    # "templateAction":Lcom/vlingo/voicepad/tagtoaction/model/Action;
    .end local v6    # "templateName":Ljava/lang/String;
    :cond_1
    invoke-direct {p0, p2, p3, v1, v0}, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->buildActionResult(Lcom/vlingo/dialog/goal/model/Goal;ZLjava/util/List;Lcom/vlingo/voicepad/tagtoaction/model/Action;)V

    goto :goto_0

    .line 257
    .end local v0    # "action":Lcom/vlingo/voicepad/tagtoaction/model/Action;
    :cond_2
    return-object v1
.end method

.method private generateActionsForGoal(Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;Lcom/vlingo/dialog/goal/model/Goal;Ljava/lang/String;Z)Ljava/util/List;
    .locals 4
    .param p1, "featureSet"    # Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    .param p2, "goal"    # Lcom/vlingo/dialog/goal/model/Goal;
    .param p3, "platform"    # Ljava/lang/String;
    .param p4, "evaluateAllVPath"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;",
            "Lcom/vlingo/dialog/goal/model/Goal;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/model/VLActionResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 199
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    .line 201
    .local v1, "goalClass":Ljava/lang/String;
    invoke-virtual {p1, p2}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;->findFirstMatchingFeature(Lcom/vlingo/dialog/goal/model/Goal;)Lcom/vlingo/voicepad/tagtoaction/model/Feature;

    move-result-object v0

    .line 202
    .local v0, "featureMatch":Lcom/vlingo/voicepad/tagtoaction/model/Feature;
    if-nez v0, :cond_0

    .line 205
    new-instance v2, Lcom/vlingo/common/message/MessageException;

    const/4 v3, 0x0

    invoke-static {p3, v3, v1}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionError;->getNoMatchingFeature(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/vlingo/common/message/MessageException;-><init>(Lcom/vlingo/common/message/VMessage;)V

    throw v2

    .line 208
    :cond_0
    invoke-direct {p0, v0, p2, p4}, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->generateActionsForFeature(Lcom/vlingo/voicepad/tagtoaction/model/Feature;Lcom/vlingo/dialog/goal/model/Goal;Z)Ljava/util/List;

    move-result-object v2

    return-object v2
.end method

.method private generateActionsForMessages(Lcom/vlingo/dialog/goal/model/Goal;)Ljava/util/List;
    .locals 1
    .param p1, "goal"    # Lcom/vlingo/dialog/goal/model/Goal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/goal/model/Goal;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/model/VLActionResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 281
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method private init(Lcom/vlingo/mda/cfgloader/ConfigLoader;)V
    .locals 4
    .param p1, "loader"    # Lcom/vlingo/mda/cfgloader/ConfigLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;
        }
    .end annotation

    .prologue
    .line 114
    invoke-interface {p1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->getVersion()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivVersion:Ljava/lang/String;

    .line 115
    const-string/jumbo v1, "GoalToAction.main.xml"

    invoke-static {v1}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->matchingPaths(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 116
    new-instance v1, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;

    const-string/jumbo v2, "GoalToAction.main.xml"

    iget-object v3, p0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivResourceManager:Lcom/vlingo/voicepad/config/ResourceManager;

    invoke-direct {v1, p1, v2, v3}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;-><init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;Lcom/vlingo/voicepad/config/ResourceManager;)V

    iput-object v1, p0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivFeatureSetMgr:Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;

    .line 124
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivFeatureSetMgr:Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;

    if-nez v1, :cond_2

    .line 125
    new-instance v1, Ljava/io/IOException;

    const-string/jumbo v2, "could not find GoalToAction.main.xml or a single GoalToAction\\..*\\.xml match"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 118
    :cond_1
    const-string/jumbo v1, "GoalToAction\\..*\\.xml"

    invoke-interface {p1, v1}, Lcom/vlingo/mda/cfgloader/ConfigLoader;->matchingPaths(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v0

    .line 119
    .local v0, "goalToActionFiles":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    sget-object v1, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " matching GoalToAction files"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 120
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 121
    new-instance v2, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivResourceManager:Lcom/vlingo/voicepad/config/ResourceManager;

    invoke-direct {v2, p1, v1, v3}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;-><init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;Ljava/lang/String;Lcom/vlingo/voicepad/config/ResourceManager;)V

    iput-object v2, p0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivFeatureSetMgr:Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;

    goto :goto_0

    .line 127
    .end local v0    # "goalToActionFiles":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :cond_2
    sget-object v1, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Initialized GoalToActionGenerator from configuration version: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivVersion:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 128
    return-void
.end method

.method public static loadFromDirectory(Ljava/io/File;Lcom/vlingo/voicepad/config/ResourceManager;)Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;
    .locals 3
    .param p0, "dir"    # Ljava/io/File;
    .param p1, "resourceManager"    # Lcom/vlingo/voicepad/config/ResourceManager;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;
        }
    .end annotation

    .prologue
    .line 87
    new-instance v0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;

    new-instance v1, Lcom/vlingo/mda/cfgloader/DirectoryMDAConfigLoader;

    const-class v2, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionConfig;

    invoke-direct {v1, p0, v2}, Lcom/vlingo/mda/cfgloader/DirectoryMDAConfigLoader;-><init>(Ljava/io/File;Ljava/lang/Class;)V

    invoke-direct {v0, v1, p1}, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;-><init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;Lcom/vlingo/voicepad/config/ResourceManager;)V

    return-object v0
.end method

.method public static loadFromJar(Ljava/io/File;Lcom/vlingo/voicepad/config/ResourceManager;)Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;
    .locals 3
    .param p0, "config"    # Ljava/io/File;
    .param p1, "resourceManager"    # Lcom/vlingo/voicepad/config/ResourceManager;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;
        }
    .end annotation

    .prologue
    .line 83
    new-instance v0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;

    new-instance v1, Lcom/vlingo/mda/cfgloader/JarMDAConfigLoader;

    const-class v2, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionConfig;

    invoke-direct {v1, p0, v2}, Lcom/vlingo/mda/cfgloader/JarMDAConfigLoader;-><init>(Ljava/io/File;Ljava/lang/Class;)V

    invoke-direct {v0, v1, p1}, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;-><init>(Lcom/vlingo/mda/cfgloader/ConfigLoader;Lcom/vlingo/voicepad/config/ResourceManager;)V

    return-object v0
.end method

.method private static stripPrefixAndSuffix(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "var"    # Ljava/lang/String;

    .prologue
    .line 329
    const-string/jumbo v0, "${jexl:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 330
    const/4 v0, 0x7

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 332
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private validateAndReplaceVariables(Ljava/lang/String;Lcom/vlingo/dialog/goal/model/Goal;Z)Ljava/lang/String;
    .locals 12
    .param p1, "paramValue"    # Ljava/lang/String;
    .param p2, "goal"    # Lcom/vlingo/dialog/goal/model/Goal;
    .param p3, "evaluateAllVPath"    # Z

    .prologue
    .line 285
    sget-object v9, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->JEXL_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v9, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 286
    .local v4, "matcher":Ljava/util/regex/Matcher;
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 289
    .local v5, "replacedParamValue":Ljava/lang/StringBuffer;
    :goto_0
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 290
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v7

    .line 293
    .local v7, "var":Ljava/lang/String;
    invoke-static {v7}, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->stripPrefixAndSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 294
    .local v3, "expressionString":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 295
    sget-object v9, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->engine:Lorg/apache/commons/jexl2/JexlEngine;

    invoke-virtual {v9, v3}, Lorg/apache/commons/jexl2/JexlEngine;->createExpression(Ljava/lang/String;)Lorg/apache/commons/jexl2/Expression;

    move-result-object v2

    .line 296
    .local v2, "expression":Lorg/apache/commons/jexl2/Expression;
    new-instance v0, Lorg/apache/commons/jexl2/MapContext;

    const-string/jumbo v9, "goal"

    invoke-static {v9, p2}, Lcom/google/common/collect/ImmutableMap;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v9

    invoke-direct {v0, v9}, Lorg/apache/commons/jexl2/MapContext;-><init>(Ljava/util/Map;)V

    .line 297
    .local v0, "context":Lorg/apache/commons/jexl2/JexlContext;
    const/4 v6, 0x0

    .line 299
    .local v6, "result":Ljava/lang/Object;
    :try_start_0
    invoke-interface {v2, v0}, Lorg/apache/commons/jexl2/Expression;->evaluate(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/apache/commons/jexl2/JexlException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 304
    if-nez v6, :cond_1

    .line 305
    const/4 v8, 0x0

    .line 315
    .end local v0    # "context":Lorg/apache/commons/jexl2/JexlContext;
    .end local v2    # "expression":Lorg/apache/commons/jexl2/Expression;
    .end local v6    # "result":Ljava/lang/Object;
    .local v8, "varValue":Ljava/lang/String;
    :goto_1
    if-nez v8, :cond_0

    .line 316
    const-string/jumbo v8, ""

    .line 317
    :cond_0
    invoke-static {v8}, Ljava/util/regex/Matcher;->quoteReplacement(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v5, v9}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    goto :goto_0

    .line 300
    .end local v8    # "varValue":Ljava/lang/String;
    .restart local v0    # "context":Lorg/apache/commons/jexl2/JexlContext;
    .restart local v2    # "expression":Lorg/apache/commons/jexl2/Expression;
    .restart local v6    # "result":Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 301
    .local v1, "e":Lorg/apache/commons/jexl2/JexlException;
    sget-object v9, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v10, "JEXL error"

    invoke-virtual {v9, v10, v1}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 302
    throw v1

    .line 307
    .end local v1    # "e":Lorg/apache/commons/jexl2/JexlException;
    :cond_1
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "varValue":Ljava/lang/String;
    goto :goto_1

    .line 311
    .end local v0    # "context":Lorg/apache/commons/jexl2/JexlContext;
    .end local v2    # "expression":Lorg/apache/commons/jexl2/Expression;
    .end local v6    # "result":Ljava/lang/Object;
    .end local v8    # "varValue":Ljava/lang/String;
    :cond_2
    sget-object v9, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "***Somehow matching JEXL Pattern but not JEXL? : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    .line 312
    move-object v8, v7

    .restart local v8    # "varValue":Ljava/lang/String;
    goto :goto_1

    .line 321
    .end local v3    # "expressionString":Ljava/lang/String;
    .end local v7    # "var":Ljava/lang/String;
    .end local v8    # "varValue":Ljava/lang/String;
    :cond_3
    invoke-virtual {v4, v5}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 323
    sget-object v9, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "**** Param value: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " Replaced value: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 325
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->length()I

    move-result v9

    if-lez v9, :cond_4

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    :goto_2
    return-object v9

    :cond_4
    const/4 v9, 0x0

    goto :goto_2
.end method


# virtual methods
.method public generateActionList(Lcom/vlingo/workflow/task/TaskEnv;Ljava/util/List;ZZ)Ljava/util/List;
    .locals 11
    .param p3, "evaluateAllVPath"    # Z
    .param p4, "showUserErrors"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/workflow/task/TaskEnv",
            "<",
            "Lcom/vlingo/voicepad/vvs/VVEnv;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/goal/model/Goal;",
            ">;ZZ)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/model/VLActionResult;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "taskEnv":Lcom/vlingo/workflow/task/TaskEnv;, "Lcom/vlingo/workflow/task/TaskEnv<Lcom/vlingo/voicepad/vvs/VVEnv;>;"
    .local p2, "goals":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/goal/model/Goal;>;"
    const/4 v10, 0x0

    .line 132
    invoke-virtual {p1}, Lcom/vlingo/workflow/task/TaskEnv;->getRequest()Lcom/vlingo/workflow/model/VRequest;

    move-result-object v7

    const-string/jumbo v8, "x-vlclient"

    const-string/jumbo v9, "DeviceOSName"

    invoke-interface {v7, v8, v9}, Lcom/vlingo/workflow/model/VRequest;->parseVLHeaderField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 135
    .local v5, "platform":Ljava/lang/String;
    const/4 v1, 0x0

    .line 136
    .local v1, "featureSet":Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_0

    .line 137
    iget-object v7, p0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivFeatureSetMgr:Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;

    invoke-virtual {p1}, Lcom/vlingo/workflow/task/TaskEnv;->getRequest()Lcom/vlingo/workflow/model/VRequest;

    move-result-object v8

    invoke-virtual {v7, v10, v8}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->findFeatureSetForRequest(Lcom/vlingo/voicepad/model/LicenseInfo;Lcom/vlingo/workflow/model/VRequest;)Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;

    move-result-object v1

    .line 138
    if-nez v1, :cond_0

    .line 141
    new-instance v7, Lcom/vlingo/common/message/MessageException;

    invoke-static {v5, v10}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionError;->getNoMatchingFeatureMap(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/vlingo/common/message/MessageException;-><init>(Lcom/vlingo/common/message/VMessage;)V

    throw v7

    .line 145
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 147
    .local v0, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/model/VLActionResult;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/dialog/goal/model/Goal;

    .line 148
    .local v2, "goal":Lcom/vlingo/dialog/goal/model/Goal;
    invoke-direct {p0, v1, v2, v5, p3}, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->generateActionsForGoal(Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;Lcom/vlingo/dialog/goal/model/Goal;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v6

    .line 149
    .local v6, "tagActions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/model/VLActionResult;>;"
    invoke-interface {v0, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 151
    if-eqz p4, :cond_1

    .line 152
    invoke-direct {p0, v2}, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->generateActionsForMessages(Lcom/vlingo/dialog/goal/model/Goal;)Ljava/util/List;

    move-result-object v4

    .line 153
    .local v4, "msgActions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/model/VLActionResult;>;"
    if-eqz v4, :cond_1

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_1

    .line 154
    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 159
    .end local v2    # "goal":Lcom/vlingo/dialog/goal/model/Goal;
    .end local v4    # "msgActions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/model/VLActionResult;>;"
    .end local v6    # "tagActions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/model/VLActionResult;>;"
    :cond_2
    return-object v0
.end method

.method public generateActionList(Ljava/util/List;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/goal/model/Goal;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/model/VLActionResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    .local p1, "goals":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/goal/model/Goal;>;"
    const-string/jumbo v5, "embedded"

    .line 164
    .local v5, "platform":Ljava/lang/String;
    const/4 v1, 0x1

    .line 165
    .local v1, "evaluateAllVPath":Z
    const/4 v6, 0x0

    .line 169
    .local v6, "showUserErrors":Z
    const/4 v2, 0x0

    .line 170
    .local v2, "featureSet":Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_0

    .line 171
    iget-object v8, p0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivFeatureSetMgr:Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;

    invoke-virtual {v8}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->findSingleFeatureSet()Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;

    move-result-object v2

    .line 172
    if-nez v2, :cond_0

    .line 175
    new-instance v8, Lcom/vlingo/common/message/MessageException;

    const-string/jumbo v9, "embedded"

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionError;->getNoMatchingFeatureMap(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/vlingo/common/message/MessageException;-><init>(Lcom/vlingo/common/message/VMessage;)V

    throw v8

    .line 179
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 181
    .local v0, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/model/VLActionResult;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/dialog/goal/model/Goal;

    .line 182
    .local v3, "goal":Lcom/vlingo/dialog/goal/model/Goal;
    const-string/jumbo v8, "embedded"

    const/4 v9, 0x1

    invoke-direct {p0, v2, v3, v8, v9}, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->generateActionsForGoal(Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;Lcom/vlingo/dialog/goal/model/Goal;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v7

    .line 183
    .local v7, "tagActions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/model/VLActionResult;>;"
    invoke-interface {v0, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 193
    .end local v3    # "goal":Lcom/vlingo/dialog/goal/model/Goal;
    .end local v7    # "tagActions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/model/VLActionResult;>;"
    :cond_1
    return-object v0
.end method

.method public getFeatureTester()Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator$GoalToActionFeatureTester;
    .locals 1

    .prologue
    .line 235
    new-instance v0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator$GoalToActionFeatureTester;

    invoke-direct {v0, p0}, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator$GoalToActionFeatureTester;-><init>(Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;)V

    return-object v0
.end method

.method public setResourceManager(Lcom/vlingo/voicepad/config/ResourceManager;)V
    .locals 2
    .param p1, "resourceManager"    # Lcom/vlingo/voicepad/config/ResourceManager;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivResourceManager:Lcom/vlingo/voicepad/config/ResourceManager;

    .line 106
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivFeatureSetMgr:Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;

    iget-object v1, p0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->ivResourceManager:Lcom/vlingo/voicepad/config/ResourceManager;

    invoke-virtual {v0, v1}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;->setResourceManager(Lcom/vlingo/voicepad/config/ResourceManager;)V

    .line 107
    return-void
.end method
