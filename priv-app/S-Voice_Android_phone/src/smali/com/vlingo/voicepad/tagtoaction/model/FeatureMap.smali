.class public Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;
.super Lcom/vlingo/voicepad/tagtoaction/model/FeatureMapBase;
.source "FeatureMap.java"


# instance fields
.field private ivCachedFeatureSet:Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/voicepad/tagtoaction/model/FeatureMapBase;-><init>()V

    return-void
.end method


# virtual methods
.method public getCachedFeatureSet()Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;->ivCachedFeatureSet:Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;

    return-object v0
.end method

.method public setCachedFeatureSet(Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;)V
    .locals 0
    .param p1, "cachedFeatureSet"    # Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureMap;->ivCachedFeatureSet:Lcom/vlingo/voicepad/tagtoaction/model/FeatureSet;

    .line 24
    return-void
.end method
