.class public Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator$GoalToActionFeatureTester;
.super Ljava/lang/Object;
.source "GoalToActionGenerator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GoalToActionFeatureTester"
.end annotation


# instance fields
.field private ivGenerator:Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;

.field final synthetic this$0:Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;


# direct methods
.method public constructor <init>(Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator$GoalToActionFeatureTester;->this$0:Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219
    return-void
.end method


# virtual methods
.method public generateActionsForFeature(Lcom/vlingo/voicepad/tagtoaction/model/Feature;Lcom/vlingo/dialog/goal/model/Goal;Z)Ljava/util/List;
    .locals 4
    .param p1, "feature"    # Lcom/vlingo/voicepad/tagtoaction/model/Feature;
    .param p2, "goal"    # Lcom/vlingo/dialog/goal/model/Goal;
    .param p3, "evaluateAllVPath"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/voicepad/tagtoaction/model/Feature;",
            "Lcom/vlingo/dialog/goal/model/Goal;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/model/VLActionResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 223
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 225
    .local v1, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/model/VLActionResult;>;"
    invoke-virtual {p1}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getActions()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/voicepad/tagtoaction/model/Action;

    .line 227
    .local v0, "action":Lcom/vlingo/voicepad/tagtoaction/model/Action;
    iget-object v3, p0, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator$GoalToActionFeatureTester;->this$0:Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;

    # invokes: Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->buildActionResult(Lcom/vlingo/dialog/goal/model/Goal;ZLjava/util/List;Lcom/vlingo/voicepad/tagtoaction/model/Action;)V
    invoke-static {v3, p2, p3, v1, v0}, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->access$000(Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;Lcom/vlingo/dialog/goal/model/Goal;ZLjava/util/List;Lcom/vlingo/voicepad/tagtoaction/model/Action;)V

    goto :goto_0

    .line 230
    .end local v0    # "action":Lcom/vlingo/voicepad/tagtoaction/model/Action;
    :cond_0
    return-object v1
.end method
