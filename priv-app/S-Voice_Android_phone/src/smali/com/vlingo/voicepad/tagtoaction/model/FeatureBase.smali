.class public Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;
.super Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;
.source "FeatureBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_Actions:Ljava/lang/String; = "Actions"

.field public static final PROP_CapabilityName:Ljava/lang/String; = "CapabilityName"

.field public static final PROP_CapabilityValue:Ljava/lang/String; = "CapabilityValue"

.field public static final PROP_FieldContext:Ljava/lang/String; = "FieldContext"

.field public static final PROP_FieldID:Ljava/lang/String; = "FieldID"

.field public static final PROP_GoalClass:Ljava/lang/String; = "GoalClass"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Name:Ljava/lang/String; = "Name"

.field public static final PROP_ParseType:Ljava/lang/String; = "ParseType"

.field public static final PROP_VPath2Equals:Ljava/lang/String; = "VPath2Equals"

.field public static final PROP_VPath2Exists:Ljava/lang/String; = "VPath2Exists"

.field public static final PROP_VPath2NotExists:Ljava/lang/String; = "VPath2NotExists"

.field public static final PROP_VPathEquals:Ljava/lang/String; = "VPathEquals"

.field public static final PROP_VPathExists:Ljava/lang/String; = "VPathExists"

.field public static final PROP_VPathNotExists:Ljava/lang/String; = "VPathNotExists"


# instance fields
.field private Actions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/voicepad/tagtoaction/model/Action;",
            ">;"
        }
    .end annotation
.end field

.field private CapabilityName:Ljava/lang/String;

.field private CapabilityValue:Ljava/lang/String;

.field private FieldContext:Ljava/lang/String;

.field private FieldID:Ljava/lang/String;

.field private GoalClass:Ljava/lang/String;

.field private ID:J

.field private Name:Ljava/lang/String;

.field private ParseType:Ljava/lang/String;

.field private VPath2Equals:Ljava/lang/String;

.field private VPath2Exists:Ljava/lang/String;

.field private VPath2NotExists:Ljava/lang/String;

.field private VPathEquals:Ljava/lang/String;

.field private VPathExists:Ljava/lang/String;

.field private VPathNotExists:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;-><init>()V

    .line 31
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->Actions:Ljava/util/Set;

    .line 37
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 140
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/voicepad/tagtoaction/model/Feature;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getActions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/voicepad/tagtoaction/model/Action;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->Actions:Ljava/util/Set;

    return-object v0
.end method

.method public getCapabilityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->CapabilityName:Ljava/lang/String;

    return-object v0
.end method

.method public getCapabilityValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->CapabilityValue:Ljava/lang/String;

    return-object v0
.end method

.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 144
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/voicepad/tagtoaction/model/Feature;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getFieldContext()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->FieldContext:Ljava/lang/String;

    return-object v0
.end method

.method public getFieldID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->FieldID:Ljava/lang/String;

    return-object v0
.end method

.method public getGoalClass()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->GoalClass:Ljava/lang/String;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 132
    iget-wide v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->ID:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->Name:Ljava/lang/String;

    return-object v0
.end method

.method public getParseType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->ParseType:Ljava/lang/String;

    return-object v0
.end method

.method public getVPath2Equals()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->VPath2Equals:Ljava/lang/String;

    return-object v0
.end method

.method public getVPath2Exists()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->VPath2Exists:Ljava/lang/String;

    return-object v0
.end method

.method public getVPath2NotExists()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->VPath2NotExists:Ljava/lang/String;

    return-object v0
.end method

.method public getVPathEquals()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->VPathEquals:Ljava/lang/String;

    return-object v0
.end method

.method public getVPathExists()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->VPathExists:Ljava/lang/String;

    return-object v0
.end method

.method public getVPathNotExists()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->VPathNotExists:Ljava/lang/String;

    return-object v0
.end method

.method public setCapabilityName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->CapabilityName:Ljava/lang/String;

    .line 120
    return-void
.end method

.method public setCapabilityValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->CapabilityValue:Ljava/lang/String;

    .line 127
    return-void
.end method

.method public setFieldContext(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->FieldContext:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public setFieldID(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->FieldID:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public setGoalClass(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->GoalClass:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 135
    iput-wide p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->ID:J

    .line 136
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->Name:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public setParseType(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->ParseType:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public setVPath2Equals(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->VPath2Equals:Ljava/lang/String;

    .line 113
    return-void
.end method

.method public setVPath2Exists(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->VPath2Exists:Ljava/lang/String;

    .line 99
    return-void
.end method

.method public setVPath2NotExists(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->VPath2NotExists:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public setVPathEquals(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->VPathEquals:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public setVPathExists(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->VPathExists:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public setVPathNotExists(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/FeatureBase;->VPathNotExists:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
