.class Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager$FeatureComparator;
.super Ljava/lang/Object;
.source "FeatureSetManager.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FeatureComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/vlingo/voicepad/tagtoaction/model/Feature;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;


# direct methods
.method private constructor <init>(Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;)V
    .locals 0

    .prologue
    .line 393
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager$FeatureComparator;->this$0:Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;
    .param p2, "x1"    # Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager$1;

    .prologue
    .line 393
    invoke-direct {p0, p1}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager$FeatureComparator;-><init>(Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager;)V

    return-void
.end method


# virtual methods
.method public compare(Lcom/vlingo/voicepad/tagtoaction/model/Feature;Lcom/vlingo/voicepad/tagtoaction/model/Feature;)I
    .locals 2
    .param p1, "o1"    # Lcom/vlingo/voicepad/tagtoaction/model/Feature;
    .param p2, "o2"    # Lcom/vlingo/voicepad/tagtoaction/model/Feature;

    .prologue
    .line 396
    invoke-virtual {p1}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getParseType()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 397
    invoke-virtual {p2}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getParseType()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 398
    const/4 v0, 0x0

    .line 407
    :goto_0
    return v0

    .line 400
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 403
    :cond_1
    invoke-virtual {p2}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getParseType()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 404
    const/4 v0, 0x1

    goto :goto_0

    .line 407
    :cond_2
    invoke-virtual {p1}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getParseType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/vlingo/voicepad/tagtoaction/model/Feature;->getParseType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 393
    check-cast p1, Lcom/vlingo/voicepad/tagtoaction/model/Feature;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/vlingo/voicepad/tagtoaction/model/Feature;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/voicepad/tagtoaction/FeatureSetManager$FeatureComparator;->compare(Lcom/vlingo/voicepad/tagtoaction/model/Feature;Lcom/vlingo/voicepad/tagtoaction/model/Feature;)I

    move-result v0

    return v0
.end method
