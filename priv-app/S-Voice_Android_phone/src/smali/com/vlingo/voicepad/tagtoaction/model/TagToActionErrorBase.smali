.class public Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorBase;
.super Ljava/lang/Object;
.source "TagToActionErrorBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_ID:Ljava/lang/String; = "ID"


# instance fields
.field private ID:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    return-void
.end method

.method public static getBadMeta(Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;
    .locals 6
    .param p0, "detail"    # Ljava/lang/String;

    .prologue
    .line 27
    new-instance v0, Lcom/vlingo/common/message/VMessage;

    new-instance v1, Lcom/vlingo/common/VText;

    const-string/jumbo v2, "com.vlingo.voicepad.tagtoaction.model.TagToActionError"

    const-string/jumbo v3, "BadMeta"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-direct {v1, v2, v3, v4}, Lcom/vlingo/common/VText;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v2, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->BadMeta:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    invoke-direct {v0, v1, v2}, Lcom/vlingo/common/message/VMessage;-><init>(Lcom/vlingo/common/VText;Lcom/vlingo/common/message/MessageCode;)V

    return-object v0
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 47
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionError;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public static getInvalidRecognitionReference(Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;
    .locals 6
    .param p0, "vpath"    # Ljava/lang/String;

    .prologue
    .line 31
    new-instance v0, Lcom/vlingo/common/message/VMessage;

    new-instance v1, Lcom/vlingo/common/VText;

    const-string/jumbo v2, "com.vlingo.voicepad.tagtoaction.model.TagToActionError"

    const-string/jumbo v3, "InvalidRecognitionReference"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-direct {v1, v2, v3, v4}, Lcom/vlingo/common/VText;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v2, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->InvalidRecognitionReference:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    invoke-direct {v0, v1, v2}, Lcom/vlingo/common/message/VMessage;-><init>(Lcom/vlingo/common/VText;Lcom/vlingo/common/message/MessageCode;)V

    return-object v0
.end method

.method public static getInvalidVariableReference(Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;
    .locals 6
    .param p0, "variable"    # Ljava/lang/String;

    .prologue
    .line 35
    new-instance v0, Lcom/vlingo/common/message/VMessage;

    new-instance v1, Lcom/vlingo/common/VText;

    const-string/jumbo v2, "com.vlingo.voicepad.tagtoaction.model.TagToActionError"

    const-string/jumbo v3, "InvalidVariableReference"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-direct {v1, v2, v3, v4}, Lcom/vlingo/common/VText;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v2, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->InvalidVariableReference:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    invoke-direct {v0, v1, v2}, Lcom/vlingo/common/message/VMessage;-><init>(Lcom/vlingo/common/VText;Lcom/vlingo/common/message/MessageCode;)V

    return-object v0
.end method

.method public static getInvalidXML()Lcom/vlingo/common/message/VMessage;
    .locals 5

    .prologue
    .line 23
    new-instance v0, Lcom/vlingo/common/message/VMessage;

    new-instance v1, Lcom/vlingo/common/VText;

    const-string/jumbo v2, "com.vlingo.voicepad.tagtoaction.model.TagToActionError"

    const-string/jumbo v3, "InvalidXML"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-direct {v1, v2, v3, v4}, Lcom/vlingo/common/VText;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v2, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->InvalidXML:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    invoke-direct {v0, v1, v2}, Lcom/vlingo/common/message/VMessage;-><init>(Lcom/vlingo/common/VText;Lcom/vlingo/common/message/MessageCode;)V

    return-object v0
.end method

.method public static getMissingMeta(Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;
    .locals 6
    .param p0, "filename"    # Ljava/lang/String;

    .prologue
    .line 19
    new-instance v0, Lcom/vlingo/common/message/VMessage;

    new-instance v1, Lcom/vlingo/common/VText;

    const-string/jumbo v2, "com.vlingo.voicepad.tagtoaction.model.TagToActionError"

    const-string/jumbo v3, "MissingMeta"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-direct {v1, v2, v3, v4}, Lcom/vlingo/common/VText;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v2, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->MissingMeta:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    invoke-direct {v0, v1, v2}, Lcom/vlingo/common/message/VMessage;-><init>(Lcom/vlingo/common/VText;Lcom/vlingo/common/message/MessageCode;)V

    return-object v0
.end method

.method public static getNoMatchingFeature(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;
    .locals 6
    .param p0, "platform"    # Ljava/lang/String;
    .param p1, "licenseClass"    # Ljava/lang/String;
    .param p2, "parseType"    # Ljava/lang/String;

    .prologue
    .line 43
    new-instance v0, Lcom/vlingo/common/message/VMessage;

    new-instance v1, Lcom/vlingo/common/VText;

    const-string/jumbo v2, "com.vlingo.voicepad.tagtoaction.model.TagToActionError"

    const-string/jumbo v3, "NoMatchingFeature"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    const/4 v5, 0x1

    aput-object p1, v4, v5

    const/4 v5, 0x2

    aput-object p2, v4, v5

    invoke-direct {v1, v2, v3, v4}, Lcom/vlingo/common/VText;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v2, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->NoMatchingFeature:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    invoke-direct {v0, v1, v2}, Lcom/vlingo/common/message/VMessage;-><init>(Lcom/vlingo/common/VText;Lcom/vlingo/common/message/MessageCode;)V

    return-object v0
.end method

.method public static getNoMatchingFeatureMap(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;
    .locals 6
    .param p0, "platform"    # Ljava/lang/String;
    .param p1, "licenseClass"    # Ljava/lang/String;

    .prologue
    .line 39
    new-instance v0, Lcom/vlingo/common/message/VMessage;

    new-instance v1, Lcom/vlingo/common/VText;

    const-string/jumbo v2, "com.vlingo.voicepad.tagtoaction.model.TagToActionError"

    const-string/jumbo v3, "NoMatchingFeatureMap"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    const/4 v5, 0x1

    aput-object p1, v4, v5

    invoke-direct {v1, v2, v3, v4}, Lcom/vlingo/common/VText;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v2, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;->NoMatchingFeatureMap:Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorCode;

    invoke-direct {v0, v1, v2}, Lcom/vlingo/common/message/VMessage;-><init>(Lcom/vlingo/common/VText;Lcom/vlingo/common/message/MessageCode;)V

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 51
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionError;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 11
    iget-wide v0, p0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorBase;->ID:J

    return-wide v0
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 14
    iput-wide p1, p0, Lcom/vlingo/voicepad/tagtoaction/model/TagToActionErrorBase;->ID:J

    .line 15
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
