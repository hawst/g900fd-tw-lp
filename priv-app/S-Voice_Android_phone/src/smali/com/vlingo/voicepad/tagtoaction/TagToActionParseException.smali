.class public Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;
.super Ljava/lang/Exception;
.source "TagToActionParseException.java"


# instance fields
.field private final ivMessage:Lcom/vlingo/common/message/VMessage;


# direct methods
.method public constructor <init>(Lcom/vlingo/common/message/VMessage;)V
    .locals 1
    .param p1, "message"    # Lcom/vlingo/common/message/VMessage;

    .prologue
    .line 26
    invoke-virtual {p1}, Lcom/vlingo/common/message/VMessage;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 27
    iput-object p1, p0, Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;->ivMessage:Lcom/vlingo/common/message/VMessage;

    .line 28
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;->ivMessage:Lcom/vlingo/common/message/VMessage;

    .line 22
    return-void
.end method


# virtual methods
.method public getVMessage()Lcom/vlingo/common/message/VMessage;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/voicepad/tagtoaction/TagToActionParseException;->ivMessage:Lcom/vlingo/common/message/VMessage;

    return-object v0
.end method
