.class public Lcom/vlingo/voicepad/vvs/VVServletUtil;
.super Ljava/lang/Object;
.source "VVServletUtil.java"


# direct methods
.method public static getAutoTag(Lcom/vlingo/workflow/model/VRequest;)Ljava/lang/Boolean;
    .locals 3
    .param p0, "request"    # Lcom/vlingo/workflow/model/VRequest;

    .prologue
    .line 23
    const-string/jumbo v1, "x-vltest"

    const-string/jumbo v2, "AutoTag"

    invoke-interface {p0, v1, v2}, Lcom/vlingo/workflow/model/VRequest;->parseVLHeaderField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 24
    .local v0, "autoTag":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string/jumbo v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getVVSRequestVersion(Lcom/vlingo/workflow/model/VRequest;)Ljava/lang/String;
    .locals 4
    .param p0, "request"    # Lcom/vlingo/workflow/model/VRequest;

    .prologue
    .line 12
    const-string/jumbo v2, "X-vvs"

    invoke-static {p0, v2}, Lcom/vlingo/message/VWebUtil;->getHeaderIgnoreCase(Lcom/vlingo/message/util/HeaderProvider;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 13
    .local v1, "vvsHeader":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 14
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 15
    .local v0, "parts":[Ljava/lang/String;
    array-length v2, v0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "Version"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 16
    const/4 v2, 0x1

    aget-object v2, v0, v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 19
    .end local v0    # "parts":[Ljava/lang/String;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
