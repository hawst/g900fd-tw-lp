.class public abstract Lcom/vlingo/voicepad/vvs/VVServlet;
.super Lcom/vlingo/servlet/util/VLServlet;
.source "VVServlet.java"


# static fields
.field private static final EXECUTOR:Ljava/util/concurrent/ThreadPoolExecutor;

.field public static final MIN_DM_PROTOCOL_VERSION:Ljava/lang/Double;

.field private static final WORKFLOW_MANAGER:Lcom/vlingo/workflow/WorkflowManager;

.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private ivRequestTimeout:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 67
    const-class v2, Lcom/vlingo/voicepad/vvs/VVServlet;

    invoke-static {v2}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v2

    sput-object v2, Lcom/vlingo/voicepad/vvs/VVServlet;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    .line 84
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    sput-object v2, Lcom/vlingo/voicepad/vvs/VVServlet;->MIN_DM_PROTOCOL_VERSION:Ljava/lang/Double;

    .line 91
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 94
    .local v1, "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/vlingo/mda/model/PackageMeta;>;"
    :try_start_0
    const-class v2, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;

    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;

    invoke-virtual {v2}, Lcom/vlingo/voicepad/workflow/model/VVSRequestCondition;->getClassMeta()Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/mda/model/ClassMeta;->getPackageMeta()Lcom/vlingo/mda/model/PackageMeta;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    new-instance v2, Lcom/vlingo/workflow/WorkflowManager;

    const-string/jumbo v3, "/WorkflowCollection.xml"

    invoke-direct {v2, v3, v1}, Lcom/vlingo/workflow/WorkflowManager;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    sput-object v2, Lcom/vlingo/voicepad/vvs/VVServlet;->WORKFLOW_MANAGER:Lcom/vlingo/workflow/WorkflowManager;

    .line 101
    sget-object v2, Lcom/vlingo/voicepad/vvs/VVServlet;->WORKFLOW_MANAGER:Lcom/vlingo/workflow/WorkflowManager;

    invoke-virtual {v2}, Lcom/vlingo/workflow/WorkflowManager;->init()V

    .line 102
    const-string/jumbo v2, "VVServletExecutor"

    invoke-static {v2}, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->make(Ljava/lang/String;)Lcom/vlingo/common/util/VThreadPoolExecutor;

    move-result-object v2

    sput-object v2, Lcom/vlingo/voicepad/vvs/VVServlet;->EXECUTOR:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 103
    return-void

    .line 95
    :catch_0
    move-exception v0

    .line 97
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Error instantiating VCSRequestCondition: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/vlingo/servlet/util/VLServlet;-><init>()V

    .line 79
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/voicepad/vvs/VVServlet;->ivRequestTimeout:I

    .line 107
    return-void
.end method

.method public static isDMRequest(Lcom/vlingo/workflow/model/VRequest;)Z
    .locals 7
    .param p0, "request"    # Lcom/vlingo/workflow/model/VRequest;

    .prologue
    const/4 v2, 0x0

    .line 158
    invoke-static {p0}, Lcom/vlingo/voicepad/vvs/VVServletUtil;->getVVSRequestVersion(Lcom/vlingo/workflow/model/VRequest;)Ljava/lang/String;

    move-result-object v0

    .line 159
    .local v0, "ver":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 160
    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 161
    .local v1, "verD":Ljava/lang/Double;
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    sget-object v5, Lcom/vlingo/voicepad/vvs/VVServlet;->MIN_DM_PROTOCOL_VERSION:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    cmpl-double v3, v3, v5

    if-ltz v3, :cond_0

    const/4 v2, 0x1

    .line 163
    .end local v1    # "verD":Ljava/lang/Double;
    :cond_0
    return v2
.end method
