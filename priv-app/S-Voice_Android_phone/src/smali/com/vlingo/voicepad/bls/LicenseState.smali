.class public final enum Lcom/vlingo/voicepad/bls/LicenseState;
.super Ljava/lang/Enum;
.source "LicenseState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/voicepad/bls/LicenseState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/voicepad/bls/LicenseState;

.field public static final enum BEFORE_EFFECTIVE:Lcom/vlingo/voicepad/bls/LicenseState;

.field public static final enum EXPIRED:Lcom/vlingo/voicepad/bls/LicenseState;

.field public static final enum NOT_EXIST:Lcom/vlingo/voicepad/bls/LicenseState;

.field public static final enum NOT_VALID:Lcom/vlingo/voicepad/bls/LicenseState;

.field public static final enum VALID:Lcom/vlingo/voicepad/bls/LicenseState;


# instance fields
.field private ivDoc:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 15
    new-instance v0, Lcom/vlingo/voicepad/bls/LicenseState;

    const-string/jumbo v1, "NOT_EXIST"

    const-string/jumbo v2, "License record not found"

    invoke-direct {v0, v1, v3, v2}, Lcom/vlingo/voicepad/bls/LicenseState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/voicepad/bls/LicenseState;->NOT_EXIST:Lcom/vlingo/voicepad/bls/LicenseState;

    .line 16
    new-instance v0, Lcom/vlingo/voicepad/bls/LicenseState;

    const-string/jumbo v1, "EXPIRED"

    const-string/jumbo v2, "License has ended"

    invoke-direct {v0, v1, v4, v2}, Lcom/vlingo/voicepad/bls/LicenseState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/voicepad/bls/LicenseState;->EXPIRED:Lcom/vlingo/voicepad/bls/LicenseState;

    .line 17
    new-instance v0, Lcom/vlingo/voicepad/bls/LicenseState;

    const-string/jumbo v1, "BEFORE_EFFECTIVE"

    const-string/jumbo v2, "License is not yet active"

    invoke-direct {v0, v1, v5, v2}, Lcom/vlingo/voicepad/bls/LicenseState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/voicepad/bls/LicenseState;->BEFORE_EFFECTIVE:Lcom/vlingo/voicepad/bls/LicenseState;

    .line 18
    new-instance v0, Lcom/vlingo/voicepad/bls/LicenseState;

    const-string/jumbo v1, "NOT_VALID"

    const-string/jumbo v2, "License is not valid"

    invoke-direct {v0, v1, v6, v2}, Lcom/vlingo/voicepad/bls/LicenseState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/voicepad/bls/LicenseState;->NOT_VALID:Lcom/vlingo/voicepad/bls/LicenseState;

    .line 19
    new-instance v0, Lcom/vlingo/voicepad/bls/LicenseState;

    const-string/jumbo v1, "VALID"

    const-string/jumbo v2, "License is valid"

    invoke-direct {v0, v1, v7, v2}, Lcom/vlingo/voicepad/bls/LicenseState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/voicepad/bls/LicenseState;->VALID:Lcom/vlingo/voicepad/bls/LicenseState;

    .line 13
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vlingo/voicepad/bls/LicenseState;

    sget-object v1, Lcom/vlingo/voicepad/bls/LicenseState;->NOT_EXIST:Lcom/vlingo/voicepad/bls/LicenseState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/voicepad/bls/LicenseState;->EXPIRED:Lcom/vlingo/voicepad/bls/LicenseState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/voicepad/bls/LicenseState;->BEFORE_EFFECTIVE:Lcom/vlingo/voicepad/bls/LicenseState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/voicepad/bls/LicenseState;->NOT_VALID:Lcom/vlingo/voicepad/bls/LicenseState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/voicepad/bls/LicenseState;->VALID:Lcom/vlingo/voicepad/bls/LicenseState;

    aput-object v1, v0, v7

    sput-object v0, Lcom/vlingo/voicepad/bls/LicenseState;->$VALUES:[Lcom/vlingo/voicepad/bls/LicenseState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "doc"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    iput-object p3, p0, Lcom/vlingo/voicepad/bls/LicenseState;->ivDoc:Ljava/lang/String;

    .line 26
    return-void
.end method
