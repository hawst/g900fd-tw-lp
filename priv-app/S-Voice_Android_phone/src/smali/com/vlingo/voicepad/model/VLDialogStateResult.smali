.class public Lcom/vlingo/voicepad/model/VLDialogStateResult;
.super Ljava/lang/Object;
.source "VLDialogStateResult.java"


# instance fields
.field private final ivEncodedState:Ljava/lang/String;


# direct methods
.method public constructor <init>([B)V
    .locals 1
    .param p1, "dialogState"    # [B

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {p1}, Lcom/bzbyte/util/Base64;->encodeBytes([B)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/voicepad/model/VLDialogStateResult;->ivEncodedState:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public getDialogState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vlingo/voicepad/model/VLDialogStateResult;->ivEncodedState:Ljava/lang/String;

    return-object v0
.end method
