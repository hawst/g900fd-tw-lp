.class public Lcom/vlingo/voicepad/util/ActionUtil;
.super Ljava/lang/Object;
.source "ActionUtil.java"


# static fields
.field private static ACTION_NAMES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static SERVER_ACTIONS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    .line 30
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->SERVER_ACTIONS:Ljava/util/Set;

    .line 32
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "CloseBrowser"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 33
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "DialPage"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 34
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "EmailPage"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 35
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "FetchConfig"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 36
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "FetchUpdate"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 37
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "HideUI"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 38
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "LaunchApp"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 39
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "LaunchURL"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 40
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "LPAction"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 41
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "NoteToSelfPage"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 42
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "SendHello"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 43
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "SetConfig"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 44
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "SetUpdate"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 45
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "ShowMessage"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 46
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "ShowPage"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 47
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "SMSPage"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 48
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "SocialPage"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 49
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "TAFPage"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 50
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "WebSearchPage"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 51
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "PopulateTextbox"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 52
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "InjectText"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 53
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "Intent"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 54
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "SuperDialContact"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 55
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "SuperDialBiz"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 56
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "LocalSearchPage"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 57
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "UpdatePage"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 58
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "ReplyPage"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 59
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "AnswerQuestion"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 60
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "SpeakMessage"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 61
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->ACTION_NAMES:Ljava/util/Set;

    const-string/jumbo v1, "FetchContent"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 63
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->SERVER_ACTIONS:Ljava/util/Set;

    const-string/jumbo v1, "FetchContent"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 64
    return-void
.end method

.method public static getActionTypeForName(Ljava/lang/String;)Lcom/vlingo/voicepad/model/VLActionResult$ActionType;
    .locals 1
    .param p0, "actionName"    # Ljava/lang/String;

    .prologue
    .line 130
    sget-object v0, Lcom/vlingo/voicepad/util/ActionUtil;->SERVER_ACTIONS:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    sget-object v0, Lcom/vlingo/voicepad/model/VLActionResult$ActionType;->SERVER:Lcom/vlingo/voicepad/model/VLActionResult$ActionType;

    .line 133
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/vlingo/voicepad/model/VLActionResult$ActionType;->CLIENT:Lcom/vlingo/voicepad/model/VLActionResult$ActionType;

    goto :goto_0
.end method
