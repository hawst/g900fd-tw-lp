.class public Lcom/vlingo/voicepad/util/VoicepadProperties;
.super Lcom/vlingo/common/util/DefaultProperties;
.source "VoicepadProperties.java"


# static fields
.field private static final ivInst:Lcom/vlingo/voicepad/util/VoicepadProperties;


# instance fields
.field private ivClientBlacklist:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ivLicensedClients:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/vlingo/voicepad/util/VoicepadProperties;

    invoke-direct {v0}, Lcom/vlingo/voicepad/util/VoicepadProperties;-><init>()V

    sput-object v0, Lcom/vlingo/voicepad/util/VoicepadProperties;->ivInst:Lcom/vlingo/voicepad/util/VoicepadProperties;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 54
    const-string/jumbo v0, "/Voicepad.properties"

    invoke-direct {p0, v0}, Lcom/vlingo/common/util/DefaultProperties;-><init>(Ljava/lang/String;)V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/voicepad/util/VoicepadProperties;->ivLicensedClients:Ljava/util/List;

    .line 55
    invoke-virtual {p0}, Lcom/vlingo/voicepad/util/VoicepadProperties;->initFromResource()V

    .line 57
    invoke-direct {p0}, Lcom/vlingo/voicepad/util/VoicepadProperties;->initLicensedClientList()V

    .line 59
    new-instance v0, Ljava/util/HashSet;

    const-string/jumbo v1, "ClientBlacklist"

    invoke-virtual {p0, v1}, Lcom/vlingo/voicepad/util/VoicepadProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/voicepad/util/VoicepadProperties;->ivClientBlacklist:Ljava/util/Set;

    .line 60
    return-void
.end method

.method private initLicensedClientList()V
    .locals 7

    .prologue
    .line 63
    const-string/jumbo v6, "LicensedClients"

    invoke-virtual {p0, v6}, Lcom/vlingo/voicepad/util/VoicepadProperties;->getRequiredString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 64
    .local v1, "clientList":Ljava/lang/String;
    const-string/jumbo v6, ","

    invoke-virtual {v1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 65
    .local v5, "softwareNames":[Ljava/lang/String;
    move-object v0, v5

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 66
    .local v4, "softwareName":Ljava/lang/String;
    iget-object v6, p0, Lcom/vlingo/voicepad/util/VoicepadProperties;->ivLicensedClients:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 68
    .end local v4    # "softwareName":Ljava/lang/String;
    :cond_0
    return-void
.end method
