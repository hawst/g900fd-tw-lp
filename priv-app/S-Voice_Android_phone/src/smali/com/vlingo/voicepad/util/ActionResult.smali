.class public Lcom/vlingo/voicepad/util/ActionResult;
.super Ljava/lang/Object;
.source "ActionResult.java"

# interfaces
.implements Lcom/vlingo/voicepad/model/VLActionResult;


# instance fields
.field private ivAction:Lcom/vlingo/voicepad/tagtoaction/model/Action;

.field private ivActionType:Lcom/vlingo/voicepad/model/VLActionResult$ActionType;

.field private ivElseClause:Ljava/lang/String;

.field private ivIfClause:Ljava/lang/String;

.field private ivName:Ljava/lang/String;

.field private ivParameters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/vlingo/voicepad/tagtoaction/model/Action;)V
    .locals 3
    .param p1, "action"    # Lcom/vlingo/voicepad/tagtoaction/model/Action;

    .prologue
    .line 109
    invoke-virtual {p1}, Lcom/vlingo/voicepad/tagtoaction/model/Action;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vlingo/voicepad/tagtoaction/model/Action;->getIf()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vlingo/voicepad/tagtoaction/model/Action;->getElse()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/vlingo/voicepad/util/ActionResult;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    iput-object p1, p0, Lcom/vlingo/voicepad/util/ActionResult;->ivAction:Lcom/vlingo/voicepad/tagtoaction/model/Action;

    .line 111
    instance-of v0, p1, Lcom/vlingo/voicepad/tagtoaction/model/LocalServerAction;

    if-eqz v0, :cond_0

    .line 112
    sget-object v0, Lcom/vlingo/voicepad/model/VLActionResult$ActionType;->LOCAL_SERVER:Lcom/vlingo/voicepad/model/VLActionResult$ActionType;

    iput-object v0, p0, Lcom/vlingo/voicepad/util/ActionResult;->ivActionType:Lcom/vlingo/voicepad/model/VLActionResult$ActionType;

    .line 113
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 105
    invoke-direct {p0, p1, v0, v0}, Lcom/vlingo/voicepad/util/ActionResult;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "ifClause"    # Ljava/lang/String;
    .param p3, "elseClause"    # Ljava/lang/String;

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/voicepad/util/ActionResult;->ivParameters:Ljava/util/Map;

    .line 117
    iput-object p1, p0, Lcom/vlingo/voicepad/util/ActionResult;->ivName:Ljava/lang/String;

    .line 118
    iput-object p2, p0, Lcom/vlingo/voicepad/util/ActionResult;->ivIfClause:Ljava/lang/String;

    .line 119
    iput-object p3, p0, Lcom/vlingo/voicepad/util/ActionResult;->ivElseClause:Ljava/lang/String;

    .line 120
    invoke-static {p1}, Lcom/vlingo/voicepad/util/ActionUtil;->getActionTypeForName(Ljava/lang/String;)Lcom/vlingo/voicepad/model/VLActionResult$ActionType;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/voicepad/util/ActionResult;->ivActionType:Lcom/vlingo/voicepad/model/VLActionResult$ActionType;

    .line 121
    return-void
.end method


# virtual methods
.method public addParameter(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 130
    iget-object v0, p0, Lcom/vlingo/voicepad/util/ActionResult;->ivParameters:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    return-void
.end method

.method public getAction()Lcom/vlingo/voicepad/tagtoaction/model/Action;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/vlingo/voicepad/util/ActionResult;->ivAction:Lcom/vlingo/voicepad/tagtoaction/model/Action;

    return-object v0
.end method

.method public getActionType()Lcom/vlingo/voicepad/model/VLActionResult$ActionType;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/vlingo/voicepad/util/ActionResult;->ivActionType:Lcom/vlingo/voicepad/model/VLActionResult$ActionType;

    return-object v0
.end method

.method public getElseClause()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/vlingo/voicepad/util/ActionResult;->ivElseClause:Ljava/lang/String;

    return-object v0
.end method

.method public getIfClause()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/vlingo/voicepad/util/ActionResult;->ivIfClause:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/vlingo/voicepad/util/ActionResult;->ivName:Ljava/lang/String;

    return-object v0
.end method

.method public getParameters()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lcom/vlingo/voicepad/util/ActionResult;->ivParameters:Ljava/util/Map;

    return-object v0
.end method
