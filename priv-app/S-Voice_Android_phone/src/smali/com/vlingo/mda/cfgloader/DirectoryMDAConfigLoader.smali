.class public Lcom/vlingo/mda/cfgloader/DirectoryMDAConfigLoader;
.super Lcom/vlingo/mda/cfgloader/BaseMDAConfigLoader;
.source "DirectoryMDAConfigLoader.java"


# instance fields
.field private final ivDirectory:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/lang/Class;)V
    .locals 0
    .param p1, "directory"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/mda/util/MDAObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p2, "configClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/mda/util/MDAObject;>;"
    invoke-direct {p0, p2}, Lcom/vlingo/mda/cfgloader/BaseMDAConfigLoader;-><init>(Ljava/lang/Class;)V

    .line 30
    iput-object p1, p0, Lcom/vlingo/mda/cfgloader/DirectoryMDAConfigLoader;->ivDirectory:Ljava/io/File;

    .line 31
    return-void
.end method


# virtual methods
.method protected getStream(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/vlingo/mda/cfgloader/DirectoryMDAConfigLoader;->ivDirectory:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 36
    .local v0, "pathFile":Ljava/io/File;
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    return-object v1
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vlingo/mda/cfgloader/DirectoryMDAConfigLoader;->ivDirectory:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public matchingPaths(Ljava/lang/String;)Ljava/util/Collection;
    .locals 3
    .param p1, "pathRegex"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 47
    .local v0, "pattern":Ljava/util/regex/Pattern;
    iget-object v1, p0, Lcom/vlingo/mda/cfgloader/DirectoryMDAConfigLoader;->ivDirectory:Ljava/io/File;

    new-instance v2, Lcom/vlingo/mda/cfgloader/DirectoryMDAConfigLoader$1;

    invoke-direct {v2, p0, v0}, Lcom/vlingo/mda/cfgloader/DirectoryMDAConfigLoader$1;-><init>(Lcom/vlingo/mda/cfgloader/DirectoryMDAConfigLoader;Ljava/util/regex/Pattern;)V

    invoke-virtual {v1, v2}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method
