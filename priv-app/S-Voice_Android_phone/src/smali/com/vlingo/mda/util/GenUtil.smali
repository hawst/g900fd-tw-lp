.class public Lcom/vlingo/mda/util/GenUtil;
.super Ljava/lang/Object;
.source "GenUtil.java"


# instance fields
.field private ivGenAvro:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/mda/util/GenUtil;->ivGenAvro:Z

    return-void
.end method

.method private checkParams(Lcom/bzbyte/util/audit/AuditLog;Lorg/w3c/dom/Node;Ljava/util/Set;)V
    .locals 6
    .param p1, "al"    # Lcom/bzbyte/util/audit/AuditLog;
    .param p2, "node"    # Lorg/w3c/dom/Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bzbyte/util/audit/AuditLog;",
            "Lorg/w3c/dom/Node;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 158
    .local p3, "allowedParams":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p2}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v1

    .line 159
    .local v1, "map":Lorg/w3c/dom/NamedNodeMap;
    if-eqz v1, :cond_1

    .line 161
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    invoke-interface {v1}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 163
    invoke-interface {v1, v0}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 164
    .local v3, "nd":Lorg/w3c/dom/Node;
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v2

    .line 165
    .local v2, "name":Ljava/lang/String;
    invoke-interface {p3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string/jumbo v4, "x"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 167
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Unexpected attribute found:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p2}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " expected:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ","

    invoke-static {p3, v5}, Lcom/bzbyte/util/Util;->toString(Ljava/util/Collection;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ",x*"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " name:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "name"

    invoke-static {p2, v5}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, p0, v4}, Lcom/bzbyte/util/audit/AuditLog;->fireError(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 174
    .end local v0    # "index":I
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "nd":Lorg/w3c/dom/Node;
    :cond_1
    return-void
.end method

.method private checkParams(Lcom/bzbyte/util/audit/AuditLog;Lorg/w3c/dom/Node;[Ljava/lang/String;)V
    .locals 5
    .param p1, "al"    # Lcom/bzbyte/util/audit/AuditLog;
    .param p2, "node"    # Lorg/w3c/dom/Node;
    .param p3, "allowedParams"    # [Ljava/lang/String;

    .prologue
    .line 178
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 179
    .local v2, "it":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    move-object v0, p3

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v3, v0, v1

    .line 181
    .local v3, "item":Ljava/lang/String;
    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 179
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 183
    .end local v3    # "item":Ljava/lang/String;
    :cond_0
    invoke-direct {p0, p1, p2, v2}, Lcom/vlingo/mda/util/GenUtil;->checkParams(Lcom/bzbyte/util/audit/AuditLog;Lorg/w3c/dom/Node;Ljava/util/Set;)V

    .line 184
    return-void
.end method

.method private checkRequiredAttributes(Lorg/w3c/dom/Node;Lcom/bzbyte/util/audit/AuditLog;[Ljava/lang/String;)V
    .locals 7
    .param p1, "nd"    # Lorg/w3c/dom/Node;
    .param p2, "al"    # Lcom/bzbyte/util/audit/AuditLog;
    .param p3, "atrs"    # [Ljava/lang/String;

    .prologue
    .line 383
    move-object v0, p3

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 385
    .local v1, "atr":Ljava/lang/String;
    invoke-static {p1, v1}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 387
    .local v4, "val":Ljava/lang/String;
    if-nez v4, :cond_0

    .line 388
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Attribute required but not found:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " atr:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, p0, v5}, Lcom/bzbyte/util/audit/AuditLog;->fireError(Ljava/lang/Object;Ljava/lang/String;)V

    .line 383
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 391
    .end local v1    # "atr":Ljava/lang/String;
    .end local v4    # "val":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public static findByClass(Ljava/util/Collection;Ljava/lang/Class;)Ljava/util/Collection;
    .locals 4
    .param p0, "items"    # Ljava/util/Collection;
    .param p1, "cls"    # Ljava/lang/Class;

    .prologue
    .line 587
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 588
    .local v2, "res":Ljava/util/ArrayList;
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 589
    .local v1, "it":Ljava/lang/Object;
    invoke-virtual {p1, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 590
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 593
    .end local v1    # "it":Ljava/lang/Object;
    :cond_1
    return-object v2
.end method

.method private static isValidChar(C)Z
    .locals 1
    .param p0, "it"    # C

    .prologue
    .line 394
    invoke-static {p0}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x5f

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private linkSuperclasses(Lcom/vlingo/mda/model/ModelMeta;)V
    .locals 7
    .param p1, "mm"    # Lcom/vlingo/mda/model/ModelMeta;

    .prologue
    .line 140
    invoke-virtual {p1}, Lcom/vlingo/mda/model/ModelMeta;->getPackageMetas()Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/mda/model/PackageMeta;

    .line 141
    .local v3, "packageMeta":Lcom/vlingo/mda/model/PackageMeta;
    invoke-virtual {v3}, Lcom/vlingo/mda/model/PackageMeta;->getClassMetas()Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/mda/model/ClassMeta;

    .line 142
    .local v0, "classMeta":Lcom/vlingo/mda/model/ClassMeta;
    invoke-virtual {v0}, Lcom/vlingo/mda/model/ClassMeta;->getExtends()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 143
    invoke-virtual {v0}, Lcom/vlingo/mda/model/ClassMeta;->getExtends()Ljava/lang/String;

    move-result-object v4

    .line 145
    .local v4, "parentClass":Ljava/lang/String;
    invoke-virtual {p1, v4}, Lcom/vlingo/mda/model/ModelMeta;->findClassMeta(Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v5

    .line 146
    .local v5, "parentClassMeta":Lcom/vlingo/mda/model/ClassMeta;
    if-eqz v5, :cond_1

    .line 147
    invoke-virtual {v0, v5}, Lcom/vlingo/mda/model/ClassMeta;->setSuperclassMeta(Lcom/vlingo/mda/model/ClassMeta;)V

    goto :goto_0

    .line 154
    .end local v0    # "classMeta":Lcom/vlingo/mda/model/ClassMeta;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "packageMeta":Lcom/vlingo/mda/model/PackageMeta;
    .end local v4    # "parentClass":Ljava/lang/String;
    .end local v5    # "parentClassMeta":Lcom/vlingo/mda/model/ClassMeta;
    :cond_2
    return-void
.end method

.method private parseAttributes(Lorg/w3c/dom/Node;Ljava/util/Map;)V
    .locals 5
    .param p1, "node"    # Lorg/w3c/dom/Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/w3c/dom/Node;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p2, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v1

    .line 74
    .local v1, "atrs":Lorg/w3c/dom/NamedNodeMap;
    if-eqz v1, :cond_0

    .line 75
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_0
    invoke-interface {v1}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 76
    invoke-interface {v1, v2}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 77
    .local v0, "atr":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 80
    .end local v0    # "atr":Lorg/w3c/dom/Node;
    .end local v2    # "index":I
    :cond_0
    return-void
.end method

.method private parseEnums(Lcom/vlingo/mda/model/SimplePropertyMeta;Lorg/w3c/dom/Node;Lcom/bzbyte/util/audit/AuditLog;)V
    .locals 8
    .param p1, "sp"    # Lcom/vlingo/mda/model/SimplePropertyMeta;
    .param p2, "parentNode"    # Lorg/w3c/dom/Node;
    .param p3, "al"    # Lcom/bzbyte/util/audit/AuditLog;

    .prologue
    .line 500
    invoke-interface {p2}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v3

    .local v3, "nd":Lorg/w3c/dom/Node;
    :goto_0
    if-eqz v3, :cond_4

    .line 502
    invoke-static {v3}, Lcom/bzbyte/util/xml/XMLUtil;->isFiller(Lorg/w3c/dom/Node;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 500
    :goto_1
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v3

    goto :goto_0

    .line 506
    :cond_0
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "Enum"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 508
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string/jumbo v7, "name"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "value"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "doc"

    aput-object v7, v5, v6

    invoke-direct {p0, p3, v3, v5}, Lcom/vlingo/mda/util/GenUtil;->checkParams(Lcom/bzbyte/util/audit/AuditLog;Lorg/w3c/dom/Node;[Ljava/lang/String;)V

    .line 510
    const-string/jumbo v5, "name"

    invoke-static {v3, v5}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 511
    .local v2, "name":Ljava/lang/String;
    const-string/jumbo v5, "value"

    invoke-static {v3, v5}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 512
    .local v4, "value":Ljava/lang/String;
    const-string/jumbo v5, "doc"

    invoke-static {v3, v5}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 513
    .local v0, "doc":Ljava/lang/String;
    if-nez v4, :cond_1

    invoke-virtual {p1}, Lcom/vlingo/mda/model/SimplePropertyMeta;->getJavaType()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "String"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 515
    move-object v4, v2

    .line 518
    :cond_1
    if-nez v4, :cond_2

    .line 520
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Value is required for Enum node name:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p3, p0, v5}, Lcom/bzbyte/util/audit/AuditLog;->fireError(Ljava/lang/Object;Ljava/lang/String;)V

    .line 523
    :cond_2
    new-instance v1, Lcom/vlingo/mda/model/EnumMeta;

    invoke-direct {v1, v2, v4, v0}, Lcom/vlingo/mda/model/EnumMeta;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 524
    .local v1, "em":Lcom/vlingo/mda/model/EnumMeta;
    invoke-virtual {p1}, Lcom/vlingo/mda/model/SimplePropertyMeta;->getEnums()Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v1}, Lcom/vlingo/mda/model/EnumMeta;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 528
    .end local v0    # "doc":Ljava/lang/String;
    .end local v1    # "em":Lcom/vlingo/mda/model/EnumMeta;
    .end local v2    # "name":Ljava/lang/String;
    .end local v4    # "value":Ljava/lang/String;
    :cond_3
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Unrecognized Node:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 531
    :cond_4
    return-void
.end method

.method private static parseNotNull(Lorg/w3c/dom/Node;)Ljava/lang/Boolean;
    .locals 3
    .param p0, "node"    # Lorg/w3c/dom/Node;

    .prologue
    .line 420
    const-string/jumbo v2, "not-null"

    invoke-static {p0, v2}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 422
    .local v0, "notNull":Ljava/lang/String;
    const/4 v1, 0x0

    .line 423
    .local v1, "notNullVal":Ljava/lang/Boolean;
    if-eqz v0, :cond_0

    .line 425
    invoke-static {v0}, Lcom/bzbyte/util/Util;->toBooleanObject(Ljava/lang/Object;)Ljava/lang/Boolean;

    move-result-object v1

    .line 427
    :cond_0
    return-object v1
.end method

.method private parseText(Lcom/bzbyte/util/audit/AuditLog;Lcom/vlingo/mda/model/TextMeta;Lorg/w3c/dom/NodeList;)V
    .locals 4
    .param p1, "al"    # Lcom/bzbyte/util/audit/AuditLog;
    .param p2, "tm"    # Lcom/vlingo/mda/model/TextMeta;
    .param p3, "childNodes"    # Lorg/w3c/dom/NodeList;

    .prologue
    .line 340
    const/4 v0, 0x0

    .local v0, "iii":I
    :goto_0
    invoke-interface {p3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 341
    invoke-interface {p3, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 342
    .local v1, "node":Lorg/w3c/dom/Node;
    invoke-static {v1}, Lcom/bzbyte/util/xml/XMLUtil;->isFiller(Lorg/w3c/dom/Node;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 340
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 343
    :cond_1
    invoke-interface {v1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "Param"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 344
    invoke-direct {p0, p1, v1, p2}, Lcom/vlingo/mda/util/GenUtil;->parseTextParam(Lcom/bzbyte/util/audit/AuditLog;Lorg/w3c/dom/Node;Lcom/vlingo/mda/model/TextMeta;)V

    goto :goto_1

    .line 345
    :cond_2
    invoke-interface {v1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "Body"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 346
    invoke-direct {p0, p2, v1}, Lcom/vlingo/mda/util/GenUtil;->parseTextBody(Lcom/vlingo/mda/model/TextMeta;Lorg/w3c/dom/Node;)V

    goto :goto_1

    .line 349
    .end local v1    # "node":Lorg/w3c/dom/Node;
    :cond_3
    return-void
.end method

.method private parseTextBody(Lcom/vlingo/mda/model/TextMeta;Lorg/w3c/dom/Node;)V
    .locals 8
    .param p1, "tm"    # Lcom/vlingo/mda/model/TextMeta;
    .param p2, "node"    # Lorg/w3c/dom/Node;

    .prologue
    .line 352
    invoke-interface {p2}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 354
    .local v1, "children":Lorg/w3c/dom/NodeList;
    const/4 v2, 0x0

    .local v2, "iii":I
    :goto_0
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    if-ge v2, v5, :cond_4

    .line 355
    invoke-interface {v1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 356
    .local v0, "child":Lorg/w3c/dom/Node;
    invoke-static {v0}, Lcom/bzbyte/util/xml/XMLUtil;->isFiller(Lorg/w3c/dom/Node;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 357
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "\n"

    const-string/jumbo v7, " "

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/vlingo/mda/model/TextMeta;->addBodyElement(Ljava/lang/Object;)V

    .line 354
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 358
    :cond_1
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "Param"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 359
    const-string/jumbo v5, "name"

    invoke-static {v0, v5}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 360
    .local v3, "name":Ljava/lang/String;
    if-nez v3, :cond_2

    .line 361
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "<Param> in <Text name=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/vlingo/mda/model/TextMeta;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\"> must have a name attribute"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 363
    :cond_2
    invoke-virtual {p1, v3}, Lcom/vlingo/mda/model/TextMeta;->getParamMeta(Ljava/lang/String;)Lcom/vlingo/mda/model/ParamMeta;

    move-result-object v4

    .line 364
    .local v4, "pm":Lcom/vlingo/mda/model/ParamMeta;
    if-nez v4, :cond_3

    .line 365
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "<Param name=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\"> referenced in <Body> of <Text name=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/vlingo/mda/model/TextMeta;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\" is not defined."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 367
    :cond_3
    invoke-virtual {p1, v4}, Lcom/vlingo/mda/model/TextMeta;->addBodyElement(Ljava/lang/Object;)V

    goto :goto_1

    .line 370
    .end local v0    # "child":Lorg/w3c/dom/Node;
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "pm":Lcom/vlingo/mda/model/ParamMeta;
    :cond_4
    return-void
.end method

.method private parseTextParam(Lcom/bzbyte/util/audit/AuditLog;Lorg/w3c/dom/Node;Lcom/vlingo/mda/model/TextMeta;)V
    .locals 6
    .param p1, "al"    # Lcom/bzbyte/util/audit/AuditLog;
    .param p2, "node"    # Lorg/w3c/dom/Node;
    .param p3, "tm"    # Lcom/vlingo/mda/model/TextMeta;

    .prologue
    .line 374
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "name"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "type"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "style"

    aput-object v5, v3, v4

    invoke-direct {p0, p1, p2, v3}, Lcom/vlingo/mda/util/GenUtil;->checkParams(Lcom/bzbyte/util/audit/AuditLog;Lorg/w3c/dom/Node;[Ljava/lang/String;)V

    .line 375
    const-string/jumbo v3, "name"

    invoke-static {p2, v3}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 376
    .local v0, "name":Ljava/lang/String;
    const-string/jumbo v3, "type"

    invoke-static {p2, v3}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 377
    .local v2, "type":Ljava/lang/String;
    const-string/jumbo v3, "style"

    invoke-static {p2, v3}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 379
    .local v1, "style":Ljava/lang/String;
    new-instance v3, Lcom/vlingo/mda/model/ParamMeta;

    invoke-direct {v3, v0, v2, v1}, Lcom/vlingo/mda/model/ParamMeta;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p3, v3}, Lcom/vlingo/mda/model/TextMeta;->addParameter(Lcom/vlingo/mda/model/ParamMeta;)V

    .line 380
    return-void
.end method

.method public static toJavaName(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "propName"    # Ljava/lang/String;

    .prologue
    .line 403
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 404
    .local v0, "chars":[C
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    array-length v4, v0

    if-ge v1, v4, :cond_1

    .line 405
    aget-char v2, v0, v1

    .line 406
    .local v2, "it":C
    invoke-static {v2}, Lcom/vlingo/mda/util/GenUtil;->isValidChar(C)Z

    move-result v4

    if-nez v4, :cond_0

    .line 407
    const/16 v4, 0x5f

    aput-char v4, v0, v1

    .line 404
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 410
    .end local v2    # "it":C
    :cond_1
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([C)V

    .line 411
    .local v3, "res":Ljava/lang/String;
    invoke-static {v3}, Lcom/bzbyte/util/Util;->startUpperCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 412
    return-object v3
.end method


# virtual methods
.method public parseClassMeta(Lcom/bzbyte/util/audit/AuditLog;Lcom/vlingo/mda/model/PackageMeta;Lorg/w3c/dom/Node;)Lcom/vlingo/mda/model/ClassMeta;
    .locals 42
    .param p1, "al"    # Lcom/bzbyte/util/audit/AuditLog;
    .param p2, "packageMeta"    # Lcom/vlingo/mda/model/PackageMeta;
    .param p3, "parentNode"    # Lorg/w3c/dom/Node;

    .prologue
    .line 208
    const/16 v4, 0x9

    new-array v4, v4, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string/jumbo v19, "name"

    aput-object v19, v4, v13

    const/4 v13, 0x1

    const-string/jumbo v19, "compact-name"

    aput-object v19, v4, v13

    const/4 v13, 0x2

    const-string/jumbo v19, "table-name"

    aput-object v19, v4, v13

    const/4 v13, 0x3

    const-string/jumbo v19, "extends"

    aput-object v19, v4, v13

    const/4 v13, 0x4

    const-string/jumbo v19, "implements"

    aput-object v19, v4, v13

    const/4 v13, 0x5

    const-string/jumbo v19, "abstract-base"

    aput-object v19, v4, v13

    const/4 v13, 0x6

    const-string/jumbo v19, "messageType"

    aput-object v19, v4, v13

    const/4 v13, 0x7

    const-string/jumbo v19, "doc"

    aput-object v19, v4, v13

    const/16 v13, 0x8

    const-string/jumbo v19, "cache"

    aput-object v19, v4, v13

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2, v4}, Lcom/vlingo/mda/util/GenUtil;->checkParams(Lcom/bzbyte/util/audit/AuditLog;Lorg/w3c/dom/Node;[Ljava/lang/String;)V

    .line 212
    const-string/jumbo v4, "name"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 213
    .local v5, "className":Ljava/lang/String;
    const-string/jumbo v4, "extends"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 214
    .local v6, "extendsStr":Ljava/lang/String;
    const-string/jumbo v4, "implements"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 215
    .local v7, "implementsStr":Ljava/lang/String;
    const-string/jumbo v4, "table-name"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    .line 217
    .local v40, "tableName":Ljava/lang/String;
    const-string/jumbo v4, "messageType"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 219
    .local v8, "messageType":Ljava/lang/String;
    const-string/jumbo v4, "abstract-base"

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v4, v13}, Lcom/bzbyte/util/xml/XMLUtil;->getBooleanAtr(Lorg/w3c/dom/Node;Ljava/lang/String;Z)Z

    move-result v35

    .line 221
    .local v35, "abstractBase":Z
    const-string/jumbo v4, "doc"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 222
    .local v9, "classDoc":Ljava/lang/String;
    const-string/jumbo v4, "cache"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 224
    .local v10, "cacheStrategy":Ljava/lang/String;
    const-string/jumbo v4, "compact-name"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 226
    .local v11, "compactName":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/vlingo/mda/model/PackageMeta;->getOpenClasses()Ljava/util/Set;

    move-result-object v39

    .line 228
    .local v39, "openClasses":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :try_start_0
    move-object/from16 v0, v39

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 230
    new-instance v3, Lcom/vlingo/mda/model/ClassMeta;

    move-object/from16 v4, p2

    invoke-direct/range {v3 .. v11}, Lcom/vlingo/mda/model/ClassMeta;-><init>(Lcom/vlingo/mda/model/PackageMeta;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    .local v3, "clMeta":Lcom/vlingo/mda/model/ClassMeta;
    move-object/from16 v0, v40

    invoke-virtual {v3, v0}, Lcom/vlingo/mda/model/ClassMeta;->setTableName(Ljava/lang/String;)V

    .line 232
    move/from16 v0, v35

    invoke-virtual {v3, v0}, Lcom/vlingo/mda/model/ClassMeta;->setIsAbstractBase(Z)V

    .line 233
    invoke-virtual {v3}, Lcom/vlingo/mda/model/ClassMeta;->getAttributes()Ljava/util/Map;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4}, Lcom/vlingo/mda/util/GenUtil;->parseAttributes(Lorg/w3c/dom/Node;Ljava/util/Map;)V

    .line 235
    invoke-interface/range {p3 .. p3}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v38

    .local v38, "nd":Lorg/w3c/dom/Node;
    :goto_0
    if-eqz v38, :cond_a

    .line 237
    invoke-static/range {v38 .. v38}, Lcom/bzbyte/util/xml/XMLUtil;->isFiller(Lorg/w3c/dom/Node;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 235
    :goto_1
    invoke-interface/range {v38 .. v38}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v38

    goto :goto_0

    .line 239
    :cond_0
    invoke-interface/range {v38 .. v38}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v13, "Text"

    invoke-virtual {v4, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 240
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string/jumbo v19, "name"

    aput-object v19, v4, v13

    const/4 v13, 0x1

    const-string/jumbo v19, "errorEnum"

    aput-object v19, v4, v13

    const/4 v13, 0x2

    const-string/jumbo v19, "doc"

    aput-object v19, v4, v13

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v38

    invoke-direct {v0, v1, v2, v4}, Lcom/vlingo/mda/util/GenUtil;->checkParams(Lcom/bzbyte/util/audit/AuditLog;Lorg/w3c/dom/Node;[Ljava/lang/String;)V

    .line 241
    const-string/jumbo v4, "name"

    move-object/from16 v0, v38

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 242
    .local v14, "name":Ljava/lang/String;
    const-string/jumbo v4, "errorEnum"

    move-object/from16 v0, v38

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 243
    .local v37, "errorEnum":Ljava/lang/String;
    if-nez v37, :cond_1

    .line 244
    move-object/from16 v37, v14

    .line 246
    :cond_1
    const-string/jumbo v4, "doc"

    move-object/from16 v0, v38

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 247
    .local v18, "doc":Ljava/lang/String;
    new-instance v41, Lcom/vlingo/mda/model/TextMeta;

    move-object/from16 v0, v41

    move-object/from16 v1, v37

    move-object/from16 v2, v18

    invoke-direct {v0, v14, v1, v2}, Lcom/vlingo/mda/model/TextMeta;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    .local v41, "tm":Lcom/vlingo/mda/model/TextMeta;
    invoke-interface/range {v38 .. v38}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v41

    invoke-direct {v0, v1, v2, v4}, Lcom/vlingo/mda/util/GenUtil;->parseText(Lcom/bzbyte/util/audit/AuditLog;Lcom/vlingo/mda/model/TextMeta;Lorg/w3c/dom/NodeList;)V

    .line 249
    move-object/from16 v0, v41

    invoke-virtual {v3, v0}, Lcom/vlingo/mda/model/ClassMeta;->addText(Lcom/vlingo/mda/model/TextMeta;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 333
    .end local v3    # "clMeta":Lcom/vlingo/mda/model/ClassMeta;
    .end local v14    # "name":Ljava/lang/String;
    .end local v18    # "doc":Ljava/lang/String;
    .end local v37    # "errorEnum":Ljava/lang/String;
    .end local v38    # "nd":Lorg/w3c/dom/Node;
    .end local v41    # "tm":Lcom/vlingo/mda/model/TextMeta;
    :catchall_0
    move-exception v4

    move-object/from16 v0, v39

    invoke-interface {v0, v11}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    throw v4

    .line 250
    .restart local v3    # "clMeta":Lcom/vlingo/mda/model/ClassMeta;
    .restart local v38    # "nd":Lorg/w3c/dom/Node;
    :cond_2
    :try_start_1
    invoke-interface/range {v38 .. v38}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v13, "Property"

    invoke-virtual {v4, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 251
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v38

    invoke-virtual {v0, v1, v3, v2}, Lcom/vlingo/mda/util/GenUtil;->parsePropertyMeta(Lcom/bzbyte/util/audit/AuditLog;Lcom/vlingo/mda/model/ClassMeta;Lorg/w3c/dom/Node;)Lcom/vlingo/mda/model/PropertyMeta;

    move-result-object v12

    .line 260
    .local v12, "pm":Lcom/vlingo/mda/model/PropertyMeta;
    invoke-virtual {v3, v12}, Lcom/vlingo/mda/model/ClassMeta;->addProperty(Lcom/vlingo/mda/model/PropertyMeta;)V

    .line 261
    invoke-virtual {v12}, Lcom/vlingo/mda/model/PropertyMeta;->getAttributes()Ljava/util/Map;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-direct {v0, v1, v4}, Lcom/vlingo/mda/util/GenUtil;->parseAttributes(Lorg/w3c/dom/Node;Ljava/util/Map;)V

    goto/16 :goto_1

    .line 262
    .end local v12    # "pm":Lcom/vlingo/mda/model/PropertyMeta;
    :cond_3
    invoke-interface/range {v38 .. v38}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v13, "IDProperty"

    invoke-virtual {v4, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 263
    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string/jumbo v19, "name"

    aput-object v19, v4, v13

    const/4 v13, 0x1

    const-string/jumbo v19, "column-name"

    aput-object v19, v4, v13

    const/4 v13, 0x2

    const-string/jumbo v19, "type"

    aput-object v19, v4, v13

    const/4 v13, 0x3

    const-string/jumbo v19, "generator"

    aput-object v19, v4, v13

    const/4 v13, 0x4

    const-string/jumbo v19, "doc"

    aput-object v19, v4, v13

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v38

    invoke-direct {v0, v1, v2, v4}, Lcom/vlingo/mda/util/GenUtil;->checkParams(Lcom/bzbyte/util/audit/AuditLog;Lorg/w3c/dom/Node;[Ljava/lang/String;)V

    .line 264
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string/jumbo v19, "generator"

    aput-object v19, v4, v13

    const/4 v13, 0x1

    const-string/jumbo v19, "type"

    aput-object v19, v4, v13

    const/4 v13, 0x2

    const-string/jumbo v19, "name"

    aput-object v19, v4, v13

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v4}, Lcom/vlingo/mda/util/GenUtil;->checkRequiredAttributes(Lorg/w3c/dom/Node;Lcom/bzbyte/util/audit/AuditLog;[Ljava/lang/String;)V

    .line 265
    const-string/jumbo v4, "name"

    move-object/from16 v0, v38

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 266
    .restart local v14    # "name":Ljava/lang/String;
    const-string/jumbo v4, "type"

    move-object/from16 v0, v38

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 267
    .local v16, "type":Ljava/lang/String;
    const-string/jumbo v4, "generator"

    move-object/from16 v0, v38

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 268
    .local v17, "generator":Ljava/lang/String;
    const-string/jumbo v4, "doc"

    move-object/from16 v0, v38

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 269
    .restart local v18    # "doc":Ljava/lang/String;
    const-string/jumbo v4, "column-name"

    move-object/from16 v0, v38

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 275
    .local v15, "columnName":Ljava/lang/String;
    new-instance v12, Lcom/vlingo/mda/model/IDProperty;

    move-object v13, v3

    invoke-direct/range {v12 .. v18}, Lcom/vlingo/mda/model/IDProperty;-><init>(Lcom/vlingo/mda/model/ClassMeta;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    .local v12, "pm":Lcom/vlingo/mda/model/IDProperty;
    invoke-virtual {v3, v12}, Lcom/vlingo/mda/model/ClassMeta;->addProperty(Lcom/vlingo/mda/model/PropertyMeta;)V

    .line 277
    invoke-virtual {v12}, Lcom/vlingo/mda/model/IDProperty;->getAttributes()Ljava/util/Map;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-direct {v0, v1, v4}, Lcom/vlingo/mda/util/GenUtil;->parseAttributes(Lorg/w3c/dom/Node;Ljava/util/Map;)V

    goto/16 :goto_1

    .line 278
    .end local v12    # "pm":Lcom/vlingo/mda/model/IDProperty;
    .end local v14    # "name":Ljava/lang/String;
    .end local v15    # "columnName":Ljava/lang/String;
    .end local v16    # "type":Ljava/lang/String;
    .end local v17    # "generator":Ljava/lang/String;
    .end local v18    # "doc":Ljava/lang/String;
    :cond_4
    invoke-interface/range {v38 .. v38}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v13, "ReferenceProperty"

    invoke-virtual {v4, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 279
    const/4 v4, 0x7

    new-array v4, v4, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string/jumbo v19, "name"

    aput-object v19, v4, v13

    const/4 v13, 0x1

    const-string/jumbo v19, "type"

    aput-object v19, v4, v13

    const/4 v13, 0x2

    const-string/jumbo v19, "doc"

    aput-object v19, v4, v13

    const/4 v13, 0x3

    const-string/jumbo v19, "index"

    aput-object v19, v4, v13

    const/4 v13, 0x4

    const-string/jumbo v19, "unique-key"

    aput-object v19, v4, v13

    const/4 v13, 0x5

    const-string/jumbo v19, "not-null"

    aput-object v19, v4, v13

    const/4 v13, 0x6

    const-string/jumbo v19, "no-hibernate-field"

    aput-object v19, v4, v13

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v38

    invoke-direct {v0, v1, v2, v4}, Lcom/vlingo/mda/util/GenUtil;->checkParams(Lcom/bzbyte/util/audit/AuditLog;Lorg/w3c/dom/Node;[Ljava/lang/String;)V

    .line 280
    const-string/jumbo v4, "name"

    move-object/from16 v0, v38

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 281
    .restart local v14    # "name":Ljava/lang/String;
    const-string/jumbo v4, "type"

    move-object/from16 v0, v38

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 282
    .restart local v16    # "type":Ljava/lang/String;
    const-string/jumbo v4, "doc"

    move-object/from16 v0, v38

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 283
    .restart local v18    # "doc":Ljava/lang/String;
    const-string/jumbo v4, "index"

    move-object/from16 v0, v38

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 284
    .local v23, "indexName":Ljava/lang/String;
    const-string/jumbo v4, "unique-key"

    move-object/from16 v0, v38

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 286
    .local v24, "unique_key":Ljava/lang/String;
    invoke-static/range {v38 .. v38}, Lcom/vlingo/mda/util/GenUtil;->parseNotNull(Lorg/w3c/dom/Node;)Ljava/lang/Boolean;

    move-result-object v25

    .line 288
    .local v25, "notNullVal":Ljava/lang/Boolean;
    new-instance v12, Lcom/vlingo/mda/model/ReferencePropertyMeta;

    move-object/from16 v19, v12

    move-object/from16 v20, v3

    move-object/from16 v21, v14

    move-object/from16 v22, v16

    move-object/from16 v26, v18

    move-object/from16 v27, v11

    invoke-direct/range {v19 .. v27}, Lcom/vlingo/mda/model/ReferencePropertyMeta;-><init>(Lcom/vlingo/mda/model/ClassMeta;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    .local v12, "pm":Lcom/vlingo/mda/model/ReferencePropertyMeta;
    invoke-virtual {v3, v12}, Lcom/vlingo/mda/model/ClassMeta;->addProperty(Lcom/vlingo/mda/model/PropertyMeta;)V

    .line 298
    invoke-virtual {v12}, Lcom/vlingo/mda/model/ReferencePropertyMeta;->getAttributes()Ljava/util/Map;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-direct {v0, v1, v4}, Lcom/vlingo/mda/util/GenUtil;->parseAttributes(Lorg/w3c/dom/Node;Ljava/util/Map;)V

    goto/16 :goto_1

    .line 299
    .end local v12    # "pm":Lcom/vlingo/mda/model/ReferencePropertyMeta;
    .end local v14    # "name":Ljava/lang/String;
    .end local v16    # "type":Ljava/lang/String;
    .end local v18    # "doc":Ljava/lang/String;
    .end local v23    # "indexName":Ljava/lang/String;
    .end local v24    # "unique_key":Ljava/lang/String;
    .end local v25    # "notNullVal":Ljava/lang/Boolean;
    :cond_5
    invoke-interface/range {v38 .. v38}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v13, "Collection"

    invoke-virtual {v4, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 300
    const/4 v4, 0x7

    new-array v4, v4, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string/jumbo v19, "name"

    aput-object v19, v4, v13

    const/4 v13, 0x1

    const-string/jumbo v19, "compact-name"

    aput-object v19, v4, v13

    const/4 v13, 0x2

    const-string/jumbo v19, "type"

    aput-object v19, v4, v13

    const/4 v13, 0x3

    const-string/jumbo v19, "cardinality"

    aput-object v19, v4, v13

    const/4 v13, 0x4

    const-string/jumbo v19, "collection-type"

    aput-object v19, v4, v13

    const/4 v13, 0x5

    const-string/jumbo v19, "flat"

    aput-object v19, v4, v13

    const/4 v13, 0x6

    const-string/jumbo v19, "no-hibernate-field"

    aput-object v19, v4, v13

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v38

    invoke-direct {v0, v1, v2, v4}, Lcom/vlingo/mda/util/GenUtil;->checkParams(Lcom/bzbyte/util/audit/AuditLog;Lorg/w3c/dom/Node;[Ljava/lang/String;)V

    .line 301
    const-string/jumbo v4, "name"

    move-object/from16 v0, v38

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 302
    .restart local v14    # "name":Ljava/lang/String;
    const-string/jumbo v4, "compact-name"

    move-object/from16 v0, v38

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    .line 304
    .local v33, "collCompactName":Ljava/lang/String;
    const-string/jumbo v4, "type"

    move-object/from16 v0, v38

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 305
    .restart local v16    # "type":Ljava/lang/String;
    const-string/jumbo v4, "cardinality"

    move-object/from16 v0, v38

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 306
    .local v36, "cardinality":Ljava/lang/String;
    if-nez v36, :cond_6

    .line 307
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "cardinality not specified on collection property:"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v19, "."

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v4, v13}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 309
    :cond_6
    const/16 v30, 0x0

    .line 310
    .local v30, "card":Lcom/vlingo/mda/model/Cardinality;
    const-string/jumbo v4, "1-N"

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 311
    sget-object v30, Lcom/vlingo/mda/model/Cardinality;->ONE_TO_N:Lcom/vlingo/mda/model/Cardinality;

    .line 316
    :goto_2
    const-string/jumbo v4, "collection-type"

    move-object/from16 v0, v38

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    .line 317
    .local v32, "collection_type":Ljava/lang/String;
    if-nez v32, :cond_7

    .line 318
    const-string/jumbo v32, "set"

    .line 321
    :cond_7
    const-string/jumbo v4, "flat"

    move-object/from16 v0, v38

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v34

    .line 322
    .local v34, "flat_collection":Z
    new-instance v26, Lcom/vlingo/mda/model/CollectionPropertyMeta;

    const/16 v31, 0x0

    move-object/from16 v27, v3

    move-object/from16 v28, v14

    move-object/from16 v29, v16

    invoke-direct/range {v26 .. v34}, Lcom/vlingo/mda/model/CollectionPropertyMeta;-><init>(Lcom/vlingo/mda/model/ClassMeta;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/mda/model/Cardinality;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 323
    .local v26, "colProp":Lcom/vlingo/mda/model/CollectionPropertyMeta;
    move-object/from16 v0, v26

    invoke-virtual {v3, v0}, Lcom/vlingo/mda/model/ClassMeta;->addProperty(Lcom/vlingo/mda/model/PropertyMeta;)V

    .line 324
    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/mda/model/CollectionPropertyMeta;->getAttributes()Ljava/util/Map;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-direct {v0, v1, v4}, Lcom/vlingo/mda/util/GenUtil;->parseAttributes(Lorg/w3c/dom/Node;Ljava/util/Map;)V

    goto/16 :goto_1

    .line 313
    .end local v26    # "colProp":Lcom/vlingo/mda/model/CollectionPropertyMeta;
    .end local v32    # "collection_type":Ljava/lang/String;
    .end local v34    # "flat_collection":Z
    :cond_8
    sget-object v30, Lcom/vlingo/mda/model/Cardinality;->N_TO_N:Lcom/vlingo/mda/model/Cardinality;

    goto :goto_2

    .line 327
    .end local v14    # "name":Ljava/lang/String;
    .end local v16    # "type":Ljava/lang/String;
    .end local v30    # "card":Lcom/vlingo/mda/model/Cardinality;
    .end local v33    # "collCompactName":Ljava/lang/String;
    .end local v36    # "cardinality":Ljava/lang/String;
    :cond_9
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "Found unexpected node:"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface/range {v38 .. v38}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v4, v13}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 333
    :cond_a
    move-object/from16 v0, v39

    invoke-interface {v0, v11}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-object v3
.end method

.method public parseDocument(Lcom/bzbyte/util/audit/AuditLog;Lorg/w3c/dom/Node;)Lcom/vlingo/mda/model/ModelMeta;
    .locals 5
    .param p1, "al"    # Lcom/bzbyte/util/audit/AuditLog;
    .param p2, "node"    # Lorg/w3c/dom/Node;

    .prologue
    .line 107
    invoke-interface {p2}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v1

    .local v1, "nd":Lorg/w3c/dom/Node;
    :goto_0
    if-eqz v1, :cond_2

    .line 108
    invoke-static {v1}, Lcom/bzbyte/util/xml/XMLUtil;->isFiller(Lorg/w3c/dom/Node;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 107
    invoke-interface {v1}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v1

    goto :goto_0

    .line 110
    :cond_0
    invoke-interface {v1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "Model"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 111
    new-instance v0, Lcom/vlingo/mda/model/ModelMeta;

    invoke-direct {v0}, Lcom/vlingo/mda/model/ModelMeta;-><init>()V

    .line 112
    .local v0, "mm":Lcom/vlingo/mda/model/ModelMeta;
    invoke-virtual {p0, p1, v0, v1}, Lcom/vlingo/mda/util/GenUtil;->parseModelMeta(Lcom/bzbyte/util/audit/AuditLog;Lcom/vlingo/mda/model/ModelMeta;Lorg/w3c/dom/Node;)V

    .line 113
    invoke-virtual {p0, v0}, Lcom/vlingo/mda/util/GenUtil;->postProcess(Lcom/vlingo/mda/model/ModelMeta;)V

    .line 114
    return-object v0

    .line 116
    .end local v0    # "mm":Lcom/vlingo/mda/model/ModelMeta;
    :cond_1
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Found unexpected node:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 119
    :cond_2
    new-instance v2, Ljava/lang/RuntimeException;

    const-string/jumbo v3, "Model not found in document"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public parseModelMeta(Lcom/bzbyte/util/audit/AuditLog;Lcom/vlingo/mda/model/ModelMeta;Lorg/w3c/dom/Node;)V
    .locals 5
    .param p1, "al"    # Lcom/bzbyte/util/audit/AuditLog;
    .param p2, "mm"    # Lcom/vlingo/mda/model/ModelMeta;
    .param p3, "node"    # Lorg/w3c/dom/Node;

    .prologue
    .line 124
    invoke-interface {p3}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v0

    .local v0, "nd":Lorg/w3c/dom/Node;
    :goto_0
    if-eqz v0, :cond_2

    .line 125
    invoke-static {v0}, Lcom/bzbyte/util/xml/XMLUtil;->isFiller(Lorg/w3c/dom/Node;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 124
    :goto_1
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v0

    goto :goto_0

    .line 127
    :cond_0
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "Package"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 128
    invoke-virtual {p0, p1, p2, v0}, Lcom/vlingo/mda/util/GenUtil;->parsePackageMeta(Lcom/bzbyte/util/audit/AuditLog;Lcom/vlingo/mda/model/ModelMeta;Lorg/w3c/dom/Node;)Lcom/vlingo/mda/model/PackageMeta;

    move-result-object v1

    .line 129
    .local v1, "pm":Lcom/vlingo/mda/model/PackageMeta;
    invoke-virtual {p2}, Lcom/vlingo/mda/model/ModelMeta;->getPackageMetas()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v1}, Lcom/vlingo/mda/model/PackageMeta;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 132
    .end local v1    # "pm":Lcom/vlingo/mda/model/PackageMeta;
    :cond_1
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Found unexpected node:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 136
    :cond_2
    invoke-direct {p0, p2}, Lcom/vlingo/mda/util/GenUtil;->linkSuperclasses(Lcom/vlingo/mda/model/ModelMeta;)V

    .line 137
    return-void
.end method

.method public parsePackageMeta(Lcom/bzbyte/util/audit/AuditLog;Lcom/vlingo/mda/model/ModelMeta;Lorg/w3c/dom/Node;)Lcom/vlingo/mda/model/PackageMeta;
    .locals 7
    .param p1, "al"    # Lcom/bzbyte/util/audit/AuditLog;
    .param p2, "mm"    # Lcom/vlingo/mda/model/ModelMeta;
    .param p3, "node"    # Lorg/w3c/dom/Node;

    .prologue
    .line 189
    const-string/jumbo v4, "name"

    invoke-static {p3, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 190
    .local v1, "name":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "name"

    aput-object v6, v4, v5

    invoke-direct {p0, p1, p3, v4}, Lcom/vlingo/mda/util/GenUtil;->checkParams(Lcom/bzbyte/util/audit/AuditLog;Lorg/w3c/dom/Node;[Ljava/lang/String;)V

    .line 191
    new-instance v3, Lcom/vlingo/mda/model/PackageMeta;

    invoke-direct {v3, v1, p2}, Lcom/vlingo/mda/model/PackageMeta;-><init>(Ljava/lang/String;Lcom/vlingo/mda/model/ModelMeta;)V

    .line 193
    .local v3, "pm":Lcom/vlingo/mda/model/PackageMeta;
    invoke-interface {p3}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v2

    .local v2, "nd":Lorg/w3c/dom/Node;
    :goto_0
    if-eqz v2, :cond_2

    .line 194
    invoke-static {v2}, Lcom/bzbyte/util/xml/XMLUtil;->isFiller(Lorg/w3c/dom/Node;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 193
    :goto_1
    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v2

    goto :goto_0

    .line 196
    :cond_0
    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "Class"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 197
    invoke-virtual {p0, p1, v3, v2}, Lcom/vlingo/mda/util/GenUtil;->parseClassMeta(Lcom/bzbyte/util/audit/AuditLog;Lcom/vlingo/mda/model/PackageMeta;Lorg/w3c/dom/Node;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    .line 198
    .local v0, "cm":Lcom/vlingo/mda/model/ClassMeta;
    invoke-virtual {v3}, Lcom/vlingo/mda/model/PackageMeta;->getClassMetas()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v0}, Lcom/vlingo/mda/model/ClassMeta;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    invoke-virtual {v3}, Lcom/vlingo/mda/model/PackageMeta;->getClassMetasByCompactName()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v0}, Lcom/vlingo/mda/model/ClassMeta;->getCompactName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 201
    .end local v0    # "cm":Lcom/vlingo/mda/model/ClassMeta;
    :cond_1
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Found unexpected node:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 204
    :cond_2
    return-object v3
.end method

.method public parsePropertyMeta(Lcom/bzbyte/util/audit/AuditLog;Lcom/vlingo/mda/model/ClassMeta;Lorg/w3c/dom/Node;)Lcom/vlingo/mda/model/PropertyMeta;
    .locals 25
    .param p1, "al"    # Lcom/bzbyte/util/audit/AuditLog;
    .param p2, "cm"    # Lcom/vlingo/mda/model/ClassMeta;
    .param p3, "node"    # Lorg/w3c/dom/Node;

    .prologue
    .line 433
    const/16 v4, 0x11

    new-array v4, v4, [Ljava/lang/String;

    const/16 v23, 0x0

    const-string/jumbo v24, "name"

    aput-object v24, v4, v23

    const/16 v23, 0x1

    const-string/jumbo v24, "compact-name"

    aput-object v24, v4, v23

    const/16 v23, 0x2

    const-string/jumbo v24, "type"

    aput-object v24, v4, v23

    const/16 v23, 0x3

    const-string/jumbo v24, "default-value"

    aput-object v24, v4, v23

    const/16 v23, 0x4

    const-string/jumbo v24, "doc"

    aput-object v24, v4, v23

    const/16 v23, 0x5

    const-string/jumbo v24, "hbtype"

    aput-object v24, v4, v23

    const/16 v23, 0x6

    const-string/jumbo v24, "length"

    aput-object v24, v4, v23

    const/16 v23, 0x7

    const-string/jumbo v24, "index"

    aput-object v24, v4, v23

    const/16 v23, 0x8

    const-string/jumbo v24, "unique-key"

    aput-object v24, v4, v23

    const/16 v23, 0x9

    const-string/jumbo v24, "unique"

    aput-object v24, v4, v23

    const/16 v23, 0xa

    const-string/jumbo v24, "auto-increment"

    aput-object v24, v4, v23

    const/16 v23, 0xb

    const-string/jumbo v24, "db-def"

    aput-object v24, v4, v23

    const/16 v23, 0xc

    const-string/jumbo v24, "column-name"

    aput-object v24, v4, v23

    const/16 v23, 0xd

    const-string/jumbo v24, "sql-type"

    aput-object v24, v4, v23

    const/16 v23, 0xe

    const-string/jumbo v24, "not-null"

    aput-object v24, v4, v23

    const/16 v23, 0xf

    const-string/jumbo v24, "generated"

    aput-object v24, v4, v23

    const/16 v23, 0x10

    const-string/jumbo v24, "no-hibernate-field"

    aput-object v24, v4, v23

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2, v4}, Lcom/vlingo/mda/util/GenUtil;->checkParams(Lcom/bzbyte/util/audit/AuditLog;Lorg/w3c/dom/Node;[Ljava/lang/String;)V

    .line 436
    const-string/jumbo v4, "name"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 437
    .local v22, "orgName":Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Lcom/vlingo/mda/util/GenUtil;->toJavaName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 441
    .local v5, "name":Ljava/lang/String;
    const-string/jumbo v4, "type"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 442
    .local v6, "javatype":Ljava/lang/String;
    const-string/jumbo v4, "length"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 443
    .local v21, "lengthStr":Ljava/lang/String;
    const-string/jumbo v4, "default-value"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 444
    .local v15, "defaultValue":Ljava/lang/String;
    const-string/jumbo v4, "doc"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 445
    .local v19, "doc":Ljava/lang/String;
    const-string/jumbo v4, "compact-name"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 447
    .local v20, "compactName":Ljava/lang/String;
    const-string/jumbo v4, "hbtype"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 448
    .local v7, "hibernateType":Ljava/lang/String;
    const-string/jumbo v4, "sql-type"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 449
    .local v8, "sqlType":Ljava/lang/String;
    const-string/jumbo v4, "index"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 450
    .local v10, "indexName":Ljava/lang/String;
    const-string/jumbo v4, "unique-key"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 451
    .local v11, "unique_key":Ljava/lang/String;
    const-string/jumbo v4, "unique"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 453
    .local v12, "unique":Ljava/lang/String;
    const-string/jumbo v4, "column-name"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 462
    .local v9, "colName":Ljava/lang/String;
    invoke-static/range {p3 .. p3}, Lcom/vlingo/mda/util/GenUtil;->parseNotNull(Lorg/w3c/dom/Node;)Ljava/lang/Boolean;

    move-result-object v13

    .line 465
    .local v13, "notNullVal":Ljava/lang/Boolean;
    const-string/jumbo v4, "generated"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 467
    .local v17, "generated":Ljava/lang/String;
    const-string/jumbo v4, "db-def"

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 469
    .local v18, "dbDef":Ljava/lang/String;
    const-string/jumbo v4, "auto-increment"

    const/16 v23, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v23

    invoke-static {v0, v4, v1}, Lcom/bzbyte/util/xml/XMLUtil;->getBooleanAtr(Lorg/w3c/dom/Node;Ljava/lang/String;Z)Z

    move-result v16

    .line 471
    .local v16, "autoIncrement":Z
    if-nez v6, :cond_0

    .line 472
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "Property type not specified: "

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lcom/vlingo/mda/model/ClassMeta;->getName()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v23, "."

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-interface {v0, v1, v4}, Lcom/bzbyte/util/audit/AuditLog;->fireError(Ljava/lang/Object;Ljava/lang/String;)V

    .line 475
    :cond_0
    const/4 v14, 0x0

    .line 476
    .local v14, "length":Ljava/lang/Integer;
    if-eqz v21, :cond_1

    .line 477
    new-instance v14, Ljava/lang/Integer;

    .end local v14    # "length":Ljava/lang/Integer;
    move-object/from16 v0, v21

    invoke-direct {v14, v0}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    .line 479
    .restart local v14    # "length":Ljava/lang/Integer;
    :cond_1
    const-string/jumbo v4, "String"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 480
    if-nez v21, :cond_2

    .line 481
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "Length required for property:"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-interface {v0, v1, v4}, Lcom/bzbyte/util/audit/AuditLog;->fireError(Ljava/lang/Object;Ljava/lang/String;)V

    .line 487
    :cond_2
    new-instance v3, Lcom/vlingo/mda/model/SimplePropertyMeta;

    move-object/from16 v4, p2

    invoke-direct/range {v3 .. v20}, Lcom/vlingo/mda/model/SimplePropertyMeta;-><init>(Lcom/vlingo/mda/model/ClassMeta;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    .local v3, "sp":Lcom/vlingo/mda/model/SimplePropertyMeta;
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    invoke-direct {v0, v3, v1, v2}, Lcom/vlingo/mda/util/GenUtil;->parseEnums(Lcom/vlingo/mda/model/SimplePropertyMeta;Lorg/w3c/dom/Node;Lcom/bzbyte/util/audit/AuditLog;)V

    .line 494
    return-object v3
.end method

.method public postProcess(Lcom/vlingo/mda/model/ModelMeta;)V
    .locals 14
    .param p1, "mm"    # Lcom/vlingo/mda/model/ModelMeta;

    .prologue
    .line 84
    invoke-virtual {p1}, Lcom/vlingo/mda/model/ModelMeta;->getPackageMetas()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/vlingo/mda/model/PackageMeta;

    .line 85
    .local v11, "pm":Lcom/vlingo/mda/model/PackageMeta;
    invoke-virtual {v11}, Lcom/vlingo/mda/model/PackageMeta;->getClassMetas()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/mda/model/ClassMeta;

    .line 87
    .local v1, "cm":Lcom/vlingo/mda/model/ClassMeta;
    const-class v2, Lcom/vlingo/mda/model/IDProperty;

    invoke-virtual {v1, v2}, Lcom/vlingo/mda/model/ClassMeta;->findPropertyMetaByClass(Ljava/lang/Class;)Ljava/util/Collection;

    move-result-object v10

    .line 88
    .local v10, "idProps":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/mda/model/IDProperty;>;"
    invoke-interface {v10}, Ljava/util/Collection;->size()I

    move-result v2

    if-nez v2, :cond_2

    .line 89
    new-instance v0, Lcom/vlingo/mda/model/IDProperty;

    const-string/jumbo v2, "ID"

    const/4 v3, 0x0

    const-string/jumbo v4, "long"

    const-string/jumbo v5, "native"

    const-string/jumbo v6, "Unique primary key"

    invoke-direct/range {v0 .. v6}, Lcom/vlingo/mda/model/IDProperty;-><init>(Lcom/vlingo/mda/model/ClassMeta;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    .local v0, "prop":Lcom/vlingo/mda/model/IDProperty;
    invoke-virtual {v1}, Lcom/vlingo/mda/model/ClassMeta;->getPropertyMetas()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/mda/model/IDProperty;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    .end local v0    # "prop":Lcom/vlingo/mda/model/IDProperty;
    :cond_2
    const-class v2, Lcom/vlingo/mda/model/SimplePropertyMeta;

    invoke-virtual {v1, v2}, Lcom/vlingo/mda/model/ClassMeta;->findPropertyMetaByClass(Ljava/lang/Class;)Ljava/util/Collection;

    move-result-object v13

    .line 93
    .local v13, "simpleProps":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/mda/model/SimplePropertyMeta;>;"
    invoke-interface {v13}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/vlingo/mda/model/SimplePropertyMeta;

    .line 95
    .local v12, "propMeta":Lcom/vlingo/mda/model/SimplePropertyMeta;
    invoke-virtual {v12}, Lcom/vlingo/mda/model/SimplePropertyMeta;->getName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "Created"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0

    .line 104
    .end local v1    # "cm":Lcom/vlingo/mda/model/ClassMeta;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "idProps":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/mda/model/IDProperty;>;"
    .end local v11    # "pm":Lcom/vlingo/mda/model/PackageMeta;
    .end local v12    # "propMeta":Lcom/vlingo/mda/model/SimplePropertyMeta;
    .end local v13    # "simpleProps":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/mda/model/SimplePropertyMeta;>;"
    :cond_4
    return-void
.end method
