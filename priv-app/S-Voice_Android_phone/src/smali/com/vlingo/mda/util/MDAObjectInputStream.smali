.class public Lcom/vlingo/mda/util/MDAObjectInputStream;
.super Ljava/lang/Object;
.source "MDAObjectInputStream.java"


# static fields
.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private ivMetaProvider:Lcom/vlingo/mda/util/MDAMetaProvider;

.field private ivRoot:Lcom/vlingo/common/message/MNode;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/vlingo/mda/util/MDAObjectInputStream;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/mda/util/MDAObjectInputStream;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lcom/vlingo/mda/model/PackageMeta;)V
    .locals 3
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "pm"    # Lcom/vlingo/mda/model/PackageMeta;

    .prologue
    .line 40
    new-instance v0, Lcom/vlingo/mda/util/DefaultMDAMetaProvider;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/vlingo/mda/model/PackageMeta;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-direct {v0, v1}, Lcom/vlingo/mda/util/DefaultMDAMetaProvider;-><init>([Lcom/vlingo/mda/model/PackageMeta;)V

    invoke-direct {p0, p1, v0}, Lcom/vlingo/mda/util/MDAObjectInputStream;-><init>(Ljava/io/InputStream;Lcom/vlingo/mda/util/MDAMetaProvider;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lcom/vlingo/mda/util/MDAMetaProvider;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "metaProvider"    # Lcom/vlingo/mda/util/MDAMetaProvider;

    .prologue
    .line 35
    invoke-static {}, Lcom/vlingo/common/message/XMLCodec;->getInstance()Lcom/vlingo/common/message/XMLCodec;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/vlingo/mda/util/MDAObjectInputStream;-><init>(Ljava/io/InputStream;Lcom/vlingo/mda/util/MDAMetaProvider;Lcom/vlingo/common/message/Codec;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lcom/vlingo/mda/util/MDAMetaProvider;Lcom/vlingo/common/message/Codec;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "metaProvider"    # Lcom/vlingo/mda/util/MDAMetaProvider;
    .param p3, "codec"    # Lcom/vlingo/common/message/Codec;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-interface {p3, p1}, Lcom/vlingo/common/message/Codec;->decode(Ljava/io/InputStream;)Lcom/vlingo/common/message/MNode;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/mda/util/MDAObjectInputStream;->ivRoot:Lcom/vlingo/common/message/MNode;

    .line 30
    iput-object p2, p0, Lcom/vlingo/mda/util/MDAObjectInputStream;->ivMetaProvider:Lcom/vlingo/mda/util/MDAMetaProvider;

    .line 31
    return-void
.end method


# virtual methods
.method public readObject()Lcom/vlingo/mda/util/MDAObject;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vlingo/mda/util/MDAObjectInputStream;->ivRoot:Lcom/vlingo/common/message/MNode;

    iget-object v1, p0, Lcom/vlingo/mda/util/MDAObjectInputStream;->ivMetaProvider:Lcom/vlingo/mda/util/MDAMetaProvider;

    invoke-static {v0, v1}, Lcom/vlingo/mda/util/MDAObjectFromTree;->loadObjectFromTree(Lcom/vlingo/common/message/MNode;Lcom/vlingo/mda/util/MDAMetaProvider;)Lcom/vlingo/mda/util/MDAObject;

    move-result-object v0

    return-object v0
.end method
