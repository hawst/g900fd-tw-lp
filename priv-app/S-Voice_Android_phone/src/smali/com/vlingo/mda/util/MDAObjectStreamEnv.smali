.class public Lcom/vlingo/mda/util/MDAObjectStreamEnv;
.super Ljava/lang/Object;
.source "MDAObjectStreamEnv.java"


# instance fields
.field private ivNum:I

.field private ivObjToInt:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/mda/util/MDAObject;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/mda/util/MDAObjectStreamEnv;->ivObjToInt:Ljava/util/Map;

    .line 14
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/mda/util/MDAObjectStreamEnv;->ivNum:I

    return-void
.end method


# virtual methods
.method public get(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/Integer;
    .locals 1
    .param p1, "obj"    # Lcom/vlingo/mda/util/MDAObject;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/vlingo/mda/util/MDAObjectStreamEnv;->ivObjToInt:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public put(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/Integer;
    .locals 3
    .param p1, "obj"    # Lcom/vlingo/mda/util/MDAObject;

    .prologue
    .line 23
    iget-object v1, p0, Lcom/vlingo/mda/util/MDAObjectStreamEnv;->ivObjToInt:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 25
    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "Object alreaady in cache"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 27
    :cond_0
    new-instance v0, Ljava/lang/Integer;

    iget v1, p0, Lcom/vlingo/mda/util/MDAObjectStreamEnv;->ivNum:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/vlingo/mda/util/MDAObjectStreamEnv;->ivNum:I

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    .line 28
    .local v0, "num":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/vlingo/mda/util/MDAObjectStreamEnv;->ivObjToInt:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    return-object v0
.end method
