.class public Lcom/vlingo/mda/util/DefaultMDAMetaProvider;
.super Ljava/lang/Object;
.source "DefaultMDAMetaProvider.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAMetaProvider;


# static fields
.field private static final ivLogger:Lorg/apache/log4j/Logger;


# instance fields
.field private ivPackages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/mda/model/PackageMeta;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/vlingo/mda/util/DefaultMDAMetaProvider;

    invoke-static {v0}, Lorg/apache/log4j/Logger;->getLogger(Ljava/lang/Class;)Lorg/apache/log4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/mda/util/DefaultMDAMetaProvider;->ivLogger:Lorg/apache/log4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/mda/util/DefaultMDAMetaProvider;->ivPackages:Ljava/util/Set;

    .line 44
    return-void
.end method

.method public constructor <init>([Lcom/vlingo/mda/model/PackageMeta;)V
    .locals 5
    .param p1, "packages"    # [Lcom/vlingo/mda/model/PackageMeta;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v4, Ljava/util/LinkedHashSet;

    invoke-direct {v4}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v4, p0, Lcom/vlingo/mda/util/DefaultMDAMetaProvider;->ivPackages:Ljava/util/Set;

    .line 30
    move-object v0, p1

    .local v0, "arr$":[Lcom/vlingo/mda/model/PackageMeta;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 31
    .local v3, "pm":Lcom/vlingo/mda/model/PackageMeta;
    iget-object v4, p0, Lcom/vlingo/mda/util/DefaultMDAMetaProvider;->ivPackages:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 30
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 32
    .end local v3    # "pm":Lcom/vlingo/mda/model/PackageMeta;
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/mda/util/DefaultMDAMetaProvider;->checkShortNameDups()V

    .line 33
    return-void
.end method

.method private checkShortNameDups()V
    .locals 8

    .prologue
    .line 103
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 104
    .local v3, "names":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/mda/model/ClassMeta;>;"
    iget-object v5, p0, Lcom/vlingo/mda/util/DefaultMDAMetaProvider;->ivPackages:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/mda/model/PackageMeta;

    .line 106
    .local v4, "packageMeta":Lcom/vlingo/mda/model/PackageMeta;
    invoke-virtual {v4}, Lcom/vlingo/mda/model/PackageMeta;->getClassMetas()Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/mda/model/ClassMeta;

    .line 108
    .local v0, "cm":Lcom/vlingo/mda/model/ClassMeta;
    invoke-virtual {v0}, Lcom/vlingo/mda/model/ClassMeta;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 110
    sget-object v6, Lcom/vlingo/mda/util/DefaultMDAMetaProvider;->ivLogger:Lorg/apache/log4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Multiple objects with same name, may cause errors during parsing. name:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/vlingo/mda/model/ClassMeta;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v7, " cls:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/vlingo/mda/model/ClassMeta;->getFullName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v7, " other:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/vlingo/mda/model/ClassMeta;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/mda/model/ClassMeta;

    invoke-virtual {v5}, Lcom/vlingo/mda/model/ClassMeta;->getFullName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Lorg/apache/log4j/Logger;->error(Ljava/lang/Object;)V

    goto :goto_0

    .line 115
    :cond_1
    invoke-virtual {v0}, Lcom/vlingo/mda/model/ClassMeta;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 119
    .end local v0    # "cm":Lcom/vlingo/mda/model/ClassMeta;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "packageMeta":Lcom/vlingo/mda/model/PackageMeta;
    :cond_2
    return-void
.end method


# virtual methods
.method public findClassMeta(Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;
    .locals 7
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 123
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 124
    .local v3, "res":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/mda/model/ClassMeta;>;"
    iget-object v4, p0, Lcom/vlingo/mda/util/DefaultMDAMetaProvider;->ivPackages:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/mda/model/PackageMeta;

    .line 126
    .local v2, "packageMeta":Lcom/vlingo/mda/model/PackageMeta;
    invoke-virtual {v2}, Lcom/vlingo/mda/model/PackageMeta;->getClassMetas()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/mda/model/ClassMeta;

    .line 127
    .local v0, "cm":Lcom/vlingo/mda/model/ClassMeta;
    if-eqz v0, :cond_0

    .line 129
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 132
    .end local v0    # "cm":Lcom/vlingo/mda/model/ClassMeta;
    .end local v2    # "packageMeta":Lcom/vlingo/mda/model/PackageMeta;
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_2

    .line 133
    const/4 v4, 0x0

    .line 135
    :goto_1
    return-object v4

    .line 134
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_3

    .line 135
    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/mda/model/ClassMeta;

    goto :goto_1

    .line 137
    :cond_3
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Found more than one class meta matching name:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " found:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ","

    invoke-static {v3, v6}, Lcom/bzbyte/util/Util;->toString(Ljava/util/Collection;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public findClassMetaByCompactName(Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;
    .locals 7
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 142
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 143
    .local v3, "res":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/mda/model/ClassMeta;>;"
    iget-object v4, p0, Lcom/vlingo/mda/util/DefaultMDAMetaProvider;->ivPackages:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/mda/model/PackageMeta;

    .line 145
    .local v2, "packageMeta":Lcom/vlingo/mda/model/PackageMeta;
    invoke-virtual {v2}, Lcom/vlingo/mda/model/PackageMeta;->getClassMetasByCompactName()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/mda/model/ClassMeta;

    .line 146
    .local v0, "cm":Lcom/vlingo/mda/model/ClassMeta;
    if-eqz v0, :cond_0

    .line 148
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 151
    .end local v0    # "cm":Lcom/vlingo/mda/model/ClassMeta;
    .end local v2    # "packageMeta":Lcom/vlingo/mda/model/PackageMeta;
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_2

    .line 152
    const/4 v4, 0x0

    .line 154
    :goto_1
    return-object v4

    .line 153
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_3

    .line 154
    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/mda/model/ClassMeta;

    goto :goto_1

    .line 156
    :cond_3
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Found more than one class meta matching name:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " found:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ","

    invoke-static {v3, v6}, Lcom/bzbyte/util/Util;->toString(Ljava/util/Collection;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
.end method
