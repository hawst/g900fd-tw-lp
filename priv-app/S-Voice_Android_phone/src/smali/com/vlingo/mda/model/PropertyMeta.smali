.class public Lcom/vlingo/mda/model/PropertyMeta;
.super Ljava/lang/Object;
.source "PropertyMeta.java"


# instance fields
.field private ivAttributes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ivClassMeta:Lcom/vlingo/mda/model/ClassMeta;

.field private ivCompactName:Ljava/lang/String;

.field private ivDoc:Ljava/lang/String;

.field private ivJavaType:Ljava/lang/String;

.field private ivName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vlingo/mda/model/ClassMeta;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "cm"    # Lcom/vlingo/mda/model/ClassMeta;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "javaType"    # Ljava/lang/String;
    .param p4, "doc"    # Ljava/lang/String;
    .param p5, "compactName"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/mda/model/PropertyMeta;->ivAttributes:Ljava/util/HashMap;

    .line 25
    iput-object p1, p0, Lcom/vlingo/mda/model/PropertyMeta;->ivClassMeta:Lcom/vlingo/mda/model/ClassMeta;

    .line 26
    iput-object p2, p0, Lcom/vlingo/mda/model/PropertyMeta;->ivName:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lcom/vlingo/mda/model/PropertyMeta;->ivJavaType:Ljava/lang/String;

    .line 28
    iput-object p4, p0, Lcom/vlingo/mda/model/PropertyMeta;->ivDoc:Ljava/lang/String;

    .line 29
    iput-object p5, p0, Lcom/vlingo/mda/model/PropertyMeta;->ivCompactName:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public getAttributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/vlingo/mda/model/PropertyMeta;->ivAttributes:Ljava/util/HashMap;

    return-object v0
.end method

.method public getCompactName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/vlingo/mda/model/PropertyMeta;->ivCompactName:Ljava/lang/String;

    return-object v0
.end method

.method public getJavaType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vlingo/mda/model/PropertyMeta;->ivJavaType:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/vlingo/mda/model/PropertyMeta;->ivName:Ljava/lang/String;

    return-object v0
.end method
