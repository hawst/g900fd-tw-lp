.class public Lcom/vlingo/mda/model/IDProperty;
.super Lcom/vlingo/mda/model/PropertyMeta;
.source "IDProperty.java"


# instance fields
.field private ivColumnName:Ljava/lang/String;

.field private ivGenerator:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vlingo/mda/model/ClassMeta;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "cm"    # Lcom/vlingo/mda/model/ClassMeta;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "columnName"    # Ljava/lang/String;
    .param p4, "javaType"    # Ljava/lang/String;
    .param p5, "generator"    # Ljava/lang/String;
    .param p6, "doc"    # Ljava/lang/String;

    .prologue
    .line 15
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p6

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/mda/model/PropertyMeta;-><init>(Lcom/vlingo/mda/model/ClassMeta;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    iput-object p3, p0, Lcom/vlingo/mda/model/IDProperty;->ivColumnName:Ljava/lang/String;

    .line 17
    if-nez p5, :cond_0

    .line 18
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Generator is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_0
    iput-object p5, p0, Lcom/vlingo/mda/model/IDProperty;->ivGenerator:Ljava/lang/String;

    .line 21
    return-void
.end method
