.class public Lcom/vlingo/mda/model/ParamMeta;
.super Ljava/lang/Object;
.source "ParamMeta.java"


# instance fields
.field private ivIdx:I

.field private ivName:Ljava/lang/String;

.field private ivStyle:Ljava/lang/String;

.field private ivType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "style"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/vlingo/mda/model/ParamMeta;->ivName:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lcom/vlingo/mda/model/ParamMeta;->ivType:Ljava/lang/String;

    .line 40
    iput-object p3, p0, Lcom/vlingo/mda/model/ParamMeta;->ivStyle:Ljava/lang/String;

    .line 41
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/mda/model/ParamMeta;->ivName:Ljava/lang/String;

    return-object v0
.end method

.method public setIdx(I)V
    .locals 0
    .param p1, "idx"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/vlingo/mda/model/ParamMeta;->ivIdx:I

    .line 45
    return-void
.end method
