.class public Lcom/vlingo/mda/model/ColumnPropertyMeta;
.super Lcom/vlingo/mda/model/PropertyMeta;
.source "ColumnPropertyMeta.java"


# instance fields
.field private ivIndexName:Ljava/lang/String;

.field private ivNotNull:Ljava/lang/Boolean;

.field private ivUniqueKey:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vlingo/mda/model/ClassMeta;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "cm"    # Lcom/vlingo/mda/model/ClassMeta;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "javaType"    # Ljava/lang/String;
    .param p4, "indexName"    # Ljava/lang/String;
    .param p5, "uniqueKey"    # Ljava/lang/String;
    .param p6, "notNull"    # Ljava/lang/Boolean;
    .param p7, "doc"    # Ljava/lang/String;
    .param p8, "compactName"    # Ljava/lang/String;

    .prologue
    .line 17
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p7

    move-object v5, p8

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/mda/model/PropertyMeta;-><init>(Lcom/vlingo/mda/model/ClassMeta;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    iput-object p4, p0, Lcom/vlingo/mda/model/ColumnPropertyMeta;->ivIndexName:Ljava/lang/String;

    .line 19
    iput-object p5, p0, Lcom/vlingo/mda/model/ColumnPropertyMeta;->ivUniqueKey:Ljava/lang/String;

    .line 20
    iput-object p6, p0, Lcom/vlingo/mda/model/ColumnPropertyMeta;->ivNotNull:Ljava/lang/Boolean;

    .line 22
    return-void
.end method
