.class public final enum Lcom/vlingo/mda/model/Cardinality;
.super Ljava/lang/Enum;
.source "Cardinality.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/mda/model/Cardinality;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/mda/model/Cardinality;

.field public static final enum N_TO_N:Lcom/vlingo/mda/model/Cardinality;

.field public static final enum ONE_TO_N:Lcom/vlingo/mda/model/Cardinality;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9
    new-instance v0, Lcom/vlingo/mda/model/Cardinality;

    const-string/jumbo v1, "ONE_TO_N"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/mda/model/Cardinality;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/mda/model/Cardinality;->ONE_TO_N:Lcom/vlingo/mda/model/Cardinality;

    new-instance v0, Lcom/vlingo/mda/model/Cardinality;

    const-string/jumbo v1, "N_TO_N"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/mda/model/Cardinality;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/mda/model/Cardinality;->N_TO_N:Lcom/vlingo/mda/model/Cardinality;

    .line 7
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/mda/model/Cardinality;

    sget-object v1, Lcom/vlingo/mda/model/Cardinality;->ONE_TO_N:Lcom/vlingo/mda/model/Cardinality;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/mda/model/Cardinality;->N_TO_N:Lcom/vlingo/mda/model/Cardinality;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/mda/model/Cardinality;->$VALUES:[Lcom/vlingo/mda/model/Cardinality;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method
