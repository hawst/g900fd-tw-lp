.class public abstract Lcom/vlingo/servlet/util/BaseServlet;
.super Lcom/vlingo/servlet/util/JMXServlet;
.source "BaseServlet.java"


# static fields
.field protected static final ivDisplayMessage:Lcom/vlingo/servlet/util/DisplayMessageMapper;

.field private static ivLogger:Lcom/vlingo/common/log4j/VLogger;

.field private static ivNonVLogger:Lorg/apache/log4j/Logger;

.field private static ivRequestLogger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private ivAuditExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

.field private ivInitHttpSession:Z

.field private ivMetrics:Lcom/vlingo/servlet/util/VLServletMetrics;

.field private ivShowResponseAsInfo:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/vlingo/servlet/util/BaseServlet;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/servlet/util/BaseServlet;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    .line 37
    const-class v0, Lcom/vlingo/servlet/util/BaseServlet;

    invoke-static {v0}, Lorg/apache/log4j/Logger;->getLogger(Ljava/lang/Class;)Lorg/apache/log4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/servlet/util/BaseServlet;->ivNonVLogger:Lorg/apache/log4j/Logger;

    .line 40
    const-string/jumbo v0, "request.info"

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/String;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/servlet/util/BaseServlet;->ivRequestLogger:Lcom/vlingo/common/log4j/VLogger;

    .line 49
    new-instance v0, Lcom/vlingo/servlet/util/DisplayMessageMapper;

    invoke-direct {v0}, Lcom/vlingo/servlet/util/DisplayMessageMapper;-><init>()V

    sput-object v0, Lcom/vlingo/servlet/util/BaseServlet;->ivDisplayMessage:Lcom/vlingo/servlet/util/DisplayMessageMapper;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/servlet/util/BaseServlet;-><init>(Z)V

    .line 59
    return-void
.end method

.method protected constructor <init>(Z)V
    .locals 2
    .param p1, "initHttpSession"    # Z

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-direct {p0}, Lcom/vlingo/servlet/util/JMXServlet;-><init>()V

    .line 42
    iput-object v1, p0, Lcom/vlingo/servlet/util/BaseServlet;->ivAuditExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/servlet/util/BaseServlet;->ivShowResponseAsInfo:Z

    .line 51
    iput-object v1, p0, Lcom/vlingo/servlet/util/BaseServlet;->ivMetrics:Lcom/vlingo/servlet/util/VLServletMetrics;

    .line 62
    iput-boolean p1, p0, Lcom/vlingo/servlet/util/BaseServlet;->ivInitHttpSession:Z

    .line 64
    return-void
.end method
