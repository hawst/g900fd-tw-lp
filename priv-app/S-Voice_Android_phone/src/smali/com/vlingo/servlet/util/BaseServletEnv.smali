.class public Lcom/vlingo/servlet/util/BaseServletEnv;
.super Ljava/lang/Object;
.source "BaseServletEnv.java"


# static fields
.field private static ivLocalHostName:Ljava/lang/String;

.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private ivActiveLocale:Ljava/util/Locale;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    const-class v1, Lcom/vlingo/servlet/util/BaseServletEnv;

    invoke-static {v1}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v1

    sput-object v1, Lcom/vlingo/servlet/util/BaseServletEnv;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    .line 44
    :try_start_0
    invoke-static {}, Ljava/net/InetAddress;->getLocalHost()Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/vlingo/servlet/util/BaseServletEnv;->ivLocalHostName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 46
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 48
    .restart local v0    # "e":Ljava/lang/Exception;
    sget-object v1, Lcom/vlingo/servlet/util/BaseServletEnv;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v2, "Failed to determine local host name"

    invoke-static {v2}, Lcom/vlingo/message/model/response/GeneralError;->getFailedOperation(Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/vlingo/common/log4j/VLogger;->error(Lcom/vlingo/common/message/VMessage;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public getActiveLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/vlingo/servlet/util/BaseServletEnv;->ivActiveLocale:Ljava/util/Locale;

    return-object v0
.end method
