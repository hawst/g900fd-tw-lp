.class public Lcom/vlingo/servlet/util/DisplayMessageMapper;
.super Ljava/lang/Object;
.source "DisplayMessageMapper.java"


# static fields
.field private static ivLogger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private ivMapped:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/common/message/VMessage;",
            ">;"
        }
    .end annotation
.end field

.field private ivPassThru:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/vlingo/servlet/util/DisplayMessageMapper;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/servlet/util/DisplayMessageMapper;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/servlet/util/DisplayMessageMapper;->ivPassThru:Ljava/util/HashSet;

    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/servlet/util/DisplayMessageMapper;->ivMapped:Ljava/util/HashMap;

    return-void
.end method
