.class public abstract Lcom/vlingo/servlet/util/VLServlet;
.super Lcom/vlingo/servlet/util/BaseServlet;
.source "VLServlet.java"


# static fields
.field private static PROTOCOL_VERSIONS:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SUPPORTED_ENCODINGS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static ivLogger:Lcom/vlingo/common/log4j/VLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 30
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/vlingo/servlet/util/VLServlet;->PROTOCOL_VERSIONS:Ljava/util/HashSet;

    .line 31
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "text/xml"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/vlingo/servlet/util/VLServlet;->SUPPORTED_ENCODINGS:Ljava/util/Set;

    .line 35
    const-class v0, Lcom/vlingo/servlet/util/VLServlet;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/String;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/servlet/util/VLServlet;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    .line 39
    sget-object v0, Lcom/vlingo/servlet/util/VLServlet;->PROTOCOL_VERSIONS:Ljava/util/HashSet;

    const-string/jumbo v1, "3.0"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 40
    sget-object v0, Lcom/vlingo/servlet/util/VLServlet;->PROTOCOL_VERSIONS:Ljava/util/HashSet;

    const-string/jumbo v1, "3.1"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 41
    sget-object v0, Lcom/vlingo/servlet/util/VLServlet;->PROTOCOL_VERSIONS:Ljava/util/HashSet;

    const-string/jumbo v1, "3.2"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 42
    sget-object v0, Lcom/vlingo/servlet/util/VLServlet;->PROTOCOL_VERSIONS:Ljava/util/HashSet;

    const-string/jumbo v1, "3.3"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 43
    sget-object v0, Lcom/vlingo/servlet/util/VLServlet;->PROTOCOL_VERSIONS:Ljava/util/HashSet;

    const-string/jumbo v1, "3.4"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 44
    sget-object v0, Lcom/vlingo/servlet/util/VLServlet;->PROTOCOL_VERSIONS:Ljava/util/HashSet;

    const-string/jumbo v1, "3.5"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 45
    sget-object v0, Lcom/vlingo/servlet/util/VLServlet;->PROTOCOL_VERSIONS:Ljava/util/HashSet;

    const-string/jumbo v1, "3.6"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 46
    sget-object v0, Lcom/vlingo/servlet/util/VLServlet;->PROTOCOL_VERSIONS:Ljava/util/HashSet;

    const-string/jumbo v1, "3.7"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 47
    sget-object v0, Lcom/vlingo/servlet/util/VLServlet;->PROTOCOL_VERSIONS:Ljava/util/HashSet;

    const-string/jumbo v1, "3.8"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 48
    sget-object v0, Lcom/vlingo/servlet/util/VLServlet;->PROTOCOL_VERSIONS:Ljava/util/HashSet;

    const-string/jumbo v1, "3.9"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 49
    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/vlingo/servlet/util/BaseServlet;-><init>()V

    .line 58
    return-void
.end method
