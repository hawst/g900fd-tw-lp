.class public final enum Lcom/vlingo/workflow/response/ResponsePartEnum;
.super Ljava/lang/Enum;
.source "ResponsePartEnum.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/workflow/response/ResponsePartEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/workflow/response/ResponsePartEnum;

.field public static final enum ActionList:Lcom/vlingo/workflow/response/ResponsePartEnum;

.field public static final enum Content:Lcom/vlingo/workflow/response/ResponsePartEnum;

.field public static final enum Error:Lcom/vlingo/workflow/response/ResponsePartEnum;

.field public static final enum Location:Lcom/vlingo/workflow/response/ResponsePartEnum;

.field public static final enum None:Lcom/vlingo/workflow/response/ResponsePartEnum;

.field public static final enum Recognition:Lcom/vlingo/workflow/response/ResponsePartEnum;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 15
    new-instance v0, Lcom/vlingo/workflow/response/ResponsePartEnum;

    const-string/jumbo v1, "Recognition"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/workflow/response/ResponsePartEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/workflow/response/ResponsePartEnum;->Recognition:Lcom/vlingo/workflow/response/ResponsePartEnum;

    new-instance v0, Lcom/vlingo/workflow/response/ResponsePartEnum;

    const-string/jumbo v1, "ActionList"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/workflow/response/ResponsePartEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/workflow/response/ResponsePartEnum;->ActionList:Lcom/vlingo/workflow/response/ResponsePartEnum;

    new-instance v0, Lcom/vlingo/workflow/response/ResponsePartEnum;

    const-string/jumbo v1, "Location"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/workflow/response/ResponsePartEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/workflow/response/ResponsePartEnum;->Location:Lcom/vlingo/workflow/response/ResponsePartEnum;

    new-instance v0, Lcom/vlingo/workflow/response/ResponsePartEnum;

    const-string/jumbo v1, "Content"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/workflow/response/ResponsePartEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/workflow/response/ResponsePartEnum;->Content:Lcom/vlingo/workflow/response/ResponsePartEnum;

    new-instance v0, Lcom/vlingo/workflow/response/ResponsePartEnum;

    const-string/jumbo v1, "Error"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/workflow/response/ResponsePartEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/workflow/response/ResponsePartEnum;->Error:Lcom/vlingo/workflow/response/ResponsePartEnum;

    new-instance v0, Lcom/vlingo/workflow/response/ResponsePartEnum;

    const-string/jumbo v1, "None"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/workflow/response/ResponsePartEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/workflow/response/ResponsePartEnum;->None:Lcom/vlingo/workflow/response/ResponsePartEnum;

    .line 14
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/vlingo/workflow/response/ResponsePartEnum;

    sget-object v1, Lcom/vlingo/workflow/response/ResponsePartEnum;->Recognition:Lcom/vlingo/workflow/response/ResponsePartEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/workflow/response/ResponsePartEnum;->ActionList:Lcom/vlingo/workflow/response/ResponsePartEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/workflow/response/ResponsePartEnum;->Location:Lcom/vlingo/workflow/response/ResponsePartEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/workflow/response/ResponsePartEnum;->Content:Lcom/vlingo/workflow/response/ResponsePartEnum;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/workflow/response/ResponsePartEnum;->Error:Lcom/vlingo/workflow/response/ResponsePartEnum;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/workflow/response/ResponsePartEnum;->None:Lcom/vlingo/workflow/response/ResponsePartEnum;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/workflow/response/ResponsePartEnum;->$VALUES:[Lcom/vlingo/workflow/response/ResponsePartEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method
