.class public Lcom/vlingo/workflow/model/RequestConditionBase;
.super Ljava/lang/Object;
.source "RequestConditionBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_AppChannel:Ljava/lang/String; = "AppChannel"

.field public static final PROP_AppID:Ljava/lang/String; = "AppID"

.field public static final PROP_DeviceCarrier:Ljava/lang/String; = "DeviceCarrier"

.field public static final PROP_DeviceCarrierCountry:Ljava/lang/String; = "DeviceCarrierCountry"

.field public static final PROP_DeviceID:Ljava/lang/String; = "DeviceID"

.field public static final PROP_DeviceMake:Ljava/lang/String; = "DeviceMake"

.field public static final PROP_DeviceModel:Ljava/lang/String; = "DeviceModel"

.field public static final PROP_DeviceOS:Ljava/lang/String; = "DeviceOS"

.field public static final PROP_DeviceOSName:Ljava/lang/String; = "DeviceOSName"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Language:Ljava/lang/String; = "Language"

.field public static final PROP_MinSoftwareVersion:Ljava/lang/String; = "MinSoftwareVersion"

.field public static final PROP_ServletName:Ljava/lang/String; = "ServletName"

.field public static final PROP_SoftwareName:Ljava/lang/String; = "SoftwareName"


# instance fields
.field private AppChannel:Ljava/lang/String;

.field private AppID:Ljava/lang/String;

.field private DeviceCarrier:Ljava/lang/String;

.field private DeviceCarrierCountry:Ljava/lang/String;

.field private DeviceID:Ljava/lang/String;

.field private DeviceMake:Ljava/lang/String;

.field private DeviceModel:Ljava/lang/String;

.field private DeviceOS:Ljava/lang/String;

.field private DeviceOSName:Ljava/lang/String;

.field private ID:J

.field private Language:Ljava/lang/String;

.field private MinSoftwareVersion:Ljava/lang/String;

.field private ServletName:Ljava/lang/String;

.field private SoftwareName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 136
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/workflow/model/RequestCondition;

    const-string/jumbo v2, "/WorkflowModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAppChannel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->AppChannel:Ljava/lang/String;

    return-object v0
.end method

.method public getAppID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->AppID:Ljava/lang/String;

    return-object v0
.end method

.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 140
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/workflow/model/RequestCondition;

    const-string/jumbo v2, "/WorkflowModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceCarrier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->DeviceCarrier:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceCarrierCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->DeviceCarrierCountry:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->DeviceID:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceMake()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->DeviceMake:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->DeviceModel:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceOS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->DeviceOS:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceOSName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->DeviceOSName:Ljava/lang/String;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 128
    iget-wide v0, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->ID:J

    return-wide v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->Language:Ljava/lang/String;

    return-object v0
.end method

.method public getMinSoftwareVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->MinSoftwareVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getServletName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->ServletName:Ljava/lang/String;

    return-object v0
.end method

.method public getSoftwareName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->SoftwareName:Ljava/lang/String;

    return-object v0
.end method

.method public setAppChannel(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->AppChannel:Ljava/lang/String;

    .line 62
    return-void
.end method

.method public setAppID(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->AppID:Ljava/lang/String;

    .line 125
    return-void
.end method

.method public setDeviceCarrier(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->DeviceCarrier:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public setDeviceCarrierCountry(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->DeviceCarrierCountry:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public setDeviceID(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->DeviceID:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public setDeviceMake(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->DeviceMake:Ljava/lang/String;

    .line 90
    return-void
.end method

.method public setDeviceModel(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->DeviceModel:Ljava/lang/String;

    .line 97
    return-void
.end method

.method public setDeviceOS(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->DeviceOS:Ljava/lang/String;

    .line 111
    return-void
.end method

.method public setDeviceOSName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->DeviceOSName:Ljava/lang/String;

    .line 104
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 131
    iput-wide p1, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->ID:J

    .line 132
    return-void
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->Language:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public setMinSoftwareVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->MinSoftwareVersion:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public setServletName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->ServletName:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public setSoftwareName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/vlingo/workflow/model/RequestConditionBase;->SoftwareName:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
