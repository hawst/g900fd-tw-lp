.class public Lcom/vlingo/workflow/model/ServletNameEvaluator;
.super Ljava/lang/Object;
.source "ServletNameEvaluator.java"

# interfaces
.implements Lcom/vlingo/workflow/model/EvaluatorInterface;
.implements Ljava/io/Serializable;


# static fields
.field private static final ivLogger:Lorg/apache/log4j/Logger;


# instance fields
.field private condition:Lcom/vlingo/workflow/model/RequestCondition;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/vlingo/workflow/model/ServletNameEvaluator;

    invoke-static {v0}, Lorg/apache/log4j/Logger;->getLogger(Ljava/lang/Class;)Lorg/apache/log4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/workflow/model/ServletNameEvaluator;->ivLogger:Lorg/apache/log4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/vlingo/workflow/model/RequestCondition;)V
    .locals 0
    .param p1, "condition"    # Lcom/vlingo/workflow/model/RequestCondition;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/vlingo/workflow/model/ServletNameEvaluator;->condition:Lcom/vlingo/workflow/model/RequestCondition;

    .line 26
    return-void
.end method


# virtual methods
.method public evaluate(Ljava/lang/String;Lcom/vlingo/workflow/model/VRequest;)Z
    .locals 3
    .param p1, "servletName"    # Ljava/lang/String;
    .param p2, "request"    # Lcom/vlingo/workflow/model/VRequest;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vlingo/workflow/model/ServletNameEvaluator;->condition:Lcom/vlingo/workflow/model/RequestCondition;

    invoke-virtual {v0}, Lcom/vlingo/workflow/model/RequestCondition;->getServletName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 30
    sget-object v0, Lcom/vlingo/workflow/model/ServletNameEvaluator;->ivLogger:Lorg/apache/log4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Evaluating ServletName: expected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/workflow/model/ServletNameEvaluator;->condition:Lcom/vlingo/workflow/model/RequestCondition;

    invoke-virtual {v2}, Lcom/vlingo/workflow/model/RequestCondition;->getServletName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", found "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/log4j/Logger;->trace(Ljava/lang/Object;)V

    .line 31
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/vlingo/workflow/model/ServletNameEvaluator;->condition:Lcom/vlingo/workflow/model/RequestCondition;

    invoke-virtual {v0}, Lcom/vlingo/workflow/model/RequestCondition;->getServletNamePattern()Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/vlingo/workflow/util/Util;->matchesRegex(Ljava/util/regex/Pattern;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 32
    :cond_0
    const/4 v0, 0x0

    .line 34
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
