.class public Lcom/vlingo/workflow/model/RequestCondition;
.super Lcom/vlingo/workflow/model/RequestConditionBase;
.source "RequestCondition.java"


# static fields
.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private appChannelPattern:Ljava/util/regex/Pattern;

.field private appIdPattern:Ljava/util/regex/Pattern;

.field private deviceCarrierCountryPattern:Ljava/util/regex/Pattern;

.field private deviceCarrierPattern:Ljava/util/regex/Pattern;

.field private deviceIdPattern:Ljava/util/regex/Pattern;

.field private deviceMakePattern:Ljava/util/regex/Pattern;

.field private deviceModelPattern:Ljava/util/regex/Pattern;

.field private deviceOsNamePattern:Ljava/util/regex/Pattern;

.field private deviceOsPattern:Ljava/util/regex/Pattern;

.field protected evaluators:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/workflow/model/EvaluatorInterface;",
            ">;"
        }
    .end annotation
.end field

.field private languagePattern:Ljava/util/regex/Pattern;

.field private servletNamePattern:Ljava/util/regex/Pattern;

.field private softwareNamePattern:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/vlingo/workflow/model/RequestCondition;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/workflow/model/RequestCondition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/vlingo/workflow/model/RequestConditionBase;-><init>()V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->evaluators:Ljava/util/ArrayList;

    .line 32
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->evaluators:Ljava/util/ArrayList;

    new-instance v1, Lcom/vlingo/workflow/model/ServletNameEvaluator;

    invoke-direct {v1, p0}, Lcom/vlingo/workflow/model/ServletNameEvaluator;-><init>(Lcom/vlingo/workflow/model/RequestCondition;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 33
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->evaluators:Ljava/util/ArrayList;

    new-instance v1, Lcom/vlingo/workflow/model/SoftwareNameEvaluator;

    invoke-direct {v1, p0}, Lcom/vlingo/workflow/model/SoftwareNameEvaluator;-><init>(Lcom/vlingo/workflow/model/RequestCondition;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->evaluators:Ljava/util/ArrayList;

    new-instance v1, Lcom/vlingo/workflow/model/AppChannelEvaluator;

    invoke-direct {v1, p0}, Lcom/vlingo/workflow/model/AppChannelEvaluator;-><init>(Lcom/vlingo/workflow/model/RequestCondition;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->evaluators:Ljava/util/ArrayList;

    new-instance v1, Lcom/vlingo/workflow/model/MinSoftwareVersionEvaluator;

    invoke-direct {v1, p0}, Lcom/vlingo/workflow/model/MinSoftwareVersionEvaluator;-><init>(Lcom/vlingo/workflow/model/RequestCondition;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->evaluators:Ljava/util/ArrayList;

    new-instance v1, Lcom/vlingo/workflow/model/DeviceCarrierEvaluator;

    invoke-direct {v1, p0}, Lcom/vlingo/workflow/model/DeviceCarrierEvaluator;-><init>(Lcom/vlingo/workflow/model/RequestCondition;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->evaluators:Ljava/util/ArrayList;

    new-instance v1, Lcom/vlingo/workflow/model/DeviceCarrierCountryEvaluator;

    invoke-direct {v1, p0}, Lcom/vlingo/workflow/model/DeviceCarrierCountryEvaluator;-><init>(Lcom/vlingo/workflow/model/RequestCondition;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->evaluators:Ljava/util/ArrayList;

    new-instance v1, Lcom/vlingo/workflow/model/DeviceMakeEvaluator;

    invoke-direct {v1, p0}, Lcom/vlingo/workflow/model/DeviceMakeEvaluator;-><init>(Lcom/vlingo/workflow/model/RequestCondition;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->evaluators:Ljava/util/ArrayList;

    new-instance v1, Lcom/vlingo/workflow/model/DeviceModelEvaluator;

    invoke-direct {v1, p0}, Lcom/vlingo/workflow/model/DeviceModelEvaluator;-><init>(Lcom/vlingo/workflow/model/RequestCondition;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->evaluators:Ljava/util/ArrayList;

    new-instance v1, Lcom/vlingo/workflow/model/DeviceOsNameEvaluator;

    invoke-direct {v1, p0}, Lcom/vlingo/workflow/model/DeviceOsNameEvaluator;-><init>(Lcom/vlingo/workflow/model/RequestCondition;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->evaluators:Ljava/util/ArrayList;

    new-instance v1, Lcom/vlingo/workflow/model/DeviceOsEvaluator;

    invoke-direct {v1, p0}, Lcom/vlingo/workflow/model/DeviceOsEvaluator;-><init>(Lcom/vlingo/workflow/model/RequestCondition;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->evaluators:Ljava/util/ArrayList;

    new-instance v1, Lcom/vlingo/workflow/model/DeviceIdEvaluator;

    invoke-direct {v1, p0}, Lcom/vlingo/workflow/model/DeviceIdEvaluator;-><init>(Lcom/vlingo/workflow/model/RequestCondition;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->evaluators:Ljava/util/ArrayList;

    new-instance v1, Lcom/vlingo/workflow/model/LanguageEvaluator;

    invoke-direct {v1, p0}, Lcom/vlingo/workflow/model/LanguageEvaluator;-><init>(Lcom/vlingo/workflow/model/RequestCondition;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->evaluators:Ljava/util/ArrayList;

    new-instance v1, Lcom/vlingo/workflow/model/AppIdEvaluator;

    invoke-direct {v1, p0}, Lcom/vlingo/workflow/model/AppIdEvaluator;-><init>(Lcom/vlingo/workflow/model/RequestCondition;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    return-void
.end method

.method public static compareVersions(Ljava/lang/String;Ljava/lang/String;)I
    .locals 9
    .param p0, "version1"    # Ljava/lang/String;
    .param p1, "version2"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 325
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 339
    :cond_0
    :goto_0
    return v6

    .line 328
    :cond_1
    const-string/jumbo v7, "\\."

    invoke-virtual {p0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 329
    .local v2, "v1":[Ljava/lang/String;
    const-string/jumbo v7, "\\."

    invoke-virtual {p1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 331
    .local v3, "v2":[Ljava/lang/String;
    array-length v7, v2

    array-length v8, v3

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 332
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_0

    .line 333
    const/4 v4, 0x0

    .local v4, "vc1":I
    const/4 v5, 0x0

    .line 334
    .local v5, "vc2":I
    array-length v7, v2

    if-ge v0, v7, :cond_2

    aget-object v7, v2, v0

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 335
    :cond_2
    array-length v7, v3

    if-ge v0, v7, :cond_3

    aget-object v7, v3, v0

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 336
    :cond_3
    if-le v4, v5, :cond_4

    const/4 v6, 0x1

    goto :goto_0

    .line 337
    :cond_4
    if-ge v4, v5, :cond_5

    const/4 v6, -0x1

    goto :goto_0

    .line 332
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static matchesRegexIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p0, "regex"    # Ljava/lang/String;
    .param p1, "matchString"    # Ljava/lang/String;

    .prologue
    .line 319
    const/4 v2, 0x2

    invoke-static {p0, v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 320
    .local v1, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 321
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    return v2
.end method


# virtual methods
.method public evaluateConditions(Ljava/lang/String;Lcom/vlingo/workflow/model/VRequest;)Z
    .locals 3
    .param p1, "servletName"    # Ljava/lang/String;
    .param p2, "request"    # Lcom/vlingo/workflow/model/VRequest;

    .prologue
    .line 168
    const/4 v1, 0x0

    .line 169
    .local v1, "ret":Z
    iget-object v2, p0, Lcom/vlingo/workflow/model/RequestCondition;->evaluators:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 171
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/workflow/model/EvaluatorInterface;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/workflow/model/EvaluatorInterface;

    invoke-interface {v2, p1, p2}, Lcom/vlingo/workflow/model/EvaluatorInterface;->evaluate(Ljava/lang/String;Lcom/vlingo/workflow/model/VRequest;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 173
    :cond_1
    return v1
.end method

.method public evaluateConditions_old(Ljava/lang/String;Lcom/vlingo/workflow/model/VRequest;)Z
    .locals 16
    .param p1, "servletName"    # Ljava/lang/String;
    .param p2, "request"    # Lcom/vlingo/workflow/model/VRequest;

    .prologue
    .line 178
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getServletName()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_1

    .line 180
    sget-object v13, Lcom/vlingo/workflow/model/RequestCondition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Evaluating ServletName: expected "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getServletName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ", found "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/vlingo/common/log4j/VLogger;->trace(Ljava/lang/Object;)V

    .line 181
    if-eqz p1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getServletName()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v13, v0}, Lcom/vlingo/workflow/model/RequestCondition;->matchesRegexIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_1

    .line 182
    :cond_0
    const/4 v13, 0x0

    .line 316
    :goto_0
    return v13

    .line 205
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getAppChannel()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_3

    .line 207
    const-string/jumbo v13, "x-vlsoftware"

    const-string/jumbo v14, "AppChannel"

    move-object/from16 v0, p2

    invoke-interface {v0, v13, v14}, Lcom/vlingo/workflow/model/VRequest;->parseVLHeaderField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 208
    .local v1, "appChannel":Ljava/lang/String;
    sget-object v13, Lcom/vlingo/workflow/model/RequestCondition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Evaluating AppChannel: expected "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getAppChannel()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ", found "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/vlingo/common/log4j/VLogger;->trace(Ljava/lang/Object;)V

    .line 209
    if-eqz v1, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getAppChannel()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v1}, Lcom/vlingo/workflow/model/RequestCondition;->matchesRegexIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_3

    .line 210
    :cond_2
    const/4 v13, 0x0

    goto :goto_0

    .line 213
    .end local v1    # "appChannel":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getSoftwareName()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_5

    .line 215
    const-string/jumbo v13, "x-vlsoftware"

    const-string/jumbo v14, "Name"

    move-object/from16 v0, p2

    invoke-interface {v0, v13, v14}, Lcom/vlingo/workflow/model/VRequest;->parseVLHeaderField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 216
    .local v11, "softwareName":Ljava/lang/String;
    sget-object v13, Lcom/vlingo/workflow/model/RequestCondition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Evaluating SoftwareName: expected "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getSoftwareName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ", found "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/vlingo/common/log4j/VLogger;->trace(Ljava/lang/Object;)V

    .line 217
    if-eqz v11, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getSoftwareName()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v11}, Lcom/vlingo/workflow/model/RequestCondition;->matchesRegexIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_5

    .line 218
    :cond_4
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 221
    .end local v11    # "softwareName":Ljava/lang/String;
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getMinSoftwareVersion()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_7

    .line 223
    const-string/jumbo v13, "x-vlsoftware"

    const-string/jumbo v14, "Version"

    move-object/from16 v0, p2

    invoke-interface {v0, v13, v14}, Lcom/vlingo/workflow/model/VRequest;->parseVLHeaderField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 224
    .local v12, "softwareVersion":Ljava/lang/String;
    sget-object v13, Lcom/vlingo/workflow/model/RequestCondition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Evaluating MinSoftwareVersion: expected "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getMinSoftwareVersion()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " or higher, found "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/vlingo/common/log4j/VLogger;->trace(Ljava/lang/Object;)V

    .line 225
    if-eqz v12, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getMinSoftwareVersion()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/vlingo/workflow/WorkflowManager;->compareVersions(Ljava/lang/String;Ljava/lang/String;)I

    move-result v13

    if-gez v13, :cond_7

    .line 226
    :cond_6
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 229
    .end local v12    # "softwareVersion":Ljava/lang/String;
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getDeviceCarrier()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_9

    .line 231
    const-string/jumbo v13, "x-vlclient"

    const-string/jumbo v14, "Carrier"

    move-object/from16 v0, p2

    invoke-interface {v0, v13, v14}, Lcom/vlingo/workflow/model/VRequest;->parseVLHeaderField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 232
    .local v3, "carrier":Ljava/lang/String;
    sget-object v13, Lcom/vlingo/workflow/model/RequestCondition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Evaluating DeviceCarrier: expected "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getDeviceCarrier()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ", found "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/vlingo/common/log4j/VLogger;->trace(Ljava/lang/Object;)V

    .line 233
    if-eqz v3, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getDeviceCarrier()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v3}, Lcom/vlingo/workflow/model/RequestCondition;->matchesRegexIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_9

    .line 234
    :cond_8
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 237
    .end local v3    # "carrier":Ljava/lang/String;
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getDeviceCarrierCountry()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_b

    .line 239
    const-string/jumbo v13, "x-vlclient"

    const-string/jumbo v14, "CarrierCountry"

    move-object/from16 v0, p2

    invoke-interface {v0, v13, v14}, Lcom/vlingo/workflow/model/VRequest;->parseVLHeaderField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 240
    .local v4, "country":Ljava/lang/String;
    sget-object v13, Lcom/vlingo/workflow/model/RequestCondition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Evaluating DeviceCarrierCountry: expected "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getDeviceCarrierCountry()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ", found "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/vlingo/common/log4j/VLogger;->trace(Ljava/lang/Object;)V

    .line 241
    if-eqz v4, :cond_a

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getDeviceCarrierCountry()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v4}, Lcom/vlingo/workflow/model/RequestCondition;->matchesRegexIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_b

    .line 242
    :cond_a
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 245
    .end local v4    # "country":Ljava/lang/String;
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getDeviceMake()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_d

    .line 247
    const-string/jumbo v13, "x-vlclient"

    const-string/jumbo v14, "DeviceMake"

    move-object/from16 v0, p2

    invoke-interface {v0, v13, v14}, Lcom/vlingo/workflow/model/VRequest;->parseVLHeaderField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 248
    .local v7, "make":Ljava/lang/String;
    sget-object v13, Lcom/vlingo/workflow/model/RequestCondition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Evaluating DeviceMake: expected "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getDeviceMake()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ", found "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/vlingo/common/log4j/VLogger;->trace(Ljava/lang/Object;)V

    .line 249
    if-eqz v7, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getDeviceMake()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v7}, Lcom/vlingo/workflow/model/RequestCondition;->matchesRegexIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_d

    .line 250
    :cond_c
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 253
    .end local v7    # "make":Ljava/lang/String;
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getDeviceModel()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_f

    .line 255
    const-string/jumbo v13, "x-vlclient"

    const-string/jumbo v14, "DeviceModel"

    move-object/from16 v0, p2

    invoke-interface {v0, v13, v14}, Lcom/vlingo/workflow/model/VRequest;->parseVLHeaderField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 256
    .local v8, "model":Ljava/lang/String;
    sget-object v13, Lcom/vlingo/workflow/model/RequestCondition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Evaluating DeviceModel: expected "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getDeviceModel()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ", found "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/vlingo/common/log4j/VLogger;->trace(Ljava/lang/Object;)V

    .line 257
    if-eqz v8, :cond_e

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getDeviceModel()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v8}, Lcom/vlingo/workflow/model/RequestCondition;->matchesRegexIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_f

    .line 258
    :cond_e
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 261
    .end local v8    # "model":Ljava/lang/String;
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getDeviceOSName()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_11

    .line 263
    const-string/jumbo v13, "x-vlclient"

    const-string/jumbo v14, "DeviceOSName"

    move-object/from16 v0, p2

    invoke-interface {v0, v13, v14}, Lcom/vlingo/workflow/model/VRequest;->parseVLHeaderField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 264
    .local v9, "osName":Ljava/lang/String;
    sget-object v13, Lcom/vlingo/workflow/model/RequestCondition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Evaluating DeviceOSName: expected "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getDeviceOSName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ", found "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/vlingo/common/log4j/VLogger;->trace(Ljava/lang/Object;)V

    .line 265
    if-eqz v9, :cond_10

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getDeviceOSName()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v9}, Lcom/vlingo/workflow/model/RequestCondition;->matchesRegexIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_11

    .line 266
    :cond_10
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 269
    .end local v9    # "osName":Ljava/lang/String;
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getDeviceOS()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_13

    .line 271
    const-string/jumbo v13, "x-vlclient"

    const-string/jumbo v14, "DeviceOS"

    move-object/from16 v0, p2

    invoke-interface {v0, v13, v14}, Lcom/vlingo/workflow/model/VRequest;->parseVLHeaderField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 272
    .local v10, "osVersion":Ljava/lang/String;
    sget-object v13, Lcom/vlingo/workflow/model/RequestCondition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Evaluating DeviceOS: expected "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getDeviceOS()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ", found "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/vlingo/common/log4j/VLogger;->trace(Ljava/lang/Object;)V

    .line 273
    if-eqz v10, :cond_12

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getDeviceOS()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v10}, Lcom/vlingo/workflow/model/RequestCondition;->matchesRegexIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_13

    .line 274
    :cond_12
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 277
    .end local v10    # "osVersion":Ljava/lang/String;
    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getDeviceID()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_15

    .line 279
    const-string/jumbo v13, "x-vlclient"

    const-string/jumbo v14, "DeviceID"

    move-object/from16 v0, p2

    invoke-interface {v0, v13, v14}, Lcom/vlingo/workflow/model/VRequest;->parseVLHeaderField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 280
    .local v5, "id":Ljava/lang/String;
    sget-object v13, Lcom/vlingo/workflow/model/RequestCondition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Evaluating DeviceID: expected "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getDeviceID()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ", found "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/vlingo/common/log4j/VLogger;->trace(Ljava/lang/Object;)V

    .line 281
    if-eqz v5, :cond_14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getDeviceID()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v5}, Lcom/vlingo/workflow/model/RequestCondition;->matchesRegexIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_15

    .line 282
    :cond_14
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 285
    .end local v5    # "id":Ljava/lang/String;
    :cond_15
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getLanguage()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_17

    .line 287
    const-string/jumbo v13, "x-vlclient"

    const-string/jumbo v14, "Language"

    move-object/from16 v0, p2

    invoke-interface {v0, v13, v14}, Lcom/vlingo/workflow/model/VRequest;->parseVLHeaderField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 288
    .local v6, "language":Ljava/lang/String;
    sget-object v13, Lcom/vlingo/workflow/model/RequestCondition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Evaluating Language: expected "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getLanguage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ", found "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/vlingo/common/log4j/VLogger;->trace(Ljava/lang/Object;)V

    .line 289
    if-eqz v6, :cond_16

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getLanguage()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v6}, Lcom/vlingo/workflow/model/RequestCondition;->matchesRegexIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_17

    .line 290
    :cond_16
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 293
    .end local v6    # "language":Ljava/lang/String;
    :cond_17
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getAppID()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_19

    .line 295
    const-string/jumbo v13, "x-vlsr"

    const-string/jumbo v14, "AppID"

    move-object/from16 v0, p2

    invoke-interface {v0, v13, v14}, Lcom/vlingo/workflow/model/VRequest;->parseVLHeaderField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 296
    .local v2, "appID":Ljava/lang/String;
    sget-object v13, Lcom/vlingo/workflow/model/RequestCondition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Evaluating AppID: expected "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getAppID()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ", found "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/vlingo/common/log4j/VLogger;->trace(Ljava/lang/Object;)V

    .line 297
    if-eqz v2, :cond_18

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/workflow/model/RequestCondition;->getAppID()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v2}, Lcom/vlingo/workflow/model/RequestCondition;->matchesRegexIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_19

    .line 298
    :cond_18
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 316
    .end local v2    # "appID":Ljava/lang/String;
    :cond_19
    const/4 v13, 0x1

    goto/16 :goto_0
.end method

.method public getAppChannelPattern()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->appChannelPattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public getAppIdPattern()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->appIdPattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public getDeviceCarrierCountryPattern()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->deviceCarrierCountryPattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public getDeviceCarrierPattern()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->deviceCarrierPattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public getDeviceIdPattern()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->deviceIdPattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public getDeviceMakePattern()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->deviceMakePattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public getDeviceModelPattern()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->deviceModelPattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public getDeviceOsNamePattern()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->deviceOsNamePattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public getDeviceOsPattern()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->deviceOsPattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public getLanguagePattern()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->languagePattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public getServletNamePattern()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->servletNamePattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public getSoftwareNamePattern()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->softwareNamePattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public setAppChannel(Ljava/lang/String;)V
    .locals 1
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 69
    const/4 v0, 0x2

    invoke-static {p1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->appChannelPattern:Ljava/util/regex/Pattern;

    .line 70
    invoke-super {p0, p1}, Lcom/vlingo/workflow/model/RequestConditionBase;->setAppChannel(Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method public setAppID(Ljava/lang/String;)V
    .locals 1
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 159
    const/4 v0, 0x2

    invoke-static {p1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->appIdPattern:Ljava/util/regex/Pattern;

    .line 160
    invoke-super {p0, p1}, Lcom/vlingo/workflow/model/RequestConditionBase;->setAppID(Ljava/lang/String;)V

    .line 161
    return-void
.end method

.method public setDeviceCarrier(Ljava/lang/String;)V
    .locals 1
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 79
    const/4 v0, 0x2

    invoke-static {p1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->deviceCarrierPattern:Ljava/util/regex/Pattern;

    .line 80
    invoke-super {p0, p1}, Lcom/vlingo/workflow/model/RequestConditionBase;->setDeviceCarrier(Ljava/lang/String;)V

    .line 81
    return-void
.end method

.method public setDeviceCarrierCountry(Ljava/lang/String;)V
    .locals 1
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 89
    const/4 v0, 0x2

    invoke-static {p1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->deviceCarrierCountryPattern:Ljava/util/regex/Pattern;

    .line 90
    invoke-super {p0, p1}, Lcom/vlingo/workflow/model/RequestConditionBase;->setDeviceCarrierCountry(Ljava/lang/String;)V

    .line 91
    return-void
.end method

.method public setDeviceID(Ljava/lang/String;)V
    .locals 1
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 149
    const/4 v0, 0x2

    invoke-static {p1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->deviceIdPattern:Ljava/util/regex/Pattern;

    .line 150
    invoke-super {p0, p1}, Lcom/vlingo/workflow/model/RequestConditionBase;->setDeviceID(Ljava/lang/String;)V

    .line 151
    return-void
.end method

.method public setDeviceMake(Ljava/lang/String;)V
    .locals 1
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 109
    const/4 v0, 0x2

    invoke-static {p1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->deviceMakePattern:Ljava/util/regex/Pattern;

    .line 110
    invoke-super {p0, p1}, Lcom/vlingo/workflow/model/RequestConditionBase;->setDeviceMake(Ljava/lang/String;)V

    .line 111
    return-void
.end method

.method public setDeviceModel(Ljava/lang/String;)V
    .locals 1
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 119
    const/4 v0, 0x2

    invoke-static {p1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->deviceModelPattern:Ljava/util/regex/Pattern;

    .line 120
    invoke-super {p0, p1}, Lcom/vlingo/workflow/model/RequestConditionBase;->setDeviceModel(Ljava/lang/String;)V

    .line 121
    return-void
.end method

.method public setDeviceOS(Ljava/lang/String;)V
    .locals 1
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 139
    const/4 v0, 0x2

    invoke-static {p1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->deviceOsPattern:Ljava/util/regex/Pattern;

    .line 140
    invoke-super {p0, p1}, Lcom/vlingo/workflow/model/RequestConditionBase;->setDeviceOS(Ljava/lang/String;)V

    .line 141
    return-void
.end method

.method public setDeviceOSName(Ljava/lang/String;)V
    .locals 1
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 129
    const/4 v0, 0x2

    invoke-static {p1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->deviceOsNamePattern:Ljava/util/regex/Pattern;

    .line 130
    invoke-super {p0, p1}, Lcom/vlingo/workflow/model/RequestConditionBase;->setDeviceOSName(Ljava/lang/String;)V

    .line 131
    return-void
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 1
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 99
    const/4 v0, 0x2

    invoke-static {p1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->languagePattern:Ljava/util/regex/Pattern;

    .line 100
    invoke-super {p0, p1}, Lcom/vlingo/workflow/model/RequestConditionBase;->setLanguage(Ljava/lang/String;)V

    .line 101
    return-void
.end method

.method public setServletName(Ljava/lang/String;)V
    .locals 1
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 49
    const/4 v0, 0x2

    invoke-static {p1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->servletNamePattern:Ljava/util/regex/Pattern;

    .line 50
    invoke-super {p0, p1}, Lcom/vlingo/workflow/model/RequestConditionBase;->setServletName(Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method public setSoftwareName(Ljava/lang/String;)V
    .locals 1
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 59
    const/4 v0, 0x2

    invoke-static {p1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/workflow/model/RequestCondition;->softwareNamePattern:Ljava/util/regex/Pattern;

    .line 60
    invoke-super {p0, p1}, Lcom/vlingo/workflow/model/RequestConditionBase;->setSoftwareName(Ljava/lang/String;)V

    .line 61
    return-void
.end method
