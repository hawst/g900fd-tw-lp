.class public Lcom/vlingo/workflow/model/LoopWorkflow;
.super Lcom/vlingo/workflow/model/LoopWorkflowBase;
.source "LoopWorkflow.java"


# static fields
.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private exitCondition:Lcom/vlingo/workflow/model/ExitCondition;

.field private referencedTaskIds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private taskClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/workflow/task/LoopWorkflowTaskInterface;",
            ">;"
        }
    .end annotation
.end field

.field private taskDefinitionManager:Lcom/vlingo/workflow/TaskDefinitionManager;

.field private templateManager:Lcom/vlingo/workflow/WorkflowTemplateManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/vlingo/workflow/model/LoopWorkflow;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/workflow/model/LoopWorkflow;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Lcom/vlingo/workflow/model/LoopWorkflowBase;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/vlingo/workflow/model/LoopWorkflow;->templateManager:Lcom/vlingo/workflow/WorkflowTemplateManager;

    .line 27
    iput-object v0, p0, Lcom/vlingo/workflow/model/LoopWorkflow;->taskDefinitionManager:Lcom/vlingo/workflow/TaskDefinitionManager;

    .line 28
    iput-object v0, p0, Lcom/vlingo/workflow/model/LoopWorkflow;->taskClass:Ljava/lang/Class;

    .line 29
    iput-object v0, p0, Lcom/vlingo/workflow/model/LoopWorkflow;->exitCondition:Lcom/vlingo/workflow/model/ExitCondition;

    .line 30
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/workflow/model/LoopWorkflow;->referencedTaskIds:Ljava/util/HashSet;

    return-void
.end method


# virtual methods
.method public init(Lcom/vlingo/workflow/WorkflowTemplateManager;Lcom/vlingo/workflow/TaskDefinitionManager;)V
    .locals 13
    .param p1, "templateManager"    # Lcom/vlingo/workflow/WorkflowTemplateManager;
    .param p2, "taskDefinitionManager"    # Lcom/vlingo/workflow/TaskDefinitionManager;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/InstantiationException;
        }
    .end annotation

    .prologue
    .line 33
    iput-object p2, p0, Lcom/vlingo/workflow/model/LoopWorkflow;->taskDefinitionManager:Lcom/vlingo/workflow/TaskDefinitionManager;

    .line 34
    iput-object p1, p0, Lcom/vlingo/workflow/model/LoopWorkflow;->templateManager:Lcom/vlingo/workflow/WorkflowTemplateManager;

    .line 36
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LoopWorkflow;->getType()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/workflow/model/LoopWorkflow;->taskClass:Ljava/lang/Class;

    .line 38
    const-class v10, Lcom/vlingo/workflow/task/WorkflowTask;

    iget-object v11, p0, Lcom/vlingo/workflow/model/LoopWorkflow;->taskClass:Ljava/lang/Class;

    invoke-virtual {v10, v11}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 39
    new-instance v10, Ljava/lang/RuntimeException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LoopWorkflow;->getType()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " is not a subclass of the WorkflowTask"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 41
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LoopWorkflow;->getExitCondition()Lcom/vlingo/workflow/model/ExitCondition;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/workflow/model/LoopWorkflow;->exitCondition:Lcom/vlingo/workflow/model/ExitCondition;

    .line 42
    iget-object v10, p0, Lcom/vlingo/workflow/model/LoopWorkflow;->exitCondition:Lcom/vlingo/workflow/model/ExitCondition;

    invoke-virtual {v10}, Lcom/vlingo/workflow/model/ExitCondition;->init()V

    .line 44
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LoopWorkflow;->getExitCondition()Lcom/vlingo/workflow/model/ExitCondition;

    move-result-object v0

    .line 45
    .local v0, "exitCondition":Lcom/vlingo/workflow/model/ExitCondition;
    invoke-virtual {v0}, Lcom/vlingo/workflow/model/ExitCondition;->init()V

    .line 47
    new-instance v9, Ljava/util/LinkedHashSet;

    invoke-direct {v9}, Ljava/util/LinkedHashSet;-><init>()V

    .line 55
    .local v9, "templateTaskList":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/workflow/model/TaskNode;>;"
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LoopWorkflow;->getTaskNodeList()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "taskNodeIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/workflow/model/TaskNode;>;"
    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 56
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/workflow/model/TaskNode;

    .line 57
    .local v6, "taskNode":Lcom/vlingo/workflow/model/TaskNode;
    instance-of v10, v6, Lcom/vlingo/workflow/model/WorkflowTemplateRef;

    if-eqz v10, :cond_2

    move-object v10, v6

    .line 58
    check-cast v10, Lcom/vlingo/workflow/model/WorkflowTemplateRef;

    invoke-virtual {v10}, Lcom/vlingo/workflow/model/WorkflowTemplateRef;->getWorkflowTemplateName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, Lcom/vlingo/workflow/WorkflowTemplateManager;->getTemplate(Ljava/lang/String;)Lcom/vlingo/workflow/model/WorkflowTemplate;

    move-result-object v8

    .line 59
    .local v8, "template":Lcom/vlingo/workflow/model/WorkflowTemplate;
    invoke-virtual {v8, p1}, Lcom/vlingo/workflow/model/WorkflowTemplate;->getTaskNodeList(Lcom/vlingo/workflow/WorkflowTemplateManager;)Ljava/util/Set;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 60
    invoke-interface {v7}, Ljava/util/Iterator;->remove()V

    .line 62
    .end local v8    # "template":Lcom/vlingo/workflow/model/WorkflowTemplate;
    :cond_2
    instance-of v10, v6, Lcom/vlingo/workflow/model/WorkflowTemplate;

    if-eqz v10, :cond_1

    .line 63
    new-instance v10, Ljava/lang/RuntimeException;

    const-string/jumbo v11, "Workflow templates definitions are not allowed inside the WorkflowDefinition. Workflow templates must be defined in the workflow templates file."

    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 65
    .end local v6    # "taskNode":Lcom/vlingo/workflow/model/TaskNode;
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LoopWorkflow;->getTaskNodeList()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10, v9}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 67
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LoopWorkflow;->getTaskNodeList()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/workflow/model/TaskNode;

    .line 68
    .local v4, "node":Lcom/vlingo/workflow/model/TaskNode;
    check-cast v4, Lcom/vlingo/workflow/model/LeafTaskNode;

    .end local v4    # "node":Lcom/vlingo/workflow/model/TaskNode;
    invoke-virtual {v4, p1, p2}, Lcom/vlingo/workflow/model/LeafTaskNode;->init(Lcom/vlingo/workflow/WorkflowTemplateManager;Lcom/vlingo/workflow/TaskDefinitionManager;)V

    goto :goto_0

    .line 70
    :cond_4
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LoopWorkflow;->getInputParameterMappings()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/workflow/model/ParameterMapping;

    .line 71
    .local v5, "parameterMapping":Lcom/vlingo/workflow/model/ParameterMapping;
    invoke-virtual {v5}, Lcom/vlingo/workflow/model/ParameterMapping;->getExpression()Ljava/lang/String;

    move-result-object v1

    .line 72
    .local v1, "expression":Ljava/lang/String;
    if-eqz v1, :cond_5

    .line 74
    const/4 v3, 0x0

    .line 75
    .local v3, "id":Ljava/lang/String;
    const-string/jumbo v10, "/"

    invoke-virtual {v1, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 76
    const/4 v10, 0x0

    const/16 v11, 0x2f

    invoke-virtual {v1, v11}, Ljava/lang/String;->indexOf(I)I

    move-result v11

    invoke-virtual {v1, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 80
    :goto_2
    iget-object v10, p0, Lcom/vlingo/workflow/model/LoopWorkflow;->referencedTaskIds:Ljava/util/HashSet;

    invoke-virtual {v10, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 78
    :cond_6
    move-object v3, v1

    goto :goto_2

    .line 83
    .end local v1    # "expression":Ljava/lang/String;
    .end local v3    # "id":Ljava/lang/String;
    .end local v5    # "parameterMapping":Lcom/vlingo/workflow/model/ParameterMapping;
    :cond_7
    return-void
.end method

.method public validate()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 116
    iget-object v3, p0, Lcom/vlingo/workflow/model/LoopWorkflow;->taskClass:Ljava/lang/Class;

    if-nez v3, :cond_0

    .line 117
    sget-object v3, Lcom/vlingo/workflow/model/LoopWorkflow;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "LoopWorkflow type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LoopWorkflow;->getType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "either cannot be found or it does not implement LoopWorkflowTaskInterface."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    .line 144
    :goto_0
    return v2

    .line 120
    :cond_0
    const-class v3, Lcom/vlingo/workflow/task/WorkflowTask;

    iget-object v4, p0, Lcom/vlingo/workflow/model/LoopWorkflow;->taskClass:Ljava/lang/Class;

    invoke-virtual {v3, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 121
    sget-object v3, Lcom/vlingo/workflow/model/LoopWorkflow;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "LoopWorkflow type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LoopWorkflow;->getType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "is not a subclass of WorkflowTask."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    goto :goto_0

    .line 125
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LoopWorkflow;->getType()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LoopWorkflow;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 126
    :cond_2
    sget-object v3, Lcom/vlingo/workflow/model/LoopWorkflow;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v4, "LoopWorkflow: Type is required."

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    goto :goto_0

    .line 130
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LoopWorkflow;->getExitCondition()Lcom/vlingo/workflow/model/ExitCondition;

    move-result-object v3

    if-nez v3, :cond_4

    .line 131
    sget-object v3, Lcom/vlingo/workflow/model/LoopWorkflow;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v4, "LoopWorkflow: ExitCondition is required."

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    goto :goto_0

    .line 135
    :cond_4
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LoopWorkflow;->getWaitToFinishBeforeNextLoopId()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_5

    .line 136
    sget-object v3, Lcom/vlingo/workflow/model/LoopWorkflow;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v4, "LoopWorkflow: WaitToFinishBeforeNextLoopId is required."

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    goto :goto_0

    .line 140
    :cond_5
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LoopWorkflow;->getTaskNodeList()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/workflow/model/TaskNode;

    .line 141
    .local v1, "node":Lcom/vlingo/workflow/model/TaskNode;
    check-cast v1, Lcom/vlingo/workflow/model/LeafTaskNode;

    .end local v1    # "node":Lcom/vlingo/workflow/model/TaskNode;
    invoke-virtual {v1}, Lcom/vlingo/workflow/model/LeafTaskNode;->validate()Z

    move-result v3

    if-nez v3, :cond_6

    goto/16 :goto_0

    .line 144
    :cond_7
    const/4 v2, 0x1

    goto/16 :goto_0
.end method
