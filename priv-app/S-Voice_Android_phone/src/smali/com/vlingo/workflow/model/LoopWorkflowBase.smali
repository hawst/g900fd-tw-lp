.class public Lcom/vlingo/workflow/model/LoopWorkflowBase;
.super Lcom/vlingo/workflow/model/TaskNode;
.source "LoopWorkflowBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# instance fields
.field private ExitCondition:Lcom/vlingo/workflow/model/ExitCondition;

.field private Id:Ljava/lang/String;

.field private InputParameterMappings:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/workflow/model/ParameterMapping;",
            ">;"
        }
    .end annotation
.end field

.field private IsEssential:Ljava/lang/Boolean;

.field private TaskNodeList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/workflow/model/TaskNode;",
            ">;"
        }
    .end annotation
.end field

.field private Type:Ljava/lang/String;

.field private WaitToFinishBeforeNextLoopId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/vlingo/workflow/model/TaskNode;-><init>()V

    .line 5
    const-string/jumbo v0, "com.vlingo.workflow.BasicLoopWorkflowTask"

    iput-object v0, p0, Lcom/vlingo/workflow/model/LoopWorkflowBase;->Type:Ljava/lang/String;

    .line 9
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/workflow/model/LoopWorkflowBase;->InputParameterMappings:Ljava/util/Set;

    .line 11
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/vlingo/workflow/model/LoopWorkflowBase;->IsEssential:Ljava/lang/Boolean;

    .line 13
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/workflow/model/LoopWorkflowBase;->TaskNodeList:Ljava/util/Set;

    .line 23
    return-void
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 76
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/workflow/model/LoopWorkflow;

    const-string/jumbo v2, "/WorkflowModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getExitCondition()Lcom/vlingo/workflow/model/ExitCondition;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vlingo/workflow/model/LoopWorkflowBase;->ExitCondition:Lcom/vlingo/workflow/model/ExitCondition;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vlingo/workflow/model/LoopWorkflowBase;->Id:Ljava/lang/String;

    return-object v0
.end method

.method public getInputParameterMappings()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/workflow/model/ParameterMapping;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vlingo/workflow/model/LoopWorkflowBase;->InputParameterMappings:Ljava/util/Set;

    return-object v0
.end method

.method public getTaskNodeList()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/workflow/model/TaskNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/workflow/model/LoopWorkflowBase;->TaskNodeList:Ljava/util/Set;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/workflow/model/LoopWorkflowBase;->Type:Ljava/lang/String;

    return-object v0
.end method

.method public getWaitToFinishBeforeNextLoopId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vlingo/workflow/model/LoopWorkflowBase;->WaitToFinishBeforeNextLoopId:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
