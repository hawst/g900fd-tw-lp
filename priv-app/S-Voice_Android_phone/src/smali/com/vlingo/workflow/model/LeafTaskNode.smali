.class public Lcom/vlingo/workflow/model/LeafTaskNode;
.super Lcom/vlingo/workflow/model/LeafTaskNodeBase;
.source "LeafTaskNode.java"


# static fields
.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private referencedTaskIds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private taskClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/workflow/task/WorkflowTask;",
            ">;"
        }
    .end annotation
.end field

.field private taskDefinition:Lcom/vlingo/workflow/task/model/TaskDefinition;

.field private taskDefinitionManager:Lcom/vlingo/workflow/TaskDefinitionManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/vlingo/workflow/model/TaskNode;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/workflow/model/LeafTaskNode;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/vlingo/workflow/model/LeafTaskNodeBase;-><init>()V

    .line 25
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/workflow/model/LeafTaskNode;->referencedTaskIds:Ljava/util/HashSet;

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/workflow/model/LeafTaskNode;->taskDefinitionManager:Lcom/vlingo/workflow/TaskDefinitionManager;

    return-void
.end method


# virtual methods
.method public init(Lcom/vlingo/workflow/WorkflowTemplateManager;Lcom/vlingo/workflow/TaskDefinitionManager;)V
    .locals 7
    .param p1, "templateManager"    # Lcom/vlingo/workflow/WorkflowTemplateManager;
    .param p2, "taskDefinitionManager"    # Lcom/vlingo/workflow/TaskDefinitionManager;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/InstantiationException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 32
    iput-object p2, p0, Lcom/vlingo/workflow/model/LeafTaskNode;->taskDefinitionManager:Lcom/vlingo/workflow/TaskDefinitionManager;

    .line 33
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LeafTaskNode;->getTaskDefinitionName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Lcom/vlingo/workflow/TaskDefinitionManager;->getDefinition(Ljava/lang/String;)Lcom/vlingo/workflow/task/model/TaskDefinition;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/workflow/model/LeafTaskNode;->taskDefinition:Lcom/vlingo/workflow/task/model/TaskDefinition;

    .line 34
    iget-object v4, p0, Lcom/vlingo/workflow/model/LeafTaskNode;->taskDefinition:Lcom/vlingo/workflow/task/model/TaskDefinition;

    invoke-virtual {v4}, Lcom/vlingo/workflow/task/model/TaskDefinition;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/workflow/model/LeafTaskNode;->taskClass:Ljava/lang/Class;

    .line 35
    const-class v4, Lcom/vlingo/workflow/task/WorkflowTask;

    iget-object v5, p0, Lcom/vlingo/workflow/model/LeafTaskNode;->taskClass:Ljava/lang/Class;

    invoke-virtual {v4, v5}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 36
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/vlingo/workflow/model/LeafTaskNode;->taskDefinition:Lcom/vlingo/workflow/task/model/TaskDefinition;

    invoke-virtual {v6}, Lcom/vlingo/workflow/task/model/TaskDefinition;->getType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " is not a subclass of the WorkflowTask"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 37
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LeafTaskNode;->getInputParameterMappings()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/workflow/model/ParameterMapping;

    .line 38
    .local v3, "parameterMapping":Lcom/vlingo/workflow/model/ParameterMapping;
    invoke-virtual {v3}, Lcom/vlingo/workflow/model/ParameterMapping;->getExpression()Ljava/lang/String;

    move-result-object v0

    .line 39
    .local v0, "expression":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 41
    const/4 v2, 0x0

    .line 42
    .local v2, "id":Ljava/lang/String;
    const-string/jumbo v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 43
    const/4 v4, 0x0

    const/16 v5, 0x2f

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 47
    :goto_1
    iget-object v4, p0, Lcom/vlingo/workflow/model/LeafTaskNode;->referencedTaskIds:Ljava/util/HashSet;

    invoke-virtual {v4, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 45
    :cond_2
    move-object v2, v0

    goto :goto_1

    .line 49
    .end local v0    # "expression":Ljava/lang/String;
    .end local v2    # "id":Ljava/lang/String;
    .end local v3    # "parameterMapping":Lcom/vlingo/workflow/model/ParameterMapping;
    :cond_3
    return-void
.end method

.method public validate()Z
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 67
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LeafTaskNode;->getId()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LeafTaskNode;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 68
    :cond_0
    sget-object v7, Lcom/vlingo/workflow/model/LeafTaskNode;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v8, "TaskNode: Id is required"

    invoke-virtual {v7, v8}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 100
    :goto_0
    return v6

    .line 72
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LeafTaskNode;->getTaskDefinitionName()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LeafTaskNode;->getTaskDefinitionName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 73
    :cond_2
    sget-object v7, Lcom/vlingo/workflow/model/LeafTaskNode;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "TaskNode "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LeafTaskNode;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ": TaskDefinitionName is required"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    goto :goto_0

    .line 77
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LeafTaskNode;->getIsEssential()Ljava/lang/Boolean;

    move-result-object v7

    if-nez v7, :cond_4

    .line 78
    sget-object v7, Lcom/vlingo/workflow/model/LeafTaskNode;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "TaskNode "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LeafTaskNode;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ": IsEssential is required."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    goto :goto_0

    .line 82
    :cond_4
    iget-object v7, p0, Lcom/vlingo/workflow/model/LeafTaskNode;->taskDefinitionManager:Lcom/vlingo/workflow/TaskDefinitionManager;

    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LeafTaskNode;->getTaskDefinitionName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vlingo/workflow/TaskDefinitionManager;->getDefinition(Ljava/lang/String;)Lcom/vlingo/workflow/task/model/TaskDefinition;

    move-result-object v0

    .line 83
    .local v0, "def":Lcom/vlingo/workflow/task/model/TaskDefinition;
    if-nez v0, :cond_5

    .line 84
    sget-object v7, Lcom/vlingo/workflow/model/LeafTaskNode;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "TaskNode "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LeafTaskNode;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ": TaskDefinition with the name: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LeafTaskNode;->getTaskDefinitionName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " does not exist."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 88
    :cond_5
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LeafTaskNode;->getInputParameterMappings()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/workflow/model/ParameterMapping;

    .line 89
    .local v4, "p":Lcom/vlingo/workflow/model/ParameterMapping;
    const/4 v1, 0x0

    .line 90
    .local v1, "foundParameter":Z
    invoke-virtual {v0}, Lcom/vlingo/workflow/task/model/TaskDefinition;->getInputs()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/workflow/task/model/Parameter;

    .line 91
    .local v5, "parameter":Lcom/vlingo/workflow/task/model/Parameter;
    invoke-virtual {v5}, Lcom/vlingo/workflow/task/model/Parameter;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Lcom/vlingo/workflow/model/ParameterMapping;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 93
    const/4 v1, 0x1

    .line 96
    .end local v5    # "parameter":Lcom/vlingo/workflow/task/model/Parameter;
    :cond_8
    if-nez v1, :cond_6

    .line 97
    sget-object v6, Lcom/vlingo/workflow/model/LeafTaskNode;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "TaskNode "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/vlingo/workflow/model/LeafTaskNode;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ": Unknown input parameter "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/vlingo/workflow/model/ParameterMapping;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    goto :goto_1

    .line 100
    .end local v1    # "foundParameter":Z
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "p":Lcom/vlingo/workflow/model/ParameterMapping;
    :cond_9
    const/4 v6, 0x1

    goto/16 :goto_0
.end method
