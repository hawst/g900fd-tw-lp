.class public Lcom/vlingo/workflow/model/WorkflowDefinition;
.super Lcom/vlingo/workflow/model/WorkflowDefinitionBase;
.source "WorkflowDefinition.java"


# static fields
.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/vlingo/workflow/model/WorkflowDefinition;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/workflow/model/WorkflowDefinition;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/workflow/model/WorkflowDefinitionBase;-><init>()V

    return-void
.end method


# virtual methods
.method public init(Lcom/vlingo/workflow/WorkflowTemplateManager;Lcom/vlingo/workflow/TaskDefinitionManager;)V
    .locals 10
    .param p1, "templateManager"    # Lcom/vlingo/workflow/WorkflowTemplateManager;
    .param p2, "taskDefinitionManager"    # Lcom/vlingo/workflow/TaskDefinitionManager;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InstantiationException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 24
    new-instance v6, Ljava/util/LinkedHashSet;

    invoke-direct {v6}, Ljava/util/LinkedHashSet;-><init>()V

    .line 25
    .local v6, "templateTaskList":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/workflow/model/TaskNode;>;"
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/WorkflowDefinition;->getTaskNodeList()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "taskNodeIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/workflow/model/TaskNode;>;"
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 26
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/workflow/model/TaskNode;

    .line 27
    .local v2, "taskNode":Lcom/vlingo/workflow/model/TaskNode;
    instance-of v7, v2, Lcom/vlingo/workflow/model/WorkflowTemplateRef;

    if-eqz v7, :cond_1

    move-object v7, v2

    .line 28
    check-cast v7, Lcom/vlingo/workflow/model/WorkflowTemplateRef;

    invoke-virtual {v7}, Lcom/vlingo/workflow/model/WorkflowTemplateRef;->getWorkflowTemplateName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Lcom/vlingo/workflow/WorkflowTemplateManager;->getTemplate(Ljava/lang/String;)Lcom/vlingo/workflow/model/WorkflowTemplate;

    move-result-object v5

    .line 29
    .local v5, "template":Lcom/vlingo/workflow/model/WorkflowTemplate;
    invoke-virtual {v5, p1}, Lcom/vlingo/workflow/model/WorkflowTemplate;->getTaskNodeList(Lcom/vlingo/workflow/WorkflowTemplateManager;)Ljava/util/Set;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 30
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 32
    .end local v5    # "template":Lcom/vlingo/workflow/model/WorkflowTemplate;
    :cond_1
    instance-of v7, v2, Lcom/vlingo/workflow/model/WorkflowTemplate;

    if-eqz v7, :cond_0

    .line 33
    new-instance v7, Ljava/lang/RuntimeException;

    const-string/jumbo v8, "Workflow templates definitions are not allowed inside the WorkflowDefinition. Workflow templates must be defined in the workflow templates file."

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 35
    .end local v2    # "taskNode":Lcom/vlingo/workflow/model/TaskNode;
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/WorkflowDefinition;->getTaskNodeList()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7, v6}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 37
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 38
    .local v4, "taskReferences":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/WorkflowDefinition;->getTaskNodeList()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/workflow/model/TaskNode;

    .line 39
    .restart local v2    # "taskNode":Lcom/vlingo/workflow/model/TaskNode;
    const/4 v1, 0x0

    .line 40
    .local v1, "id":Ljava/lang/String;
    instance-of v7, v2, Lcom/vlingo/workflow/model/LeafTaskNode;

    if-eqz v7, :cond_4

    move-object v7, v2

    .line 41
    check-cast v7, Lcom/vlingo/workflow/model/LeafTaskNode;

    invoke-virtual {v7, p1, p2}, Lcom/vlingo/workflow/model/LeafTaskNode;->init(Lcom/vlingo/workflow/WorkflowTemplateManager;Lcom/vlingo/workflow/TaskDefinitionManager;)V

    .line 42
    check-cast v2, Lcom/vlingo/workflow/model/LeafTaskNode;

    .end local v2    # "taskNode":Lcom/vlingo/workflow/model/TaskNode;
    invoke-virtual {v2}, Lcom/vlingo/workflow/model/LeafTaskNode;->getId()Ljava/lang/String;

    move-result-object v1

    .line 47
    :cond_3
    :goto_1
    invoke-virtual {v4, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 48
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Duplicate definition of a leaf task node with the Id "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 43
    .restart local v2    # "taskNode":Lcom/vlingo/workflow/model/TaskNode;
    :cond_4
    instance-of v7, v2, Lcom/vlingo/workflow/model/LoopWorkflow;

    if-eqz v7, :cond_3

    move-object v7, v2

    .line 44
    check-cast v7, Lcom/vlingo/workflow/model/LoopWorkflow;

    invoke-virtual {v7, p1, p2}, Lcom/vlingo/workflow/model/LoopWorkflow;->init(Lcom/vlingo/workflow/WorkflowTemplateManager;Lcom/vlingo/workflow/TaskDefinitionManager;)V

    .line 45
    check-cast v2, Lcom/vlingo/workflow/model/LoopWorkflow;

    .end local v2    # "taskNode":Lcom/vlingo/workflow/model/TaskNode;
    invoke-virtual {v2}, Lcom/vlingo/workflow/model/LoopWorkflow;->getId()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 49
    :cond_5
    invoke-virtual {v4, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 53
    .end local v1    # "id":Ljava/lang/String;
    :cond_6
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/WorkflowDefinition;->validate()Z

    move-result v7

    if-nez v7, :cond_7

    .line 54
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Invalid Workflow: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/vlingo/workflow/model/WorkflowDefinition;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 55
    :cond_7
    return-void
.end method

.method public validate()Z
    .locals 6

    .prologue
    .line 58
    const/4 v1, 0x1

    .line 59
    .local v1, "ret":Z
    invoke-virtual {p0}, Lcom/vlingo/workflow/model/WorkflowDefinition;->getTaskNodeList()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/workflow/model/TaskNode;

    .line 60
    .local v2, "taskNode":Lcom/vlingo/workflow/model/TaskNode;
    instance-of v3, v2, Lcom/vlingo/workflow/model/LeafTaskNode;

    if-eqz v3, :cond_0

    .line 61
    check-cast v2, Lcom/vlingo/workflow/model/LeafTaskNode;

    .end local v2    # "taskNode":Lcom/vlingo/workflow/model/TaskNode;
    invoke-virtual {v2}, Lcom/vlingo/workflow/model/LeafTaskNode;->validate()Z

    move-result v1

    goto :goto_0

    .line 62
    .restart local v2    # "taskNode":Lcom/vlingo/workflow/model/TaskNode;
    :cond_0
    instance-of v3, v2, Lcom/vlingo/workflow/model/LoopWorkflow;

    if-eqz v3, :cond_1

    .line 63
    check-cast v2, Lcom/vlingo/workflow/model/LoopWorkflow;

    .end local v2    # "taskNode":Lcom/vlingo/workflow/model/TaskNode;
    invoke-virtual {v2}, Lcom/vlingo/workflow/model/LoopWorkflow;->validate()Z

    move-result v1

    goto :goto_0

    .line 65
    .restart local v2    # "taskNode":Lcom/vlingo/workflow/model/TaskNode;
    :cond_1
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Unknown task node class: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 68
    .end local v2    # "taskNode":Lcom/vlingo/workflow/model/TaskNode;
    :cond_2
    const/4 v3, 0x1

    return v3
.end method
