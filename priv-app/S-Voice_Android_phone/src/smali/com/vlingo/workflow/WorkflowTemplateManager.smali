.class public Lcom/vlingo/workflow/WorkflowTemplateManager;
.super Ljava/lang/Object;
.source "WorkflowTemplateManager.java"


# static fields
.field private static final ivLogger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private additionalPackages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/mda/model/PackageMeta;",
            ">;"
        }
    .end annotation
.end field

.field private ivTemplatesFile:Ljava/lang/String;

.field private ivWorkflowTemplateMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/workflow/model/WorkflowTemplate;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/vlingo/workflow/WorkflowTemplateManager;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/workflow/WorkflowTemplateManager;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/util/Set;)V
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/mda/model/PackageMeta;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "additionalPackages":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/mda/model/PackageMeta;>;"
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object v0, p0, Lcom/vlingo/workflow/WorkflowTemplateManager;->ivTemplatesFile:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lcom/vlingo/workflow/WorkflowTemplateManager;->additionalPackages:Ljava/util/Set;

    .line 25
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/workflow/WorkflowTemplateManager;->ivWorkflowTemplateMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 32
    iput-object p2, p0, Lcom/vlingo/workflow/WorkflowTemplateManager;->additionalPackages:Ljava/util/Set;

    .line 33
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 37
    :cond_0
    :goto_0
    return-void

    .line 35
    :cond_1
    iput-object p1, p0, Lcom/vlingo/workflow/WorkflowTemplateManager;->ivTemplatesFile:Ljava/lang/String;

    .line 36
    invoke-direct {p0}, Lcom/vlingo/workflow/WorkflowTemplateManager;->init()V

    goto :goto_0
.end method

.method private init()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/vlingo/workflow/WorkflowTemplateManager;->loadTemplates()V

    .line 40
    return-void
.end method

.method private loadTemplates()V
    .locals 6

    .prologue
    .line 44
    const/4 v2, 0x0

    .line 45
    .local v2, "templateCollecton":Lcom/vlingo/workflow/model/WorkflowTemplateCollecton;
    iget-object v3, p0, Lcom/vlingo/workflow/WorkflowTemplateManager;->additionalPackages:Ljava/util/Set;

    if-nez v3, :cond_0

    .line 46
    const-class v3, Lcom/vlingo/workflow/model/WorkflowTemplateCollecton;

    iget-object v4, p0, Lcom/vlingo/workflow/WorkflowTemplateManager;->ivTemplatesFile:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/vlingo/workflow/util/Util;->loadMdaObject(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/util/MDAObject;

    move-result-object v2

    .end local v2    # "templateCollecton":Lcom/vlingo/workflow/model/WorkflowTemplateCollecton;
    check-cast v2, Lcom/vlingo/workflow/model/WorkflowTemplateCollecton;

    .line 49
    .restart local v2    # "templateCollecton":Lcom/vlingo/workflow/model/WorkflowTemplateCollecton;
    :goto_0
    invoke-virtual {v2}, Lcom/vlingo/workflow/model/WorkflowTemplateCollecton;->getWorkflowTemplates()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/workflow/model/WorkflowTemplate;

    .line 50
    .local v1, "template":Lcom/vlingo/workflow/model/WorkflowTemplate;
    iget-object v3, p0, Lcom/vlingo/workflow/WorkflowTemplateManager;->ivWorkflowTemplateMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Lcom/vlingo/workflow/model/WorkflowTemplate;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 51
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Duplicate definition of the workflow template with the name "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/vlingo/workflow/model/WorkflowTemplate;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 48
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "template":Lcom/vlingo/workflow/model/WorkflowTemplate;
    :cond_0
    const-class v3, Lcom/vlingo/workflow/model/WorkflowTemplateCollecton;

    iget-object v4, p0, Lcom/vlingo/workflow/WorkflowTemplateManager;->ivTemplatesFile:Ljava/lang/String;

    iget-object v5, p0, Lcom/vlingo/workflow/WorkflowTemplateManager;->additionalPackages:Ljava/util/Set;

    invoke-static {v3, v4, v5}, Lcom/vlingo/workflow/util/Util;->loadMdaObject(Ljava/lang/Class;Ljava/lang/String;Ljava/util/Set;)Lcom/vlingo/mda/util/MDAObject;

    move-result-object v2

    .end local v2    # "templateCollecton":Lcom/vlingo/workflow/model/WorkflowTemplateCollecton;
    check-cast v2, Lcom/vlingo/workflow/model/WorkflowTemplateCollecton;

    .restart local v2    # "templateCollecton":Lcom/vlingo/workflow/model/WorkflowTemplateCollecton;
    goto :goto_0

    .line 52
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v1    # "template":Lcom/vlingo/workflow/model/WorkflowTemplate;
    :cond_1
    iget-object v3, p0, Lcom/vlingo/workflow/WorkflowTemplateManager;->ivWorkflowTemplateMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Lcom/vlingo/workflow/model/WorkflowTemplate;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 54
    .end local v1    # "template":Lcom/vlingo/workflow/model/WorkflowTemplate;
    :cond_2
    sget-object v3, Lcom/vlingo/workflow/WorkflowTemplateManager;->ivLogger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "loaded "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/workflow/WorkflowTemplateManager;->ivWorkflowTemplateMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " workflow templates from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/workflow/WorkflowTemplateManager;->ivTemplatesFile:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 55
    return-void
.end method


# virtual methods
.method public getTemplate(Ljava/lang/String;)Lcom/vlingo/workflow/model/WorkflowTemplate;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vlingo/workflow/WorkflowTemplateManager;->ivWorkflowTemplateMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Workflow template with the name "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " does not exist."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/vlingo/workflow/WorkflowTemplateManager;->ivWorkflowTemplateMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/workflow/model/WorkflowTemplate;

    return-object v0
.end method
