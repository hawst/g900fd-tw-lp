.class public Lcom/vlingo/workflow/WorkflowManager;
.super Ljava/lang/Object;
.source "WorkflowManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/workflow/WorkflowManager$PropertyChangeAdapter;
    }
.end annotation


# static fields
.field private static final ivLogger:Lorg/apache/log4j/Logger;


# instance fields
.field private additionalPackages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/mda/model/PackageMeta;",
            ">;"
        }
    .end annotation
.end field

.field private ivChangeListener:Lcom/vlingo/workflow/WorkflowManager$PropertyChangeAdapter;

.field private ivTaskDefinitionManager:Lcom/vlingo/workflow/TaskDefinitionManager;

.field private ivWorkflowCollectionFileName:Ljava/lang/String;

.field private ivWorkflowMeta:Lcom/vlingo/workflow/model/WorkflowCollection;

.field private ivWorkflowTemplateManager:Lcom/vlingo/workflow/WorkflowTemplateManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/vlingo/workflow/WorkflowManager;

    invoke-static {v0}, Lorg/apache/log4j/Logger;->getLogger(Ljava/lang/Class;)Lorg/apache/log4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/workflow/WorkflowManager;->ivLogger:Lorg/apache/log4j/Logger;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "workflowCollectionFileName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object v0, p0, Lcom/vlingo/workflow/WorkflowManager;->ivWorkflowCollectionFileName:Ljava/lang/String;

    .line 29
    iput-object v0, p0, Lcom/vlingo/workflow/WorkflowManager;->ivWorkflowTemplateManager:Lcom/vlingo/workflow/WorkflowTemplateManager;

    .line 30
    iput-object v0, p0, Lcom/vlingo/workflow/WorkflowManager;->ivTaskDefinitionManager:Lcom/vlingo/workflow/TaskDefinitionManager;

    .line 31
    iput-object v0, p0, Lcom/vlingo/workflow/WorkflowManager;->additionalPackages:Ljava/util/Set;

    .line 35
    new-instance v0, Lcom/vlingo/workflow/WorkflowManager$PropertyChangeAdapter;

    invoke-direct {v0, p0}, Lcom/vlingo/workflow/WorkflowManager$PropertyChangeAdapter;-><init>(Lcom/vlingo/workflow/WorkflowManager;)V

    iput-object v0, p0, Lcom/vlingo/workflow/WorkflowManager;->ivChangeListener:Lcom/vlingo/workflow/WorkflowManager$PropertyChangeAdapter;

    .line 43
    iput-object p1, p0, Lcom/vlingo/workflow/WorkflowManager;->ivWorkflowCollectionFileName:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Set;)V
    .locals 0
    .param p1, "workflowCollectionFileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/mda/model/PackageMeta;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    .local p2, "additionalPackages":Ljava/util/Set;, "Ljava/util/Set<Lcom/vlingo/mda/model/PackageMeta;>;"
    invoke-direct {p0, p1}, Lcom/vlingo/workflow/WorkflowManager;-><init>(Ljava/lang/String;)V

    .line 55
    iput-object p2, p0, Lcom/vlingo/workflow/WorkflowManager;->additionalPackages:Ljava/util/Set;

    .line 56
    return-void
.end method

.method public static compareVersions(Ljava/lang/String;Ljava/lang/String;)I
    .locals 9
    .param p0, "version1"    # Ljava/lang/String;
    .param p1, "version2"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 133
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 147
    :cond_0
    :goto_0
    return v6

    .line 136
    :cond_1
    const-string/jumbo v7, "\\."

    invoke-virtual {p0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 137
    .local v2, "v1":[Ljava/lang/String;
    const-string/jumbo v7, "\\."

    invoke-virtual {p1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 139
    .local v3, "v2":[Ljava/lang/String;
    array-length v7, v2

    array-length v8, v3

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 140
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_0

    .line 141
    const/4 v4, 0x0

    .local v4, "vc1":I
    const/4 v5, 0x0

    .line 142
    .local v5, "vc2":I
    array-length v7, v2

    if-ge v0, v7, :cond_2

    aget-object v7, v2, v0

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 143
    :cond_2
    array-length v7, v3

    if-ge v0, v7, :cond_3

    aget-object v7, v3, v0

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 144
    :cond_3
    if-le v4, v5, :cond_4

    const/4 v6, 0x1

    goto :goto_0

    .line 145
    :cond_4
    if-ge v4, v5, :cond_5

    const/4 v6, -0x1

    goto :goto_0

    .line 140
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private loadWorkflows()V
    .locals 9

    .prologue
    .line 92
    iget-object v6, p0, Lcom/vlingo/workflow/WorkflowManager;->additionalPackages:Ljava/util/Set;

    if-nez v6, :cond_0

    .line 93
    const-class v6, Lcom/vlingo/workflow/model/WorkflowCollection;

    iget-object v7, p0, Lcom/vlingo/workflow/WorkflowManager;->ivWorkflowCollectionFileName:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/vlingo/workflow/util/Util;->loadMdaObject(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/util/MDAObject;

    move-result-object v6

    check-cast v6, Lcom/vlingo/workflow/model/WorkflowCollection;

    iput-object v6, p0, Lcom/vlingo/workflow/WorkflowManager;->ivWorkflowMeta:Lcom/vlingo/workflow/model/WorkflowCollection;

    .line 98
    :goto_0
    new-instance v4, Ljava/util/HashSet;

    iget-object v6, p0, Lcom/vlingo/workflow/WorkflowManager;->ivWorkflowMeta:Lcom/vlingo/workflow/model/WorkflowCollection;

    invoke-virtual {v6}, Lcom/vlingo/workflow/model/WorkflowCollection;->getWorkflows()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->size()I

    move-result v6

    invoke-direct {v4, v6}, Ljava/util/HashSet;-><init>(I)V

    .line 99
    .local v4, "workflowNames":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/vlingo/workflow/WorkflowManager;->ivWorkflowMeta:Lcom/vlingo/workflow/model/WorkflowCollection;

    invoke-virtual {v6}, Lcom/vlingo/workflow/model/WorkflowCollection;->getWorkflows()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/workflow/model/WorkflowDefinition;

    .line 100
    .local v1, "flow":Lcom/vlingo/workflow/model/WorkflowDefinition;
    invoke-virtual {v1}, Lcom/vlingo/workflow/model/WorkflowDefinition;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 101
    sget-object v6, Lcom/vlingo/workflow/WorkflowManager;->ivLogger:Lorg/apache/log4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Duplicate workflow name "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Lcom/vlingo/workflow/model/WorkflowDefinition;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "in file "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/vlingo/workflow/WorkflowManager;->ivWorkflowCollectionFileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/log4j/Logger;->error(Ljava/lang/Object;)V

    .line 102
    new-instance v6, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Duplicate workflow name: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Lcom/vlingo/workflow/model/WorkflowDefinition;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 95
    .end local v1    # "flow":Lcom/vlingo/workflow/model/WorkflowDefinition;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "workflowNames":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_0
    const-class v6, Lcom/vlingo/workflow/model/WorkflowCollection;

    iget-object v7, p0, Lcom/vlingo/workflow/WorkflowManager;->ivWorkflowCollectionFileName:Ljava/lang/String;

    iget-object v8, p0, Lcom/vlingo/workflow/WorkflowManager;->additionalPackages:Ljava/util/Set;

    invoke-static {v6, v7, v8}, Lcom/vlingo/workflow/util/Util;->loadMdaObject(Ljava/lang/Class;Ljava/lang/String;Ljava/util/Set;)Lcom/vlingo/mda/util/MDAObject;

    move-result-object v6

    check-cast v6, Lcom/vlingo/workflow/model/WorkflowCollection;

    iput-object v6, p0, Lcom/vlingo/workflow/WorkflowManager;->ivWorkflowMeta:Lcom/vlingo/workflow/model/WorkflowCollection;

    goto/16 :goto_0

    .line 104
    .restart local v1    # "flow":Lcom/vlingo/workflow/model/WorkflowDefinition;
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v4    # "workflowNames":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_1
    invoke-virtual {v1}, Lcom/vlingo/workflow/model/WorkflowDefinition;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 107
    .end local v1    # "flow":Lcom/vlingo/workflow/model/WorkflowDefinition;
    :cond_2
    sget-object v6, Lcom/vlingo/workflow/WorkflowManager;->ivLogger:Lorg/apache/log4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "loaded "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/vlingo/workflow/WorkflowManager;->ivWorkflowMeta:Lcom/vlingo/workflow/model/WorkflowCollection;

    invoke-virtual {v8}, Lcom/vlingo/workflow/model/WorkflowCollection;->getWorkflows()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " workflow rules from "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/vlingo/workflow/WorkflowManager;->ivWorkflowCollectionFileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/log4j/Logger;->info(Ljava/lang/Object;)V

    .line 109
    iget-object v6, p0, Lcom/vlingo/workflow/WorkflowManager;->ivWorkflowMeta:Lcom/vlingo/workflow/model/WorkflowCollection;

    invoke-virtual {v6}, Lcom/vlingo/workflow/model/WorkflowCollection;->getWorkflowTemplatesFile()Ljava/lang/String;

    move-result-object v5

    .line 110
    .local v5, "workflowTemplatesFileName":Ljava/lang/String;
    if-eqz v5, :cond_3

    .line 111
    new-instance v6, Lcom/vlingo/workflow/WorkflowTemplateManager;

    iget-object v7, p0, Lcom/vlingo/workflow/WorkflowManager;->additionalPackages:Ljava/util/Set;

    invoke-direct {v6, v5, v7}, Lcom/vlingo/workflow/WorkflowTemplateManager;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    iput-object v6, p0, Lcom/vlingo/workflow/WorkflowManager;->ivWorkflowTemplateManager:Lcom/vlingo/workflow/WorkflowTemplateManager;

    .line 113
    :cond_3
    iget-object v6, p0, Lcom/vlingo/workflow/WorkflowManager;->ivWorkflowMeta:Lcom/vlingo/workflow/model/WorkflowCollection;

    invoke-virtual {v6}, Lcom/vlingo/workflow/model/WorkflowCollection;->getTaskDefinitionsFile()Ljava/lang/String;

    move-result-object v3

    .line 114
    .local v3, "taskDefinitionsFileName":Ljava/lang/String;
    new-instance v6, Lcom/vlingo/workflow/TaskDefinitionManager;

    invoke-direct {v6, v3}, Lcom/vlingo/workflow/TaskDefinitionManager;-><init>(Ljava/lang/String;)V

    iput-object v6, p0, Lcom/vlingo/workflow/WorkflowManager;->ivTaskDefinitionManager:Lcom/vlingo/workflow/TaskDefinitionManager;

    .line 116
    iget-object v6, p0, Lcom/vlingo/workflow/WorkflowManager;->ivWorkflowMeta:Lcom/vlingo/workflow/model/WorkflowCollection;

    invoke-virtual {v6}, Lcom/vlingo/workflow/model/WorkflowCollection;->getWorkflows()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/workflow/model/WorkflowDefinition;

    .line 118
    .restart local v1    # "flow":Lcom/vlingo/workflow/model/WorkflowDefinition;
    :try_start_0
    iget-object v6, p0, Lcom/vlingo/workflow/WorkflowManager;->ivWorkflowTemplateManager:Lcom/vlingo/workflow/WorkflowTemplateManager;

    iget-object v7, p0, Lcom/vlingo/workflow/WorkflowManager;->ivTaskDefinitionManager:Lcom/vlingo/workflow/TaskDefinitionManager;

    invoke-virtual {v1, v6, v7}, Lcom/vlingo/workflow/model/WorkflowDefinition;->init(Lcom/vlingo/workflow/WorkflowTemplateManager;Lcom/vlingo/workflow/TaskDefinitionManager;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 119
    :catch_0
    move-exception v0

    .line 120
    .local v0, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/vlingo/workflow/WorkflowManager;->ivLogger:Lorg/apache/log4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Failed to initialize workflow "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Lcom/vlingo/workflow/model/WorkflowDefinition;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/log4j/Logger;->error(Ljava/lang/Object;)V

    goto :goto_2

    .line 123
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "flow":Lcom/vlingo/workflow/model/WorkflowDefinition;
    :cond_4
    return-void
.end method


# virtual methods
.method public init()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/vlingo/workflow/WorkflowManager;->loadWorkflows()V

    .line 60
    return-void
.end method
