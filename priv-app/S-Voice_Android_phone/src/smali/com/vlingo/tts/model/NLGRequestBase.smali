.class public Lcom/vlingo/tts/model/NLGRequestBase;
.super Ljava/lang/Object;
.source "NLGRequestBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_Context:Ljava/lang/String; = "Context"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Index:Ljava/lang/String; = "Index"

.field public static final PROP_Language:Ljava/lang/String; = "Language"

.field public static final PROP_Type:Ljava/lang/String; = "Type"


# instance fields
.field private Context:Lcom/vlingo/mda/util/MDAObject;

.field private ID:J

.field private Index:Ljava/lang/Integer;

.field private Language:Ljava/lang/String;

.field private Type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 55
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/tts/model/NLGRequest;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 59
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/tts/model/NLGRequest;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getContext()Lcom/vlingo/mda/util/MDAObject;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/tts/model/NLGRequestBase;->Context:Lcom/vlingo/mda/util/MDAObject;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/vlingo/tts/model/NLGRequestBase;->ID:J

    return-wide v0
.end method

.method public getIndex()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/tts/model/NLGRequestBase;->Index:Ljava/lang/Integer;

    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/tts/model/NLGRequestBase;->Language:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vlingo/tts/model/NLGRequestBase;->Type:Ljava/lang/String;

    return-object v0
.end method

.method public setContext(Lcom/vlingo/mda/util/MDAObject;)V
    .locals 0
    .param p1, "val"    # Lcom/vlingo/mda/util/MDAObject;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/vlingo/tts/model/NLGRequestBase;->Context:Lcom/vlingo/mda/util/MDAObject;

    .line 44
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 50
    iput-wide p1, p0, Lcom/vlingo/tts/model/NLGRequestBase;->ID:J

    .line 51
    return-void
.end method

.method public setIndex(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Integer;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/vlingo/tts/model/NLGRequestBase;->Index:Ljava/lang/Integer;

    .line 37
    return-void
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/vlingo/tts/model/NLGRequestBase;->Language:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/vlingo/tts/model/NLGRequestBase;->Type:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
