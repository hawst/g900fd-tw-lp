.class public final Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;
.super Ljava/lang/Object;
.source "VLHelpPageRecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$1;,
        Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;
    }
.end annotation


# static fields
.field public static final MAX_LENGTH:I = 0x80


# instance fields
.field private mPageId:Ljava/lang/String;

.field private mViewCount:I


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;->pageId:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;->access$100(Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;->mPageId:Ljava/lang/String;

    .line 33
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;->viewCount:I
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;->access$200(Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;->mViewCount:I

    .line 34
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;
    .param p2, "x1"    # Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$1;

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;-><init>(Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;)V

    return-void
.end method


# virtual methods
.method generateXml()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "<help-page page-id=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;->mPageId:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\" count=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;->mViewCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\"/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPageId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;->mPageId:Ljava/lang/String;

    return-object v0
.end method

.method public getViewCount()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;->mViewCount:I

    return v0
.end method
