.class public Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;
.super Ljava/lang/Object;
.source "VLHelpPageRecord.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private pageId:Ljava/lang/String;

.field private viewCount:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "pageId"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;->pageId:Ljava/lang/String;

    .line 18
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;->pageId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;

    .prologue
    .line 13
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;->viewCount:I

    return v0
.end method


# virtual methods
.method public build()Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;
    .locals 2

    .prologue
    .line 24
    new-instance v0, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;-><init>(Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$1;)V

    return-object v0
.end method

.method public pageViewed()Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;->viewCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord$Builder;->viewCount:I

    .line 21
    return-object p0
.end method
