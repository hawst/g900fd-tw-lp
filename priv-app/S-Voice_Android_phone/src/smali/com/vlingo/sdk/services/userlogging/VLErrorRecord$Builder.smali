.class public Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;
.super Ljava/lang/Object;
.source "VLErrorRecord.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/services/userlogging/VLErrorRecord;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private errorId:Ljava/lang/String;

.field private viewCount:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "errorId"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;->errorId:Ljava/lang/String;

    .line 18
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;->errorId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;

    .prologue
    .line 12
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;->viewCount:I

    return v0
.end method


# virtual methods
.method public build()Lcom/vlingo/sdk/services/userlogging/VLErrorRecord;
    .locals 2

    .prologue
    .line 24
    new-instance v0, Lcom/vlingo/sdk/services/userlogging/VLErrorRecord;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/sdk/services/userlogging/VLErrorRecord;-><init>(Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$1;)V

    return-object v0
.end method

.method public errorDisplayed()Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;->viewCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLErrorRecord$Builder;->viewCount:I

    .line 21
    return-object p0
.end method
