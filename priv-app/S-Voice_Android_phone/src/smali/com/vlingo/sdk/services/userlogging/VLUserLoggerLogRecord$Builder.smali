.class public Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;
.super Ljava/lang/Object;
.source "VLUserLoggerLogRecord.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/services/userlogging/VLErrorRecord;",
            ">;"
        }
    .end annotation
.end field

.field private helpPages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;",
            ">;"
        }
    .end annotation
.end field

.field private landingPages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;",
            ">;"
        }
    .end annotation
.end field

.field private settings:Ljava/lang/String;

.field private setupFinished:Z

.field private setupStarted:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-boolean v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->setupStarted:Z

    .line 24
    iput-boolean v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->setupFinished:Z

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->settings:Ljava/lang/String;

    .line 31
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->helpPages:Ljava/util/List;

    .line 32
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->errors:Ljava/util/List;

    .line 33
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->landingPages:Ljava/util/List;

    .line 34
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->setupStarted:Z

    return v0
.end method

.method static synthetic access$200(Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->setupFinished:Z

    return v0
.end method

.method static synthetic access$300(Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->settings:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->helpPages:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->errors:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->landingPages:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public addErrorRecord(Lcom/vlingo/sdk/services/userlogging/VLErrorRecord;)Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;
    .locals 1
    .param p1, "r"    # Lcom/vlingo/sdk/services/userlogging/VLErrorRecord;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->errors:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    return-object p0
.end method

.method public addHelpPageRecord(Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;)Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;
    .locals 1
    .param p1, "r"    # Lcom/vlingo/sdk/services/userlogging/VLHelpPageRecord;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->helpPages:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    return-object p0
.end method

.method public addLandingPageRecord(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;)Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;
    .locals 1
    .param p1, "r"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->landingPages:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    return-object p0
.end method

.method public build()Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;
    .locals 2

    .prologue
    .line 67
    new-instance v0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord;-><init>(Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$1;)V

    return-object v0
.end method

.method public settings(Ljava/lang/String;)Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;
    .locals 0
    .param p1, "settings"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->settings:Ljava/lang/String;

    .line 48
    return-object p0
.end method

.method public setupFinished(Z)Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;
    .locals 0
    .param p1, "setupFinished"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->setupFinished:Z

    .line 43
    return-object p0
.end method

.method public setupStarted(Z)Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;
    .locals 0
    .param p1, "setupStarted"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/vlingo/sdk/services/userlogging/VLUserLoggerLogRecord$Builder;->setupStarted:Z

    .line 38
    return-object p0
.end method
