.class public Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;
.super Ljava/lang/Object;
.source "VLLandingPageRecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;,
        Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;,
        Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    }
.end annotation


# instance fields
.field private actionCount:I

.field private actionNoEditCount:I

.field private actionUtteranceCount:I

.field private alternativePhrasePickedCount:I

.field private backCount:I

.field private backNoEditCount:I

.field private backUtteranceCount:I

.field private contactChangeCount:I

.field private fields:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;",
            ">;"
        }
    .end annotation
.end field

.field private fromEmailChangedCount:I

.field private launchCount:I

.field private launchTimeTotal:J

.field private noteTypeChangeCount:I

.field private pageId:Ljava/lang/String;

.field private phoneChangeCount:I

.field private reserved1:Ljava/lang/String;

.field private reserved2:Ljava/lang/String;

.field private undoCount:I

.field private viewCount:I


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .prologue
    .line 201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 203
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->pageId:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->access$000(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->pageId:Ljava/lang/String;

    .line 204
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->viewCount:I
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->access$100(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->viewCount:I

    .line 205
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->launchTimeTotal:J
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->access$200(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->launchTimeTotal:J

    .line 206
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->launchCount:I
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->access$300(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->launchCount:I

    .line 207
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->backCount:I
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->access$400(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->backCount:I

    .line 208
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->backNoEditCount:I
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->access$500(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->backNoEditCount:I

    .line 209
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->backUtteranceCount:I
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->access$600(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->backUtteranceCount:I

    .line 210
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->actionCount:I
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->access$700(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->actionCount:I

    .line 211
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->actionNoEditCount:I
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->access$800(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->actionNoEditCount:I

    .line 212
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->actionUtteranceCount:I
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->access$900(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->actionUtteranceCount:I

    .line 213
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->fromEmailChangedCount:I
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->access$1000(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->fromEmailChangedCount:I

    .line 214
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->alternativePhrasePickedCount:I
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->access$1100(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->alternativePhrasePickedCount:I

    .line 215
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->contactChangeCount:I
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->access$1200(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->contactChangeCount:I

    .line 216
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->phoneChangeCount:I
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->access$1300(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->phoneChangeCount:I

    .line 217
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->noteTypeChangeCount:I
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->access$1400(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->noteTypeChangeCount:I

    .line 218
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->undoCount:I
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->access$1500(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->undoCount:I

    .line 219
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->reserved1:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->access$1600(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->reserved1:Ljava/lang/String;

    .line 220
    # getter for: Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->reserved2:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->access$1700(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->reserved2:Ljava/lang/String;

    .line 222
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->fields:Ljava/util/List;

    .line 223
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->fields:Ljava/util/List;

    # getter for: Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->fields:Ljava/util/List;
    invoke-static {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->access$1800(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 224
    return-void
.end method


# virtual methods
.method generateXml()Ljava/lang/String;
    .locals 6

    .prologue
    .line 231
    iget v4, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->launchCount:I

    if-nez v4, :cond_0

    const/4 v2, 0x0

    .line 233
    .local v2, "launchTimeAvg":F
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 234
    .local v3, "sb":Ljava/lang/StringBuilder;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "<landing-page page-id=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->pageId:Ljava/lang/String;

    invoke-static {v5}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\" count=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->viewCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\" launch-time-avg=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\" back-count=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->backCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\" back-noedit-count=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->backNoEditCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\" back-utt-avg=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->backUtteranceCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\" action-count=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->actionCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\" action-noedit-count=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->actionNoEditCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\" action-utt-avg=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->actionUtteranceCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\" from-email-count=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->fromEmailChangedCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\" undo-count=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->undoCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\" alternative-count=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->alternativePhrasePickedCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\" contact-change-count=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->contactChangeCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\" phone-change-count=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->phoneChangeCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\" note-change-count=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->noteTypeChangeCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\" reserved1=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->reserved1:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\" reserved2=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->reserved2:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\">"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    iget-object v4, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->fields:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;

    .line 257
    .local v0, "field":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;
    invoke-virtual {v0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->generateXml()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 231
    .end local v0    # "field":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "launchTimeAvg":F
    .end local v3    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    iget-wide v4, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->launchTimeTotal:J

    long-to-float v4, v4

    iget v5, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->launchCount:I

    int-to-float v5, v5

    div-float v2, v4, v5

    goto/16 :goto_0

    .line 260
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v2    # "launchTimeAvg":F
    .restart local v3    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    const-string/jumbo v4, "</landing-page>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public getPageId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;->pageId:Ljava/lang/String;

    return-object v0
.end method
