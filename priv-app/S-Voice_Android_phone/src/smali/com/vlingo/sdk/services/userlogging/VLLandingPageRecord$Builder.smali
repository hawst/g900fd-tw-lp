.class public Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
.super Ljava/lang/Object;
.source "VLLandingPageRecord.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private actionCount:I

.field private actionNoEditCount:I

.field private actionUtteranceCount:I

.field private alternativePhrasePickedCount:I

.field private backCount:I

.field private backNoEditCount:I

.field private backUtteranceCount:I

.field private contactChangeCount:I

.field private fieldLoggingEnabled:Z

.field private fields:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;",
            ">;"
        }
    .end annotation
.end field

.field private fromEmailChangedCount:I

.field private launchCount:I

.field private launchTimeTotal:J

.field private noteTypeChangeCount:I

.field private pageId:Ljava/lang/String;

.field private phoneChangeCount:I

.field private reserved1:Ljava/lang/String;

.field private reserved2:Ljava/lang/String;

.field private undoCount:I

.field private viewCount:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "pageId"    # Ljava/lang/String;
    .param p2, "doFieldLogging"    # Z

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->pageId:Ljava/lang/String;

    .line 50
    iput-boolean p2, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->fieldLoggingEnabled:Z

    .line 51
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->fields:Ljava/util/List;

    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->pageId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->viewCount:I

    return v0
.end method

.method static synthetic access$1000(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->fromEmailChangedCount:I

    return v0
.end method

.method static synthetic access$1100(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->alternativePhrasePickedCount:I

    return v0
.end method

.method static synthetic access$1200(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->contactChangeCount:I

    return v0
.end method

.method static synthetic access$1300(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->phoneChangeCount:I

    return v0
.end method

.method static synthetic access$1400(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->noteTypeChangeCount:I

    return v0
.end method

.method static synthetic access$1500(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->undoCount:I

    return v0
.end method

.method static synthetic access$1600(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->reserved1:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->reserved2:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->fields:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)J
    .locals 2
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .prologue
    .line 15
    iget-wide v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->launchTimeTotal:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->launchCount:I

    return v0
.end method

.method static synthetic access$400(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->backCount:I

    return v0
.end method

.method static synthetic access$500(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->backNoEditCount:I

    return v0
.end method

.method static synthetic access$600(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->backUtteranceCount:I

    return v0
.end method

.method static synthetic access$700(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->actionCount:I

    return v0
.end method

.method static synthetic access$800(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->actionNoEditCount:I

    return v0
.end method

.method static synthetic access$900(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->actionUtteranceCount:I

    return v0
.end method

.method private getTextFieldRecord(Ljava/lang/String;)Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;
    .locals 3
    .param p1, "fieldId"    # Ljava/lang/String;

    .prologue
    .line 158
    iget-object v2, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->fields:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;

    .line 159
    .local v1, "rec":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;
    invoke-virtual {v1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->getFieldId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 165
    :goto_0
    return-object v1

    .line 163
    .end local v1    # "rec":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;
    :cond_1
    new-instance v1, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;

    invoke-direct {v1, p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;-><init>(Ljava/lang/String;)V

    .line 164
    .restart local v1    # "rec":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;
    iget-object v2, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->fields:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private noTextFieldEdits(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 147
    .local p1, "textFieldData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;>;"
    if-eqz p1, :cond_1

    .line 148
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 149
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;

    invoke-virtual {v1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->isEdited()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    const/4 v1, 0x0

    .line 154
    .end local v0    # "i":I
    :goto_1
    return v1

    .line 148
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 154
    .end local v0    # "i":I
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private updateTextFieldRecords(Ljava/util/List;Z)V
    .locals 5
    .param p2, "action"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 130
    .local p1, "textFieldData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;>;"
    iget-boolean v3, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->fieldLoggingEnabled:Z

    if-eqz v3, :cond_1

    if-eqz p1, :cond_1

    .line 131
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 132
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;

    .line 133
    .local v0, "fieldData":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;
    invoke-virtual {v0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getFieldID()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->getTextFieldRecord(Ljava/lang/String;)Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;

    move-result-object v2

    .line 134
    .local v2, "rec":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;
    invoke-virtual {v2, v0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->update(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;)V

    .line 136
    if-eqz p2, :cond_0

    .line 137
    iget v3, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->actionUtteranceCount:I

    invoke-virtual {v0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getCountRecognitions()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->actionUtteranceCount:I

    .line 131
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 140
    :cond_0
    iget v3, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->backUtteranceCount:I

    invoke-virtual {v0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getCountRecognitions()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->backUtteranceCount:I

    goto :goto_1

    .line 144
    .end local v0    # "fieldData":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;
    .end local v1    # "i":I
    .end local v2    # "rec":Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;
    :cond_1
    return-void
.end method


# virtual methods
.method public actionClicked(Ljava/util/List;)Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;",
            ">;)",
            "Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;"
        }
    .end annotation

    .prologue
    .line 90
    .local p1, "textFieldData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->updateTextFieldRecords(Ljava/util/List;Z)V

    .line 91
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->noTextFieldEdits(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->actionNoEditCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->actionNoEditCount:I

    .line 96
    :goto_0
    return-object p0

    .line 95
    :cond_0
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->actionCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->actionCount:I

    goto :goto_0
.end method

.method public addLaunchTime(J)Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    .locals 2
    .param p1, "time"    # J

    .prologue
    .line 110
    iget-wide v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->launchTimeTotal:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->launchTimeTotal:J

    .line 111
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->launchCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->launchCount:I

    .line 112
    return-object p0
.end method

.method public alterPhrasePicked()Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->alternativePhrasePickedCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->alternativePhrasePickedCount:I

    .line 61
    return-object p0
.end method

.method public backClicked(Ljava/util/List;)Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;",
            ">;)",
            "Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;"
        }
    .end annotation

    .prologue
    .line 100
    .local p1, "textFieldData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->updateTextFieldRecords(Ljava/util/List;Z)V

    .line 101
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->noTextFieldEdits(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->backNoEditCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->backNoEditCount:I

    .line 106
    :goto_0
    return-object p0

    .line 105
    :cond_0
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->backCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->backCount:I

    goto :goto_0
.end method

.method public build()Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;
    .locals 1

    .prologue
    .line 169
    new-instance v0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;-><init>(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;)V

    return-object v0
.end method

.method public fromEmailChanged()Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->fromEmailChangedCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->fromEmailChangedCount:I

    .line 56
    return-object p0
.end method

.method public getFieldCount()I
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->fields:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public incrContactChange()Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->contactChangeCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->contactChangeCount:I

    .line 66
    return-object p0
.end method

.method public incrNoteChanged()Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->noteTypeChangeCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->noteTypeChangeCount:I

    .line 76
    return-object p0
.end method

.method public incrPhoneChange()Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->phoneChangeCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->phoneChangeCount:I

    .line 71
    return-object p0
.end method

.method public incrUndoCount()Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->undoCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->undoCount:I

    .line 81
    return-object p0
.end method

.method public pageViewed()Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->viewCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->viewCount:I

    .line 86
    return-object p0
.end method

.method public setReserved1(Ljava/lang/String;)Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    .locals 0
    .param p1, "reserved1"    # Ljava/lang/String;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->reserved1:Ljava/lang/String;

    .line 121
    return-object p0
.end method

.method public setReserved2(Ljava/lang/String;)Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;
    .locals 0
    .param p1, "reserved2"    # Ljava/lang/String;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$Builder;->reserved2:Ljava/lang/String;

    .line 126
    return-object p0
.end method
