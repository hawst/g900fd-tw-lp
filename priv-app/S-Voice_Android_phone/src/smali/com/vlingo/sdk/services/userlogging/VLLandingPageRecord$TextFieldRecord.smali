.class Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;
.super Ljava/lang/Object;
.source "VLLandingPageRecord.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TextFieldRecord"
.end annotation


# instance fields
.field private clearCount:I

.field private deleteCount:I

.field private fieldId:Ljava/lang/String;

.field private fixAcceptCount:I

.field private fixInvokeCount:I

.field private keyCount:I

.field private recognitionCount:I


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "fieldId"    # Ljava/lang/String;

    .prologue
    .line 276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 277
    iput-object p1, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->fieldId:Ljava/lang/String;

    .line 278
    return-void
.end method


# virtual methods
.method generateXml()Ljava/lang/String;
    .locals 3

    .prologue
    .line 290
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 291
    .local v0, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "<field field-id=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->fieldId:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\" fix-invoke-count=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->fixInvokeCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\" fix-accept-count=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->fixAcceptCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\" clear-count=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->clearCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\" key-count=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->keyCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\" char-delete-count=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->deleteCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\" char-recs-count=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->recognitionCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\"/>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method getFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->fieldId:Ljava/lang/String;

    return-object v0
.end method

.method update(Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;)V
    .locals 2
    .param p1, "counts"    # Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;

    .prologue
    .line 281
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->fixInvokeCount:I

    invoke-virtual {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getCountFixInvoke()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->fixInvokeCount:I

    .line 282
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->fixAcceptCount:I

    invoke-virtual {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getCountFixAccept()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->fixAcceptCount:I

    .line 283
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->clearCount:I

    invoke-virtual {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getCountClears()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->clearCount:I

    .line 284
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->keyCount:I

    invoke-virtual {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getCountKeys()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->keyCount:I

    .line 285
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->deleteCount:I

    invoke-virtual {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getCountDeletes()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->deleteCount:I

    .line 286
    iget v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->recognitionCount:I

    invoke-virtual {p1}, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldUsageCounts;->getCountRecognitions()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/vlingo/sdk/services/userlogging/VLLandingPageRecord$TextFieldRecord;->recognitionCount:I

    .line 287
    return-void
.end method
