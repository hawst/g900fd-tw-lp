.class public Lcom/vlingo/sdk/util/DebugSettings;
.super Ljava/lang/Object;
.source "DebugSettings.java"


# instance fields
.field private mCarrier:Ljava/lang/String;

.field private mCarrierCountry:Ljava/lang/String;

.field private mForceNonDM:Z

.field private mLocation:Ljava/lang/String;

.field private mLogRecoWaveform:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object v0, p0, Lcom/vlingo/sdk/util/DebugSettings;->mLocation:Ljava/lang/String;

    .line 14
    iput-object v0, p0, Lcom/vlingo/sdk/util/DebugSettings;->mCarrierCountry:Ljava/lang/String;

    .line 15
    iput-object v0, p0, Lcom/vlingo/sdk/util/DebugSettings;->mCarrier:Ljava/lang/String;

    .line 16
    iput-boolean v1, p0, Lcom/vlingo/sdk/util/DebugSettings;->mForceNonDM:Z

    .line 17
    iput-boolean v1, p0, Lcom/vlingo/sdk/util/DebugSettings;->mLogRecoWaveform:Z

    return-void
.end method


# virtual methods
.method public getCarrier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/vlingo/sdk/util/DebugSettings;->mCarrier:Ljava/lang/String;

    return-object v0
.end method

.method public getCarrierCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/sdk/util/DebugSettings;->mCarrierCountry:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vlingo/sdk/util/DebugSettings;->mLocation:Ljava/lang/String;

    return-object v0
.end method

.method public isForceNonDM()Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/vlingo/sdk/util/DebugSettings;->mForceNonDM:Z

    return v0
.end method

.method public isLogRecoWaveform()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/vlingo/sdk/util/DebugSettings;->mLogRecoWaveform:Z

    return v0
.end method

.method public setCarrier(Ljava/lang/String;)V
    .locals 0
    .param p1, "mCarrier"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/vlingo/sdk/util/DebugSettings;->mCarrier:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public setCarrierCountry(Ljava/lang/String;)V
    .locals 0
    .param p1, "carrierCountry"    # Ljava/lang/String;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/vlingo/sdk/util/DebugSettings;->mCarrierCountry:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public setForceNonDM(Z)V
    .locals 0
    .param p1, "forceNonDM"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/vlingo/sdk/util/DebugSettings;->mForceNonDM:Z

    .line 41
    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/vlingo/sdk/util/DebugSettings;->mLocation:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public setLogRecoWaveform(Z)V
    .locals 0
    .param p1, "logWaveForm"    # Z

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/vlingo/sdk/util/DebugSettings;->mLogRecoWaveform:Z

    .line 49
    return-void
.end method
