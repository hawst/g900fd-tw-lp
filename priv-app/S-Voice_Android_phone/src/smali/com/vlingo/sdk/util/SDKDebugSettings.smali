.class public Lcom/vlingo/sdk/util/SDKDebugSettings;
.super Ljava/lang/Object;
.source "SDKDebugSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;
    }
.end annotation


# instance fields
.field private mCarrier:Ljava/lang/String;

.field private mCarrierCountry:Ljava/lang/String;

.field private mForceNonDM:Z

.field private mLocation:Ljava/lang/String;

.field private mLogRecoWaveform:Z

.field private mModelNumber:Ljava/lang/String;

.field private mRawServerLogBase:Ljava/lang/String;

.field private saveAudioFiles:Z

.field private serverResponseLoggingState:Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object v1, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->mLocation:Ljava/lang/String;

    .line 50
    iput-object v1, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->mCarrierCountry:Ljava/lang/String;

    .line 51
    iput-object v1, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->mCarrier:Ljava/lang/String;

    .line 52
    iput-boolean v0, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->mForceNonDM:Z

    .line 53
    iput-boolean v0, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->mLogRecoWaveform:Z

    .line 54
    iput-boolean v0, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->saveAudioFiles:Z

    .line 55
    sget-object v0, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->NONE:Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    iput-object v0, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->serverResponseLoggingState:Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    .line 56
    const-string/jumbo v0, "rawServerLog"

    iput-object v0, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->mRawServerLogBase:Ljava/lang/String;

    .line 58
    iput-object v1, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->mModelNumber:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getCarrier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->mCarrier:Ljava/lang/String;

    return-object v0
.end method

.method public getCarrierCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->mCarrierCountry:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->mLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getModelNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->mModelNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getServerResponseLoggingState()Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->serverResponseLoggingState:Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    return-object v0
.end method

.method public getmRawServerLogBase()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->mRawServerLogBase:Ljava/lang/String;

    return-object v0
.end method

.method public isForceNonDM()Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->mForceNonDM:Z

    return v0
.end method

.method public isLogRecoWaveform()Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->mLogRecoWaveform:Z

    return v0
.end method

.method public isSaveAudioFiles()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->saveAudioFiles:Z

    return v0
.end method

.method public setCarrier(Ljava/lang/String;)V
    .locals 0
    .param p1, "mCarrier"    # Ljava/lang/String;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->mCarrier:Ljava/lang/String;

    .line 132
    return-void
.end method

.method public setCarrierCountry(Ljava/lang/String;)V
    .locals 0
    .param p1, "carrierCountry"    # Ljava/lang/String;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->mCarrierCountry:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public setForceNonDM(Z)V
    .locals 0
    .param p1, "forceNonDM"    # Z

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->mForceNonDM:Z

    .line 83
    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->mLocation:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public setLogRecoWaveform(Z)V
    .locals 0
    .param p1, "logWaveForm"    # Z

    .prologue
    .line 90
    iput-boolean p1, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->mLogRecoWaveform:Z

    .line 91
    return-void
.end method

.method public setModelNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "mModelNumber"    # Ljava/lang/String;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->mModelNumber:Ljava/lang/String;

    .line 124
    return-void
.end method

.method public setSaveAudioFiles(Z)V
    .locals 0
    .param p1, "saveAudioFiles"    # Z

    .prologue
    .line 98
    iput-boolean p1, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->saveAudioFiles:Z

    .line 99
    return-void
.end method

.method public setServerResponseLoggingState(Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;)V
    .locals 0
    .param p1, "serverResponseLoggingState"    # Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->serverResponseLoggingState:Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;

    .line 116
    return-void
.end method

.method public setmRawServerLogBase(Ljava/lang/String;)V
    .locals 0
    .param p1, "mRawServerLogBase"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/vlingo/sdk/util/SDKDebugSettings;->mRawServerLogBase:Ljava/lang/String;

    .line 107
    return-void
.end method
