.class public interface abstract Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerCallback;
.super Ljava/lang/Object;
.source "VLEmbeddedTrainerCallback.java"


# virtual methods
.method public abstract onCancelled()V
.end method

.method public abstract onError()V
.end method

.method public abstract onFinished(Ljava/util/Collection;Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onStarted()V
.end method
