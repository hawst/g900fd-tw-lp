.class public Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;
.super Ljava/lang/Object;
.source "VLEmbeddedTrainerSlotsUpdate.java"


# instance fields
.field private final addedTerminals:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;",
            ">;"
        }
    .end annotation
.end field

.field private final removedTerminals:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;",
            ">;"
        }
    .end annotation
.end field

.field private final slotName:Ljava/lang/String;

.field private final startFromScratch:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/util/Collection;Ljava/util/Collection;)V
    .locals 0
    .param p1, "slotName"    # Ljava/lang/String;
    .param p2, "startFromScratch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    .local p3, "addedTerminals":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;>;"
    .local p4, "removedTerminals":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;->slotName:Ljava/lang/String;

    .line 26
    iput-boolean p2, p0, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;->startFromScratch:Z

    .line 27
    iput-object p3, p0, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;->addedTerminals:Ljava/util/Collection;

    .line 28
    iput-object p4, p0, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;->removedTerminals:Ljava/util/Collection;

    .line 29
    return-void
.end method


# virtual methods
.method public getAddedTerminals()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;->addedTerminals:Ljava/util/Collection;

    return-object v0
.end method

.method public getRemovedTerminals()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotTerminal;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;->removedTerminals:Ljava/util/Collection;

    return-object v0
.end method

.method public getSlotName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;->slotName:Ljava/lang/String;

    return-object v0
.end method

.method public isStartFromScratch()Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;->startFromScratch:Z

    return v0
.end method
