.class public interface abstract Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;
.super Ljava/lang/Object;
.source "VLEmbeddedDialogManager.java"

# interfaces
.implements Lcom/vlingo/sdk/VLComponent;


# virtual methods
.method public abstract backgroundInit()V
.end method

.method public abstract increasePriority()V
.end method

.method public abstract isEnabled()Z
.end method

.method public abstract setEnabled(Z)V
.end method

.method public abstract updateDynamicFeaturesConfig()V
.end method
