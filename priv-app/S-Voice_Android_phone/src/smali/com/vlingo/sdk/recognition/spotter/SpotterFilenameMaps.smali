.class public Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;
.super Ljava/lang/Object;
.source "SpotterFilenameMaps.java"


# static fields
.field public static SPOTTER_ACOUSTIC_MODEL_FILENAME:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static SPOTTER_PRONUN_MODEL_FILENAME:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static SPOTTER_S2_ACOUSTIC_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static SPOTTER_S2_ACOUSTIC_MODEL_QUIET_FILENAME:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static SPOTTER_S2_GRAMMAR_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static SPOTTER_S2_GRAMMAR_MODEL_QUIET_FILENAME:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xb

    const/16 v3, 0xd

    .line 14
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v4}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_ACOUSTIC_MODEL_FILENAME:Ljava/util/HashMap;

    .line 16
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_ACOUSTIC_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "en-US"

    const-string/jumbo v2, "nn_en_us_mfcc_16k_15_big_250_v4_5.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_ACOUSTIC_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "en-GB"

    const-string/jumbo v2, "nn_en_uk_mfcc_16k_15_big_250_v2_0.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_ACOUSTIC_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "fr-FR"

    const-string/jumbo v2, "nn_fr_mfcc_16k_15_big_250_v2_2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_ACOUSTIC_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "it-IT"

    const-string/jumbo v2, "nn_it_mfcc_16k_15_big_250_v3_2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_ACOUSTIC_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "de-DE"

    const-string/jumbo v2, "nn_de_mfcc_16k_15_big_250_v3_0.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_ACOUSTIC_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "es-ES"

    const-string/jumbo v2, "nn_es_mfcc_16k_15_big_250_v1_1.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_ACOUSTIC_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "v-es-LA"

    const-string/jumbo v2, "nn_es_mfcc_16k_15_big_250_v1_1.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_ACOUSTIC_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "v-es-NA"

    const-string/jumbo v2, "nn_es_mfcc_16k_15_big_250_v1_1.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_ACOUSTIC_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "zh-CN"

    const-string/jumbo v2, "nn_zh_mfcc_16k_15_big_250_v2_2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_ACOUSTIC_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "ja-JP"

    const-string/jumbo v2, "nn_ja_mfcc_16k_15_big_250_v4_2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_ACOUSTIC_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "ko-KR"

    const-string/jumbo v2, "nn_ko_mfcc_16k_15_big_250_v2_1.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v4}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_PRONUN_MODEL_FILENAME:Ljava/util/HashMap;

    .line 37
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_PRONUN_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "en-US"

    const-string/jumbo v2, "lts_en_us_9.2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_PRONUN_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "en-GB"

    const-string/jumbo v2, "lts_en_2.8.1.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_PRONUN_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "fr-FR"

    const-string/jumbo v2, "lts_fr_3.12.1.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_PRONUN_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "it-IT"

    const-string/jumbo v2, "lts_it_1.6.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_PRONUN_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "de-DE"

    const-string/jumbo v2, "lts_de_3.3.5.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_PRONUN_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "es-ES"

    const-string/jumbo v2, "lts_es_3.14.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_PRONUN_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "v-es-LA"

    const-string/jumbo v2, "lts_es_3.14.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_PRONUN_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "v-es-NA"

    const-string/jumbo v2, "lts_es_3.14.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_PRONUN_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "zh-CN"

    const-string/jumbo v2, "lts_zh_3.2.5.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_PRONUN_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "ja-JP"

    const-string/jumbo v2, "lts_jp_5.17.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_PRONUN_MODEL_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "ko-KR"

    const-string/jumbo v2, "lts_ko_1.0.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    .line 58
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "en-US"

    const-string/jumbo v2, "samsung_wakeup_am_quiet_en_us_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "en-GB"

    const-string/jumbo v2, "samsung_wakeup_am_quiet_en_gb_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "fr-FR"

    const-string/jumbo v2, "samsung_wakeup_am_quiet_fr_fr_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "it-IT"

    const-string/jumbo v2, "samsung_wakeup_am_quiet_it_it_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "de-DE"

    const-string/jumbo v2, "samsung_wakeup_am_quiet_de_de_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "es-ES"

    const-string/jumbo v2, "samsung_wakeup_am_quiet_es_es_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "v-es-LA"

    const-string/jumbo v2, "samsung_wakeup_am_quiet_es_la_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "v-es-NA"

    const-string/jumbo v2, "samsung_wakeup_am_quiet_es_es_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "zh-CN"

    const-string/jumbo v2, "samsung_wakeup_am_quiet_zh_cn_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "ja-JP"

    const-string/jumbo v2, "samsung_wakeup_am_quiet_ja_jp_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "ko-KR"

    const-string/jumbo v2, "samsung_wakeup_am_quiet_ko_kr_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "ru-RU"

    const-string/jumbo v2, "samsung_wakeup_am_quiet_ru_ru_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "pt-BR"

    const-string/jumbo v2, "samsung_wakeup_am_quiet_pt_br_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    .line 81
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "en-US"

    const-string/jumbo v2, "samsung_wakeup_am_media_en_us_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "en-GB"

    const-string/jumbo v2, "samsung_wakeup_am_media_en_gb_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "fr-FR"

    const-string/jumbo v2, "samsung_wakeup_am_media_fr_fr_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "it-IT"

    const-string/jumbo v2, "samsung_wakeup_am_media_it_it_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "de-DE"

    const-string/jumbo v2, "samsung_wakeup_am_media_de_de_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "es-ES"

    const-string/jumbo v2, "samsung_wakeup_am_media_es_es_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "v-es-LA"

    const-string/jumbo v2, "samsung_wakeup_am_media_es_la_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "v-es-NA"

    const-string/jumbo v2, "samsung_wakeup_am_media_es_es_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "zh-CN"

    const-string/jumbo v2, "samsung_wakeup_am_media_zh_cn_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "ja-JP"

    const-string/jumbo v2, "samsung_wakeup_am_media_ja_jp_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "ko-KR"

    const-string/jumbo v2, "samsung_wakeup_am_media_ko_kr_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "ru-RU"

    const-string/jumbo v2, "samsung_wakeup_am_media_ru_ru_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_ACOUSTIC_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "pt-BR"

    const-string/jumbo v2, "samsung_wakeup_am_media_pt_br_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    .line 104
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "en-US"

    const-string/jumbo v2, "samsung_wakeup_grammar_quiet_en_us_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "en-GB"

    const-string/jumbo v2, "samsung_wakeup_grammar_quiet_en_gb_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "fr-FR"

    const-string/jumbo v2, "samsung_wakeup_grammar_quiet_fr_fr_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "it-IT"

    const-string/jumbo v2, "samsung_wakeup_grammar_quiet_it_it_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "de-DE"

    const-string/jumbo v2, "samsung_wakeup_grammar_quiet_de_de_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "es-ES"

    const-string/jumbo v2, "samsung_wakeup_grammar_quiet_es_es_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "v-es-LA"

    const-string/jumbo v2, "samsung_wakeup_grammar_quiet_es_la_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "v-es-NA"

    const-string/jumbo v2, "samsung_wakeup_grammar_quiet_es_es_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "zh-CN"

    const-string/jumbo v2, "samsung_wakeup_grammar_quiet_zh_cn_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "ja-JP"

    const-string/jumbo v2, "samsung_wakeup_grammar_quiet_ja_jp_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "ko-KR"

    const-string/jumbo v2, "samsung_wakeup_grammar_quiet_ko_kr_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "ru-RU"

    const-string/jumbo v2, "samsung_wakeup_grammar_quiet_ru_ru_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_QUIET_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "pt-BR"

    const-string/jumbo v2, "samsung_wakeup_grammar_quiet_pt_br_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    .line 127
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "en-US"

    const-string/jumbo v2, "samsung_wakeup_grammar_media_en_us_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "en-GB"

    const-string/jumbo v2, "samsung_wakeup_grammar_media_en_gb_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "fr-FR"

    const-string/jumbo v2, "samsung_wakeup_grammar_media_fr_fr_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "it-IT"

    const-string/jumbo v2, "samsung_wakeup_grammar_media_it_it_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "de-DE"

    const-string/jumbo v2, "samsung_wakeup_grammar_media_de_de_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "es-ES"

    const-string/jumbo v2, "samsung_wakeup_grammar_media_es_es_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "v-es-LA"

    const-string/jumbo v2, "samsung_wakeup_grammar_media_es_la_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "v-es-NA"

    const-string/jumbo v2, "samsung_wakeup_grammar_media_es_es_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "zh-CN"

    const-string/jumbo v2, "samsung_wakeup_grammar_media_zh_cn_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "ja-JP"

    const-string/jumbo v2, "samsung_wakeup_grammar_media_ja_jp_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "ko-KR"

    const-string/jumbo v2, "samsung_wakeup_grammar_media_ko_kr_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "ru-RU"

    const-string/jumbo v2, "samsung_wakeup_grammar_media_ru_ru_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    sget-object v0, Lcom/vlingo/sdk/recognition/spotter/SpotterFilenameMaps;->SPOTTER_S2_GRAMMAR_MODEL_MEDIA_FILENAME:Ljava/util/HashMap;

    const-string/jumbo v1, "pt-BR"

    const-string/jumbo v2, "samsung_wakeup_grammar_media_pt_br_v2.raw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
