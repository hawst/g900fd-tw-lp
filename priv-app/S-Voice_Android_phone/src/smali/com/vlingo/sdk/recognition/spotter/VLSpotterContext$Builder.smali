.class public Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;
.super Ljava/lang/Object;
.source "VLSpotterContext.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private absbeam:F

.field private aoffset:F

.field private audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

.field private beam:F

.field private delay:F

.field private grammarSource:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;

.field private language:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;)V
    .locals 1
    .param p1, "grammarSource"    # Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    sget-object v0, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;->PCM_16KHZ_16BIT:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    invoke-static {v0}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getDataBufferSource(Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;)Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    .line 158
    const-string/jumbo v0, "en-US"

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->language:Ljava/lang/String;

    .line 160
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->beam:F

    .line 161
    const/high16 v0, 0x42200000    # 40.0f

    iput v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->absbeam:F

    .line 162
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->aoffset:F

    .line 163
    const/high16 v0, 0x42c80000    # 100.0f

    iput v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->delay:F

    .line 166
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->validateDefined(Ljava/lang/Object;)V

    .line 167
    iput-object p1, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->grammarSource:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;

    .line 168
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;)Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;

    .prologue
    .line 156
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;

    .prologue
    .line 156
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->language:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;)Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;

    .prologue
    .line 156
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->grammarSource:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;)F
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;

    .prologue
    .line 156
    iget v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->beam:F

    return v0
.end method

.method static synthetic access$500(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;)F
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;

    .prologue
    .line 156
    iget v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->absbeam:F

    return v0
.end method

.method static synthetic access$600(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;)F
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;

    .prologue
    .line 156
    iget v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->aoffset:F

    return v0
.end method

.method static synthetic access$700(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;)F
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;

    .prologue
    .line 156
    iget v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->delay:F

    return v0
.end method

.method private validateDefined(Ljava/lang/Object;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/Object;

    .prologue
    .line 213
    if-nez p1, :cond_0

    .line 214
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Value cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 216
    :cond_0
    return-void
.end method

.method private validateStringNotEmpty(Ljava/lang/String;)V
    .locals 2
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 219
    invoke-static {p1}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Value cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 222
    :cond_0
    return-void
.end method


# virtual methods
.method public audioSourceInfo(Lcom/vlingo/sdk/recognition/AudioSourceInfo;)Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;
    .locals 4
    .param p1, "info"    # Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    .prologue
    .line 175
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->validateDefined(Ljava/lang/Object;)V

    .line 176
    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getType()Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    move-result-object v1

    .line 177
    .local v1, "type":Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;
    sget-object v2, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;->BUFFER:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceType;

    if-eq v1, v2, :cond_0

    .line 178
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Unsupported audio source, supported formats are: SourceType.BUFFER"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 180
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getFormat()Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    move-result-object v0

    .line 181
    .local v0, "format":Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;
    sget-object v2, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;->PCM_16KHZ_16BIT:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    if-eq v0, v2, :cond_1

    .line 182
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Unsupported audio format format, supported formats are: SourceFormat.PCM_16KHZ_16BIT"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 184
    :cond_1
    iput-object p1, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    .line 185
    return-object p0
.end method

.method public build()Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;
    .locals 2

    .prologue
    .line 209
    new-instance v0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;-><init>(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$1;)V

    return-object v0
.end method

.method public language(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;
    .locals 0
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 189
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->validateStringNotEmpty(Ljava/lang/String;)V

    .line 190
    iput-object p1, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->language:Ljava/lang/String;

    .line 191
    return-object p0
.end method

.method public spotterParams(FFFFLjava/lang/String;)Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;
    .locals 0
    .param p1, "beam"    # F
    .param p2, "absbeam"    # F
    .param p3, "aoffset"    # F
    .param p4, "delay"    # F
    .param p5, "language"    # Ljava/lang/String;

    .prologue
    .line 195
    iput p1, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->beam:F

    .line 196
    iput p2, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->absbeam:F

    .line 197
    iput p3, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->aoffset:F

    .line 198
    iput p4, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->delay:F

    .line 199
    iput-object p5, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->language:Ljava/lang/String;

    .line 200
    return-object p0
.end method
