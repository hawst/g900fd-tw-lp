.class public interface abstract Lcom/vlingo/sdk/recognition/VLRecognitionResult;
.super Ljava/lang/Object;
.source "VLRecognitionResult.java"


# virtual methods
.method public abstract getActions()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getDialogGUID()Ljava/lang/String;
.end method

.method public abstract getDialogState()[B
.end method

.method public abstract getDialogTurn()I
.end method

.method public abstract getFieldId()Ljava/lang/String;
.end method

.method public abstract getGUttId()Ljava/lang/String;
.end method

.method public abstract getNBestData()Lcom/vlingo/sdk/recognition/NBestData;
.end method

.method public abstract getParseGroup()Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;
.end method

.method public abstract getRawResponseString()Ljava/lang/String;
.end method

.method public abstract getResultString()Ljava/lang/String;
.end method

.method public abstract isFromEDM()Z
.end method
