.class public final Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;
.super Ljava/lang/Object;
.source "VLSpotterContext.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GrammarSource"
.end annotation


# instance fields
.field private cgFile:Ljava/lang/String;

.field private grammarSpec:Ljava/lang/String;

.field private pronunciationList:[Ljava/lang/String;

.field private wordList:[Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCompiledFileSource(Ljava/lang/String;)Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;
    .locals 3
    .param p0, "cgFile"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-static {p0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 63
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "cgFile cannot be null or empty"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 65
    :cond_0
    new-instance v0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;

    invoke-direct {v0}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;-><init>()V

    .line 66
    .local v0, "gs":Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;
    iput-object p0, v0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;->cgFile:Ljava/lang/String;

    .line 67
    return-object v0
.end method

.method private static getGrammarSpec([Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "phraseList"    # [Ljava/lang/String;

    .prologue
    .line 124
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "g=("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 125
    .local v4, "sb":Ljava/lang/StringBuilder;
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 126
    .local v3, "phrase":Ljava/lang/String;
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    const-string/jumbo v5, "|"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 129
    .end local v3    # "phrase":Ljava/lang/String;
    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 130
    const-string/jumbo v5, ");\nparamA:$g 0;\nparamB:$g 320;\nparamC:$g 500;\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public static getGrammarSpecSource(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;
    .locals 3
    .param p0, "grammarSpec"    # Ljava/lang/String;
    .param p1, "wordList"    # [Ljava/lang/String;
    .param p2, "pronunciationList"    # [Ljava/lang/String;

    .prologue
    .line 80
    invoke-static {p0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "grammarSpec cannot be null or empty"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 83
    :cond_0
    if-eqz p1, :cond_1

    array-length v1, p1

    if-nez v1, :cond_2

    .line 84
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "wordList cannot be null and must have at least 1 element"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 86
    :cond_2
    if-eqz p2, :cond_3

    array-length v1, p2

    array-length v2, p1

    if-eq v1, v2, :cond_3

    .line 87
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "when not null, pronunciationList must contain the same number of elements as wordList"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 89
    :cond_3
    new-instance v0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;

    invoke-direct {v0}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;-><init>()V

    .line 90
    .local v0, "gs":Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;
    iput-object p0, v0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;->grammarSpec:Ljava/lang/String;

    .line 91
    iput-object p1, v0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;->wordList:[Ljava/lang/String;

    .line 92
    iput-object p2, v0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;->pronunciationList:[Ljava/lang/String;

    .line 93
    return-object v0
.end method

.method public static getGrammarSpecSource([Ljava/lang/String;)Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;
    .locals 3
    .param p0, "phraseList"    # [Ljava/lang/String;

    .prologue
    .line 106
    if-eqz p0, :cond_0

    array-length v1, p0

    if-nez v1, :cond_1

    .line 107
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "phraseList cannot be null and must have at least 1 element"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 109
    :cond_1
    new-instance v0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;

    invoke-direct {v0}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;-><init>()V

    .line 110
    .local v0, "gs":Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;
    invoke-static {p0}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;->getGrammarSpec([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;->grammarSpec:Ljava/lang/String;

    .line 111
    invoke-static {p0}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;->getWordList([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;->wordList:[Ljava/lang/String;

    .line 112
    return-object v0
.end method

.method private static getWordList([Ljava/lang/String;)[Ljava/lang/String;
    .locals 12
    .param p0, "phraseList"    # [Ljava/lang/String;

    .prologue
    .line 135
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 136
    .local v9, "wordList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    move v3, v2

    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v4    # "len$":I
    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v6, v0, v3

    .line 137
    .local v6, "phrase":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 138
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v11

    if-lez v11, :cond_1

    .line 139
    const/16 v11, 0x20

    invoke-static {v6, v11}, Lcom/vlingo/sdk/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v8

    .line 140
    .local v8, "phraseWords":[Ljava/lang/String;
    move-object v1, v8

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v2, 0x0

    .end local v3    # "i$":I
    .restart local v2    # "i$":I
    :goto_1
    if-ge v2, v5, :cond_1

    aget-object v7, v1, v2

    .line 141
    .local v7, "phraseWord":Ljava/lang/String;
    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 142
    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 136
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v5    # "len$":I
    .end local v7    # "phraseWord":Ljava/lang/String;
    .end local v8    # "phraseWords":[Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v3, 0x1

    .restart local v2    # "i$":I
    move v3, v2

    .end local v2    # "i$":I
    .restart local v3    # "i$":I
    goto :goto_0

    .line 147
    .end local v6    # "phrase":Ljava/lang/String;
    :cond_2
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v11

    new-array v10, v11, [Ljava/lang/String;

    .line 148
    .local v10, "words":[Ljava/lang/String;
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 149
    return-object v10
.end method


# virtual methods
.method public getCompiledFilepath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;->cgFile:Ljava/lang/String;

    return-object v0
.end method

.method public getGrammarSpec()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;->grammarSpec:Ljava/lang/String;

    return-object v0
.end method

.method public getPronunciationList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;->pronunciationList:[Ljava/lang/String;

    return-object v0
.end method

.method public getWordList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;->wordList:[Ljava/lang/String;

    return-object v0
.end method

.method public isCompiledFileSource()Z
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;->cgFile:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGrammarSpecSource()Z
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;->grammarSpec:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
