.class public interface abstract Lcom/vlingo/sdk/recognition/spotter/VLSpotter;
.super Ljava/lang/Object;
.source "VLSpotter.java"

# interfaces
.implements Lcom/vlingo/sdk/VLComponent;


# virtual methods
.method public abstract getLastScore()F
.end method

.method public abstract getSupportedLanguageList()[Ljava/lang/String;
.end method

.method public abstract phrasespotPipe(Ljava/nio/ByteBuffer;J)Ljava/lang/String;
.end method

.method public abstract processShortArray([SII)Ljava/lang/String;
.end method

.method public abstract startSpotter(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;Ljava/lang/String;ZZ)Z
.end method

.method public abstract stopSpotter()V
.end method
