.class public final Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;
.super Ljava/lang/Object;
.source "VLSpotterContext.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$1;,
        Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;,
        Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;
    }
.end annotation


# static fields
.field public static final DEFAULT_LANGUAGE:Ljava/lang/String; = "en-US"

.field public static final DEFAULT_PHRASESPOT_ABSBEAM:F = 40.0f

.field public static final DEFAULT_PHRASESPOT_AOFFSET:F = 0.0f

.field public static final DEFAULT_PHRASESPOT_BEAM:F = 20.0f

.field public static final DEFAULT_PHRASESPOT_DELAY:F = 100.0f

.field public static final DEFAULT_PHRASESPOT_PARAMA:I = 0x0

.field public static final DEFAULT_PHRASESPOT_PARAMB:I = 0x140

.field public static final DEFAULT_PHRASESPOT_PARAMC:I = 0x1f4


# instance fields
.field absbeam:F

.field aoffset:F

.field private audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

.field beam:F

.field delay:F

.field grammarSource:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;

.field private language:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;

    .prologue
    .line 233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 234
    # getter for: Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->access$100(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;)Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    .line 235
    # getter for: Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->language:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->access$200(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->language:Ljava/lang/String;

    .line 236
    # getter for: Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->grammarSource:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->access$300(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;)Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->grammarSource:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;

    .line 237
    # getter for: Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->beam:F
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->access$400(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;)F

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->beam:F

    .line 238
    # getter for: Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->absbeam:F
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->access$500(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;)F

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->absbeam:F

    .line 239
    # getter for: Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->aoffset:F
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->access$600(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;)F

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->aoffset:F

    .line 240
    # getter for: Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->delay:F
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;->access$700(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;)F

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->delay:F

    .line 241
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;
    .param p2, "x1"    # Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$1;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;-><init>(Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$Builder;)V

    return-void
.end method


# virtual methods
.method public getAbsBeam()F
    .locals 1

    .prologue
    .line 247
    iget v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->absbeam:F

    return v0
.end method

.method public getAoffset()F
    .locals 1

    .prologue
    .line 248
    iget v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->aoffset:F

    return v0
.end method

.method public getAudioSourceInfo()Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    return-object v0
.end method

.method public getBeam()F
    .locals 1

    .prologue
    .line 246
    iget v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->beam:F

    return v0
.end method

.method public getDelay()F
    .locals 1

    .prologue
    .line 249
    iget v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->delay:F

    return v0
.end method

.method public getGrammarSource()Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->grammarSource:Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext$GrammarSource;

    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/spotter/VLSpotterContext;->language:Ljava/lang/String;

    return-object v0
.end method
