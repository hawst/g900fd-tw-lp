.class public final Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;
.super Lcom/vlingo/sdk/recognition/VLRecognitionContext;
.source "VLDialogContext.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$1;,
        Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;
    }
.end annotation


# instance fields
.field private final dialogGUID:Ljava/lang/String;

.field private final dialogState:[B

.field private final dialogTurnNumber:I

.field private final dmHeaderKVPairs:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final eventList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final isFromEDM:Z

.field private final metaKVPairs:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final username:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;

    .prologue
    .line 165
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;-><init>(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)V

    .line 166
    # getter for: Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->dialogState:[B
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->access$100(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->dialogState:[B

    .line 167
    # getter for: Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->username:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->access$200(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->username:Ljava/lang/String;

    .line 168
    # getter for: Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->dialogGUID:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->access$300(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->dialogGUID:Ljava/lang/String;

    .line 169
    # getter for: Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->dialogTurnNumber:I
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->access$400(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->dialogTurnNumber:I

    .line 170
    new-instance v0, Ljava/util/ArrayList;

    # getter for: Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->eventList:Ljava/util/List;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->access$500(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->eventList:Ljava/util/List;

    .line 171
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->eventList:Ljava/util/List;

    # getter for: Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->eventList:Ljava/util/List;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->access$500(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 172
    new-instance v0, Ljava/util/HashMap;

    # getter for: Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->dmHeaderKVPairs:Ljava/util/HashMap;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->access$600(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->dmHeaderKVPairs:Ljava/util/HashMap;

    .line 173
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->dmHeaderKVPairs:Ljava/util/HashMap;

    # getter for: Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->dmHeaderKVPairs:Ljava/util/HashMap;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->access$600(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 174
    new-instance v0, Ljava/util/HashMap;

    # getter for: Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->metaKVPairs:Ljava/util/HashMap;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->access$700(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->metaKVPairs:Ljava/util/HashMap;

    .line 175
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->metaKVPairs:Ljava/util/HashMap;

    # getter for: Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->metaKVPairs:Ljava/util/HashMap;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->access$700(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 176
    # getter for: Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->isFromEDM:Z
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;->access$800(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->isFromEDM:Z

    .line 177
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;
    .param p2, "x1"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$1;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;-><init>(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext$Builder;)V

    return-void
.end method


# virtual methods
.method public getDMHeaderKVPairs()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 224
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->dmHeaderKVPairs:Ljava/util/HashMap;

    return-object v0
.end method

.method public getDialogGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->dialogGUID:Ljava/lang/String;

    return-object v0
.end method

.method public getDialogState()[B
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->dialogState:[B

    return-object v0
.end method

.method public getDialogTurnNumber()I
    .locals 1

    .prologue
    .line 216
    iget v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->dialogTurnNumber:I

    return v0
.end method

.method public getEvents()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->eventList:Ljava/util/List;

    return-object v0
.end method

.method public getMetaKVPairs()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 232
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->metaKVPairs:Ljava/util/HashMap;

    return-object v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->username:Ljava/lang/String;

    return-object v0
.end method

.method public isEDMFlow()Z
    .locals 1

    .prologue
    .line 240
    iget-boolean v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->isFromEDM:Z

    return v0
.end method
