.class public interface abstract Lcom/vlingo/sdk/recognition/VLAction;
.super Ljava/lang/Object;
.source "VLAction.java"


# virtual methods
.method public abstract getElseStatement()Ljava/lang/String;
.end method

.method public abstract getIfCondition()Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getParamValue(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getParameterNames()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isConditional()Z
.end method
