.class public Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
.super Ljava/lang/Object;
.source "VLDialogEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;
    }
.end annotation


# instance fields
.field fieldGroupList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;",
            ">;"
        }
    .end annotation
.end field

.field fieldList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field name:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    # getter for: Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->name:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->access$000(Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;->name:Ljava/lang/String;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    # getter for: Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->fieldList:Ljava/util/ArrayList;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->access$100(Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;->fieldList:Ljava/util/ArrayList;

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    # getter for: Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->fieldGroupList:Ljava/util/ArrayList;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;->access$200(Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent$Builder;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;->fieldGroupList:Ljava/util/ArrayList;

    .line 82
    return-void
.end method


# virtual methods
.method generateXML(Ljava/lang/String;Ljava/lang/StringBuilder;)V
    .locals 5
    .param p1, "tagName"    # Ljava/lang/String;
    .param p2, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 105
    const-string/jumbo v3, "<"

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    const-string/jumbo v3, "n"

    iget-object v4, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;->name:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    const-string/jumbo v3, ">"

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    iget-object v3, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;->fieldList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 109
    .local v0, "field":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v3, "<Field "

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    const-string/jumbo v4, "n"

    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-static {v4, v3}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    const-string/jumbo v4, "v"

    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-static {v4, v3}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    const-string/jumbo v3, "/>"

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 114
    .end local v0    # "field":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    iget-object v3, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;->fieldGroupList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;

    .line 115
    .local v1, "group":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;
    const-string/jumbo v3, "FieldGroup"

    invoke-virtual {v1, v3, p2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;->generateXML(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    goto :goto_1

    .line 117
    .end local v1    # "group":Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;
    :cond_1
    const-string/jumbo v3, "</"

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ">"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    return-void
.end method

.method public getFieldGroups()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/dialog/VLDialogEventFieldGroup;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;->fieldGroupList:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getFields()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 90
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;->fieldList:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getXML()Ljava/lang/String;
    .locals 2

    .prologue
    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "Event"

    invoke-virtual {p0, v1, v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;->generateXML(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 101
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
