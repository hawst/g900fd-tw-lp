.class public interface abstract Lcom/vlingo/sdk/recognition/VLRecognizer;
.super Ljava/lang/Object;
.source "VLRecognizer.java"

# interfaces
.implements Lcom/vlingo/sdk/VLComponent;


# virtual methods
.method public abstract acceptedText(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract cancelRecognition()V
.end method

.method public abstract getIsStoppedDataReader()Z
.end method

.method public abstract getSupportedLanguageList()[Ljava/lang/String;
.end method

.method public abstract sendEvent(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;Lcom/vlingo/sdk/recognition/VLRecognitionListener;)V
.end method

.method public abstract startRecognition(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/sdk/recognition/VLRecognitionListener;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;Lcom/vlingo/sdk/recognition/RecognitionMode;)V
.end method

.method public abstract stopRecognition()V
.end method
