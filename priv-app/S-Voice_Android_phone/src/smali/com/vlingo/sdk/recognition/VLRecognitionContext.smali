.class public Lcom/vlingo/sdk/recognition/VLRecognitionContext;
.super Ljava/lang/Object;
.source "VLRecognitionContext.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;,
        Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;
    }
.end annotation


# static fields
.field public static final DEFAULT_AUDIO_FORMAT_CHANNEL_CONFIG:I = 0x2

.field public static final DEFAULT_AUTO_ENDPOINTING:Z = false

.field public static final DEFAULT_AUTO_PUNCTUATION:Z = true

.field public static final DEFAULT_FIELD_ID:Ljava/lang/String; = "vp_car_main"

.field public static final DEFAULT_LANGUAGE:Ljava/lang/String; = "en-US"

.field public static final DEFAULT_MIN_VOICE_DURATION:F = 0.08f

.field public static final DEFAULT_MIN_VOICE_LEVEL:F = 57.0f

.field public static final DEFAULT_PROFANITY_FILTER:Z = true

.field public static final DEFAULT_SILENCE_THRESHOLD:F = 11.0f

.field public static final DEFAULT_SPEEX_COMPLEXITY:I = 0x3

.field public static final DEFAULT_SPEEX_QUALITY:I = 0x8

.field public static final DEFAULT_SPEEX_VARIABLE_BITRATE:I = 0x0

.field public static final DEFAULT_SPEEX_VOICE_ACTIVITY_DETECTION:I = 0x0

.field public static final DEFAULT_VOICE_PORTION:F = 0.02f

.field public static final MAX_AUDIO_TIME:I = 0x9c40


# instance fields
.field private appName:Ljava/lang/String;

.field private audioFormatChannelConfig:I

.field private audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

.field private autoEndpointing:Z

.field private autoPunctuation:Z

.field private capitalizationMode:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

.field private controlName:Ljava/lang/String;

.field private currentText:Ljava/lang/String;

.field private cursorPosition:I

.field private fieldID:Ljava/lang/String;

.field private language:Ljava/lang/String;

.field private maxAudioTime:I

.field private minVoiceDuration:F

.field private minVoiceLevel:F

.field private nospeechEndpointTimeout:I

.field private profanityFilter:Z

.field private recoMode:Lcom/vlingo/sdk/recognition/RecognitionMode;

.field private screenName:Ljava/lang/String;

.field private silenceThreshold:F

.field private speechEndpointTimeout:I

.field private speexComplexity:I

.field private speexQuality:I

.field private speexVariableBitrate:I

.field private speexVoiceActivityDetection:I

.field private voicePortion:F


# direct methods
.method protected constructor <init>(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;

    .prologue
    .line 356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354
    sget-object v0, Lcom/vlingo/sdk/recognition/RecognitionMode;->CLOUD:Lcom/vlingo/sdk/recognition/RecognitionMode;

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->recoMode:Lcom/vlingo/sdk/recognition/RecognitionMode;

    .line 357
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->language:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$000(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->language:Ljava/lang/String;

    .line 358
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->maxAudioTime:I
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$100(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->maxAudioTime:I

    .line 359
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->autoEndpointing:Z
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$200(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->autoEndpointing:Z

    .line 360
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speechEndpointTimeout:I
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$300(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->speechEndpointTimeout:I

    .line 361
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->nospeechEndpointTimeout:I
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$400(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->nospeechEndpointTimeout:I

    .line 362
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->silenceThreshold:F
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$500(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)F

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->silenceThreshold:F

    .line 363
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->minVoiceDuration:F
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$600(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)F

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->minVoiceDuration:F

    .line 364
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->voicePortion:F
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$700(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)F

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->voicePortion:F

    .line 365
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->minVoiceLevel:F
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$800(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)F

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->minVoiceLevel:F

    .line 366
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speexQuality:I
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$900(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->speexQuality:I

    .line 367
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speexVariableBitrate:I
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$1000(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->speexVariableBitrate:I

    .line 368
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speexVoiceActivityDetection:I
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$1100(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->speexVoiceActivityDetection:I

    .line 369
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->speexComplexity:I
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$1200(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->speexComplexity:I

    .line 370
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$1300(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    .line 371
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->autoPunctuation:Z
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$1400(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->autoPunctuation:Z

    .line 372
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->capitalizationMode:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$1500(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->capitalizationMode:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    .line 373
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->fieldID:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$1600(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->fieldID:Ljava/lang/String;

    .line 374
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->appName:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$1700(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->appName:Ljava/lang/String;

    .line 375
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->screenName:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$1800(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->screenName:Ljava/lang/String;

    .line 376
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->controlName:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$1900(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->controlName:Ljava/lang/String;

    .line 377
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->currentText:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$2000(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->currentText:Ljava/lang/String;

    .line 378
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->cursorPosition:I
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$2100(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->cursorPosition:I

    .line 379
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->profanityFilter:Z
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$2200(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->profanityFilter:Z

    .line 380
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->audioFormatChannelConfig:I
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$2300(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->audioFormatChannelConfig:I

    .line 381
    # getter for: Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->recoMode:Lcom/vlingo/sdk/recognition/RecognitionMode;
    invoke-static {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;->access$2400(Lcom/vlingo/sdk/recognition/VLRecognitionContext$Builder;)Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->recoMode:Lcom/vlingo/sdk/recognition/RecognitionMode;

    .line 382
    return-void
.end method


# virtual methods
.method public autoEndpointingEnabled()Z
    .locals 1

    .prologue
    .line 394
    iget-boolean v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->autoEndpointing:Z

    return v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->appName:Ljava/lang/String;

    return-object v0
.end method

.method public getAudioFormatChannelConfig()I
    .locals 1

    .prologue
    .line 407
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->audioFormatChannelConfig:I

    return v0
.end method

.method public getAudioSourceInfo()Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->audioSourceInfo:Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    return-object v0
.end method

.method public getAutoPunctuation()Z
    .locals 1

    .prologue
    .line 389
    iget-boolean v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->autoPunctuation:Z

    return v0
.end method

.method public getCapitalizationMode()Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->capitalizationMode:Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    return-object v0
.end method

.method public getControlName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->controlName:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->currentText:Ljava/lang/String;

    return-object v0
.end method

.method public getCursorPosition()I
    .locals 1

    .prologue
    .line 392
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->cursorPosition:I

    return v0
.end method

.method public getFieldID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->fieldID:Ljava/lang/String;

    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->language:Ljava/lang/String;

    return-object v0
.end method

.method public getMaxAudioTime()I
    .locals 1

    .prologue
    .line 393
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->maxAudioTime:I

    return v0
.end method

.method public getMinVoiceDuration()F
    .locals 1

    .prologue
    .line 398
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->minVoiceDuration:F

    return v0
.end method

.method public getMinVoiceLevel()F
    .locals 1

    .prologue
    .line 400
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->minVoiceLevel:F

    return v0
.end method

.method public getNoSpeechEndPointTimeout()I
    .locals 1

    .prologue
    .line 396
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->nospeechEndpointTimeout:I

    return v0
.end method

.method public getProfanityFilter()Z
    .locals 1

    .prologue
    .line 406
    iget-boolean v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->profanityFilter:Z

    return v0
.end method

.method public getRecoMode()Lcom/vlingo/sdk/recognition/RecognitionMode;
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->recoMode:Lcom/vlingo/sdk/recognition/RecognitionMode;

    return-object v0
.end method

.method public getScreenName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->screenName:Ljava/lang/String;

    return-object v0
.end method

.method public getSilenceThreshold()F
    .locals 1

    .prologue
    .line 397
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->silenceThreshold:F

    return v0
.end method

.method public getSpeechEndpointTimeout()I
    .locals 1

    .prologue
    .line 395
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->speechEndpointTimeout:I

    return v0
.end method

.method public getSpeexComplexity()I
    .locals 1

    .prologue
    .line 404
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->speexComplexity:I

    return v0
.end method

.method public getSpeexQuality()I
    .locals 1

    .prologue
    .line 401
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->speexQuality:I

    return v0
.end method

.method public getSpeexVariableBitrate()I
    .locals 1

    .prologue
    .line 402
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->speexVariableBitrate:I

    return v0
.end method

.method public getSpeexVoiceActivityDetection()I
    .locals 1

    .prologue
    .line 403
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->speexVoiceActivityDetection:I

    return v0
.end method

.method public getVoicePortion()F
    .locals 1

    .prologue
    .line 399
    iget v0, p0, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->voicePortion:F

    return v0
.end method
