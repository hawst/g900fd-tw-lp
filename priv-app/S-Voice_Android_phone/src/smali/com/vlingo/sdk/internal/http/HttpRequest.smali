.class public Lcom/vlingo/sdk/internal/http/HttpRequest;
.super Ljava/lang/Object;
.source "HttpRequest.java"


# static fields
.field protected static final DEFAULT_RETRY_COUNT:I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected callback:Lcom/vlingo/sdk/internal/http/HttpCallback;

.field protected volatile clientHttp:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

.field private volatile cookie:Ljava/lang/Object;

.field protected volatile cookies:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private volatile countRetries:I

.field protected data:[B

.field protected volatile extraHeaders:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private volatile flagForRetry:Z

.field protected volatile gzipPostData:Z

.field private volatile mCanceled:Z

.field protected volatile maxRetry:I

.field protected volatile method:Ljava/lang/String;

.field protected volatile softwareHttp:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

.field private volatile startTime:J

.field protected taskName:Ljava/lang/String;

.field protected volatile timeout:I

.field protected url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->TAG:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Ljava/lang/String;[B)V
    .locals 3
    .param p1, "taskName"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/vlingo/sdk/internal/http/HttpCallback;
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "data"    # [B

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-boolean v1, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->flagForRetry:Z

    .line 67
    iput v1, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->countRetries:I

    .line 69
    iput-boolean v1, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->mCanceled:Z

    .line 83
    const-string/jumbo v0, "POST"

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->method:Ljava/lang/String;

    .line 84
    iput-object v2, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->cookies:Ljava/util/Hashtable;

    .line 85
    iput-object v2, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->extraHeaders:Ljava/util/Hashtable;

    .line 86
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->clientHttp:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    .line 87
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->softwareHttp:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    .line 88
    iput v1, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->timeout:I

    .line 89
    iput v1, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->maxRetry:I

    .line 90
    iput-boolean v1, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->gzipPostData:Z

    .line 92
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->taskName:Ljava/lang/String;

    .line 93
    iput-object p2, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->callback:Lcom/vlingo/sdk/internal/http/HttpCallback;

    .line 94
    iput-object p3, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->url:Ljava/lang/String;

    .line 95
    iput-object p4, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->data:[B

    .line 96
    return-void
.end method

.method public static declared-synchronized createRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Ljava/lang/String;)Lcom/vlingo/sdk/internal/http/HttpRequest;
    .locals 2
    .param p0, "taskName"    # Ljava/lang/String;
    .param p1, "callback"    # Lcom/vlingo/sdk/internal/http/HttpCallback;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 73
    const-class v0, Lcom/vlingo/sdk/internal/http/HttpRequest;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0, p1, p2, v1}, Lcom/vlingo/sdk/internal/http/HttpRequest;->createRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Ljava/lang/String;[B)Lcom/vlingo/sdk/internal/http/HttpRequest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized createRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Ljava/lang/String;[B)Lcom/vlingo/sdk/internal/http/HttpRequest;
    .locals 3
    .param p0, "taskName"    # Ljava/lang/String;
    .param p1, "callback"    # Lcom/vlingo/sdk/internal/http/HttpCallback;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "data"    # [B

    .prologue
    .line 78
    const-class v2, Lcom/vlingo/sdk/internal/http/HttpRequest;

    monitor-enter v2

    :try_start_0
    new-instance v0, Lcom/vlingo/sdk/internal/http/HttpRequest;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/vlingo/sdk/internal/http/HttpRequest;-><init>(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Ljava/lang/String;[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    .local v0, "request":Lcom/vlingo/sdk/internal/http/HttpRequest;
    monitor-exit v2

    return-object v0

    .line 78
    .end local v0    # "request":Lcom/vlingo/sdk/internal/http/HttpRequest;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->mCanceled:Z

    .line 104
    invoke-static {}, Lcom/vlingo/sdk/internal/http/HttpManager;->getInstance()Lcom/vlingo/sdk/internal/http/HttpManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vlingo/sdk/internal/http/HttpManager;->cancelRequest(Lcom/vlingo/sdk/internal/http/HttpRequest;)V

    .line 105
    return-void
.end method

.method protected fetchResponse()Lcom/vlingo/sdk/internal/http/HttpResponse;
    .locals 22

    .prologue
    .line 135
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->flagForRetry:Z

    .line 136
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->startTime:J

    .line 138
    const/4 v14, 0x0

    .line 139
    .local v14, "hc":Lcom/vlingo/sdk/internal/net/HttpConnection;
    const/4 v15, 0x0

    .line 140
    .local v15, "inputstream":Ljava/io/InputStream;
    const/4 v12, 0x0

    .line 141
    .local v12, "dos":Ljava/io/DataOutputStream;
    const/16 v18, 0x0

    .line 142
    .local v18, "respData":[B
    const/16 v20, 0x0

    .line 143
    .local v20, "responseCookies":Lcom/vlingo/sdk/internal/http/cookies/CookieJar;
    const/16 v19, -0x1

    .line 145
    .local v19, "responseCode":I
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->mCanceled:Z

    if-eqz v3, :cond_3

    .line 146
    new-instance v3, Ljava/io/IOException;

    const-string/jumbo v4, "Cancelled"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    :catch_0
    move-exception v13

    .line 209
    .local v13, "ex":Ljava/lang/Exception;
    :try_start_1
    sget-object v3, Lcom/vlingo/sdk/internal/http/HttpRequest;->TAG:Ljava/lang/String;

    invoke-static {v13}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 211
    if-eqz v15, :cond_0

    .line 213
    :try_start_2
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_5

    .line 218
    :cond_0
    :goto_0
    if-eqz v12, :cond_1

    .line 220
    :try_start_3
    invoke-virtual {v12}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_6

    .line 225
    :cond_1
    :goto_1
    if-eqz v14, :cond_2

    .line 227
    :try_start_4
    invoke-interface {v14}, Lcom/vlingo/sdk/internal/net/HttpConnection;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_7

    .line 242
    .end local v13    # "ex":Ljava/lang/Exception;
    :cond_2
    :goto_2
    new-instance v3, Lcom/vlingo/sdk/internal/http/HttpResponse;

    move/from16 v0, v19

    move-object/from16 v1, v18

    move-object/from16 v2, v20

    invoke-direct {v3, v0, v1, v2}, Lcom/vlingo/sdk/internal/http/HttpResponse;-><init>(I[BLcom/vlingo/sdk/internal/http/cookies/CookieJar;)V

    return-object v3

    .line 147
    :cond_3
    :try_start_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->method:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->url:Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->cookies:Ljava/util/Hashtable;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->extraHeaders:Ljava/util/Hashtable;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->clientHttp:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->softwareHttp:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    invoke-static/range {v3 .. v9}, Lcom/vlingo/sdk/internal/http/HttpUtil;->newHttpConnection(Ljava/lang/String;Ljava/lang/String;ZLjava/util/Hashtable;Ljava/util/Hashtable;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;)Lcom/vlingo/sdk/internal/net/HttpConnection;

    move-result-object v14

    .line 150
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->mCanceled:Z

    if-eqz v3, :cond_7

    .line 151
    new-instance v3, Ljava/io/IOException;

    const-string/jumbo v4, "Cancelled"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 211
    :catchall_0
    move-exception v3

    if-eqz v15, :cond_4

    .line 213
    :try_start_6
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2

    .line 218
    :cond_4
    :goto_3
    if-eqz v12, :cond_5

    .line 220
    :try_start_7
    invoke-virtual {v12}, Ljava/io/DataOutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3

    .line 225
    :cond_5
    :goto_4
    if-eqz v14, :cond_6

    .line 227
    :try_start_8
    invoke-interface {v14}, Lcom/vlingo/sdk/internal/net/HttpConnection;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_4

    .line 211
    :cond_6
    :goto_5
    throw v3

    .line 152
    :cond_7
    :try_start_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->method:Ljava/lang/String;

    const-string/jumbo v4, "POST"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->data:[B

    if-eqz v3, :cond_a

    .line 156
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->gzipPostData:Z

    if-eqz v3, :cond_8

    .line 159
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->data:[B

    invoke-static {v3}, Lcom/vlingo/sdk/internal/util/CompressUtils;->deflate([B)[B

    move-result-object v11

    .line 161
    .local v11, "compressedData":[B
    if-eqz v11, :cond_8

    .line 165
    const-string/jumbo v3, "Content-Encoding"

    const-string/jumbo v4, "deflate"

    invoke-interface {v14, v3, v4}, Lcom/vlingo/sdk/internal/net/HttpConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->data:[B

    .line 171
    .end local v11    # "compressedData":[B
    :cond_8
    const-string/jumbo v3, "Content-Length"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->data:[B

    array-length v4, v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v14, v3, v4}, Lcom/vlingo/sdk/internal/net/HttpConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const/4 v3, 0x0

    invoke-interface {v14, v3}, Lcom/vlingo/sdk/internal/net/HttpConnection;->startRequest(Lcom/vlingo/sdk/internal/http/custom/HttpRequest;)V

    .line 173
    invoke-interface {v14}, Lcom/vlingo/sdk/internal/net/HttpConnection;->openDataOutputStream()Ljava/io/DataOutputStream;

    move-result-object v12

    .line 174
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->mCanceled:Z

    if-eqz v3, :cond_9

    .line 175
    new-instance v3, Ljava/io/IOException;

    const-string/jumbo v4, "Cancelled"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 176
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->data:[B

    invoke-virtual {v12, v3}, Ljava/io/DataOutputStream;->write([B)V

    .line 179
    :cond_a
    invoke-interface {v14}, Lcom/vlingo/sdk/internal/net/HttpConnection;->getResponseCode()I

    move-result v19

    .line 180
    invoke-interface {v14}, Lcom/vlingo/sdk/internal/net/HttpConnection;->getLength()I

    move-result v16

    .line 181
    .local v16, "length":I
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->mCanceled:Z

    if-eqz v3, :cond_b

    .line 182
    new-instance v3, Ljava/io/IOException;

    const-string/jumbo v4, "Cancelled"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 187
    :cond_b
    const/4 v3, 0x0

    invoke-interface {v14, v3}, Lcom/vlingo/sdk/internal/net/HttpConnection;->startResponse(Lcom/vlingo/sdk/internal/http/custom/HttpResponse;)V

    .line 188
    invoke-interface {v14}, Lcom/vlingo/sdk/internal/net/HttpConnection;->openInputStream()Ljava/io/InputStream;

    move-result-object v15

    .line 189
    if-nez v15, :cond_c

    .line 190
    new-instance v3, Ljava/io/IOException;

    const-string/jumbo v4, "InputStream does not exist (check response code for error)"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 191
    :cond_c
    const/4 v3, -0x1

    move/from16 v0, v16

    if-eq v0, v3, :cond_10

    .line 192
    move/from16 v0, v16

    new-array v0, v0, [B

    move-object/from16 v18, v0

    .line 193
    const/16 v17, 0x0

    .line 195
    .local v17, "offset":I
    :cond_d
    sub-int v3, v16, v17

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v15, v0, v1, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v10

    .line 196
    .local v10, "bytesRead":I
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->mCanceled:Z

    if-eqz v3, :cond_e

    .line 197
    new-instance v3, Ljava/io/IOException;

    const-string/jumbo v4, "Cancelled"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 198
    :cond_e
    const/4 v3, -0x1

    if-ne v10, v3, :cond_f

    .line 205
    .end local v10    # "bytesRead":I
    .end local v17    # "offset":I
    :goto_6
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/vlingo/sdk/internal/http/HttpRequest;->mCanceled:Z

    if-eqz v3, :cond_11

    .line 206
    new-instance v3, Ljava/io/IOException;

    const-string/jumbo v4, "Cancelled"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 200
    .restart local v10    # "bytesRead":I
    .restart local v17    # "offset":I
    :cond_f
    add-int v17, v17, v10

    .line 201
    move/from16 v0, v16

    move/from16 v1, v17

    if-gt v0, v1, :cond_d

    goto :goto_6

    .line 203
    .end local v10    # "bytesRead":I
    .end local v17    # "offset":I
    :cond_10
    invoke-static {v15}, Lcom/vlingo/sdk/internal/http/HttpUtil;->readData(Ljava/io/InputStream;)[B

    move-result-object v18

    goto :goto_6

    .line 207
    :cond_11
    invoke-static {v14}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->extractCookies(Lcom/vlingo/sdk/internal/net/HttpConnection;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result-object v20

    .line 211
    if-eqz v15, :cond_12

    .line 213
    :try_start_a
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_8

    .line 218
    :cond_12
    :goto_7
    if-eqz v12, :cond_13

    .line 220
    :try_start_b
    invoke-virtual {v12}, Ljava/io/DataOutputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_9

    .line 225
    :cond_13
    :goto_8
    if-eqz v14, :cond_2

    .line 227
    :try_start_c
    invoke-interface {v14}, Lcom/vlingo/sdk/internal/net/HttpConnection;->close()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_1

    goto/16 :goto_2

    .line 228
    :catch_1
    move-exception v21

    .line 229
    .local v21, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/vlingo/sdk/internal/http/HttpRequest;->TAG:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    .end local v16    # "length":I
    :goto_9
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 214
    .end local v21    # "t":Ljava/lang/Throwable;
    :catch_2
    move-exception v21

    .line 215
    .restart local v21    # "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/vlingo/sdk/internal/http/HttpRequest;->TAG:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 221
    .end local v21    # "t":Ljava/lang/Throwable;
    :catch_3
    move-exception v21

    .line 222
    .restart local v21    # "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/vlingo/sdk/internal/http/HttpRequest;->TAG:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 228
    .end local v21    # "t":Ljava/lang/Throwable;
    :catch_4
    move-exception v21

    .line 229
    .restart local v21    # "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/vlingo/sdk/internal/http/HttpRequest;->TAG:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 214
    .end local v21    # "t":Ljava/lang/Throwable;
    .restart local v13    # "ex":Ljava/lang/Exception;
    :catch_5
    move-exception v21

    .line 215
    .restart local v21    # "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/vlingo/sdk/internal/http/HttpRequest;->TAG:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 221
    .end local v21    # "t":Ljava/lang/Throwable;
    :catch_6
    move-exception v21

    .line 222
    .restart local v21    # "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/vlingo/sdk/internal/http/HttpRequest;->TAG:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 228
    .end local v21    # "t":Ljava/lang/Throwable;
    :catch_7
    move-exception v21

    .line 229
    .restart local v21    # "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/vlingo/sdk/internal/http/HttpRequest;->TAG:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    goto :goto_9

    .line 214
    .end local v13    # "ex":Ljava/lang/Exception;
    .end local v21    # "t":Ljava/lang/Throwable;
    .restart local v16    # "length":I
    :catch_8
    move-exception v21

    .line 215
    .restart local v21    # "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/vlingo/sdk/internal/http/HttpRequest;->TAG:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 221
    .end local v21    # "t":Ljava/lang/Throwable;
    :catch_9
    move-exception v21

    .line 222
    .restart local v21    # "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/vlingo/sdk/internal/http/HttpRequest;->TAG:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8
.end method

.method getCallback()Lcom/vlingo/sdk/internal/http/HttpCallback;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->callback:Lcom/vlingo/sdk/internal/http/HttpCallback;

    return-object v0
.end method

.method public getCookie()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->cookie:Ljava/lang/Object;

    return-object v0
.end method

.method public getElapsedTime()J
    .locals 4

    .prologue
    .line 277
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->startTime:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public getMaxRetry()I
    .locals 1

    .prologue
    .line 294
    iget v0, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->maxRetry:I

    return v0
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->taskName:Ljava/lang/String;

    return-object v0
.end method

.method getTimeout()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->timeout:I

    return v0
.end method

.method public isGzipPostDataEnabled()Z
    .locals 1

    .prologue
    .line 334
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->gzipPostData:Z

    return v0
.end method

.method isRetry()Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->flagForRetry:Z

    return v0
.end method

.method public markRetry()V
    .locals 2

    .prologue
    .line 281
    iget v0, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->countRetries:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->countRetries:I

    iget v1, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->maxRetry:I

    if-ge v0, v1, :cond_0

    .line 282
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->flagForRetry:Z

    .line 287
    :goto_0
    return-void

    .line 284
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->flagForRetry:Z

    .line 285
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/HttpRequest;->notifyFailure()V

    goto :goto_0
.end method

.method protected notifyCancelled()V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->callback:Lcom/vlingo/sdk/internal/http/HttpCallback;

    invoke-interface {v0, p0}, Lcom/vlingo/sdk/internal/http/HttpCallback;->onCancelled(Lcom/vlingo/sdk/internal/http/HttpRequest;)V

    .line 256
    return-void
.end method

.method protected notifyFailure()V
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->callback:Lcom/vlingo/sdk/internal/http/HttpCallback;

    invoke-interface {v0, p0}, Lcom/vlingo/sdk/internal/http/HttpCallback;->onFailure(Lcom/vlingo/sdk/internal/http/HttpRequest;)V

    .line 262
    return-void
.end method

.method protected notifyResponse(Lcom/vlingo/sdk/internal/http/HttpResponse;)V
    .locals 1
    .param p1, "response"    # Lcom/vlingo/sdk/internal/http/HttpResponse;

    .prologue
    .line 273
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->callback:Lcom/vlingo/sdk/internal/http/HttpCallback;

    invoke-interface {v0, p0, p1}, Lcom/vlingo/sdk/internal/http/HttpCallback;->onResponse(Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpResponse;)V

    .line 274
    return-void
.end method

.method protected notifyTimeout()Z
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->callback:Lcom/vlingo/sdk/internal/http/HttpCallback;

    invoke-interface {v0, p0}, Lcom/vlingo/sdk/internal/http/HttpCallback;->onTimeout(Lcom/vlingo/sdk/internal/http/HttpRequest;)Z

    move-result v0

    return v0
.end method

.method protected notifyWillExecute()V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->callback:Lcom/vlingo/sdk/internal/http/HttpCallback;

    invoke-interface {v0, p0}, Lcom/vlingo/sdk/internal/http/HttpCallback;->onWillExecuteRequest(Lcom/vlingo/sdk/internal/http/HttpRequest;)V

    .line 250
    return-void
.end method

.method public reachedMaxRetryCount()Z
    .locals 2

    .prologue
    .line 290
    iget v0, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->countRetries:I

    iget v1, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->maxRetry:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public schedule()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 112
    invoke-static {}, Lcom/vlingo/sdk/internal/http/HttpManager;->getInstance()Lcom/vlingo/sdk/internal/http/HttpManager;

    move-result-object v0

    invoke-virtual {v0, p0, v1, v1}, Lcom/vlingo/sdk/internal/http/HttpManager;->doBackgroundRequest(Lcom/vlingo/sdk/internal/http/HttpRequest;ZZ)V

    .line 113
    return-void
.end method

.method public schedule(JZZ)V
    .locals 6
    .param p1, "delay"    # J
    .param p3, "highPriority"    # Z
    .param p4, "ordered"    # Z

    .prologue
    .line 116
    invoke-static {}, Lcom/vlingo/sdk/internal/http/HttpManager;->getInstance()Lcom/vlingo/sdk/internal/http/HttpManager;

    move-result-object v0

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/vlingo/sdk/internal/http/HttpManager;->doBackgroundRequestLater(Lcom/vlingo/sdk/internal/http/HttpRequest;JZZ)V

    .line 117
    return-void
.end method

.method public setClientMeta(Lcom/vlingo/sdk/internal/recognizer/ClientMeta;)V
    .locals 0
    .param p1, "client"    # Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    .prologue
    .line 318
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->clientHttp:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    .line 319
    return-void
.end method

.method public setCookie(Ljava/lang/Object;)V
    .locals 0
    .param p1, "cookie"    # Ljava/lang/Object;

    .prologue
    .line 302
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->cookie:Ljava/lang/Object;

    .line 303
    return-void
.end method

.method public setCookies(Ljava/util/Hashtable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 310
    .local p1, "cookies":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->cookies:Ljava/util/Hashtable;

    .line 311
    return-void
.end method

.method public setExtraHeaders(Ljava/util/Hashtable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 314
    .local p1, "extraHeaders":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->extraHeaders:Ljava/util/Hashtable;

    .line 315
    return-void
.end method

.method public setGzipPostData(Z)V
    .locals 0
    .param p1, "gzipPostData"    # Z

    .prologue
    .line 338
    iput-boolean p1, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->gzipPostData:Z

    .line 339
    return-void
.end method

.method public setMaxRetry(I)V
    .locals 0
    .param p1, "maxRetry"    # I

    .prologue
    .line 330
    iput p1, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->maxRetry:I

    .line 331
    return-void
.end method

.method public setMethod(Ljava/lang/String;)V
    .locals 0
    .param p1, "method"    # Ljava/lang/String;

    .prologue
    .line 306
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->method:Ljava/lang/String;

    .line 307
    return-void
.end method

.method public setSoftwareMeta(Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;)V
    .locals 0
    .param p1, "software"    # Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    .prologue
    .line 322
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->softwareHttp:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    .line 323
    return-void
.end method

.method public setTimeout(I)V
    .locals 0
    .param p1, "timeout"    # I

    .prologue
    .line 326
    iput p1, p0, Lcom/vlingo/sdk/internal/http/HttpRequest;->timeout:I

    .line 327
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 108
    invoke-static {}, Lcom/vlingo/sdk/internal/http/HttpManager;->getInstance()Lcom/vlingo/sdk/internal/http/HttpManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vlingo/sdk/internal/http/HttpManager;->doRequestNow(Lcom/vlingo/sdk/internal/http/HttpRequest;)V

    .line 109
    return-void
.end method
