.class public Lcom/vlingo/sdk/internal/recognizer/reader/AMRDataReader;
.super Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;
.source "AMRDataReader.java"


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;)V
    .locals 0
    .param p1, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .param p2, "listener"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;
    .param p3, "DataReadyListener"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;-><init>(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;)V

    .line 18
    return-void
.end method

.method private getAMRData(Ljava/io/InputStream;I)[B
    .locals 4
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "duration"    # I

    .prologue
    .line 27
    const/4 v2, -0x1

    if-ne p2, v2, :cond_1

    .line 28
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/reader/AMRDataReader;->getRawData()[B

    move-result-object v1

    .line 51
    :cond_0
    :goto_0
    return-object v1

    .line 30
    :cond_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v2, 0x1f4

    invoke-direct {v0, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 31
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    const/4 v1, 0x0

    .line 34
    .local v1, "data":[B
    :try_start_0
    invoke-static {p1, p2}, Lcom/vlingo/sdk/internal/audio/AMRUtil;->readInAMRMaxFrames(Ljava/io/InputStream;I)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 36
    if-eqz p1, :cond_2

    .line 38
    :try_start_1
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 41
    :goto_1
    const/4 p1, 0x0

    .line 43
    :cond_2
    if-eqz v0, :cond_0

    .line 45
    :try_start_2
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 48
    :goto_2
    const/4 v0, 0x0

    goto :goto_0

    .line 36
    :catchall_0
    move-exception v2

    if-eqz p1, :cond_3

    .line 38
    :try_start_3
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 41
    :goto_3
    const/4 p1, 0x0

    .line 43
    :cond_3
    if-eqz v0, :cond_4

    .line 45
    :try_start_4
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 48
    :goto_4
    const/4 v0, 0x0

    .line 36
    :cond_4
    throw v2

    .line 39
    :catch_0
    move-exception v3

    goto :goto_3

    .line 46
    :catch_1
    move-exception v3

    goto :goto_4

    .line 39
    :catch_2
    move-exception v2

    goto :goto_1

    .line 46
    :catch_3
    move-exception v2

    goto :goto_2
.end method


# virtual methods
.method protected onDeinit()V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method protected onInit()Z
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    return v0
.end method

.method protected onProcessData()V
    .locals 3

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/reader/AMRDataReader;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/reader/AMRDataReader;->getMaxDuration()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/reader/AMRDataReader;->getAMRData(Ljava/io/InputStream;I)[B

    move-result-object v0

    .line 22
    .local v0, "data":[B
    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/reader/AMRDataReader;->onDataAvailable([B[SI)V

    .line 23
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/reader/AMRDataReader;->stop()V

    .line 24
    return-void
.end method
