.class public abstract Lcom/vlingo/sdk/internal/Strings;
.super Ljava/lang/Object;
.source "Strings.java"


# static fields
.field public static final android_core_ex:Ljava/lang/String; = " ex: "

.field public static final android_core_listening:Ljava/lang/String; = "Listening..."

.field public static final android_core_method:Ljava/lang/String; = " method: "

.field public static final android_core_missing_permissions:Ljava/lang/String; = "Vlingo is missing some required permission(s)"

.field public static final client_core_NETWORK_NOT_AVAILABLE:Ljava/lang/String; = "Network not available"

.field public static final client_core_NETWORK_TIMEOUT:Ljava/lang/String; = "Timeout waiting for request"

.field public static final client_core_NO_SPEECH:Ljava/lang/String; = "No speech detected"

.field public static final client_core_PHONE_IN_USE:Ljava/lang/String; = "Phone in use"

.field public static final client_core_RECOGNITION_ABORTED:Ljava/lang/String; = "Recognition stopped. Please try again."

.field public static final client_core_RECORDED_MAX:Ljava/lang/String; = "I can only listen to 50 seconds of speech at a time. I\'m working on what you said so far."

.field public static final client_core_RECORDER_ERROR:Ljava/lang/String; = "Can\'t record.  Please try again."

.field public static final client_core_TOO_LONG:Ljava/lang/String; = "Your message was too long.  Please try a shorter request."

.field public static final client_core_TOO_SHORT:Ljava/lang/String; = "Audio too short. Please try again."

.field public static final client_core_cant_connect:Ljava/lang/String; = "Unable to connect"

.field public static final client_core_connecting:Ljava/lang/String; = "Connecting..."

.field public static final client_core_initializing:Ljava/lang/String; = "Initializing..."

.field public static final client_core_network_busy:Ljava/lang/String; = "Device network is busy. Please wait a minute and try again."

.field public static final client_core_network_error:Ljava/lang/String; = "Network error"

.field public static final client_core_readerinit_error:Ljava/lang/String; = "Error initializing reader"

.field public static final client_core_recognizer_busy:Ljava/lang/String; = "Recognizer is busy"

.field public static final client_core_server_error:Ljava/lang/String; = "No result. Please try again."

.field public static final client_core_thinking:Ljava/lang/String; = "Thinking..."

.field public static final progress_dialog_getting_ready:Ljava/lang/String; = "Getting ready..."

.field public static final tts_file_error:Ljava/lang/String; = "Error creating/writing to file"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
