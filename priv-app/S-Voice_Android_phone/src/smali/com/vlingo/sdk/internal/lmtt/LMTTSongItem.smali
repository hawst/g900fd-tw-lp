.class public Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;
.super Lcom/vlingo/sdk/internal/lmtt/LMTTItem;
.source "LMTTSongItem.java"


# instance fields
.field public album:Ljava/lang/String;

.field public artist:Ljava/lang/String;

.field public composer:Ljava/lang/String;

.field public folder:Ljava/lang/String;

.field public genre:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public year:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V
    .locals 11
    .param p1, "uid"    # J
    .param p3, "changeType"    # Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    .prologue
    const/4 v1, 0x0

    .line 40
    const/4 v6, -0x1

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v7, v1

    move-wide v8, p1

    move-object v10, p3

    invoke-direct/range {v0 .. v10}, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "artist"    # Ljava/lang/String;
    .param p3, "composer"    # Ljava/lang/String;
    .param p4, "album"    # Ljava/lang/String;
    .param p5, "genre"    # Ljava/lang/String;
    .param p6, "year"    # I
    .param p7, "folder"    # Ljava/lang/String;
    .param p8, "uid"    # J
    .param p10, "changeType"    # Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    .prologue
    .line 22
    sget-object v0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;->TYPE_SONG:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;

    invoke-direct {p0, v0, p8, p9, p10}, Lcom/vlingo/sdk/internal/lmtt/LMTTItem;-><init>(Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V

    .line 24
    if-nez p1, :cond_0

    .line 25
    const-string/jumbo p1, ""

    .line 28
    :cond_0
    iput-object p1, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->title:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->artist:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->composer:Ljava/lang/String;

    .line 31
    iput-object p4, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->album:Ljava/lang/String;

    .line 32
    iput-object p5, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->genre:Ljava/lang/String;

    .line 33
    if-lez p6, :cond_1

    .line 34
    invoke-static {p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->year:Ljava/lang/String;

    .line 36
    :cond_1
    iput-object p7, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->folder:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method public getDelXML(Ljava/lang/StringBuilder;)V
    .locals 2
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 44
    const-string/jumbo v0, "<SD"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    const-string/jumbo v0, " uid=\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->uid:J

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 47
    const-string/jumbo v0, "\"/>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    return-void
.end method

.method public getInsXML(Ljava/lang/StringBuilder;)V
    .locals 3
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    const/16 v2, 0x22

    .line 57
    const-string/jumbo v0, "<SU"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    const-string/jumbo v0, " uid=\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->uid:J

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 60
    const-string/jumbo v0, "\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    const-string/jumbo v0, " ttl=\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->title:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 63
    const-string/jumbo v0, "\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->artist:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 65
    const-string/jumbo v0, " art=\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->artist:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 67
    const-string/jumbo v0, "\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->composer:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 70
    const-string/jumbo v0, " cmp=\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->composer:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 72
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->album:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 75
    const-string/jumbo v0, " alb=\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->album:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 77
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 79
    :cond_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->genre:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 80
    const-string/jumbo v0, " gen=\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->genre:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 82
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 84
    :cond_3
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->year:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 85
    const-string/jumbo v0, " yr=\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->year:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 87
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 89
    :cond_4
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->folder:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 90
    const-string/jumbo v0, " fld=\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    iget-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->folder:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/util/XmlUtils;->xmlEncode(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 92
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 94
    :cond_5
    const-string/jumbo v0, "/>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    return-void
.end method

.method public getUpXML(Ljava/lang/StringBuilder;)V
    .locals 0
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->getInsXML(Ljava/lang/StringBuilder;)V

    .line 53
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "LMTTSongItem | uid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->uid:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " | changeType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->changeType:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " | title: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTSongItem;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
