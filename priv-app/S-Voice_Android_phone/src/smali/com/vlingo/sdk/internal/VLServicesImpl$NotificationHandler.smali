.class Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;
.super Landroid/os/Handler;
.source "VLServicesImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/VLServicesImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NotificationHandler"
.end annotation


# static fields
.field static final HELLO_ERROR:I = 0x2

.field static final HELLO_SUCCESS:I = 0x1

.field static final UAL_ERROR:I = 0x4

.field static final UAL_SUCCESS:I = 0x3


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/VLServicesImpl;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/VLServicesImpl;)V
    .locals 1

    .prologue
    .line 56
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLServicesImpl;

    .line 57
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 58
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 61
    iget v4, p1, Landroid/os/Message;->what:I

    if-eq v4, v9, :cond_0

    iget v4, p1, Landroid/os/Message;->what:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_4

    .line 62
    :cond_0
    iget-object v4, p0, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLServicesImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLServicesImpl;->mUserActivityListener:Lcom/vlingo/sdk/services/VLServicesListener;
    invoke-static {v4}, Lcom/vlingo/sdk/internal/VLServicesImpl;->access$000(Lcom/vlingo/sdk/internal/VLServicesImpl;)Lcom/vlingo/sdk/services/VLServicesListener;

    move-result-object v4

    if-nez v4, :cond_2

    .line 97
    :cond_1
    :goto_0
    return-void

    .line 66
    :cond_2
    iget-object v4, p0, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLServicesImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLServicesImpl;->mUserActivityListener:Lcom/vlingo/sdk/services/VLServicesListener;
    invoke-static {v4}, Lcom/vlingo/sdk/internal/VLServicesImpl;->access$000(Lcom/vlingo/sdk/internal/VLServicesImpl;)Lcom/vlingo/sdk/services/VLServicesListener;

    move-result-object v3

    .line 67
    .local v3, "l":Lcom/vlingo/sdk/services/VLServicesListener;
    iget-object v4, p0, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLServicesImpl;

    # setter for: Lcom/vlingo/sdk/internal/VLServicesImpl;->mUserActivityListener:Lcom/vlingo/sdk/services/VLServicesListener;
    invoke-static {v4, v7}, Lcom/vlingo/sdk/internal/VLServicesImpl;->access$002(Lcom/vlingo/sdk/internal/VLServicesImpl;Lcom/vlingo/sdk/services/VLServicesListener;)Lcom/vlingo/sdk/services/VLServicesListener;

    .line 69
    iget v4, p1, Landroid/os/Message;->what:I

    if-ne v4, v9, :cond_3

    .line 70
    invoke-interface {v3, v7}, Lcom/vlingo/sdk/services/VLServicesListener;->onSuccess(Ljava/util/List;)V

    goto :goto_0

    .line 73
    :cond_3
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, [Ljava/lang/Object;

    check-cast v4, [Ljava/lang/Object;

    aget-object v1, v4, v8

    check-cast v1, Lcom/vlingo/sdk/services/VLServicesErrors;

    .line 74
    .local v1, "errorCode":Lcom/vlingo/sdk/services/VLServicesErrors;
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, [Ljava/lang/Object;

    check-cast v4, [Ljava/lang/Object;

    aget-object v2, v4, v6

    check-cast v2, Ljava/lang/String;

    .line 75
    .local v2, "errorMsg":Ljava/lang/String;
    invoke-interface {v3, v1, v2}, Lcom/vlingo/sdk/services/VLServicesListener;->onError(Lcom/vlingo/sdk/services/VLServicesErrors;Ljava/lang/String;)V

    goto :goto_0

    .line 78
    .end local v1    # "errorCode":Lcom/vlingo/sdk/services/VLServicesErrors;
    .end local v2    # "errorMsg":Ljava/lang/String;
    .end local v3    # "l":Lcom/vlingo/sdk/services/VLServicesListener;
    :cond_4
    iget v4, p1, Landroid/os/Message;->what:I

    if-eq v4, v6, :cond_5

    iget v4, p1, Landroid/os/Message;->what:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 79
    :cond_5
    iget-object v4, p0, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLServicesImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLServicesImpl;->mHelloListener:Lcom/vlingo/sdk/services/VLServicesListener;
    invoke-static {v4}, Lcom/vlingo/sdk/internal/VLServicesImpl;->access$100(Lcom/vlingo/sdk/internal/VLServicesImpl;)Lcom/vlingo/sdk/services/VLServicesListener;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 83
    iget-object v4, p0, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLServicesImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLServicesImpl;->mHelloListener:Lcom/vlingo/sdk/services/VLServicesListener;
    invoke-static {v4}, Lcom/vlingo/sdk/internal/VLServicesImpl;->access$100(Lcom/vlingo/sdk/internal/VLServicesImpl;)Lcom/vlingo/sdk/services/VLServicesListener;

    move-result-object v3

    .line 84
    .restart local v3    # "l":Lcom/vlingo/sdk/services/VLServicesListener;
    iget-object v4, p0, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLServicesImpl;

    # setter for: Lcom/vlingo/sdk/internal/VLServicesImpl;->mHelloListener:Lcom/vlingo/sdk/services/VLServicesListener;
    invoke-static {v4, v7}, Lcom/vlingo/sdk/internal/VLServicesImpl;->access$102(Lcom/vlingo/sdk/internal/VLServicesImpl;Lcom/vlingo/sdk/services/VLServicesListener;)Lcom/vlingo/sdk/services/VLServicesListener;

    .line 86
    iget v4, p1, Landroid/os/Message;->what:I

    if-ne v4, v6, :cond_6

    .line 88
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 89
    .local v0, "actionList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/VLAction;>;"
    invoke-interface {v3, v0}, Lcom/vlingo/sdk/services/VLServicesListener;->onSuccess(Ljava/util/List;)V

    goto :goto_0

    .line 92
    .end local v0    # "actionList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/VLAction;>;"
    :cond_6
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, [Ljava/lang/Object;

    check-cast v4, [Ljava/lang/Object;

    aget-object v1, v4, v8

    check-cast v1, Lcom/vlingo/sdk/services/VLServicesErrors;

    .line 93
    .restart local v1    # "errorCode":Lcom/vlingo/sdk/services/VLServicesErrors;
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, [Ljava/lang/Object;

    check-cast v4, [Ljava/lang/Object;

    aget-object v2, v4, v6

    check-cast v2, Ljava/lang/String;

    .line 94
    .restart local v2    # "errorMsg":Ljava/lang/String;
    invoke-interface {v3, v1, v2}, Lcom/vlingo/sdk/services/VLServicesListener;->onError(Lcom/vlingo/sdk/services/VLServicesErrors;Ljava/lang/String;)V

    goto :goto_0
.end method

.method declared-synchronized notifyError(ILcom/vlingo/sdk/services/VLServicesErrors;Ljava/lang/String;)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "error"    # Lcom/vlingo/sdk/services/VLServicesErrors;
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x2

    .line 109
    monitor-enter p0

    if-ne p1, v0, :cond_1

    .line 110
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLServicesImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLServicesImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/VLServicesImpl;->access$200(Lcom/vlingo/sdk/internal/VLServicesImpl;)Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 112
    :cond_1
    if-ne p1, v1, :cond_0

    .line 113
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLServicesImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLServicesImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/VLServicesImpl;->access$200(Lcom/vlingo/sdk/internal/VLServicesImpl;)Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 109
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized notifySuccess(ILjava/util/List;)V
    .locals 2
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "actionList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/VLAction;>;"
    const/4 v1, 0x1

    .line 100
    monitor-enter p0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 101
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLServicesImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLServicesImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/VLServicesImpl;->access$200(Lcom/vlingo/sdk/internal/VLServicesImpl;)Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p2}, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 103
    :cond_1
    if-ne p1, v1, :cond_0

    .line 104
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLServicesImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLServicesImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/VLServicesImpl;->access$200(Lcom/vlingo/sdk/internal/VLServicesImpl;)Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/VLServicesImpl$NotificationHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
