.class public Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;
.source "VoconInitTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask$Callback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask$Callback;",
        ">;"
    }
.end annotation


# instance fields
.field private grammarVocabularies:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;",
            ">;"
        }
    .end annotation
.end field

.field private voconConfig:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask$Callback;Landroid/os/Handler;)V
    .locals 0
    .param p1, "callback"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask$Callback;
    .param p2, "mainHandler"    # Landroid/os/Handler;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;-><init>(Ljava/lang/Object;Landroid/os/Handler;)V

    .line 23
    return-void
.end method

.method private getVocabularies()Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    new-instance v8, Ljava/io/File;

    const-string/jumbo v10, "/sdcard"

    invoke-direct {v8, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 98
    .local v8, "voconDir":Ljava/io/File;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 99
    .local v7, "vocabularies":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;>;"
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->getSlotNames()[Ljava/lang/String;

    move-result-object v5

    .line 100
    .local v5, "slots":[Ljava/lang/String;
    move-object v0, v5

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 101
    .local v4, "slot":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    move-result-object v9

    .line 102
    .local v9, "voconSlot":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;
    invoke-virtual {v9}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->getContext()Ljava/lang/String;

    move-result-object v1

    .line 103
    .local v1, "filename":Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v8, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 104
    .local v6, "vFile":Ljava/io/File;
    new-instance v10, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;

    const-string/jumbo v11, "tizen"

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v11, v4, v12}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 107
    .end local v1    # "filename":Ljava/lang/String;
    .end local v4    # "slot":Ljava/lang/String;
    .end local v6    # "vFile":Ljava/io/File;
    .end local v9    # "voconSlot":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;
    :cond_0
    return-object v7
.end method


# virtual methods
.method protected execute()Z
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 29
    :try_start_0
    new-instance v4, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$Builder;

    const-string/jumbo v6, "/sdcard/acmod.dat"

    const-string/jumbo v8, "/sdcard/clc.dat"

    const-string/jumbo v9, "/sdcard/tizen.fcf"

    invoke-direct {v4, v6, v8, v9}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$Builder;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .local v4, "voconConfigBuilder":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$Builder;
    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$Builder;->build()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask;->voconConfig:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;

    .line 32
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask;->voconConfig:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;

    invoke-virtual {v6}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->getModelFilename()Ljava/lang/String;

    move-result-object v3

    .line 33
    .local v3, "modelFilePath":Ljava/lang/String;
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask;->voconConfig:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;

    invoke-virtual {v6}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->getClcFilename()Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "clcFilePath":Ljava/lang/String;
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask;->voconConfig:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;

    invoke-virtual {v6}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;->getGrammarFilename()Ljava/lang/String;

    move-result-object v2

    .line 36
    .local v2, "grammarFilePath":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 37
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string/jumbo v8, "Model file path is not specified"

    invoke-direct {v6, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    .end local v0    # "clcFilePath":Ljava/lang/String;
    .end local v2    # "grammarFilePath":Ljava/lang/String;
    .end local v3    # "modelFilePath":Ljava/lang/String;
    .end local v4    # "voconConfigBuilder":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$Builder;
    :catch_0
    move-exception v1

    .line 70
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    move-result-object v6

    invoke-interface {v6}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->isInit()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 71
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    move-result-object v6

    invoke-interface {v6}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->uninit()Z
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    :goto_0
    move v6, v7

    .line 81
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return v6

    .line 39
    .restart local v0    # "clcFilePath":Ljava/lang/String;
    .restart local v2    # "grammarFilePath":Ljava/lang/String;
    .restart local v3    # "modelFilePath":Ljava/lang/String;
    .restart local v4    # "voconConfigBuilder":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$Builder;
    :cond_1
    :try_start_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 40
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string/jumbo v8, "CLC file path is not specified"

    invoke-direct {v6, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 42
    :cond_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 43
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string/jumbo v8, "Grammar file path is not specified"

    invoke-direct {v6, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 45
    :cond_3
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVoconInstance;->getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    move-result-object v5

    .line 46
    .local v5, "voconJNI":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;
    invoke-interface {v5, v3, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->init(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 47
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask;->getVocabularies()Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask;->grammarVocabularies:Ljava/util/List;

    .line 48
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask;->grammarVocabularies:Ljava/util/List;

    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask;->grammarVocabularies:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    new-array v8, v8, [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;

    invoke-interface {v6, v8}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;

    invoke-interface {v5, v2, v6}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->loadGrammar(Ljava/lang/String;[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 61
    :cond_4
    invoke-interface {v5}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->uninit()Z

    :cond_5
    move v6, v7

    .line 67
    goto :goto_1

    .line 51
    :cond_6
    invoke-interface {v5}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->resetTimeoutBeforeSpeech()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 54
    invoke-interface {v5}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;->resetTimeoutAfterSpeech()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result v6

    if-eqz v6, :cond_4

    .line 58
    const/4 v6, 0x1

    goto :goto_1

    .line 75
    .end local v0    # "clcFilePath":Ljava/lang/String;
    .end local v2    # "grammarFilePath":Ljava/lang/String;
    .end local v3    # "modelFilePath":Ljava/lang/String;
    .end local v4    # "voconConfigBuilder":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig$Builder;
    .end local v5    # "voconJNI":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;
    .restart local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v6

    goto :goto_0

    .line 73
    :catch_2
    move-exception v6

    goto :goto_0
.end method

.method protected onExecutionFailed(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask$Callback;)V
    .locals 0
    .param p1, "callback"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask$Callback;

    .prologue
    .line 92
    invoke-interface {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask$Callback;->onVoconInitFailed()V

    .line 93
    return-void
.end method

.method protected bridge synthetic onExecutionFailed(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 14
    check-cast p1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask$Callback;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask;->onExecutionFailed(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask$Callback;)V

    return-void
.end method

.method protected onExecutionSuccess(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask$Callback;)V
    .locals 2
    .param p1, "callback"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask$Callback;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask;->voconConfig:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask;->grammarVocabularies:Ljava/util/List;

    invoke-interface {p1, v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask$Callback;->onVoconInitSuccess(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;Ljava/util/List;)V

    .line 88
    return-void
.end method

.method protected bridge synthetic onExecutionSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 14
    check-cast p1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask$Callback;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask;->onExecutionSuccess(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask$Callback;)V

    return-void
.end method
