.class public Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;
.super Ljava/lang/Object;
.source "HybridArbiter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$1;,
        Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ResultTimer;,
        Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;
    }
.end annotation


# instance fields
.field private arbiterListener:Lcom/vlingo/sdk/internal/recognizer/arbiter/ArbiterListener;

.field private countDownTimer:Landroid/os/CountDownTimer;

.field private networkErrorMessage:Ljava/lang/String;

.field private networkErrorType:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;

.field private state:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

.field private voconResult:Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/recognizer/arbiter/ArbiterListener;I)V
    .locals 6
    .param p1, "arbiterListener"    # Lcom/vlingo/sdk/internal/recognizer/arbiter/ArbiterListener;
    .param p2, "midConfidenceTimeoutMs"    # I

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->NO_RESULT:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->state:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    .line 26
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->arbiterListener:Lcom/vlingo/sdk/internal/recognizer/arbiter/ArbiterListener;

    .line 27
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ResultTimer;

    int-to-long v2, p2

    int-to-long v4, p2

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ResultTimer;-><init>(Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;JJ)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->countDownTimer:Landroid/os/CountDownTimer;

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;)Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->state:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->sendVoconResult()V

    return-void
.end method

.method private sendNetworkResult(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    .locals 1
    .param p1, "result"    # Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->setToFinishedState()V

    .line 140
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->arbiterListener:Lcom/vlingo/sdk/internal/recognizer/arbiter/ArbiterListener;

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/arbiter/ArbiterListener;->onNetworkResult(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    .line 141
    return-void
.end method

.method private sendVoconAndNetworkError()V
    .locals 3

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->setToFinishedState()V

    .line 150
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->arbiterListener:Lcom/vlingo/sdk/internal/recognizer/arbiter/ArbiterListener;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->networkErrorType:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->networkErrorMessage:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/arbiter/ArbiterListener;->onNetworkError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V

    .line 151
    return-void
.end method

.method private sendVoconResult()V
    .locals 2

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->setToFinishedState()V

    .line 145
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->arbiterListener:Lcom/vlingo/sdk/internal/recognizer/arbiter/ArbiterListener;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->voconResult:Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;->getResult()Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/arbiter/ArbiterListener;->onVoconResult(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    .line 146
    return-void
.end method

.method private setNetworkErrorState(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V
    .locals 1
    .param p1, "recError"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 121
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->NETWORK_ERROR:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->state:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    .line 122
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->networkErrorType:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;

    .line 123
    iput-object p2, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->networkErrorMessage:Ljava/lang/String;

    .line 124
    return-void
.end method

.method private setToFinishedState()V
    .locals 1

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->stopWaitingForNetworkResult()V

    .line 155
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->FINISHED:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->state:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    .line 156
    return-void
.end method

.method private stopWaitingForNetworkResult()V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->countDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 160
    return-void
.end method

.method private voconResultHasHighConfidence(Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;)Z
    .locals 4
    .param p1, "result"    # Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;
    .param p2, "thresholdInfo"    # Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;

    .prologue
    .line 163
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;->getConfidenceScore()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;->getHighThreshold()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private voconResultHasMidConfidence(Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;)Z
    .locals 4
    .param p1, "result"    # Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;
    .param p2, "thresholdInfo"    # Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;

    .prologue
    .line 167
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;->getConfidenceScore()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;->getMidThreshold()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private waitForNetworkResult()V
    .locals 1

    .prologue
    .line 134
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->WAITING_FOR_NETWORK_RESULT:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->state:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    .line 135
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->countDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 136
    return-void
.end method


# virtual methods
.method public notifyOfNetworkError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V
    .locals 2
    .param p1, "recError"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 99
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$1;->$SwitchMap$com$vlingo$sdk$internal$recognizer$arbiter$HybridArbiter$ArbiterState:[I

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->state:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 118
    :goto_0
    :pswitch_0
    return-void

    .line 101
    :pswitch_1
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->setNetworkErrorState(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :pswitch_2
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->sendVoconAndNetworkError()V

    goto :goto_0

    .line 109
    :pswitch_3
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->sendVoconResult()V

    goto :goto_0

    .line 113
    :pswitch_4
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->stopWaitingForNetworkResult()V

    .line 114
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->sendVoconResult()V

    goto :goto_0

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public notifyOfNetworkResult(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    .locals 2
    .param p1, "result"    # Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .prologue
    .line 74
    if-nez p1, :cond_0

    .line 75
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;->NO_RESULTS:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;

    const-string/jumbo v1, "No network results"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->notifyOfNetworkError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V

    .line 91
    :goto_0
    return-void

    .line 78
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$1;->$SwitchMap$com$vlingo$sdk$internal$recognizer$arbiter$HybridArbiter$ArbiterState:[I

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->state:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 82
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->sendNetworkResult(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    goto :goto_0

    .line 86
    :pswitch_2
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->stopWaitingForNetworkResult()V

    .line 87
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->sendNetworkResult(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    goto :goto_0

    .line 78
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public notifyOfVoconResult(Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;)V
    .locals 3
    .param p1, "result"    # Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->voconResult:Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;

    .line 36
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$1;->$SwitchMap$com$vlingo$sdk$internal$recognizer$arbiter$HybridArbiter$ArbiterState:[I

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->state:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 67
    :goto_0
    return-void

    .line 38
    :pswitch_0
    if-nez p1, :cond_0

    .line 39
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->VOCON_ERROR:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->state:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    goto :goto_0

    .line 42
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;->getThresholdInfo()Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;

    move-result-object v0

    .line 43
    .local v0, "thresholdInfo":Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;
    if-nez v0, :cond_1

    .line 44
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->VOCON_ERROR:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->state:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    goto :goto_0

    .line 48
    :cond_1
    invoke-direct {p0, p1, v0}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->voconResultHasHighConfidence(Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 49
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->sendVoconResult()V

    goto :goto_0

    .line 50
    :cond_2
    invoke-direct {p0, p1, v0}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->voconResultHasMidConfidence(Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 51
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->waitForNetworkResult()V

    goto :goto_0

    .line 53
    :cond_3
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;->VOCON_BAD_QUALITY_RESULT:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->state:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter$ArbiterState;

    goto :goto_0

    .line 59
    .end local v0    # "thresholdInfo":Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;
    :pswitch_1
    if-nez p1, :cond_4

    .line 60
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->sendVoconAndNetworkError()V

    goto :goto_0

    .line 62
    :cond_4
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->sendVoconResult()V

    goto :goto_0

    .line 36
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public notifyRecognitionCancelled()V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->setToFinishedState()V

    .line 131
    return-void
.end method
