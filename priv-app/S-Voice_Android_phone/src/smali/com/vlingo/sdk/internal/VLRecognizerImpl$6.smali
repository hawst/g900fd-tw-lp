.class Lcom/vlingo/sdk/internal/VLRecognizerImpl$6;
.super Ljava/lang/Object;
.source "VLRecognizerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/sdk/internal/VLRecognizerImpl;->acceptedText(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

.field final synthetic val$collection:Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;

.field final synthetic val$gUttId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/VLRecognizerImpl;Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;)V
    .locals 0

    .prologue
    .line 348
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$6;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    iput-object p2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$6;->val$gUttId:Ljava/lang/String;

    iput-object p3, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$6;->val$collection:Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 351
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$6;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    monitor-enter v1

    .line 352
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$6;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->isValid()Z

    move-result v0

    if-nez v0, :cond_0

    .line 353
    monitor-exit v1

    .line 357
    :goto_0
    return-void

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$6;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecognizer:Lcom/vlingo/sdk/internal/recognizer/Recognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$600(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    move-result-object v0

    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$6;->val$gUttId:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$6;->val$collection:Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;

    invoke-virtual {v0, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->sendAcceptedText(Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;)V

    .line 356
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
