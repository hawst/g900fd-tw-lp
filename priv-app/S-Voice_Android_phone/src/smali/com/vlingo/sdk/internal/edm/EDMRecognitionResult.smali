.class public Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;
.super Ljava/lang/Object;
.source "EDMRecognitionResult.java"

# interfaces
.implements Lcom/vlingo/sdk/recognition/VLRecognitionResult;


# instance fields
.field private actions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            ">;"
        }
    .end annotation
.end field

.field private dialogGuid:Ljava/lang/String;

.field private dialogState:[B

.field private dialogTurn:I

.field private fieldId:Ljava/lang/String;

.field private guttId:Ljava/lang/String;

.field private language:Ljava/lang/String;

.field private nBestData:Lcom/vlingo/sdk/recognition/NBestData;

.field private parseGroup:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

.field private rawResponseString:Ljava/lang/String;

.field private resultString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/nuance/embeddeddialogmanager/EDMResults;)V
    .locals 1
    .param p1, "results"    # Lcom/nuance/embeddeddialogmanager/EDMResults;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->actions:Ljava/util/List;

    .line 47
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->init(Lcom/nuance/embeddeddialogmanager/EDMResults;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/sdk/recognition/VLRecognitionResult;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 1
    .param p1, "originalResult"    # Lcom/vlingo/sdk/recognition/VLRecognitionResult;
    .param p2, "fieldId"    # Ljava/lang/String;
    .param p3, "language"    # Ljava/lang/String;
    .param p4, "dialogState"    # [B

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getGUttId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->guttId:Ljava/lang/String;

    .line 33
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getDialogGUID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->dialogGuid:Ljava/lang/String;

    .line 34
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getDialogTurn()I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->dialogTurn:I

    .line 35
    iput-object p4, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->dialogState:[B

    .line 36
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getResultString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->resultString:Ljava/lang/String;

    .line 37
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getNBestData()Lcom/vlingo/sdk/recognition/NBestData;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->nBestData:Lcom/vlingo/sdk/recognition/NBestData;

    .line 38
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getActions()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->actions:Ljava/util/List;

    .line 39
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getParseGroup()Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->parseGroup:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

    .line 40
    iput-object p2, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->fieldId:Ljava/lang/String;

    .line 41
    iput-object p3, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->language:Ljava/lang/String;

    .line 42
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getRawResponseString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->rawResponseString:Ljava/lang/String;

    .line 43
    return-void
.end method

.method private addActions(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/model/VLActionResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 81
    .local p1, "vlActionResults":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/model/VLActionResult;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/voicepad/model/VLActionResult;

    .line 82
    .local v2, "result":Lcom/vlingo/voicepad/model/VLActionResult;
    new-instance v0, Lcom/vlingo/sdk/internal/edm/EDMAction;

    invoke-direct {v0, v2}, Lcom/vlingo/sdk/internal/edm/EDMAction;-><init>(Lcom/vlingo/voicepad/model/VLActionResult;)V

    .line 83
    .local v0, "action":Lcom/vlingo/sdk/recognition/VLAction;
    if-eqz v0, :cond_0

    .line 84
    iget-object v3, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->actions:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 87
    .end local v0    # "action":Lcom/vlingo/sdk/recognition/VLAction;
    .end local v2    # "result":Lcom/vlingo/voicepad/model/VLActionResult;
    :cond_1
    return-void
.end method


# virtual methods
.method public getActions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->actions:Ljava/util/List;

    return-object v0
.end method

.method public getDialogGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->dialogGuid:Ljava/lang/String;

    return-object v0
.end method

.method public getDialogState()[B
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->dialogState:[B

    return-object v0
.end method

.method public getDialogTurn()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->dialogTurn:I

    return v0
.end method

.method public getFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->fieldId:Ljava/lang/String;

    return-object v0
.end method

.method public getGUttId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->guttId:Ljava/lang/String;

    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->language:Ljava/lang/String;

    return-object v0
.end method

.method public getNBestData()Lcom/vlingo/sdk/recognition/NBestData;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->nBestData:Lcom/vlingo/sdk/recognition/NBestData;

    return-object v0
.end method

.method public getParseGroup()Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->parseGroup:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

    return-object v0
.end method

.method public getRawResponseString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->rawResponseString:Ljava/lang/String;

    return-object v0
.end method

.method public getResultString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->resultString:Ljava/lang/String;

    return-object v0
.end method

.method public init(Lcom/nuance/embeddeddialogmanager/EDMResults;)V
    .locals 1
    .param p1, "results"    # Lcom/nuance/embeddeddialogmanager/EDMResults;

    .prologue
    .line 51
    if-eqz p1, :cond_0

    .line 52
    invoke-virtual {p1}, Lcom/nuance/embeddeddialogmanager/EDMResults;->getActionResults()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->addActions(Ljava/util/List;)V

    .line 53
    invoke-virtual {p1}, Lcom/nuance/embeddeddialogmanager/EDMResults;->getFieldId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->fieldId:Ljava/lang/String;

    .line 54
    invoke-virtual {p1}, Lcom/nuance/embeddeddialogmanager/EDMResults;->getState()[B

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->dialogState:[B

    .line 57
    :cond_0
    return-void
.end method

.method public isFromEDM()Z
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const/16 v7, 0xa

    .line 61
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 62
    .local v4, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v5, "guttId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->guttId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 63
    const-string/jumbo v5, "dialogGuid: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->dialogGuid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 64
    const-string/jumbo v5, "dialogTurn: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->dialogTurn:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 65
    const-string/jumbo v5, "resultString: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->resultString:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 66
    iget-object v5, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->actions:Ljava/util/List;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->actions:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 67
    const-string/jumbo v5, "Actions: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 68
    iget-object v5, p0, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->actions:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/recognition/VLAction;

    .line 69
    .local v0, "action":Lcom/vlingo/sdk/recognition/VLAction;
    const-string/jumbo v5, "Action Name: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 70
    const-string/jumbo v5, "ActionParameters: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 71
    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLAction;->getParameterNames()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 72
    .local v3, "paramKey":Ljava/lang/String;
    const-string/jumbo v5, "ParamName: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ", ParamValue: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0, v3}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 77
    .end local v0    # "action":Lcom/vlingo/sdk/recognition/VLAction;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "paramKey":Ljava/lang/String;
    :cond_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method
