.class Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$1;
.super Ljava/lang/Object;
.source "VLTextToSpeechImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->synthesizeToFile(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;Ljava/lang/String;Lcom/vlingo/sdk/tts/VLTextToSpeechListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;

.field final synthetic val$language:Ljava/lang/String;

.field final synthetic val$ttsRequest:Lcom/vlingo/sdk/internal/audio/TTSRequest;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;Lcom/vlingo/sdk/internal/audio/TTSRequest;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;

    iput-object p2, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$1;->val$ttsRequest:Lcom/vlingo/sdk/internal/audio/TTSRequest;

    iput-object p3, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$1;->val$language:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 135
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$1;->val$ttsRequest:Lcom/vlingo/sdk/internal/audio/TTSRequest;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$1;->val$language:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;

    invoke-static {v1, v2, v3}, Lcom/vlingo/sdk/internal/audio/TTSServerManager;->createTTSRequest(Lcom/vlingo/sdk/internal/audio/TTSRequest;Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    move-result-object v1

    # setter for: Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mCurrentRequest:Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->access$102(Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    .line 136
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mCurrentRequest:Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->access$100(Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2, v4, v4}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->schedule(JZZ)V

    .line 137
    return-void
.end method
