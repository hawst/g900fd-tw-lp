.class Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;
.super Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
.source "ASRRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReceivedState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V
    .locals 1

    .prologue
    .line 1123
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p2, "x1"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;

    .prologue
    .line 1123
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V

    return-void
.end method


# virtual methods
.method public cancel(Z)V
    .locals 1
    .param p1, "timedOut"    # Z

    .prologue
    .line 1138
    const-string/jumbo v0, "cancel(): ignored"

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;->log(Ljava/lang/String;)V

    .line 1139
    return-void
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 1133
    const-string/jumbo v0, "finish(): ignored"

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;->log(Ljava/lang/String;)V

    .line 1134
    return-void
.end method

.method public isResponseReceived()Z
    .locals 1

    .prologue
    .line 1124
    const/4 v0, 0x1

    return v0
.end method

.method public onRun()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1149
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->stop()V
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$4300(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V

    .line 1151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "[LatencyCheck] ReceivedState response = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getResponse()Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$4400(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;->log(Ljava/lang/String;)V

    .line 1153
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->setTimedOut(Z)V

    .line 1155
    const-string/jumbo v0, "closing connection"

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;->log(Ljava/lang/String;)V

    .line 1156
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getOutputStream()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3500(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->close()V

    .line 1157
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 1159
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getResponse()Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$4400(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    move-result-object v1

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->notifyListeners(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$4500(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    .line 1161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "done at "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getTimingString()Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$4600(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;->log(Ljava/lang/String;)V

    .line 1162
    return-void
.end method

.method public sendAudio([BII)V
    .locals 1
    .param p1, "audio"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 1128
    const-string/jumbo v0, "sendAudio(): ignored"

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;->log(Ljava/lang/String;)V

    .line 1129
    return-void
.end method

.method public setState(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V
    .locals 2
    .param p1, "state"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .prologue
    .line 1143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "[LatencyCheck] setState("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "): ignored"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;->log(Ljava/lang/String;)V

    .line 1144
    return-void
.end method
