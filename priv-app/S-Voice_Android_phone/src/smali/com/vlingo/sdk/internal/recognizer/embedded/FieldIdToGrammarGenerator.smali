.class public final Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;
.super Ljava/lang/Object;
.source "FieldIdToGrammarGenerator.java"


# static fields
.field private static final SETTINGS_FOLDER:Ljava/lang/String; = "field_id_to_grammar_rules"

.field private static instance:Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;


# instance fields
.field private volatile rules:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;-><init>()V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;->instance:Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;->rules:Ljava/util/concurrent/ConcurrentMap;

    .line 35
    return-void
.end method

.method public static getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;
    .locals 2

    .prologue
    .line 39
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 40
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Should be run on worker thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;->instance:Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;

    return-object v0
.end method

.method private loadRuleSets()V
    .locals 12

    .prologue
    .line 52
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v10

    invoke-virtual {v10}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    .line 53
    .local v2, "assetManager":Landroid/content/res/AssetManager;
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 55
    .local v8, "ruleMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;>;"
    :try_start_0
    const-string/jumbo v10, "field_id_to_grammar_rules"

    invoke-virtual {v2, v10}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .local v1, "arr$":[Ljava/lang/String;
    array-length v7, v1

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v7, :cond_0

    aget-object v4, v1, v5

    .line 56
    .local v4, "fileName":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "field_id_to_grammar_rules/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x2

    invoke-virtual {v2, v10, v11}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;

    move-result-object v6

    .line 58
    .local v6, "inputStream":Ljava/io/InputStream;
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser;->getFieldIdToGrammarBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;

    move-result-object v10

    invoke-static {v6, v10}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser;->parse(Ljava/io/InputStream;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;)Ljava/lang/Object;

    move-result-object v10

    move-object v0, v10

    check-cast v0, Ljava/util/Map;

    move-object v8, v0

    .line 55
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 61
    .end local v4    # "fileName":Ljava/lang/String;
    .end local v6    # "inputStream":Ljava/io/InputStream;
    :cond_0
    iget-object v11, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;->rules:Ljava/util/concurrent/ConcurrentMap;

    monitor-enter v11
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1

    .line 62
    :try_start_1
    iget-object v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;->rules:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v10}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    .line 63
    iget-object v10, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;->rules:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v10, v8}, Ljava/util/concurrent/ConcurrentMap;->putAll(Ljava/util/Map;)V

    .line 64
    monitor-exit v11

    .line 76
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v7    # "len$":I
    :goto_1
    return-void

    .line 64
    .restart local v1    # "arr$":[Ljava/lang/String;
    .restart local v5    # "i$":I
    .restart local v7    # "len$":I
    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v10
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_1

    .line 65
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v7    # "len$":I
    :catch_0
    move-exception v3

    .line 68
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 69
    .end local v3    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v9

    .line 72
    .local v9, "xppe":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v9}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public getGrammarRuleSet(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;
    .locals 2
    .param p1, "fieldId"    # Ljava/lang/String;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;->rules:Ljava/util/concurrent/ConcurrentMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;->rules:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 92
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "No field id to grammar rules loaded."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 98
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Field Id shouldn\'t be empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :cond_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;->rules:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/FieldIdToGrammarRuleSet;

    return-object v0
.end method

.method public initialize()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/FieldIdToGrammarGenerator;->loadRuleSets()V

    .line 47
    return-void
.end method
