.class public Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;
.super Ljava/lang/Object;
.source "ParameterTag.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag$1;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag$ParameterTagBuilder;
    }
.end annotation


# static fields
.field private static final HIGH_CONFIDENCE_PARAM_NAME:Ljava/lang/String; = "HighConfidence"

.field private static final LANGUAGE_PARAM_NAME:Ljava/lang/String; = "language"

.field private static final MID_CONFIDENCE_PARAM_NAME:Ljava/lang/String; = "MidConfidence"

.field public static final PARAM_TAG_NAME:Ljava/lang/String; = "Param"

.field private static final VALUE_PARAM_NAME:Ljava/lang/String; = "value"


# instance fields
.field private language:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    return-void
.end method

.method static synthetic access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 8
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->language:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 8
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->name:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 8
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->value:Ljava/lang/String;

    return-object p1
.end method

.method public static getBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag$ParameterTagBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag$ParameterTagBuilder;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag$1;)V

    return-object v0
.end method


# virtual methods
.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->language:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->value:Ljava/lang/String;

    return-object v0
.end method

.method public isHighConfidenceParam()Z
    .locals 2

    .prologue
    .line 32
    const-string/jumbo v0, "HighConfidence"

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isMidConfidenceParam()Z
    .locals 2

    .prologue
    .line 36
    const-string/jumbo v0, "MidConfidence"

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
