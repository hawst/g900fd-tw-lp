.class Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$TtaAbstractRulesListBuilder;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
.source "AbstractRule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TtaAbstractRulesListBuilder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;",
        ">;>;"
    }
.end annotation


# instance fields
.field private arbitrationInfoMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;",
            ">;"
        }
    .end annotation
.end field

.field private rulesList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;-><init>()V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$TtaAbstractRulesListBuilder;->rulesList:Ljava/util/List;

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$TtaAbstractRulesListBuilder;->arbitrationInfoMap:Ljava/util/Map;

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$1;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$TtaAbstractRulesListBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public addChildTagObject(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 7
    .param p1, "childName"    # Ljava/lang/String;
    .param p2, "tagObject"    # Ljava/lang/Object;

    .prologue
    .line 86
    const-string/jumbo v5, "ResultBlock"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    instance-of v5, p2, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;

    if-nez v5, :cond_1

    :cond_0
    const-string/jumbo v5, "Reference"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    instance-of v5, p2, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference;

    if-eqz v5, :cond_3

    .line 88
    :cond_1
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$TtaAbstractRulesListBuilder;->rulesList:Ljava/util/List;

    check-cast p2, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;

    .end local p2    # "tagObject":Ljava/lang/Object;
    invoke-interface {v5, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    :cond_2
    :goto_0
    return-void

    .line 89
    .restart local p2    # "tagObject":Ljava/lang/Object;
    :cond_3
    const-string/jumbo v5, "Threshold"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 91
    :try_start_0
    move-object v0, p2

    check-cast v0, Ljava/util/List;

    move-object v4, v0

    .line 92
    .local v4, "parameterTags":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;

    .line 93
    .local v3, "parameter":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$TtaAbstractRulesListBuilder;->arbitrationInfoMap:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;

    .line 94
    .local v2, "info":Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;
    if-nez v2, :cond_4

    .line 95
    new-instance v2, Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;

    .end local v2    # "info":Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;
    invoke-direct {v2}, Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;-><init>()V

    .line 97
    .restart local v2    # "info":Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;
    :cond_4
    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->isHighConfidenceParam()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 98
    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;->setHighThreshold(I)V

    .line 102
    :cond_5
    :goto_2
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$TtaAbstractRulesListBuilder;->arbitrationInfoMap:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 104
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "info":Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;
    .end local v3    # "parameter":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;
    .end local v4    # "parameterTags":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;>;"
    :catch_0
    move-exception v5

    goto :goto_0

    .line 99
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v2    # "info":Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;
    .restart local v3    # "parameter":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;
    .restart local v4    # "parameterTags":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;>;"
    :cond_6
    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->isMidConfidenceParam()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 100
    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;->setMidThreshold(I)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2
.end method

.method protected addChildrenBuilders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 116
    .local p1, "builders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder<*>;>;"
    const-string/jumbo v0, "Threshold"

    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;->getThresholdBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    const-string/jumbo v0, "ResultBlock"

    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;->getBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    const-string/jumbo v0, "Reference"

    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference;->getBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    return-void
.end method

.method public addParametersNames(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 80
    .local p1, "paramNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v0, "fieldid"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    return-void
.end method

.method protected bridge synthetic buildResultObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$TtaAbstractRulesListBuilder;->buildResultObject()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected buildResultObject()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    new-instance v2, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$TtaAbstractRulesListBuilder;->rulesList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 61
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;>;"
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$TtaAbstractRulesListBuilder;->parametersMap:Ljava/util/Map;

    const-string/jumbo v6, "fieldid"

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 62
    .local v0, "fieldId":Ljava/lang/String;
    new-instance v4, Ljava/util/HashMap;

    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$TtaAbstractRulesListBuilder;->arbitrationInfoMap:Ljava/util/Map;

    invoke-direct {v4, v5}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 63
    .local v4, "threshold":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;>;"
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$TtaAbstractRulesListBuilder;->rulesList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;

    .line 64
    .local v3, "rule":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;
    iput-object v4, v3, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;->arbitrationInfoMap:Ljava/util/Map;

    .line 65
    iput-object v0, v3, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;->fieldId:Ljava/lang/String;

    .line 66
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 68
    .end local v3    # "rule":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;
    :cond_0
    return-object v2
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$TtaAbstractRulesListBuilder;->arbitrationInfoMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 74
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$TtaAbstractRulesListBuilder;->rulesList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 75
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$TtaAbstractRulesListBuilder;->parametersMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 76
    return-void
.end method

.method public getTagName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    const-string/jumbo v0, "RuleSet"

    return-object v0
.end method
