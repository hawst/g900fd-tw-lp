.class Lcom/vlingo/sdk/internal/VLRecognizerImpl$1;
.super Ljava/lang/Object;
.source "VLRecognizerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/sdk/internal/VLRecognizerImpl;->startRecognition(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/sdk/recognition/VLRecognitionListener;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;Lcom/vlingo/sdk/recognition/RecognitionMode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

.field final synthetic this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

.field final synthetic val$mode:Lcom/vlingo/sdk/recognition/RecognitionMode;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/VLRecognizerImpl;Lcom/vlingo/sdk/recognition/RecognitionMode;)V
    .locals 1

    .prologue
    .line 190
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    iput-object p2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$1;->val$mode:Lcom/vlingo/sdk/recognition/RecognitionMode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 192
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mCurrentRecognizerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$200(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$1;->mListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 196
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->getDestroyLock()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 197
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->isValid()Z

    move-result v1

    if-nez v1, :cond_0

    .line 198
    monitor-exit v2

    .line 209
    :goto_0
    return-void

    .line 200
    :cond_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$300(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    move-result-object v1

    sget-object v3, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->GETTING_READY:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-virtual {v1, v3}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->notifyEvent(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    :try_start_1
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecognizer:Lcom/vlingo/sdk/internal/recognizer/Recognizer;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$600(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    move-result-object v1

    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # invokes: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->getSRContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;
    invoke-static {v3}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$400(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$1;->mListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mDataReadyListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;
    invoke-static {v5}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$500(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$1;->val$mode:Lcom/vlingo/sdk/recognition/RecognitionMode;

    invoke-virtual {v1, v3, v4, v5, v6}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->startRecognition(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;Lcom/vlingo/sdk/recognition/RecognitionMode;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 208
    :goto_1
    :try_start_2
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 203
    :catch_0
    move-exception v0

    .line 206
    .local v0, "ise":Ljava/lang/IllegalStateException;
    :try_start_3
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$300(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    move-result-object v1

    sget-object v3, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_RECOGNIZER_BUSY:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    const-string/jumbo v4, "Failed to start recognition."

    invoke-virtual {v1, v3, v4}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->notifyError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method
