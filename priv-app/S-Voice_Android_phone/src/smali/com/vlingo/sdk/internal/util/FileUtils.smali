.class public abstract Lcom/vlingo/sdk/internal/util/FileUtils;
.super Ljava/lang/Object;
.source "FileUtils.java"


# static fields
.field private static final UNZIP_BUFFER_SIZE:I = 0x100

.field private static lastUpdateTime:J

.field private static final log:Lcom/vlingo/sdk/internal/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    const-class v0, Lcom/vlingo/sdk/internal/util/FileUtils;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/sdk/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/util/FileUtils;->log:Lcom/vlingo/sdk/internal/logging/Logger;

    .line 35
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/vlingo/sdk/internal/util/FileUtils;->lastUpdateTime:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static convertHashToString([B)Ljava/lang/String;
    .locals 8
    .param p0, "bytes"    # [B

    .prologue
    .line 315
    const-string/jumbo v4, ""

    .line 316
    .local v4, "returnVal":Ljava/lang/String;
    move-object v0, p0

    .local v0, "arr$":[B
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-byte v1, v0, v2

    .line 317
    .local v1, "b":B
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    and-int/lit16 v6, v1, 0xff

    add-int/lit16 v6, v6, 0x100

    const/16 v7, 0x10

    invoke-static {v6, v7}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 316
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 319
    .end local v1    # "b":B
    :cond_0
    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public static copyFile(Ljava/io/File;Ljava/io/File;)Z
    .locals 10
    .param p0, "srcFile"    # Ljava/io/File;
    .param p1, "dstFile"    # Ljava/io/File;

    .prologue
    const/4 v6, 0x0

    .line 201
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 202
    .local v3, "in":Ljava/io/InputStream;
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 203
    .local v5, "out":Ljava/io/OutputStream;
    const/16 v7, 0x400

    new-array v0, v7, [B

    .line 205
    .local v0, "buf":[B
    :goto_0
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v4

    .local v4, "len":I
    if-lez v4, :cond_0

    .line 206
    const/4 v7, 0x0

    invoke-virtual {v5, v0, v7, v4}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 212
    .end local v0    # "buf":[B
    .end local v3    # "in":Ljava/io/InputStream;
    .end local v4    # "len":I
    .end local v5    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v2

    .line 213
    .local v2, "ex":Ljava/io/FileNotFoundException;
    const-string/jumbo v7, "FileUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "FileNotFound: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    .end local v2    # "ex":Ljava/io/FileNotFoundException;
    :goto_1
    return v6

    .line 208
    .restart local v0    # "buf":[B
    .restart local v3    # "in":Ljava/io/InputStream;
    .restart local v4    # "len":I
    .restart local v5    # "out":Ljava/io/OutputStream;
    :cond_0
    :try_start_1
    invoke-virtual {v5}, Ljava/io/OutputStream;->flush()V

    .line 209
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 210
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 211
    const/4 v6, 0x1

    goto :goto_1

    .line 216
    .end local v0    # "buf":[B
    .end local v3    # "in":Ljava/io/InputStream;
    .end local v4    # "len":I
    .end local v5    # "out":Ljava/io/OutputStream;
    :catch_1
    move-exception v1

    .line 217
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v7, "FileUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "IOException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static doesAssetFileExist(Ljava/lang/String;)Z
    .locals 4
    .param p0, "assetFileName"    # Ljava/lang/String;

    .prologue
    .line 111
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 114
    .local v0, "context":Landroid/content/Context;
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 115
    .local v2, "is":Ljava/io/InputStream;
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    const/4 v3, 0x1

    .line 119
    .end local v2    # "is":Ljava/io/InputStream;
    :goto_0
    return v3

    .line 118
    :catch_0
    move-exception v1

    .line 119
    .local v1, "ex":Ljava/io/IOException;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static extractAssetZip(Ljava/lang/String;Ljava/io/File;)V
    .locals 13
    .param p0, "zipFileName"    # Ljava/lang/String;
    .param p1, "destDir"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v10

    invoke-virtual {v10}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 128
    .local v1, "context":Landroid/content/Context;
    const/4 v8, 0x0

    .line 130
    .local v8, "zip":Ljava/util/zip/ZipInputStream;
    :try_start_0
    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v10

    invoke-virtual {v10, p0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 137
    .local v4, "is":Ljava/io/InputStream;
    :try_start_1
    new-instance v9, Ljava/util/zip/ZipInputStream;

    invoke-direct {v9, v4}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 141
    .end local v8    # "zip":Ljava/util/zip/ZipInputStream;
    .local v9, "zip":Ljava/util/zip/ZipInputStream;
    const/16 v10, 0x100

    :try_start_2
    new-array v0, v10, [B

    .line 142
    .local v0, "bytes":[B
    invoke-virtual {v9}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v7

    .local v7, "ze":Ljava/util/zip/ZipEntry;
    :goto_0
    if-eqz v7, :cond_4

    .line 144
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 146
    .local v6, "path":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 147
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v5

    .line 149
    .local v5, "parent":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_2

    .line 150
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    move-result v10

    if-nez v10, :cond_2

    .line 151
    new-instance v10, Ljava/io/IOException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Unable to create folder "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 162
    .end local v0    # "bytes":[B
    .end local v5    # "parent":Ljava/io/File;
    .end local v6    # "path":Ljava/lang/String;
    .end local v7    # "ze":Ljava/util/zip/ZipEntry;
    :catchall_0
    move-exception v10

    move-object v8, v9

    .end local v9    # "zip":Ljava/util/zip/ZipInputStream;
    .restart local v8    # "zip":Ljava/util/zip/ZipInputStream;
    :goto_1
    if-eqz v8, :cond_0

    .line 164
    :try_start_3
    invoke-virtual {v8}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 169
    :cond_0
    :goto_2
    if-eqz v4, :cond_1

    .line 171
    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 162
    :cond_1
    :goto_3
    throw v10

    .line 131
    .end local v4    # "is":Ljava/io/InputStream;
    :catch_0
    move-exception v3

    .line 177
    :goto_4
    return-void

    .line 155
    .end local v8    # "zip":Ljava/util/zip/ZipInputStream;
    .restart local v0    # "bytes":[B
    .restart local v4    # "is":Ljava/io/InputStream;
    .restart local v6    # "path":Ljava/lang/String;
    .restart local v7    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v9    # "zip":Ljava/util/zip/ZipInputStream;
    :cond_2
    :try_start_5
    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v10

    if-nez v10, :cond_3

    .line 156
    invoke-static {v9, v6, v0}, Lcom/vlingo/sdk/internal/util/FileUtils;->getFileFromZip(Ljava/util/zip/ZipInputStream;Ljava/lang/String;[B)V

    .line 142
    :goto_5
    invoke-virtual {v9}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v7

    goto :goto_0

    .line 158
    :cond_3
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->mkdirs()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_5

    .line 162
    .end local v6    # "path":Ljava/lang/String;
    :cond_4
    if-eqz v9, :cond_5

    .line 164
    :try_start_6
    invoke-virtual {v9}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 169
    :cond_5
    :goto_6
    if-eqz v4, :cond_6

    .line 171
    :try_start_7
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    :cond_6
    :goto_7
    move-object v8, v9

    .line 177
    .end local v9    # "zip":Ljava/util/zip/ZipInputStream;
    .restart local v8    # "zip":Ljava/util/zip/ZipInputStream;
    goto :goto_4

    .line 165
    .end local v0    # "bytes":[B
    .end local v7    # "ze":Ljava/util/zip/ZipEntry;
    :catch_1
    move-exception v2

    .line 166
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 172
    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 173
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 165
    .end local v2    # "e":Ljava/io/IOException;
    .end local v8    # "zip":Ljava/util/zip/ZipInputStream;
    .restart local v0    # "bytes":[B
    .restart local v7    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v9    # "zip":Ljava/util/zip/ZipInputStream;
    :catch_3
    move-exception v2

    .line 166
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 172
    .end local v2    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v2

    .line 173
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 162
    .end local v0    # "bytes":[B
    .end local v2    # "e":Ljava/io/IOException;
    .end local v7    # "ze":Ljava/util/zip/ZipEntry;
    .end local v9    # "zip":Ljava/util/zip/ZipInputStream;
    .restart local v8    # "zip":Ljava/util/zip/ZipInputStream;
    :catchall_1
    move-exception v10

    goto :goto_1
.end method

.method public static extractAssetZipIfNeeded(Ljava/lang/String;Ljava/io/File;Landroid/content/SharedPreferences$Editor;)Z
    .locals 11
    .param p0, "zipFileName"    # Ljava/lang/String;
    .param p1, "destDir"    # Ljava/io/File;
    .param p2, "editor"    # Landroid/content/SharedPreferences$Editor;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    const/4 v5, 0x0

    const-wide/16 v9, 0x0

    .line 39
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 41
    .local v0, "context":Landroid/content/Context;
    sget-wide v6, Lcom/vlingo/sdk/internal/util/FileUtils;->lastUpdateTime:J

    cmp-long v6, v6, v9

    if-nez v6, :cond_0

    .line 43
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget-wide v6, v6, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    sput-wide v6, Lcom/vlingo/sdk/internal/util/FileUtils;->lastUpdateTime:J
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    :cond_0
    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "zip_timestamp_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 50
    .local v2, "settingsName":Ljava/lang/String;
    invoke-static {v2, v9, v10}, Lcom/vlingo/sdk/internal/settings/Settings;->getPersistentLong(Ljava/lang/String;J)J

    move-result-wide v3

    .line 51
    .local v3, "timestamp":J
    cmp-long v6, v3, v9

    if-eqz v6, :cond_1

    sget-wide v6, Lcom/vlingo/sdk/internal/util/FileUtils;->lastUpdateTime:J

    cmp-long v6, v6, v3

    if-eqz v6, :cond_2

    .line 55
    :cond_1
    :try_start_1
    invoke-static {p0, p1}, Lcom/vlingo/sdk/internal/util/FileUtils;->extractAssetZip(Ljava/lang/String;Ljava/io/File;)V

    .line 56
    sget-wide v6, Lcom/vlingo/sdk/internal/util/FileUtils;->lastUpdateTime:J

    invoke-interface {p2, v2, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 57
    const/4 v5, 0x1

    .line 68
    :cond_2
    :goto_1
    return v5

    .line 44
    .end local v2    # "settingsName":Ljava/lang/String;
    .end local v3    # "timestamp":J
    :catch_0
    move-exception v1

    .line 45
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 58
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v2    # "settingsName":Ljava/lang/String;
    .restart local v3    # "timestamp":J
    :catch_1
    move-exception v1

    .line 61
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public static extractFileFromAssetZip(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V
    .locals 10
    .param p0, "zipFileName"    # Ljava/lang/String;
    .param p1, "destDir"    # Ljava/io/File;
    .param p2, "absolutePathName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 74
    .local v1, "context":Landroid/content/Context;
    const/4 v3, 0x0

    .line 75
    .local v3, "is":Ljava/io/InputStream;
    const/4 v6, 0x0

    .line 77
    .local v6, "zip":Ljava/util/zip/ZipInputStream;
    :try_start_0
    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v8

    invoke-virtual {v8, p0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 78
    new-instance v7, Ljava/util/zip/ZipInputStream;

    invoke-direct {v7, v3}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    .end local v6    # "zip":Ljava/util/zip/ZipInputStream;
    .local v7, "zip":Ljava/util/zip/ZipInputStream;
    const/16 v8, 0x100

    :try_start_1
    new-array v0, v8, [B

    .line 83
    .local v0, "bytes":[B
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v5

    .local v5, "ze":Ljava/util/zip/ZipEntry;
    :goto_0
    if-eqz v5, :cond_3

    .line 85
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 86
    .local v4, "path":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v8

    if-nez v8, :cond_0

    .line 87
    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 88
    invoke-static {v7, v4, v0}, Lcom/vlingo/sdk/internal/util/FileUtils;->getFileFromZip(Ljava/util/zip/ZipInputStream;Ljava/lang/String;[B)V

    .line 83
    :cond_0
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v5

    goto :goto_0

    .line 93
    .end local v0    # "bytes":[B
    .end local v4    # "path":Ljava/lang/String;
    .end local v5    # "ze":Ljava/util/zip/ZipEntry;
    .end local v7    # "zip":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "zip":Ljava/util/zip/ZipInputStream;
    :catchall_0
    move-exception v8

    :goto_1
    if-eqz v6, :cond_1

    .line 95
    :try_start_2
    invoke-virtual {v6}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 100
    :cond_1
    :goto_2
    if-eqz v3, :cond_2

    .line 102
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 93
    :cond_2
    :goto_3
    throw v8

    .end local v6    # "zip":Ljava/util/zip/ZipInputStream;
    .restart local v0    # "bytes":[B
    .restart local v5    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v7    # "zip":Ljava/util/zip/ZipInputStream;
    :cond_3
    if-eqz v7, :cond_4

    .line 95
    :try_start_4
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 100
    :cond_4
    :goto_4
    if-eqz v3, :cond_5

    .line 102
    :try_start_5
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 108
    :cond_5
    :goto_5
    return-void

    .line 96
    .end local v0    # "bytes":[B
    .end local v5    # "ze":Ljava/util/zip/ZipEntry;
    .end local v7    # "zip":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "zip":Ljava/util/zip/ZipInputStream;
    :catch_0
    move-exception v2

    .line 97
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 103
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 104
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 96
    .end local v2    # "e":Ljava/io/IOException;
    .end local v6    # "zip":Ljava/util/zip/ZipInputStream;
    .restart local v0    # "bytes":[B
    .restart local v5    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v7    # "zip":Ljava/util/zip/ZipInputStream;
    :catch_2
    move-exception v2

    .line 97
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 103
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 104
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 93
    .end local v0    # "bytes":[B
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "ze":Ljava/util/zip/ZipEntry;
    :catchall_1
    move-exception v8

    move-object v6, v7

    .end local v7    # "zip":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "zip":Ljava/util/zip/ZipInputStream;
    goto :goto_1
.end method

.method public static fileToHash(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "algorithm"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 278
    const/4 v3, 0x0

    .line 280
    .local v3, "inputStream":Ljava/io/InputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281
    .end local v3    # "inputStream":Ljava/io/InputStream;
    .local v4, "inputStream":Ljava/io/InputStream;
    const/16 v7, 0x100

    :try_start_1
    new-array v0, v7, [B

    .line 282
    .local v0, "buffer":[B
    invoke-static {p1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 283
    .local v1, "digest":Ljava/security/MessageDigest;
    const/4 v6, 0x0

    .line 284
    .local v6, "numRead":I
    :cond_0
    :goto_0
    const/4 v7, -0x1

    if-eq v6, v7, :cond_2

    .line 285
    invoke-virtual {v4, v0}, Ljava/io/InputStream;->read([B)I

    move-result v6

    .line 286
    if-lez v6, :cond_0

    .line 287
    const/4 v7, 0x0

    invoke-virtual {v1, v0, v7, v6}, Ljava/security/MessageDigest;->update([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 294
    .end local v0    # "buffer":[B
    .end local v1    # "digest":Ljava/security/MessageDigest;
    .end local v6    # "numRead":I
    :catch_0
    move-exception v2

    move-object v3, v4

    .line 299
    .end local v4    # "inputStream":Ljava/io/InputStream;
    .local v2, "e":Ljava/io/IOException;
    .restart local v3    # "inputStream":Ljava/io/InputStream;
    :goto_1
    const/4 v7, 0x0

    .line 301
    if-eqz v3, :cond_1

    .line 303
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 299
    .end local v2    # "e":Ljava/io/IOException;
    :cond_1
    :goto_2
    return-object v7

    .line 290
    .end local v3    # "inputStream":Ljava/io/InputStream;
    .restart local v0    # "buffer":[B
    .restart local v1    # "digest":Ljava/security/MessageDigest;
    .restart local v4    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "numRead":I
    :cond_2
    :try_start_3
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v5

    .line 292
    .local v5, "md5Bytes":[B
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 293
    invoke-static {v5}, Lcom/vlingo/sdk/internal/util/FileUtils;->convertHashToString([B)Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v7

    .line 301
    if-eqz v4, :cond_3

    .line 303
    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_3
    :goto_3
    move-object v3, v4

    .line 293
    .end local v4    # "inputStream":Ljava/io/InputStream;
    .restart local v3    # "inputStream":Ljava/io/InputStream;
    goto :goto_2

    .line 301
    .end local v0    # "buffer":[B
    .end local v1    # "digest":Ljava/security/MessageDigest;
    .end local v5    # "md5Bytes":[B
    .end local v6    # "numRead":I
    :catchall_0
    move-exception v7

    :goto_4
    if-eqz v3, :cond_4

    .line 303
    :try_start_5
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 301
    :cond_4
    :goto_5
    throw v7

    .line 304
    .restart local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v8

    goto :goto_2

    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v8

    goto :goto_5

    .end local v3    # "inputStream":Ljava/io/InputStream;
    .restart local v0    # "buffer":[B
    .restart local v1    # "digest":Ljava/security/MessageDigest;
    .restart local v4    # "inputStream":Ljava/io/InputStream;
    .restart local v5    # "md5Bytes":[B
    .restart local v6    # "numRead":I
    :catch_3
    move-exception v8

    goto :goto_3

    .line 301
    .end local v0    # "buffer":[B
    .end local v1    # "digest":Ljava/security/MessageDigest;
    .end local v5    # "md5Bytes":[B
    .end local v6    # "numRead":I
    :catchall_1
    move-exception v7

    move-object v3, v4

    .end local v4    # "inputStream":Ljava/io/InputStream;
    .restart local v3    # "inputStream":Ljava/io/InputStream;
    goto :goto_4

    .line 294
    :catch_4
    move-exception v2

    goto :goto_1
.end method

.method private static getFileFromZip(Ljava/util/zip/ZipInputStream;Ljava/lang/String;[B)V
    .locals 6
    .param p0, "zipInputStream"    # Ljava/util/zip/ZipInputStream;
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "bytes"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 180
    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 181
    .local v1, "fout":Ljava/io/OutputStream;
    const/4 v2, 0x0

    .line 182
    .local v2, "wrote":Z
    invoke-virtual {p0, p2}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v0

    .local v0, "c":I
    :goto_0
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 183
    const/4 v3, 0x0

    invoke-virtual {v1, p2, v3, v0}, Ljava/io/OutputStream;->write([BII)V

    .line 184
    const/4 v2, 0x1

    .line 182
    invoke-virtual {p0, p2}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v0

    goto :goto_0

    .line 186
    :cond_0
    invoke-virtual {p0}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 187
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 189
    if-eqz v2, :cond_1

    .line 197
    return-void

    .line 194
    :cond_1
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 195
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "didn\'t actually write to new file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public static readResource(Ljava/lang/String;)[B
    .locals 7
    .param p0, "resource"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0x1f4

    .line 227
    const/4 v4, 0x0

    .line 228
    .local v4, "is":Ljava/io/InputStream;
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0, v5}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 229
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    const/4 v3, 0x0

    .line 230
    .local v3, "data":[B
    new-array v1, v5, [B

    .line 231
    .local v1, "buf":[B
    const/4 v2, 0x0

    .line 235
    .local v2, "bytesRead":I
    :try_start_0
    const-class v5, Ljava/lang/Class;

    invoke-virtual {v5, p0}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 236
    if-nez v4, :cond_1

    .line 237
    const/4 v5, 0x0

    .line 251
    if-eqz v4, :cond_0

    .line 253
    :try_start_1
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 258
    :cond_0
    :goto_0
    :try_start_2
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6

    .line 262
    :goto_1
    return-object v5

    .line 238
    :cond_1
    :goto_2
    :try_start_3
    invoke-virtual {v4, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v5, -0x1

    if-eq v2, v5, :cond_3

    .line 240
    const/4 v5, 0x0

    invoke-virtual {v0, v1, v5, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 245
    :catch_0
    move-exception v5

    .line 251
    if-eqz v4, :cond_2

    .line 253
    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 258
    :cond_2
    :goto_3
    :try_start_5
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :goto_4
    move-object v5, v3

    .line 262
    goto :goto_1

    .line 243
    :cond_3
    :try_start_6
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 244
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v3

    .line 251
    if-eqz v4, :cond_4

    .line 253
    :try_start_7
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7

    .line 258
    :cond_4
    :goto_5
    :try_start_8
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_4

    .line 259
    :catch_1
    move-exception v5

    goto :goto_4

    .line 251
    :catchall_0
    move-exception v5

    if-eqz v4, :cond_5

    .line 253
    :try_start_9
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    .line 258
    :cond_5
    :goto_6
    :try_start_a
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    .line 251
    :goto_7
    throw v5

    .line 254
    :catch_2
    move-exception v5

    goto :goto_3

    :catch_3
    move-exception v6

    goto :goto_6

    .line 259
    :catch_4
    move-exception v6

    goto :goto_7

    .line 254
    :catch_5
    move-exception v6

    goto :goto_0

    .line 259
    :catch_6
    move-exception v6

    goto :goto_1

    .line 254
    :catch_7
    move-exception v5

    goto :goto_5
.end method
