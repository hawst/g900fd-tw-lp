.class public abstract Lcom/vlingo/sdk/internal/lmtt/LMTTItem;
.super Ljava/lang/Object;
.source "LMTTItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/lmtt/LMTTItem$1;,
        Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;,
        Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;
    }
.end annotation


# instance fields
.field public changeType:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

.field public uid:J

.field public upType:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;JLcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V
    .locals 1
    .param p1, "type"    # Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;
    .param p2, "uid"    # J
    .param p4, "changeType"    # Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget-object v0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;->TYPE_UNKNOWN:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem;->upType:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;

    .line 23
    iput-object p1, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem;->upType:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;

    .line 24
    iput-wide p2, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem;->uid:J

    .line 25
    iput-object p4, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem;->changeType:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    .line 26
    return-void
.end method


# virtual methods
.method public abstract getDelXML(Ljava/lang/StringBuilder;)V
.end method

.method public abstract getInsXML(Ljava/lang/StringBuilder;)V
.end method

.method public abstract getUpXML(Ljava/lang/StringBuilder;)V
.end method

.method public getXML()Ljava/lang/String;
    .locals 3

    .prologue
    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .local v0, "sb":Ljava/lang/StringBuilder;
    sget-object v1, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$1;->$SwitchMap$com$vlingo$sdk$internal$lmtt$LMTTItem$ChangeType:[I

    iget-object v2, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem;->changeType:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 39
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 35
    :pswitch_0
    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/lmtt/LMTTItem;->getUpXML(Ljava/lang/StringBuilder;)V

    goto :goto_0

    .line 36
    :pswitch_1
    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/lmtt/LMTTItem;->getInsXML(Ljava/lang/StringBuilder;)V

    goto :goto_0

    .line 37
    :pswitch_2
    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/lmtt/LMTTItem;->getDelXML(Ljava/lang/StringBuilder;)V

    goto :goto_0

    .line 34
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setChangeType(Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;)V
    .locals 0
    .param p1, "type"    # Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem;->changeType:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    .line 30
    return-void
.end method
