.class public final Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;
.super Ljava/lang/Object;
.source "VoconRecognizer.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer$VoconRecognizerListenerImpl;
    }
.end annotation


# instance fields
.field private isInitialized:Z

.field private mListener:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;

.field private voconController:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

.field private voconRecognitionControl:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognitionControl;

.field private final voconRecognitionPrepareSync:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->voconRecognitionPrepareSync:Ljava/lang/Object;

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->isInitialized:Z

    .line 26
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->mListener:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;

    .line 27
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->bindToInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->voconController:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->voconRecognitionPrepareSync:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public completeAudioRecognition()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->voconRecognitionControl:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognitionControl;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->voconRecognitionControl:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognitionControl;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognitionControl;->completeRecognition()V

    .line 84
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->onCancelled()V

    .line 35
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->voconController:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->voconController:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->unbindFromInstance(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;)V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->voconController:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    .line 39
    :cond_0
    return-void
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->isInitialized:Z

    return v0
.end method

.method public onCancelled()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->voconRecognitionControl:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognitionControl;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->voconRecognitionControl:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognitionControl;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognitionControl;->cancelRecognition()V

    .line 108
    :cond_0
    return-void
.end method

.method public onRecordingEnpointed()V
    .locals 0

    .prologue
    .line 91
    return-void
.end method

.method public onRecordingFinished()V
    .locals 0

    .prologue
    .line 98
    return-void
.end method

.method public onRecordingStarted()V
    .locals 0

    .prologue
    .line 74
    return-void
.end method

.method public prepareForRequest(Lcom/vlingo/sdk/internal/recognizer/SRContext;ZLcom/vlingo/sdk/internal/recognizer/RecognizerListener;)Z
    .locals 8
    .param p1, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .param p2, "sendAudio"    # Z
    .param p3, "listener"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 45
    new-array v1, v7, [Z

    aput-boolean v2, v1, v2

    .line 47
    .local v1, "preparedForRecognition":[Z
    :try_start_0
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->voconRecognitionPrepareSync:Ljava/lang/Object;

    monitor-enter v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    :try_start_1
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->voconController:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    new-instance v5, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer$1;

    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->mListener:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;

    invoke-direct {v5, p0, v6, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer$1;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;[Z)V

    invoke-virtual {v3, v5, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognize(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizerListener;Lcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognitionControl;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->voconRecognitionControl:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognitionControl;

    .line 57
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->voconRecognitionPrepareSync:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->wait()V

    .line 58
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 65
    aget-boolean v3, v1, v2

    if-eqz v3, :cond_0

    .line 66
    iput-boolean v7, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->isInitialized:Z

    .line 68
    :cond_0
    aget-boolean v2, v1, v2

    :goto_0
    return v2

    .line 58
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v3
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 59
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->mListener:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;

    sget-object v4, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;->READER_ERROR:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;

    const-string/jumbo v5, "Can\'t record.  Please try again."

    invoke-interface {v3, v4, v5}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;->onRecognizerError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public processAudio([S)V
    .locals 1
    .param p1, "rawShorts"    # [S

    .prologue
    .line 114
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->voconRecognitionControl:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognitionControl;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->voconRecognitionControl:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognitionControl;

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognitionControl;->processAudio([S)V

    .line 117
    :cond_0
    return-void
.end method
