.class public Lcom/vlingo/sdk/internal/net/ConnectionResult;
.super Ljava/lang/Object;
.source "ConnectionResult.java"


# instance fields
.field public apnUsed:Z

.field public connection:Lcom/vlingo/sdk/internal/net/Connection;

.field public connectionType:I

.field public connectionTypeString:Ljava/lang/String;

.field public failureException:Ljava/lang/Exception;

.field public isFailure:Z

.field public timeToComplete:J

.field public url:Ljava/lang/String;

.field public wifiActive:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-boolean v1, p0, Lcom/vlingo/sdk/internal/net/ConnectionResult;->isFailure:Z

    .line 23
    iput-object v0, p0, Lcom/vlingo/sdk/internal/net/ConnectionResult;->failureException:Ljava/lang/Exception;

    .line 26
    iput-object v0, p0, Lcom/vlingo/sdk/internal/net/ConnectionResult;->connection:Lcom/vlingo/sdk/internal/net/Connection;

    .line 27
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/sdk/internal/net/ConnectionResult;->url:Ljava/lang/String;

    .line 28
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/sdk/internal/net/ConnectionResult;->connectionType:I

    .line 29
    iput-boolean v1, p0, Lcom/vlingo/sdk/internal/net/ConnectionResult;->wifiActive:Z

    .line 30
    iput-boolean v1, p0, Lcom/vlingo/sdk/internal/net/ConnectionResult;->apnUsed:Z

    .line 31
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/sdk/internal/net/ConnectionResult;->timeToComplete:J

    .line 32
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/sdk/internal/net/ConnectionResult;->connectionTypeString:Ljava/lang/String;

    .line 33
    return-void
.end method
