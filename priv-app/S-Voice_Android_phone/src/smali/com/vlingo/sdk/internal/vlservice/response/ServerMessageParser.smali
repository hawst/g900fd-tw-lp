.class public Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;
.super Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;
.source "ServerMessageParser.java"


# instance fields
.field private final XML_ELEMENT_CODE:I

.field private final XML_ELEMENT_DETAILS:I

.field private final XML_ELEMENT_ERROR:I

.field private final XML_ELEMENT_MESSAGE:I

.field private final XML_ELEMENT_STATUS:I

.field private final XML_ELEMENT_WARNING:I

.field private inDetails:Z

.field private msg:Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;)V
    .locals 1
    .param p1, "parser"    # Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;-><init>(Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;)V

    .line 28
    const-string/jumbo v0, "Status"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->XML_ELEMENT_STATUS:I

    .line 29
    const-string/jumbo v0, "Warning"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->XML_ELEMENT_WARNING:I

    .line 30
    const-string/jumbo v0, "Error"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->XML_ELEMENT_ERROR:I

    .line 31
    const-string/jumbo v0, "Message"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->XML_ELEMENT_MESSAGE:I

    .line 32
    const-string/jumbo v0, "Code"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->XML_ELEMENT_CODE:I

    .line 33
    const-string/jumbo v0, "Details"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->XML_ELEMENT_DETAILS:I

    .line 34
    return-void
.end method


# virtual methods
.method public beginElement(ILcom/vlingo/sdk/internal/xml/XmlAttributes;[CI)V
    .locals 3
    .param p1, "elementType"    # I
    .param p2, "attributes"    # Lcom/vlingo/sdk/internal/xml/XmlAttributes;
    .param p3, "cData"    # [C
    .param p4, "elementEndPosition"    # I

    .prologue
    const/4 v2, 0x1

    .line 37
    const/4 v0, 0x0

    .line 38
    .local v0, "cDataString":Ljava/lang/String;
    if-eqz p3, :cond_0

    .line 39
    invoke-static {p3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v0

    .line 42
    :cond_0
    iget v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->XML_ELEMENT_STATUS:I

    if-ne v1, p1, :cond_2

    .line 43
    new-instance v1, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    invoke-direct {v1}, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;-><init>()V

    iput-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->msg:Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    .line 44
    iget-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->msg:Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;->setType(I)V

    .line 71
    :cond_1
    :goto_0
    return-void

    .line 46
    :cond_2
    iget v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->XML_ELEMENT_WARNING:I

    if-ne v1, p1, :cond_3

    .line 47
    new-instance v1, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    invoke-direct {v1}, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;-><init>()V

    iput-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->msg:Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    .line 48
    iget-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->msg:Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;->setType(I)V

    goto :goto_0

    .line 50
    :cond_3
    iget v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->XML_ELEMENT_ERROR:I

    if-ne v1, p1, :cond_4

    .line 51
    new-instance v1, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    invoke-direct {v1}, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;-><init>()V

    iput-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->msg:Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    .line 52
    iget-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->msg:Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;->setType(I)V

    goto :goto_0

    .line 54
    :cond_4
    iget v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->XML_ELEMENT_MESSAGE:I

    if-ne v1, p1, :cond_6

    .line 55
    iget-boolean v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->inDetails:Z

    if-eqz v1, :cond_5

    .line 56
    iget-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->msg:Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    invoke-virtual {v1, v0}, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;->setDetailMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 58
    :cond_5
    iget-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->msg:Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    invoke-virtual {v1, v0}, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;->setMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 61
    :cond_6
    iget v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->XML_ELEMENT_CODE:I

    if-ne v1, p1, :cond_8

    .line 62
    iget-boolean v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->inDetails:Z

    if-eqz v1, :cond_7

    .line 63
    iget-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->msg:Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    invoke-virtual {v1, v0}, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;->setDetailCode(Ljava/lang/String;)V

    goto :goto_0

    .line 65
    :cond_7
    iget-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->msg:Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    invoke-virtual {v1, v0}, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;->setCode(Ljava/lang/String;)V

    goto :goto_0

    .line 68
    :cond_8
    iget v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->XML_ELEMENT_DETAILS:I

    if-ne v1, p1, :cond_1

    .line 69
    iput-boolean v2, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->inDetails:Z

    goto :goto_0
.end method

.method public endElement(II)V
    .locals 2
    .param p1, "elementType"    # I
    .param p2, "elementStartPosition"    # I

    .prologue
    .line 74
    iget v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->XML_ELEMENT_STATUS:I

    if-eq v0, p1, :cond_0

    iget v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->XML_ELEMENT_WARNING:I

    if-eq v0, p1, :cond_0

    iget v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->XML_ELEMENT_ERROR:I

    if-ne v0, p1, :cond_2

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->responseParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->getResponse()Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->msg:Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->addMessage(Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;)V

    .line 79
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->responseParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->onSectionComplete()V

    .line 84
    :cond_1
    :goto_0
    return-void

    .line 81
    :cond_2
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->inDetails:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->XML_ELEMENT_DETAILS:I

    if-ne v0, p1, :cond_1

    .line 82
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->inDetails:Z

    goto :goto_0
.end method

.method public handlesElement(I)Z
    .locals 1
    .param p1, "elementType"    # I

    .prologue
    .line 87
    iget v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->XML_ELEMENT_STATUS:I

    if-eq v0, p1, :cond_0

    iget v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->XML_ELEMENT_WARNING:I

    if-eq v0, p1, :cond_0

    iget v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessageParser;->XML_ELEMENT_ERROR:I

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
