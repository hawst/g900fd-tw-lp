.class public abstract Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;
.super Ljava/lang/Object;
.source "DataReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/reader/DataReader$ReaderThread;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mDataReadyListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

.field private mGuttId:Ljava/lang/String;

.field private mInputStream:Ljava/io/InputStream;

.field private mIsStopped:Z

.field private mListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;

.field private mRawData:[B

.field private mReaderThread:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader$ReaderThread;

.field private mSrContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

.field private mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

.field private mTotalDuration:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->TAG:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;)V
    .locals 1
    .param p1, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .param p2, "listener"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;
    .param p3, "DataReadyListener"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mRawData:[B

    .line 55
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mSrContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    .line 56
    iput-object p2, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;

    .line 57
    iput-object p3, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mDataReadyListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    .line 58
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;)Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mIsStopped:Z

    return v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    .prologue
    .line 25
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mTotalDuration:I

    return v0
.end method

.method static synthetic access$500(Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;)Ljava/io/InputStream;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mInputStream:Ljava/io/InputStream;

    return-object v0
.end method

.method public static getDataReader(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;
    .locals 2
    .param p0, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .param p1, "listener"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;
    .param p2, "DataReadyListener"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getAudioSourceInfo()Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v0

    .line 47
    .local v0, "asi":Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->isAMR()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    new-instance v1, Lcom/vlingo/sdk/internal/recognizer/reader/AMRDataReader;

    invoke-direct {v1, p0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/reader/AMRDataReader;-><init>(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;)V

    .line 50
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;

    invoke-direct {v1, p0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/reader/SpeexDataReader;-><init>(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;)V

    goto :goto_0
.end method


# virtual methods
.method protected getDataReadyListener()Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mDataReadyListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    return-object v0
.end method

.method protected getInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mInputStream:Ljava/io/InputStream;

    return-object v0
.end method

.method protected getMaxDuration()I
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mSrContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getMaxAudioTime()I

    move-result v0

    return v0
.end method

.method protected getRawData()[B
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mRawData:[B

    return-object v0
.end method

.method protected getSRContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mSrContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    return-object v0
.end method

.method public init()Z
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 71
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    if-eqz v4, :cond_0

    .line 72
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    const-string/jumbo v5, "PPRS"

    invoke-virtual {v4, v5}, Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;->recordAndTimeStampEvent(Ljava/lang/String;)V

    .line 75
    :cond_0
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mSrContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getAudioSourceInfo()Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v0

    .line 76
    .local v0, "asi":Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->isFile()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 79
    :try_start_0
    new-instance v4, Lcom/vlingo/sdk/internal/util/AutoCloseFileInputStream;

    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getFilename()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/vlingo/sdk/internal/util/AutoCloseFileInputStream;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mInputStream:Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :goto_0
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/sdk/VLSdk;->getDebugSettings()Lcom/vlingo/sdk/util/SDKDebugSettings;

    move-result-object v1

    .line 93
    .local v1, "ds":Lcom/vlingo/sdk/util/SDKDebugSettings;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/vlingo/sdk/util/SDKDebugSettings;->isLogRecoWaveform()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v1}, Lcom/vlingo/sdk/util/SDKDebugSettings;->isSaveAudioFiles()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 94
    :cond_1
    new-instance v4, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;

    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mInputStream:Ljava/io/InputStream;

    invoke-virtual {v1}, Lcom/vlingo/sdk/util/SDKDebugSettings;->isSaveAudioFiles()Z

    move-result v6

    invoke-virtual {v1}, Lcom/vlingo/sdk/util/SDKDebugSettings;->isLogRecoWaveform()Z

    move-result v7

    invoke-direct {v4, v5, v6, v7}, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;-><init>(Ljava/io/InputStream;ZZ)V

    iput-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mInputStream:Ljava/io/InputStream;

    .line 97
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->onInit()Z

    move-result v3

    .line 99
    .local v3, "isSuccess":Z
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    if-eqz v4, :cond_3

    .line 100
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    const-string/jumbo v5, "PPRE"

    invoke-virtual {v4, v5}, Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;->recordAndTimeStampEvent(Ljava/lang/String;)V

    .line 103
    .end local v1    # "ds":Lcom/vlingo/sdk/util/SDKDebugSettings;
    .end local v3    # "isSuccess":Z
    :cond_3
    :goto_1
    return v3

    .line 80
    :catch_0
    move-exception v2

    .line 81
    .local v2, "fnfe":Ljava/io/FileNotFoundException;
    sget-object v4, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Unable to create inputstream from file: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 85
    .end local v2    # "fnfe":Ljava/io/FileNotFoundException;
    :cond_4
    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->isStream()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 86
    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mInputStream:Ljava/io/InputStream;

    goto :goto_0

    .line 88
    :cond_5
    sget-object v4, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "Unknown audio source"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method isSpeechDetected()Z
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x1

    return v0
.end method

.method protected onDataAvailable([B[SI)V
    .locals 1
    .param p1, "encodedBytes"    # [B
    .param p2, "rawShorts"    # [S
    .param p3, "totalDuration"    # I

    .prologue
    .line 199
    iput p3, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mTotalDuration:I

    .line 200
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;

    invoke-interface {v0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;->onDataAvailable([B[S)V

    .line 201
    return-void
.end method

.method protected abstract onDeinit()V
.end method

.method protected onError(Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;Ljava/lang/String;)V
    .locals 1
    .param p1, "code"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->stop()V

    .line 187
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;

    invoke-interface {v0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;->onError(Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;Ljava/lang/String;)V

    .line 188
    return-void
.end method

.method protected abstract onInit()Z
.end method

.method protected abstract onProcessData()V
.end method

.method protected onRMSDataAvailable(I)V
    .locals 1
    .param p1, "energy"    # I

    .prologue
    .line 204
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;->onRMSDataAvailable(I)V

    .line 205
    return-void
.end method

.method public final setGuttId(Ljava/lang/String;)V
    .locals 0
    .param p1, "guttId"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mGuttId:Ljava/lang/String;

    .line 123
    return-void
.end method

.method public final setTimings(Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;)V
    .locals 0
    .param p1, "timings"    # Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    .line 62
    return-void
.end method

.method public final start()V
    .locals 1

    .prologue
    .line 110
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader$ReaderThread;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader$ReaderThread;-><init>(Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mReaderThread:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader$ReaderThread;

    .line 111
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mReaderThread:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader$ReaderThread;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader$ReaderThread;->start()V

    .line 112
    return-void
.end method

.method public final stop()V
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mIsStopped:Z

    .line 119
    return-void
.end method

.method public final writeLog()V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mInputStream:Ljava/io/InputStream;

    instance-of v0, v0, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mInputStream:Ljava/io/InputStream;

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->mGuttId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/reader/InputStreamLogger;->writeLog(Ljava/lang/String;)V

    .line 129
    :cond_0
    return-void
.end method
