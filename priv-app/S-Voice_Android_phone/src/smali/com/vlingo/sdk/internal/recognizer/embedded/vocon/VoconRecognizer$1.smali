.class Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer$1;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer$VoconRecognizerListenerImpl;
.source "VoconRecognizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->prepareForRequest(Lcom/vlingo/sdk/internal/recognizer/SRContext;ZLcom/vlingo/sdk/internal/recognizer/RecognizerListener;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;

.field final synthetic val$preparedForRecognition:[Z


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;[Z)V
    .locals 0
    .param p2, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer$1;->this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;

    iput-object p3, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer$1;->val$preparedForRecognition:[Z

    invoke-direct {p0, p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer$VoconRecognizerListenerImpl;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedListener;)V

    return-void
.end method


# virtual methods
.method public onPreparedForRecognition(Z)V
    .locals 3
    .param p1, "success"    # Z

    .prologue
    .line 51
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer$1;->this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->voconRecognitionPrepareSync:Ljava/lang/Object;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->access$000(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 52
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer$1;->val$preparedForRecognition:[Z

    const/4 v2, 0x0

    aput-boolean p1, v0, v2

    .line 53
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer$1;->this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->voconRecognitionPrepareSync:Ljava/lang/Object;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;->access$000(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizer;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 54
    monitor-exit v1

    .line 55
    return-void

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
