.class public Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;
.super Lcom/vlingo/sdk/internal/recognizer/network/SRManager;
.source "ASRManager.java"


# instance fields
.field private mClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

.field private volatile mLastGuttId:Ljava/lang/String;

.field private mServerDetails:Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;

.field private mSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

.field private final mTimingRepository:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/net/ConnectionProvider;Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;)V
    .locals 1
    .param p1, "connectionProvider"    # Lcom/vlingo/sdk/internal/net/ConnectionProvider;
    .param p2, "timings"    # Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/SRManager;-><init>()V

    .line 36
    const-string/jumbo v0, "ctor"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->log(Ljava/lang/String;)V

    .line 37
    iput-object p2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->mTimingRepository:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    .line 38
    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 159
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 61
    const-string/jumbo v0, "destroy()"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->log(Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method public getConnection(Lcom/vlingo/sdk/internal/recognizer/SRContext;)Ljava/net/HttpURLConnection;
    .locals 3
    .param p1, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    const-string/jumbo v2, "getConnection()"

    invoke-direct {p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->log(Ljava/lang/String;)V

    .line 150
    new-instance v1, Ljava/net/URL;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->mServerDetails:Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;

    invoke-interface {v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;->getASRURL()Lcom/vlingo/sdk/internal/http/URL;

    move-result-object v2

    iget-object v2, v2, Lcom/vlingo/sdk/internal/http/URL;->urlField:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 151
    .local v1, "url":Ljava/net/URL;
    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 152
    .local v0, "conn":Ljava/net/HttpURLConnection;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->setupTLSIfNeeded(Ljava/net/HttpURLConnection;)V

    .line 153
    return-object v0
.end method

.method public getLastGuttID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->mLastGuttId:Ljava/lang/String;

    return-object v0
.end method

.method public getServerDetails()Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->mServerDetails:Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;

    return-object v0
.end method

.method public init(Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;)V
    .locals 1
    .param p1, "serverDetails"    # Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;
    .param p2, "clientMeta"    # Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    .param p3, "softwareMeta"    # Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    .prologue
    .line 42
    const-string/jumbo v0, "init()"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->log(Ljava/lang/String;)V

    .line 43
    iput-object p2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->mClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    .line 44
    iput-object p3, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->mSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    .line 46
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->setServer(Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;)V

    .line 47
    return-void
.end method

.method public newRequest(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;)Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;
    .locals 1
    .param p1, "info"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .param p2, "listener"    # Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    .prologue
    .line 71
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->newRequest(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;Z)Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;

    move-result-object v0

    return-object v0
.end method

.method public newRequest(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;Z)Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;
    .locals 7
    .param p1, "info"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .param p2, "listener"    # Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;
    .param p3, "sendAudio"    # Z

    .prologue
    .line 76
    const-string/jumbo v1, "newRequest()"

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->log(Ljava/lang/String;)V

    .line 78
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->mClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->mSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->mTimingRepository:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    move-object v1, p1

    move-object v4, p0

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;-><init>(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;Z)V

    .line 89
    .local v0, "request":Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->getConnectTimeout()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->setConnectTimeout(I)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .line 90
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->getReadTimeout()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->setReadTimeout(I)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .line 91
    invoke-virtual {v0, p2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->addListener(Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;)V

    .line 92
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->start()V

    .line 94
    return-object v0
.end method

.method public readyForRecognition()Z
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x1

    return v0
.end method

.method public sendStatistics(Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;)V
    .locals 4
    .param p1, "stats"    # Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->isStatsEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    const-string/jumbo v1, "sendStatistics(): queuing stats"

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->log(Ljava/lang/String;)V

    .line 101
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;-><init>()V

    .line 102
    .local v0, "collection":Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;
    invoke-virtual {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;->addStatistics(Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;)V

    .line 103
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->mServerDetails:Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->mClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->mSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    invoke-virtual {v0, v1, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;->schedule(Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;)V

    .line 105
    .end local v0    # "collection":Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;
    :cond_0
    return-void
.end method

.method public sendStatsCollection(Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;)V
    .locals 3
    .param p1, "collection"    # Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->isAcceptedTextEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    const-string/jumbo v0, "sendStatsCollection(): queueing stats collection"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->log(Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->mServerDetails:Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->mClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->mSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    invoke-virtual {p1, v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;->schedule(Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;)V

    .line 113
    :cond_0
    return-void
.end method

.method setLastGuttID(Ljava/lang/String;)V
    .locals 0
    .param p1, "guttID"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->mLastGuttId:Ljava/lang/String;

    .line 122
    return-void
.end method

.method public setServer(Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;)V
    .locals 1
    .param p1, "serverDetails"    # Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;

    .prologue
    .line 51
    const-string/jumbo v0, "setServer()"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->log(Ljava/lang/String;)V

    .line 52
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->mServerDetails:Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;

    .line 53
    return-void
.end method
