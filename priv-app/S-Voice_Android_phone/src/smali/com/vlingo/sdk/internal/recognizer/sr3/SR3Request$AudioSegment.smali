.class Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;
.super Ljava/lang/Object;
.source "SR3Request.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AudioSegment"
.end annotation


# instance fields
.field audio:[B

.field length:I

.field offset:I


# direct methods
.method constructor <init>([BII)V
    .locals 0
    .param p1, "audio"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 783
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 784
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;->audio:[B

    .line 785
    iput p2, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;->offset:I

    .line 786
    iput p3, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;->length:I

    .line 787
    return-void
.end method
