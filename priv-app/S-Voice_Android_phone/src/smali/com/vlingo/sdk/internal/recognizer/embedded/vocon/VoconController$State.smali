.class public final enum Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;
.super Ljava/lang/Enum;
.source "VoconController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

.field public static final enum DESTROYED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

.field public static final enum IDLE:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

.field public static final enum INITIALIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

.field public static final enum RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

.field public static final enum RELEASING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

.field public static final enum UNINITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

.field public static final enum UPDATING_SLOTS:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 294
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    const-string/jumbo v1, "UNINITIALIZED"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->UNINITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    .line 295
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    const-string/jumbo v1, "INITIALIZING"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->INITIALIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    .line 296
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    const-string/jumbo v1, "IDLE"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->IDLE:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    .line 297
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    const-string/jumbo v1, "RECOGNIZING"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    .line 298
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    const-string/jumbo v1, "UPDATING_SLOTS"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->UPDATING_SLOTS:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    .line 299
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    const-string/jumbo v1, "RELEASING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->RELEASING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    .line 300
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    const-string/jumbo v1, "DESTROYED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->DESTROYED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    .line 293
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->UNINITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->INITIALIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->IDLE:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->UPDATING_SLOTS:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->RELEASING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->DESTROYED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->$VALUES:[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 293
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 293
    const-class v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;
    .locals 1

    .prologue
    .line 293
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->$VALUES:[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    invoke-virtual {v0}, [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    return-object v0
.end method
