.class public Lcom/vlingo/sdk/internal/crypto/SHA256;
.super Ljava/lang/Object;
.source "SHA256.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/crypto/HashFunction;


# static fields
.field private static final BLOCK_SIZE:I = 0x40

.field private static final HASH_SIZE:I = 0x20

.field private static final HEX_CHARS:[C

.field private static final K:[I


# instance fields
.field private bufOff:I

.field private final bufSha:[B

.field private final bufferSha:[I

.field private byteCount:J

.field private final context:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/vlingo/sdk/internal/crypto/SHA256;->HEX_CHARS:[C

    .line 32
    const/16 v0, 0x40

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/vlingo/sdk/internal/crypto/SHA256;->K:[I

    return-void

    .line 13
    nop

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x61s
        0x62s
        0x63s
        0x64s
        0x65s
        0x66s
    .end array-data

    .line 32
    :array_1
    .array-data 4
        0x428a2f98
        0x71374491
        -0x4a3f0431
        -0x164a245b
        0x3956c25b
        0x59f111f1
        -0x6dc07d5c    # -6.043E-28f
        -0x54e3a12b
        -0x27f85568
        0x12835b01
        0x243185be
        0x550c7dc3
        0x72be5d74
        -0x7f214e02
        -0x6423f959
        -0x3e640e8c
        -0x1b64963f
        -0x1041b87a
        0xfc19dc6
        0x240ca1cc
        0x2de92c6f
        0x4a7484aa    # 4006186.5f
        0x5cb0a9dc
        0x76f988da
        -0x67c1aeae
        -0x57ce3993
        -0x4ffcd838
        -0x40a68039
        -0x391ff40d
        -0x2a586eb9
        0x6ca6351
        0x14292967
        0x27b70a85
        0x2e1b2138
        0x4d2c6dfc    # 1.80805568E8f
        0x53380d13
        0x650a7354
        0x766a0abb
        -0x7e3d36d2
        -0x6d8dd37b
        -0x5d40175f
        -0x57e599b5
        -0x3db47490
        -0x3893ae5d
        -0x2e6d17e7
        -0x2966f9dc
        -0xbf1ca7b
        0x106aa070
        0x19a4c116
        0x1e376c08
        0x2748774c
        0x34b0bcb5
        0x391c0cb3
        0x4ed8aa4a    # 1.81751936E9f
        0x5b9cca4f
        0x682e6ff3
        0x748f82ee
        0x78a5636f
        -0x7b3787ec
        -0x7338fdf8
        -0x6f410006
        -0x5baf9315
        -0x41065c09
        -0x398e870e
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x40

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const/16 v0, 0x8

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    .line 68
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufferSha:[I

    .line 69
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufSha:[B

    .line 70
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/crypto/SHA256;->coreReset()V

    .line 71
    return-void
.end method

.method private final ch(III)I
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "z"    # I

    .prologue
    .line 149
    and-int v0, p1, p2

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v1, p3

    xor-int/2addr v0, v1

    return v0
.end method

.method private final mR(II)I
    .locals 1
    .param p1, "off"    # I
    .param p2, "x"    # I

    .prologue
    .line 158
    ushr-int v0, p2, p1

    return v0
.end method

.method private final mS(II)I
    .locals 2
    .param p1, "off"    # I
    .param p2, "x"    # I

    .prologue
    .line 159
    ushr-int v0, p2, p1

    rsub-int/lit8 v1, p1, 0x20

    shl-int v1, p2, v1

    or-int/2addr v0, v1

    return v0
.end method

.method private final mSig0(I)I
    .locals 2
    .param p1, "x"    # I

    .prologue
    .line 153
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lcom/vlingo/sdk/internal/crypto/SHA256;->mS(II)I

    move-result v0

    const/16 v1, 0xd

    invoke-direct {p0, v1, p1}, Lcom/vlingo/sdk/internal/crypto/SHA256;->mS(II)I

    move-result v1

    xor-int/2addr v0, v1

    const/16 v1, 0x16

    invoke-direct {p0, v1, p1}, Lcom/vlingo/sdk/internal/crypto/SHA256;->mS(II)I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method private final mSig1(I)I
    .locals 2
    .param p1, "x"    # I

    .prologue
    .line 154
    const/4 v0, 0x6

    invoke-direct {p0, v0, p1}, Lcom/vlingo/sdk/internal/crypto/SHA256;->mS(II)I

    move-result v0

    const/16 v1, 0xb

    invoke-direct {p0, v1, p1}, Lcom/vlingo/sdk/internal/crypto/SHA256;->mS(II)I

    move-result v1

    xor-int/2addr v0, v1

    const/16 v1, 0x19

    invoke-direct {p0, v1, p1}, Lcom/vlingo/sdk/internal/crypto/SHA256;->mS(II)I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method private final maj(III)I
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "z"    # I

    .prologue
    .line 151
    and-int v0, p1, p2

    and-int v1, p1, p3

    xor-int/2addr v0, v1

    and-int v1, p2, p3

    xor-int/2addr v0, v1

    return v0
.end method

.method private privateDigest([BIIZ)I
    .locals 10
    .param p1, "buf"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .param p4, "reset"    # Z

    .prologue
    const/4 v9, 0x0

    .line 164
    iget-object v5, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufSha:[B

    iget v6, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    const/16 v7, -0x80

    aput-byte v7, v5, v6

    .line 166
    const/16 v4, 0x8

    .line 167
    .local v4, "lenOfBitLen":I
    rsub-int/lit8 v2, v4, 0x40

    .line 168
    .local v2, "c":I
    iget v5, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    if-le v5, v2, :cond_1

    .line 169
    :goto_0
    iget v5, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    const/16 v6, 0x40

    if-ge v5, v6, :cond_0

    .line 170
    iget-object v5, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufSha:[B

    iget v6, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    aput-byte v9, v5, v6

    goto :goto_0

    .line 172
    :cond_0
    iget-object v5, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufSha:[B

    invoke-virtual {p0, v5, v9}, Lcom/vlingo/sdk/internal/crypto/SHA256;->coreUpdate([BI)V

    .line 173
    iput v9, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    .line 176
    :cond_1
    :goto_1
    iget v5, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    if-ge v5, v2, :cond_2

    .line 177
    iget-object v5, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufSha:[B

    iget v6, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    aput-byte v9, v5, v6

    goto :goto_1

    .line 179
    :cond_2
    iget-wide v5, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->byteCount:J

    const-wide/16 v7, 0x8

    mul-long v0, v5, v7

    .line 181
    .local v0, "bitCount":J
    const/16 v3, 0x38

    .local v3, "i":I
    :goto_2
    if-ltz v3, :cond_3

    .line 182
    iget-object v5, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufSha:[B

    iget v6, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    ushr-long v7, v0, v3

    long-to-int v7, v7

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 181
    add-int/lit8 v3, v3, -0x8

    goto :goto_2

    .line 184
    :cond_3
    iget-object v5, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufSha:[B

    invoke-virtual {p0, v5, v9}, Lcom/vlingo/sdk/internal/crypto/SHA256;->coreUpdate([BI)V

    .line 185
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/sdk/internal/crypto/SHA256;->coreDigest([BI)V

    .line 187
    if-eqz p4, :cond_4

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/crypto/SHA256;->coreReset()V

    .line 188
    :cond_4
    const/16 v5, 0x20

    return v5
.end method

.method private final sig0(I)I
    .locals 2
    .param p1, "x"    # I

    .prologue
    .line 155
    const/4 v0, 0x7

    invoke-direct {p0, v0, p1}, Lcom/vlingo/sdk/internal/crypto/SHA256;->mS(II)I

    move-result v0

    const/16 v1, 0x12

    invoke-direct {p0, v1, p1}, Lcom/vlingo/sdk/internal/crypto/SHA256;->mS(II)I

    move-result v1

    xor-int/2addr v0, v1

    const/4 v1, 0x3

    invoke-direct {p0, v1, p1}, Lcom/vlingo/sdk/internal/crypto/SHA256;->mR(II)I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method private final sig1(I)I
    .locals 2
    .param p1, "x"    # I

    .prologue
    .line 156
    const/16 v0, 0x11

    invoke-direct {p0, v0, p1}, Lcom/vlingo/sdk/internal/crypto/SHA256;->mS(II)I

    move-result v0

    const/16 v1, 0x13

    invoke-direct {p0, v1, p1}, Lcom/vlingo/sdk/internal/crypto/SHA256;->mS(II)I

    move-result v1

    xor-int/2addr v0, v1

    const/16 v1, 0xa

    invoke-direct {p0, v1, p1}, Lcom/vlingo/sdk/internal/crypto/SHA256;->mR(II)I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public static final toHex([B)Ljava/lang/String;
    .locals 6
    .param p0, "hash"    # [B

    .prologue
    .line 284
    array-length v4, p0

    mul-int/lit8 v4, v4, 0x2

    new-array v0, v4, [C

    .line 285
    .local v0, "buf":[C
    const/4 v1, 0x0

    .local v1, "i":I
    const/4 v2, 0x0

    .local v2, "x":I
    :goto_0
    array-length v4, p0

    if-ge v1, v4, :cond_0

    .line 286
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "x":I
    .local v3, "x":I
    sget-object v4, Lcom/vlingo/sdk/internal/crypto/SHA256;->HEX_CHARS:[C

    aget-byte v5, p0, v1

    ushr-int/lit8 v5, v5, 0x4

    and-int/lit8 v5, v5, 0xf

    aget-char v4, v4, v5

    aput-char v4, v0, v2

    .line 287
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "x":I
    .restart local v2    # "x":I
    sget-object v4, Lcom/vlingo/sdk/internal/crypto/SHA256;->HEX_CHARS:[C

    aget-byte v5, p0, v1

    and-int/lit8 v5, v5, 0xf

    aget-char v4, v4, v5

    aput-char v4, v0, v3

    .line 285
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 289
    :cond_0
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>([C)V

    return-object v4
.end method


# virtual methods
.method protected coreDigest([BI)V
    .locals 5
    .param p1, "buf"    # [B
    .param p2, "off"    # I

    .prologue
    .line 77
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 78
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    const/4 v2, 0x4

    if-ge v1, v2, :cond_0

    .line 79
    mul-int/lit8 v2, v0, 0x4

    rsub-int/lit8 v3, v1, 0x3

    add-int/2addr v2, v3

    add-int/2addr v2, p2

    iget-object v3, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    aget v3, v3, v0

    mul-int/lit8 v4, v1, 0x8

    ushr-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, p1, v2

    .line 78
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 77
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 80
    .end local v1    # "j":I
    :cond_1
    return-void
.end method

.method protected coreReset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 84
    iput v2, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    .line 85
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->byteCount:J

    .line 88
    iget-object v0, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const v1, 0x6a09e667

    aput v1, v0, v2

    .line 89
    iget-object v0, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v1, 0x1

    const v2, -0x4498517b

    aput v2, v0, v1

    .line 90
    iget-object v0, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v1, 0x2

    const v2, 0x3c6ef372

    aput v2, v0, v1

    .line 91
    iget-object v0, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v1, 0x3

    const v2, -0x5ab00ac6

    aput v2, v0, v1

    .line 92
    iget-object v0, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v1, 0x4

    const v2, 0x510e527f

    aput v2, v0, v1

    .line 93
    iget-object v0, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v1, 0x5

    const v2, -0x64fa9774

    aput v2, v0, v1

    .line 94
    iget-object v0, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v1, 0x6

    const v2, 0x1f83d9ab

    aput v2, v0, v1

    .line 95
    iget-object v0, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v1, 0x7

    const v2, 0x5be0cd19

    aput v2, v0, v1

    .line 96
    return-void
.end method

.method protected coreUpdate([BI)V
    .locals 17
    .param p1, "block"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 101
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufferSha:[I

    .line 104
    .local v13, "w":[I
    const/4 v9, 0x0

    .local v9, "i":I
    move/from16 v10, p2

    .end local p2    # "offset":I
    .local v10, "offset":I
    :goto_0
    const/16 v14, 0x10

    if-ge v9, v14, :cond_0

    .line 105
    add-int/lit8 p2, v10, 0x1

    .end local v10    # "offset":I
    .restart local p2    # "offset":I
    aget-byte v14, p1, v10

    shl-int/lit8 v14, v14, 0x18

    add-int/lit8 v10, p2, 0x1

    .end local p2    # "offset":I
    .restart local v10    # "offset":I
    aget-byte v15, p1, p2

    and-int/lit16 v15, v15, 0xff

    shl-int/lit8 v15, v15, 0x10

    or-int/2addr v14, v15

    add-int/lit8 p2, v10, 0x1

    .end local v10    # "offset":I
    .restart local p2    # "offset":I
    aget-byte v15, p1, v10

    and-int/lit16 v15, v15, 0xff

    shl-int/lit8 v15, v15, 0x8

    or-int/2addr v14, v15

    add-int/lit8 v10, p2, 0x1

    .end local p2    # "offset":I
    .restart local v10    # "offset":I
    aget-byte v15, p1, p2

    and-int/lit16 v15, v15, 0xff

    or-int/2addr v14, v15

    aput v14, v13, v9

    .line 104
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 111
    :cond_0
    const/16 v9, 0x10

    :goto_1
    const/16 v14, 0x40

    if-ge v9, v14, :cond_1

    .line 112
    add-int/lit8 v14, v9, -0x2

    aget v14, v13, v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/vlingo/sdk/internal/crypto/SHA256;->sig1(I)I

    move-result v14

    add-int/lit8 v15, v9, -0x7

    aget v15, v13, v15

    add-int/2addr v14, v15

    add-int/lit8 v15, v9, -0xf

    aget v15, v13, v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/vlingo/sdk/internal/crypto/SHA256;->sig0(I)I

    move-result v15

    add-int/2addr v14, v15

    add-int/lit8 v15, v9, -0x10

    aget v15, v13, v15

    add-int/2addr v14, v15

    aput v14, v13, v9

    .line 111
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 114
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v15, 0x0

    aget v1, v14, v15

    .line 115
    .local v1, "a":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v15, 0x1

    aget v2, v14, v15

    .line 116
    .local v2, "b":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v15, 0x2

    aget v3, v14, v15

    .line 117
    .local v3, "c":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v15, 0x3

    aget v4, v14, v15

    .line 118
    .local v4, "d":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v15, 0x4

    aget v5, v14, v15

    .line 119
    .local v5, "e":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v15, 0x5

    aget v6, v14, v15

    .line 120
    .local v6, "f":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v15, 0x6

    aget v7, v14, v15

    .line 121
    .local v7, "g":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v15, 0x7

    aget v8, v14, v15

    .line 124
    .local v8, "h":I
    const/4 v9, 0x0

    :goto_2
    const/16 v14, 0x40

    if-ge v9, v14, :cond_2

    .line 125
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/vlingo/sdk/internal/crypto/SHA256;->mSig1(I)I

    move-result v14

    add-int/2addr v14, v8

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6, v7}, Lcom/vlingo/sdk/internal/crypto/SHA256;->ch(III)I

    move-result v15

    add-int/2addr v14, v15

    sget-object v15, Lcom/vlingo/sdk/internal/crypto/SHA256;->K:[I

    aget v15, v15, v9

    add-int/2addr v14, v15

    aget v15, v13, v9

    add-int v11, v14, v15

    .line 126
    .local v11, "t1":I
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/crypto/SHA256;->mSig0(I)I

    move-result v14

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/sdk/internal/crypto/SHA256;->maj(III)I

    move-result v15

    add-int v12, v14, v15

    .line 127
    .local v12, "t2":I
    move v8, v7

    .line 128
    move v7, v6

    .line 129
    move v6, v5

    .line 130
    add-int v5, v4, v11

    .line 131
    move v4, v3

    .line 132
    move v3, v2

    .line 133
    move v2, v1

    .line 134
    add-int v1, v11, v12

    .line 124
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 138
    .end local v11    # "t1":I
    .end local v12    # "t2":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v15, 0x0

    aget v16, v14, v15

    add-int v16, v16, v1

    aput v16, v14, v15

    .line 139
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v15, 0x1

    aget v16, v14, v15

    add-int v16, v16, v2

    aput v16, v14, v15

    .line 140
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v15, 0x2

    aget v16, v14, v15

    add-int v16, v16, v3

    aput v16, v14, v15

    .line 141
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v15, 0x3

    aget v16, v14, v15

    add-int v16, v16, v4

    aput v16, v14, v15

    .line 142
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v15, 0x4

    aget v16, v14, v15

    add-int v16, v16, v5

    aput v16, v14, v15

    .line 143
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v15, 0x5

    aget v16, v14, v15

    add-int v16, v16, v6

    aput v16, v14, v15

    .line 144
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v15, 0x6

    aget v16, v14, v15

    add-int v16, v16, v7

    aput v16, v14, v15

    .line 145
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    const/4 v15, 0x7

    aget v16, v14, v15

    add-int v16, v16, v8

    aput v16, v14, v15

    .line 146
    return-void
.end method

.method public digest(Z[BI)V
    .locals 3
    .param p1, "reset"    # Z
    .param p2, "buffer"    # [B
    .param p3, "offset"    # I

    .prologue
    .line 263
    iget-object v0, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufSha:[B

    array-length v1, p2

    const/4 v2, 0x1

    invoke-direct {p0, v0, p3, v1, v2}, Lcom/vlingo/sdk/internal/crypto/SHA256;->privateDigest([BIIZ)I

    .line 264
    return-void
.end method

.method public digest()[B
    .locals 4

    .prologue
    const/16 v3, 0x20

    .line 253
    new-array v0, v3, [B

    .line 254
    .local v0, "tmp":[B
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v3, v2}, Lcom/vlingo/sdk/internal/crypto/SHA256;->privateDigest([BIIZ)I

    .line 255
    return-object v0
.end method

.method public final digestSize()I
    .locals 1

    .prologue
    .line 270
    const/16 v0, 0x100

    return v0
.end method

.method public extract([II)V
    .locals 3
    .param p1, "digest"    # [I
    .param p2, "offset"    # I

    .prologue
    .line 201
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 202
    add-int v1, v0, p2

    iget-object v2, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->context:[I

    aget v2, v2, v0

    aput v2, p1, v1

    .line 201
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 203
    :cond_0
    return-void
.end method

.method public hash(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/crypto/SHA256;->coreReset()V

    .line 275
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 277
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    int-to-byte v2, v2

    invoke-virtual {p0, v2}, Lcom/vlingo/sdk/internal/crypto/SHA256;->update(B)V

    .line 275
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 279
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/crypto/SHA256;->digest()[B

    move-result-object v0

    .line 280
    .local v0, "d":[B
    invoke-static {v0}, Lcom/vlingo/sdk/internal/crypto/SHA256;->toHex([B)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public update(B)V
    .locals 5
    .param p1, "b"    # B

    .prologue
    const/4 v4, 0x0

    .line 213
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->byteCount:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->byteCount:J

    .line 214
    iget-object v0, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufSha:[B

    iget v1, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    aput-byte p1, v0, v1

    .line 215
    iget v0, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    .line 216
    iget-object v0, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufSha:[B

    invoke-virtual {p0, v0, v4}, Lcom/vlingo/sdk/internal/crypto/SHA256;->coreUpdate([BI)V

    .line 217
    iput v4, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    .line 220
    :cond_0
    return-void
.end method

.method public update([B)V
    .locals 2
    .param p1, "data"    # [B

    .prologue
    .line 245
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/vlingo/sdk/internal/crypto/SHA256;->update([BII)V

    .line 246
    return-void
.end method

.method public update([BII)V
    .locals 6
    .param p1, "input"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    const/4 v5, 0x0

    .line 229
    iget-wide v1, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->byteCount:J

    int-to-long v3, p3

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->byteCount:J

    .line 232
    :goto_0
    iget v1, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    rsub-int/lit8 v0, v1, 0x40

    .local v0, "todo":I
    if-lt p3, v0, :cond_0

    .line 233
    iget-object v1, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufSha:[B

    iget v2, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 234
    iget-object v1, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufSha:[B

    invoke-virtual {p0, v1, v5}, Lcom/vlingo/sdk/internal/crypto/SHA256;->coreUpdate([BI)V

    .line 235
    sub-int/2addr p3, v0

    .line 236
    add-int/2addr p2, v0

    .line 237
    iput v5, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    goto :goto_0

    .line 240
    :cond_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufSha:[B

    iget v2, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    invoke-static {p1, p2, v1, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 241
    iget v1, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    add-int/2addr v1, p3

    iput v1, p0, Lcom/vlingo/sdk/internal/crypto/SHA256;->bufOff:I

    .line 242
    return-void
.end method
