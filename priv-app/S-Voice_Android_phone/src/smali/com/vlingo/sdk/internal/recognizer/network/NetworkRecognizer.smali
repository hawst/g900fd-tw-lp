.class public Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;
.super Ljava/lang/Object;
.source "NetworkRecognizer.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$1;,
        Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;
    }
.end annotation


# instance fields
.field private isInitialized:Z

.field private lastGuttID:Ljava/lang/String;

.field private mBeginStopDelta:I

.field private mEndStopDelta:I

.field private mGotResultDelta:I

.field private mGuttId:Ljava/lang/String;

.field private mListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

.field private mParseResultDelta:I

.field private mRequest:Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;

.field private mRequestListener:Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

.field private mSRManager:Lcom/vlingo/sdk/internal/recognizer/network/SRManager;

.field private mSendFinishDelta:I

.field private mSendStartDelta:I

.field private mStartDelta:I

.field private mStartTime:J

.field private mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

.field private mUttBytes:I


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->lastGuttID:Ljava/lang/String;

    .line 54
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    .line 55
    invoke-static {}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getInstance()Lcom/vlingo/sdk/internal/net/ConnectionManager;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/SRManager;->create(Lcom/vlingo/sdk/internal/net/ConnectionProvider;Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;)Lcom/vlingo/sdk/internal/recognizer/network/SRManager;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mSRManager:Lcom/vlingo/sdk/internal/recognizer/network/SRManager;

    .line 56
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mSRManager:Lcom/vlingo/sdk/internal/recognizer/network/SRManager;

    new-instance v1, Lcom/vlingo/sdk/internal/AndroidServerDetails;

    invoke-direct {v1}, Lcom/vlingo/sdk/internal/AndroidServerDetails;-><init>()V

    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    move-result-object v2

    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/network/SRManager;->init(Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;)V

    .line 57
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->isInitialized:Z

    .line 59
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;)Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mRequestListener:Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    return-object v0
.end method

.method static synthetic access$202(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mSendStartDelta:I

    return p1
.end method

.method static synthetic access$300(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;)Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mRequest:Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;)J
    .locals 2
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    .prologue
    .line 23
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mStartTime:J

    return-wide v0
.end method

.method static synthetic access$502(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mSendFinishDelta:I

    return p1
.end method

.method static synthetic access$602(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mGotResultDelta:I

    return p1
.end method

.method static synthetic access$702(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mParseResultDelta:I

    return p1
.end method

.method static synthetic access$800(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->handleResponse(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    return-void
.end method

.method static synthetic access$900(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;)Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    return-object v0
.end method

.method private handleResponse(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    .locals 3
    .param p1, "response"    # Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .prologue
    .line 180
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->getGUttId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mGuttId:Ljava/lang/String;

    .line 182
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->hasActions()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->hasResults()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->hasMessages()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->hasDialogState()Z

    move-result v0

    if-nez v0, :cond_1

    .line 184
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;->NO_RESULTS:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;

    const-string/jumbo v2, "No result. Please try again."

    invoke-interface {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;->onRecognizerError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V

    .line 188
    :goto_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mGuttId:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mGuttId:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->sendRecTiming(Ljava/lang/String;)V

    .line 191
    :cond_0
    return-void

    .line 186
    :cond_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;->onRecognitionResponse(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    goto :goto_0
.end method

.method private sendRecTiming(Ljava/lang/String;)V
    .locals 4
    .param p1, "guttId"    # Ljava/lang/String;

    .prologue
    .line 197
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;->TYPE_REC_TIMING:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    .local v0, "stats":Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;
    const-string/jumbo v1, "BOR"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mStartDelta:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;->addStatistic(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const-string/jumbo v1, "EOS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mBeginStopDelta:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;->addStatistic(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const-string/jumbo v1, "EOR"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mEndStopDelta:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;->addStatistic(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    const-string/jumbo v1, "Custom1"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mSendStartDelta:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;->addStatistic(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    const-string/jumbo v1, "EOD"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mSendFinishDelta:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;->addStatistic(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    const-string/jumbo v1, "SED"

    const-string/jumbo v2, "-1"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;->addStatistic(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const-string/jumbo v1, "RES"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mGotResultDelta:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;->addStatistic(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const-string/jumbo v1, "PAR"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mParseResultDelta:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;->addStatistic(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    const-string/jumbo v1, "UTT"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mUttBytes:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;->addStatistic(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const-string/jumbo v1, "Custom2"

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;->getStatString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;->addStatistic(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mSRManager:Lcom/vlingo/sdk/internal/recognizer/network/SRManager;

    invoke-virtual {v1, v0}, Lcom/vlingo/sdk/internal/recognizer/network/SRManager;->sendStatistics(Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;)V

    .line 222
    return-void
.end method


# virtual methods
.method public completeAudioRecognition()V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mRequest:Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;->finish()V

    .line 154
    return-void
.end method

.method public declared-synchronized destroy()V
    .locals 1

    .prologue
    .line 81
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mSRManager:Lcom/vlingo/sdk/internal/recognizer/network/SRManager;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/network/SRManager;->destroy()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    monitor-exit p0

    return-void

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getGuttId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mGuttId:Ljava/lang/String;

    return-object v0
.end method

.method public getTimingRepository()Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 292
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->isInitialized:Z

    return v0
.end method

.method public onCancelled()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 130
    invoke-static {}, Lcom/vlingo/sdk/internal/http/HttpManager;->getInstance()Lcom/vlingo/sdk/internal/http/HttpManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/HttpManager;->resume()V

    .line 132
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mRequest:Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mRequest:Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mRequestListener:Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    invoke-interface {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;->removeListener(Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;)V

    .line 134
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mRequest:Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;->cancel(Z)V

    .line 135
    iput-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mRequest:Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;

    .line 137
    :cond_0
    iput-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mRequestListener:Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    .line 138
    return-void
.end method

.method public onRecordingEnpointed()V
    .locals 4

    .prologue
    .line 175
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mStartTime:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mBeginStopDelta:I

    .line 176
    return-void
.end method

.method public onRecordingFinished()V
    .locals 4

    .prologue
    .line 165
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mStartTime:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mEndStopDelta:I

    .line 168
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mBeginStopDelta:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 169
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mEndStopDelta:I

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mBeginStopDelta:I

    .line 171
    :cond_0
    return-void
.end method

.method public onRecordingStarted()V
    .locals 4

    .prologue
    .line 158
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mStartTime:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mStartDelta:I

    .line 160
    invoke-static {}, Lcom/vlingo/sdk/internal/http/HttpManager;->getInstance()Lcom/vlingo/sdk/internal/http/HttpManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/HttpManager;->pause()V

    .line 161
    return-void
.end method

.method public prepareForRequest(Lcom/vlingo/sdk/internal/recognizer/SRContext;ZLcom/vlingo/sdk/internal/recognizer/RecognizerListener;)Z
    .locals 8
    .param p1, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .param p2, "sendAudio"    # Z
    .param p3, "listener"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v7, -0x1

    .line 95
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;->clear()V

    .line 96
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;->markTimeZero()V

    .line 97
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mStartTime:J

    .line 98
    iput v7, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mStartDelta:I

    .line 99
    iput v7, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mBeginStopDelta:I

    .line 100
    iput v7, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mEndStopDelta:I

    .line 101
    iput v7, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mSendStartDelta:I

    .line 102
    iput v7, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mSendFinishDelta:I

    .line 103
    iput v7, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mGotResultDelta:I

    .line 104
    iput v7, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mParseResultDelta:I

    .line 105
    iput v4, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mUttBytes:I

    .line 107
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 108
    .local v1, "context":Landroid/content/Context;
    const-string/jumbo v5, "connectivity"

    invoke-virtual {v1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 109
    .local v0, "conMgr":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 110
    .local v2, "netInfo":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v5

    sget-object v6, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-eq v5, v6, :cond_1

    .line 111
    :cond_0
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    sget-object v5, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;->FAIL_CONNECT:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;

    const-string/jumbo v6, "Network not available"

    invoke-interface {v3, v5, v6}, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;->onRecognizerError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V

    move v3, v4

    .line 122
    :goto_0
    return v3

    .line 114
    :cond_1
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mSRManager:Lcom/vlingo/sdk/internal/recognizer/network/SRManager;

    invoke-static {}, Lcom/vlingo/sdk/internal/settings/Settings;->getAsrConnectTimeout()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/vlingo/sdk/internal/recognizer/network/SRManager;->setConnectTimeout(I)V

    .line 115
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mSRManager:Lcom/vlingo/sdk/internal/recognizer/network/SRManager;

    invoke-static {}, Lcom/vlingo/sdk/internal/settings/Settings;->getAsrReadTimeout()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/vlingo/sdk/internal/recognizer/network/SRManager;->setReadTimeout(I)V

    .line 117
    new-instance v4, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$SRRequestListenerImpl;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer$1;)V

    iput-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mRequestListener:Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    .line 118
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mSRManager:Lcom/vlingo/sdk/internal/recognizer/network/SRManager;

    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mRequestListener:Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    invoke-virtual {v4, p1, v5, p2}, Lcom/vlingo/sdk/internal/recognizer/network/SRManager;->newRequest(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;Z)Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mRequest:Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;

    .line 120
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    const-string/jumbo v5, "RSC"

    invoke-virtual {v4, v5}, Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;->recordAndTimeStampEvent(Ljava/lang/String;)V

    .line 121
    iput-boolean v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->isInitialized:Z

    goto :goto_0
.end method

.method public processAudio([B)V
    .locals 3
    .param p1, "buffer"    # [B

    .prologue
    .line 147
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mRequest:Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;

    const/4 v1, 0x0

    array-length v2, p1

    invoke-interface {v0, p1, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;->sendAudio([BII)V

    .line 148
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mUttBytes:I

    array-length v1, p1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mUttBytes:I

    .line 149
    return-void
.end method

.method public declared-synchronized sendAcceptedText(Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;)V
    .locals 3
    .param p1, "gUttId"    # Ljava/lang/String;
    .param p2, "collection"    # Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;

    .prologue
    .line 66
    monitor-enter p0

    :try_start_0
    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;->getAcceptedText()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 68
    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->lastGuttID:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->lastGuttID:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 70
    :cond_0
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;->TYPE_ACCEPTED_TEXT:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    .local v0, "stats":Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;->STAT_ACCEPTED_TEXT:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;->getAcceptedText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;->addStatistic(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    invoke-virtual {p2, v0}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;->addStatistics(Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;)V

    .line 73
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->lastGuttID:Ljava/lang/String;

    .line 76
    .end local v0    # "stats":Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;
    :cond_1
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/NetworkRecognizer;->mSRManager:Lcom/vlingo/sdk/internal/recognizer/network/SRManager;

    invoke-virtual {v1, p2}, Lcom/vlingo/sdk/internal/recognizer/network/SRManager;->sendStatsCollection(Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    monitor-exit p0

    return-void

    .line 66
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
