.class Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;
.super Ljava/lang/Object;
.source "Recognizer.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/Recognizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DataReaderListenerImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)V
    .locals 0

    .prologue
    .line 337
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/Recognizer;
    .param p2, "x1"    # Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;

    .prologue
    .line 337
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;-><init>(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)V

    return-void
.end method


# virtual methods
.method public onDataAvailable([B[S)V
    .locals 3
    .param p1, "encodedBytes"    # [B
    .param p2, "rawShorts"    # [S

    .prologue
    .line 439
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    monitor-enter v1

    .line 440
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReaderListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$400(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;

    move-result-object v0

    if-eq p0, v0, :cond_0

    .line 441
    monitor-exit v1

    .line 467
    :goto_0
    return-void

    .line 446
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;->$SwitchMap$com$vlingo$sdk$recognition$RecognitionMode:[I

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$600(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/RecognitionMode;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 466
    :cond_1
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 448
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 449
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->processAudio([S)V

    goto :goto_1

    .line 453
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 454
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->processAudio([B)V

    goto :goto_1

    .line 458
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 459
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->processAudio([B)V

    .line 461
    :cond_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 462
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->processAudio([S)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 446
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onError(Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;Ljava/lang/String;)V
    .locals 4
    .param p1, "code"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 471
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    monitor-enter v1

    .line 472
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReaderListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$400(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;

    move-result-object v0

    if-eq p0, v0, :cond_0

    .line 473
    monitor-exit v1

    .line 480
    :goto_0
    return-void

    .line 478
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;->READER_ERROR:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;

    const/4 v3, 0x0

    # invokes: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->handleError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V
    invoke-static {v0, v2, p2, v3}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$900(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V

    .line 479
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onRMSDataAvailable(I)V
    .locals 3
    .param p1, "energy"    # I

    .prologue
    .line 484
    const/4 v0, -0x1

    if-le p1, v0, :cond_0

    .line 485
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->RMS_CHANGED:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->handleStateChange(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;Ljava/lang/Object;)V

    .line 487
    :cond_0
    return-void
.end method

.method public onStarted()V
    .locals 4

    .prologue
    .line 341
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    monitor-enter v1

    .line 342
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReaderListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$400(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;

    move-result-object v0

    if-eq p0, v0, :cond_0

    .line 343
    monitor-exit v1

    .line 371
    :goto_0
    return-void

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    const/4 v2, 0x0

    # setter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mIsStoppedDataReader:Z
    invoke-static {v0, v2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$502(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Z)Z

    .line 349
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;->$SwitchMap$com$vlingo$sdk$recognition$RecognitionMode:[I

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$600(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/RecognitionMode;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 369
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->LISTENING:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->handleStateChange(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;Ljava/lang/Object;)V

    .line 370
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 351
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 352
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->onRecordingStarted()V

    goto :goto_1

    .line 356
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 357
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->onRecordingStarted()V

    goto :goto_1

    .line 361
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 362
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->onRecordingStarted()V

    .line 364
    :cond_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 365
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->onRecordingStarted()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 349
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onStopped(IZ)V
    .locals 5
    .param p1, "totalDuration"    # I
    .param p2, "isSpeechDetected"    # Z

    .prologue
    .line 375
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    monitor-enter v1

    .line 376
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReaderListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$400(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;

    move-result-object v0

    if-eq p0, v0, :cond_0

    .line 377
    monitor-exit v1

    .line 435
    :goto_0
    return-void

    .line 382
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    const/4 v2, 0x1

    # setter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mIsStoppedDataReader:Z
    invoke-static {v0, v2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$502(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Z)Z

    .line 383
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;->$SwitchMap$com$vlingo$sdk$recognition$RecognitionMode:[I

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$600(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/RecognitionMode;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 404
    :cond_1
    :goto_1
    if-nez p2, :cond_4

    .line 407
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;->NO_SPEECH:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;

    const-string/jumbo v3, "No speech detected"

    const/4 v4, 0x0

    # invokes: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->handleError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V
    invoke-static {v0, v2, v3, v4}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$900(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V

    .line 434
    :cond_2
    :goto_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 385
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 386
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->onRecordingFinished()V

    goto :goto_1

    .line 390
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 391
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->onRecordingFinished()V

    goto :goto_1

    .line 395
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 396
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->onRecordingFinished()V

    .line 398
    :cond_3
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 399
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->onRecordingFinished()V

    goto :goto_1

    .line 408
    :cond_4
    const/4 v0, -0x1

    if-le p1, v0, :cond_5

    const/16 v0, 0x1f4

    if-ge p1, v0, :cond_5

    .line 409
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;->TOO_SHORT:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;

    const-string/jumbo v3, "Audio too short. Please try again."

    const/4 v4, 0x0

    # invokes: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->handleError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V
    invoke-static {v0, v2, v3, v4}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$900(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V

    goto :goto_2

    .line 411
    :cond_5
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;->THINKING:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->handleStateChange(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;Ljava/lang/Object;)V

    .line 413
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;->$SwitchMap$com$vlingo$sdk$recognition$RecognitionMode:[I

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$600(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/RecognitionMode;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_2

    .line 415
    :pswitch_3
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 416
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->completeAudioRecognition()V

    goto/16 :goto_2

    .line 420
    :pswitch_4
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 421
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->completeAudioRecognition()V

    goto/16 :goto_2

    .line 425
    :pswitch_5
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 426
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->completeAudioRecognition()V

    .line 428
    :cond_6
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 429
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$DataReaderListenerImpl;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mEmbeddedRecognizer:Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$700(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/IEmbeddedRecognizer;->completeAudioRecognition()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 383
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 413
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
