.class Lcom/vlingo/sdk/internal/VLDownloadServiceImpl$1;
.super Landroid/content/BroadcastReceiver;
.source "VLDownloadServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;-><init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 60
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 61
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v2, "extra_download_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p2, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 64
    .local v1, "actionId":Ljava/lang/Long;
    const-string/jumbo v2, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->downloadsMap:Ljava/util/concurrent/ConcurrentMap;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->access$000(Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 66
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;

    const/4 v3, 0x0

    # invokes: Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->processDownloadId(Ljava/lang/Long;Lcom/vlingo/sdk/filedownload/VLDownloadListener;)Z
    invoke-static {v2, v1, v3}, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;->access$100(Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;Ljava/lang/Long;Lcom/vlingo/sdk/filedownload/VLDownloadListener;)Z

    .line 68
    :cond_0
    return-void
.end method
