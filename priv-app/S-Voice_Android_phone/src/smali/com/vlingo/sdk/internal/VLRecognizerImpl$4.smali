.class Lcom/vlingo/sdk/internal/VLRecognizerImpl$4;
.super Ljava/lang/Object;
.source "VLRecognizerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/sdk/internal/VLRecognizerImpl;->cancelRecognition()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)V
    .locals 0

    .prologue
    .line 312
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$4;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 315
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$4;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    monitor-enter v1

    .line 316
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$4;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->isValid()Z

    move-result v0

    if-nez v0, :cond_0

    .line 317
    monitor-exit v1

    .line 321
    :goto_0
    return-void

    .line 319
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$4;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecognizer:Lcom/vlingo/sdk/internal/recognizer/Recognizer;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$600(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->cancel()V

    .line 320
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
