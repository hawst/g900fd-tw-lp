.class public Lcom/vlingo/sdk/internal/http/custom/HttpResponse;
.super Ljava/lang/Object;
.source "HttpResponse.java"


# instance fields
.field private ivClientDin:Ljava/io/InputStream;

.field private ivCode:Ljava/lang/String;

.field private ivHeaders:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ivInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

.field private ivMessage:Ljava/lang/String;

.field private ivProtocol:Ljava/lang/String;

.field private ivReadHeaders:Z


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;)V
    .locals 1
    .param p1, "interaction"    # Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ivReadHeaders:Z

    .line 30
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ivHeaders:Ljava/util/Hashtable;

    .line 40
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ivInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    .line 41
    return-void
.end method

.method private ensureHeaders()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    iget-boolean v1, p0, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ivReadHeaders:Z

    if-nez v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ivInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getHTTPConnection()Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->getInputStream(Lcom/vlingo/sdk/internal/http/custom/HttpResponse;)Ljava/io/DataInputStream;

    move-result-object v0

    .line 88
    .local v0, "din":Ljava/io/DataInputStream;
    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->setupInputStream(Ljava/io/InputStream;)V

    .line 90
    .end local v0    # "din":Ljava/io/DataInputStream;
    :cond_0
    return-void
.end method

.method private setupInputStream(Ljava/io/InputStream;)V
    .locals 5
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    iget-object v3, p0, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ivHeaders:Ljava/util/Hashtable;

    const-string/jumbo v4, "Transfer-Encoding"

    invoke-virtual {v3, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 100
    .local v2, "transferEncoding":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 102
    iget-object v3, p0, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ivHeaders:Ljava/util/Hashtable;

    const-string/jumbo v4, "Content-Length"

    invoke-virtual {v3, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 103
    .local v0, "contentLength":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 105
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 106
    .local v1, "length":I
    new-instance v3, Lcom/vlingo/sdk/internal/http/custom/LimitInputStream;

    invoke-direct {v3, p1, v1}, Lcom/vlingo/sdk/internal/http/custom/LimitInputStream;-><init>(Ljava/io/InputStream;I)V

    iput-object v3, p0, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ivClientDin:Ljava/io/InputStream;

    .line 117
    .end local v0    # "contentLength":Ljava/lang/String;
    .end local v1    # "length":I
    :goto_0
    return-void

    .line 109
    .restart local v0    # "contentLength":Ljava/lang/String;
    :cond_0
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ivClientDin:Ljava/io/InputStream;

    goto :goto_0

    .line 115
    .end local v0    # "contentLength":Ljava/lang/String;
    :cond_1
    new-instance v3, Lcom/vlingo/sdk/internal/http/custom/ChunkedInputStream;

    invoke-direct {v3, p1}, Lcom/vlingo/sdk/internal/http/custom/ChunkedInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v3, p0, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ivClientDin:Ljava/io/InputStream;

    goto :goto_0
.end method


# virtual methods
.method public finish()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ivInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getHTTPConnection()Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->notifyResponseDone(Lcom/vlingo/sdk/internal/http/custom/HttpResponse;)V

    .line 130
    return-void
.end method

.method public getCode()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ensureHeaders()V

    .line 47
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ivCode:Ljava/lang/String;

    return-object v0
.end method

.method public getHeaderNames()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<*>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ensureHeaders()V

    .line 68
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ivHeaders:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getHeaderValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ensureHeaders()V

    .line 75
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ivHeaders:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ensureHeaders()V

    .line 122
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ivClientDin:Ljava/io/InputStream;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ensureHeaders()V

    .line 54
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ivMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ensureHeaders()V

    .line 61
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ivProtocol:Ljava/lang/String;

    return-object v0
.end method

.method public readHeaders()Ljava/util/Hashtable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->ivHeaders:Ljava/util/Hashtable;

    return-object v0
.end method
