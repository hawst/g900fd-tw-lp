.class public Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;
.super Ljava/lang/Object;
.source "ThresholdInfo.java"


# instance fields
.field private highThreshold:I

.field private midThreshold:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getHighThreshold()I
    .locals 1

    .prologue
    .line 12
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;->highThreshold:I

    return v0
.end method

.method public getMidThreshold()I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;->midThreshold:I

    return v0
.end method

.method public setHighThreshold(I)V
    .locals 0
    .param p1, "highThreshold"    # I

    .prologue
    .line 16
    iput p1, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;->highThreshold:I

    .line 17
    return-void
.end method

.method public setMidThreshold(I)V
    .locals 0
    .param p1, "midThreshold"    # I

    .prologue
    .line 24
    iput p1, p0, Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;->midThreshold:I

    .line 25
    return-void
.end method
