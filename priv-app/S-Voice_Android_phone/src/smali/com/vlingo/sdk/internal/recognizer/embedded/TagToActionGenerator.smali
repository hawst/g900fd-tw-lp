.class public final Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;
.super Ljava/lang/Object;
.source "TagToActionGenerator.java"


# static fields
.field private static final SETTINGS_FOLDER:Ljava/lang/String; = "tta_rules"

.field private static final TEMPLATE_FILE_NAME_PATTERN:Ljava/lang/String; = "template"

.field private static final UTT_PARAM_NAME:Ljava/lang/String; = "utt"

.field private static instance:Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;


# instance fields
.field private rules:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;-><init>()V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->instance:Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->rules:Ljava/util/concurrent/ConcurrentMap;

    .line 49
    return-void
.end method

.method private addLoadedRule(Ljava/util/Map;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;)V
    .locals 3
    .param p2, "rule"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;",
            ">;>;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;",
            ")V"
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "rulesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;>;>;"
    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;->getFieldId()Ljava/lang/String;

    move-result-object v1

    .line 100
    .local v1, "fieldId":Ljava/lang/String;
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 101
    .local v0, "alreadyLoadedRules":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;>;"
    if-nez v0, :cond_0

    .line 102
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "alreadyLoadedRules":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 104
    .restart local v0    # "alreadyLoadedRules":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;>;"
    :cond_0
    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;->getTagNames()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->generateTagsNamesKey(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    return-void
.end method

.method private extractRule(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;Ljava/util/Map;)Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;
    .locals 4
    .param p1, "abstractRule"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;",
            ">;)",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;"
        }
    .end annotation

    .prologue
    .line 85
    .local p2, "templates":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;>;"
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;->getTemplateName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 86
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;->getTemplateName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;

    .line 87
    .local v0, "result":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;
    if-nez v0, :cond_0

    .line 88
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "No template with name:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;->getTemplateName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 90
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;->getArbitrationInfoMap()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;->setArbitrationInfoMap(Ljava/util/Map;)V

    .line 91
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;->getFieldId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;->setFieldId(Ljava/lang/String;)V

    .line 95
    .end local v0    # "result":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;
    .end local p1    # "abstractRule":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;
    :goto_0
    return-object v0

    .restart local p1    # "abstractRule":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;
    :cond_1
    check-cast p1, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;

    .end local p1    # "abstractRule":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;
    move-object v0, p1

    goto :goto_0
.end method

.method private generateAction(Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;)Lcom/vlingo/sdk/internal/vlservice/response/Action;
    .locals 7
    .param p1, "action"    # Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;

    .prologue
    .line 209
    new-instance v5, Lcom/vlingo/sdk/internal/vlservice/response/Action;

    invoke-direct {v5}, Lcom/vlingo/sdk/internal/vlservice/response/Action;-><init>()V

    .line 210
    .local v5, "vlAction":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;->getName()Ljava/lang/String;

    move-result-object v1

    .line 211
    .local v1, "name":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 212
    invoke-virtual {v5, v1}, Lcom/vlingo/sdk/internal/vlservice/response/Action;->setName(Ljava/lang/String;)V

    .line 213
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;->getParameterNames()Ljava/util/Set;

    move-result-object v3

    .line 214
    .local v3, "paramNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 215
    .local v2, "paramName":Ljava/lang/String;
    invoke-virtual {p1, v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 216
    .local v4, "paramValue":Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-static {v4}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 217
    const/4 v6, 0x0

    invoke-virtual {v5, v2, v4, v6}, Lcom/vlingo/sdk/internal/vlservice/response/Action;->addParameter(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;)V

    goto :goto_0

    .line 221
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "paramName":Ljava/lang/String;
    .end local v3    # "paramNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v4    # "paramValue":Ljava/lang/String;
    :cond_1
    return-object v5
.end method

.method private generateActionList(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;)Ljava/util/List;
    .locals 12
    .param p1, "voconResult"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;
    .param p2, "rule"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    .line 225
    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;->getActionsList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    invoke-direct {v5, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 226
    .local v5, "result":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;>;"
    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;->getActionsList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;

    .line 227
    .local v0, "actionRule":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;
    new-instance v7, Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;-><init>(Ljava/lang/String;)V

    .line 228
    .local v7, "vAction":Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;->getParameterNames()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 229
    .local v3, "paramName":Ljava/lang/String;
    invoke-virtual {v0, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 230
    .local v4, "paramValue":Ljava/lang/String;
    const-string/jumbo v9, "["

    invoke-virtual {v4, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    const-string/jumbo v9, "]"

    invoke-virtual {v4, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 231
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v4, v11, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 232
    .local v6, "searchName":Ljava/lang/String;
    const-string/jumbo v9, "utt"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {p1, v11}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->getRecognizedUtterance(Z)Ljava/lang/String;

    move-result-object v8

    .line 234
    .local v8, "vlActionParamValue":Ljava/lang/String;
    :goto_2
    invoke-virtual {v7, v3, v8}, Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 232
    .end local v8    # "vlActionParamValue":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1, v6}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->getTag(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconTagResult;->getRecognizedUtterance(Z)Ljava/lang/String;

    move-result-object v8

    goto :goto_2

    .line 236
    .end local v6    # "searchName":Ljava/lang/String;
    :cond_1
    invoke-virtual {v7, v3, v4}, Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 239
    .end local v3    # "paramName":Ljava/lang/String;
    .end local v4    # "paramValue":Ljava/lang/String;
    :cond_2
    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 241
    .end local v0    # "actionRule":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v7    # "vAction":Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;
    :cond_3
    return-object v5
.end method

.method private generateSRRecognitionResponse(Ljava/util/List;)Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;",
            ">;)",
            "Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;"
        }
    .end annotation

    .prologue
    .line 198
    .local p1, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;>;"
    new-instance v2, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    invoke-direct {v2}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;-><init>()V

    .line 199
    .local v2, "recognitionResponse":Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 200
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;

    .line 201
    .local v0, "action":Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;
    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->generateAction(Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;)Lcom/vlingo/sdk/internal/vlservice/response/Action;

    move-result-object v3

    .line 202
    .local v3, "vlAction":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    invoke-virtual {v2, v3}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->addAction(Lcom/vlingo/sdk/internal/vlservice/response/Action;)V

    goto :goto_0

    .line 205
    .end local v0    # "action":Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "vlAction":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    :cond_0
    return-object v2
.end method

.method public static generateTagsNamesKey(Ljava/util/List;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 245
    .local p0, "tagNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Ljava/util/Collections;->reverseOrder()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 246
    const-string/jumbo v0, ","

    invoke-static {v0, p0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;
    .locals 2

    .prologue
    .line 52
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 53
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Should be run on worker thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->instance:Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;

    return-object v0
.end method

.method private loadRuleSets()V
    .locals 8

    .prologue
    .line 65
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 66
    .local v4, "rulesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;>;>;"
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 67
    .local v5, "templates":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 68
    .local v1, "abstractRules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;>;"
    invoke-direct {p0, v5, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->loadRulesFromFiles(Ljava/util/Map;Ljava/util/List;)V

    .line 70
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;

    .line 71
    .local v0, "abstractRule":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;
    invoke-direct {p0, v0, v5}, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->extractRule(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;Ljava/util/Map;)Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;

    move-result-object v3

    .line 72
    .local v3, "rule":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;
    invoke-direct {p0, v4, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->addLoadedRule(Ljava/util/Map;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;)V

    goto :goto_0

    .line 75
    .end local v0    # "abstractRule":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;
    .end local v3    # "rule":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;
    :cond_0
    iget-object v7, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->rules:Ljava/util/concurrent/ConcurrentMap;

    monitor-enter v7

    .line 76
    :try_start_0
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->rules:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v6}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    .line 77
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->rules:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v6, v4}, Ljava/util/concurrent/ConcurrentMap;->putAll(Ljava/util/Map;)V

    .line 78
    monitor-exit v7

    .line 82
    return-void

    .line 78
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method

.method private loadRulesFromFiles(Ljava/util/Map;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 109
    .local p1, "templates":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;>;"
    .local p2, "rules":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;>;"
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    .line 111
    .local v1, "assetManager":Landroid/content/res/AssetManager;
    :try_start_0
    const-string/jumbo v8, "tta_rules"

    invoke-virtual {v1, v8}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v6, :cond_1

    aget-object v3, v0, v4

    .line 112
    .local v3, "fileName":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "tta_rules/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    invoke-virtual {v1, v8, v9}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;

    move-result-object v5

    .line 114
    .local v5, "inputStream":Ljava/io/InputStream;
    const-string/jumbo v8, "template"

    invoke-virtual {v3, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 115
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;->getTemplatesMapBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser;->parse(Ljava/io/InputStream;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map;

    invoke-interface {p1, v8}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 111
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 118
    :cond_0
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser;->getTagToActionBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsXmlParser;->parse(Ljava/io/InputStream;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Collection;

    invoke-interface {p2, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 122
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v3    # "fileName":Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v5    # "inputStream":Ljava/io/InputStream;
    .end local v6    # "len$":I
    :catch_0
    move-exception v2

    .line 125
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 131
    .end local v2    # "e":Ljava/io/IOException;
    :cond_1
    :goto_2
    return-void

    .line 126
    :catch_1
    move-exception v7

    .line 129
    .local v7, "xppe":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v7}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto :goto_2
.end method


# virtual methods
.method public getRecognitionResult(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;
    .locals 8
    .param p1, "voconResult"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;
    .param p2, "fieldId"    # Ljava/lang/String;
    .param p3, "lang"    # Ljava/lang/String;

    .prologue
    .line 150
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->rules:Ljava/util/concurrent/ConcurrentMap;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->rules:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v6}, Ljava/util/concurrent/ConcurrentMap;->size()I

    move-result v6

    if-nez v6, :cond_1

    .line 153
    :cond_0
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string/jumbo v7, "No tta rules loaded. Based on vocon regontion result action list can not be generated"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 157
    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 160
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v7, "Field Id shouldn\'t be empty. Based on vocon regontion result action list can not be generated"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 164
    :cond_2
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconNBestResult;->getBest()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;

    move-result-object v1

    .line 165
    .local v1, "bestItem":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;
    if-nez v1, :cond_3

    .line 168
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v7, "Vocon recognition result is empty. Action list can not be generated"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 172
    :cond_3
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->rules:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v6, p2}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map;

    .line 173
    .local v5, "rulesByFieldId":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;>;"
    if-eqz v5, :cond_4

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v6

    if-nez v6, :cond_5

    .line 176
    :cond_4
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string/jumbo v7, "No rule for recognition result. Action list can not be generated"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 179
    :cond_5
    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->getAllTagNamesList()Ljava/util/List;

    move-result-object v6

    invoke-static {v6}, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->generateTagsNamesKey(Ljava/util/List;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;

    .line 180
    .local v4, "rule":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;
    if-nez v4, :cond_6

    .line 183
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string/jumbo v7, "No rule for recognition result. Action list can not be generated"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 186
    :cond_6
    new-instance v3, Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;

    invoke-direct {v3}, Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;-><init>()V

    .line 187
    .local v3, "result":Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;
    invoke-virtual {v4, p3}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;->getArbitrationInfo(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;->setThresholdInfo(Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;)V

    .line 188
    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;->getConfidence()Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;->setConfidenceScore(Ljava/lang/Long;)V

    .line 189
    invoke-direct {p0, v1, v4}, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->generateActionList(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/results/VoconHypothesisResult;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;)Ljava/util/List;

    move-result-object v0

    .line 190
    .local v0, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/internal/recognizer/embedded/VoconAction;>;"
    invoke-virtual {v3, v0}, Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;->setEmbeddedActionsList(Ljava/util/List;)V

    .line 191
    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->generateSRRecognitionResponse(Ljava/util/List;)Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    move-result-object v2

    .line 192
    .local v2, "response":Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;
    invoke-virtual {v3, v2}, Lcom/vlingo/sdk/internal/recognizer/arbiter/VoconRecognitionResult;->setResult(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    .line 194
    return-object v3
.end method

.method public initialize()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/TagToActionGenerator;->loadRuleSets()V

    .line 60
    return-void
.end method
