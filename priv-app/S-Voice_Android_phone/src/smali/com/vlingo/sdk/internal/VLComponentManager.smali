.class public Lcom/vlingo/sdk/internal/VLComponentManager;
.super Ljava/lang/Object;
.source "VLComponentManager.java"


# instance fields
.field private mComponents:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/sdk/internal/VLComponentImpl;",
            ">;",
            "Lcom/vlingo/sdk/VLComponent;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLComponentManager;->mHandler:Landroid/os/Handler;

    .line 35
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLComponentManager;->mComponents:Ljava/util/concurrent/ConcurrentHashMap;

    .line 36
    return-void
.end method


# virtual methods
.method public destroyAll()V
    .locals 4

    .prologue
    .line 112
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLComponentManager;->mComponents:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    .line 113
    .local v2, "key":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/sdk/internal/VLComponentImpl;>;"
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLComponentManager;->mComponents:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/VLComponent;

    .line 118
    .local v0, "c":Lcom/vlingo/sdk/VLComponent;
    invoke-interface {v0}, Lcom/vlingo/sdk/VLComponent;->destroy()V

    goto :goto_0

    .line 120
    .end local v0    # "c":Lcom/vlingo/sdk/VLComponent;
    .end local v2    # "key":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/sdk/internal/VLComponentImpl;>;"
    :cond_0
    return-void
.end method

.method public destroySettingsDependentComponents()V
    .locals 4

    .prologue
    .line 126
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLComponentManager;->mComponents:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    .line 127
    .local v2, "key":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/sdk/internal/VLComponentImpl;>;"
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLComponentManager;->mComponents:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/VLComponent;

    .line 128
    .local v0, "c":Lcom/vlingo/sdk/VLComponent;
    instance-of v3, v0, Lcom/vlingo/sdk/internal/VLComponentImpl;

    if-eqz v3, :cond_1

    move-object v3, v0

    .line 129
    check-cast v3, Lcom/vlingo/sdk/internal/VLComponentImpl;

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/VLComponentImpl;->doesDependOnSettings()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 133
    :cond_1
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLComponentManager;->mComponents:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    invoke-interface {v0}, Lcom/vlingo/sdk/VLComponent;->destroy()V

    goto :goto_0

    .line 140
    .end local v0    # "c":Lcom/vlingo/sdk/VLComponent;
    .end local v2    # "key":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/sdk/internal/VLComponentImpl;>;"
    :cond_2
    return-void
.end method

.method getComponent(Ljava/lang/Class;)Lcom/vlingo/sdk/VLComponent;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/sdk/internal/VLComponentImpl;",
            ">;)",
            "Lcom/vlingo/sdk/VLComponent;"
        }
    .end annotation

    .prologue
    .line 78
    .local p1, "componentClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/sdk/internal/VLComponentImpl;>;"
    iget-object v4, p0, Lcom/vlingo/sdk/internal/VLComponentManager;->mComponents:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/VLComponent;

    .line 79
    .local v1, "component":Lcom/vlingo/sdk/VLComponent;
    if-nez v1, :cond_0

    .line 84
    const/4 v4, 0x2

    :try_start_0
    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-class v6, Landroid/os/Handler;

    aput-object v6, v4, v5

    invoke-virtual {p1, v4}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    .line 85
    .local v2, "componentConst":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<+Lcom/vlingo/sdk/internal/VLComponentImpl;>;"
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/vlingo/sdk/internal/VLComponentManager;->mHandler:Landroid/os/Handler;

    aput-object v6, v4, v5

    invoke-virtual {v2, v4}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/vlingo/sdk/VLComponent;

    move-object v1, v0

    .line 86
    iget-object v4, p0, Lcom/vlingo/sdk/internal/VLComponentManager;->mComponents:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    .end local v2    # "componentConst":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<+Lcom/vlingo/sdk/internal/VLComponentImpl;>;"
    :cond_0
    :goto_0
    return-object v1

    .line 88
    :catch_0
    move-exception v3

    .line 91
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getDownloadService()Lcom/vlingo/sdk/filedownload/VLDownloadService;
    .locals 1

    .prologue
    .line 71
    const-class v0, Lcom/vlingo/sdk/internal/VLDownloadServiceImpl;

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->getComponent(Ljava/lang/Class;)Lcom/vlingo/sdk/VLComponent;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/filedownload/VLDownloadService;

    return-object v0
.end method

.method public getEmbeddedDialogManager()Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/vlingo/sdk/internal/VLEmbeddedDialogManagerImpl;

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->getComponent(Ljava/lang/Class;)Lcom/vlingo/sdk/VLComponent;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;

    return-object v0
.end method

.method public getEmbeddedTrainer()Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainer;
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/vlingo/sdk/internal/VLEmbeddedTrainerImpl;

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->getComponent(Ljava/lang/Class;)Lcom/vlingo/sdk/VLComponent;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainer;

    return-object v0
.end method

.method public getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->getComponent(Ljava/lang/Class;)Lcom/vlingo/sdk/VLComponent;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/recognition/VLRecognizer;

    return-object v0
.end method

.method public getSpeechDetector()Lcom/vlingo/sdk/recognition/speech/VLSpeechDetector;
    .locals 1

    .prologue
    .line 67
    const-class v0, Lcom/vlingo/sdk/internal/VLSpeechDetectorImpl;

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->getComponent(Ljava/lang/Class;)Lcom/vlingo/sdk/VLComponent;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetector;

    return-object v0
.end method

.method public getSpotter()Lcom/vlingo/sdk/recognition/spotter/VLSpotter;
    .locals 1

    .prologue
    .line 63
    const-class v0, Lcom/vlingo/sdk/internal/VLSpotterSensory2Impl;

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->getComponent(Ljava/lang/Class;)Lcom/vlingo/sdk/VLComponent;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/recognition/spotter/VLSpotter;

    return-object v0
.end method

.method public getTextToSpeech()Lcom/vlingo/sdk/tts/VLTextToSpeech;
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->getComponent(Ljava/lang/Class;)Lcom/vlingo/sdk/VLComponent;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/tts/VLTextToSpeech;

    return-object v0
.end method

.method public getTrainer()Lcom/vlingo/sdk/training/VLTrainer;
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/vlingo/sdk/internal/VLTrainerImpl;

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->getComponent(Ljava/lang/Class;)Lcom/vlingo/sdk/VLComponent;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/training/VLTrainer;

    return-object v0
.end method

.method public getVLServices()Lcom/vlingo/sdk/services/VLServices;
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/vlingo/sdk/internal/VLServicesImpl;

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->getComponent(Ljava/lang/Class;)Lcom/vlingo/sdk/VLComponent;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/services/VLServices;

    return-object v0
.end method

.method removeComponent(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/sdk/internal/VLComponentImpl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 105
    .local p1, "componentClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/sdk/internal/VLComponentImpl;>;"
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLComponentManager;->mComponents:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    return-void
.end method
