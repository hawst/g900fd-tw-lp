.class public final Lcom/vlingo/sdk/internal/VLSpeechDetectorImpl;
.super Lcom/vlingo/sdk/internal/VLComponentImpl;
.source "VLSpeechDetectorImpl.java"

# interfaces
.implements Lcom/vlingo/sdk/recognition/speech/VLSpeechDetector;


# instance fields
.field private mIsStarted:Z

.field private mPrevLastSpeechSample:I

.field private mVLSilJNI:Lcom/vlingo/sdk/internal/audio/VLSilJNI;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V
    .locals 1
    .param p1, "manager"    # Lcom/vlingo/sdk/internal/VLComponentManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/VLComponentImpl;-><init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V

    .line 33
    invoke-static {}, Lcom/vlingo/sdk/internal/audio/VLSilJNI;->init()V

    .line 34
    new-instance v0, Lcom/vlingo/sdk/internal/audio/VLSilJNI;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/audio/VLSilJNI;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLSpeechDetectorImpl;->mVLSilJNI:Lcom/vlingo/sdk/internal/audio/VLSilJNI;

    .line 35
    return-void
.end method


# virtual methods
.method public bridge synthetic destroy()V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0}, Lcom/vlingo/sdk/internal/VLComponentImpl;->destroy()V

    return-void
.end method

.method public doesDependOnSettings()Z
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic isValid()Z
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/vlingo/sdk/internal/VLComponentImpl;->isValid()Z

    move-result v0

    return v0
.end method

.method onDestroy()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLSpeechDetectorImpl;->mVLSilJNI:Lcom/vlingo/sdk/internal/audio/VLSilJNI;

    .line 46
    return-void
.end method

.method public declared-synchronized processShortArray([SII)Z
    .locals 4
    .param p1, "audioData"    # [S
    .param p2, "offsetInShorts"    # I
    .param p3, "sizeInShorts"    # I

    .prologue
    .line 88
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLSpeechDetectorImpl;->validateInstance()V

    .line 90
    iget-boolean v2, p0, Lcom/vlingo/sdk/internal/VLSpeechDetectorImpl;->mIsStarted:Z

    if-nez v2, :cond_0

    .line 91
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "Cannot call processShortArray when SpeechDetector is not started."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 94
    :cond_0
    if-lez p2, :cond_1

    .line 95
    add-int v2, p2, p3

    :try_start_1
    invoke-static {p1, p2, v2}, Ljava/util/Arrays;->copyOfRange([SII)[S

    move-result-object p1

    .line 98
    :cond_1
    const/4 v1, 0x0

    .line 99
    .local v1, "speechDetected":Z
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLSpeechDetectorImpl;->mVLSilJNI:Lcom/vlingo/sdk/internal/audio/VLSilJNI;

    invoke-virtual {v2, p1, p3}, Lcom/vlingo/sdk/internal/audio/VLSilJNI;->processShortArray([SI)I

    move-result v0

    .line 101
    .local v0, "lastSpeechSample":I
    iget v2, p0, Lcom/vlingo/sdk/internal/VLSpeechDetectorImpl;->mPrevLastSpeechSample:I

    if-le v0, v2, :cond_2

    .line 102
    const/4 v1, 0x1

    .line 104
    :cond_2
    iput v0, p0, Lcom/vlingo/sdk/internal/VLSpeechDetectorImpl;->mPrevLastSpeechSample:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 106
    monitor-exit p0

    return v1
.end method

.method public declared-synchronized startSpeechDetector(Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;)Z
    .locals 7
    .param p1, "context"    # Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;

    .prologue
    const/4 v6, 0x1

    .line 53
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLSpeechDetectorImpl;->validateInstance()V

    .line 55
    if-nez p1, :cond_0

    .line 56
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "context must be specified"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 58
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/VLSpeechDetectorImpl;->mIsStarted:Z

    if-eqz v0, :cond_1

    .line 59
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "SpeechDetector already started"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_1
    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;->getAudioSourceInfo()Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getFormat()Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    move-result-object v0

    sget-object v2, Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;->PCM_16KHZ_16BIT:Lcom/vlingo/sdk/recognition/AudioSourceInfo$SourceFormat;

    if-ne v0, v2, :cond_2

    const/16 v1, 0x3e80

    .line 64
    .local v1, "sampleRate":I
    :goto_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLSpeechDetectorImpl;->mVLSilJNI:Lcom/vlingo/sdk/internal/audio/VLSilJNI;

    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;->getSilenceThreshold()F

    move-result v2

    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;->getMinVoiceDuration()F

    move-result v3

    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;->getVoicePortion()F

    move-result v4

    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;->getMinVoiceLevel()F

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/vlingo/sdk/internal/audio/VLSilJNI;->initialize(IFFFF)Z

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/sdk/internal/VLSpeechDetectorImpl;->mPrevLastSpeechSample:I

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/VLSpeechDetectorImpl;->mIsStarted:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 68
    monitor-exit p0

    return v6

    .line 62
    .end local v1    # "sampleRate":I
    :cond_2
    const/16 v1, 0x1f40

    goto :goto_0
.end method

.method public declared-synchronized stopSpeechDetector()V
    .locals 1

    .prologue
    .line 76
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLSpeechDetectorImpl;->validateInstance()V

    .line 78
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/VLSpeechDetectorImpl;->mIsStarted:Z

    if-eqz v0, :cond_0

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/VLSpeechDetectorImpl;->mIsStarted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    :cond_0
    monitor-exit p0

    return-void

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
