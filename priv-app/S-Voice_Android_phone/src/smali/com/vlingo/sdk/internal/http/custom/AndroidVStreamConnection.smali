.class public Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;
.super Ljava/lang/Object;
.source "AndroidVStreamConnection.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;
.implements Lcom/vlingo/sdk/internal/net/HttpConnection;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private m_Connection:Ljava/net/HttpURLConnection;

.field private m_DataInputStream:Ljava/io/DataInputStream;

.field private m_DataOutputStream:Ljava/io/DataOutputStream;

.field private m_InputStatus:Ljava/lang/Boolean;

.field private m_InputStream:Ljava/io/InputStream;

.field private m_OutputStatus:Ljava/lang/Boolean;

.field private m_OutputStream:Ljava/io/OutputStream;

.field private m_URL:Ljava/net/URL;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/vlingo/sdk/internal/http/URL;)V
    .locals 5
    .param p1, "vUrl"    # Lcom/vlingo/sdk/internal/http/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    new-instance v0, Ljava/net/URL;

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/http/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/vlingo/sdk/internal/http/URL;->host:Ljava/lang/String;

    iget v3, p1, Lcom/vlingo/sdk/internal/http/URL;->port:I

    iget-object v4, p1, Lcom/vlingo/sdk/internal/http/URL;->path:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;-><init>(Ljava/net/URL;)V

    .line 73
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;-><init>(Ljava/net/URL;)V

    .line 69
    return-void
.end method

.method public constructor <init>(Ljava/net/URL;)V
    .locals 3
    .param p1, "url"    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    .line 39
    iput-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_URL:Ljava/net/URL;

    .line 40
    iput-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_InputStream:Ljava/io/InputStream;

    .line 41
    iput-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_OutputStream:Ljava/io/OutputStream;

    .line 42
    iput-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_DataInputStream:Ljava/io/DataInputStream;

    .line 43
    iput-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_DataOutputStream:Ljava/io/DataOutputStream;

    .line 44
    iput-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_InputStatus:Ljava/lang/Boolean;

    .line 45
    iput-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_OutputStatus:Ljava/lang/Boolean;

    .line 48
    if-nez p1, :cond_0

    .line 49
    new-instance v1, Ljava/io/IOException;

    const-string/jumbo v2, "url is null"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 52
    :cond_0
    :try_start_0
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_URL:Ljava/net/URL;

    .line 53
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_URL:Ljava/net/URL;

    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    check-cast v1, Ljava/net/HttpURLConnection;

    iput-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    .line 54
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    invoke-static {v1}, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->setupTLSIfNeeded(Ljava/net/HttpURLConnection;)V

    .line 55
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 56
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 57
    invoke-static {}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->isSlowNetwork()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 58
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    const v2, 0x9c40

    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 65
    :goto_0
    return-void

    .line 60
    :cond_1
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    const/16 v2, 0x4e20

    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 62
    :catch_0
    move-exception v0

    .line 63
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->TAG:Ljava/lang/String;

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_URL:Ljava/net/URL;

    monitor-enter v2

    .line 82
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_DataInputStream:Ljava/io/DataInputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 84
    :try_start_1
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_DataInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    .line 85
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_DataInputStream:Ljava/io/DataInputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90
    :cond_0
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_InputStream:Ljava/io/InputStream;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v1, :cond_1

    .line 92
    :try_start_3
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_InputStream:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 93
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_InputStream:Ljava/io/InputStream;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 98
    :cond_1
    :goto_1
    :try_start_4
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_DataOutputStream:Ljava/io/DataOutputStream;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v1, :cond_2

    .line 100
    :try_start_5
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_DataOutputStream:Ljava/io/DataOutputStream;

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 101
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_DataOutputStream:Ljava/io/DataOutputStream;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 106
    :cond_2
    :goto_2
    :try_start_6
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_OutputStream:Ljava/io/OutputStream;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v1, :cond_3

    .line 108
    :try_start_7
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_OutputStream:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 109
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_OutputStream:Ljava/io/OutputStream;
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 114
    :cond_3
    :goto_3
    :try_start_8
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->isOpen()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 115
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 116
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    .line 118
    :cond_4
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_InputStatus:Ljava/lang/Boolean;

    .line 119
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_OutputStatus:Ljava/lang/Boolean;

    .line 120
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_URL:Ljava/net/URL;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 121
    monitor-exit v2

    .line 122
    return-void

    .line 86
    :catch_0
    move-exception v0

    .line 87
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->TAG:Ljava/lang/String;

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 121
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    throw v1

    .line 94
    :catch_1
    move-exception v0

    .line 95
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_9
    sget-object v1, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->TAG:Ljava/lang/String;

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 102
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 103
    .restart local v0    # "e":Ljava/io/IOException;
    sget-object v1, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->TAG:Ljava/lang/String;

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 110
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 111
    .restart local v0    # "e":Ljava/io/IOException;
    sget-object v1, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->TAG:Ljava/lang/String;

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_3
.end method

.method public getConnectionDetails()Lcom/vlingo/sdk/internal/net/ConnectionResult;
    .locals 2

    .prologue
    .line 126
    new-instance v0, Lcom/vlingo/sdk/internal/net/ConnectionResult;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/net/ConnectionResult;-><init>()V

    .line 127
    .local v0, "r":Lcom/vlingo/sdk/internal/net/ConnectionResult;
    const/4 v1, 0x0

    iput v1, v0, Lcom/vlingo/sdk/internal/net/ConnectionResult;->connectionType:I

    .line 128
    iput-object p0, v0, Lcom/vlingo/sdk/internal/net/ConnectionResult;->connection:Lcom/vlingo/sdk/internal/net/Connection;

    .line 129
    return-object v0
.end method

.method public getFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_URL:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHeaderField(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 211
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->getHeaderField(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHeaderFieldKey(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 216
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->getHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_URL:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInputStream()Ljava/io/DataInputStream;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    .line 135
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "m_Connection is null"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    monitor-enter v1

    .line 138
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_DataInputStream:Ljava/io/DataInputStream;

    if-nez v0, :cond_2

    .line 139
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->openInputStream()Ljava/io/InputStream;

    .line 140
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_InputStream:Ljava/io/InputStream;

    if-nez v0, :cond_1

    .line 141
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v2, "m_InputStream is null"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 142
    :cond_1
    :try_start_1
    new-instance v0, Ljava/io/DataInputStream;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_InputStream:Ljava/io/InputStream;

    invoke-direct {v0, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_DataInputStream:Ljava/io/DataInputStream;

    .line 144
    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 145
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_DataInputStream:Ljava/io/DataInputStream;

    return-object v0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v0

    return v0
.end method

.method public getOutputStream()Ljava/io/DataOutputStream;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    .line 177
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "m_Connection is null"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_URL:Ljava/net/URL;

    monitor-enter v1

    .line 180
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_DataOutputStream:Ljava/io/DataOutputStream;

    if-nez v0, :cond_4

    .line 181
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_OutputStatus:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 182
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v2, "m_OutputStatus is null"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 184
    :cond_1
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_OutputStatus:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_2

    .line 186
    :try_start_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_URL:Ljava/net/URL;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getReadTimeout()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 187
    :catch_0
    move-exception v0

    goto :goto_0

    .line 190
    :cond_2
    :try_start_3
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_OutputStream:Ljava/io/OutputStream;

    if-nez v0, :cond_3

    .line 191
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v2, "m_OutputStream is null"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 192
    :cond_3
    new-instance v0, Ljava/io/DataOutputStream;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_OutputStream:Ljava/io/OutputStream;

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_DataOutputStream:Ljava/io/DataOutputStream;

    .line 194
    :cond_4
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 195
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_DataOutputStream:Ljava/io/DataOutputStream;

    return-object v0
.end method

.method public getResponseCode()I
    .locals 3

    .prologue
    .line 232
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 237
    :goto_0
    return v1

    .line 233
    :catch_0
    move-exception v0

    .line 234
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->TAG:Ljava/lang/String;

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    .end local v0    # "e":Ljava/io/IOException;
    :goto_1
    const/4 v1, -0x1

    goto :goto_0

    .line 235
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public isOpen()Z
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public openDataInputStream()Ljava/io/DataInputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->getInputStream()Ljava/io/DataInputStream;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic openDataInputStream()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->openDataInputStream()Ljava/io/DataInputStream;

    move-result-object v0

    return-object v0
.end method

.method public openDataOutputStream()Ljava/io/DataOutputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->getOutputStream()Ljava/io/DataOutputStream;

    move-result-object v0

    return-object v0
.end method

.method public openInputStream()Ljava/io/InputStream;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    .line 156
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "m_Connection is null"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 158
    :cond_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_URL:Ljava/net/URL;

    monitor-enter v1

    .line 159
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_InputStream:Ljava/io/InputStream;

    if-nez v0, :cond_2

    .line 160
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_InputStatus:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 161
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v2, "m_InputStatus is null"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 163
    :cond_1
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_InputStatus:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_2

    .line 165
    :try_start_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_URL:Ljava/net/URL;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getReadTimeout()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 166
    :catch_0
    move-exception v0

    goto :goto_0

    .line 170
    :cond_2
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 171
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_InputStream:Ljava/io/InputStream;

    return-object v0
.end method

.method public setDoOutput(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 76
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 77
    return-void
.end method

.method public setRequestMethod(Ljava/lang/String;)V
    .locals 4
    .param p1, "method"    # Ljava/lang/String;

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->isOpen()Z

    move-result v1

    if-nez v1, :cond_0

    .line 250
    :goto_0
    return-void

    .line 245
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v1, p1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 246
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    const-string/jumbo v2, "Connection"

    const-string/jumbo v3, "CLOSE"

    invoke-virtual {v1, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/ProtocolException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 247
    :catch_0
    move-exception v0

    .line 248
    .local v0, "e":Ljava/net/ProtocolException;
    sget-object v1, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->TAG:Ljava/lang/String;

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    .line 258
    :goto_0
    return-void

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1, p2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startRequest(Lcom/vlingo/sdk/internal/http/custom/HttpRequest;)V
    .locals 8
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/custom/HttpRequest;

    .prologue
    .line 272
    :try_start_0
    iget-object v6, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_URL:Ljava/net/URL;

    monitor-enter v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    if-eqz p1, :cond_1

    :try_start_1
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->isOpen()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 274
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->sendHeaders()Ljava/util/Hashtable;

    move-result-object v1

    .line 275
    .local v1, "headers":Ljava/util/Hashtable;, "Ljava/util/Hashtable<**>;"
    invoke-virtual {v1}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v2

    .line 276
    .local v2, "iter":Ljava/util/Enumeration;, "Ljava/util/Enumeration<*>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 277
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 278
    .local v3, "key":Ljava/lang/String;
    invoke-virtual {v1, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 279
    .local v4, "value":Ljava/lang/String;
    iget-object v5, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v5, v3, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 283
    .end local v1    # "headers":Ljava/util/Hashtable;, "Ljava/util/Hashtable<**>;"
    .end local v2    # "iter":Ljava/util/Enumeration;, "Ljava/util/Enumeration<*>;"
    .end local v3    # "key":Ljava/lang/String;
    .end local v4    # "value":Ljava/lang/String;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v5
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 291
    :catch_0
    move-exception v0

    .line 292
    .local v0, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->TAG:Ljava/lang/String;

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    iget-object v6, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_URL:Ljava/net/URL;

    monitor-enter v6

    .line 295
    const/4 v5, 0x1

    :try_start_3
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_OutputStatus:Ljava/lang/Boolean;

    .line 296
    iget-object v5, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_URL:Ljava/net/URL;

    invoke-virtual {v5}, Ljava/lang/Object;->notifyAll()V

    .line 297
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 298
    return-void

    .line 281
    .restart local v1    # "headers":Ljava/util/Hashtable;, "Ljava/util/Hashtable<**>;"
    .restart local v2    # "iter":Ljava/util/Enumeration;, "Ljava/util/Enumeration<*>;"
    :cond_0
    :try_start_4
    iget-object v5, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    const/16 v7, 0x400

    invoke-virtual {v5, v7}, Ljava/net/HttpURLConnection;->setChunkedStreamingMode(I)V

    .line 283
    .end local v1    # "headers":Ljava/util/Hashtable;, "Ljava/util/Hashtable<**>;"
    .end local v2    # "iter":Ljava/util/Enumeration;, "Ljava/util/Enumeration<*>;"
    :cond_1
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 285
    const/4 v5, 0x0

    :try_start_5
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_OutputStatus:Ljava/lang/Boolean;

    .line 286
    iget-object v6, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_URL:Ljava/net/URL;

    monitor-enter v6
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    .line 287
    :try_start_6
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->isOpen()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 288
    iget-object v5, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_OutputStream:Ljava/io/OutputStream;

    .line 290
    :cond_2
    monitor-exit v6

    goto :goto_1

    :catchall_1
    move-exception v5

    monitor-exit v6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v5
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    .line 297
    :catchall_2
    move-exception v5

    :try_start_8
    monitor-exit v6
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v5
.end method

.method public startResponse(Lcom/vlingo/sdk/internal/http/custom/HttpResponse;)V
    .locals 9
    .param p1, "response"    # Lcom/vlingo/sdk/internal/http/custom/HttpResponse;

    .prologue
    .line 307
    :try_start_0
    iget-object v7, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_URL:Ljava/net/URL;

    monitor-enter v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 308
    if-eqz p1, :cond_2

    :try_start_1
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->isOpen()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 309
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;->readHeaders()Ljava/util/Hashtable;

    move-result-object v4

    .line 310
    .local v4, "headers":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 313
    .local v3, "fields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    iget-object v6, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v3

    .line 316
    const-string/jumbo v6, "set-cookie"

    invoke-interface {v3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 317
    .local v1, "cookies":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v3, :cond_2

    if-eqz v1, :cond_2

    .line 320
    const/4 v0, 0x0

    .line 321
    .local v0, "cookieStr":Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 322
    .local v5, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 323
    if-nez v0, :cond_0

    .line 324
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cookieStr":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "cookieStr":Ljava/lang/String;
    goto :goto_0

    .line 326
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v8, ","

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 330
    :cond_1
    const-string/jumbo v6, "Set-Cookie"

    invoke-virtual {v4, v6, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 333
    .end local v0    # "cookieStr":Ljava/lang/String;
    .end local v1    # "cookies":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "fields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v4    # "headers":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 334
    const/4 v6, 0x0

    :try_start_2
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_InputStatus:Ljava/lang/Boolean;

    .line 337
    iget-object v7, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_URL:Ljava/net/URL;

    monitor-enter v7
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 338
    :try_start_3
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->isOpen()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 339
    iget-object v6, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_Connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_InputStream:Ljava/io/InputStream;

    .line 341
    :cond_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 345
    :goto_1
    iget-object v7, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_URL:Ljava/net/URL;

    monitor-enter v7

    .line 346
    const/4 v6, 0x1

    :try_start_4
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_InputStatus:Ljava/lang/Boolean;

    .line 347
    iget-object v6, p0, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->m_URL:Ljava/net/URL;

    invoke-virtual {v6}, Ljava/lang/Object;->notifyAll()V

    .line 348
    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 349
    return-void

    .line 333
    :catchall_0
    move-exception v6

    :try_start_5
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v6
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    .line 342
    :catch_0
    move-exception v2

    .line 343
    .local v2, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->TAG:Ljava/lang/String;

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 341
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v6

    :try_start_7
    monitor-exit v7
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v6
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    .line 348
    :catchall_2
    move-exception v6

    :try_start_9
    monitor-exit v7
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    throw v6
.end method
