.class Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpConnectionWrapper;
.super Ljava/lang/Object;
.source "CookieHandler.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HttpConnectionWrapper"
.end annotation


# instance fields
.field private connection:Lcom/vlingo/sdk/internal/net/HttpConnection;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/net/HttpConnection;)V
    .locals 2
    .param p1, "connection"    # Lcom/vlingo/sdk/internal/net/HttpConnection;

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    if-nez p1, :cond_0

    .line 94
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "you MUST provide a non-null \'connection\'"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_0
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpConnectionWrapper;->connection:Lcom/vlingo/sdk/internal/net/HttpConnection;

    .line 97
    return-void
.end method


# virtual methods
.method public getHeaderField(I)Ljava/lang/String;
    .locals 1
    .param p1, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpConnectionWrapper;->connection:Lcom/vlingo/sdk/internal/net/HttpConnection;

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/internal/net/HttpConnection;->getHeaderField(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHeaderFieldKey(I)Ljava/lang/String;
    .locals 1
    .param p1, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpConnectionWrapper;->connection:Lcom/vlingo/sdk/internal/net/HttpConnection;

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/internal/net/HttpConnection;->getHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpConnectionWrapper;->connection:Lcom/vlingo/sdk/internal/net/HttpConnection;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/net/HttpConnection;->getHost()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
