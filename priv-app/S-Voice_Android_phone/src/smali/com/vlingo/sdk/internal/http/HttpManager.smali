.class public Lcom/vlingo/sdk/internal/http/HttpManager;
.super Ljava/lang/Object;
.source "HttpManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/http/HttpManager$1;,
        Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;,
        Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;
    }
.end annotation


# static fields
.field private static final DEFAULT_BACKGROUND_TASK_DELAY:I = 0x1388

.field private static s_HttpManager:Lcom/vlingo/sdk/internal/http/HttpManager;


# instance fields
.field private final backgroudExecutorLock:Ljava/lang/Object;

.field private final backgroundExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

.field private volatile backgroundExecutorPaused:Z

.field private final onDemandExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

.field private final requestTable:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/sdk/internal/http/HttpRequest;",
            "Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;",
            ">;"
        }
    .end annotation
.end field

.field private final timeoutTimer:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/http/HttpManager;->s_HttpManager:Lcom/vlingo/sdk/internal/http/HttpManager;

    return-void
.end method

.method protected constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    const-string/jumbo v1, "OnDemandHttpManager"

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->onDemandExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    .line 66
    new-instance v0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    const-string/jumbo v1, "BackgroundHttpManager"

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->backgroundExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    .line 69
    iput-boolean v3, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->backgroundExecutorPaused:Z

    .line 70
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->backgroudExecutorLock:Ljava/lang/Object;

    .line 79
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->requestTable:Ljava/util/HashMap;

    .line 82
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->onDemandExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    invoke-virtual {v0, v2}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->setMinPoolSize(I)V

    .line 83
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->onDemandExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->setMaxPoolSize(I)V

    .line 84
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->onDemandExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    invoke-virtual {v0, v2}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->setDynamicSizing(Z)V

    .line 85
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->onDemandExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->setThreadPriority(I)V

    .line 87
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->backgroundExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    invoke-virtual {v0, v3}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->setMinPoolSize(I)V

    .line 88
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->backgroundExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    invoke-virtual {v0, v2}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->setMaxPoolSize(I)V

    .line 89
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->backgroundExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    invoke-virtual {v0, v2}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->setDynamicSizing(Z)V

    .line 90
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->backgroundExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    invoke-virtual {v0, v2}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->setThreadPriority(I)V

    .line 92
    invoke-static {}, Lcom/vlingo/sdk/internal/util/TimerSingleton;->getTimer()Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->timeoutTimer:Ljava/util/Timer;

    .line 93
    return-void
.end method

.method static synthetic access$400(Lcom/vlingo/sdk/internal/http/HttpManager;)Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/http/HttpManager;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->backgroundExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/sdk/internal/http/HttpManager;)Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/http/HttpManager;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->onDemandExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/sdk/internal/http/HttpManager;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/http/HttpManager;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->timeoutTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$800(Lcom/vlingo/sdk/internal/http/HttpManager;Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/http/HttpManager;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/http/HttpManager;->requestWasRan(Lcom/vlingo/sdk/internal/http/HttpRequest;)V

    return-void
.end method

.method static synthetic access$900(Lcom/vlingo/sdk/internal/http/HttpManager;Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/http/HttpManager;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/http/HttpRequest;
    .param p2, "x2"    # Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/http/HttpManager;->requestHasTimedOut(Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;)V

    return-void
.end method

.method public static destroy()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/http/HttpManager;->s_HttpManager:Lcom/vlingo/sdk/internal/http/HttpManager;

    .line 60
    return-void
.end method

.method private declared-synchronized doBackgroundRequestInternal(Lcom/vlingo/sdk/internal/http/HttpRequest;ZZ)V
    .locals 7
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;
    .param p2, "highPriority"    # Z
    .param p3, "ordered"    # Z

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;

    const/4 v3, 0x1

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;-><init>(Lcom/vlingo/sdk/internal/http/HttpManager;Lcom/vlingo/sdk/internal/http/HttpRequest;ZZZLcom/vlingo/sdk/internal/http/HttpManager$1;)V

    .line 121
    .local v0, "fetcher":Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;
    # invokes: Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->start()V
    invoke-static {v0}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->access$100(Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;)V

    .line 122
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->requestTable:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    monitor-exit p0

    return-void

    .line 120
    .end local v0    # "fetcher":Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized doBackgroundRequestLaterInternal(Lcom/vlingo/sdk/internal/http/HttpRequest;JZZ)V
    .locals 7
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;
    .param p2, "delay"    # J
    .param p4, "highPriority"    # Z
    .param p5, "ordered"    # Z

    .prologue
    .line 127
    monitor-enter p0

    const-wide/16 v1, 0x0

    cmp-long v1, p2, v1

    if-nez v1, :cond_0

    .line 128
    :try_start_0
    invoke-virtual {p0, p1, p4, p5}, Lcom/vlingo/sdk/internal/http/HttpManager;->doBackgroundRequest(Lcom/vlingo/sdk/internal/http/HttpRequest;ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    :goto_0
    monitor-exit p0

    return-void

    .line 130
    :cond_0
    :try_start_1
    new-instance v0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;

    const/4 v3, 0x1

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;-><init>(Lcom/vlingo/sdk/internal/http/HttpManager;Lcom/vlingo/sdk/internal/http/HttpRequest;ZZZLcom/vlingo/sdk/internal/http/HttpManager$1;)V

    .line 131
    .local v0, "fetcher":Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;
    # invokes: Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->schedule(J)V
    invoke-static {v0, p2, p3}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->access$300(Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;J)V

    .line 132
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->requestTable:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 127
    .end local v0    # "fetcher":Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized doRequestNowInternal(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 114
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;-><init>(Lcom/vlingo/sdk/internal/http/HttpManager;Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpManager$1;)V

    .line 115
    .local v0, "fetcher":Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;
    # invokes: Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->start()V
    invoke-static {v0}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->access$100(Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;)V

    .line 116
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->requestTable:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    monitor-exit p0

    return-void

    .line 114
    .end local v0    # "fetcher":Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public static declared-synchronized getInstance()Lcom/vlingo/sdk/internal/http/HttpManager;
    .locals 2

    .prologue
    .line 53
    const-class v1, Lcom/vlingo/sdk/internal/http/HttpManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/http/HttpManager;->s_HttpManager:Lcom/vlingo/sdk/internal/http/HttpManager;

    if-nez v0, :cond_0

    .line 54
    new-instance v0, Lcom/vlingo/sdk/internal/http/HttpManager;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/http/HttpManager;-><init>()V

    sput-object v0, Lcom/vlingo/sdk/internal/http/HttpManager;->s_HttpManager:Lcom/vlingo/sdk/internal/http/HttpManager;

    .line 55
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/internal/http/HttpManager;->s_HttpManager:Lcom/vlingo/sdk/internal/http/HttpManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private declared-synchronized requestHasTimedOut(Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;
    .param p2, "originatingTimeoutTask"    # Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;

    .prologue
    .line 148
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->requestTable:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 149
    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->requestTable:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;

    .line 150
    .local v0, "fetcher":Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;
    invoke-virtual {v0, p2}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->timeout(Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTimeoutTask;)Z

    move-result v1

    .line 151
    .local v1, "timedOut":Z
    if-eqz v1, :cond_0

    .line 152
    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->requestTable:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    .end local v0    # "fetcher":Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;
    .end local v1    # "timedOut":Z
    :cond_0
    monitor-exit p0

    return-void

    .line 148
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private declared-synchronized requestWasRan(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 158
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->requestTable:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    monitor-exit p0

    return-void

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public cancelRequest(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 110
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/http/HttpManager;->cancelRequestInternal(Lcom/vlingo/sdk/internal/http/HttpRequest;)V

    .line 111
    return-void
.end method

.method protected declared-synchronized cancelRequestInternal(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 137
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->requestTable:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->requestTable:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;

    .line 139
    .local v0, "fetcher":Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;->cancel()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    .end local v0    # "fetcher":Lcom/vlingo/sdk/internal/http/HttpManager$HttpRequestFetchTask;
    :cond_0
    monitor-exit p0

    return-void

    .line 137
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public doBackgroundRequest(Lcom/vlingo/sdk/internal/http/HttpRequest;ZZ)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;
    .param p2, "highPriority"    # Z
    .param p3, "ordered"    # Z

    .prologue
    .line 101
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/sdk/internal/http/HttpManager;->doBackgroundRequestInternal(Lcom/vlingo/sdk/internal/http/HttpRequest;ZZ)V

    .line 102
    return-void
.end method

.method public doBackgroundRequestLater(Lcom/vlingo/sdk/internal/http/HttpRequest;JZZ)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;
    .param p2, "delay"    # J
    .param p4, "highPriority"    # Z
    .param p5, "ordered"    # Z

    .prologue
    .line 106
    invoke-direct/range {p0 .. p5}, Lcom/vlingo/sdk/internal/http/HttpManager;->doBackgroundRequestLaterInternal(Lcom/vlingo/sdk/internal/http/HttpRequest;JZZ)V

    .line 107
    return-void
.end method

.method public doRequestNow(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/http/HttpManager;->doRequestNowInternal(Lcom/vlingo/sdk/internal/http/HttpRequest;)V

    .line 97
    return-void
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 164
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->backgroudExecutorLock:Ljava/lang/Object;

    monitor-enter v1

    .line 165
    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->backgroundExecutorPaused:Z

    if-nez v0, :cond_0

    .line 166
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->backgroundExecutorPaused:Z

    .line 167
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->backgroundExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->pause()V

    .line 169
    :cond_0
    monitor-exit v1

    .line 170
    return-void

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public resume()V
    .locals 2

    .prologue
    .line 173
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->backgroudExecutorLock:Ljava/lang/Object;

    monitor-enter v1

    .line 175
    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->backgroundExecutorPaused:Z

    if-eqz v0, :cond_0

    .line 176
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->backgroundExecutorPaused:Z

    .line 177
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/HttpManager;->backgroundExecutor:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->resume()V

    .line 179
    :cond_0
    monitor-exit v1

    .line 180
    return-void

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
