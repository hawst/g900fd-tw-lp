.class public Lcom/vlingo/sdk/internal/deviceinfo/Carrier;
.super Ljava/lang/Object;
.source "Carrier.java"


# instance fields
.field public iso2letterCountry:Ljava/lang/String;

.field public iso3letterCountry:Ljava/lang/String;

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "iso2letterCountry"    # Ljava/lang/String;
    .param p3, "iso3letterCountry"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/vlingo/sdk/internal/deviceinfo/Carrier;->name:Ljava/lang/String;

    .line 16
    iput-object p2, p0, Lcom/vlingo/sdk/internal/deviceinfo/Carrier;->iso2letterCountry:Ljava/lang/String;

    .line 17
    iput-object p3, p0, Lcom/vlingo/sdk/internal/deviceinfo/Carrier;->iso3letterCountry:Ljava/lang/String;

    .line 18
    return-void
.end method
