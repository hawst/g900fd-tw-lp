.class public final enum Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;
.super Ljava/lang/Enum;
.source "DataReaderListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ErrorCode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;

.field public static final enum READ_ERROR:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;

    const-string/jumbo v1, "READ_ERROR"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;->READ_ERROR:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;->READ_ERROR:Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;

    aput-object v1, v0, v2

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;->$VALUES:[Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 4
    const-class v0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;->$VALUES:[Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;

    invoke-virtual {v0}, [Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/sdk/internal/recognizer/reader/DataReaderListener$ErrorCode;

    return-object v0
.end method
