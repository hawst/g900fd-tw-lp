.class final enum Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;
.super Ljava/lang/Enum;
.source "VoconRecoTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "VoconAudioPushError"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;

.field public static final enum ILLEGAL_STATE:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;

.field public static final enum RECOGNITION_ERROR:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 346
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;

    const-string/jumbo v1, "RECOGNITION_ERROR"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;->RECOGNITION_ERROR:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;

    .line 347
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;

    const-string/jumbo v1, "ILLEGAL_STATE"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;->ILLEGAL_STATE:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;

    .line 345
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;->RECOGNITION_ERROR:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;->ILLEGAL_STATE:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;->$VALUES:[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 345
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 345
    const-class v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;
    .locals 1

    .prologue
    .line 345
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;->$VALUES:[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;

    invoke-virtual {v0}, [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;

    return-object v0
.end method
