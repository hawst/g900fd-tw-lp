.class Lcom/vlingo/sdk/internal/VLTrainerImpl$1;
.super Ljava/lang/Object;
.source "VLTrainerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/sdk/internal/VLTrainerImpl;->sendUpdate(Lcom/vlingo/sdk/training/VLTrainerUpdateList;Ljava/lang/String;Lcom/vlingo/sdk/training/VLTrainerListener;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/VLTrainerImpl;

.field final synthetic val$doWhole:Z

.field final synthetic val$language:Ljava/lang/String;

.field final synthetic val$list:Lcom/vlingo/sdk/training/VLTrainerUpdateList;

.field final synthetic val$listener:Lcom/vlingo/sdk/training/VLTrainerListener;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/VLTrainerImpl;Lcom/vlingo/sdk/training/VLTrainerListener;Lcom/vlingo/sdk/training/VLTrainerUpdateList;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLTrainerImpl;

    iput-object p2, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl$1;->val$listener:Lcom/vlingo/sdk/training/VLTrainerListener;

    iput-object p3, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl$1;->val$list:Lcom/vlingo/sdk/training/VLTrainerUpdateList;

    iput-boolean p4, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl$1;->val$doWhole:Z

    iput-object p5, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl$1;->val$language:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 243
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLTrainerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLTrainerImpl;->semaphore:Ljava/util/concurrent/Semaphore;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/VLTrainerImpl;->access$100(Lcom/vlingo/sdk/internal/VLTrainerImpl;)Ljava/util/concurrent/Semaphore;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 244
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLTrainerImpl;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/vlingo/sdk/internal/VLTrainerImpl;->setBusy(Z)V

    .line 245
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLTrainerImpl;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl$1;->val$listener:Lcom/vlingo/sdk/training/VLTrainerListener;

    # setter for: Lcom/vlingo/sdk/internal/VLTrainerImpl;->mTrainerListener:Lcom/vlingo/sdk/training/VLTrainerListener;
    invoke-static {v2, v3}, Lcom/vlingo/sdk/internal/VLTrainerImpl;->access$002(Lcom/vlingo/sdk/internal/VLTrainerImpl;Lcom/vlingo/sdk/training/VLTrainerListener;)Lcom/vlingo/sdk/training/VLTrainerListener;

    .line 246
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl$1;->val$list:Lcom/vlingo/sdk/training/VLTrainerUpdateList;

    check-cast v2, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->getLmttItems()Ljava/util/HashMap;

    move-result-object v0

    .line 247
    .local v0, "lmttItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;Ljava/util/Collection<Lcom/vlingo/sdk/internal/lmtt/LMTTItem;>;>;"
    iget-boolean v2, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl$1;->val$doWhole:Z

    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl$1;->val$language:Ljava/lang/String;

    iget-object v4, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl$1;->this$0:Lcom/vlingo/sdk/internal/VLTrainerImpl;

    invoke-static {v0, v2, v3, v4}, Lcom/vlingo/sdk/internal/lmtt/LMTTComm;->createLMTTRequest(Ljava/util/HashMap;ZLjava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    move-result-object v1

    .line 248
    .local v1, "request":Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    const-wide/16 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->schedule(JZZ)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    .end local v0    # "lmttItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;Ljava/util/Collection<Lcom/vlingo/sdk/internal/lmtt/LMTTItem;>;>;"
    .end local v1    # "request":Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    :goto_0
    return-void

    .line 249
    :catch_0
    move-exception v2

    goto :goto_0
.end method
