.class public Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;
.super Ljava/lang/Object;
.source "VLRecognitionResultImpl.java"

# interfaces
.implements Lcom/vlingo/sdk/recognition/VLRecognitionResult;


# instance fields
.field private mActionList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            ">;"
        }
    .end annotation
.end field

.field private mDialogGuid:Ljava/lang/String;

.field private mDialogState:[B

.field private mDialogTurn:I

.field private mGUttId:Ljava/lang/String;

.field private mNBestData:Lcom/vlingo/sdk/recognition/NBestData;

.field private mParseGroup:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

.field private mResult:Ljava/lang/String;

.field private rawResponseString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    .locals 7
    .param p1, "response"    # Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    if-eqz p1, :cond_1

    .line 36
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->getResults()Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    move-result-object v4

    .line 37
    .local v4, "results":Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;
    if-eqz v4, :cond_0

    .line 38
    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->getParseGroup()Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;->mParseGroup:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

    .line 39
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->getResults()Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->getGUttID()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;->mGUttId:Ljava/lang/String;

    .line 40
    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->getUttResults()Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;->mResult:Ljava/lang/String;

    .line 41
    new-instance v5, Lcom/vlingo/sdk/recognition/NBestData;

    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->getUttResults()Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/vlingo/sdk/recognition/NBestData;-><init>(Lcom/vlingo/sdk/internal/recognizer/results/RecResults;)V

    iput-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;->mNBestData:Lcom/vlingo/sdk/recognition/NBestData;

    .line 44
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->getDialogGuid()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;->mDialogGuid:Ljava/lang/String;

    .line 45
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->getDialogTurn()I

    move-result v5

    iput v5, p0, Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;->mDialogTurn:I

    .line 46
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->getDialogState()[B

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;->mDialogState:[B

    .line 47
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->getRawServerResponse()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;->rawResponseString:Ljava/lang/String;

    .line 50
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->hasActions()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 51
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->getActionList()Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    move-result-object v1

    .line 52
    .local v1, "actionList":Lcom/vlingo/sdk/internal/vlservice/response/ActionList;
    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->size()I

    move-result v3

    .line 53
    .local v3, "listSize":I
    if-lez v3, :cond_1

    .line 54
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;->mActionList:Ljava/util/List;

    .line 55
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 56
    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->elementAt(I)Lcom/vlingo/sdk/internal/vlservice/response/Action;

    move-result-object v0

    .line 57
    .local v0, "action":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;->mActionList:Ljava/util/List;

    new-instance v6, Lcom/vlingo/sdk/internal/VLActionImpl;

    invoke-direct {v6, v0}, Lcom/vlingo/sdk/internal/VLActionImpl;-><init>(Lcom/vlingo/sdk/internal/vlservice/response/Action;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 62
    .end local v0    # "action":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    .end local v1    # "actionList":Lcom/vlingo/sdk/internal/vlservice/response/ActionList;
    .end local v2    # "i":I
    .end local v3    # "listSize":I
    .end local v4    # "results":Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;
    :cond_1
    return-void
.end method


# virtual methods
.method public getActions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;->mActionList:Ljava/util/List;

    return-object v0
.end method

.method public getDialogGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;->mDialogGuid:Ljava/lang/String;

    return-object v0
.end method

.method public getDialogState()[B
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;->mDialogState:[B

    return-object v0
.end method

.method public getDialogTurn()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;->mDialogTurn:I

    return v0
.end method

.method public getFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public getGUttId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;->mGUttId:Ljava/lang/String;

    return-object v0
.end method

.method public getNBestData()Lcom/vlingo/sdk/recognition/NBestData;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;->mNBestData:Lcom/vlingo/sdk/recognition/NBestData;

    return-object v0
.end method

.method public getParseGroup()Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;->mParseGroup:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

    return-object v0
.end method

.method public getRawResponseString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;->rawResponseString:Ljava/lang/String;

    return-object v0
.end method

.method public getResultString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognitionResultImpl;->mResult:Ljava/lang/String;

    return-object v0
.end method

.method public isFromEDM()Z
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method
