.class Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;
.super Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
.source "ASRRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StreamingState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V
    .locals 1

    .prologue
    .line 998
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p2, "x1"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;

    .prologue
    .line 998
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V

    return-void
.end method


# virtual methods
.method public onRun()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, -0x2

    .line 1002
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isExpectingAudio()Z
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3900(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1003
    const-string/jumbo v2, "no audio excpected, transitioning ..."

    invoke-virtual {p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;->log(Ljava/lang/String;)V

    .line 1030
    :cond_0
    :goto_0
    return-void

    .line 1016
    .local v0, "audioSegment":Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;
    :cond_1
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "streaming "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;->log(Ljava/lang/String;)V

    .line 1017
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getAudioOutputStream()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3800(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    move-result-object v2

    iget-object v3, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;->audio:[B

    iget v4, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;->offset:I

    iget v5, v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;->length:I

    invoke-virtual {v2, v3, v4, v5}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->write([BII)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    .line 1009
    .end local v0    # "audioSegment":Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;
    :cond_2
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$800(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1011
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getAudioQueue()Ljava/util/concurrent/BlockingQueue;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1800(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/util/concurrent/BlockingQueue;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;

    .line 1012
    .restart local v0    # "audioSegment":Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;->isEndSegment()Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 1020
    .end local v0    # "audioSegment":Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;
    :catch_0
    move-exception v1

    .line 1021
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;->log(Ljava/lang/Throwable;)V

    .line 1022
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->onError(I)V
    invoke-static {v2, v6}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;I)V

    goto :goto_0

    .line 1023
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    .line 1024
    .local v1, "e":Ljava/net/SocketTimeoutException;
    const-string/jumbo v2, "streaming TIMED OUT"

    invoke-virtual {p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;->log(Ljava/lang/String;)V

    .line 1025
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const/4 v3, -0x3

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->onTimeout(I)V
    invoke-static {v2, v3}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$900(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;I)V

    goto :goto_0

    .line 1026
    .end local v1    # "e":Ljava/net/SocketTimeoutException;
    :catch_2
    move-exception v1

    .line 1027
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;->log(Ljava/lang/Throwable;)V

    .line 1028
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->onError(I)V
    invoke-static {v2, v6}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;I)V

    goto :goto_0
.end method
