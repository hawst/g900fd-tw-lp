.class Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;
.super Ljava/lang/Object;
.source "VoconRecoTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VoconAudioPushResult"
.end annotation


# instance fields
.field private mError:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;

.field private mResult:Ljava/lang/String;

.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;Ljava/lang/String;)V
    .locals 0
    .param p2, "error"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;
    .param p3, "result"    # Ljava/lang/String;

    .prologue
    .line 331
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;->this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 332
    iput-object p2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;->mError:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;

    .line 333
    iput-object p3, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;->mResult:Ljava/lang/String;

    .line 334
    return-void
.end method


# virtual methods
.method public getError()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;->mError:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushError;

    return-object v0
.end method

.method public getResult()Ljava/lang/String;
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$VoconAudioPushResult;->mResult:Ljava/lang/String;

    return-object v0
.end method
