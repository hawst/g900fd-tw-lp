.class Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpURLConnectionWrapper;
.super Ljava/lang/Object;
.source "CookieHandler.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HttpURLConnectionWrapper"
.end annotation


# instance fields
.field private connection:Ljava/net/HttpURLConnection;


# direct methods
.method public constructor <init>(Ljava/net/HttpURLConnection;)V
    .locals 2
    .param p1, "connection"    # Ljava/net/HttpURLConnection;

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    if-nez p1, :cond_0

    .line 132
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "you MUST provide a non-null \'connection\'"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134
    :cond_0
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpURLConnectionWrapper;->connection:Ljava/net/HttpURLConnection;

    .line 135
    return-void
.end method


# virtual methods
.method public getHeaderField(I)Ljava/lang/String;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 139
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpURLConnectionWrapper;->connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->getHeaderField(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHeaderFieldKey(I)Ljava/lang/String;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 138
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpURLConnectionWrapper;->connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->getHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpURLConnectionWrapper;->connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
