.class public Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;
.super Ljava/lang/Object;
.source "HttpInteraction.java"


# instance fields
.field private ivCon:Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

.field private ivRequest:Lcom/vlingo/sdk/internal/http/custom/HttpRequest;

.field private ivResponse:Lcom/vlingo/sdk/internal/http/custom/HttpResponse;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;)V
    .locals 2
    .param p1, "con"    # Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    if-nez p1, :cond_0

    .line 26
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Connection is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_0
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    .line 31
    new-instance v0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;-><init>(Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->ivRequest:Lcom/vlingo/sdk/internal/http/custom/HttpRequest;

    .line 32
    new-instance v0, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/http/custom/HttpResponse;-><init>(Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->ivResponse:Lcom/vlingo/sdk/internal/http/custom/HttpResponse;

    .line 34
    return-void
.end method


# virtual methods
.method public getHTTPConnection()Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->ivCon:Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    return-object v0
.end method

.method public getRequest()Lcom/vlingo/sdk/internal/http/custom/HttpRequest;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->ivRequest:Lcom/vlingo/sdk/internal/http/custom/HttpRequest;

    return-object v0
.end method

.method public getResponse()Lcom/vlingo/sdk/internal/http/custom/HttpResponse;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->ivResponse:Lcom/vlingo/sdk/internal/http/custom/HttpResponse;

    return-object v0
.end method
