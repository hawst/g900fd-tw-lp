.class Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;
.super Ljava/lang/Object;
.source "ASRRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AudioOutputStream"
.end annotation


# instance fields
.field private flushOnWrite:Z

.field private mAudioBytesWritten:I

.field private mAudioTagSent:Ljava/lang/Boolean;

.field private mIsOpen:Z

.field private out:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 634
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629
    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->flushOnWrite:Z

    .line 630
    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->mIsOpen:Z

    .line 632
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->mAudioTagSent:Ljava/lang/Boolean;

    .line 635
    return-void
.end method

.method private insertAudioTag()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 692
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->mAudioTagSent:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 693
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->mAudioTagSent:Ljava/lang/Boolean;

    .line 694
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    # setter for: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimeSendStart:J
    invoke-static {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1502(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;J)J

    .line 695
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const/4 v1, 0x2

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->notifyListeners(I)V
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1600(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;I)V

    .line 696
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->out:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    const-string/jumbo v1, "audio"

    const-string/jumbo v2, "audio"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeFileFieldHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    :cond_0
    return-void
.end method


# virtual methods
.method public close()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 679
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->mIsOpen:Z

    .line 680
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->out:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeEndFieldValue()V

    .line 681
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->out:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeBoundary()V

    .line 682
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->out:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->insertCRCTag(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1400(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V

    .line 683
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const-string/jumbo v1, "REQD"

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->recordDetailedTiming(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1200(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/String;)V

    .line 684
    return-object p0
.end method

.method public flush()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 674
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->out:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->flush()V

    .line 675
    return-object p0
.end method

.method public getBytesWritten()I
    .locals 1

    .prologue
    .line 688
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->mAudioBytesWritten:I

    return v0
.end method

.method public isOpen()Z
    .locals 1

    .prologue
    .line 638
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->mIsOpen:Z

    return v0
.end method

.method public open(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;
    .locals 2
    .param p1, "out"    # Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 642
    if-nez p1, :cond_0

    .line 643
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "you MUST provide an output stream"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 645
    :cond_0
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->out:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    .line 646
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->insertMetaTag(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1100(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V

    .line 647
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->flush()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    .line 649
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->mIsOpen:Z

    .line 651
    return-object p0
.end method

.method public setFlushOnWrite(Z)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;
    .locals 0
    .param p1, "flushOnWrite"    # Z

    .prologue
    .line 655
    iput-boolean p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->flushOnWrite:Z

    .line 656
    return-object p0
.end method

.method public write([BII)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;
    .locals 3
    .param p1, "audio"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 660
    if-lez p3, :cond_0

    .line 661
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->insertAudioTag()V

    .line 662
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->out:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->write([BII)V

    .line 663
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "AUD"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->recordDetailedTiming(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1200(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/String;)V

    .line 664
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->mAudioBytesWritten:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->mAudioBytesWritten:I

    .line 665
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mCRC32:Ljava/util/zip/CRC32;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1300(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/util/zip/CRC32;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/zip/CRC32;->update([BII)V

    .line 666
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->flushOnWrite:Z

    if-eqz v0, :cond_0

    .line 667
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->flush()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    .line 670
    :cond_0
    return-object p0
.end method
