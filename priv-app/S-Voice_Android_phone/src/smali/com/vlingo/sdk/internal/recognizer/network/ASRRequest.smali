.class public Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
.super Ljava/lang/Object;
.source "ASRRequest.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;,
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;,
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;,
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$FinishedState;,
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;,
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;,
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;,
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$NoAudioOutputStream;,
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;,
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;,
        Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$Connection;
    }
.end annotation


# static fields
.field public static final BOUNDRY:Ljava/lang/String; = "-------------------------------1878979834"

.field private static final HTTP_CHUNK_SIZE:I = 0x200

.field private static final MAX_RETRY_COUNT:I = 0x2

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isExpectingAudio:Z

.field private isFinished:Z

.field private isTimeoutScheduled:Z

.field private mAudioOutputStream:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

.field private final mAudioQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;",
            ">;"
        }
    .end annotation
.end field

.field private final mCRC32:Ljava/util/zip/CRC32;

.field private mCancelledState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

.field private final mClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

.field private mConnectTimeout:I

.field private mConnection:Ljava/net/HttpURLConnection;

.field private mDisconnectedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

.field private mFinishedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

.field private mHandler:Landroid/os/Handler;

.field private final mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;",
            ">;"
        }
    .end annotation
.end field

.field private mMPOutputStream:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

.field private mNetworkThread:Landroid/os/HandlerThread;

.field private mReadTimeout:I

.field private mReceivedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

.field private mReceivingState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

.field private mRequestId:I

.field private mResponse:Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

.field private final mSRContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

.field private final mSRManager:Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;

.field private final mSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

.field private mState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

.field private mStreamingState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

.field private mTimeGotResults:J

.field private mTimeSendFinish:J

.field private mTimeSendStart:J

.field private final mTimeoutTimer:Ljava/util/Timer;

.field private mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-class v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;Z)V
    .locals 3
    .param p1, "context"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .param p2, "clientMeta"    # Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    .param p3, "softwareMeta"    # Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;
    .param p4, "srManager"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;
    .param p5, "timings"    # Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;
    .param p6, "sendAudio"    # Z

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 93
    iput v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mConnectTimeout:I

    .line 94
    iput v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mReadTimeout:I

    .line 96
    const/4 v0, 0x1

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mRequestId:I

    .line 99
    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mCRC32:Ljava/util/zip/CRC32;

    .line 113
    new-instance v0, Ljava/util/Timer;

    const-string/jumbo v1, "ASRRequest:TimeoutTimer"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimeoutTimer:Ljava/util/Timer;

    .line 115
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mAudioQueue:Ljava/util/concurrent/BlockingQueue;

    .line 117
    iput-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mConnection:Ljava/net/HttpURLConnection;

    .line 118
    iput-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mMPOutputStream:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    .line 119
    iput-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mAudioOutputStream:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    .line 120
    iput-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mResponse:Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .line 123
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;

    invoke-direct {v0, p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$DisconnectedState;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mDisconnectedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .line 124
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;

    invoke-direct {v0, p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$StreamingState;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mStreamingState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .line 125
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$FinishedState;

    invoke-direct {v0, p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$FinishedState;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mFinishedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .line 126
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;

    invoke-direct {v0, p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivingState;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mReceivingState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .line 127
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;

    invoke-direct {v0, p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$ReceivedState;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mReceivedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .line 128
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;

    invoke-direct {v0, p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mCancelledState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .line 131
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mDisconnectedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "ctor(): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p6, :cond_0

    const-string/jumbo v0, "with"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " audio stream"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->log(Ljava/lang/String;)V

    .line 137
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mSRContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    .line 138
    iput-object p2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    .line 139
    iput-object p3, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    .line 140
    iput-object p4, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mSRManager:Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;

    .line 142
    iput-boolean p6, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isExpectingAudio:Z

    .line 145
    iput-object p5, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    .line 149
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mDisconnectedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mStreamingState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->transitionTo(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V

    .line 150
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mStreamingState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mFinishedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->transitionTo(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V

    .line 151
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mFinishedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mReceivingState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->transitionTo(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V

    .line 152
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mReceivingState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mReceivedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->transitionTo(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V

    .line 153
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mReceivedState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->noTransition()V

    .line 154
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mCancelledState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->noTransition()V

    .line 156
    if-eqz p6, :cond_1

    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V

    :goto_1
    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mAudioOutputStream:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    .line 157
    return-void

    .line 135
    :cond_0
    const-string/jumbo v0, "without"

    goto :goto_0

    .line 156
    :cond_1
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$NoAudioOutputStream;

    invoke-direct {v0, p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$NoAudioOutputStream;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V

    goto :goto_1
.end method

.method static synthetic access$1002(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Z

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isTimeoutScheduled:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->insertMetaTag(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->recordDetailedTiming(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/util/zip/CRC32;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mCRC32:Ljava/util/zip/CRC32;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->insertCRCTag(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V

    return-void
.end method

.method static synthetic access$1502(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;J)J
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # J

    .prologue
    .line 58
    iput-wide p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimeSendStart:J

    return-wide p1
.end method

.method static synthetic access$1600(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # I

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->notifyListeners(I)V

    return-void
.end method

.method static synthetic access$1700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # I

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->onError(I)V

    return-void
.end method

.method static synthetic access$1800(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/util/concurrent/BlockingQueue;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getAudioQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1900(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isFinished:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Z

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isFinished:Z

    return p1
.end method

.method static synthetic access$2000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->startTimeoutTimer()V

    return-void
.end method

.method static synthetic access$2100(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mCancelledState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->runCurrentState()V

    return-void
.end method

.method static synthetic access$2300(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->changeState(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V

    return-void
.end method

.method static synthetic access$2400(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/Throwable;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Ljava/lang/Throwable;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->log(Ljava/lang/Throwable;)V

    return-void
.end method

.method static synthetic access$2500(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->log(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2800(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestManager()Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2900(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/net/HttpURLConnection;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Ljava/net/HttpURLConnection;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->setConnection(Ljava/net/HttpURLConnection;)V

    return-void
.end method

.method static synthetic access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3108(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)I
    .locals 2
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 58
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mRequestId:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mRequestId:I

    return v0
.end method

.method static synthetic access$3200(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getClientData()Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3300(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getSoftwareData()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3400(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->setOutputStream(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V

    return-void
.end method

.method static synthetic access$3500(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getOutputStream()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3600(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->buildEventElement()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->insertTextTag(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V

    return-void
.end method

.method static synthetic access$3800(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getAudioOutputStream()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3900(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isExpectingAudio()Z

    move-result v0

    return v0
.end method

.method static synthetic access$4002(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;J)J
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # J

    .prologue
    .line 58
    iput-wide p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimeGotResults:J

    return-wide p1
.end method

.method static synthetic access$4100(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/io/InputStream;)[B
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->consume(Ljava/io/InputStream;)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4200(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->setResponse(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    return-void
.end method

.method static synthetic access$4300(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->stop()V

    return-void
.end method

.method static synthetic access$4400(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getResponse()Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4500(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->notifyListeners(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    return-void
.end method

.method static synthetic access$4600(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getTimingString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->serverCheckLog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p1, "x1"    # I

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->onTimeout(I)V

    return-void
.end method

.method private buildEventElement()Ljava/lang/String;
    .locals 5

    .prologue
    .line 343
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 345
    .local v3, "sb":Ljava/lang/StringBuffer;
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getEvents()Ljava/util/List;

    move-result-object v1

    .line 346
    .local v1, "eventList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;>;"
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 347
    const-string/jumbo v4, "<Events>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 348
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;

    .line 349
    .local v0, "event":Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;->getXML()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 351
    .end local v0    # "event":Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    :cond_0
    const-string/jumbo v4, "</Events>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 354
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private buildMetaElement()Ljava/lang/String;
    .locals 13

    .prologue
    .line 359
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v10

    invoke-virtual {v10}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getCapitalization()Ljava/lang/String;

    move-result-object v1

    .line 360
    .local v1, "capitalize":Ljava/lang/String;
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v10

    invoke-virtual {v10}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getAutoPunctuation()Z

    move-result v10

    if-eqz v10, :cond_2

    const-string/jumbo v0, "true"

    .line 361
    .local v0, "autoPunctuate":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v10

    invoke-virtual {v10}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->isAudioStreamingEnabled()Z

    move-result v10

    if-eqz v10, :cond_3

    const-string/jumbo v9, "true"

    .line 366
    .local v9, "streaming":Ljava/lang/String;
    :goto_1
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v10

    invoke-virtual {v10}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getCurrentText()Ljava/lang/String;

    move-result-object v2

    .line 368
    .local v2, "curText":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    .line 370
    .local v8, "sb":Ljava/lang/StringBuffer;
    const-string/jumbo v10, "<Request "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 371
    const-string/jumbo v10, "FieldID"

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v11

    invoke-virtual {v11}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getFieldID()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 372
    const-string/jumbo v10, "AppID"

    iget-object v11, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    invoke-virtual {v11}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getAppId()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 373
    const-string/jumbo v10, "FieldType"

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v11

    invoke-virtual {v11}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getFieldType()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 374
    const-string/jumbo v10, "FieldContext"

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v11

    invoke-virtual {v11}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getFieldContext()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 376
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_0

    .line 377
    const-string/jumbo v10, "CurrentText"

    invoke-static {v10, v2}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 378
    const-string/jumbo v10, "CursorPosition"

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v11

    invoke-virtual {v11}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getCursorPosition()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 381
    :cond_0
    const/4 v5, 0x1

    .local v5, "i":I
    :goto_2
    const/4 v10, 0x6

    if-gt v5, v10, :cond_4

    .line 382
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Custom"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getCustomParam(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 383
    .local v3, "customValue":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_1

    .line 384
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Custom"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, v3}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 381
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 360
    .end local v0    # "autoPunctuate":Ljava/lang/String;
    .end local v2    # "curText":Ljava/lang/String;
    .end local v3    # "customValue":Ljava/lang/String;
    .end local v5    # "i":I
    .end local v8    # "sb":Ljava/lang/StringBuffer;
    .end local v9    # "streaming":Ljava/lang/String;
    :cond_2
    const-string/jumbo v0, "false"

    goto/16 :goto_0

    .line 361
    .restart local v0    # "autoPunctuate":Ljava/lang/String;
    :cond_3
    const-string/jumbo v9, "false"

    goto/16 :goto_1

    .line 388
    .restart local v2    # "curText":Ljava/lang/String;
    .restart local v5    # "i":I
    .restart local v8    # "sb":Ljava/lang/StringBuffer;
    .restart local v9    # "streaming":Ljava/lang/String;
    :cond_4
    const-string/jumbo v10, "StreamingAudio"

    invoke-static {v10, v9}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 389
    const-string/jumbo v10, "Punctuate"

    invoke-static {v10, v0}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 390
    const-string/jumbo v10, "Capitalize"

    invoke-static {v10, v1}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 393
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v10

    invoke-virtual {v10}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getMetaKVPairs()Ljava/util/HashMap;

    move-result-object v7

    .line 394
    .local v7, "metaKVPairs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v7, :cond_5

    invoke-virtual {v7}, Ljava/util/HashMap;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_5

    .line 395
    invoke-virtual {v7}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 396
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-static {v10, v11}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 400
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v6    # "i$":Ljava/util/Iterator;
    :cond_5
    const-string/jumbo v10, "/>"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 403
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    return-object v10
.end method

.method private changeState(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V
    .locals 2
    .param p1, "state"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .prologue
    .line 462
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setState("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->serverCheckLog(Ljava/lang/String;)V

    .line 463
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .line 464
    return-void
.end method

.method private consume(Ljava/io/InputStream;)[B
    .locals 4
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 534
    const/4 v1, 0x0

    .line 535
    .local v1, "bytesRead":I
    const/16 v3, 0x100

    new-array v0, v3, [B

    .line 536
    .local v0, "buffer":[B
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 537
    .local v2, "out":Ljava/io/ByteArrayOutputStream;
    :goto_0
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    if-ltz v1, :cond_0

    .line 538
    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v1}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 539
    :cond_0
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    return-object v3
.end method

.method private getAudioOutputStream()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;
    .locals 1

    .prologue
    .line 511
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mAudioOutputStream:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    return-object v0
.end method

.method private getAudioQueue()Ljava/util/concurrent/BlockingQueue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 499
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mAudioQueue:Ljava/util/concurrent/BlockingQueue;

    return-object v0
.end method

.method private getClientData()Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    .locals 1

    .prologue
    .line 495
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    return-object v0
.end method

.method private getConnection()Ljava/net/HttpURLConnection;
    .locals 1

    .prologue
    .line 503
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mConnection:Ljava/net/HttpURLConnection;

    return-object v0
.end method

.method private getOutputStream()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mMPOutputStream:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    return-object v0
.end method

.method private getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mSRContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    return-object v0
.end method

.method private getRequestManager()Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;
    .locals 1

    .prologue
    .line 487
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mSRManager:Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;

    return-object v0
.end method

.method private getResponse()Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;
    .locals 1

    .prologue
    .line 515
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mResponse:Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    return-object v0
.end method

.method private getSoftwareData()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    return-object v0
.end method

.method private getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    return-object v0
.end method

.method private getTimingString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 433
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "HH:mm:ss.SSS"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 435
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 438
    .local v0, "buf":Ljava/lang/StringBuffer;
    :try_start_0
    const-string/jumbo v3, "Timing Data:\n\tSend start:\t\t\t\t"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getTimeSendStart()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, "\n\tSend finish:\t\t\t"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getTimeSendFinish()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, "\n\tTime got results:\t\t"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getTimeGotResult()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, "\n\tAudio bytes written:\t"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getAudioOutputStream()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->getBytesWritten()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 452
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 448
    :catch_0
    move-exception v1

    .line 449
    .local v1, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->TAG:Ljava/lang/String;

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private insertCRCTag(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    .locals 5
    .param p1, "out"    # Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 419
    const-string/jumbo v0, "checksum"

    const-string/jumbo v1, "text/crc32"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mCRC32:Ljava/util/zip/CRC32;

    invoke-virtual {v3}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeField(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->flush()V

    .line 421
    return-void
.end method

.method private insertMetaTag(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    .locals 3
    .param p1, "out"    # Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 407
    const-string/jumbo v0, "meta"

    const-string/jumbo v1, "text/xml"

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->buildMetaElement()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeField(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->flush()V

    .line 409
    const-string/jumbo v0, "META"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->recordDetailedTiming(Ljava/lang/String;)V

    .line 410
    return-void
.end method

.method private insertTextTag(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    .locals 3
    .param p1, "out"    # Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 413
    const-string/jumbo v0, "text"

    const-string/jumbo v1, "text/plain; charset=utf-8"

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getAudioSourceInfo()Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeField(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->flush()V

    .line 415
    const-string/jumbo v0, "TEXT"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->recordDetailedTiming(Ljava/lang/String;)V

    .line 416
    return-void
.end method

.method private isErrorState(I)Z
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 473
    if-gez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isExpectingAudio()Z
    .locals 1

    .prologue
    .line 477
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isExpectingAudio:Z

    return v0
.end method

.method private log(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;Ljava/lang/String;)V
    .locals 0
    .param p1, "state"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 612
    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 619
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->log(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;Ljava/lang/String;)V

    .line 620
    return-void
.end method

.method private log(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 615
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "EXCEPTION: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->serverCheckLog(Ljava/lang/String;)V

    .line 616
    return-void
.end method

.method private notifyListeners(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    .line 327
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "notifyListeners("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->serverCheckLog(Ljava/lang/String;)V

    .line 331
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    .line 332
    .local v1, "ivListener":Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isErrorState(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 333
    invoke-interface {v1, p1}, Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;->requestFailed(I)V

    goto :goto_0

    .line 335
    :cond_0
    invoke-interface {v1, p1}, Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;->stateChanged(I)V

    goto :goto_0

    .line 338
    .end local v1    # "ivListener":Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;
    :cond_1
    return-void
.end method

.method private notifyListeners(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    .locals 3
    .param p1, "result"    # Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .prologue
    .line 317
    const-string/jumbo v2, "notify resultReceived()"

    invoke-direct {p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->serverCheckLog(Ljava/lang/String;)V

    .line 321
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    .line 322
    .local v1, "ivListener":Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;
    invoke-interface {v1, p1}, Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;->resultReceived(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    goto :goto_0

    .line 324
    .end local v1    # "ivListener":Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;
    :cond_0
    return-void
.end method

.method private onError(I)V
    .locals 1
    .param p1, "reason"    # I

    .prologue
    .line 310
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->cancel(Z)V

    .line 311
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->notifyListeners(I)V

    .line 312
    return-void
.end method

.method private onTimeout(I)V
    .locals 2
    .param p1, "reason"    # I

    .prologue
    const/4 v1, 0x1

    .line 303
    const-string/jumbo v0, "[LatencyCheck] onTimeout()"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->log(Ljava/lang/String;)V

    .line 304
    invoke-static {v1}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->setTimedOut(Z)V

    .line 305
    invoke-virtual {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->cancel(Z)V

    .line 306
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->notifyListeners(I)V

    .line 307
    return-void
.end method

.method private recordDetailedTiming(Ljava/lang/String;)V
    .locals 1
    .param p1, "event"    # Ljava/lang/String;

    .prologue
    .line 426
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    if-eqz v0, :cond_0

    .line 427
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    invoke-virtual {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;->recordAndTimeStampEvent(Ljava/lang/String;)V

    .line 429
    :cond_0
    return-void
.end method

.method private runCurrentState()V
    .locals 2

    .prologue
    .line 458
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mHandler:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 459
    return-void
.end method

.method private serverCheckLog(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 606
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "][ServerCheck] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    return-void
.end method

.method private setConnection(Ljava/net/HttpURLConnection;)V
    .locals 0
    .param p1, "connection"    # Ljava/net/HttpURLConnection;

    .prologue
    .line 521
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mConnection:Ljava/net/HttpURLConnection;

    .line 522
    return-void
.end method

.method private setOutputStream(Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;)V
    .locals 0
    .param p1, "out"    # Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    .prologue
    .line 525
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mMPOutputStream:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    .line 526
    return-void
.end method

.method private setResponse(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    .locals 2
    .param p1, "response"    # Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .prologue
    .line 529
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mResponse:Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .line 530
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getRequestManager()Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->getGUttId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRManager;->setLastGuttID(Ljava/lang/String;)V

    .line 531
    return-void
.end method

.method private startTimeoutTimer()V
    .locals 4

    .prologue
    .line 278
    iget-boolean v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isTimeoutScheduled:Z

    if-nez v1, :cond_0

    .line 279
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "starting timeout timer ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getReadTimeout()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " ms]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->serverCheckLog(Ljava/lang/String;)V

    .line 280
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V

    .line 297
    .local v0, "task":Ljava/util/TimerTask;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimeoutTimer:Ljava/util/Timer;

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getReadTimeout()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 298
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isTimeoutScheduled:Z

    .line 300
    .end local v0    # "task":Ljava/util/TimerTask;
    :cond_0
    return-void
.end method

.method private stop()V
    .locals 2

    .prologue
    .line 264
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "[LatencyCheck] stop(): background processing halted (mNetworkThread), isTimeoutScheduled ? = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isTimeoutScheduled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->log(Ljava/lang/String;)V

    .line 265
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mNetworkThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mNetworkThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 267
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mNetworkThread:Landroid/os/HandlerThread;

    .line 270
    :cond_0
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isTimeoutScheduled:Z

    if-eqz v0, :cond_1

    .line 271
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimeoutTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 272
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimeoutTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 273
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isTimeoutScheduled:Z

    .line 275
    :cond_1
    return-void
.end method


# virtual methods
.method public addListener(Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    .prologue
    .line 218
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    return-void
.end method

.method public cancel(Z)V
    .locals 1
    .param p1, "timedOut"    # Z

    .prologue
    .line 197
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->cancel(Z)V

    .line 198
    return-void
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 192
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->finish()V

    .line 193
    return-void
.end method

.method public getConnectTimeout()I
    .locals 2

    .prologue
    .line 166
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mConnectTimeout:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getOptimalConnectTimeout()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mConnectTimeout:I

    goto :goto_0
.end method

.method public getReadTimeout()I
    .locals 2

    .prologue
    .line 170
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mReadTimeout:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getOptimalConnectTimeout()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mReadTimeout:I

    goto :goto_0
.end method

.method public getTimeGotResult()J
    .locals 2

    .prologue
    .line 242
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimeGotResults:J

    return-wide v0
.end method

.method public getTimeSendFinish()J
    .locals 2

    .prologue
    .line 237
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimeSendFinish:J

    return-wide v0
.end method

.method public getTimeSendStart()J
    .locals 2

    .prologue
    .line 232
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mTimeSendStart:J

    return-wide v0
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 204
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->isCancelled()Z

    move-result v0

    return v0
.end method

.method public isResponseReceived()Z
    .locals 1

    .prologue
    .line 209
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->isResponseReceived()Z

    move-result v0

    return v0
.end method

.method public removeListener(Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    .prologue
    .line 225
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 226
    return-void
.end method

.method public sendAudio([BII)V
    .locals 1
    .param p1, "audio"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 187
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->sendAudio([BII)V

    .line 188
    return-void
.end method

.method public setConnectTimeout(I)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .locals 0
    .param p1, "timeout"    # I

    .prologue
    .line 174
    iput p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mConnectTimeout:I

    .line 175
    return-object p0
.end method

.method public setReadTimeout(I)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .locals 0
    .param p1, "timeout"    # I

    .prologue
    .line 179
    iput p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mReadTimeout:I

    .line 180
    return-object p0
.end method

.method public start()V
    .locals 3

    .prologue
    .line 246
    const-string/jumbo v0, "[LatencyCheck] mNetworkThread.start()"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->log(Ljava/lang/String;)V

    .line 248
    new-instance v0, Landroid/os/HandlerThread;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "ASRRequest:NetworkThread"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mRequestId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mNetworkThread:Landroid/os/HandlerThread;

    .line 249
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mNetworkThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 250
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mNetworkThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mHandler:Landroid/os/Handler;

    .line 252
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->runCurrentState()V

    .line 253
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->startTimeoutTimer()V

    .line 254
    return-void
.end method
