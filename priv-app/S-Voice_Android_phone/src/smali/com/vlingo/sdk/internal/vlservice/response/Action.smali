.class public Lcom/vlingo/sdk/internal/vlservice/response/Action;
.super Ljava/lang/Object;
.source "Action.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final c_VPathPrefix:Ljava/lang/String; = "${"


# instance fields
.field public elseStatement:Ljava/lang/String;

.field public ifCondition:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field protected parameters:Ljava/util/Hashtable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/vlingo/sdk/internal/vlservice/response/Action;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->parameters:Ljava/util/Hashtable;

    .line 57
    return-void
.end method

.method protected constructor <init>(Lcom/vlingo/sdk/internal/vlservice/response/Action;)V
    .locals 1
    .param p1, "a"    # Lcom/vlingo/sdk/internal/vlservice/response/Action;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iget-object v0, p1, Lcom/vlingo/sdk/internal/vlservice/response/Action;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->name:Ljava/lang/String;

    .line 39
    iget-object v0, p1, Lcom/vlingo/sdk/internal/vlservice/response/Action;->parameters:Ljava/util/Hashtable;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->parameters:Ljava/util/Hashtable;

    .line 40
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/vlservice/response/Action;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->name:Ljava/lang/String;

    .line 62
    return-void
.end method

.method private expandVariableSection(Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;)Ljava/lang/String;
    .locals 16
    .param p1, "input"    # Ljava/lang/String;
    .param p2, "results"    # Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    .prologue
    .line 96
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    const-string/jumbo v14, "${alternates"

    invoke-virtual {v13, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 97
    .local v6, "startVSection":I
    if-ltz v6, :cond_8

    .line 98
    const-string/jumbo v13, "}"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    .line 99
    .local v2, "endVSection":I
    if-ltz v2, :cond_8

    .line 100
    const-string/jumbo v11, ""

    .line 101
    .local v11, "vOutput":Ljava/lang/String;
    add-int/lit8 v13, v6, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 104
    .local v12, "vSection":Ljava/lang/String;
    const-string/jumbo v13, "alternates.wl"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_0

    const-string/jumbo v13, "alternates.ul"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 105
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->getUttResults()Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-result-object v13

    invoke-virtual {v13}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->getString()Ljava/lang/String;

    move-result-object v11

    .line 134
    :cond_1
    :goto_0
    if-nez v11, :cond_6

    .line 135
    sget-object v13, Lcom/vlingo/sdk/internal/vlservice/response/Action;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Unable to parse vSection "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    const/4 v3, 0x0

    .line 152
    .end local v2    # "endVSection":I
    .end local v11    # "vOutput":Ljava/lang/String;
    .end local v12    # "vSection":Ljava/lang/String;
    :cond_2
    :goto_1
    return-object v3

    .line 108
    .restart local v2    # "endVSection":I
    .restart local v11    # "vOutput":Ljava/lang/String;
    .restart local v12    # "vSection":Ljava/lang/String;
    :cond_3
    const-string/jumbo v13, "tag("

    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    .line 109
    .local v10, "tagnameStart":I
    if-ltz v10, :cond_1

    .line 110
    add-int/lit8 v10, v10, 0x4

    .line 111
    const-string/jumbo v13, ")"

    invoke-virtual {v12, v13, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v9

    .line 112
    .local v9, "tagnameEnd":I
    if-ltz v9, :cond_1

    .line 113
    invoke-virtual {v12, v10, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 116
    .local v8, "tagName":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->getParseGroup()Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

    move-result-object v4

    .line 117
    .local v4, "pg":Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;
    const/4 v5, 0x0

    .line 118
    .local v5, "res":Lcom/vlingo/sdk/internal/recognizer/results/RecResults;
    if-eqz v4, :cond_4

    .line 119
    invoke-virtual {v4, v8}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->lookupTagByName(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;

    move-result-object v7

    .line 120
    .local v7, "tag":Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;
    if-eqz v7, :cond_4

    .line 121
    invoke-virtual {v7}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->getRecResults()Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-result-object v5

    .line 124
    .end local v7    # "tag":Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;
    :cond_4
    add-int/lit8 v13, v9, 0x1

    invoke-virtual {v12, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 125
    .local v1, "canonicalString":Ljava/lang/String;
    if-eqz v5, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v13

    if-lez v13, :cond_5

    iget-object v13, v5, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttListCannonical:[Ljava/lang/String;

    if-eqz v13, :cond_5

    iget-object v13, v5, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttListCannonical:[Ljava/lang/String;

    array-length v13, v13

    if-lez v13, :cond_5

    .line 126
    iget-object v13, v5, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttListCannonical:[Ljava/lang/String;

    const/4 v14, 0x0

    aget-object v11, v13, v14

    goto :goto_0

    .line 129
    :cond_5
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->getString()Ljava/lang/String;

    move-result-object v11

    goto :goto_0

    .line 139
    .end local v1    # "canonicalString":Ljava/lang/String;
    .end local v4    # "pg":Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;
    .end local v5    # "res":Lcom/vlingo/sdk/internal/recognizer/results/RecResults;
    .end local v8    # "tagName":Ljava/lang/String;
    .end local v9    # "tagnameEnd":I
    .end local v10    # "tagnameStart":I
    :cond_6
    const/4 v3, 0x0

    .line 140
    .local v3, "newString":Ljava/lang/String;
    if-lez v6, :cond_7

    .line 141
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 146
    :goto_2
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    if-ge v2, v13, :cond_2

    .line 147
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    add-int/lit8 v14, v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 144
    :cond_7
    move-object v3, v11

    goto :goto_2

    .line 152
    .end local v2    # "endVSection":I
    .end local v3    # "newString":Ljava/lang/String;
    .end local v11    # "vOutput":Ljava/lang/String;
    .end local v12    # "vSection":Ljava/lang/String;
    :cond_8
    const/4 v3, 0x0

    goto/16 :goto_1
.end method

.method private parseVPath(Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;)Ljava/lang/Object;
    .locals 2
    .param p1, "vpath"    # Ljava/lang/String;
    .param p2, "results"    # Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    .prologue
    .line 83
    move-object v1, p1

    .line 85
    .local v1, "resultString":Ljava/lang/String;
    invoke-direct {p0, v1, p2}, Lcom/vlingo/sdk/internal/vlservice/response/Action;->expandVariableSection(Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;)Ljava/lang/String;

    move-result-object v0

    .line 86
    .local v0, "newString":Ljava/lang/String;
    :goto_0
    if-eqz v0, :cond_0

    .line 87
    move-object v1, v0

    .line 88
    invoke-direct {p0, v1, p2}, Lcom/vlingo/sdk/internal/vlservice/response/Action;->expandVariableSection(Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 90
    :cond_0
    return-object v1
.end method


# virtual methods
.method public addParameter(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "results"    # Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    .prologue
    .line 65
    move-object v0, p2

    .line 66
    .local v0, "val":Ljava/lang/String;
    if-eqz p3, :cond_0

    invoke-virtual {p0, p2}, Lcom/vlingo/sdk/internal/vlservice/response/Action;->isVPath(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    invoke-direct {p0, p2, p3}, Lcom/vlingo/sdk/internal/vlservice/response/Action;->parseVPath(Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;)Ljava/lang/Object;

    move-result-object v0

    .line 69
    .end local v0    # "val":Ljava/lang/String;
    :cond_0
    if-nez v0, :cond_1

    .line 70
    const-string/jumbo v0, ""

    .line 72
    :cond_1
    invoke-virtual {p0, p1, v0}, Lcom/vlingo/sdk/internal/vlservice/response/Action;->setParameterValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 73
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 49
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/vlservice/response/Action;

    .line 50
    .local v0, "clone":Lcom/vlingo/sdk/internal/vlservice/response/Action;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->parameters:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Hashtable;

    iput-object v1, v0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->parameters:Ljava/util/Hashtable;

    .line 51
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getParamValue(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 195
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 196
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->parameters:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getParameters()Ljava/util/Hashtable;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->parameters:Ljava/util/Hashtable;

    return-object v0
.end method

.method public getParams()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 274
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->parameters:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getStringParamValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 162
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/vlservice/response/Action;->getParamValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 164
    .local v1, "v":Ljava/lang/Object;
    if-nez v1, :cond_0

    .line 165
    const-string/jumbo v1, ""

    .line 171
    .end local v1    # "v":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 166
    .restart local v1    # "v":Ljava/lang/Object;
    :cond_0
    instance-of v2, v1, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    if-eqz v2, :cond_1

    move-object v0, v1

    .line 167
    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    .line 168
    .local v0, "r":Lcom/vlingo/sdk/internal/recognizer/results/RecResults;
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->getString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 171
    .end local v0    # "r":Lcom/vlingo/sdk/internal/recognizer/results/RecResults;
    :cond_1
    check-cast v1, Ljava/lang/String;

    goto :goto_0
.end method

.method public isConditional()Z
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->ifCondition:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->ifCondition:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isVPath(Ljava/lang/String;)Z
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 157
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    const-string/jumbo v0, "${"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public keys()Ljava/util/Enumeration;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->parameters:Ljava/util/Hashtable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->parameters:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    .line 269
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 204
    iput-object p1, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->name:Ljava/lang/String;

    .line 205
    return-void
.end method

.method public setParameterValue(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->parameters:Ljava/util/Hashtable;

    invoke-virtual {v0, p1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    return-void
.end method

.method public toHtmlString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 240
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 241
    .local v2, "sb":Ljava/lang/StringBuffer;
    const-string/jumbo v4, "Action: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 242
    iget-object v4, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->name:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 243
    const-string/jumbo v4, "<br/>"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 244
    const-string/jumbo v4, "    If: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 245
    iget-object v4, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->ifCondition:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 246
    const-string/jumbo v4, "<br/>"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 247
    const-string/jumbo v4, "  Else: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 248
    iget-object v4, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->elseStatement:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 249
    const-string/jumbo v4, "<br/>"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 252
    iget-object v4, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->parameters:Ljava/util/Hashtable;

    invoke-virtual {v4}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    .line 253
    .local v0, "e":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 254
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 255
    .local v1, "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->parameters:Ljava/util/Hashtable;

    invoke-virtual {v4, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 256
    .local v3, "value":Ljava/lang/Object;
    const-string/jumbo v4, "   "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 257
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 258
    const-string/jumbo v4, ": "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 259
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 260
    const-string/jumbo v4, "<br/>"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 263
    .end local v1    # "key":Ljava/lang/String;
    .end local v3    # "value":Ljava/lang/Object;
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 213
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 214
    .local v2, "sb":Ljava/lang/StringBuffer;
    const-string/jumbo v4, "Action: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 215
    iget-object v4, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->name:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 216
    const-string/jumbo v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 217
    const-string/jumbo v4, "    If: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 218
    iget-object v4, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->ifCondition:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 219
    const-string/jumbo v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 220
    const-string/jumbo v4, "  Else: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 221
    iget-object v4, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->elseStatement:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 222
    const-string/jumbo v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 225
    iget-object v4, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->parameters:Ljava/util/Hashtable;

    invoke-virtual {v4}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    .line 226
    .local v0, "e":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 227
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 228
    .local v1, "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/vlingo/sdk/internal/vlservice/response/Action;->parameters:Ljava/util/Hashtable;

    invoke-virtual {v4, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 229
    .local v3, "value":Ljava/lang/Object;
    const-string/jumbo v4, "   "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 230
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 231
    const-string/jumbo v4, ": "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 232
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 233
    const-string/jumbo v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 236
    .end local v1    # "key":Ljava/lang/String;
    .end local v3    # "value":Ljava/lang/Object;
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method
