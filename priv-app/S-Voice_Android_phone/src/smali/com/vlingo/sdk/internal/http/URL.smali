.class public Lcom/vlingo/sdk/internal/http/URL;
.super Ljava/lang/Object;
.source "URL.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field public host:Ljava/lang/String;

.field private index:I

.field public path:Ljava/lang/String;

.field public port:I

.field public urlField:Ljava/lang/String;

.field public useSSL:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/vlingo/sdk/internal/http/URL;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/http/URL;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/URL;->host:Ljava/lang/String;

    .line 26
    const/16 v0, 0x50

    iput v0, p0, Lcom/vlingo/sdk/internal/http/URL;->port:I

    .line 27
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/URL;->path:Ljava/lang/String;

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/http/URL;->useSSL:Z

    .line 31
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/URL;->urlField:Ljava/lang/String;

    .line 32
    invoke-virtual {p0, p1}, Lcom/vlingo/sdk/internal/http/URL;->parseFromURL(Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "port"    # I
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/URL;->host:Ljava/lang/String;

    .line 26
    const/16 v0, 0x50

    iput v0, p0, Lcom/vlingo/sdk/internal/http/URL;->port:I

    .line 27
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/URL;->path:Ljava/lang/String;

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/http/URL;->useSSL:Z

    .line 36
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/URL;->host:Ljava/lang/String;

    .line 37
    iput p2, p0, Lcom/vlingo/sdk/internal/http/URL;->port:I

    .line 38
    iput-object p3, p0, Lcom/vlingo/sdk/internal/http/URL;->path:Ljava/lang/String;

    .line 39
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/URL;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/URL;->urlField:Ljava/lang/String;

    .line 40
    return-void
.end method

.method private parseFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 144
    const-string/jumbo v3, ""

    .line 145
    .local v3, "token":Ljava/lang/String;
    iget v5, p0, Lcom/vlingo/sdk/internal/http/URL;->index:I

    invoke-virtual {p1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 146
    .local v0, "buf":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_0

    move-object v4, v3

    .line 158
    .end local v3    # "token":Ljava/lang/String;
    .local v4, "token":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 147
    .end local v4    # "token":Ljava/lang/String;
    .restart local v3    # "token":Ljava/lang/String;
    :cond_0
    const/16 v5, 0x23

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 148
    .local v2, "n":I
    const/16 v5, 0x3f

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 149
    .local v1, "m":I
    if-gez v2, :cond_2

    if-gez v1, :cond_2

    .line 150
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    .line 156
    :cond_1
    :goto_1
    const/4 v5, 0x0

    invoke-virtual {v0, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 157
    iget v5, p0, Lcom/vlingo/sdk/internal/http/URL;->index:I

    add-int/2addr v5, v2

    iput v5, p0, Lcom/vlingo/sdk/internal/http/URL;->index:I

    move-object v4, v3

    .line 158
    .end local v3    # "token":Ljava/lang/String;
    .restart local v4    # "token":Ljava/lang/String;
    goto :goto_0

    .line 151
    .end local v4    # "token":Ljava/lang/String;
    .restart local v3    # "token":Ljava/lang/String;
    :cond_2
    if-ltz v2, :cond_3

    if-lez v1, :cond_1

    if-ge v1, v2, :cond_1

    .line 152
    :cond_3
    move v2, v1

    goto :goto_1
.end method

.method private parseHostname(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 107
    iget v3, p0, Lcom/vlingo/sdk/internal/http/URL;->index:I

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 108
    .local v0, "buf":Ljava/lang/String;
    const-string/jumbo v3, "//"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 109
    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 110
    iget v3, p0, Lcom/vlingo/sdk/internal/http/URL;->index:I

    add-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/vlingo/sdk/internal/http/URL;->index:I

    .line 112
    :cond_0
    const/16 v3, 0x3a

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 113
    .local v1, "n":I
    if-gez v1, :cond_1

    const/16 v3, 0x2f

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 114
    :cond_1
    if-gez v1, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    .line 115
    :cond_2
    const/4 v3, 0x0

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 116
    .local v2, "token":Ljava/lang/String;
    iget v3, p0, Lcom/vlingo/sdk/internal/http/URL;->index:I

    add-int/2addr v3, v1

    iput v3, p0, Lcom/vlingo/sdk/internal/http/URL;->index:I

    .line 117
    return-object v2
.end method

.method private parsePort(Ljava/lang/String;)I
    .locals 7
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 122
    iget-boolean v5, p0, Lcom/vlingo/sdk/internal/http/URL;->useSSL:Z

    if-eqz v5, :cond_0

    const/16 v3, 0x1bb

    .line 124
    .local v3, "p":I
    :goto_0
    iget v5, p0, Lcom/vlingo/sdk/internal/http/URL;->index:I

    invoke-virtual {p1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 125
    .local v0, "buf":Ljava/lang/String;
    const-string/jumbo v5, ":"

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    move v4, v3

    .line 140
    .end local v3    # "p":I
    .local v4, "p":I
    :goto_1
    return v4

    .line 123
    .end local v0    # "buf":Ljava/lang/String;
    .end local v4    # "p":I
    :cond_0
    const/16 v3, 0x50

    .restart local v3    # "p":I
    goto :goto_0

    .line 126
    .restart local v0    # "buf":Ljava/lang/String;
    :cond_1
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 127
    iget v5, p0, Lcom/vlingo/sdk/internal/http/URL;->index:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/vlingo/sdk/internal/http/URL;->index:I

    .line 128
    const/16 v5, 0x2f

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 129
    .local v1, "n":I
    if-gez v1, :cond_2

    const/16 v5, 0x3f

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 130
    :cond_2
    if-gez v1, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    .line 132
    :cond_3
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 133
    if-gtz v3, :cond_4

    .line 134
    new-instance v5, Ljava/lang/NumberFormatException;

    invoke-direct {v5}, Ljava/lang/NumberFormatException;-><init>()V

    throw v5
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :catch_0
    move-exception v2

    .line 137
    .local v2, "nfe":Ljava/lang/NumberFormatException;
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v6, "invalid port"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 139
    .end local v2    # "nfe":Ljava/lang/NumberFormatException;
    :cond_4
    iget v5, p0, Lcom/vlingo/sdk/internal/http/URL;->index:I

    add-int/2addr v5, v1

    iput v5, p0, Lcom/vlingo/sdk/internal/http/URL;->index:I

    move v4, v3

    .line 140
    .end local v3    # "p":I
    .restart local v4    # "p":I
    goto :goto_1
.end method

.method private parseUseSSL(Ljava/lang/String;)Z
    .locals 3
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 100
    const/16 v2, 0x3a

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 101
    .local v0, "n":I
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 102
    .local v1, "token":Ljava/lang/String;
    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lcom/vlingo/sdk/internal/http/URL;->index:I

    .line 103
    const-string/jumbo v2, "https"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    return v2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "ob"    # Ljava/lang/Object;

    .prologue
    .line 70
    instance-of v1, p1, Lcom/vlingo/sdk/internal/http/URL;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 71
    check-cast v0, Lcom/vlingo/sdk/internal/http/URL;

    .line 72
    .local v0, "obURL":Lcom/vlingo/sdk/internal/http/URL;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/URL;->urlField:Ljava/lang/String;

    iget-object v2, v0, Lcom/vlingo/sdk/internal/http/URL;->urlField:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/vlingo/sdk/internal/util/StringUtils;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/URL;->host:Ljava/lang/String;

    iget-object v2, v0, Lcom/vlingo/sdk/internal/http/URL;->host:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/vlingo/sdk/internal/util/StringUtils;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/URL;->path:Ljava/lang/String;

    iget-object v2, v0, Lcom/vlingo/sdk/internal/http/URL;->path:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/vlingo/sdk/internal/util/StringUtils;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/vlingo/sdk/internal/http/URL;->port:I

    iget v2, v0, Lcom/vlingo/sdk/internal/http/URL;->port:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/vlingo/sdk/internal/http/URL;->useSSL:Z

    iget-boolean v2, v0, Lcom/vlingo/sdk/internal/http/URL;->useSSL:Z

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    .line 78
    .end local v0    # "obURL":Lcom/vlingo/sdk/internal/http/URL;
    :goto_0
    return v1

    .line 72
    .restart local v0    # "obURL":Lcom/vlingo/sdk/internal/http/URL;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 78
    .end local v0    # "obURL":Lcom/vlingo/sdk/internal/http/URL;
    :cond_1
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/URL;->host:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/URL;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getPort()I
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lcom/vlingo/sdk/internal/http/URL;->port:I

    return v0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/http/URL;->useSSL:Z

    if-eqz v0, :cond_0

    .line 187
    const-string/jumbo v0, "https"

    .line 189
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "http"

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 58
    iget v1, p0, Lcom/vlingo/sdk/internal/http/URL;->port:I

    mul-int/lit8 v2, v1, 0x3

    iget-boolean v1, p0, Lcom/vlingo/sdk/internal/http/URL;->useSSL:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x7

    :goto_0
    mul-int v0, v2, v1

    .line 59
    .local v0, "hash":I
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/URL;->urlField:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 60
    mul-int/lit8 v1, v0, 0x59

    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/URL;->urlField:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 61
    :cond_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/URL;->host:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 62
    mul-int/lit8 v1, v0, 0x59

    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/URL;->host:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 63
    :cond_1
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/URL;->path:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 64
    mul-int/lit8 v1, v0, 0x59

    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/URL;->path:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 65
    :cond_2
    return v0

    .line 58
    .end local v0    # "hash":I
    :cond_3
    const/16 v1, 0xb

    goto :goto_0
.end method

.method public parseFromURL(Ljava/lang/String;)V
    .locals 4
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 43
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x5

    if-le v1, v2, :cond_0

    .line 45
    const/4 v1, 0x0

    :try_start_0
    iput v1, p0, Lcom/vlingo/sdk/internal/http/URL;->index:I

    .line 46
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/http/URL;->parseUseSSL(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/vlingo/sdk/internal/http/URL;->useSSL:Z

    .line 47
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/http/URL;->parseHostname(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/sdk/internal/http/URL;->host:Ljava/lang/String;

    .line 48
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/http/URL;->parsePort(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/vlingo/sdk/internal/http/URL;->port:I

    .line 49
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/http/URL;->parseFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/sdk/internal/http/URL;->path:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 50
    :catch_0
    move-exception v0

    .line 51
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/vlingo/sdk/internal/http/URL;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Malformed URL: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " url="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setHost(Ljava/lang/String;)V
    .locals 0
    .param p1, "host"    # Ljava/lang/String;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/URL;->host:Ljava/lang/String;

    .line 167
    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/URL;->path:Ljava/lang/String;

    .line 175
    return-void
.end method

.method public setPort(I)V
    .locals 0
    .param p1, "port"    # I

    .prologue
    .line 182
    iput p1, p0, Lcom/vlingo/sdk/internal/http/URL;->port:I

    .line 183
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 84
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-boolean v1, p0, Lcom/vlingo/sdk/internal/http/URL;->useSSL:Z

    if-eqz v1, :cond_0

    .line 85
    const-string/jumbo v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    :goto_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/URL;->host:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    iget v1, p0, Lcom/vlingo/sdk/internal/http/URL;->port:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 91
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/URL;->path:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 87
    :cond_0
    const-string/jumbo v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
