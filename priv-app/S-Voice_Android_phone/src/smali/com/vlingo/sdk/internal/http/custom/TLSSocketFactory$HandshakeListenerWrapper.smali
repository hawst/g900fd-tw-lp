.class Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory$HandshakeListenerWrapper;
.super Ljava/lang/Object;
.source "TLSSocketFactory.java"

# interfaces
.implements Ljavax/net/ssl/HandshakeCompletedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HandshakeListenerWrapper"
.end annotation


# instance fields
.field private cbCount:I

.field private innerListener:Ljavax/net/ssl/HandshakeCompletedListener;

.field final synthetic this$0:Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;Ljavax/net/ssl/HandshakeCompletedListener;)V
    .locals 1
    .param p2, "listener"    # Ljavax/net/ssl/HandshakeCompletedListener;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory$HandshakeListenerWrapper;->this$0:Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory$HandshakeListenerWrapper;->cbCount:I

    .line 50
    iput-object p2, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory$HandshakeListenerWrapper;->innerListener:Ljavax/net/ssl/HandshakeCompletedListener;

    .line 51
    return-void
.end method


# virtual methods
.method public handshakeCompleted(Ljavax/net/ssl/HandshakeCompletedEvent;)V
    .locals 4
    .param p1, "event"    # Ljavax/net/ssl/HandshakeCompletedEvent;

    .prologue
    .line 55
    invoke-virtual {p1}, Ljavax/net/ssl/HandshakeCompletedEvent;->getSocket()Ljavax/net/ssl/SSLSocket;

    move-result-object v0

    .line 56
    .local v0, "socket":Ljavax/net/ssl/SSLSocket;
    iget v1, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory$HandshakeListenerWrapper;->cbCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory$HandshakeListenerWrapper;->cbCount:I

    .line 57
    # getter for: Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "handshakeCompleted: cbCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory$HandshakeListenerWrapper;->cbCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "; createCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory$HandshakeListenerWrapper;->this$0:Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;

    iget v3, v3, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->createSocketCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "; ipAddr="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "; protocol="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v3

    invoke-interface {v3}, Ljavax/net/ssl/SSLSession;->getProtocol()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "; cipher suite="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljavax/net/ssl/HandshakeCompletedEvent;->getCipherSuite()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "; socket="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory$HandshakeListenerWrapper;->this$0:Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;

    # getter for: Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->socketHashCode:I
    invoke-static {v3}, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->access$100(Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "; conn="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory$HandshakeListenerWrapper;->this$0:Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;

    # getter for: Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->httpsConn:Ljavax/net/ssl/HttpsURLConnection;
    invoke-static {v3}, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->access$200(Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;)Ljavax/net/ssl/HttpsURLConnection;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory$HandshakeListenerWrapper;->innerListener:Ljavax/net/ssl/HandshakeCompletedListener;

    if-eqz v1, :cond_0

    .line 60
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory$HandshakeListenerWrapper;->innerListener:Ljavax/net/ssl/HandshakeCompletedListener;

    invoke-interface {v1, p1}, Ljavax/net/ssl/HandshakeCompletedListener;->handshakeCompleted(Ljavax/net/ssl/HandshakeCompletedEvent;)V

    .line 62
    :cond_0
    return-void
.end method
