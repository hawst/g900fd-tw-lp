.class Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;
.super Landroid/os/Handler;
.source "VLTextToSpeechImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NotificationHandler"
.end annotation


# static fields
.field static final ERROR:I = 0x2

.field static final SUCCESS:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;)V
    .locals 1

    .prologue
    .line 45
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;

    .line 46
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 47
    return-void
.end method


# virtual methods
.method declared-synchronized cancelNotifications()V
    .locals 1

    .prologue
    .line 81
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;->removeMessages(I)V

    .line 82
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;->removeMessages(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    monitor-exit p0

    return-void

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x0

    .line 51
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;

    invoke-virtual {v3, v5}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->setBusy(Z)V

    .line 53
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mTextToSpeechListener:Lcom/vlingo/sdk/tts/VLTextToSpeechListener;
    invoke-static {v3}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->access$000(Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;)Lcom/vlingo/sdk/tts/VLTextToSpeechListener;

    move-result-object v2

    .line 54
    .local v2, "l":Lcom/vlingo/sdk/tts/VLTextToSpeechListener;
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;

    const/4 v4, 0x0

    # setter for: Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mTextToSpeechListener:Lcom/vlingo/sdk/tts/VLTextToSpeechListener;
    invoke-static {v3, v4}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->access$002(Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;Lcom/vlingo/sdk/tts/VLTextToSpeechListener;)Lcom/vlingo/sdk/tts/VLTextToSpeechListener;

    .line 56
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 58
    :pswitch_0
    if-eqz v2, :cond_0

    .line 59
    invoke-interface {v2}, Lcom/vlingo/sdk/tts/VLTextToSpeechListener;->onSuccess()V

    goto :goto_0

    .line 63
    :pswitch_1
    if-eqz v2, :cond_0

    .line 64
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, [Ljava/lang/Object;

    check-cast v3, [Ljava/lang/Object;

    aget-object v0, v3, v5

    check-cast v0, Lcom/vlingo/sdk/tts/VLTextToSpeechErrors;

    .line 65
    .local v0, "errorCode":Lcom/vlingo/sdk/tts/VLTextToSpeechErrors;
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, [Ljava/lang/Object;

    check-cast v3, [Ljava/lang/Object;

    const/4 v4, 0x1

    aget-object v1, v3, v4

    check-cast v1, Ljava/lang/String;

    .line 66
    .local v1, "errorMsg":Ljava/lang/String;
    invoke-interface {v2, v0, v1}, Lcom/vlingo/sdk/tts/VLTextToSpeechListener;->onError(Lcom/vlingo/sdk/tts/VLTextToSpeechErrors;Ljava/lang/String;)V

    goto :goto_0

    .line 56
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method declared-synchronized notifyError(Lcom/vlingo/sdk/tts/VLTextToSpeechErrors;Ljava/lang/String;)V
    .locals 3
    .param p1, "error"    # Lcom/vlingo/sdk/tts/VLTextToSpeechErrors;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 77
    monitor-enter p0

    const/4 v0, 0x2

    const/4 v1, 0x2

    :try_start_0
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    monitor-exit p0

    return-void

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized notifySuccess()V
    .locals 1

    .prologue
    .line 73
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    monitor-exit p0

    return-void

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
