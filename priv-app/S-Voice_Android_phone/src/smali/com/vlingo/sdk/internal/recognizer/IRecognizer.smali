.class public interface abstract Lcom/vlingo/sdk/internal/recognizer/IRecognizer;
.super Ljava/lang/Object;
.source "IRecognizer.java"


# virtual methods
.method public abstract completeAudioRecognition()V
.end method

.method public abstract destroy()V
.end method

.method public abstract onCancelled()V
.end method

.method public abstract onRecordingEnpointed()V
.end method

.method public abstract onRecordingFinished()V
.end method

.method public abstract onRecordingStarted()V
.end method

.method public abstract prepareForRequest(Lcom/vlingo/sdk/internal/recognizer/SRContext;ZLcom/vlingo/sdk/internal/recognizer/RecognizerListener;)Z
.end method
