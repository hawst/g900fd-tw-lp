.class public Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;
.super Ljava/lang/Object;
.source "VoconJNI.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;
    }
.end annotation


# static fields
.field private static volatile sInstance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;


# instance fields
.field private mGrammarLoaded:Z

.field private mInit:Z

.field private mRecognizing:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->sInstance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    .line 31
    const-string/jumbo v0, "libvocon3200_platform.so"

    invoke-static {v0}, Lcom/vlingo/sdk/util/SystemUtil;->loadLibrary(Ljava/lang/String;)V

    .line 32
    const-string/jumbo v0, "libvocon3200_base.so"

    invoke-static {v0}, Lcom/vlingo/sdk/util/SystemUtil;->loadLibrary(Ljava/lang/String;)V

    .line 33
    const-string/jumbo v0, "libgenericdca.so"

    invoke-static {v0}, Lcom/vlingo/sdk/util/SystemUtil;->loadLibrary(Ljava/lang/String;)V

    .line 34
    const-string/jumbo v0, "libvocon_jni.so"

    invoke-static {v0}, Lcom/vlingo/sdk/util/SystemUtil;->loadLibrary(Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->mInit:Z

    .line 13
    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->mGrammarLoaded:Z

    .line 14
    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->mRecognizing:Z

    .line 27
    return-void
.end method

.method private native activateAllGrammarRulesJNI()Z
.end method

.method private native activateGrammarRuleJNI(Ljava/lang/String;)Z
.end method

.method private native addTerminalsToVocabularyJNI(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;)J
.end method

.method private native cancelRecognitionJNI()V
.end method

.method private native clearMergedGrammarsJNI()Z
.end method

.method private native clearTerminalsFromVocabularyJNI(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;)Z
.end method

.method private native commitVocabulariesJNI()Z
.end method

.method private native commitVocabularyJNI(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;)Z
.end method

.method private native deactivateAllGrammarRulesJNI()Z
.end method

.method private native deactivateGrammarRuleJNI(Ljava/lang/String;)Z
.end method

.method private enforceGrammarNotLoaded()V
    .locals 2

    .prologue
    .line 563
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->isGrammarLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Grammar Not Loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 566
    :cond_0
    return-void
.end method

.method private enforceGrammarUpload()V
    .locals 2

    .prologue
    .line 569
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->isGrammarLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 570
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Grammar Not Loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 572
    :cond_0
    return-void
.end method

.method private enforceInitialization()V
    .locals 2

    .prologue
    .line 551
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->isInit()Z

    move-result v0

    if-nez v0, :cond_0

    .line 552
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Not Initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 554
    :cond_0
    return-void
.end method

.method private enforceNotRecognizing()V
    .locals 2

    .prologue
    .line 575
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->isRecognizing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 576
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Already Recognizing"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 578
    :cond_0
    return-void
.end method

.method private enforcePreconditions(Ljava/util/EnumSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 530
    .local p1, "preconditions":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;>;"
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-virtual {p1, v0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 531
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforceInitialization()V

    .line 533
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-virtual {p1, v0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 534
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforceUnInitialization()V

    .line 536
    :cond_1
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-virtual {p1, v0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 537
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforceGrammarUpload()V

    .line 539
    :cond_2
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-virtual {p1, v0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 540
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforceNotRecognizing()V

    .line 542
    :cond_3
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_NOT_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-virtual {p1, v0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 543
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforceGrammarNotLoaded()V

    .line 545
    :cond_4
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-virtual {p1, v0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 546
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforceRecognizing()V

    .line 548
    :cond_5
    return-void
.end method

.method private enforceRecognizing()V
    .locals 2

    .prologue
    .line 581
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->isRecognizing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 582
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Not Recognizing"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 584
    :cond_0
    return-void
.end method

.method private enforceUnInitialization()V
    .locals 2

    .prologue
    .line 557
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->isInit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 558
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Not Initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 560
    :cond_0
    return-void
.end method

.method private native getGrammarRulesJNI()[Ljava/lang/String;
.end method

.method static getInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->sInstance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    if-nez v0, :cond_0

    .line 19
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;-><init>()V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->sInstance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    .line 22
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->sInstance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon;

    return-object v0
.end method

.method private native getMergedGrammarsJNI()[Ljava/lang/String;
.end method

.method private native loadGrammarJNI(Ljava/lang/String;[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;)Z
.end method

.method private native mergeGrammarJNI(Ljava/lang/String;)Z
.end method

.method private native notifyNoMoreAudioJNI()Ljava/lang/String;
.end method

.method private native pushAudioFrameJNI([S)Ljava/lang/String;
.end method

.method private native removeMergedGrammarJNI(Ljava/lang/String;)Z
.end method

.method private native removeTerminalsFromVocabularyJNI(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;)J
.end method

.method private native setLeadingTimeoutJNI(I)Z
.end method

.method private native setTrailingTimeoutJNI(I)Z
.end method

.method private native startRecognitionJNI()Z
.end method

.method private native uninitJNI()Z
.end method

.method private native unloadGrammarJNI()Z
.end method


# virtual methods
.method public declared-synchronized activateAllGrammarRules()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 413
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v0, v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 415
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->activateAllGrammarRulesJNI()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 413
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized activateGrammarRule(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "grammarName"    # Ljava/lang/String;
    .param p2, "ruleName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 377
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v3, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v1, v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 379
    const/4 v0, 0x0

    .line 381
    .local v0, "ret":Z
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 382
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->activateGrammarRuleJNI(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 385
    :cond_0
    monitor-exit p0

    return v0

    .line 377
    .end local v0    # "ret":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized addTerminalToVocabulary(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;)Z
    .locals 7
    .param p1, "grammarVocabulary"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;
    .param p2, "terminal"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 252
    monitor-enter p0

    :try_start_0
    sget-object v4, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v5, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v6, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v4, v5, v6}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 254
    const-wide/16 v0, -0x1

    .line 255
    .local v0, "ret":J
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 256
    const/4 v4, 0x1

    new-array v4, v4, [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-direct {p0, p1, v4}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->addTerminalsToVocabularyJNI(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 260
    :cond_0
    const-wide/16 v4, 0x1

    cmp-long v4, v0, v4

    if-nez v4, :cond_1

    :goto_0
    monitor-exit p0

    return v2

    :cond_1
    move v2, v3

    goto :goto_0

    .line 252
    .end local v0    # "ret":J
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized addTerminalsToVocabulary(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;)J
    .locals 7
    .param p1, "grammarVocabulary"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;
    .param p2, "terminals"    # [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 224
    monitor-enter p0

    :try_start_0
    sget-object v4, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v5, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v6, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v4, v5, v6}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 226
    const-wide/16 v1, -0x1

    .line 227
    .local v1, "ret":J
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 228
    array-length v4, p2

    if-lez v4, :cond_1

    .line 229
    const/4 v0, 0x0

    .line 231
    .local v0, "nullTerminalFound":Z
    const/4 v3, 0x0

    .local v3, "terminalCnt":I
    :goto_0
    array-length v4, p2

    if-ge v3, v4, :cond_0

    .line 232
    aget-object v4, p2, v3

    if-nez v4, :cond_2

    .line 233
    const/4 v0, 0x1

    .line 238
    :cond_0
    if-nez v0, :cond_1

    .line 239
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->addTerminalsToVocabularyJNI(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v1

    .line 245
    .end local v0    # "nullTerminalFound":Z
    .end local v3    # "terminalCnt":I
    :cond_1
    monitor-exit p0

    return-wide v1

    .line 231
    .restart local v0    # "nullTerminalFound":Z
    .restart local v3    # "terminalCnt":I
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 224
    .end local v0    # "nullTerminalFound":Z
    .end local v1    # "ret":J
    .end local v3    # "terminalCnt":I
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public declared-synchronized cancelRecognition()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 514
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 516
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->cancelRecognitionJNI()V

    .line 517
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->setRecognizing(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 518
    monitor-exit p0

    return-void

    .line 514
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized clearMergedGrammars()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 449
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v0, v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 451
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->clearMergedGrammarsJNI()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 449
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized clearTerminalsFromVocabulary(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;)Z
    .locals 4
    .param p1, "grammarVocabulary"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 309
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v3, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v1, v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 311
    const/4 v0, 0x0

    .line 313
    .local v0, "ret":Z
    if-eqz p1, :cond_0

    .line 314
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->clearTerminalsFromVocabularyJNI(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 317
    :cond_0
    monitor-exit p0

    return v0

    .line 309
    .end local v0    # "ret":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized commitVocabularies()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 337
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v0, v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 339
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->commitVocabulariesJNI()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 337
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized commitVocabulary(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;)Z
    .locals 4
    .param p1, "grammarVocabulary"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 323
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v3, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v1, v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 325
    const/4 v0, 0x0

    .line 327
    .local v0, "ret":Z
    if-eqz p1, :cond_0

    .line 328
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->commitVocabularyJNI(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 331
    :cond_0
    monitor-exit p0

    return v0

    .line 323
    .end local v0    # "ret":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized deactivateAllGrammarRules()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 405
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v0, v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 407
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->deactivateAllGrammarRulesJNI()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 405
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized deactivateGrammarRule(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "grammarName"    # Ljava/lang/String;
    .param p2, "ruleName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 391
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v3, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v1, v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 393
    const/4 v0, 0x0

    .line 395
    .local v0, "ret":Z
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 396
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->deactivateGrammarRuleJNI(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 399
    :cond_0
    monitor-exit p0

    return v0

    .line 391
    .end local v0    # "ret":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getGrammarRules()Ljava/util/Set;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    const/4 v10, 0x2

    .line 345
    monitor-enter p0

    :try_start_0
    sget-object v7, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v8, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v9, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v7, v8, v9}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 347
    const/4 v3, 0x0

    .line 349
    .local v3, "ret":Ljava/util/Set;, "Ljava/util/Set<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->getGrammarRulesJNI()[Ljava/lang/String;

    move-result-object v1

    .line 350
    .local v1, "fullyQualifiedRules":[Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 351
    new-instance v3, Ljava/util/HashSet;

    .end local v3    # "ret":Ljava/util/Set;, "Ljava/util/Set<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 353
    .restart local v3    # "ret":Ljava/util/Set;, "Ljava/util/Set<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/4 v0, 0x0

    .local v0, "cnt":I
    :goto_0
    array-length v7, v1

    if-ge v0, v7, :cond_2

    .line 354
    aget-object v5, v1, v0

    .line 355
    .local v5, "thisFullyQualifiedRule":Ljava/lang/String;
    if-nez v5, :cond_1

    .line 353
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 358
    :cond_1
    const-string/jumbo v7, "#"

    const/4 v8, 0x2

    invoke-virtual {v5, v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v6

    .line 359
    .local v6, "tokens":[Ljava/lang/String;
    if-eqz v6, :cond_0

    array-length v7, v6

    if-lt v7, v10, :cond_0

    .line 360
    const/4 v7, 0x0

    aget-object v2, v6, v7

    .line 361
    .local v2, "grammarName":Ljava/lang/String;
    const/4 v7, 0x1

    aget-object v4, v6, v7

    .line 363
    .local v4, "ruleName":Ljava/lang/String;
    if-eqz v2, :cond_0

    if-eqz v4, :cond_0

    .line 364
    new-instance v7, Landroid/util/Pair;

    invoke-direct {v7, v2, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v3, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 345
    .end local v0    # "cnt":I
    .end local v1    # "fullyQualifiedRules":[Ljava/lang/String;
    .end local v2    # "grammarName":Ljava/lang/String;
    .end local v3    # "ret":Ljava/util/Set;, "Ljava/util/Set<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v4    # "ruleName":Ljava/lang/String;
    .end local v5    # "thisFullyQualifiedRule":Ljava/lang/String;
    .end local v6    # "tokens":[Ljava/lang/String;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    .line 371
    .restart local v1    # "fullyQualifiedRules":[Ljava/lang/String;
    .restart local v3    # "ret":Ljava/util/Set;, "Ljava/util/Set<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    :cond_2
    monitor-exit p0

    return-object v3
.end method

.method public declared-synchronized getMergedGrammars()[Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 457
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v0, v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 459
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->getMergedGrammarsJNI()[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 457
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized init(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "modelFilePath"    # Ljava/lang/String;
    .param p2, "clcFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 122
    const/4 v0, 0x0

    .line 124
    .local v0, "ret":Z
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 125
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->initJNI(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 126
    if-eqz v0, :cond_0

    .line 127
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->setInit(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    :cond_0
    monitor-exit p0

    return v0

    .line 120
    .end local v0    # "ret":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public native initJNI(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public declared-synchronized isGrammarLoaded()Z
    .locals 1

    .prologue
    .line 101
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->mGrammarLoaded:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isInit()Z
    .locals 1

    .prologue
    .line 92
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->mInit:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isRecognizing()Z
    .locals 1

    .prologue
    .line 110
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->mRecognizing:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized loadGrammar(Ljava/lang/String;[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;)Z
    .locals 5
    .param p1, "grammarFilePath"    # Ljava/lang/String;
    .param p2, "vocabularies"    # [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 196
    monitor-enter p0

    :try_start_0
    sget-object v3, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v4, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_NOT_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v3, v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 198
    const/4 v2, 0x0

    .line 200
    .local v2, "ret":Z
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    array-length v3, p2

    if-lez v3, :cond_2

    .line 201
    const/4 v1, 0x0

    .line 203
    .local v1, "nullFound":Z
    const/4 v0, 0x0

    .local v0, "cnt":I
    :goto_0
    array-length v3, p2

    if-ge v0, v3, :cond_1

    .line 204
    aget-object v3, p2, v0

    if-nez v3, :cond_0

    .line 205
    const/4 v1, 0x0

    .line 203
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 209
    :cond_1
    if-nez v1, :cond_2

    .line 210
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->loadGrammarJNI(Ljava/lang/String;[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;)Z

    move-result v2

    .line 211
    if-eqz v2, :cond_2

    .line 212
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->setGrammarLoaded(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 217
    .end local v0    # "cnt":I
    .end local v1    # "nullFound":Z
    :cond_2
    monitor-exit p0

    return v2

    .line 196
    .end local v2    # "ret":Z
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized mergeGrammar(Ljava/lang/String;)Z
    .locals 4
    .param p1, "grammarPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 422
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v3, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v1, v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 424
    const/4 v0, 0x0

    .line 426
    .local v0, "ret":Z
    if-eqz p1, :cond_0

    .line 427
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->mergeGrammarJNI(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 430
    :cond_0
    monitor-exit p0

    return v0

    .line 422
    .end local v0    # "ret":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized pushAudioFrame([S)Ljava/lang/String;
    .locals 4
    .param p1, "chunks"    # [S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon$RecognitionException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 487
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v3, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v1, v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 489
    const/4 v0, 0x0

    .line 491
    .local v0, "ret":Ljava/lang/String;
    if-eqz p1, :cond_2

    .line 492
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->pushAudioFrameJNI([S)Ljava/lang/String;

    move-result-object v0

    .line 497
    :goto_0
    if-eqz v0, :cond_0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 498
    const/4 v0, 0x0

    .line 501
    :cond_0
    if-eqz v0, :cond_1

    .line 502
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->setRecognizing(Z)V

    .line 505
    :cond_1
    const-string/jumbo v1, "Error"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 506
    new-instance v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon$RecognitionException;

    invoke-direct {v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/NativeVocon$RecognitionException;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 487
    .end local v0    # "ret":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 494
    .restart local v0    # "ret":Ljava/lang/String;
    :cond_2
    :try_start_1
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->notifyNoMoreAudioJNI()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 509
    :cond_3
    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized removeMergedGrammar(Ljava/lang/String;)Z
    .locals 4
    .param p1, "grammarPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 436
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v3, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v1, v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 438
    const/4 v0, 0x0

    .line 440
    .local v0, "ret":Z
    if-eqz p1, :cond_0

    .line 441
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->removeMergedGrammarJNI(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 443
    :cond_0
    monitor-exit p0

    return v0

    .line 436
    .end local v0    # "ret":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized removeTerminalFromVocabulary(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;)Z
    .locals 7
    .param p1, "grammarVocabulary"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;
    .param p2, "terminal"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 295
    monitor-enter p0

    :try_start_0
    sget-object v4, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v5, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v6, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v4, v5, v6}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 297
    const-wide/16 v0, -0x1

    .line 298
    .local v0, "ret":J
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 299
    const/4 v4, 0x1

    new-array v4, v4, [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-direct {p0, p1, v4}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->removeTerminalsFromVocabularyJNI(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 303
    :cond_0
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-lez v4, :cond_1

    :goto_0
    monitor-exit p0

    return v2

    :cond_1
    move v2, v3

    goto :goto_0

    .line 295
    .end local v0    # "ret":J
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized removeTerminalsFromVocabulary(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;)J
    .locals 7
    .param p1, "grammarVocabulary"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;
    .param p2, "terminals"    # [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 267
    monitor-enter p0

    :try_start_0
    sget-object v4, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v5, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v6, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v4, v5, v6}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 269
    const-wide/16 v1, -0x1

    .line 270
    .local v1, "ret":J
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 271
    array-length v4, p2

    if-lez v4, :cond_1

    .line 272
    const/4 v0, 0x0

    .line 274
    .local v0, "nullTerminalFound":Z
    const/4 v3, 0x0

    .local v3, "terminalCnt":I
    :goto_0
    array-length v4, p2

    if-ge v3, v4, :cond_0

    .line 275
    aget-object v4, p2, v3

    if-nez v4, :cond_2

    .line 276
    const/4 v0, 0x1

    .line 281
    :cond_0
    if-nez v0, :cond_1

    .line 282
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->removeTerminalsFromVocabularyJNI(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarTerminal;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v1

    .line 288
    .end local v0    # "nullTerminalFound":Z
    .end local v3    # "terminalCnt":I
    :cond_1
    monitor-exit p0

    return-wide v1

    .line 274
    .restart local v0    # "nullTerminalFound":Z
    .restart local v3    # "terminalCnt":I
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 267
    .end local v0    # "nullTerminalFound":Z
    .end local v1    # "ret":J
    .end local v3    # "terminalCnt":I
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public declared-synchronized resetTimeoutAfterSpeech()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 188
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 190
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->setTrailingTimeoutJNI(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized resetTimeoutBeforeSpeech()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 165
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 167
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->setLeadingTimeoutJNI(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 165
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized setGrammarLoaded(Z)V
    .locals 1
    .param p1, "grammarLoaded"    # Z

    .prologue
    .line 105
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->mGrammarLoaded:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    monitor-exit p0

    return-void

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized setInit(Z)V
    .locals 1
    .param p1, "init"    # Z

    .prologue
    .line 96
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->mInit:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    monitor-exit p0

    return-void

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized setRecognizing(Z)V
    .locals 1
    .param p1, "recognizing"    # Z

    .prologue
    .line 114
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->mRecognizing:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    monitor-exit p0

    return-void

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setTimeoutAfterSpeech(I)Z
    .locals 2
    .param p1, "timeoutMS"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 173
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 175
    move v0, p1

    .line 176
    .local v0, "val":I
    const/16 v1, 0x64

    if-ge v0, v1, :cond_1

    .line 177
    const/16 v0, 0x64

    .line 182
    :cond_0
    :goto_0
    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->setTrailingTimeoutJNI(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    monitor-exit p0

    return v1

    .line 178
    :cond_1
    const/16 v1, 0x2710

    if-le v0, v1, :cond_0

    .line 179
    const/16 v0, 0x2710

    goto :goto_0

    .line 173
    .end local v0    # "val":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized setTimeoutBeforeSpeech(I)Z
    .locals 2
    .param p1, "timeoutMS"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 150
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 152
    move v0, p1

    .line 153
    .local v0, "val":I
    const/16 v1, 0x64

    if-ge v0, v1, :cond_1

    .line 154
    const/16 v0, 0x64

    .line 159
    :cond_0
    :goto_0
    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->setLeadingTimeoutJNI(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    monitor-exit p0

    return v1

    .line 155
    :cond_1
    const/16 v1, 0x2710

    if-le v0, v1, :cond_0

    .line 156
    const/16 v0, 0x2710

    goto :goto_0

    .line 150
    .end local v0    # "val":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized startRecognition()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 475
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v3, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v1, v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 477
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->startRecognitionJNI()Z

    move-result v0

    .line 478
    .local v0, "ret":Z
    if-eqz v0, :cond_0

    .line 479
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->setRecognizing(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 481
    :cond_0
    monitor-exit p0

    return v0

    .line 475
    .end local v0    # "ret":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized uninit()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 136
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 138
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->uninitJNI()Z

    move-result v0

    .line 139
    .local v0, "ret":Z
    if-eqz v0, :cond_0

    .line 140
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->setInit(Z)V

    .line 141
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->setGrammarLoaded(Z)V

    .line 142
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->setRecognizing(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    :cond_0
    monitor-exit p0

    return v0

    .line 136
    .end local v0    # "ret":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized unloadGrammar()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 464
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v3, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v1, v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->enforcePreconditions(Ljava/util/EnumSet;)V

    .line 466
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->unloadGrammarJNI()Z

    move-result v0

    .line 467
    .local v0, "ret":Z
    if-eqz v0, :cond_0

    .line 468
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;->setGrammarLoaded(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 470
    :cond_0
    monitor-exit p0

    return v0

    .line 464
    .end local v0    # "ret":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
