.class public Lcom/vlingo/sdk/internal/http/BaseHttpCallback;
.super Ljava/lang/Object;
.source "BaseHttpCallback.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/http/HttpCallback;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelled(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 35
    return-void
.end method

.method public onFailure(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 32
    return-void
.end method

.method public onResponse(Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpResponse;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;
    .param p2, "response"    # Lcom/vlingo/sdk/internal/http/HttpResponse;

    .prologue
    .line 25
    return-void
.end method

.method public onTimeout(Lcom/vlingo/sdk/internal/http/HttpRequest;)Z
    .locals 1
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 28
    const/4 v0, 0x1

    return v0
.end method

.method public onWillExecuteRequest(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 22
    return-void
.end method
