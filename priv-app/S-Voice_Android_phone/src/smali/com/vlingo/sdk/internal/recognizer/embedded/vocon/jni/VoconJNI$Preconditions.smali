.class final enum Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;
.super Ljava/lang/Enum;
.source "VoconJNI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Preconditions"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

.field public static final enum GRAMMAR_NOT_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

.field public static final enum GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

.field public static final enum INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

.field public static final enum NOT_INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

.field public static final enum NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

.field public static final enum RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 521
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    const-string/jumbo v1, "INITIALIZED"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    .line 522
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    const-string/jumbo v1, "NOT_INITIALIZED"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    .line 523
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    const-string/jumbo v1, "GRAMMAR_UPLOADED"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    .line 524
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    const-string/jumbo v1, "GRAMMAR_NOT_UPLOADED"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_NOT_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    .line 525
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    const-string/jumbo v1, "NOT_RECOGNIZING"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    .line 526
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    const-string/jumbo v1, "RECOGNIZING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    .line 520
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_INITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->GRAMMAR_NOT_UPLOADED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->NOT_RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->$VALUES:[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 520
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 520
    const-class v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;
    .locals 1

    .prologue
    .line 520
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->$VALUES:[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    invoke-virtual {v0}, [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/VoconJNI$Preconditions;

    return-object v0
.end method
