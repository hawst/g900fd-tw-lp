.class Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;
.super Landroid/os/Handler;
.source "VLRecognizerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/VLRecognizerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NotificationHandler"
.end annotation


# static fields
.field static final ERROR:I = 0x5

.field static final RESULT:I = 0x3

.field static final RMS:I = 0x2

.field static final STATE:I = 0x1

.field static final WARNING:I = 0x4


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)V
    .locals 1

    .prologue
    .line 57
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    .line 58
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 59
    return-void
.end method


# virtual methods
.method declared-synchronized clear()V
    .locals 1

    .prologue
    .line 125
    monitor-enter p0

    const/4 v0, 0x2

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->removeMessages(I)V

    .line 126
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->removeMessages(I)V

    .line 127
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->removeMessages(I)V

    .line 128
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->removeMessages(I)V

    .line 129
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->removeMessages(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    monitor-exit p0

    return-void

    .line 125
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 63
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 65
    :pswitch_0
    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    invoke-static {v5}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$000(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 66
    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    invoke-static {v5}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$000(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    move-result-object v6

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-interface {v6, v5}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onRmsChanged(I)V

    goto :goto_0

    .line 70
    :pswitch_1
    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    invoke-static {v5}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$000(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 71
    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    invoke-static {v5}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$000(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    move-result-object v6

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-interface {v6, v5}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onStateChanged(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V

    goto :goto_0

    .line 75
    :pswitch_2
    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    invoke-virtual {v5, v6}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->setBusy(Z)V

    .line 76
    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    invoke-static {v5}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$000(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 78
    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    invoke-static {v5}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$000(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    move-result-object v2

    .line 79
    .local v2, "l":Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # setter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    invoke-static {v5, v8}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$002(Lcom/vlingo/sdk/internal/VLRecognizerImpl;Lcom/vlingo/sdk/recognition/VLRecognitionListener;)Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    .line 80
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    invoke-interface {v2, v5}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onRecognitionResults(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)V

    goto :goto_0

    .line 84
    .end local v2    # "l":Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    :pswitch_3
    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    invoke-static {v5}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$000(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 85
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, [Ljava/lang/Object;

    check-cast v5, [Ljava/lang/Object;

    aget-object v3, v5, v6

    check-cast v3, Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;

    .line 86
    .local v3, "warningCode":Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, [Ljava/lang/Object;

    check-cast v5, [Ljava/lang/Object;

    aget-object v4, v5, v7

    check-cast v4, Ljava/lang/String;

    .line 87
    .local v4, "warningMsg":Ljava/lang/String;
    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    invoke-static {v5}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$000(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    move-result-object v5

    invoke-interface {v5, v3, v4}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onWarning(Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;Ljava/lang/String;)V

    goto :goto_0

    .line 91
    .end local v3    # "warningCode":Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;
    .end local v4    # "warningMsg":Ljava/lang/String;
    :pswitch_4
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, [Ljava/lang/Object;

    check-cast v5, [Ljava/lang/Object;

    aget-object v0, v5, v6

    check-cast v0, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    .line 92
    .local v0, "errorCode":Lcom/vlingo/sdk/recognition/VLRecognitionErrors;
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, [Ljava/lang/Object;

    check-cast v5, [Ljava/lang/Object;

    aget-object v1, v5, v7

    check-cast v1, Ljava/lang/String;

    .line 93
    .local v1, "errorMsg":Ljava/lang/String;
    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    invoke-virtual {v5, v6}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->setBusy(Z)V

    .line 94
    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    invoke-static {v5}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$000(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 96
    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    invoke-static {v5}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$000(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    move-result-object v2

    .line 97
    .restart local v2    # "l":Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    # setter for: Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    invoke-static {v5, v8}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->access$002(Lcom/vlingo/sdk/internal/VLRecognizerImpl;Lcom/vlingo/sdk/recognition/VLRecognitionListener;)Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    .line 98
    invoke-interface {v2, v0, v1}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 63
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method declared-synchronized notifyError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V
    .locals 3
    .param p1, "error"    # Lcom/vlingo/sdk/recognition/VLRecognitionErrors;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 121
    monitor-enter p0

    const/4 v0, 0x5

    const/4 v1, 0x2

    :try_start_0
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    monitor-exit p0

    return-void

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized notifyEvent(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V
    .locals 1
    .param p1, "newState"    # Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    .prologue
    .line 109
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    monitor-exit p0

    return-void

    .line 109
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized notifyResult(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)V
    .locals 1
    .param p1, "result"    # Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    .prologue
    .line 113
    monitor-enter p0

    const/4 v0, 0x3

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    monitor-exit p0

    return-void

    .line 113
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized notifyRmsChange(Ljava/lang/Object;)V
    .locals 1
    .param p1, "rmsValue"    # Ljava/lang/Object;

    .prologue
    .line 105
    monitor-enter p0

    const/4 v0, 0x2

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    monitor-exit p0

    return-void

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized notifyWarning(Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;Ljava/lang/String;)V
    .locals 3
    .param p1, "warning"    # Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 117
    monitor-enter p0

    const/4 v0, 0x4

    const/4 v1, 0x2

    :try_start_0
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    monitor-exit p0

    return-void

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
