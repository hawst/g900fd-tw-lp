.class public Lcom/vlingo/sdk/internal/audio/TTSRequestParams;
.super Ljava/lang/Object;
.source "TTSRequestParams.java"


# static fields
.field public static final AUDIO:Ljava/lang/String; = "AudioFormat"

.field public static final AUDIO_AMR:Ljava/lang/String; = "AMR"

.field public static final AUDIO_MP3:Ljava/lang/String; = "MP3"

.field public static final BODY:Ljava/lang/String; = "Body"

.field public static final FROM:Ljava/lang/String; = "Sender"

.field public static final GENDER:Ljava/lang/String; = "Gender"

.field public static final GENDER_FEMALE:Ljava/lang/String; = "Female"

.field public static final GENDER_MALE:Ljava/lang/String; = "Male"

.field public static final HEADERS:Ljava/lang/String; = "Headers"

.field public static final LANG:Ljava/lang/String; = "Language"

.field public static final LANG_EN_GB:Ljava/lang/String; = "en-GB"

.field public static final LANG_EN_US:Ljava/lang/String; = "en-US"

.field public static final PRESILENCE:Ljava/lang/String; = "PreSilence"

.field public static final REQUEST:Ljava/lang/String; = "TTSMessageRequest"

.field public static final SENDER_ONLY:Ljava/lang/String; = "SenderOnly"

.field public static final SPEECHRATE:Ljava/lang/String; = "SpeechRate"

.field public static final SPEECHRATE_FAST:Ljava/lang/String; = "Fast"

.field public static final SPEECHRATE_NORMAL:Ljava/lang/String; = "Normal"

.field public static final SPEECHRATE_SLOW:Ljava/lang/String; = "Slow"

.field public static final SPEECHRATE_VERYFAST:Ljava/lang/String; = "VeryFast"

.field public static final SPEECHRATE_VERYSLOW:Ljava/lang/String; = "VerySlow"

.field public static final SUBJ:Ljava/lang/String; = "Subject"

.field public static final TYPE:Ljava/lang/String; = "MessageType"

.field public static final TYPE_CONFIRM:Ljava/lang/String; = "Confirm"

.field public static final TYPE_EMAIL:Ljava/lang/String; = "Email"

.field public static final TYPE_MMS:Ljava/lang/String; = "MMS"

.field public static final TYPE_SMS:Ljava/lang/String; = "SMS"

.field public static final VERSION:Ljava/lang/String; = "Version"

.field public static final VOLUME:Ljava/lang/String; = "Volume"

.field public static final WORD_COUNT:Ljava/lang/String; = "WordLimit"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
