.class public Lcom/vlingo/sdk/internal/edm/EDMResultsProcessor;
.super Ljava/lang/Object;
.source "EDMResultsProcessor.java"


# static fields
.field private static final NLU_ACTION:Ljava/lang/String; = "NluAction"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addEventsToInputBuilder(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;)V
    .locals 4
    .param p0, "context"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;
    .param p1, "builder"    # Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->getEvents()Ljava/util/List;

    move-result-object v1

    .line 153
    .local v1, "eventList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;>;"
    invoke-static {v1}, Lcom/vlingo/sdk/util/CollectionUtils;->isEmpty(Ljava/util/Collection;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 154
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;

    .line 155
    .local v0, "event":Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;->getXML()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->addEventXml(Ljava/lang/String;)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;

    goto :goto_0

    .line 158
    .end local v0    # "event":Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    return-void
.end method

.method public static conformResult(Lcom/vlingo/sdk/recognition/VLRecognitionResult;Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/vlingo/sdk/recognition/VLRecognitionResult;
    .locals 5
    .param p0, "originalResult"    # Lcom/vlingo/sdk/recognition/VLRecognitionResult;
    .param p1, "context"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext;
    .param p2, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;

    .prologue
    .line 28
    new-instance v1, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;

    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getFieldID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getDialogState()[B

    move-result-object v4

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;-><init>(Lcom/vlingo/sdk/recognition/VLRecognitionResult;Ljava/lang/String;Ljava/lang/String;[B)V

    .line 29
    .local v1, "result":Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;
    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->getActions()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/sdk/internal/edm/EDMResultsProcessor;->extractNluAction(Ljava/util/List;)Lcom/vlingo/sdk/recognition/VLAction;

    move-result-object v0

    .line 30
    .local v0, "nluAction":Lcom/vlingo/sdk/recognition/VLAction;
    if-nez v0, :cond_0

    .line 33
    const/4 v1, 0x0

    .line 38
    .end local v1    # "result":Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;
    :goto_0
    return-object v1

    .line 35
    .restart local v1    # "result":Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;
    :cond_0
    invoke-static {v0, p1, p2}, Lcom/vlingo/sdk/internal/edm/EDMResultsProcessor;->getEDMResults(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/nuance/embeddeddialogmanager/EDMResults;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;->init(Lcom/nuance/embeddeddialogmanager/EDMResults;)V

    goto :goto_0
.end method

.method private static extractNluAction(Ljava/util/List;)Lcom/vlingo/sdk/recognition/VLAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            ">;)",
            "Lcom/vlingo/sdk/recognition/VLAction;"
        }
    .end annotation

    .prologue
    .line 75
    .local p0, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/VLAction;>;"
    if-eqz p0, :cond_1

    .line 76
    invoke-interface {p0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/sdk/recognition/VLAction;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 77
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/recognition/VLAction;

    .line 78
    .local v0, "action":Lcom/vlingo/sdk/recognition/VLAction;
    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "NluAction"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 79
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 84
    .end local v0    # "action":Lcom/vlingo/sdk/recognition/VLAction;
    .end local v1    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/sdk/recognition/VLAction;>;"
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getEDMResults(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/nuance/embeddeddialogmanager/EDMResults;
    .locals 6
    .param p0, "nluAction"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p1, "context"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext;
    .param p2, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;

    .prologue
    .line 44
    new-instance v0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;

    invoke-direct {v0}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;-><init>()V

    .line 45
    .local v0, "builder":Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;
    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getDialogState()[B

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->setDialogStateBytes([B)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getFieldID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->setFieldId(Ljava/lang/String;)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4, v5}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->setDateTime(Ljava/util/Date;)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;

    move-result-object v4

    invoke-interface {p0}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->setNluActionName(Ljava/lang/String;)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->setLanguage(Ljava/lang/String;)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;

    .line 50
    invoke-interface {p0}, Lcom/vlingo/sdk/recognition/VLAction;->getParameterNames()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 51
    .local v3, "key":Ljava/lang/String;
    invoke-interface {p0, v3}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->addNluActionParam(Ljava/lang/String;Ljava/lang/String;)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;

    goto :goto_0

    .line 53
    .end local v3    # "key":Ljava/lang/String;
    :cond_0
    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getDMHeaderKVPairs()Ljava/util/HashMap;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 54
    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getDMHeaderKVPairs()Ljava/util/HashMap;

    move-result-object v4

    const-string/jumbo v5, "viewableListSize"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 56
    :try_start_0
    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getDMHeaderKVPairs()Ljava/util/HashMap;

    move-result-object v4

    const-string/jumbo v5, "viewableListSize"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->setViewableListSize(Ljava/lang/Integer;)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :cond_1
    :goto_1
    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getDMHeaderKVPairs()Ljava/util/HashMap;

    move-result-object v4

    const-string/jumbo v5, "listPosition"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->setListPosition(Ljava/lang/String;)V

    .line 65
    :cond_2
    invoke-virtual {v0}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->build()Lcom/nuance/embeddeddialogmanager/EDMInputStateData;

    move-result-object v1

    .line 67
    .local v1, "edmInputData":Lcom/nuance/embeddeddialogmanager/EDMInputStateData;
    if-nez v1, :cond_3

    .line 68
    const/4 v4, 0x0

    .line 71
    :goto_2
    return-object v4

    :cond_3
    invoke-static {v1}, Lcom/vlingo/sdk/internal/edm/EDMResultsProcessor;->retrieveDMActions(Lcom/nuance/embeddeddialogmanager/EDMInputStateData;)Lcom/nuance/embeddeddialogmanager/EDMResults;

    move-result-object v4

    goto :goto_2

    .line 57
    .end local v1    # "edmInputData":Lcom/nuance/embeddeddialogmanager/EDMInputStateData;
    :catch_0
    move-exception v4

    goto :goto_1
.end method

.method public static hasNluAction(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)Z
    .locals 5
    .param p0, "result"    # Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    .prologue
    .line 103
    if-eqz p0, :cond_1

    .line 104
    invoke-interface {p0}, Lcom/vlingo/sdk/recognition/VLRecognitionResult;->getActions()Ljava/util/List;

    move-result-object v1

    .line 105
    .local v1, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/VLAction;>;"
    if-eqz v1, :cond_1

    .line 106
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/recognition/VLAction;

    .line 107
    .local v0, "action":Lcom/vlingo/sdk/recognition/VLAction;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "NluAction"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 109
    const/4 v3, 0x1

    .line 114
    .end local v0    # "action":Lcom/vlingo/sdk/recognition/VLAction;
    .end local v1    # "actions":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/VLAction;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private static retrieveDMActions(Lcom/nuance/embeddeddialogmanager/EDMInputStateData;)Lcom/nuance/embeddeddialogmanager/EDMResults;
    .locals 2
    .param p0, "data"    # Lcom/nuance/embeddeddialogmanager/EDMInputStateData;

    .prologue
    .line 89
    :try_start_0
    invoke-static {}, Lcom/nuance/embeddeddialogmanager/EDM;->getInstance()Lcom/nuance/embeddeddialogmanager/EDM;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/nuance/embeddeddialogmanager/EDM;->processNLUActionString(Landroid/content/Context;Lcom/nuance/embeddeddialogmanager/EDMInputStateData;)Lcom/nuance/embeddeddialogmanager/EDMResults;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 99
    :goto_0
    return-object v0

    .line 95
    :catch_0
    move-exception v0

    .line 99
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 90
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static sendEvents(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;Lcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/vlingo/sdk/recognition/VLRecognitionResult;
    .locals 6
    .param p0, "dialogContext"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;
    .param p1, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;

    .prologue
    .line 118
    const/4 v2, 0x0

    .line 119
    .local v2, "result":Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;
    new-instance v0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;

    invoke-direct {v0}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;-><init>()V

    .line 120
    .local v0, "builder":Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;
    invoke-virtual {p0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->getDialogState()[B

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->setDialogStateBytes([B)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->getFieldID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->setFieldId(Ljava/lang/String;)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3, v4}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->setDateTime(Ljava/util/Date;)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->setLanguage(Ljava/lang/String;)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;

    .line 124
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getDMHeaderKVPairs()Ljava/util/HashMap;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 125
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getDMHeaderKVPairs()Ljava/util/HashMap;

    move-result-object v3

    const-string/jumbo v4, "viewableListSize"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 127
    :try_start_0
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getDMHeaderKVPairs()Ljava/util/HashMap;

    move-result-object v3

    const-string/jumbo v4, "viewableListSize"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->setViewableListSize(Ljava/lang/Integer;)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 133
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getDMHeaderKVPairs()Ljava/util/HashMap;

    move-result-object v3

    const-string/jumbo v4, "listPosition"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->setListPosition(Ljava/lang/String;)V

    .line 136
    :cond_1
    invoke-static {p0, v0}, Lcom/vlingo/sdk/internal/edm/EDMResultsProcessor;->addEventsToInputBuilder(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;)V

    .line 137
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/sdk/VLSdk;->doesUseEDM()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 139
    :try_start_1
    new-instance v2, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;

    .end local v2    # "result":Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;
    invoke-static {}, Lcom/nuance/embeddeddialogmanager/EDM;->getInstance()Lcom/nuance/embeddeddialogmanager/EDM;

    move-result-object v3

    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v0}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->build()Lcom/nuance/embeddeddialogmanager/EDMInputStateData;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/nuance/embeddeddialogmanager/EDM;->processEvents(Landroid/content/Context;Lcom/nuance/embeddeddialogmanager/EDMInputStateData;)Lcom/nuance/embeddeddialogmanager/EDMResults;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;-><init>(Lcom/nuance/embeddeddialogmanager/EDMResults;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 148
    .restart local v2    # "result":Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;
    :cond_2
    :goto_1
    return-object v2

    .line 141
    .end local v2    # "result":Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;
    :catch_0
    move-exception v1

    .line 144
    .local v1, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    .restart local v2    # "result":Lcom/vlingo/sdk/internal/edm/EDMRecognitionResult;
    goto :goto_1

    .line 128
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    goto :goto_0
.end method
