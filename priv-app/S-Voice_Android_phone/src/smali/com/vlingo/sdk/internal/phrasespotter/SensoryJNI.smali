.class public Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;
.super Ljava/lang/Object;
.source "SensoryJNI.java"


# static fields
.field public static GRAMMAR_FORMALITY_DEFAULT:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    sput v0, Lcom/vlingo/sdk/internal/phrasespotter/SensoryJNI;->GRAMMAR_FORMALITY_DEFAULT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static init()V
    .locals 1

    .prologue
    .line 76
    const-string/jumbo v0, "libsensory2.so"

    invoke-static {v0}, Lcom/vlingo/sdk/util/SystemUtil;->loadLibrary(Ljava/lang/String;)V

    .line 77
    return-void
.end method


# virtual methods
.method public native Deinitialize()V
.end method

.method public native GetLastScore()F
.end method

.method public native Initialize(Ljava/lang/String;Ljava/lang/String;IFFFFLjava/lang/String;I)Z
.end method

.method public native InitializePhrases(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;IFFFFLjava/lang/String;I)Z
.end method

.method public native MakeReady()Z
.end method

.method public native ProcessShortArray([SI)Ljava/lang/String;
.end method

.method public asyncPrint(Ljava/lang/String;)V
    .locals 0
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 83
    return-void
.end method

.method public native phrasespotClose(J)V
.end method

.method public native phrasespotInit(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
.end method

.method public native phrasespotPipe(JLjava/nio/ByteBuffer;J)Ljava/lang/String;
.end method
