.class public Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;
.super Ljava/lang/Object;
.source "ThreadPoolExecutor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private volatile availWorkers:I

.field private volatile dynamic:Z

.field private volatile maxPoolSize:I

.field private volatile minPoolSize:I

.field private volatile nextThreadID:I

.field private volatile paused:Z

.field private volatile priority:I

.field private final queue:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/sdk/internal/util/Future;",
            ">;"
        }
    .end annotation
.end field

.field private volatile shutDown:Z

.field private final workerName:Ljava/lang/String;

.field private final workers:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    const-string/jumbo v0, "Worker"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;-><init>(Ljava/lang/String;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "workerName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;

    .line 26
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->workers:Ljava/util/Vector;

    .line 27
    iput v1, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->availWorkers:I

    .line 28
    iput v2, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->maxPoolSize:I

    .line 29
    iput v2, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->minPoolSize:I

    .line 30
    iput-boolean v1, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->shutDown:Z

    .line 31
    const/4 v0, 0x5

    iput v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->priority:I

    .line 32
    iput-boolean v1, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->dynamic:Z

    .line 34
    iput v2, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->nextThreadID:I

    .line 36
    iput-boolean v1, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->paused:Z

    .line 43
    iput-object p1, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->workerName:Ljava/lang/String;

    .line 44
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Ljava/util/Vector;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    .prologue
    .line 15
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->dynamic:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->minPoolSize:I

    return v0
.end method

.method static synthetic access$200(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    .prologue
    .line 15
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->paused:Z

    return v0
.end method

.method static synthetic access$300(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    .prologue
    .line 15
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->shutDown:Z

    return v0
.end method

.method static synthetic access$400(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Ljava/util/Vector;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->workers:Ljava/util/Vector;

    return-object v0
.end method

.method static synthetic access$508(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)I
    .locals 2
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->availWorkers:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->availWorkers:I

    return v0
.end method

.method static synthetic access$510(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)I
    .locals 2
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->availWorkers:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->availWorkers:I

    return v0
.end method

.method static synthetic access$600(Ljava/lang/Runnable;)Z
    .locals 1
    .param p0, "x0"    # Ljava/lang/Runnable;

    .prologue
    .line 15
    invoke-static {p0}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->isRunnableRetry(Ljava/lang/Runnable;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Ljava/lang/Runnable;)Z
    .locals 1
    .param p0, "x0"    # Ljava/lang/Runnable;

    .prologue
    .line 15
    invoke-static {p0}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->isRunnableOrdered(Ljava/lang/Runnable;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Ljava/lang/Runnable;)Z
    .locals 1
    .param p0, "x0"    # Ljava/lang/Runnable;

    .prologue
    .line 15
    invoke-static {p0}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->isRunnableHighPriority(Ljava/lang/Runnable;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$900()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private static isRunnableHighPriority(Ljava/lang/Runnable;)Z
    .locals 1
    .param p0, "run"    # Ljava/lang/Runnable;

    .prologue
    .line 270
    instance-of v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolRunnable;

    if-eqz v0, :cond_0

    .line 271
    check-cast p0, Lcom/vlingo/sdk/internal/util/ThreadPoolRunnable;

    .end local p0    # "run":Ljava/lang/Runnable;
    invoke-interface {p0}, Lcom/vlingo/sdk/internal/util/ThreadPoolRunnable;->isHighPriority()Z

    move-result v0

    .line 273
    :goto_0
    return v0

    .restart local p0    # "run":Ljava/lang/Runnable;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isRunnableOrdered(Ljava/lang/Runnable;)Z
    .locals 1
    .param p0, "run"    # Ljava/lang/Runnable;

    .prologue
    .line 277
    instance-of v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolRunnable;

    if-eqz v0, :cond_0

    .line 278
    check-cast p0, Lcom/vlingo/sdk/internal/util/ThreadPoolRunnable;

    .end local p0    # "run":Ljava/lang/Runnable;
    invoke-interface {p0}, Lcom/vlingo/sdk/internal/util/ThreadPoolRunnable;->isOrdered()Z

    move-result v0

    .line 280
    :goto_0
    return v0

    .restart local p0    # "run":Ljava/lang/Runnable;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isRunnableRetry(Ljava/lang/Runnable;)Z
    .locals 1
    .param p0, "run"    # Ljava/lang/Runnable;

    .prologue
    .line 284
    instance-of v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolRunnable;

    if-eqz v0, :cond_0

    .line 285
    check-cast p0, Lcom/vlingo/sdk/internal/util/ThreadPoolRunnable;

    .end local p0    # "run":Ljava/lang/Runnable;
    invoke-interface {p0}, Lcom/vlingo/sdk/internal/util/ThreadPoolRunnable;->isRetry()Z

    move-result v0

    .line 287
    :goto_0
    return v0

    .restart local p0    # "run":Ljava/lang/Runnable;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 166
    iget-object v1, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;

    monitor-enter v1

    .line 167
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    .line 168
    iget-object v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 169
    monitor-exit v1

    .line 170
    return-void

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public execute(Ljava/lang/Runnable;)Lcom/vlingo/sdk/internal/util/Future;
    .locals 13
    .param p1, "run"    # Ljava/lang/Runnable;

    .prologue
    .line 70
    iget-boolean v7, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->shutDown:Z

    if-eqz v7, :cond_0

    .line 71
    new-instance v7, Ljava/lang/IllegalStateException;

    invoke-direct {v7}, Ljava/lang/IllegalStateException;-><init>()V

    throw v7

    .line 73
    :cond_0
    const/4 v1, 0x0

    .line 74
    .local v1, "fut":Lcom/vlingo/sdk/internal/util/Future;
    iget-object v8, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;

    monitor-enter v8

    .line 75
    :try_start_0
    iget-object v9, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->workers:Ljava/util/Vector;

    monitor-enter v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 79
    :try_start_1
    iget-object v7, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->workers:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v7

    iget v10, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->maxPoolSize:I

    if-ge v7, v10, :cond_1

    iget v7, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->availWorkers:I

    if-nez v7, :cond_1

    .line 82
    new-instance v6, Ljava/lang/Thread;

    new-instance v7, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;

    const/4 v10, 0x0

    invoke-direct {v7, p0, v10}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;-><init>(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$1;)V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->workerName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->nextThreadID:I

    add-int/lit8 v12, v11, 0x1

    iput v12, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->nextThreadID:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v6, v7, v10}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 83
    .local v6, "thread":Ljava/lang/Thread;
    iget v7, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->priority:I

    invoke-virtual {v6, v7}, Ljava/lang/Thread;->setPriority(I)V

    .line 84
    iget-object v7, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->workers:Ljava/util/Vector;

    invoke-virtual {v7, v6}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 85
    iget v7, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->availWorkers:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->availWorkers:I

    .line 86
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 88
    .end local v6    # "thread":Ljava/lang/Thread;
    :cond_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89
    :try_start_2
    new-instance v2, Lcom/vlingo/sdk/internal/util/Future;

    invoke-direct {v2, p1}, Lcom/vlingo/sdk/internal/util/Future;-><init>(Ljava/lang/Runnable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 90
    .end local v1    # "fut":Lcom/vlingo/sdk/internal/util/Future;
    .local v2, "fut":Lcom/vlingo/sdk/internal/util/Future;
    :try_start_3
    invoke-static {p1}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->isRunnableHighPriority(Ljava/lang/Runnable;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 92
    const/4 v0, 0x0

    .line 93
    .local v0, "added":Z
    iget-object v7, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v5

    .line 94
    .local v5, "size":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v5, :cond_2

    .line 95
    iget-object v7, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;

    invoke-virtual {v7, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/sdk/internal/util/Future;

    .line 96
    .local v4, "queuedFuture":Lcom/vlingo/sdk/internal/util/Future;
    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/util/Future;->getRunnable()Ljava/lang/Runnable;

    move-result-object v7

    invoke-static {v7}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->isRunnableHighPriority(Ljava/lang/Runnable;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 97
    iget-object v7, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;

    invoke-virtual {v7, v2, v3}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    .line 98
    const/4 v0, 0x1

    .line 102
    .end local v4    # "queuedFuture":Lcom/vlingo/sdk/internal/util/Future;
    :cond_2
    if-nez v0, :cond_3

    .line 103
    iget-object v7, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;

    invoke-virtual {v7, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 110
    .end local v0    # "added":Z
    .end local v3    # "i":I
    .end local v5    # "size":I
    :cond_3
    :goto_1
    iget-object v7, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/lang/Object;->notifyAll()V

    .line 111
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 112
    return-object v2

    .line 88
    .end local v2    # "fut":Lcom/vlingo/sdk/internal/util/Future;
    .restart local v1    # "fut":Lcom/vlingo/sdk/internal/util/Future;
    :catchall_0
    move-exception v7

    :try_start_4
    monitor-exit v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v7

    .line 111
    :catchall_1
    move-exception v7

    :goto_2
    monitor-exit v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v7

    .line 94
    .end local v1    # "fut":Lcom/vlingo/sdk/internal/util/Future;
    .restart local v0    # "added":Z
    .restart local v2    # "fut":Lcom/vlingo/sdk/internal/util/Future;
    .restart local v3    # "i":I
    .restart local v4    # "queuedFuture":Lcom/vlingo/sdk/internal/util/Future;
    .restart local v5    # "size":I
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 108
    .end local v0    # "added":Z
    .end local v3    # "i":I
    .end local v4    # "queuedFuture":Lcom/vlingo/sdk/internal/util/Future;
    .end local v5    # "size":I
    :cond_5
    :try_start_6
    iget-object v7, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;

    invoke-virtual {v7, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto :goto_1

    .line 111
    :catchall_2
    move-exception v7

    move-object v1, v2

    .end local v2    # "fut":Lcom/vlingo/sdk/internal/util/Future;
    .restart local v1    # "fut":Lcom/vlingo/sdk/internal/util/Future;
    goto :goto_2
.end method

.method public executeLater(Ljava/lang/Runnable;J)Lcom/vlingo/sdk/internal/util/Future;
    .locals 3
    .param p1, "runnable"    # Ljava/lang/Runnable;
    .param p2, "wait"    # J

    .prologue
    .line 121
    new-instance v0, Lcom/vlingo/sdk/internal/util/Future;

    invoke-direct {v0, p1}, Lcom/vlingo/sdk/internal/util/Future;-><init>(Ljava/lang/Runnable;)V

    .line 122
    .local v0, "fut":Lcom/vlingo/sdk/internal/util/Future;
    invoke-static {}, Lcom/vlingo/sdk/internal/util/TimerSingleton;->getTimer()Ljava/util/Timer;

    move-result-object v2

    .line 123
    .local v2, "timer":Ljava/util/Timer;
    new-instance v1, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$1;

    invoke-direct {v1, p0, v0}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$1;-><init>(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;Lcom/vlingo/sdk/internal/util/Future;)V

    .line 129
    .local v1, "task":Ljava/util/TimerTask;
    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/util/Future;->setScheduleTask(Ljava/util/TimerTask;)V

    .line 130
    invoke-virtual {v2, v1, p2, p3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 131
    return-object v0
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 47
    iget v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->availWorkers:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->workers:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    iget v1, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->maxPoolSize:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    .line 142
    iget-object v1, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;

    monitor-enter v1

    .line 143
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 144
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->paused:Z

    if-nez v0, :cond_0

    .line 149
    iget-object v1, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;

    monitor-enter v1

    .line 150
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->paused:Z

    .line 151
    iget-object v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 152
    monitor-exit v1

    .line 154
    :cond_0
    return-void

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public resume()V
    .locals 2

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->paused:Z

    if-eqz v0, :cond_0

    .line 158
    iget-object v1, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;

    monitor-enter v1

    .line 159
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->paused:Z

    .line 160
    iget-object v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 161
    monitor-exit v1

    .line 163
    :cond_0
    return-void

    .line 161
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setDynamicSizing(Z)V
    .locals 0
    .param p1, "dynamic"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->dynamic:Z

    .line 60
    return-void
.end method

.method public setMaxPoolSize(I)V
    .locals 0
    .param p1, "s"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->maxPoolSize:I

    .line 52
    return-void
.end method

.method public setMinPoolSize(I)V
    .locals 0
    .param p1, "s"    # I

    .prologue
    .line 55
    iput p1, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->minPoolSize:I

    .line 56
    return-void
.end method

.method public setThreadPriority(I)V
    .locals 0
    .param p1, "priority"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->priority:I

    .line 64
    return-void
.end method

.method public shutdown()V
    .locals 2

    .prologue
    .line 135
    iget-object v1, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;

    monitor-enter v1

    .line 136
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->shutDown:Z

    .line 137
    iget-object v0, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 138
    monitor-exit v1

    .line 139
    return-void

    .line 138
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
