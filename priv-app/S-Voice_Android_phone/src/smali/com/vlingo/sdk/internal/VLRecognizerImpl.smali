.class public final Lcom/vlingo/sdk/internal/VLRecognizerImpl;
.super Lcom/vlingo/sdk/internal/VLComponentImpl;
.source "VLRecognizerImpl.java"

# interfaces
.implements Lcom/vlingo/sdk/recognition/VLRecognizer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/VLRecognizerImpl$7;,
        Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;,
        Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;
    }
.end annotation


# instance fields
.field private mCurrentRecognizerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

.field private mDataReadyListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

.field private final mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

.field private mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

.field private mRecoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

.field private mRecognizer:Lcom/vlingo/sdk/internal/recognizer/Recognizer;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V
    .locals 1
    .param p1, "manager"    # Lcom/vlingo/sdk/internal/VLComponentManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 134
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/VLComponentImpl;-><init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V

    .line 139
    new-instance v0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;-><init>(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    .line 142
    invoke-static {}, Lcom/vlingo/sdk/internal/audio/SpeexJNI;->init()V

    .line 144
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecognizer:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    .line 145
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    return-object v0
.end method

.method static synthetic access$002(Lcom/vlingo/sdk/internal/VLRecognizerImpl;Lcom/vlingo/sdk/recognition/VLRecognitionListener;)Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLRecognizerImpl;
    .param p1, "x1"    # Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    return-object p1
.end method

.method static synthetic access$200(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mCurrentRecognizerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->getSRContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mDataReadyListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/internal/recognizer/Recognizer;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecognizer:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/sdk/internal/VLRecognizerImpl;Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLRecognizerImpl;
    .param p1, "x1"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->sendEventToEDM(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;)V

    return-void
.end method

.method static synthetic access$800(Lcom/vlingo/sdk/internal/VLRecognizerImpl;Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLRecognizerImpl;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->sendEventToRecognizer(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;)V

    return-void
.end method

.method static synthetic access$900(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)Lcom/vlingo/sdk/recognition/VLRecognitionContext;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLRecognizerImpl;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    return-object v0
.end method

.method private getSRContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .locals 6

    .prologue
    const/16 v4, 0x3a

    .line 368
    new-instance v1, Lcom/vlingo/sdk/internal/recognizer/SRContext;

    invoke-direct {v1}, Lcom/vlingo/sdk/internal/recognizer/SRContext;-><init>()V

    .line 369
    .local v1, "srContext":Lcom/vlingo/sdk/internal/recognizer/SRContext;
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getFieldID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setFieldID(Ljava/lang/String;)V

    .line 372
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 373
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getAppName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 374
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getAppName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 376
    :cond_0
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getScreenName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 377
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v3}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getScreenName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 378
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getControlName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 379
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v3}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getControlName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 385
    :cond_1
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setFieldContext(Ljava/lang/String;)V

    .line 387
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getCurrentText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setCurText(Ljava/lang/String;)V

    .line 388
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getCursorPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setCursorPos(I)V

    .line 389
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getAutoPunctuation()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setAutoPunctuation(Z)V

    .line 390
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getProfanityFilter()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setProfanityFilter(Z)V

    .line 391
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getCapitalizationMode()Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setCapitalizationMode(Lcom/vlingo/sdk/recognition/VLRecognitionContext$CapitalizationMode;)V

    .line 392
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getMaxAudioTime()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setMaxAudioTime(I)V

    .line 393
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->autoEndpointingEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setAutoEndpointing(Z)V

    .line 394
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getSilenceThreshold()F

    move-result v2

    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v3}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getMinVoiceDuration()F

    move-result v3

    iget-object v4, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v4}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getVoicePortion()F

    move-result v4

    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v5}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getMinVoiceLevel()F

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setSilenceDetectionParams(FFFF)V

    .line 395
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getSpeexComplexity()I

    move-result v2

    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v3}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getSpeexQuality()I

    move-result v3

    iget-object v4, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v4}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getSpeexVariableBitrate()I

    move-result v4

    iget-object v5, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v5}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getSpeexVoiceActivityDetection()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setSpeexParams(IIII)V

    .line 396
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getSpeechEndpointTimeout()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setSpeechEndpointTimeout(I)V

    .line 397
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getNoSpeechEndPointTimeout()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setNoSpeechEndpointTimeout(I)V

    .line 398
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getAudioSourceInfo()Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setAudioSourceInfo(Lcom/vlingo/sdk/recognition/AudioSourceInfo;)V

    .line 399
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getAudioFormatChannelConfig()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setAudioFormatChannelConfig(I)V

    .line 401
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    instance-of v2, v2, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;

    if-eqz v2, :cond_2

    .line 402
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setIsDMRequest(Z)V

    .line 403
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    check-cast v2, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->getDialogState()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setDialogState([B)V

    .line 404
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    check-cast v2, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->getEvents()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setEvents(Ljava/util/List;)V

    .line 405
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    check-cast v2, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->getUsername()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setUsername(Ljava/lang/String;)V

    .line 406
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    check-cast v2, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->getDialogGUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setDialogGUID(Ljava/lang/String;)V

    .line 407
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    check-cast v2, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->getDialogTurnNumber()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setDialogTurnNumber(I)V

    .line 408
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    check-cast v2, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->getDMHeaderKVPairs()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setDMHeaderKVPairs(Ljava/util/HashMap;)V

    .line 409
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    check-cast v2, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->getMetaKVPairs()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->setMetaKVPairs(Ljava/util/HashMap;)V

    .line 412
    :cond_2
    return-object v1

    .line 382
    :cond_3
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getControlName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 383
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "::"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v3}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getControlName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method

.method private sendEventToEDM(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;

    .prologue
    .line 275
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->getSRContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/vlingo/sdk/internal/edm/EDMResultsProcessor;->sendEvents(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;Lcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/vlingo/sdk/recognition/VLRecognitionResult;

    move-result-object v0

    .line 276
    .local v0, "result":Lcom/vlingo/sdk/recognition/VLRecognitionResult;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    invoke-virtual {v1, v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->notifyResult(Lcom/vlingo/sdk/recognition/VLRecognitionResult;)V

    .line 277
    return-void
.end method

.method private sendEventToRecognizer(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    .prologue
    .line 266
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecognizer:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->getSRContext()Lcom/vlingo/sdk/internal/recognizer/SRContext;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->startSendEvent(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 272
    :goto_0
    return-void

    .line 267
    :catch_0
    move-exception v0

    .line 270
    .local v0, "ise":Ljava/lang/IllegalStateException;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    sget-object v2, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_RECOGNIZER_BUSY:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    const-string/jumbo v3, "Failed to start recognition."

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->notifyError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public acceptedText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "gUttId"    # Ljava/lang/String;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 340
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->validateInstance()V

    .line 342
    invoke-static {p2}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 359
    :goto_0
    return-void

    .line 345
    :cond_0
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;-><init>()V

    .line 346
    .local v0, "collection":Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;
    invoke-virtual {v0, p2}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;->setAcceptedText(Ljava/lang/String;)V

    .line 348
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/vlingo/sdk/internal/VLRecognizerImpl$6;

    invoke-direct {v2, p0, p1, v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$6;-><init>(Lcom/vlingo/sdk/internal/VLRecognizerImpl;Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public cancelRecognition()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 304
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->validateInstance()V

    .line 307
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    .line 308
    .local v0, "l":Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    iput-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    .line 309
    iput-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mCurrentRecognizerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    .line 310
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->clear()V

    .line 312
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/vlingo/sdk/internal/VLRecognizerImpl$4;

    invoke-direct {v2, p0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$4;-><init>(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 324
    if-eqz v0, :cond_0

    .line 325
    iget-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;

    new-instance v2, Lcom/vlingo/sdk/internal/VLRecognizerImpl$5;

    invoke-direct {v2, p0, v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$5;-><init>(Lcom/vlingo/sdk/internal/VLRecognizerImpl;Lcom/vlingo/sdk/recognition/VLRecognitionListener;)V

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$NotificationHandler;->post(Ljava/lang/Runnable;)Z

    .line 333
    :cond_0
    return-void
.end method

.method public bridge synthetic destroy()V
    .locals 0

    .prologue
    .line 38
    invoke-super {p0}, Lcom/vlingo/sdk/internal/VLComponentImpl;->destroy()V

    return-void
.end method

.method public doesDependOnSettings()Z
    .locals 1

    .prologue
    .line 547
    const/4 v0, 0x1

    return v0
.end method

.method public getIsStoppedDataReader()Z
    .locals 1

    .prologue
    .line 538
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecognizer:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    if-eqz v0, :cond_0

    .line 539
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecognizer:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->getIsStoppedDataReader()Z

    move-result v0

    .line 541
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSupportedLanguageList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 363
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->validateInstance()V

    .line 364
    sget-object v0, Lcom/vlingo/sdk/internal/settings/Settings;->SUPPORTED_LANGUAGES:[Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic isValid()Z
    .locals 1

    .prologue
    .line 38
    invoke-super {p0}, Lcom/vlingo/sdk/internal/VLComponentImpl;->isValid()Z

    move-result v0

    return v0
.end method

.method onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 155
    iput-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mCurrentRecognizerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    .line 156
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecognizer:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->destroy()V

    .line 157
    iput-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecognizer:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    .line 158
    iput-object v1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    .line 160
    return-void
.end method

.method public sendEvent(Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;Lcom/vlingo/sdk/recognition/VLRecognitionListener;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;
    .param p2, "listener"    # Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    .prologue
    .line 218
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->validateInstance()V

    .line 221
    if-nez p1, :cond_0

    .line 222
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "context must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 224
    :cond_0
    if-nez p2, :cond_1

    .line 225
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "listener must be specifed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 227
    :cond_1
    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->getEvents()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;->getEvents()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 228
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "context must contain at least 1 event"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 230
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->isBusy()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 231
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Recognition already in progress"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 234
    :cond_4
    iput-object p2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    .line 235
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    .line 236
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/sdk/internal/settings/Settings;->setLanguage(Ljava/lang/String;)V

    .line 238
    new-instance v0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;-><init>(Lcom/vlingo/sdk/internal/VLRecognizerImpl;Lcom/vlingo/sdk/internal/VLRecognizerImpl$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mCurrentRecognizerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    .line 239
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->setBusy(Z)V

    .line 241
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/vlingo/sdk/internal/VLRecognizerImpl$2;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$2;-><init>(Lcom/vlingo/sdk/internal/VLRecognizerImpl;Lcom/vlingo/sdk/recognition/dialog/VLDialogContext;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 262
    return-void
.end method

.method public startRecognition(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/sdk/recognition/VLRecognitionListener;Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;Lcom/vlingo/sdk/recognition/RecognitionMode;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/sdk/recognition/VLRecognitionContext;
    .param p2, "listener"    # Lcom/vlingo/sdk/recognition/VLRecognitionListener;
    .param p3, "DataReadyListener"    # Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;
    .param p4, "mode"    # Lcom/vlingo/sdk/recognition/RecognitionMode;

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->validateInstance()V

    .line 170
    if-nez p1, :cond_0

    .line 171
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "context must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 173
    :cond_0
    if-nez p2, :cond_1

    .line 174
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "listener must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 176
    :cond_1
    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getAudioSourceInfo()Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v0

    if-nez v0, :cond_2

    .line 177
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "AudioSourceInfo is required for startRecognition()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->isBusy()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 180
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Recognition already in progress"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 183
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->setBusy(Z)V

    .line 184
    iput-object p2, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoListener:Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    .line 185
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    .line 186
    iput-object p3, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mDataReadyListener:Lcom/vlingo/sdk/internal/recognizer/reader/DataReadyListener;

    .line 187
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mRecoContext:Lcom/vlingo/sdk/recognition/VLRecognitionContext;

    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/VLRecognitionContext;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/sdk/internal/settings/Settings;->setLanguage(Ljava/lang/String;)V

    .line 188
    new-instance v0, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$RecognitionListenerImpl;-><init>(Lcom/vlingo/sdk/internal/VLRecognizerImpl;Lcom/vlingo/sdk/internal/VLRecognizerImpl$1;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->mCurrentRecognizerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    .line 190
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/vlingo/sdk/internal/VLRecognizerImpl$1;

    invoke-direct {v1, p0, p4}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$1;-><init>(Lcom/vlingo/sdk/internal/VLRecognizerImpl;Lcom/vlingo/sdk/recognition/RecognitionMode;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 211
    return-void
.end method

.method public stopRecognition()V
    .locals 2

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->validateInstance()V

    .line 286
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/vlingo/sdk/internal/VLRecognizerImpl$3;

    invoke-direct {v1, p0}, Lcom/vlingo/sdk/internal/VLRecognizerImpl$3;-><init>(Lcom/vlingo/sdk/internal/VLRecognizerImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 297
    return-void
.end method
