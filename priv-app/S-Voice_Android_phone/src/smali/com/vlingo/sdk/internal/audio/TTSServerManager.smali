.class public Lcom/vlingo/sdk/internal/audio/TTSServerManager;
.super Ljava/lang/Object;
.source "TTSServerManager.java"


# static fields
.field private static ttsSequenceId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    sput v0, Lcom/vlingo/sdk/internal/audio/TTSServerManager;->ttsSequenceId:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createTTSRequest(Lcom/vlingo/sdk/internal/audio/TTSRequest;Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    .locals 7
    .param p0, "ttsRequest"    # Lcom/vlingo/sdk/internal/audio/TTSRequest;
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/vlingo/sdk/internal/http/HttpCallback;

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/vlingo/sdk/internal/audio/TTSServerManager;->getTTSRequestXML(Lcom/vlingo/sdk/internal/audio/TTSRequest;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 36
    .local v3, "xml":Ljava/lang/String;
    new-instance v0, Ljava/util/Hashtable;

    const/4 v4, 0x2

    invoke-direct {v0, v4}, Ljava/util/Hashtable;-><init>(I)V

    .line 37
    .local v0, "headers":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v4, "Content-type"

    const-string/jumbo v5, "application/x-www-form-urlencoded"

    invoke-virtual {v0, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    const-string/jumbo v4, "X-vlrequest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "CL=true; RequestID="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v6, Lcom/vlingo/sdk/internal/audio/TTSServerManager;->ttsSequenceId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget v4, Lcom/vlingo/sdk/internal/audio/TTSServerManager;->ttsSequenceId:I

    add-int/lit8 v4, v4, 0x1

    sput v4, Lcom/vlingo/sdk/internal/audio/TTSServerManager;->ttsSequenceId:I

    .line 41
    invoke-static {}, Lcom/vlingo/sdk/internal/AndroidServerDetails;->getTTSURL()Lcom/vlingo/sdk/internal/http/URL;

    move-result-object v2

    .line 42
    .local v2, "url":Lcom/vlingo/sdk/internal/http/URL;
    const-string/jumbo v4, "TTS"

    invoke-static {v4, p2, v2, v3, p1}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->createVLRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Lcom/vlingo/sdk/internal/http/URL;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    move-result-object v1

    .line 43
    .local v1, "request":Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    invoke-virtual {v1, v0}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->setExtraHeaders(Ljava/util/Hashtable;)V

    .line 45
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->setMaxRetry(I)V

    .line 46
    return-object v1
.end method

.method private static getTTSRequestXML(Lcom/vlingo/sdk/internal/audio/TTSRequest;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "request"    # Lcom/vlingo/sdk/internal/audio/TTSRequest;
    .param p1, "lang"    # Ljava/lang/String;

    .prologue
    .line 63
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 65
    .local v3, "sb":Ljava/lang/StringBuffer;
    const-string/jumbo v0, "MP3"

    .line 66
    .local v0, "format":Ljava/lang/String;
    const/16 v5, 0x64

    .line 67
    .local v5, "volume":I
    const-string/jumbo v4, "1.0"

    .line 68
    .local v4, "version":Ljava/lang/String;
    const-string/jumbo v2, "0"

    .line 69
    .local v2, "preSilence":Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    const-string/jumbo v1, "en-US"

    .line 71
    .local v1, "language":Ljava/lang/String;
    :goto_0
    const-string/jumbo v6, "<"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "TTSMessageRequest"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 72
    const-string/jumbo v6, "SenderOnly"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget-boolean v7, p0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->senderOnly:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "\" "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 73
    const-string/jumbo v6, "Gender"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->gender:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "\" "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 74
    const-string/jumbo v6, "Language"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "\" "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 75
    const-string/jumbo v6, "AudioFormat"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "\" "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 76
    const-string/jumbo v6, "MessageType"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->type:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "\" "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 77
    const-string/jumbo v6, "SpeechRate"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->speechRate:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "\" "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 79
    iget v6, p0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->wordLimit:I

    if-lez v6, :cond_0

    .line 80
    const-string/jumbo v6, "WordLimit"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget v7, p0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->wordLimit:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "\" "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 83
    :cond_0
    const-string/jumbo v6, "Volume"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "\" "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 84
    const-string/jumbo v6, "Version"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "\" "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 85
    const-string/jumbo v6, "PreSilence"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 86
    const-string/jumbo v6, ">"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 88
    iget-object v6, p0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->senderDisplayName:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 89
    const-string/jumbo v6, "<"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "Sender"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, ">"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 90
    iget-object v6, p0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->senderDisplayName:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/XmlUtils;->wrapInCDATA(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 91
    const-string/jumbo v6, "</"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "Sender"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, ">"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 93
    :cond_1
    iget-object v6, p0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->subject:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 94
    const-string/jumbo v6, "<"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "Subject"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, ">"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 95
    iget-object v6, p0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->subject:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/XmlUtils;->wrapInCDATA(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 96
    const-string/jumbo v6, "</"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "Subject"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, ">"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 98
    :cond_2
    iget-object v6, p0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->body:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 99
    const-string/jumbo v6, "<"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "Body"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, ">"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 100
    iget-object v6, p0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->body:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/XmlUtils;->wrapInCDATA(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 101
    const-string/jumbo v6, "</"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "Body"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, ">"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 104
    :cond_3
    const-string/jumbo v6, "</"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "TTSMessageRequest"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, ">"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 106
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .end local v1    # "language":Ljava/lang/String;
    :cond_4
    move-object v1, p1

    .line 69
    goto/16 :goto_0
.end method

.method public static parseTTSResponse(Lcom/vlingo/sdk/internal/http/HttpResponse;)[B
    .locals 3
    .param p0, "res"    # Lcom/vlingo/sdk/internal/http/HttpResponse;

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/HttpResponse;->getDataAsBytes()[B

    move-result-object v0

    .line 51
    .local v0, "data":[B
    if-eqz v0, :cond_0

    .line 52
    array-length v1, v0

    const/16 v2, 0x190

    if-ge v1, v2, :cond_1

    .line 53
    const/4 v0, 0x0

    .line 59
    :cond_0
    :goto_0
    return-object v0

    .line 55
    :cond_1
    const/4 v1, 0x0

    aget-byte v1, v0, v1

    const/16 v2, 0x3c

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    aget-byte v1, v0, v1

    const/16 v2, 0x3f

    if-ne v1, v2, :cond_0

    const/4 v1, 0x2

    aget-byte v1, v0, v1

    const/16 v2, 0x78

    if-ne v1, v2, :cond_0

    const/4 v1, 0x3

    aget-byte v1, v0, v1

    const/16 v2, 0x6d

    if-ne v1, v2, :cond_0

    const/4 v1, 0x4

    aget-byte v1, v0, v1

    const/16 v2, 0x6c

    if-ne v1, v2, :cond_0

    .line 56
    const/4 v0, 0x0

    goto :goto_0
.end method
