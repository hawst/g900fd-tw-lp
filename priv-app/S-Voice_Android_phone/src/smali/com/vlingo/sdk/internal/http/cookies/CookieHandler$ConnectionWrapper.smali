.class interface abstract Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;
.super Ljava/lang/Object;
.source "CookieHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "ConnectionWrapper"
.end annotation


# virtual methods
.method public abstract getHeaderField(I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getHeaderFieldKey(I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getHost()Ljava/lang/String;
.end method
