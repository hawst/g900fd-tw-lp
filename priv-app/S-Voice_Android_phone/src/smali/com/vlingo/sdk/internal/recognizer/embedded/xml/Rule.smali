.class public Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;
.source "Rule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$1;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$TemplateBuilder;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$TemplateMapBuilder;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;
    }
.end annotation


# static fields
.field public static final ACTION_LIST_TAG_NAME:Ljava/lang/String; = "ActionList"

.field public static final RULE_TAG_NAME:Ljava/lang/String; = "ResultBlock"

.field public static final TAG_TAG_NAME:Ljava/lang/String; = "Tag"

.field public static final TEMPLATE_TAG_NAME:Ljava/lang/String; = "TtaTemplate"


# instance fields
.field private actionsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;",
            ">;"
        }
    .end annotation
.end field

.field private tagNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;-><init>()V

    .line 171
    return-void
.end method

.method static synthetic access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 16
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;->tagNames:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 16
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;->actionsList:Ljava/util/List;

    return-object p1
.end method

.method public static getBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$1;)V

    return-object v0
.end method

.method public static getTemplateBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 168
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$TemplateBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$TemplateBuilder;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$1;)V

    return-object v0
.end method

.method public static getTemplatesMapBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 127
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$TemplateMapBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$TemplateMapBuilder;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$1;)V

    return-object v0
.end method


# virtual methods
.method public getActionsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;->actionsList:Ljava/util/List;

    return-object v0
.end method

.method public getTagNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule;->tagNames:Ljava/util/List;

    return-object v0
.end method
