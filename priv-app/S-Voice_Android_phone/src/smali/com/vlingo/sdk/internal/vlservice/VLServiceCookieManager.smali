.class public abstract Lcom/vlingo/sdk/internal/vlservice/VLServiceCookieManager;
.super Ljava/lang/Object;
.source "VLServiceCookieManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/vlingo/sdk/internal/http/cookies/CookieJarManager;
    .locals 2

    .prologue
    .line 18
    const-class v0, Lcom/vlingo/sdk/internal/vlservice/VLServiceCookieManager;

    monitor-enter v0

    :try_start_0
    invoke-static {}, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManagerSingleton;->getInstance()Lcom/vlingo/sdk/internal/http/cookies/CookieJarManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method
