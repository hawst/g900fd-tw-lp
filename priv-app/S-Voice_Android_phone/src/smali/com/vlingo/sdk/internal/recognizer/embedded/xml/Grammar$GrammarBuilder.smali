.class Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar$GrammarBuilder;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
.source "Grammar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GrammarBuilder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;",
        ">;"
    }
.end annotation


# instance fields
.field private configurations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;",
            ">;"
        }
    .end annotation
.end field

.field private events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;-><init>()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar$GrammarBuilder;->configurations:Ljava/util/List;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar$GrammarBuilder;->events:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar$1;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar$GrammarBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public addChildTagObject(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "childName"    # Ljava/lang/String;
    .param p2, "tagObject"    # Ljava/lang/Object;

    .prologue
    .line 67
    const-string/jumbo v0, "Configuration"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    instance-of v0, p2, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;

    if-eqz v0, :cond_1

    .line 68
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar$GrammarBuilder;->configurations:Ljava/util/List;

    check-cast p2, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;

    .end local p2    # "tagObject":Ljava/lang/Object;
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 69
    .restart local p2    # "tagObject":Ljava/lang/Object;
    :cond_1
    const-string/jumbo v0, "Event"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p2, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar$GrammarBuilder;->events:Ljava/util/List;

    check-cast p2, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;

    .end local p2    # "tagObject":Ljava/lang/Object;
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected addChildrenBuilders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 79
    .local p1, "builders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder<*>;>;"
    const-string/jumbo v0, "Configuration"

    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Configuration;->getBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    const-string/jumbo v0, "Event"

    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Event;->getBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    return-void
.end method

.method public addParametersNames(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p1, "paramNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v0, "rule"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    return-void
.end method

.method protected buildResultObject()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;
    .locals 3

    .prologue
    .line 45
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;-><init>()V

    .line 46
    .local v0, "result":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar$GrammarBuilder;->configurations:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;->configurationList:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;->access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;Ljava/util/List;)Ljava/util/List;

    .line 47
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar$GrammarBuilder;->events:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;->eventsList:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;->access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;Ljava/util/List;)Ljava/util/List;

    .line 48
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar$GrammarBuilder;->parametersMap:Ljava/util/Map;

    const-string/jumbo v2, "rule"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;->ruleName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;->access$302(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;Ljava/lang/String;)Ljava/lang/String;

    .line 49
    return-object v0
.end method

.method protected bridge synthetic buildResultObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar$GrammarBuilder;->buildResultObject()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar$GrammarBuilder;->configurations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 55
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar$GrammarBuilder;->events:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 56
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Grammar$GrammarBuilder;->parametersMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 57
    return-void
.end method

.method public getTagName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    const-string/jumbo v0, "Grammar"

    return-object v0
.end method
