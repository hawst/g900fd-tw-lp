.class public Lcom/vlingo/sdk/internal/http/cookies/CookieJarManagerSingleton;
.super Ljava/lang/Object;
.source "CookieJarManagerSingleton.java"


# static fields
.field private static ImplClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static instance:Lcom/vlingo/sdk/internal/http/cookies/CookieJarManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    sput-object v0, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManagerSingleton;->instance:Lcom/vlingo/sdk/internal/http/cookies/CookieJarManager;

    .line 23
    sput-object v0, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManagerSingleton;->ImplClass:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cleanup()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53
    sput-object v0, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManagerSingleton;->ImplClass:Ljava/lang/Class;

    .line 54
    sput-object v0, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManagerSingleton;->instance:Lcom/vlingo/sdk/internal/http/cookies/CookieJarManager;

    .line 55
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/vlingo/sdk/internal/http/cookies/CookieJarManager;
    .locals 5

    .prologue
    .line 34
    const-class v2, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManagerSingleton;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManagerSingleton;->instance:Lcom/vlingo/sdk/internal/http/cookies/CookieJarManager;

    if-nez v1, :cond_1

    .line 35
    sget-object v1, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManagerSingleton;->ImplClass:Ljava/lang/Class;

    if-nez v1, :cond_0

    .line 36
    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v3, "CookieJarManager implementation class is not set"

    invoke-direct {v1, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    .local v0, "ex":Ljava/lang/InstantiationException;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 38
    .end local v0    # "ex":Ljava/lang/InstantiationException;
    :cond_0
    :try_start_1
    sget-object v1, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManagerSingleton;->ImplClass:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManager;

    sput-object v1, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManagerSingleton;->instance:Lcom/vlingo/sdk/internal/http/cookies/CookieJarManager;
    :try_end_1
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 49
    :cond_1
    :try_start_2
    sget-object v1, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManagerSingleton;->instance:Lcom/vlingo/sdk/internal/http/cookies/CookieJarManager;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v2

    return-object v1

    .line 39
    :catch_0
    move-exception v0

    .line 42
    .restart local v0    # "ex":Ljava/lang/InstantiationException;
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "CookieJarManager InstantiationException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 43
    .end local v0    # "ex":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v0

    .line 46
    .local v0, "ex":Ljava/lang/IllegalAccessException;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "CookieJarManager IllegalAccessException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public static declared-synchronized setCookieJarManagerImpl(Ljava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p0, "implClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v1, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManagerSingleton;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 27
    :try_start_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "CookieJarManager clazz null"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 26
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 28
    :cond_0
    :try_start_1
    const-class v0, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManager;

    invoke-virtual {v0, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 29
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "CookieJarManager invalid impl: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_1
    sput-object p0, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManagerSingleton;->ImplClass:Ljava/lang/Class;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 31
    monitor-exit v1

    return-void
.end method
