.class Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask$1;
.super Ljava/lang/Object;
.source "VoconTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;

.field final synthetic val$finalSuccess:Z

.field final synthetic val$finalUnexpectedException:Ljava/lang/Exception;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;ZLjava/lang/Exception;)V
    .locals 0

    .prologue
    .line 32
    .local p0, "this":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask$1;, "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask.1;"
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask$1;->this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;

    iput-boolean p2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask$1;->val$finalSuccess:Z

    iput-object p3, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask$1;->val$finalUnexpectedException:Ljava/lang/Exception;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 35
    .local p0, "this":Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask$1;, "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask.1;"
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask$1;->val$finalSuccess:Z

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask$1;->this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask$1;->this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;->callback:Ljava/lang/Object;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;->access$000(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;->onExecutionSuccess(Ljava/lang/Object;)V

    .line 43
    :goto_0
    return-void

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask$1;->val$finalUnexpectedException:Ljava/lang/Exception;

    if-eqz v0, :cond_1

    .line 39
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask$1;->this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask$1;->val$finalUnexpectedException:Ljava/lang/Exception;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;->onUnexpectedError(Ljava/lang/Exception;)V

    .line 41
    :cond_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask$1;->this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask$1;->this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;->callback:Ljava/lang/Object;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;->access$000(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconTask;->onExecutionFailed(Ljava/lang/Object;)V

    goto :goto_0
.end method
