.class Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
.super Ljava/lang/Object;
.source "ASRRequest.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "State"
.end annotation


# instance fields
.field private mNextState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V
    .locals 0

    .prologue
    .line 727
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p2, "x1"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;

    .prologue
    .line 727
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V

    return-void
.end method


# virtual methods
.method public cancel(Z)V
    .locals 2
    .param p1, "timedOut"    # Z

    .prologue
    .line 788
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "[LatencyCheck] cancel(): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p1, :cond_0

    const-string/jumbo v0, "due"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " to timeout"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->log(Ljava/lang/String;)V

    .line 790
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->mCancelledState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$2100(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->setState(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V

    .line 792
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->endAudio()Z

    .line 793
    return-void

    .line 788
    :cond_0
    const-string/jumbo v0, "not due"

    goto :goto_0
.end method

.method public endAudio()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 752
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v3, v3}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;-><init>([BII)V

    .line 753
    .local v0, "segment":Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;->setEndSegment()V

    .line 754
    const-string/jumbo v2, "endAudio(): queuing end-of-audio segment"

    invoke-virtual {p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->log(Ljava/lang/String;)V

    .line 755
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getAudioQueue()Ljava/util/concurrent/BlockingQueue;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1800(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/util/concurrent/BlockingQueue;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    move-result v1

    .line 756
    .local v1, "success":Z
    if-nez v1, :cond_0

    .line 757
    const-string/jumbo v2, "Unable to add end-of-audio AudioSegment to audio queue (queue full?)"

    invoke-virtual {p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->log(Ljava/lang/String;)V

    .line 759
    :cond_0
    return v1
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 772
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const-string/jumbo v1, "finish()"

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->serverCheckLog(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/String;)V

    .line 776
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isFinished:Z
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1900(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 777
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const/4 v1, 0x1

    # setter for: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->isFinished:Z
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1902(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Z)Z

    .line 778
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->startTimeoutTimer()V
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$2000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V

    .line 781
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->endAudio()Z

    move-result v0

    if-nez v0, :cond_0

    .line 782
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const/4 v1, -0x2

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->onError(I)V
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;I)V

    .line 785
    :cond_0
    return-void
.end method

.method protected hasTransition()Z
    .locals 1

    .prologue
    .line 823
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->mNextState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 795
    const/4 v0, 0x0

    return v0
.end method

.method public isResponseReceived()Z
    .locals 1

    .prologue
    .line 796
    const/4 v0, 0x0

    return v0
.end method

.method protected log(Ljava/lang/String;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 850
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->log(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;Ljava/lang/String;)V
    invoke-static {v0, p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$2500(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;Ljava/lang/String;)V

    .line 851
    return-void
.end method

.method protected log(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 846
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->log(Ljava/lang/Throwable;)V
    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$2400(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/Throwable;)V

    .line 847
    return-void
.end method

.method public noTransition()V
    .locals 1

    .prologue
    .line 803
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->mNextState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .line 804
    return-void
.end method

.method protected onRun()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 814
    return-void
.end method

.method public final run()V
    .locals 3

    .prologue
    .line 739
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->onRun()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 745
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->transition()V

    .line 746
    return-void

    .line 740
    :catch_0
    move-exception v0

    .line 741
    .local v0, "t":Ljava/lang/Throwable;
    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->log(Ljava/lang/Throwable;)V

    .line 742
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const/4 v2, -0x2

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->onError(I)V
    invoke-static {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;I)V

    goto :goto_0
.end method

.method public sendAudio([BII)V
    .locals 3
    .param p1, "audio"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 763
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;

    invoke-direct {v0, p1, p2, p3}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;-><init>([BII)V

    .line 764
    .local v0, "segment":Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "sendAudio(): queuing "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->log(Ljava/lang/String;)V

    .line 765
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getAudioQueue()Ljava/util/concurrent/BlockingQueue;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1800(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/util/concurrent/BlockingQueue;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 766
    const-string/jumbo v1, "Unable to add AudioSegment to audio queue (queue full?)"

    invoke-virtual {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->log(Ljava/lang/String;)V

    .line 767
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const/4 v2, -0x2

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->onError(I)V
    invoke-static {v1, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;I)V

    .line 769
    :cond_0
    return-void
.end method

.method protected setState(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V
    .locals 1
    .param p1, "state"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .prologue
    .line 842
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->changeState(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V
    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$2300(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V

    .line 843
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 807
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected transition()V
    .locals 2

    .prologue
    .line 831
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->hasTransition()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 832
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getState()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$800(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->mNextState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->setState(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V

    .line 833
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->runCurrentState()V
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$2200(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V

    .line 835
    :cond_0
    return-void
.end method

.method public transitionTo(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V
    .locals 0
    .param p1, "state"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .prologue
    .line 799
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;->mNextState:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .line 800
    return-void
.end method
