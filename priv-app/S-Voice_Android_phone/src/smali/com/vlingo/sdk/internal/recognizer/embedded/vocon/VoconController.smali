.class public final Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;
.super Ljava/lang/Object;
.source "VoconController.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask$Callback;
.implements Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$Callback;
.implements Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask$Callback;
.implements Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$1;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;
    }
.end annotation


# static fields
.field static final VOCON_DATA_DIR:Ljava/lang/String; = "vocon"

.field private static instance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;


# instance fields
.field private final grammarVocabularies:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;",
            ">;"
        }
    .end annotation
.end field

.field private final mainHandler:Landroid/os/Handler;

.field private recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

.field private volatile referenceCount:I

.field private state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

.field private final stateSync:Ljava/lang/Object;

.field private updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

.field private voconConfig:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;

.field private final workerHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->instance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->stateSync:Ljava/lang/Object;

    .line 25
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->UNINITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    .line 26
    const/4 v1, 0x1

    iput v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->referenceCount:I

    .line 32
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->grammarVocabularies:Ljava/util/List;

    .line 55
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->mainHandler:Landroid/os/Handler;

    .line 56
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "VoconController"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 57
    .local v0, "workerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 58
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->workerHandler:Landroid/os/Handler;

    .line 59
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->init()V

    .line 60
    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 1
    .param p1, "mainHandlerValue"    # Landroid/os/Handler;
    .param p2, "workerHandlerValue"    # Landroid/os/Handler;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->stateSync:Ljava/lang/Object;

    .line 25
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->UNINITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    .line 26
    const/4 v0, 0x1

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->referenceCount:I

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->grammarVocabularies:Ljava/util/List;

    .line 314
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->referenceCount:I

    .line 315
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->mainHandler:Landroid/os/Handler;

    .line 316
    iput-object p2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->workerHandler:Landroid/os/Handler;

    .line 317
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->init()V

    .line 318
    return-void
.end method

.method public static declared-synchronized bindToInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;
    .locals 3

    .prologue
    .line 35
    const-class v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->instance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;-><init>()V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->instance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    .line 41
    :goto_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->instance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 38
    :cond_0
    :try_start_1
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->instance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    iget v2, v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->referenceCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->referenceCount:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getTestInstance()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 322
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->instance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    return-object v0
.end method

.method private init()V
    .locals 4

    .prologue
    .line 63
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->stateSync:Ljava/lang/Object;

    monitor-enter v1

    .line 64
    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$1;->$SwitchMap$com$vlingo$sdk$internal$recognizer$embedded$vocon$VoconController$State:[I

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 72
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Vocon controller already initializing"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 66
    :pswitch_0
    :try_start_1
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->INITIALIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    .line 67
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->workerHandler:Landroid/os/Handler;

    new-instance v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->mainHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconInitTask$Callback;Landroid/os/Handler;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 74
    monitor-exit v1

    .line 75
    return-void

    .line 70
    :pswitch_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Vocon controller was destroyed. No more interactions are allowed"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 64
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private release()V
    .locals 4

    .prologue
    .line 78
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->stateSync:Ljava/lang/Object;

    monitor-enter v1

    .line 79
    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$1;->$SwitchMap$com$vlingo$sdk$internal$recognizer$embedded$vocon$VoconController$State:[I

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 105
    :goto_0
    :pswitch_0
    monitor-exit v1

    .line 106
    return-void

    .line 83
    :pswitch_1
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->RELEASING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    .line 84
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->workerHandler:Landroid/os/Handler;

    new-instance v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->mainHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask$Callback;Landroid/os/Handler;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 87
    :pswitch_2
    :try_start_1
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->RELEASING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    .line 88
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->cancelRecognition()V

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    .line 90
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->workerHandler:Landroid/os/Handler;

    new-instance v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->mainHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask$Callback;Landroid/os/Handler;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 93
    :pswitch_3
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->RELEASING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    .line 94
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->cancel()V

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    .line 96
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->workerHandler:Landroid/os/Handler;

    new-instance v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->mainHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconReleaseTask$Callback;Landroid/os/Handler;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 79
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static setTestInstance(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 1
    .param p0, "mainHandler"    # Landroid/os/Handler;
    .param p1, "workerHandler"    # Landroid/os/Handler;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 305
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->instance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    if-eqz v0, :cond_0

    .line 306
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->instance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->release()V

    .line 307
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->instance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    .line 309
    :cond_0
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;-><init>(Landroid/os/Handler;Landroid/os/Handler;)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->instance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    .line 310
    return-void
.end method

.method public static declared-synchronized unbindFromInstance(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;)V
    .locals 3
    .param p0, "voconController"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    .prologue
    .line 45
    const-class v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->instance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->instance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->instance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    iget v2, v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->referenceCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->referenceCount:I

    .line 47
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->instance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->referenceCount:I

    if-gtz v0, :cond_0

    .line 48
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->instance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->release()V

    .line 49
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->instance:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    :cond_0
    monitor-exit v1

    return-void

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public getReferenceCount()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 327
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->referenceCount:I

    return v0
.end method

.method public getState()Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 332
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    return-object v0
.end method

.method public onVoconInitFailed()V
    .locals 3

    .prologue
    .line 207
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->stateSync:Ljava/lang/Object;

    monitor-enter v1

    .line 208
    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$1;->$SwitchMap$com$vlingo$sdk$internal$recognizer$embedded$vocon$VoconController$State:[I

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 225
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Not in INITIALIZING state during error of vocon init"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 210
    :pswitch_1
    :try_start_1
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->UNINITIALIZED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    .line 211
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->cancelRecognition()V

    .line 213
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 216
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->cancel()V

    .line 217
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    .line 227
    :cond_1
    :pswitch_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 228
    return-void

    .line 208
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onVoconInitSuccess(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;Ljava/util/List;)V
    .locals 3
    .param p1, "voconConfig"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 180
    .local p2, "grammarVocabularies":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/jni/GrammarVocabulary;>;"
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->stateSync:Ljava/lang/Object;

    monitor-enter v1

    .line 181
    :try_start_0
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->voconConfig:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconConfig;

    .line 182
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->grammarVocabularies:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 183
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$1;->$SwitchMap$com$vlingo$sdk$internal$recognizer$embedded$vocon$VoconController$State:[I

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 200
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Not in INITIALIZING state during completion of vocon init"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 185
    :pswitch_1
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 186
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    .line 187
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->workerHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 202
    :goto_0
    :pswitch_2
    monitor-exit v1

    .line 203
    return-void

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 189
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->UPDATING_SLOTS:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    .line 190
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->workerHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 192
    :cond_1
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->IDLE:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 183
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onVoconRecognitionCompleted()V
    .locals 3

    .prologue
    .line 271
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->stateSync:Ljava/lang/Object;

    monitor-enter v1

    .line 272
    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$1;->$SwitchMap$com$vlingo$sdk$internal$recognizer$embedded$vocon$VoconController$State:[I

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 288
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Not in RECOGNIZING state during completion of recognition"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 290
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 274
    :pswitch_1
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    .line 275
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->workerHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 277
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->UPDATING_SLOTS:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    .line 290
    :goto_0
    monitor-exit v1

    .line 291
    return-void

    .line 279
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->IDLE:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    goto :goto_0

    .line 283
    :pswitch_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 272
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onVoconReleased()V
    .locals 3

    .prologue
    .line 232
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->stateSync:Ljava/lang/Object;

    monitor-enter v1

    .line 233
    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$1;->$SwitchMap$com$vlingo$sdk$internal$recognizer$embedded$vocon$VoconController$State:[I

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 240
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Not in RELEASING state during vocon released"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 242
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 235
    :pswitch_0
    :try_start_1
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->DESTROYED:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    .line 236
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->workerHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 237
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->workerHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 242
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 243
    return-void

    .line 233
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
    .end packed-switch
.end method

.method public onVoconUpdateSlotsCompleted()V
    .locals 3

    .prologue
    .line 247
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->stateSync:Ljava/lang/Object;

    monitor-enter v1

    .line 248
    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$1;->$SwitchMap$com$vlingo$sdk$internal$recognizer$embedded$vocon$VoconController$State:[I

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 264
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Not in UPDATING_SLOTS state during completion of slots update"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 266
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 250
    :pswitch_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    .line 251
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->workerHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 253
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    .line 266
    :goto_0
    monitor-exit v1

    .line 267
    return-void

    .line 255
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->IDLE:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    goto :goto_0

    .line 259
    :pswitch_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 248
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public recognize(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizerListener;Lcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognitionControl;
    .locals 3
    .param p1, "recognizerListener"    # Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizerListener;
    .param p2, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;

    .prologue
    .line 109
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->stateSync:Ljava/lang/Object;

    monitor-enter v1

    .line 110
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 111
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;->READER_ERROR:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;

    const-string/jumbo v2, "Can\'t record.  Please try again."

    invoke-interface {p1, v0, v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizerListener;->onRecognizerError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V

    .line 115
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    .line 116
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$1;->$SwitchMap$com$vlingo$sdk$internal$recognizer$embedded$vocon$VoconController$State:[I

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 135
    :pswitch_0
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;->READER_ERROR:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;

    const-string/jumbo v2, "Can\'t record.  Please try again."

    invoke-interface {p1, v0, v2}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizerListener;->onRecognizerError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V

    .line 139
    :goto_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    monitor-exit v1

    return-object v0

    .line 118
    :pswitch_1
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->init()V

    .line 119
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->mainHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v2, p2, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$Callback;Landroid/os/Handler;Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizerListener;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    goto :goto_0

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 123
    :pswitch_2
    :try_start_1
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->mainHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v2, p2, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$Callback;Landroid/os/Handler;Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizerListener;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    goto :goto_0

    .line 126
    :pswitch_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "During RECOGNIZING state recognitionTask should be null or cancelled"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :pswitch_4
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->RECOGNIZING:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    .line 129
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->mainHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v2, p2, p1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask$Callback;Landroid/os/Handler;Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecognizerListener;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    .line 130
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->workerHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->recognitionTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconRecoTask;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 116
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public updateSlots(Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerCallback;Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;)Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerControl;
    .locals 7
    .param p1, "listener"    # Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerCallback;
    .param p2, "slotsUpdate"    # Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;

    .prologue
    .line 144
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->stateSync:Ljava/lang/Object;

    monitor-enter v6

    .line 145
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    invoke-interface {p1}, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerCallback;->onCancelled()V

    .line 150
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    .line 151
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$1;->$SwitchMap$com$vlingo$sdk$internal$recognizer$embedded$vocon$VoconController$State:[I

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 170
    :pswitch_0
    invoke-interface {p1}, Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerCallback;->onCancelled()V

    .line 174
    :goto_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    return-object v0

    .line 153
    :pswitch_1
    :try_start_1
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->init()V

    .line 154
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->mainHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->grammarVocabularies:Ljava/util/List;

    move-object v1, p0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$Callback;Landroid/os/Handler;Ljava/util/List;Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerCallback;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    goto :goto_0

    .line 174
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 158
    :pswitch_2
    :try_start_2
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->mainHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->grammarVocabularies:Ljava/util/List;

    move-object v1, p0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$Callback;Landroid/os/Handler;Ljava/util/List;Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerCallback;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    goto :goto_0

    .line 161
    :pswitch_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "During UPDATING_SLOTS state updateSlotsTask should be null or cancelled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :pswitch_4
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;->UPDATING_SLOTS:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->state:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController$State;

    .line 164
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->mainHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->grammarVocabularies:Ljava/util/List;

    move-object v1, p0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask$Callback;Landroid/os/Handler;Ljava/util/List;Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerSlotsUpdate;Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainerCallback;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    .line 165
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->workerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconController;->updateSlotsTask:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconUpdateSlotsTask;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 151
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
