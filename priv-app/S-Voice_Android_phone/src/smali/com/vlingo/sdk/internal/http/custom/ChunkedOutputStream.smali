.class public Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;
.super Ljava/io/OutputStream;
.source "ChunkedOutputStream.java"


# static fields
.field private static final DEFAULT_MIN_CHUNK_SIZE:I = 0x400

.field private static log:Lcom/vlingo/sdk/internal/logging/Logger;


# instance fields
.field private ivClosed:Z

.field private ivMinChunkSize:I

.field private ivOut:Ljava/io/OutputStream;

.field private ivWriteBuffer:Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;

.field private singleByteBuffer:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/sdk/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->log:Lcom/vlingo/sdk/internal/logging/Logger;

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 25
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->singleByteBuffer:[B

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->ivClosed:Z

    .line 29
    const/16 v0, 0x400

    iput v0, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->ivMinChunkSize:I

    .line 35
    new-instance v0, Lcom/vlingo/sdk/internal/http/custom/BufferedOutputStream;

    iget v1, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->ivMinChunkSize:I

    mul-int/lit8 v1, v1, 0x3

    invoke-direct {v0, p1, v1}, Lcom/vlingo/sdk/internal/http/custom/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->ivOut:Ljava/io/OutputStream;

    .line 36
    new-instance v0, Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;

    iget v1, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->ivMinChunkSize:I

    mul-int/lit8 v1, v1, 0x2

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->ivWriteBuffer:Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;

    .line 37
    return-void
.end method

.method private bufferedWrite([BII)V
    .locals 2
    .param p1, "buff"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->checkClosed()V

    .line 54
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->ivWriteBuffer:Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;->write([BII)V

    .line 56
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->ivWriteBuffer:Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;->size()I

    move-result v0

    iget v1, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->ivMinChunkSize:I

    if-lt v0, v1, :cond_0

    .line 57
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->flushBuffer()V

    .line 59
    :cond_0
    return-void
.end method

.method private checkClosed()V
    .locals 2

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->ivClosed:Z

    if-eqz v0, :cond_0

    .line 42
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Stream is closed already"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    return-void
.end method

.method private flushBuffer()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->ivWriteBuffer:Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;->getByteArray()[B

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->ivWriteBuffer:Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;->size()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->writeChunk([BII)V

    .line 63
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->ivWriteBuffer:Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/util/NoCopyByteArrayOutputStream;->reset()V

    .line 64
    return-void
.end method

.method private writeChunk([BII)V
    .locals 1
    .param p1, "buff"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->ivOut:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 72
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->ivOut:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 73
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->ivClosed:Z

    .line 49
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->ivOut:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 50
    return-void
.end method

.method public declared-synchronized flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->flushBuffer()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    monitor-exit p0

    return-void

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized write(I)V
    .locals 3
    .param p1, "it"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->singleByteBuffer:[B

    const/4 v1, 0x0

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 87
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->singleByteBuffer:[B

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->bufferedWrite([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    monitor-exit p0

    return-void

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized write([B)V
    .locals 2
    .param p1, "buff"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->bufferedWrite([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    monitor-exit p0

    return-void

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized write([BII)V
    .locals 1
    .param p1, "buff"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->bufferedWrite([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    monitor-exit p0

    return-void

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized writeLastChunk()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->checkClosed()V

    .line 79
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->flushBuffer()V

    .line 81
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->ivOut:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    monitor-exit p0

    return-void

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
