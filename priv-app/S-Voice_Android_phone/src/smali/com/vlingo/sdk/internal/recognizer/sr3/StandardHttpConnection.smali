.class public Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;
.super Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;
.source "StandardHttpConnection.java"


# instance fields
.field private ivCon:Lcom/vlingo/sdk/internal/net/HttpConnection;

.field private ivDin:Ljava/io/InputStream;

.field private ivDout:Ljava/io/DataOutputStream;

.field private ivMPOut:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/net/HttpConnection;I)V
    .locals 0
    .param p1, "con"    # Lcom/vlingo/sdk/internal/net/HttpConnection;
    .param p2, "requestID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p2}, Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;-><init>(I)V

    .line 47
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/net/HttpConnection;

    .line 48
    return-void
.end method

.method public static newConnection(Lcom/vlingo/sdk/internal/net/ConnectionProvider;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Ljava/util/Hashtable;ILcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;
    .locals 10
    .param p0, "connectionProvider"    # Lcom/vlingo/sdk/internal/net/ConnectionProvider;
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "client"    # Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    .param p4, "software"    # Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;
    .param p6, "clientRequestId"    # I
    .param p7, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/internal/net/ConnectionProvider;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/ClientMeta;",
            "Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I",
            "Lcom/vlingo/sdk/internal/recognizer/SRContext;",
            ")",
            "Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    .local p5, "cookies":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v6, Ljava/util/Hashtable;

    invoke-direct {v6}, Ljava/util/Hashtable;-><init>()V

    .line 57
    .local v6, "headers":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, p7

    invoke-static {v6, p3, p4, v0}, Lcom/vlingo/sdk/internal/vlservice/VLServiceUtil;->addStandardVlingoHttpHeaders(Ljava/util/Hashtable;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Lcom/vlingo/sdk/internal/recognizer/SRContext;)V

    .line 58
    invoke-static {p2}, Lcom/vlingo/sdk/internal/http/HttpUtil;->getDomain(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Lcom/vlingo/sdk/internal/http/HttpUtil;->getPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p5, v1, v2}, Lcom/vlingo/sdk/internal/vlservice/VLServiceUtil;->addVLServiceCookies(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object p5

    .line 59
    invoke-static {}, Lcom/vlingo/sdk/internal/settings/Settings;->isAsrKeepAliveEnabled()Z

    move-result v4

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p5

    move-object v7, p3

    move-object v8, p4

    invoke-static/range {v1 .. v8}, Lcom/vlingo/sdk/internal/http/HttpUtil;->newHttpConnection(Lcom/vlingo/sdk/internal/net/ConnectionProvider;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Hashtable;Ljava/util/Hashtable;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;)Lcom/vlingo/sdk/internal/net/HttpConnection;

    move-result-object v9

    .line 63
    .local v9, "hc":Lcom/vlingo/sdk/internal/net/HttpConnection;
    new-instance v1, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;

    move/from16 v0, p6

    invoke-direct {v1, v9, v0}, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;-><init>(Lcom/vlingo/sdk/internal/net/HttpConnection;I)V

    return-object v1
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivDin:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivDin:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 116
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivMPOut:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    if-eqz v0, :cond_1

    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivMPOut:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 117
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivDout:Ljava/io/DataOutputStream;

    if-eqz v0, :cond_2

    :try_start_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivDout:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 118
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/net/HttpConnection;

    if-eqz v0, :cond_3

    :try_start_3
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/net/HttpConnection;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/net/HttpConnection;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 119
    :cond_3
    :goto_3
    return-void

    .line 118
    :catch_0
    move-exception v0

    goto :goto_3

    .line 117
    :catch_1
    move-exception v0

    goto :goto_2

    .line 116
    :catch_2
    move-exception v0

    goto :goto_1

    .line 115
    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method public finishRequest()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    return-void
.end method

.method public finishResponse()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->close()V

    .line 127
    return-void
.end method

.method public getConnection()Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIn()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivDin:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/net/HttpConnection;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/net/HttpConnection;->openDataInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivDin:Ljava/io/InputStream;

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivDin:Ljava/io/InputStream;

    return-object v0
.end method

.method public getOut()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivMPOut:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    if-nez v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/net/HttpConnection;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/net/HttpConnection;->openDataOutputStream()Ljava/io/DataOutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivDout:Ljava/io/DataOutputStream;

    .line 96
    new-instance v0, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivDout:Ljava/io/DataOutputStream;

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivBoundary:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;-><init>(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivMPOut:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivMPOut:Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    return-object v0
.end method

.method public getRequestID()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivRequestID:I

    return v0
.end method

.method public getResponseHeaderField(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/net/HttpConnection;

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/internal/net/HttpConnection;->getHeaderField(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResponseHeaderFieldKey(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/net/HttpConnection;

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/internal/net/HttpConnection;->getHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/StandardHttpConnection;->ivCon:Lcom/vlingo/sdk/internal/net/HttpConnection;

    invoke-interface {v0, p1, p2}, Lcom/vlingo/sdk/internal/net/HttpConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    return-void
.end method
