.class public Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
.super Lcom/vlingo/sdk/internal/http/HttpRequest;
.source "VLHttpServiceRequest.java"


# static fields
.field public static final RESPONSE_ENCODING_JSON:Ljava/lang/String; = "application/json"

.field public static final RESPONSE_ENCODING_XML:Ljava/lang/String; = "text/xml"


# instance fields
.field private mLanguage:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Ljava/lang/String;[BLjava/lang/String;)V
    .locals 0
    .param p1, "taskName"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/vlingo/sdk/internal/http/HttpCallback;
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "data"    # [B
    .param p5, "language"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/vlingo/sdk/internal/http/HttpRequest;-><init>(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Ljava/lang/String;[B)V

    .line 40
    iput-object p5, p0, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->mLanguage:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public static createVLRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Lcom/vlingo/sdk/internal/http/URL;Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    .locals 6
    .param p0, "taskName"    # Ljava/lang/String;
    .param p1, "callback"    # Lcom/vlingo/sdk/internal/http/HttpCallback;
    .param p2, "url"    # Lcom/vlingo/sdk/internal/http/URL;
    .param p3, "request"    # Ljava/lang/String;

    .prologue
    .line 35
    new-instance v0, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    iget-object v3, p2, Lcom/vlingo/sdk/internal/http/URL;->urlField:Ljava/lang/String;

    invoke-static {p3}, Lcom/vlingo/sdk/internal/util/StringUtils;->convertStringToBytes(Ljava/lang/String;)[B

    move-result-object v4

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;-><init>(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Ljava/lang/String;[BLjava/lang/String;)V

    return-object v0
.end method

.method public static createVLRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Lcom/vlingo/sdk/internal/http/URL;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    .locals 6
    .param p0, "taskName"    # Ljava/lang/String;
    .param p1, "callback"    # Lcom/vlingo/sdk/internal/http/HttpCallback;
    .param p2, "url"    # Lcom/vlingo/sdk/internal/http/URL;
    .param p3, "request"    # Ljava/lang/String;
    .param p4, "language"    # Ljava/lang/String;

    .prologue
    .line 31
    new-instance v0, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    iget-object v3, p2, Lcom/vlingo/sdk/internal/http/URL;->urlField:Ljava/lang/String;

    invoke-static {p3}, Lcom/vlingo/sdk/internal/util/StringUtils;->convertStringToBytes(Ljava/lang/String;)[B

    move-result-object v4

    move-object v1, p0

    move-object v2, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;-><init>(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Ljava/lang/String;[BLjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public fetchResponse()Lcom/vlingo/sdk/internal/http/HttpResponse;
    .locals 6

    .prologue
    .line 56
    iget-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->extraHeaders:Ljava/util/Hashtable;

    if-nez v1, :cond_0

    .line 57
    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    iput-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->extraHeaders:Ljava/util/Hashtable;

    .line 58
    :cond_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->cookies:Ljava/util/Hashtable;

    if-nez v1, :cond_1

    .line 59
    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    iput-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->cookies:Ljava/util/Hashtable;

    .line 62
    :cond_1
    iget-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->extraHeaders:Ljava/util/Hashtable;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->clientHttp:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->softwareHttp:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    iget-object v4, p0, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->mLanguage:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v1, v2, v3, v4, v5}, Lcom/vlingo/sdk/internal/vlservice/VLServiceUtil;->addStandardVlingoHttpHeaders(Ljava/util/Hashtable;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/SRContext;)V

    .line 65
    iget-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->cookies:Ljava/util/Hashtable;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->url:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/sdk/internal/http/HttpUtil;->getDomain(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->url:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/sdk/internal/http/HttpUtil;->getPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/vlingo/sdk/internal/vlservice/VLServiceUtil;->addVLServiceCookies(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->cookies:Ljava/util/Hashtable;

    .line 67
    invoke-super {p0}, Lcom/vlingo/sdk/internal/http/HttpRequest;->fetchResponse()Lcom/vlingo/sdk/internal/http/HttpResponse;

    move-result-object v0

    .line 69
    .local v0, "response":Lcom/vlingo/sdk/internal/http/HttpResponse;
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/HttpResponse;->getCookies()Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/sdk/internal/vlservice/VLServiceUtil;->handleResponseCookies(Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)V

    .line 71
    return-object v0
.end method

.method public setResponseEncoding(Ljava/lang/String;)V
    .locals 1
    .param p1, "responseEncoding"    # Ljava/lang/String;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->extraHeaders:Ljava/util/Hashtable;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->extraHeaders:Ljava/util/Hashtable;

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->extraHeaders:Ljava/util/Hashtable;

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/vlservice/VLServiceUtil;->setProtocolHeader(Ljava/util/Hashtable;Ljava/lang/String;)V

    .line 47
    return-void
.end method
