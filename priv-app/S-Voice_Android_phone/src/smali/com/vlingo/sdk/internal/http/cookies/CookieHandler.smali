.class public Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;
.super Ljava/lang/Object;
.source "CookieHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpURLConnectionWrapper;,
        Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpConnectionAdapterWrapper;,
        Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpConnectionWrapper;,
        Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    return-void
.end method

.method private static extractCookies(Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;
    .locals 8
    .param p0, "conn"    # Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;
    .param p1, "cookieJar"    # Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    .prologue
    .line 157
    invoke-interface {p0}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 158
    .local v0, "domain":Ljava/lang/String;
    const/4 v3, 0x0

    .line 161
    .local v3, "key":Ljava/lang/String;
    const/4 v2, 0x0

    .line 163
    .local v2, "i":I
    :goto_0
    :try_start_0
    invoke-interface {p0, v2}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;->getHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 166
    const-string/jumbo v5, "Set-Cookie"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 169
    invoke-interface {p0, v2}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;->getHeaderField(I)Ljava/lang/String;

    move-result-object v4

    .line 172
    .local v4, "value":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 173
    if-nez p1, :cond_0

    .line 174
    invoke-static {}, Lcom/vlingo/sdk/internal/http/cookies/CookieJarFactory;->newInstance()Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    move-result-object p1

    .line 176
    :cond_0
    invoke-static {v4, v0, p1}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->parseSetCookieString(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    .end local v4    # "value":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 181
    :catch_0
    move-exception v1

    .line 182
    .local v1, "ex":Ljava/lang/Exception;
    sget-object v5, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "COK1 Exception while extracting cookies: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    .end local v1    # "ex":Ljava/lang/Exception;
    :cond_2
    return-object p1
.end method

.method public static extractCookies(Lcom/vlingo/sdk/internal/net/HttpConnection;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;
    .locals 1
    .param p0, "conn"    # Lcom/vlingo/sdk/internal/net/HttpConnection;

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->extractCookies(Lcom/vlingo/sdk/internal/net/HttpConnection;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    move-result-object v0

    return-object v0
.end method

.method public static extractCookies(Lcom/vlingo/sdk/internal/net/HttpConnection;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;
    .locals 1
    .param p0, "conn"    # Lcom/vlingo/sdk/internal/net/HttpConnection;
    .param p1, "cookieJar"    # Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    .prologue
    .line 59
    new-instance v0, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpConnectionWrapper;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpConnectionWrapper;-><init>(Lcom/vlingo/sdk/internal/net/HttpConnection;)V

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->extractCookies(Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    move-result-object v0

    return-object v0
.end method

.method public static extractCookies(Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;
    .locals 1
    .param p0, "conn"    # Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->extractCookies(Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    move-result-object v0

    return-object v0
.end method

.method public static extractCookies(Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;
    .locals 1
    .param p0, "conn"    # Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;
    .param p1, "cookieJar"    # Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    .prologue
    .line 63
    new-instance v0, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpConnectionAdapterWrapper;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpConnectionAdapterWrapper;-><init>(Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;)V

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->extractCookies(Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    move-result-object v0

    return-object v0
.end method

.method public static extractCookies(Ljava/net/HttpURLConnection;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;
    .locals 1
    .param p0, "conn"    # Ljava/net/HttpURLConnection;

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->extractCookies(Ljava/net/HttpURLConnection;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    move-result-object v0

    return-object v0
.end method

.method public static extractCookies(Ljava/net/HttpURLConnection;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;
    .locals 1
    .param p0, "conn"    # Ljava/net/HttpURLConnection;
    .param p1, "cookieJar"    # Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    .prologue
    .line 67
    new-instance v0, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpURLConnectionWrapper;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$HttpURLConnectionWrapper;-><init>(Ljava/net/HttpURLConnection;)V

    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->extractCookies(Lcom/vlingo/sdk/internal/http/cookies/CookieHandler$ConnectionWrapper;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    move-result-object v0

    return-object v0
.end method

.method public static parseSetCookieString(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)V
    .locals 20
    .param p0, "cookieString"    # Ljava/lang/String;
    .param p1, "domain"    # Ljava/lang/String;
    .param p2, "cookieJar"    # Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    .prologue
    .line 200
    const/16 v16, 0x3b

    :try_start_0
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v15

    .line 201
    .local v15, "vals":[Ljava/lang/String;
    const/16 v16, 0x0

    aget-object v4, v15, v16

    .line 202
    .local v4, "cookie":Ljava/lang/String;
    const/16 v16, 0x3d

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    .line 203
    .local v6, "eqIndex":I
    const/16 v16, -0x1

    move/from16 v0, v16

    if-eq v6, v0, :cond_5

    .line 204
    const/4 v2, 0x0

    .line 205
    .local v2, "c":Lcom/vlingo/sdk/internal/http/cookies/Cookie;
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v4, v0, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 206
    .local v3, "cname":Ljava/lang/String;
    const-string/jumbo v5, ""

    .line 207
    .local v5, "cvalue":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v16

    add-int/lit8 v17, v6, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-le v0, v1, :cond_0

    .line 208
    add-int/lit8 v16, v6, 0x1

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 209
    invoke-static {v3, v5}, Lcom/vlingo/sdk/internal/http/cookies/CookieFactory;->newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/internal/http/cookies/Cookie;

    move-result-object v2

    .line 210
    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->setDomain(Ljava/lang/String;)V

    .line 211
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Lcom/vlingo/sdk/internal/http/cookies/CookieJar;->addCookie(Lcom/vlingo/sdk/internal/http/cookies/Cookie;)V

    .line 217
    :cond_0
    if-eqz v2, :cond_5

    .line 218
    const/4 v10, 0x1

    .local v10, "j":I
    :goto_0
    array-length v0, v15

    move/from16 v16, v0

    move/from16 v0, v16

    if-ge v10, v0, :cond_5

    .line 219
    aget-object v13, v15, v10

    .line 220
    .local v13, "pv":Ljava/lang/String;
    const/4 v12, 0x0

    .line 221
    .local v12, "pname":Ljava/lang/String;
    const-string/jumbo v14, ""

    .line 222
    .local v14, "pvalue":Ljava/lang/String;
    const-string/jumbo v16, "="

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v11

    .line 223
    .local v11, "peqIndex":I
    if-lez v11, :cond_3

    .line 224
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v13, v0, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    .line 225
    add-int/lit8 v16, v11, 0x1

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v14

    .line 227
    const-string/jumbo v16, "\""

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_1

    const-string/jumbo v16, "\""

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_1

    .line 228
    const/16 v16, 0x1

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v17

    add-int/lit8 v17, v17, -0x1

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v14, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 236
    :cond_1
    :goto_1
    const-string/jumbo v16, "Domain"

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_4

    .line 237
    invoke-interface {v2, v14}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->setDomain(Ljava/lang/String;)V

    .line 218
    :cond_2
    :goto_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 233
    :cond_3
    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    goto :goto_1

    .line 239
    :cond_4
    const-string/jumbo v16, "Path"

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_6

    .line 240
    invoke-interface {v2, v14}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->setPath(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 277
    .end local v2    # "c":Lcom/vlingo/sdk/internal/http/cookies/Cookie;
    .end local v3    # "cname":Ljava/lang/String;
    .end local v4    # "cookie":Ljava/lang/String;
    .end local v5    # "cvalue":Ljava/lang/String;
    .end local v6    # "eqIndex":I
    .end local v10    # "j":I
    .end local v11    # "peqIndex":I
    .end local v12    # "pname":Ljava/lang/String;
    .end local v13    # "pv":Ljava/lang/String;
    .end local v14    # "pvalue":Ljava/lang/String;
    .end local v15    # "vals":[Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 278
    .local v7, "ex":Ljava/lang/Exception;
    sget-object v16, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->TAG:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "COK2 Error parsing cookie: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-static {v7}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    sget-object v16, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->TAG:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Cookie string: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    sget-object v16, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->TAG:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Domain: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    .end local v7    # "ex":Ljava/lang/Exception;
    :cond_5
    return-void

    .line 242
    .restart local v2    # "c":Lcom/vlingo/sdk/internal/http/cookies/Cookie;
    .restart local v3    # "cname":Ljava/lang/String;
    .restart local v4    # "cookie":Ljava/lang/String;
    .restart local v5    # "cvalue":Ljava/lang/String;
    .restart local v6    # "eqIndex":I
    .restart local v10    # "j":I
    .restart local v11    # "peqIndex":I
    .restart local v12    # "pname":Ljava/lang/String;
    .restart local v13    # "pv":Ljava/lang/String;
    .restart local v14    # "pvalue":Ljava/lang/String;
    .restart local v15    # "vals":[Ljava/lang/String;
    :cond_6
    :try_start_1
    const-string/jumbo v16, "Expires"

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v16

    if-eqz v16, :cond_8

    .line 243
    const-wide/16 v8, 0x0

    .line 245
    .local v8, "expires":J
    :try_start_2
    invoke-static {v14}, Lcom/vlingo/sdk/internal/http/date/HttpDateParser;->parse(Ljava/lang/String;)J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-wide v8

    .line 249
    :goto_3
    const-wide/16 v16, 0x0

    cmp-long v16, v8, v16

    if-lez v16, :cond_2

    .line 250
    :try_start_3
    invoke-interface {v2}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->getExpires()J

    move-result-wide v16

    const-wide/16 v18, 0x0

    cmp-long v16, v16, v18

    if-eqz v16, :cond_7

    invoke-interface {v2}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->getExpires()J

    move-result-wide v16

    cmp-long v16, v8, v16

    if-gez v16, :cond_2

    .line 251
    :cond_7
    invoke-interface {v2, v8, v9}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->setExpires(J)V

    goto/16 :goto_2

    .line 255
    .end local v8    # "expires":J
    :cond_8
    const-string/jumbo v16, "Max-Age"

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-result v16

    if-eqz v16, :cond_a

    .line 256
    const-wide/16 v8, 0x0

    .line 258
    .restart local v8    # "expires":J
    :try_start_4
    invoke-static {v14}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move-result-wide v16

    const-wide/16 v18, 0x3e8

    mul-long v8, v16, v18

    .line 262
    :goto_4
    const-wide/16 v16, 0x0

    cmp-long v16, v8, v16

    if-lez v16, :cond_2

    .line 263
    :try_start_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    add-long v8, v8, v16

    .line 264
    invoke-interface {v2}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->getExpires()J

    move-result-wide v16

    const-wide/16 v18, 0x0

    cmp-long v16, v16, v18

    if-eqz v16, :cond_9

    invoke-interface {v2}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->getExpires()J

    move-result-wide v16

    cmp-long v16, v8, v16

    if-gez v16, :cond_2

    .line 265
    :cond_9
    invoke-interface {v2, v8, v9}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->setExpires(J)V

    goto/16 :goto_2

    .line 268
    .end local v8    # "expires":J
    :cond_a
    const-string/jumbo v16, "Secure"

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_2

    .line 269
    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-interface {v2, v0}, Lcom/vlingo/sdk/internal/http/cookies/Cookie;->setSecure(Z)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_2

    .line 247
    .restart local v8    # "expires":J
    :catch_1
    move-exception v16

    goto :goto_3

    .line 260
    :catch_2
    move-exception v16

    goto :goto_4
.end method
