.class public interface abstract Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
.super Ljava/lang/Object;
.source "INetworkRecognizer.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/recognizer/IRecognizer;


# virtual methods
.method public abstract getGuttId()Ljava/lang/String;
.end method

.method public abstract getTimingRepository()Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;
.end method

.method public abstract isInitialized()Z
.end method

.method public abstract processAudio([B)V
.end method

.method public abstract sendAcceptedText(Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;)V
.end method
