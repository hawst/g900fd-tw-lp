.class Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;
.super Ljava/lang/Object;
.source "ASRRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AudioSegment"
.end annotation


# instance fields
.field audio:[B

.field endSegment:Z

.field length:I

.field offset:I


# direct methods
.method constructor <init>([BII)V
    .locals 0
    .param p1, "audio"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 575
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 576
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;->audio:[B

    .line 577
    iput p2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;->offset:I

    .line 578
    iput p3, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;->length:I

    .line 579
    return-void
.end method


# virtual methods
.method public isEndSegment()Z
    .locals 1

    .prologue
    .line 599
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;->endSegment:Z

    return v0
.end method

.method public setEndSegment()V
    .locals 1

    .prologue
    .line 595
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;->endSegment:Z

    .line 596
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 582
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;->offset:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;->offset:I

    iget v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;->length:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ") ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioSegment;->length:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " bytes]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
