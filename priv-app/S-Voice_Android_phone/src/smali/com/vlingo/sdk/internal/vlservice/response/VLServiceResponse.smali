.class public Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;
.super Ljava/lang/Object;
.source "VLServiceResponse.java"


# instance fields
.field protected actionList:Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

.field private dialogGUID:Ljava/lang/String;

.field protected dialogState:[B

.field private dialogTurn:I

.field protected isError:Z

.field protected messages:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;",
            ">;"
        }
    .end annotation
.end field

.field private rawServerResponse:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->isError:Z

    .line 18
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    .line 21
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->dialogTurn:I

    return-void
.end method

.method public static createFromXml(Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;
    .locals 2
    .param p0, "xml"    # Ljava/lang/String;

    .prologue
    .line 119
    new-instance v1, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    invoke-direct {v1}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;-><init>()V

    .line 120
    .local v1, "responseParser":Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;
    invoke-virtual {v1, p0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->parseResponseXml(Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;

    move-result-object v0

    .line 121
    .local v0, "res":Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;
    return-object v0
.end method


# virtual methods
.method public addAction(Lcom/vlingo/sdk/internal/vlservice/response/Action;)V
    .locals 1
    .param p1, "action"    # Lcom/vlingo/sdk/internal/vlservice/response/Action;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->actionList:Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    if-nez v0, :cond_0

    .line 77
    new-instance v0, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->actionList:Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->actionList:Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    invoke-virtual {v0, p1}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->addElement(Lcom/vlingo/sdk/internal/vlservice/response/Action;)V

    .line 80
    return-void
.end method

.method public addMessage(Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;)V
    .locals 1
    .param p1, "message"    # Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 69
    return-void
.end method

.method public getActionList()Lcom/vlingo/sdk/internal/vlservice/response/ActionList;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->actionList:Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    return-object v0
.end method

.method public getDialogGuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->dialogGUID:Ljava/lang/String;

    return-object v0
.end method

.method public getDialogState()[B
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->dialogState:[B

    return-object v0
.end method

.method public getDialogTurn()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->dialogTurn:I

    return v0
.end method

.method public getFirstMessage()Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    .line 64
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMessages()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    return-object v0
.end method

.method public getRawServerResponse()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->rawServerResponse:Ljava/lang/String;

    return-object v0
.end method

.method public hasActions()Z
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->actionList:Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->actionList:Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/vlservice/response/ActionList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDialogState()Z
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->dialogState:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->dialogState:[B

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessageOfType(I)Z
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 33
    iget-object v2, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 34
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 35
    iget-object v2, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    .line 36
    .local v1, "message":Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;
    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;->getType()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 37
    const/4 v2, 0x1

    .line 41
    .end local v0    # "i":I
    .end local v1    # "message":Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;
    :goto_1
    return v2

    .line 34
    .restart local v0    # "i":I
    .restart local v1    # "message":Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41
    .end local v0    # "i":I
    .end local v1    # "message":Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public hasMessages()Z
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWarnings()Z
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->hasMessageOfType(I)Z

    move-result v0

    return v0
.end method

.method public isError()Z
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->hasMessageOfType(I)Z

    move-result v0

    return v0
.end method

.method public setActionList(Lcom/vlingo/sdk/internal/vlservice/response/ActionList;)V
    .locals 0
    .param p1, "actionList"    # Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->actionList:Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    .line 73
    return-void
.end method

.method public setDialogGuid(Ljava/lang/String;)V
    .locals 0
    .param p1, "guid"    # Ljava/lang/String;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->dialogGUID:Ljava/lang/String;

    .line 88
    return-void
.end method

.method public setDialogState([B)V
    .locals 0
    .param p1, "state"    # [B

    .prologue
    .line 103
    iput-object p1, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->dialogState:[B

    .line 104
    return-void
.end method

.method public setDialogTurn(I)V
    .locals 0
    .param p1, "turn"    # I

    .prologue
    .line 95
    iput p1, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->dialogTurn:I

    .line 96
    return-void
.end method

.method public setRawServerResponse(Ljava/lang/String;)V
    .locals 0
    .param p1, "rawServerResponse"    # Ljava/lang/String;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->rawServerResponse:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 125
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 126
    .local v0, "buff":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->getFirstMessage()Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 127
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 128
    iget-object v3, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->messages:Ljava/util/Vector;

    invoke-virtual {v3, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;

    .line 129
    .local v2, "msg":Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;
    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 130
    const-string/jumbo v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 127
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 133
    .end local v1    # "i":I
    .end local v2    # "msg":Lcom/vlingo/sdk/internal/vlservice/response/ServerMessage;
    :cond_0
    const-string/jumbo v3, "<no message>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 135
    :cond_1
    const-string/jumbo v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 136
    iget-object v3, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->actionList:Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    if-eqz v3, :cond_2

    .line 137
    iget-object v3, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->actionList:Lcom/vlingo/sdk/internal/vlservice/response/ActionList;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 141
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 139
    :cond_2
    const-string/jumbo v3, "<no actions>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1
.end method
