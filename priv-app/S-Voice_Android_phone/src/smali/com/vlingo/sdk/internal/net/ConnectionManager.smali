.class public Lcom/vlingo/sdk/internal/net/ConnectionManager;
.super Lcom/vlingo/sdk/internal/net/ConnectionProvider;
.source "ConnectionManager.java"


# static fields
.field public static final CONNECTION_TYPE_BIS:I = 0x1

.field public static final CONNECTION_TYPE_DIRECT:I = 0x0

.field public static final CONNECTION_TYPE_MDS:I = 0x2

.field public static final DEFAULT_TIMEOUT_FAST_NETWORK:I = 0x4e20

.field public static final DEFAULT_TIMEOUT_SLOW_NETWORK:I = 0x9c40

.field private static final RETRY_THRESHOLD_TIME_DEFAULT:I = 0x61a8

.field private static instance:Lcom/vlingo/sdk/internal/net/ConnectionManager;

.field private static final possibleParameters:[Ljava/lang/String;

.field private static sTimedOut:Z


# instance fields
.field private cdmaSignal:I

.field private evdoSignal:I

.field private gsmSignal:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 53
    sput-boolean v2, Lcom/vlingo/sdk/internal/net/ConnectionManager;->sTimedOut:Z

    .line 60
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/net/ConnectionManager;->instance:Lcom/vlingo/sdk/internal/net/ConnectionManager;

    .line 199
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "deviceside="

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string/jumbo v2, "apn="

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "interface="

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "wapgatewayapn="

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "wapsourceport="

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "wapsourceip="

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "wapgatewayport="

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "wapgatewayip="

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "wapenablewtls="

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "tunnelauthusername="

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "tunnelauthpassword="

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "endtoendrequired"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/sdk/internal/net/ConnectionManager;->possibleParameters:[Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 80
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/net/ConnectionProvider;-><init>()V

    .line 55
    iput v0, p0, Lcom/vlingo/sdk/internal/net/ConnectionManager;->cdmaSignal:I

    .line 56
    iput v0, p0, Lcom/vlingo/sdk/internal/net/ConnectionManager;->evdoSignal:I

    .line 57
    iput v0, p0, Lcom/vlingo/sdk/internal/net/ConnectionManager;->gsmSignal:I

    .line 81
    return-void
.end method

.method public static debugFixLocalIPs(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 196
    return-object p0
.end method

.method public static destroy()V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/net/ConnectionManager;->instance:Lcom/vlingo/sdk/internal/net/ConnectionManager;

    .line 78
    return-void
.end method

.method public static getConnectionTypeName(I)Ljava/lang/String;
    .locals 1
    .param p0, "conType"    # I

    .prologue
    .line 152
    packed-switch p0, :pswitch_data_0

    .line 160
    const-string/jumbo v0, "Unknown"

    :goto_0
    return-object v0

    .line 154
    :pswitch_0
    const-string/jumbo v0, "DirectTCP"

    goto :goto_0

    .line 156
    :pswitch_1
    const-string/jumbo v0, "BIS-B"

    goto :goto_0

    .line 158
    :pswitch_2
    const-string/jumbo v0, "MDS"

    goto :goto_0

    .line 152
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static declared-synchronized getInstance()Lcom/vlingo/sdk/internal/net/ConnectionManager;
    .locals 2

    .prologue
    .line 69
    const-class v1, Lcom/vlingo/sdk/internal/net/ConnectionManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/internal/net/ConnectionManager;->instance:Lcom/vlingo/sdk/internal/net/ConnectionManager;

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Lcom/vlingo/sdk/internal/net/ConnectionManager;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;-><init>()V

    sput-object v0, Lcom/vlingo/sdk/internal/net/ConnectionManager;->instance:Lcom/vlingo/sdk/internal/net/ConnectionManager;

    .line 73
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/internal/net/ConnectionManager;->instance:Lcom/vlingo/sdk/internal/net/ConnectionManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getNetworkType(Landroid/telephony/TelephonyManager;)Ljava/lang/String;
    .locals 2
    .param p1, "phonyManager"    # Landroid/telephony/TelephonyManager;

    .prologue
    .line 386
    invoke-virtual {p1}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v0

    .line 388
    .local v0, "type":I
    packed-switch v0, :pswitch_data_0

    .line 416
    const-string/jumbo v1, "unknown"

    :goto_0
    return-object v1

    .line 390
    :pswitch_0
    const-string/jumbo v1, "NETWORK_TYPE_UNKNOWN"

    goto :goto_0

    .line 392
    :pswitch_1
    const-string/jumbo v1, "NETWORK_TYPE_GPRS"

    goto :goto_0

    .line 394
    :pswitch_2
    const-string/jumbo v1, "NETWORK_TYPE_EDGE"

    goto :goto_0

    .line 396
    :pswitch_3
    const-string/jumbo v1, "NETWORK_TYPE_UMTS"

    goto :goto_0

    .line 398
    :pswitch_4
    const-string/jumbo v1, "NETWORK_TYPE_HSDPA"

    goto :goto_0

    .line 400
    :pswitch_5
    const-string/jumbo v1, "NETWORK_TYPE_HSUPA"

    goto :goto_0

    .line 402
    :pswitch_6
    const-string/jumbo v1, "NETWORK_TYPE_HSPA"

    goto :goto_0

    .line 404
    :pswitch_7
    const-string/jumbo v1, "NETWORK_TYPE_CDMA"

    goto :goto_0

    .line 406
    :pswitch_8
    const-string/jumbo v1, "NETWORK_TYPE_EVDO_0"

    goto :goto_0

    .line 408
    :pswitch_9
    const-string/jumbo v1, "NETWORK_TYPE_EVDO_0"

    goto :goto_0

    .line 410
    :pswitch_a
    const-string/jumbo v1, "NETWORK_TYPE_EVDO_B"

    goto :goto_0

    .line 412
    :pswitch_b
    const-string/jumbo v1, "NETWORK_TYPE_1xRTT"

    goto :goto_0

    .line 414
    :pswitch_c
    const-string/jumbo v1, "NETWORK_TYPE_IDEN"

    goto :goto_0

    .line 388
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_b
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_c
        :pswitch_a
    .end packed-switch
.end method

.method public static getOptimalConnectTimeout()I
    .locals 1

    .prologue
    .line 249
    invoke-static {}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->isSlowNetwork()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x9c40

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x4e20

    goto :goto_0
.end method

.method public static getOptimalNetworkTimeout()I
    .locals 1

    .prologue
    .line 253
    invoke-static {}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->isSlowNetwork()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x9c40

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x4e20

    goto :goto_0
.end method

.method public static isSlowNetwork()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 231
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 233
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v4, "phone"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 235
    .local v2, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v1

    .line 237
    .local v1, "networkTypeId":I
    invoke-static {}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->isTimedOut()Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x7

    if-eq v1, v4, :cond_0

    const/4 v4, 0x2

    if-eq v1, v4, :cond_0

    if-eq v1, v3, :cond_0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static isTimedOut()Z
    .locals 1

    .prologue
    .line 245
    sget-boolean v0, Lcom/vlingo/sdk/internal/net/ConnectionManager;->sTimedOut:Z

    return v0
.end method

.method public static declared-synchronized setTimedOut(Z)V
    .locals 2
    .param p0, "timedOut"    # Z

    .prologue
    .line 257
    const-class v0, Lcom/vlingo/sdk/internal/net/ConnectionManager;

    monitor-enter v0

    :try_start_0
    sput-boolean p0, Lcom/vlingo/sdk/internal/net/ConnectionManager;->sTimedOut:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 258
    monitor-exit v0

    return-void

    .line 257
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method protected areConnectionParametersInUrl(Ljava/lang/String;)Z
    .locals 6
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x3b

    const/4 v5, -0x1

    .line 215
    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-eq v3, v5, :cond_2

    .line 216
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 217
    invoke-static {p1, v4}, Lcom/vlingo/sdk/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v0

    .line 219
    .local v0, "components":[Ljava/lang/String;
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_2

    .line 220
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    sget-object v3, Lcom/vlingo/sdk/internal/net/ConnectionManager;->possibleParameters:[Ljava/lang/String;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 221
    aget-object v3, v0, v1

    sget-object v4, Lcom/vlingo/sdk/internal/net/ConnectionManager;->possibleParameters:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-eq v3, v5, :cond_0

    .line 222
    const/4 v3, 0x1

    .line 227
    .end local v0    # "components":[Ljava/lang/String;
    .end local v1    # "i":I
    .end local v2    # "j":I
    :goto_2
    return v3

    .line 220
    .restart local v0    # "components":[Ljava/lang/String;
    .restart local v1    # "i":I
    .restart local v2    # "j":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 219
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 227
    .end local v0    # "components":[Ljava/lang/String;
    .end local v1    # "i":I
    .end local v2    # "j":I
    :cond_2
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public buildConnectionUrl(ILjava/lang/String;ZZ)Lcom/vlingo/sdk/internal/net/ConnectionResult;
    .locals 2
    .param p1, "connectionType"    # I
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "useWiFi"    # Z
    .param p4, "useApn"    # Z

    .prologue
    .line 99
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "Not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public generateTimeoutErrorString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 365
    const-string/jumbo v0, ""

    .line 366
    .local v0, "errorMsg":Ljava/lang/String;
    const/4 v1, 0x0

    .line 369
    .local v1, "wifi":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 370
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v1

    .line 372
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "; cdmasiglev="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getCdmaSignal()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 373
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "; evdosiglev="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getEvdoSignal()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 374
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "; gsmsiglev="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getGsmSignal()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 375
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "; networkType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getNetworkTypeName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 376
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "; connType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 377
    const-string/jumbo v2, "wifi"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 378
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "; wifilinkspd="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getWifiLinkSpeed()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 381
    :cond_1
    return-object v0
.end method

.method public getCarrierName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 264
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 265
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v2, "phone"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 266
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getCdmaSignal()I
    .locals 1

    .prologue
    .line 341
    iget v0, p0, Lcom/vlingo/sdk/internal/net/ConnectionManager;->cdmaSignal:I

    return v0
.end method

.method public getConnection(Ljava/lang/String;IZ)Lcom/vlingo/sdk/internal/net/Connection;
    .locals 7
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "mode"    # I
    .param p3, "timeouts"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    const/16 v4, 0x61a8

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getConnectionWithDetails(Ljava/lang/String;IZIZ)Lcom/vlingo/sdk/internal/net/ConnectionResult;

    move-result-object v6

    .line 85
    .local v6, "result":Lcom/vlingo/sdk/internal/net/ConnectionResult;
    iget-object v0, v6, Lcom/vlingo/sdk/internal/net/ConnectionResult;->connection:Lcom/vlingo/sdk/internal/net/Connection;

    return-object v0
.end method

.method public getConnectionTypeSetting()I
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x0

    return v0
.end method

.method public getConnectionWithDetails(Ljava/lang/String;IZ)Lcom/vlingo/sdk/internal/net/ConnectionResult;
    .locals 6
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "mode"    # I
    .param p3, "timeouts"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    const/16 v4, 0x61a8

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getConnectionWithDetails(Ljava/lang/String;IZIZ)Lcom/vlingo/sdk/internal/net/ConnectionResult;

    move-result-object v0

    return-object v0
.end method

.method public getConnectionWithDetails(Ljava/lang/String;IZIZ)Lcom/vlingo/sdk/internal/net/ConnectionResult;
    .locals 3
    .param p1, "urlStr"    # Ljava/lang/String;
    .param p2, "mode"    # I
    .param p3, "timeouts"    # Z
    .param p4, "retryThresholdTime"    # I
    .param p5, "ignoreDirectTCP"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 109
    invoke-static {p1}, Lcom/vlingo/sdk/internal/http/custom/VConnectionFactory;->createConnection(Ljava/lang/String;)Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;

    move-result-object v0

    .line 111
    .local v0, "c":Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;
    if-nez p2, :cond_0

    .line 112
    invoke-virtual {v0, v2}, Lcom/vlingo/sdk/internal/http/custom/AndroidVStreamConnection;->setDoOutput(Z)V

    .line 115
    :cond_0
    new-instance v1, Lcom/vlingo/sdk/internal/net/ConnectionResult;

    invoke-direct {v1}, Lcom/vlingo/sdk/internal/net/ConnectionResult;-><init>()V

    .line 118
    .local v1, "result":Lcom/vlingo/sdk/internal/net/ConnectionResult;
    iput-object v0, v1, Lcom/vlingo/sdk/internal/net/ConnectionResult;->connection:Lcom/vlingo/sdk/internal/net/Connection;

    .line 119
    iput v2, v1, Lcom/vlingo/sdk/internal/net/ConnectionResult;->connectionType:I

    .line 121
    return-object v1
.end method

.method public getConnectionWithDetails(Ljava/lang/String;IZZ)Lcom/vlingo/sdk/internal/net/ConnectionResult;
    .locals 6
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "mode"    # I
    .param p3, "timeouts"    # Z
    .param p4, "ignoreDirectTCP"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    const/16 v4, 0x61a8

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getConnectionWithDetails(Ljava/lang/String;IZIZ)Lcom/vlingo/sdk/internal/net/ConnectionResult;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentConnectionType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getConnectionTypeSetting()I

    move-result v0

    invoke-static {v0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getConnectionTypeName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEvdoSignal()I
    .locals 1

    .prologue
    .line 349
    iget v0, p0, Lcom/vlingo/sdk/internal/net/ConnectionManager;->evdoSignal:I

    return v0
.end method

.method public getGetResponseFromServer(Ljava/lang/String;)I
    .locals 6
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 308
    const/4 v1, 0x0

    .line 311
    .local v1, "connection":Ljava/net/HttpURLConnection;
    :try_start_0
    new-instance v5, Ljava/net/URL;

    invoke-direct {v5, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v1, v0

    .line 312
    const/16 v5, 0x61a8

    invoke-virtual {v1, v5}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 313
    const/16 v5, 0x61a8

    invoke-virtual {v1, v5}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 314
    const-string/jumbo v5, "HEAD"

    invoke-virtual {v1, v5}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 315
    const-string/jumbo v5, "GET"

    invoke-virtual {v1, v5}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 316
    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 317
    .local v3, "in":Ljava/io/InputStream;
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 318
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_0
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    .line 332
    .local v4, "response":I
    if-eqz v1, :cond_0

    .line 333
    .end local v3    # "in":Ljava/io/InputStream;
    :goto_0
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 336
    :cond_0
    return v4

    .line 322
    .end local v4    # "response":I
    :catch_0
    move-exception v2

    .line 323
    .local v2, "e":Lorg/apache/http/conn/ConnectTimeoutException;
    const/4 v4, -0x1

    .line 332
    .restart local v4    # "response":I
    if-eqz v1, :cond_0

    goto :goto_0

    .line 324
    .end local v2    # "e":Lorg/apache/http/conn/ConnectTimeoutException;
    .end local v4    # "response":I
    :catch_1
    move-exception v2

    .line 325
    .local v2, "e":Ljava/net/SocketTimeoutException;
    const/4 v4, -0x2

    .line 332
    .restart local v4    # "response":I
    if-eqz v1, :cond_0

    goto :goto_0

    .line 326
    .end local v2    # "e":Ljava/net/SocketTimeoutException;
    .end local v4    # "response":I
    :catch_2
    move-exception v2

    .line 327
    .local v2, "e":Ljava/net/MalformedURLException;
    const/4 v4, -0x3

    .line 332
    .restart local v4    # "response":I
    if-eqz v1, :cond_0

    goto :goto_0

    .line 328
    .end local v2    # "e":Ljava/net/MalformedURLException;
    .end local v4    # "response":I
    :catch_3
    move-exception v2

    .line 329
    .local v2, "e":Ljava/io/IOException;
    const/4 v4, -0x4

    .line 332
    .restart local v4    # "response":I
    if-eqz v1, :cond_0

    goto :goto_0

    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "response":I
    :catchall_0
    move-exception v5

    if-eqz v1, :cond_1

    .line 333
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 332
    :cond_1
    throw v5
.end method

.method public getGsmSignal()I
    .locals 1

    .prologue
    .line 357
    iget v0, p0, Lcom/vlingo/sdk/internal/net/ConnectionManager;->gsmSignal:I

    return v0
.end method

.method public getNetworkInfo()Landroid/net/NetworkInfo;
    .locals 3

    .prologue
    .line 278
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 279
    .local v1, "context":Landroid/content/Context;
    const-string/jumbo v2, "connectivity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 280
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    return-object v2
.end method

.method public getNetworkTypeName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 271
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 272
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v2, "phone"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 273
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getNetworkType(Landroid/telephony/TelephonyManager;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getWifiLinkSpeed()I
    .locals 3

    .prologue
    .line 289
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 290
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v2, "wifi"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    .line 291
    .local v1, "wm":Landroid/net/wifi/WifiManager;
    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getLinkSpeed()I

    move-result v2

    return v2
.end method

.method public getWifiSignalStrength()I
    .locals 3

    .prologue
    .line 299
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 301
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v2, "wifi"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    .line 303
    .local v1, "wm":Landroid/net/wifi/WifiManager;
    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v2

    return v2
.end method

.method public isAirplaneModeEnabled()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 284
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 285
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "airplane_mode_on"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public isConnectionTestRequired()Z
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    return v0
.end method

.method public resetConnectionTest()V
    .locals 0

    .prologue
    .line 131
    return-void
.end method

.method public setCdmaSignal(I)V
    .locals 0
    .param p1, "cdmaSignal"    # I

    .prologue
    .line 345
    iput p1, p0, Lcom/vlingo/sdk/internal/net/ConnectionManager;->cdmaSignal:I

    .line 346
    return-void
.end method

.method public setEvdoSignal(I)V
    .locals 0
    .param p1, "evdoSignal"    # I

    .prologue
    .line 353
    iput p1, p0, Lcom/vlingo/sdk/internal/net/ConnectionManager;->evdoSignal:I

    .line 354
    return-void
.end method

.method public setGsmSignal(I)V
    .locals 0
    .param p1, "gsmSignal"    # I

    .prologue
    .line 361
    iput p1, p0, Lcom/vlingo/sdk/internal/net/ConnectionManager;->gsmSignal:I

    .line 362
    return-void
.end method
