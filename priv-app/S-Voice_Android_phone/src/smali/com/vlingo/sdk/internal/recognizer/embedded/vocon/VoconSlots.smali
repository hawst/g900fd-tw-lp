.class public final enum Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;
.super Ljava/lang/Enum;
.source "VoconSlots.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

.field public static final enum ALBUMS:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

.field public static final enum APPLICATION_NAMES:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

.field public static final enum ARTISTS:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

.field public static final enum CONTACTS:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

.field public static final enum PLAYLISTS:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

.field public static final enum SONGS:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;


# instance fields
.field private final context:Ljava/lang/String;

.field private final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 4
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    const-string/jumbo v1, "CONTACTS"

    const-string/jumbo v2, "contacts"

    const-string/jumbo v3, "ContactsContext.dat"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->CONTACTS:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    .line 5
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    const-string/jumbo v1, "SONGS"

    const-string/jumbo v2, "songs"

    const-string/jumbo v3, "SongsContext.dat"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->SONGS:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    .line 6
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    const-string/jumbo v1, "ARTISTS"

    const-string/jumbo v2, "artists"

    const-string/jumbo v3, "ArtistsContext.dat"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->ARTISTS:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    .line 7
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    const-string/jumbo v1, "ALBUMS"

    const-string/jumbo v2, "albums"

    const-string/jumbo v3, "AlbumsContext.dat"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->ALBUMS:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    .line 8
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    const-string/jumbo v1, "PLAYLISTS"

    const-string/jumbo v2, "playlists"

    const-string/jumbo v3, "PlaylistsContext.dat"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->PLAYLISTS:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    .line 9
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    const-string/jumbo v1, "APPLICATION_NAMES"

    const/4 v2, 0x5

    const-string/jumbo v3, "application_names"

    const-string/jumbo v4, "ApplicationsContext.dat"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->APPLICATION_NAMES:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    .line 3
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->CONTACTS:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->SONGS:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->ARTISTS:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->ALBUMS:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    aput-object v1, v0, v8

    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->PLAYLISTS:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->APPLICATION_NAMES:Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->$VALUES:[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .param p4, "context"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 15
    iput-object p3, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->name:Ljava/lang/String;

    .line 16
    iput-object p4, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->context:Ljava/lang/String;

    .line 17
    return-void
.end method

.method public static getSlotNames()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 29
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->values()[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    move-result-object v2

    .line 30
    .local v2, "slots":[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;
    array-length v3, v2

    new-array v1, v3, [Ljava/lang/String;

    .line 31
    .local v1, "slotNames":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "counter":I
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 32
    aget-object v3, v2, v0

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    .line 31
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 35
    :cond_0
    return-object v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->$VALUES:[Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    invoke-virtual {v0}, [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;

    return-object v0
.end method


# virtual methods
.method public getContext()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->context:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/vocon/VoconSlots;->name:Ljava/lang/String;

    return-object v0
.end method
