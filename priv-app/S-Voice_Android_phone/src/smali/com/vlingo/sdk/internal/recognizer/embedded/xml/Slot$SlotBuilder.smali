.class Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot$SlotBuilder;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
.source "Slot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SlotBuilder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot$1;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot$SlotBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public addParametersNames(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p1, "paramNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v0, "context"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    const-string/jumbo v0, "slot"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    return-void
.end method

.method protected buildResultObject()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;
    .locals 3

    .prologue
    .line 32
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;-><init>()V

    .line 33
    .local v0, "result":Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot$SlotBuilder;->parametersMap:Ljava/util/Map;

    const-string/jumbo v2, "slot"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;->name:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;->access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;Ljava/lang/String;)Ljava/lang/String;

    .line 34
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot$SlotBuilder;->parametersMap:Ljava/util/Map;

    const-string/jumbo v2, "context"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # setter for: Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;->context:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;->access$202(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;Ljava/lang/String;)Ljava/lang/String;

    .line 35
    return-object v0
.end method

.method protected bridge synthetic buildResultObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot$SlotBuilder;->buildResultObject()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Slot$SlotBuilder;->parametersMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 41
    return-void
.end method

.method public getTagName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    const-string/jumbo v0, "Slot"

    return-object v0
.end method
