.class Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser$XmlHandlerImpl;
.super Ljava/lang/Object;
.source "VLResponseParser.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/xml/XmlHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "XmlHandlerImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser$XmlHandlerImpl;->this$0:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public beginDocument()V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser$XmlHandlerImpl;->this$0:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    iget-object v0, v0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->currentParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser$XmlHandlerImpl;->this$0:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    iget-object v0, v0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->currentParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/xml/XmlHandler;->beginDocument()V

    .line 221
    :cond_0
    return-void
.end method

.method public beginElement(ILcom/vlingo/sdk/internal/xml/XmlAttributes;[CI)V
    .locals 2
    .param p1, "elementType"    # I
    .param p2, "attributes"    # Lcom/vlingo/sdk/internal/xml/XmlAttributes;
    .param p3, "cData"    # [C
    .param p4, "elementEndPosition"    # I

    .prologue
    .line 202
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser$XmlHandlerImpl;->this$0:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    iget-object v0, v0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->currentParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;

    if-nez v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser$XmlHandlerImpl;->this$0:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser$XmlHandlerImpl;->this$0:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    # invokes: Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->getParserForElement(I)Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;
    invoke-static {v1, p1}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->access$100(Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;I)Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->currentParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser$XmlHandlerImpl;->this$0:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    iget-object v0, v0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->currentParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;

    if-eqz v0, :cond_1

    .line 207
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser$XmlHandlerImpl;->this$0:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    iget-object v0, v0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->currentParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/vlingo/sdk/internal/xml/XmlHandler;->beginElement(ILcom/vlingo/sdk/internal/xml/XmlAttributes;[CI)V

    .line 209
    :cond_1
    return-void
.end method

.method public characters([C)V
    .locals 1
    .param p1, "cData"    # [C

    .prologue
    .line 229
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser$XmlHandlerImpl;->this$0:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    iget-object v0, v0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->currentParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser$XmlHandlerImpl;->this$0:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    iget-object v0, v0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->currentParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/internal/xml/XmlHandler;->characters([C)V

    .line 233
    :cond_0
    return-void
.end method

.method public endDocument()V
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser$XmlHandlerImpl;->this$0:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    iget-object v0, v0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->currentParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser$XmlHandlerImpl;->this$0:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    iget-object v0, v0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->currentParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;

    invoke-interface {v0}, Lcom/vlingo/sdk/internal/xml/XmlHandler;->endDocument()V

    .line 227
    :cond_0
    return-void
.end method

.method public endElement(II)V
    .locals 1
    .param p1, "elementType"    # I
    .param p2, "elementStartPosition"    # I

    .prologue
    .line 211
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser$XmlHandlerImpl;->this$0:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    iget-object v0, v0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->currentParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser$XmlHandlerImpl;->this$0:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    iget-object v0, v0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->currentParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;

    invoke-interface {v0, p1, p2}, Lcom/vlingo/sdk/internal/xml/XmlHandler;->endElement(II)V

    .line 215
    :cond_0
    return-void
.end method
