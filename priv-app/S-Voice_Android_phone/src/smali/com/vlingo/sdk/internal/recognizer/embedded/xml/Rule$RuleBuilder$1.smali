.class Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder$1;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;
.source "Rule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;->addChildrenBuilders(Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder$1;->this$0:Lcom/vlingo/sdk/internal/recognizer/embedded/xml/Rule$RuleBuilder;

    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method protected getChildBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/TtaAction;->getBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;

    move-result-object v0

    return-object v0
.end method

.method protected getChildTagName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    const-string/jumbo v0, "Action"

    return-object v0
.end method

.method public getTagName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    const-string/jumbo v0, "ActionList"

    return-object v0
.end method
