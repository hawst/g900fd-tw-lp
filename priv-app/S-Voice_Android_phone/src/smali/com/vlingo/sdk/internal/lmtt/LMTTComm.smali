.class public Lcom/vlingo/sdk/internal/lmtt/LMTTComm;
.super Ljava/lang/Object;
.source "LMTTComm.java"


# static fields
.field public static final LMTT_ERROR:I = 0x3

.field public static final LMTT_OK:I = 0x1

.field public static final LMTT_REQUIRES_FULL_RESYNC:I = 0x2

.field private static final SERVER_COUNT_CONTACT:Ljava/lang/String; = "contact"

.field private static final SERVER_COUNT_PLAYLIST:Ljava/lang/String; = "playlist"

.field private static final SERVER_COUNT_SONG:Ljava/lang/String; = "song"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/vlingo/sdk/internal/lmtt/LMTTComm;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/lmtt/LMTTComm;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createLMTTRequest(Ljava/util/HashMap;ZLjava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    .locals 4
    .param p1, "doWhole"    # Z
    .param p2, "language"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/vlingo/sdk/internal/http/HttpCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/sdk/internal/lmtt/LMTTItem;",
            ">;>;Z",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/http/HttpCallback;",
            ")",
            "Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "itemsToSend":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;Ljava/util/Collection<Lcom/vlingo/sdk/internal/lmtt/LMTTItem;>;>;"
    invoke-static {p0, p1}, Lcom/vlingo/sdk/internal/lmtt/LMTTComm;->generateXML(Ljava/util/HashMap;Z)Ljava/lang/String;

    move-result-object v2

    .line 52
    .local v2, "xml":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/sdk/internal/AndroidServerDetails;->getLMTTURL()Lcom/vlingo/sdk/internal/http/URL;

    move-result-object v1

    .line 53
    .local v1, "url":Lcom/vlingo/sdk/internal/http/URL;
    const-string/jumbo v3, "LMTT"

    invoke-static {v3, p3, v1, v2, p2}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->createVLRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Lcom/vlingo/sdk/internal/http/URL;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    move-result-object v0

    .line 55
    .local v0, "request":Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->setGzipPostData(Z)V

    .line 57
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->setMaxRetry(I)V

    .line 58
    return-object v0
.end method

.method private static generateXML(Ljava/util/HashMap;Z)Ljava/lang/String;
    .locals 5
    .param p1, "doWhole"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/sdk/internal/lmtt/LMTTItem;",
            ">;>;Z)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 63
    .local p0, "itemsToSend":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;Ljava/util/Collection<Lcom/vlingo/sdk/internal/lmtt/LMTTItem;>;>;"
    sget-object v4, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;->TYPE_CONTACT:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;

    invoke-virtual {p0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 64
    .local v0, "contactItems":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/sdk/internal/lmtt/LMTTItem;>;"
    sget-object v4, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;->TYPE_SONG:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;

    invoke-virtual {p0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Collection;

    .line 65
    .local v3, "songItems":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/sdk/internal/lmtt/LMTTItem;>;"
    sget-object v4, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;->TYPE_PLAYLIST:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;

    invoke-virtual {p0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    .line 67
    .local v1, "playlistItems":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/sdk/internal/lmtt/LMTTItem;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/util/HashMap;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x14

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 68
    .local v2, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v4, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    const-string/jumbo v4, "<LMTT>"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 73
    if-eqz p1, :cond_5

    .line 74
    const-string/jumbo v4, "<PIM t=\"w\">"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    :goto_0
    invoke-static {v0, v2}, Lcom/vlingo/sdk/internal/lmtt/LMTTComm;->getListItemsXML(Ljava/util/Collection;Ljava/lang/StringBuilder;)V

    .line 80
    const-string/jumbo v4, "</PIM>"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    const/4 v0, 0x0

    .line 83
    :cond_0
    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v4

    if-gtz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v4

    if-lez v4, :cond_4

    .line 84
    :cond_1
    if-eqz p1, :cond_6

    .line 85
    const-string/jumbo v4, "<MU t=\"w\">"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    :goto_1
    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 92
    const-string/jumbo v4, "<SUS>"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    invoke-static {v3, v2}, Lcom/vlingo/sdk/internal/lmtt/LMTTComm;->getListItemsXML(Ljava/util/Collection;Ljava/lang/StringBuilder;)V

    .line 94
    const-string/jumbo v4, "</SUS>"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    const/4 v3, 0x0

    .line 97
    :cond_2
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v4

    if-lez v4, :cond_3

    .line 98
    const-string/jumbo v4, "<PUS>"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    invoke-static {v1, v2}, Lcom/vlingo/sdk/internal/lmtt/LMTTComm;->getListItemsXML(Ljava/util/Collection;Ljava/lang/StringBuilder;)V

    .line 100
    const-string/jumbo v4, "</PUS>"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    const/4 v1, 0x0

    .line 104
    :cond_3
    const-string/jumbo v4, "</MU>"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    :cond_4
    const-string/jumbo v4, "</LMTT>"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 77
    :cond_5
    const-string/jumbo v4, "<PIM t=\"p\">"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 88
    :cond_6
    const-string/jumbo v4, "<MU t=\"p\">"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private static getListItemsXML(Ljava/util/Collection;Ljava/lang/StringBuilder;)V
    .locals 4
    .param p1, "sb"    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/vlingo/sdk/internal/lmtt/LMTTItem;",
            ">;",
            "Ljava/lang/StringBuilder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 114
    .local p0, "list":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/sdk/internal/lmtt/LMTTItem;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/internal/lmtt/LMTTItem;

    .line 115
    .local v1, "item":Lcom/vlingo/sdk/internal/lmtt/LMTTItem;
    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/lmtt/LMTTItem;->getXML()Ljava/lang/String;

    move-result-object v2

    .line 118
    .local v2, "xml":Ljava/lang/String;
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 120
    .end local v1    # "item":Lcom/vlingo/sdk/internal/lmtt/LMTTItem;
    .end local v2    # "xml":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private static mapServerType2ClientType(Ljava/lang/String;)Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;
    .locals 2
    .param p0, "serverLmttItemType"    # Ljava/lang/String;

    .prologue
    .line 171
    sget-object v0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;->TYPE_UNKNOWN:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;

    .line 172
    .local v0, "toReturn":Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;
    const-string/jumbo v1, "playlist"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 173
    sget-object v0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;->TYPE_PLAYLIST:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;

    .line 181
    :cond_0
    :goto_0
    return-object v0

    .line 175
    :cond_1
    const-string/jumbo v1, "song"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 176
    sget-object v0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;->TYPE_SONG:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;

    goto :goto_0

    .line 178
    :cond_2
    const-string/jumbo v1, "contact"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 179
    sget-object v0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;->TYPE_CONTACT:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;

    goto :goto_0
.end method

.method public static parseLMTTResponse(Lcom/vlingo/sdk/internal/http/HttpResponse;)Ljava/util/HashMap;
    .locals 16
    .param p0, "res"    # Lcom/vlingo/sdk/internal/http/HttpResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/internal/http/HttpResponse;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/sdk/internal/http/HttpResponse;->getDataAsString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 129
    .local v0, "checkResponse":Ljava/lang/String;
    :try_start_0
    const-string/jumbo v4, "count=\""

    .line 130
    .local v4, "countValueStart":Ljava/lang/String;
    const-string/jumbo v10, "type=\""

    .line 131
    .local v10, "typeValueStart":Ljava/lang/String;
    const-string/jumbo v12, "\""

    .line 133
    .local v12, "valueEnd":Ljava/lang/String;
    const-string/jumbo v13, "count=\""

    const-string/jumbo v14, "\""

    invoke-static {v0, v13, v14}, Lcom/vlingo/sdk/internal/util/StringUtils;->getSubstring(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 134
    .local v3, "countValue":Ljava/lang/String;
    const-string/jumbo v13, "type=\""

    const-string/jumbo v14, "\""

    invoke-static {v0, v13, v14}, Lcom/vlingo/sdk/internal/util/StringUtils;->getSubstring(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 136
    .local v9, "typeValue":Ljava/lang/String;
    if-eqz v3, :cond_1

    if-eqz v9, :cond_1

    .line 137
    const/16 v13, 0x2c

    invoke-static {v3, v13}, Lcom/vlingo/sdk/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v7

    .line 138
    .local v7, "serverCounts":[Ljava/lang/String;
    const/16 v13, 0x2c

    invoke-static {v9, v13}, Lcom/vlingo/sdk/internal/util/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v11

    .line 146
    .local v11, "types":[Ljava/lang/String;
    array-length v13, v7

    const/4 v14, 0x1

    if-lt v13, v14, :cond_1

    array-length v13, v11

    const/4 v14, 0x1

    if-lt v13, v14, :cond_1

    array-length v13, v7

    array-length v14, v11

    if-ne v13, v14, :cond_1

    .line 147
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 148
    .local v2, "countHash":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;Ljava/lang/Integer;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    array-length v13, v11

    if-ge v5, v13, :cond_2

    .line 149
    aget-object v13, v7, v5

    invoke-static {v13}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_0

    .line 150
    aget-object v13, v7, v5

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 151
    .local v6, "serverCount":I
    aget-object v13, v11, v5

    invoke-static {v13}, Lcom/vlingo/sdk/internal/lmtt/LMTTComm;->mapServerType2ClientType(Ljava/lang/String;)Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;

    move-result-object v1

    .line 152
    .local v1, "clientType":Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v2, v1, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    .end local v1    # "clientType":Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;
    .end local v6    # "serverCount":I
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 156
    :cond_0
    sget-object v13, Lcom/vlingo/sdk/internal/lmtt/LMTTComm;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "LMT2 Error parsing LMTT response counts: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 164
    .end local v2    # "countHash":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;Ljava/lang/Integer;>;"
    .end local v3    # "countValue":Ljava/lang/String;
    .end local v4    # "countValueStart":Ljava/lang/String;
    .end local v5    # "i":I
    .end local v7    # "serverCounts":[Ljava/lang/String;
    .end local v9    # "typeValue":Ljava/lang/String;
    .end local v10    # "typeValueStart":Ljava/lang/String;
    .end local v11    # "types":[Ljava/lang/String;
    .end local v12    # "valueEnd":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 165
    .local v8, "t":Ljava/lang/Throwable;
    sget-object v13, Lcom/vlingo/sdk/internal/lmtt/LMTTComm;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "LMT 2Error parsing LMTT response: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " Response: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    .end local v8    # "t":Ljava/lang/Throwable;
    :cond_1
    const/4 v2, 0x0

    :cond_2
    return-object v2
.end method
