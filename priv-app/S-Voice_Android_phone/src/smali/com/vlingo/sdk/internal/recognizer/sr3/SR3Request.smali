.class public Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;
.super Ljava/lang/Object;
.source "SR3Request.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/recognizer/network/SRRequest;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;
    }
.end annotation


# static fields
.field private static final ATR_ClientRequestID:Ljava/lang/String; = "ClientRequestID"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private volatile allAudioReceived:Z

.field private final audioQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;",
            ">;"
        }
    .end annotation
.end field

.field private ivAudioBytesWritten:I

.field private final ivCRC32:Ljava/util/zip/CRC32;

.field private final ivClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

.field private ivClientRequestID:I

.field private final ivContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

.field private final ivListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;",
            ">;"
        }
    .end annotation
.end field

.field private ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

.field private final ivSRManager:Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;

.field private final ivSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

.field private ivTimeGotResults:J

.field private ivTimeSendFinish:J

.field private ivTimeSendStart:J

.field private ivTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

.field private volatile requestCancelled:Z

.field private requestFinished:Z

.field private volatile requestThread:Ljava/lang/Thread;

.field private volatile responseReceived:Z

.field private final sendAudio:Z

.field private sentCancelRequest:Z

.field private volatile startedWritingAudio:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/vlingo/sdk/internal/recognizer/SRContext;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;Z)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .param p2, "clientMeta"    # Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    .param p3, "softwareMeta"    # Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;
    .param p4, "srManager"    # Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;
    .param p5, "timings"    # Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;
    .param p6, "sendAudio"    # Z

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 73
    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivCRC32:Ljava/util/zip/CRC32;

    .line 76
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivClientRequestID:I

    .line 99
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->audioQueue:Ljava/util/Queue;

    .line 107
    const-string/jumbo v0, "instantiation"

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->log(Ljava/lang/String;)V

    .line 108
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    .line 109
    iput-object p2, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    .line 110
    iput-object p3, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    .line 111
    iput-object p4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRManager:Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;

    .line 112
    iput-boolean p6, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->sendAudio:Z

    .line 115
    iput-object p5, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    .line 117
    return-void
.end method

.method private buildEventElement()Ljava/lang/String;
    .locals 5

    .prologue
    .line 418
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 420
    .local v3, "sb":Ljava/lang/StringBuffer;
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getEvents()Ljava/util/List;

    move-result-object v1

    .line 421
    .local v1, "eventList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;>;"
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 422
    const-string/jumbo v4, "<Events>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 423
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;

    .line 424
    .local v0, "event":Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    invoke-virtual {v0}, Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;->getXML()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 426
    .end local v0    # "event":Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;
    :cond_0
    const-string/jumbo v4, "</Events>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 428
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private buildMetaElement()Ljava/lang/String;
    .locals 10

    .prologue
    .line 432
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 433
    .local v5, "sb":Ljava/lang/StringBuffer;
    const-string/jumbo v7, "<Request "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 434
    const-string/jumbo v7, "FieldID"

    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getFieldID()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 435
    const-string/jumbo v7, "AppID"

    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getAppId()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 436
    const-string/jumbo v7, "FieldType"

    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getFieldType()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 437
    const-string/jumbo v7, "FieldContext"

    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getFieldContext()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 438
    iget-object v7, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    invoke-virtual {v7}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getCurrentText()Ljava/lang/String;

    move-result-object v2

    .line 439
    .local v2, "curText":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    .line 440
    const-string/jumbo v7, "CurrentText"

    invoke-static {v7, v2}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 441
    const-string/jumbo v7, "CursorPosition"

    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getCursorPosition()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 447
    :cond_0
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_0
    const/4 v7, 0x6

    if-gt v4, v7, :cond_2

    .line 448
    iget-object v7, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Custom"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getCustomParam(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 449
    .local v3, "customValue":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_1

    .line 450
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Custom"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v3}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 447
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 454
    .end local v3    # "customValue":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->isAudioStreamingEnabled()Z

    move-result v7

    if-eqz v7, :cond_3

    const-string/jumbo v6, "true"

    .line 455
    .local v6, "streaming":Ljava/lang/String;
    :goto_1
    const-string/jumbo v7, "StreamingAudio"

    invoke-static {v7, v6}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 457
    iget-object v7, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    invoke-virtual {v7}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getAutoPunctuation()Z

    move-result v7

    if-eqz v7, :cond_4

    const-string/jumbo v0, "true"

    .line 458
    .local v0, "autoPunctuate":Ljava/lang/String;
    :goto_2
    const-string/jumbo v7, "Punctuate"

    invoke-static {v7, v0}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 460
    iget-object v7, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    invoke-virtual {v7}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getCapitalization()Ljava/lang/String;

    move-result-object v1

    .line 461
    .local v1, "capitalize":Ljava/lang/String;
    const-string/jumbo v7, "Capitalize"

    invoke-static {v7, v1}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 463
    const-string/jumbo v7, "/>"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 464
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7

    .line 454
    .end local v0    # "autoPunctuate":Ljava/lang/String;
    .end local v1    # "capitalize":Ljava/lang/String;
    .end local v6    # "streaming":Ljava/lang/String;
    :cond_3
    const-string/jumbo v6, "false"

    goto :goto_1

    .line 457
    .restart local v6    # "streaming":Ljava/lang/String;
    :cond_4
    const-string/jumbo v0, "false"

    goto :goto_2
.end method

.method private declared-synchronized closeConnection()V
    .locals 1

    .prologue
    .line 481
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 485
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 490
    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 492
    :cond_0
    monitor-exit p0

    return-void

    .line 481
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 486
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private fail(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    .line 468
    sget-object v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, ">>>>>>>>>>>>>>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ": Exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    instance-of v0, p2, Ljava/io/InterruptedIOException;

    if-eqz v0, :cond_0

    .line 472
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->setTimedOut(Z)V

    .line 476
    :cond_0
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->notifyListeners(I)V

    .line 477
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->closeConnection()V

    .line 478
    return-void
.end method

.method private declared-synchronized finishRequest()V
    .locals 6

    .prologue
    .line 553
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v1, :cond_1

    .line 587
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 557
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;->getOut()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    move-result-object v0

    .line 559
    .local v0, "mpOut":Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    iget-boolean v1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->sendAudio:Z

    if-eqz v1, :cond_2

    .line 561
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeEndFieldValue()V

    .line 562
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeBoundary()V

    .line 565
    const-string/jumbo v1, "checksum"

    const-string/jumbo v2, "text/crc32"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivCRC32:Ljava/util/zip/CRC32;

    invoke-virtual {v4}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeField(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->flush()V

    .line 569
    :cond_2
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;->finishRequest()V

    .line 571
    const-string/jumbo v1, "REQD"

    invoke-direct {p0, v1}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->recordDetailedTiming(Ljava/lang/String;)V

    .line 573
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestFinished:Z

    .line 575
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivTimeSendFinish:J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 576
    .end local v0    # "mpOut":Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    :catch_0
    move-exception v1

    goto :goto_0

    .line 583
    :catchall_0
    move-exception v1

    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 553
    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private getTimingString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 791
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "HH:mm:ss.SSS"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 792
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 794
    .local v0, "buf":Ljava/lang/StringBuffer;
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Timing Data:\n\tSend start:\t\t\t\t"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    iget-wide v5, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivTimeSendStart:J

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 796
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\n\tSend finish:\t\t\t"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    iget-wide v5, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivTimeSendFinish:J

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 798
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\n\tTime got results:\t\t"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    iget-wide v5, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivTimeGotResults:J

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 800
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\n\tAudio bytes written:\t"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivAudioBytesWritten:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 804
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 801
    :catch_0
    move-exception v1

    .line 802
    .local v1, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->TAG:Ljava/lang/String;

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isErrorState(I)Z
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 769
    if-gez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 810
    return-void
.end method

.method private notifyListeners(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 759
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    .line 760
    .local v1, "ivListener":Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->isErrorState(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 761
    invoke-interface {v1, p1}, Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;->requestFailed(I)V

    goto :goto_0

    .line 763
    :cond_0
    invoke-interface {v1, p1}, Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;->stateChanged(I)V

    goto :goto_0

    .line 766
    .end local v1    # "ivListener":Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;
    :cond_1
    return-void
.end method

.method private notifyListeners(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    .locals 3
    .param p1, "result"    # Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .prologue
    .line 750
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    .line 751
    .local v1, "ivListener":Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;
    invoke-interface {v1, p1}, Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;->resultReceived(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    goto :goto_0

    .line 753
    .end local v1    # "ivListener":Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;
    :cond_0
    return-void
.end method

.method private receiveResponse()V
    .locals 10

    .prologue
    .line 593
    iget-boolean v8, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    if-nez v8, :cond_1

    .line 667
    :cond_0
    :goto_0
    return-void

    .line 606
    :cond_1
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 607
    :try_start_1
    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;->getConnection()Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    move-result-object v7

    .line 608
    .local v7, "vHttp":Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;
    invoke-virtual {v7}, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->getVStreamConnection()Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;

    move-result-object v6

    .line 609
    .local v6, "vCon":Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;
    invoke-virtual {v7}, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->getHttpInteraction()Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getResponse()Lcom/vlingo/sdk/internal/http/custom/HttpResponse;

    move-result-object v8

    invoke-interface {v6, v8}, Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;->startResponse(Lcom/vlingo/sdk/internal/http/custom/HttpResponse;)V

    .line 610
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 611
    :try_start_2
    iget-boolean v8, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z

    if-nez v8, :cond_0

    .line 615
    const/4 v2, 0x0

    .line 616
    .local v2, "in":Ljava/io/InputStream;
    monitor-enter p0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 617
    const/4 v8, 0x3

    :try_start_3
    invoke-direct {p0, v8}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->notifyListeners(I)V

    .line 618
    const-string/jumbo v8, "RESP"

    invoke-direct {p0, v8}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->recordDetailedTiming(Ljava/lang/String;)V

    .line 620
    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;->getIn()Ljava/io/InputStream;

    move-result-object v2

    .line 622
    const-string/jumbo v8, "RESH"

    invoke-direct {p0, v8}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->recordDetailedTiming(Ljava/lang/String;)V

    .line 623
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 624
    :try_start_4
    iget-boolean v8, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z

    if-nez v8, :cond_0

    .line 628
    const/4 v3, 0x0

    .line 629
    .local v3, "response":Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;
    monitor-enter p0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 631
    :try_start_5
    invoke-static {v2}, Lcom/vlingo/sdk/internal/http/HttpUtil;->readData(Ljava/io/InputStream;)[B

    move-result-object v1

    .line 633
    .local v1, "httpResponse":[B
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivTimeGotResults:J

    .line 635
    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    invoke-static {v8}, Lcom/vlingo/sdk/internal/http/cookies/CookieHandler;->extractCookies(Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;)Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    move-result-object v4

    .line 636
    .local v4, "responseCookies":Lcom/vlingo/sdk/internal/http/cookies/CookieJar;
    invoke-static {v4}, Lcom/vlingo/sdk/internal/vlservice/VLServiceUtil;->handleResponseCookies(Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)V

    .line 638
    new-instance v5, Ljava/lang/String;

    const-string/jumbo v8, "UTF-8"

    invoke-direct {v5, v1, v8}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 639
    .local v5, "responseXML":Ljava/lang/String;
    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRManager:Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->getResponseParser()Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;

    move-result-object v8

    invoke-virtual {v8, v5}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->parseResponseXml(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    move-result-object v3

    .line 644
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->responseReceived:Z

    .line 645
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 647
    :try_start_6
    iget-boolean v8, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z

    if-nez v8, :cond_0

    .line 649
    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRManager:Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->getGUttId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->setLastGuttID(Ljava/lang/String;)V

    .line 651
    invoke-direct {p0, v3}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->notifyListeners(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    .line 653
    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    if-eqz v8, :cond_0

    .line 654
    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;->finishResponse()V

    .line 657
    const/4 v8, 0x0

    invoke-static {v8}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->setTimedOut(Z)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 661
    .end local v1    # "httpResponse":[B
    .end local v2    # "in":Ljava/io/InputStream;
    .end local v3    # "response":Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;
    .end local v4    # "responseCookies":Lcom/vlingo/sdk/internal/http/cookies/CookieJar;
    .end local v5    # "responseXML":Ljava/lang/String;
    .end local v6    # "vCon":Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;
    .end local v7    # "vHttp":Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;
    :catch_0
    move-exception v0

    .line 662
    .local v0, "e":Ljava/lang/Exception;
    :try_start_7
    const-string/jumbo v8, "receiveResponse"

    invoke-direct {p0, v8, v0}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->fail(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 663
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v8

    throw v8

    .line 610
    :catchall_1
    move-exception v8

    :try_start_8
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    throw v8
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 623
    .restart local v2    # "in":Ljava/io/InputStream;
    .restart local v6    # "vCon":Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;
    .restart local v7    # "vHttp":Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;
    :catchall_2
    move-exception v8

    :try_start_a
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :try_start_b
    throw v8
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 645
    .restart local v3    # "response":Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;
    :catchall_3
    move-exception v8

    :try_start_c
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    :try_start_d
    throw v8
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_0
.end method

.method private recordDetailedTiming(Ljava/lang/String;)V
    .locals 1
    .param p1, "event"    # Ljava/lang/String;

    .prologue
    .line 773
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    if-eqz v0, :cond_0

    .line 774
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivTimings:Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;

    invoke-virtual {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/TimingRepository;->recordAndTimeStampEvent(Ljava/lang/String;)V

    .line 776
    :cond_0
    return-void
.end method

.method private sendAudioSegment(Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;)Z
    .locals 8
    .param p1, "chunk"    # Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 503
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    if-nez v4, :cond_0

    .line 542
    :goto_0
    return v2

    .line 507
    :cond_0
    :try_start_0
    iget v4, p1, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;->length:I

    if-nez v4, :cond_1

    move v2, v3

    .line 510
    goto :goto_0

    .line 513
    :cond_1
    iget-boolean v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->startedWritingAudio:Z

    if-nez v4, :cond_2

    .line 514
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->startedWritingAudio:Z

    .line 515
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivTimeSendStart:J

    .line 517
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;->getOut()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    move-result-object v4

    const-string/jumbo v5, "audio"

    const-string/jumbo v6, "audio"

    invoke-virtual {v4, v5, v6}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeFileFieldHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    const/4 v4, 0x2

    invoke-direct {p0, v4}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->notifyListeners(I)V

    .line 521
    :cond_2
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;->getOut()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    move-result-object v1

    .line 522
    .local v1, "mpOut":Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    iget-object v4, p1, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;->audio:[B

    iget v5, p1, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;->offset:I

    iget v6, p1, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;->length:I

    invoke-virtual {v1, v4, v5, v6}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->write([BII)V

    .line 523
    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->flush()V

    .line 526
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "AUD"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;->length:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->recordDetailedTiming(Ljava/lang/String;)V

    .line 528
    iget v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivAudioBytesWritten:I

    iget v5, p1, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;->length:I

    add-int/2addr v4, v5

    iput v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivAudioBytesWritten:I

    .line 536
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivCRC32:Ljava/util/zip/CRC32;

    iget-object v5, p1, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;->audio:[B

    iget v6, p1, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;->offset:I

    iget v7, p1, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;->length:I

    invoke-virtual {v4, v5, v6, v7}, Ljava/util/zip/CRC32;->update([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v2, v3

    .line 537
    goto :goto_0

    .line 538
    .end local v1    # "mpOut":Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    :catch_0
    move-exception v0

    .line 542
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0

    .line 543
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    throw v2
.end method

.method private sendCancelRequest()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 673
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    if-nez v5, :cond_1

    .line 744
    :cond_0
    :goto_0
    return-void

    .line 676
    :cond_1
    iget-boolean v5, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestFinished:Z

    if-eqz v5, :cond_2

    .line 677
    iget-boolean v5, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->sentCancelRequest:Z

    if-nez v5, :cond_0

    .line 678
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRManager:Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;

    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->getServerDetails()Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;

    move-result-object v4

    .line 679
    .local v4, "serverDetails":Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 680
    .local v0, "data":Ljava/lang/StringBuffer;
    const-string/jumbo v5, "<Cancel "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 681
    const-string/jumbo v5, "ClientRequestID"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivClientRequestID:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/vlingo/sdk/internal/http/HttpUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 682
    const-string/jumbo v5, "/>"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 683
    const-string/jumbo v5, "SRCancel"

    new-instance v6, Lcom/vlingo/sdk/internal/http/BaseHttpCallback;

    invoke-direct {v6}, Lcom/vlingo/sdk/internal/http/BaseHttpCallback;-><init>()V

    invoke-interface {v4}, Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;->getASRCancelURL()Lcom/vlingo/sdk/internal/http/URL;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v6, v7, v8}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->createVLRequest(Ljava/lang/String;Lcom/vlingo/sdk/internal/http/HttpCallback;Lcom/vlingo/sdk/internal/http/URL;Ljava/lang/String;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    move-result-object v3

    .line 684
    .local v3, "request":Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivClientMeta:Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    invoke-virtual {v3, v5}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->setClientMeta(Lcom/vlingo/sdk/internal/recognizer/ClientMeta;)V

    .line 685
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSoftwareMeta:Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    invoke-virtual {v3, v5}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->setSoftwareMeta(Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;)V

    .line 686
    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    invoke-virtual {v3, v5, v6, v9, v7}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->schedule(JZZ)V

    .line 687
    iput-boolean v9, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->sentCancelRequest:Z

    goto :goto_0

    .line 691
    .end local v0    # "data":Ljava/lang/StringBuffer;
    .end local v3    # "request":Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    .end local v4    # "serverDetails":Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;
    :cond_2
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 693
    :try_start_1
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    if-nez v5, :cond_3

    .line 694
    monitor-exit p0

    goto :goto_0

    .line 732
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v5
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 733
    :catch_0
    move-exception v1

    .line 738
    .local v1, "ex":Ljava/lang/Exception;
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->closeConnection()V

    goto :goto_0

    .line 696
    .end local v1    # "ex":Ljava/lang/Exception;
    :cond_3
    :try_start_3
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;->getOut()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    move-result-object v2

    .line 697
    .local v2, "mpOut":Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    iget-boolean v5, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->startedWritingAudio:Z

    if-eqz v5, :cond_4

    .line 699
    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeEndFieldValue()V

    .line 700
    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeBoundary()V

    .line 703
    const-string/jumbo v5, "checksum"

    const-string/jumbo v6, "text/crc32"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivCRC32:Ljava/util/zip/CRC32;

    invoke-virtual {v8}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v5, v6, v7}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeField(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    :cond_4
    const-string/jumbo v5, "cancel"

    const-string/jumbo v6, "text/xml"

    const-string/jumbo v7, "<cancel/>"

    invoke-virtual {v2, v5, v6, v7}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeField(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->flush()V

    .line 709
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;->finishRequest()V

    .line 711
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestFinished:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 723
    :try_start_4
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;->getIn()Ljava/io/InputStream;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/sdk/internal/http/HttpUtil;->readData(Ljava/io/InputStream;)[B
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 731
    :goto_1
    :try_start_5
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;->finishResponse()V

    .line 732
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 727
    :catch_1
    move-exception v5

    goto :goto_1
.end method


# virtual methods
.method public addListener(Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    .prologue
    .line 252
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    return-void
.end method

.method public declared-synchronized cancel(Z)V
    .locals 4
    .param p1, "timedOut"    # Z

    .prologue
    .line 212
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->responseReceived:Z

    if-nez v1, :cond_2

    .line 213
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z

    .line 217
    if-eqz p1, :cond_0

    .line 218
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->setTimedOut(Z)V

    .line 225
    :cond_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestThread:Ljava/lang/Thread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 230
    :try_start_1
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 238
    :cond_1
    :goto_0
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 240
    :cond_2
    monitor-exit p0

    return-void

    .line 232
    :catch_0
    move-exception v0

    .line 233
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_3
    sget-object v1, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "INT Execption interrupting worker thread: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 212
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized finish()V
    .locals 1

    .prologue
    .line 202
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->allAudioReceived:Z

    .line 203
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    monitor-exit p0

    return-void

    .line 202
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getTimeGotResult()J
    .locals 2

    .prologue
    .line 269
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivTimeGotResults:J

    return-wide v0
.end method

.method public getTimeSendFinish()J
    .locals 2

    .prologue
    .line 265
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivTimeSendFinish:J

    return-wide v0
.end method

.method public getTimeSendStart()J
    .locals 2

    .prologue
    .line 261
    iget-wide v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivTimeSendStart:J

    return-wide v0
.end method

.method public declared-synchronized isCancelled()Z
    .locals 1

    .prologue
    .line 243
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isResponseReceived()Z
    .locals 1

    .prologue
    .line 247
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->responseReceived:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeListener(Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/vlingo/sdk/internal/recognizer/network/SRRequestListener;

    .prologue
    .line 257
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 258
    monitor-exit p0

    return-void

    .line 257
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 124
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestThread:Ljava/lang/Thread;

    .line 126
    iget-boolean v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-eqz v4, :cond_0

    .line 181
    monitor-enter p0

    .line 182
    :try_start_1
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->closeConnection()V

    .line 183
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestThread:Ljava/lang/Thread;

    .line 184
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_7

    .line 188
    :goto_0
    return-void

    .line 129
    :cond_0
    :try_start_2
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->startRequest()V

    .line 131
    iget-boolean v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->sendAudio:Z

    if-eqz v4, :cond_8

    .line 132
    const/4 v1, 0x0

    .line 133
    .local v1, "allAudioSent":Z
    const/4 v2, 0x0

    .line 135
    .local v2, "errorOnAudioSend":Z
    :cond_1
    const/4 v3, 0x0

    .line 136
    .local v3, "segment":Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;
    monitor-enter p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 137
    :try_start_3
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->audioQueue:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;

    move-object v3, v0

    .line 138
    if-nez v3, :cond_2

    .line 139
    iget-boolean v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z

    if-nez v4, :cond_2

    .line 140
    iget-boolean v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->allAudioReceived:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-nez v4, :cond_6

    .line 142
    :try_start_4
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 148
    :cond_2
    :goto_1
    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 150
    if-eqz v3, :cond_3

    :try_start_6
    iget-boolean v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z

    if-nez v4, :cond_3

    .line 151
    invoke-direct {p0, v3}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->sendAudioSegment(Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;)Z

    move-result v4

    if-nez v4, :cond_7

    move v2, v5

    .line 153
    :cond_3
    :goto_2
    if-nez v1, :cond_4

    iget-boolean v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z

    if-nez v4, :cond_4

    if-eqz v2, :cond_1

    .line 155
    :cond_4
    iget-boolean v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->startedWritingAudio:Z

    if-nez v4, :cond_5

    iget-boolean v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z

    if-nez v4, :cond_5

    .line 157
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z

    .line 158
    const/4 v4, -0x2

    invoke-direct {p0, v4}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->notifyListeners(I)V

    .line 161
    :cond_5
    iget-boolean v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z

    if-eqz v4, :cond_8

    .line 162
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->sendCancelRequest()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 181
    monitor-enter p0

    .line 182
    :try_start_7
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->closeConnection()V

    .line 183
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestThread:Ljava/lang/Thread;

    .line 184
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    throw v4

    .line 144
    :cond_6
    const/4 v1, 0x1

    goto :goto_1

    .line 148
    :catchall_1
    move-exception v4

    :try_start_8
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    throw v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 181
    .end local v1    # "allAudioSent":Z
    .end local v2    # "errorOnAudioSend":Z
    .end local v3    # "segment":Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;
    :catchall_2
    move-exception v4

    monitor-enter p0

    .line 182
    :try_start_a
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->closeConnection()V

    .line 183
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestThread:Ljava/lang/Thread;

    .line 184
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_6

    .line 181
    throw v4

    .line 151
    .restart local v1    # "allAudioSent":Z
    .restart local v2    # "errorOnAudioSend":Z
    .restart local v3    # "segment":Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;
    :cond_7
    const/4 v2, 0x0

    goto :goto_2

    .line 167
    .end local v1    # "allAudioSent":Z
    .end local v2    # "errorOnAudioSend":Z
    .end local v3    # "segment":Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;
    :cond_8
    :try_start_b
    iget-boolean v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    if-eqz v4, :cond_9

    .line 181
    monitor-enter p0

    .line 182
    :try_start_c
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->closeConnection()V

    .line 183
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestThread:Ljava/lang/Thread;

    .line 184
    monitor-exit p0

    goto :goto_0

    :catchall_3
    move-exception v4

    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    throw v4

    .line 171
    :cond_9
    :try_start_d
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->finishRequest()V

    .line 173
    iget-boolean v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    if-eqz v4, :cond_a

    .line 181
    monitor-enter p0

    .line 182
    :try_start_e
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->closeConnection()V

    .line 183
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestThread:Ljava/lang/Thread;

    .line 184
    monitor-exit p0

    goto/16 :goto_0

    :catchall_4
    move-exception v4

    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    throw v4

    .line 177
    :cond_a
    :try_start_f
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->receiveResponse()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    .line 181
    monitor-enter p0

    .line 182
    :try_start_10
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->closeConnection()V

    .line 183
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestThread:Ljava/lang/Thread;

    .line 184
    monitor-exit p0

    goto/16 :goto_0

    :catchall_5
    move-exception v4

    monitor-exit p0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_5

    throw v4

    .line 142
    .restart local v1    # "allAudioSent":Z
    .restart local v2    # "errorOnAudioSend":Z
    .restart local v3    # "segment":Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;
    :catch_0
    move-exception v4

    goto :goto_1

    .line 184
    .end local v1    # "allAudioSent":Z
    .end local v2    # "errorOnAudioSend":Z
    .end local v3    # "segment":Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;
    :catchall_6
    move-exception v4

    :try_start_11
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_6

    throw v4

    :catchall_7
    move-exception v4

    :try_start_12
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_7

    throw v4
.end method

.method public declared-synchronized sendAudio([BII)V
    .locals 2
    .param p1, "audio"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 194
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;

    invoke-direct {v0, p1, p2, p3}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;-><init>([BII)V

    .line 195
    .local v0, "segment":Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->audioQueue:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 196
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    monitor-exit p0

    return-void

    .line 194
    .end local v0    # "segment":Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request$AudioSegment;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method startRequest()V
    .locals 17

    .prologue
    .line 276
    const-string/jumbo v12, "RUN"

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->recordDetailedTiming(Ljava/lang/String;)V

    .line 279
    const/4 v12, 0x1

    :try_start_0
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->notifyListeners(I)V

    .line 283
    const/4 v8, 0x0

    .line 293
    .local v8, "out":Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 294
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z

    if-eqz v12, :cond_1

    .line 297
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 415
    .end local v8    # "out":Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    :cond_0
    :goto_0
    return-void

    .line 301
    .restart local v8    # "out":Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    :cond_1
    :try_start_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRManager:Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    invoke-virtual {v12, v13}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Manager;->getConnection(Lcom/vlingo/sdk/internal/recognizer/SRContext;)Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    .line 303
    const-string/jumbo v12, "OPEN"

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->recordDetailedTiming(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 308
    :try_start_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    const-string/jumbo v13, "X-vlrequest"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "ClientRequestID:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    invoke-virtual {v15}, Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;->getRequestID()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;->setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    invoke-virtual {v12}, Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;->getConnection()Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    move-result-object v11

    .line 313
    .local v11, "vHttp":Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;
    invoke-virtual {v11}, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->getVStreamConnection()Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;

    move-result-object v10

    .line 314
    .local v10, "vCon":Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;
    invoke-virtual {v11}, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->getHttpInteraction()Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getRequest()Lcom/vlingo/sdk/internal/http/custom/HttpRequest;

    move-result-object v12

    invoke-interface {v10, v12}, Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;->startRequest(Lcom/vlingo/sdk/internal/http/custom/HttpRequest;)V

    .line 316
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 319
    :try_start_4
    monitor-enter p0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 320
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z

    if-nez v12, :cond_2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    if-nez v12, :cond_3

    .line 323
    :cond_2
    monitor-exit p0

    goto :goto_0

    .line 329
    :catchall_0
    move-exception v12

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v12
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 409
    .end local v8    # "out":Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    .end local v10    # "vCon":Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;
    .end local v11    # "vHttp":Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;
    :catch_0
    move-exception v3

    .line 410
    .local v3, "e":Ljava/lang/Exception;
    :try_start_7
    const-string/jumbo v12, "startRequest"

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v3}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->fail(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_0

    .line 411
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v12

    throw v12

    .line 304
    .restart local v8    # "out":Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    :catch_1
    move-exception v3

    .line 305
    .local v3, "e":Ljava/io/IOException;
    const/4 v12, -0x1

    :try_start_8
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->notifyListeners(I)V

    .line 306
    monitor-exit p0

    goto :goto_0

    .line 316
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_2
    move-exception v12

    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    throw v12
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 326
    .restart local v10    # "vCon":Lcom/vlingo/sdk/internal/http/custom/VStreamConnection;
    .restart local v11    # "vHttp":Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;
    :cond_3
    :try_start_a
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    invoke-virtual {v12}, Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;->getOut()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    move-result-object v8

    .line 327
    const-string/jumbo v12, "HDRS"

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->recordDetailedTiming(Ljava/lang/String;)V

    .line 328
    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeBoundary()V

    .line 329
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 331
    :try_start_b
    monitor-enter p0
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 332
    :try_start_c
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z

    if-nez v12, :cond_4

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    if-nez v12, :cond_5

    .line 335
    :cond_4
    monitor-exit p0

    goto/16 :goto_0

    .line 350
    :catchall_3
    move-exception v12

    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    :try_start_d
    throw v12
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 339
    :cond_5
    :try_start_e
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    invoke-virtual {v12}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getEvents()Ljava/util/List;

    move-result-object v5

    .line 340
    .local v5, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/sdk/recognition/dialog/VLDialogEvent;>;"
    if-eqz v5, :cond_6

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v12

    if-lez v12, :cond_6

    .line 341
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->buildEventElement()Ljava/lang/String;

    move-result-object v4

    .line 343
    .local v4, "event":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/sdk/internal/util/StringUtils;->convertStringToBytes(Ljava/lang/String;)[B

    move-result-object v2

    .line 344
    .local v2, "data":[B
    const-string/jumbo v12, "events"

    const-string/jumbo v13, "text/xml"

    const/4 v14, 0x1

    invoke-virtual {v8, v12, v13, v2, v14}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeDataField(Ljava/lang/String;Ljava/lang/String;[BZ)V

    .line 345
    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->flush()V

    .line 348
    const-string/jumbo v12, "DMEV"

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->recordDetailedTiming(Ljava/lang/String;)V

    .line 350
    .end local v2    # "data":[B
    .end local v4    # "event":Ljava/lang/String;
    :cond_6
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    .line 352
    :try_start_f
    monitor-enter p0
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_0
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 353
    :try_start_10
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z

    if-nez v12, :cond_7

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    if-nez v12, :cond_8

    .line 356
    :cond_7
    monitor-exit p0

    goto/16 :goto_0

    .line 366
    :catchall_4
    move-exception v12

    monitor-exit p0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    :try_start_11
    throw v12
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_0
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    .line 359
    :cond_8
    :try_start_12
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    invoke-virtual {v12}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getDialogState()[B

    move-result-object v2

    .line 360
    .restart local v2    # "data":[B
    if-eqz v2, :cond_9

    array-length v12, v2

    if-lez v12, :cond_9

    .line 361
    const-string/jumbo v12, "dialog-data"

    const-string/jumbo v13, "binary"

    invoke-virtual {v8, v12, v13, v2}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeDataField(Ljava/lang/String;Ljava/lang/String;[B)V

    .line 362
    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->flush()V

    .line 364
    const-string/jumbo v12, "DMST"

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->recordDetailedTiming(Ljava/lang/String;)V

    .line 366
    :cond_9
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_4

    .line 368
    :try_start_13
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    invoke-virtual {v12}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getAudioSourceInfo()Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v1

    .line 369
    .local v1, "asi":Lcom/vlingo/sdk/recognition/AudioSourceInfo;
    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->isString()Z

    move-result v12

    if-eqz v12, :cond_c

    const/4 v6, 0x1

    .line 371
    .local v6, "isStringReco":Z
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->sendAudio:Z

    if-nez v12, :cond_a

    if-eqz v6, :cond_e

    .line 372
    :cond_a
    monitor-enter p0
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_0
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    .line 373
    :try_start_14
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z

    if-nez v12, :cond_b

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    if-nez v12, :cond_d

    .line 376
    :cond_b
    monitor-exit p0

    goto/16 :goto_0

    .line 384
    :catchall_5
    move-exception v12

    monitor-exit p0
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_5

    :try_start_15
    throw v12
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_0
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    .line 369
    .end local v6    # "isStringReco":Z
    :cond_c
    const/4 v6, 0x0

    goto :goto_1

    .line 379
    .restart local v6    # "isStringReco":Z
    :cond_d
    :try_start_16
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->buildMetaElement()Ljava/lang/String;

    move-result-object v7

    .line 380
    .local v7, "meta":Ljava/lang/String;
    const-string/jumbo v12, "meta"

    const-string/jumbo v13, "text/xml"

    invoke-virtual {v8, v12, v13, v7}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeField(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->flush()V

    .line 383
    const-string/jumbo v12, "META"

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->recordDetailedTiming(Ljava/lang/String;)V

    .line 384
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_5

    .line 387
    .end local v7    # "meta":Ljava/lang/String;
    :cond_e
    if-eqz v6, :cond_0

    .line 388
    :try_start_17
    monitor-enter p0
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_0
    .catchall {:try_start_17 .. :try_end_17} :catchall_1

    .line 389
    :try_start_18
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->requestCancelled:Z

    if-nez v12, :cond_f

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivSRCon:Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;

    if-nez v12, :cond_10

    .line 392
    :cond_f
    monitor-exit p0

    goto/16 :goto_0

    .line 405
    :catchall_6
    move-exception v12

    monitor-exit p0
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_6

    :try_start_19
    throw v12
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_0
    .catchall {:try_start_19 .. :try_end_19} :catchall_1

    .line 395
    :cond_10
    :try_start_1a
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivContext:Lcom/vlingo/sdk/internal/recognizer/SRContext;

    invoke-virtual {v12}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getAudioSourceInfo()Lcom/vlingo/sdk/recognition/AudioSourceInfo;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vlingo/sdk/recognition/AudioSourceInfo;->getText()Ljava/lang/String;

    move-result-object v9

    .line 396
    .local v9, "text":Ljava/lang/String;
    const-string/jumbo v12, "text"

    const-string/jumbo v13, "text/plain; charset=utf-8"

    invoke-virtual {v8, v12, v13, v9}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeField(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->flush()V

    .line 399
    const-string/jumbo v12, "TEXT"

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->recordDetailedTiming(Ljava/lang/String;)V

    .line 402
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivCRC32:Ljava/util/zip/CRC32;

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/zip/CRC32;->update([B)V

    .line 403
    const-string/jumbo v12, "checksum"

    const-string/jumbo v13, "text/crc32"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/sdk/internal/recognizer/sr3/SR3Request;->ivCRC32:Ljava/util/zip/CRC32;

    invoke-virtual {v15}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v15

    invoke-virtual/range {v14 .. v16}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v8, v12, v13, v14}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeField(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->flush()V

    .line 405
    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_6

    goto/16 :goto_0
.end method
