.class public Lcom/vlingo/sdk/internal/recognizer/Queue;
.super Ljava/lang/Object;
.source "Queue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/Queue$Element;
    }
.end annotation


# instance fields
.field protected m_Last:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;

.field protected m_Nodes:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method


# virtual methods
.method public declared-synchronized add(Ljava/lang/Object;)V
    .locals 2
    .param p1, "element"    # Ljava/lang/Object;

    .prologue
    .line 17
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/Queue$Element;

    invoke-direct {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/Queue$Element;-><init>(Ljava/lang/Object;)V

    .line 18
    .local v0, "e":Lcom/vlingo/sdk/internal/recognizer/Queue$Element;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Queue;->m_Last:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;

    if-nez v1, :cond_0

    .line 19
    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Queue;->m_Nodes:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;

    .line 24
    :goto_0
    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Queue;->m_Last:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;

    .line 25
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 26
    monitor-exit p0

    return-void

    .line 22
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Queue;->m_Last:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;

    iput-object v0, v1, Lcom/vlingo/sdk/internal/recognizer/Queue$Element;->m_Next:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 17
    .end local v0    # "e":Lcom/vlingo/sdk/internal/recognizer/Queue$Element;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized clear()Z
    .locals 2

    .prologue
    .line 48
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Queue;->m_Nodes:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 49
    .local v0, "inProgress":Z
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Queue;->m_Last:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;

    .line 50
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Queue;->m_Last:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;

    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Queue;->m_Nodes:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    monitor-exit p0

    return v0

    .line 48
    .end local v0    # "inProgress":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized isEmpty()Z
    .locals 1

    .prologue
    .line 29
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Queue;->m_Nodes:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized pop()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 33
    monitor-enter p0

    const/4 v0, 0x0

    .line 34
    .local v0, "element":Ljava/lang/Object;
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Queue;->m_Nodes:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;

    if-eqz v1, :cond_0

    .line 35
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Queue;->m_Nodes:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;

    iget-object v0, v1, Lcom/vlingo/sdk/internal/recognizer/Queue$Element;->m_Element:Ljava/lang/Object;

    .line 36
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Queue;->m_Nodes:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/Queue;->m_Last:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;

    if-ne v1, v2, :cond_1

    .line 37
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Queue;->m_Last:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;

    .line 38
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Queue;->m_Last:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;

    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Queue;->m_Nodes:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    .end local v0    # "element":Ljava/lang/Object;
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 41
    .restart local v0    # "element":Ljava/lang/Object;
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Queue;->m_Nodes:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;

    iget-object v1, v1, Lcom/vlingo/sdk/internal/recognizer/Queue$Element;->m_Next:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;

    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Queue;->m_Nodes:Lcom/vlingo/sdk/internal/recognizer/Queue$Element;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 33
    .end local v0    # "element":Ljava/lang/Object;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
