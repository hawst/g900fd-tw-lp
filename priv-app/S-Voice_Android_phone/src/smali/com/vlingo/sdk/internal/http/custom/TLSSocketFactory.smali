.class public Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;
.super Ljavax/net/ssl/SSLSocketFactory;
.source "TLSSocketFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory$HandshakeListenerWrapper;
    }
.end annotation


# static fields
.field private static final ALL_TLS_EXCEPT_TLS1DOT2:[Ljava/lang/String;

.field private static final ALL_TLS_INCLUDING_TLS1DOT2:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;

.field private static allAvailableCipherSuites:[Ljava/lang/String;

.field private static final weakCipherSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final weakCiphers:[Ljava/lang/String;


# instance fields
.field createSocketCount:I

.field private final delegate:Ljavax/net/ssl/SSLSocketFactory;

.field private final handshakeListener:Ljavax/net/ssl/HandshakeCompletedListener;

.field private final httpsConn:Ljavax/net/ssl/HttpsURLConnection;

.field private socketHashCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    const-class v0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->TAG:Ljava/lang/String;

    .line 33
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->allAvailableCipherSuites:[Ljava/lang/String;

    .line 65
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "TLSv1.1"

    aput-object v1, v0, v2

    const-string/jumbo v1, "TLSv1"

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->ALL_TLS_EXCEPT_TLS1DOT2:[Ljava/lang/String;

    .line 66
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "TLSv1.1"

    aput-object v1, v0, v2

    const-string/jumbo v1, "TLSv1"

    aput-object v1, v0, v3

    const-string/jumbo v1, "TLSv1.2"

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->ALL_TLS_INCLUDING_TLS1DOT2:[Ljava/lang/String;

    .line 78
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "_RSA_WITH_RC4_128_MD5"

    aput-object v1, v0, v2

    const-string/jumbo v1, "_RSA_WITH_AES_128_CBC_SHA"

    aput-object v1, v0, v3

    const-string/jumbo v1, "_RSA_WITH_AES_256_CBC_SHA"

    aput-object v1, v0, v4

    const-string/jumbo v1, "_ECDH_ECDSA_WITH_RC4_128_SHA"

    aput-object v1, v0, v5

    const/4 v1, 0x4

    const-string/jumbo v2, "_ECDH_ECDSA_WITH_AES_128_CBC_SHA"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "_ECDH_ECDSA_WITH_AES_256_CBC_SHA"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "_ECDH_RSA_WITH_RC4_128_SHA"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "_ECDH_RSA_WITH_AES_128_CBC_SHA"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "_ECDH_RSA_WITH_AES_256_CBC_SHA"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "_RSA_WITH_3DES_EDE_CBC_SHA"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "_RSA_WITH_DES_CBC_SHA"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "_ECDH_RSA_WITH_3DES_EDE_CBC_SHA"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "_DHE_RSA_WITH_DES_CBC_SHA"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "_DHE_DSS_WITH_DES_CBC_SHA"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->weakCiphers:[Ljava/lang/String;

    .line 100
    new-instance v0, Ljava/util/HashSet;

    sget-object v1, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->weakCiphers:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->weakCipherSet:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Ljavax/net/ssl/HttpsURLConnection;)V
    .locals 1
    .param p1, "httpsConn"    # Ljavax/net/ssl/HttpsURLConnection;

    .prologue
    .line 103
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;-><init>(Ljavax/net/ssl/HttpsURLConnection;Ljavax/net/ssl/HandshakeCompletedListener;)V

    .line 104
    return-void
.end method

.method public constructor <init>(Ljavax/net/ssl/HttpsURLConnection;Ljavax/net/ssl/HandshakeCompletedListener;)V
    .locals 6
    .param p1, "conn"    # Ljavax/net/ssl/HttpsURLConnection;
    .param p2, "listener"    # Ljavax/net/ssl/HandshakeCompletedListener;

    .prologue
    .line 106
    invoke-direct {p0}, Ljavax/net/ssl/SSLSocketFactory;-><init>()V

    .line 43
    const/4 v3, 0x0

    iput v3, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->createSocketCount:I

    .line 107
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->httpsConn:Ljavax/net/ssl/HttpsURLConnection;

    .line 109
    new-instance v3, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory$HandshakeListenerWrapper;

    invoke-direct {v3, p0, p2}, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory$HandshakeListenerWrapper;-><init>(Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;Ljavax/net/ssl/HandshakeCompletedListener;)V

    iput-object v3, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->handshakeListener:Ljavax/net/ssl/HandshakeCompletedListener;

    .line 111
    const/4 v1, 0x0

    .line 117
    .local v1, "delegate":Ljavax/net/ssl/SSLSocketFactory;
    :try_start_0
    const-string/jumbo v3, "TLS"

    invoke-static {v3}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v0

    .line 118
    .local v0, "context":Ljavax/net/ssl/SSLContext;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v4, v5}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 119
    invoke-virtual {v0}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/KeyManagementException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 127
    .end local v0    # "context":Ljavax/net/ssl/SSLContext;
    :goto_0
    if-nez v1, :cond_0

    .line 128
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string/jumbo v4, "Unable to create SSLSocketFactory delegate!"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 121
    :catch_0
    move-exception v2

    .line 122
    .local v2, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v2}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0

    .line 124
    .end local v2    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v2

    .line 125
    .local v2, "e":Ljava/security/KeyManagementException;
    invoke-virtual {v2}, Ljava/security/KeyManagementException;->printStackTrace()V

    goto :goto_0

    .line 130
    .end local v2    # "e":Ljava/security/KeyManagementException;
    :cond_0
    iput-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->delegate:Ljavax/net/ssl/SSLSocketFactory;

    .line 131
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;

    .prologue
    .line 27
    iget v0, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->socketHashCode:I

    return v0
.end method

.method static synthetic access$200(Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;)Ljavax/net/ssl/HttpsURLConnection;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->httpsConn:Ljavax/net/ssl/HttpsURLConnection;

    return-object v0
.end method

.method private static excludeWeakCiphers([Ljava/lang/String;)[Ljava/lang/String;
    .locals 9
    .param p0, "suites"    # [Ljava/lang/String;

    .prologue
    .line 198
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 199
    .local v6, "suiteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v5, v0, v1

    .line 200
    .local v5, "suite":Ljava/lang/String;
    const/4 v2, 0x1

    .line 201
    .local v2, "keep":Z
    const-string/jumbo v7, "TLS_"

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string/jumbo v7, "SSL_"

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    :cond_0
    sget-object v7, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->weakCipherSet:Ljava/util/Set;

    const/4 v8, 0x3

    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 203
    const/4 v2, 0x0

    .line 209
    :cond_1
    :goto_1
    if-eqz v2, :cond_2

    .line 210
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 199
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 205
    :cond_3
    const-string/jumbo v7, "EXPORT"

    invoke-virtual {v5, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 207
    const/4 v2, 0x0

    goto :goto_1

    .line 213
    .end local v2    # "keep":Z
    .end local v5    # "suite":Ljava/lang/String;
    :cond_4
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v7, v7, [Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    .line 214
    .local v4, "remainingSuites":[Ljava/lang/String;
    return-object v4
.end method

.method public static declared-synchronized getAllDefaultCipherSuites()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 137
    const-class v2, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->allAvailableCipherSuites:[Ljava/lang/String;

    if-nez v1, :cond_0

    .line 138
    new-instance v0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;-><init>(Ljavax/net/ssl/HttpsURLConnection;)V

    .line 139
    .local v0, "f":Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;
    iget-object v1, v0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->delegate:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLSocketFactory;->getDefaultCipherSuites()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    sput-object v1, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->allAvailableCipherSuites:[Ljava/lang/String;

    .line 141
    :cond_0
    sget-object v1, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->allAvailableCipherSuites:[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    return-object v1

    .line 137
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private selectWhitelistedCiphers([Ljava/lang/String;)[Ljava/lang/String;
    .locals 11
    .param p1, "defaultSuites"    # [Ljava/lang/String;

    .prologue
    .line 168
    const-string/jumbo v9, "vlsdk.https.tls.ciphers.whitelist"

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/vlingo/sdk/internal/settings/Settings;->getPersistentString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 169
    .local v8, "whitelistString":Ljava/lang/String;
    if-eqz v8, :cond_3

    .line 171
    new-instance v1, Ljava/util/HashSet;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    invoke-direct {v1, v9}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 172
    .local v1, "availableSuites":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const-string/jumbo v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 173
    .local v7, "whitelist":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v9, v7

    if-ge v2, v9, :cond_0

    .line 174
    aget-object v9, v7, v2

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v2

    .line 173
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 176
    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 177
    .local v5, "okSuiteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object v0, v7

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_2

    aget-object v6, v0, v3

    .line 178
    .local v6, "suite":Ljava/lang/String;
    invoke-virtual {v1, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 179
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 182
    .end local v6    # "suite":Ljava/lang/String;
    :cond_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v9

    new-array v9, v9, [Ljava/lang/String;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/String;

    .line 184
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "availableSuites":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v2    # "i":I
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "okSuiteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "whitelist":[Ljava/lang/String;
    :goto_2
    return-object v9

    :cond_3
    move-object v9, p1

    goto :goto_2
.end method

.method public static setupTLSIfNeeded(Ljava/net/HttpURLConnection;)V
    .locals 3
    .param p0, "conn"    # Ljava/net/HttpURLConnection;

    .prologue
    .line 290
    instance-of v2, p0, Ljavax/net/ssl/HttpsURLConnection;

    if-eqz v2, :cond_0

    move-object v1, p0

    .line 297
    check-cast v1, Ljavax/net/ssl/HttpsURLConnection;

    .line 299
    .local v1, "httpsConn":Ljavax/net/ssl/HttpsURLConnection;
    :try_start_0
    new-instance v2, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;

    invoke-direct {v2, v1}, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;-><init>(Ljavax/net/ssl/HttpsURLConnection;)V

    invoke-virtual {v1, v2}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 309
    .end local v1    # "httpsConn":Ljavax/net/ssl/HttpsURLConnection;
    :cond_0
    :goto_0
    return-void

    .line 301
    .restart local v1    # "httpsConn":Ljavax/net/ssl/HttpsURLConnection;
    :catch_0
    move-exception v0

    .line 302
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public createSocket(Ljava/lang/String;I)Ljava/net/Socket;
    .locals 1
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "port"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/net/UnknownHostException;
        }
    .end annotation

    .prologue
    .line 286
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->delegate:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/lang/String;I)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public createSocket(Ljava/lang/String;ILjava/net/InetAddress;I)Ljava/net/Socket;
    .locals 1
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "port"    # I
    .param p3, "localHost"    # Ljava/net/InetAddress;
    .param p4, "localPort"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/net/UnknownHostException;
        }
    .end annotation

    .prologue
    .line 279
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->delegate:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/lang/String;ILjava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public createSocket(Ljava/net/InetAddress;I)Ljava/net/Socket;
    .locals 1
    .param p1, "host"    # Ljava/net/InetAddress;
    .param p2, "port"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 230
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->delegate:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public createSocket(Ljava/net/InetAddress;ILjava/net/InetAddress;I)Ljava/net/Socket;
    .locals 1
    .param p1, "address"    # Ljava/net/InetAddress;
    .param p2, "port"    # I
    .param p3, "localAddress"    # Ljava/net/InetAddress;
    .param p4, "localPort"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 223
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->delegate:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/InetAddress;ILjava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;
    .locals 4
    .param p1, "rawSocket"    # Ljava/net/Socket;
    .param p2, "host"    # Ljava/lang/String;
    .param p3, "port"    # I
    .param p4, "autoClose"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 239
    iget v2, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->createSocketCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->createSocketCount:I

    .line 241
    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->delegate:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v2, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v1

    check-cast v1, Ljavax/net/ssl/SSLSocket;

    .line 246
    .local v1, "socket":Ljavax/net/ssl/SSLSocket;
    iget v2, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->createSocketCount:I

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    .line 247
    iget v2, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->socketHashCode:I

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 252
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    iput v2, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->socketHashCode:I

    .line 259
    sget-object v0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->ALL_TLS_EXCEPT_TLS1DOT2:[Ljava/lang/String;

    .line 260
    .local v0, "httpsProtocols":[Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/sdk/internal/settings/Settings;->isTLSv1Dot2Enabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 261
    sget-object v0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->ALL_TLS_INCLUDING_TLS1DOT2:[Ljava/lang/String;

    .line 264
    :cond_1
    invoke-virtual {v1, v0}, Ljavax/net/ssl/SSLSocket;->setEnabledProtocols([Ljava/lang/String;)V

    .line 265
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->getDefaultCipherSuites()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljavax/net/ssl/SSLSocket;->setEnabledCipherSuites([Ljava/lang/String;)V

    .line 267
    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->handshakeListener:Ljavax/net/ssl/HandshakeCompletedListener;

    if-eqz v2, :cond_2

    .line 268
    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->handshakeListener:Ljavax/net/ssl/HandshakeCompletedListener;

    invoke-virtual {v1, v2}, Ljavax/net/ssl/SSLSocket;->addHandshakeCompletedListener(Ljavax/net/ssl/HandshakeCompletedListener;)V

    .line 271
    :cond_2
    return-object v1
.end method

.method public getDefaultCipherSuites()[Ljava/lang/String;
    .locals 5

    .prologue
    .line 146
    const/4 v2, 0x0

    .line 147
    .local v2, "ret":[Ljava/lang/String;
    iget-object v3, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->delegate:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v3}, Ljavax/net/ssl/SSLSocketFactory;->getDefaultCipherSuites()[Ljava/lang/String;

    move-result-object v0

    .line 148
    .local v0, "defaultSuites":[Ljava/lang/String;
    const-string/jumbo v3, "vlsdk.https.tls.ciphers.whitelist"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/vlingo/sdk/internal/settings/Settings;->getPersistentString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v1, 0x1

    .line 149
    .local v1, "haveWhitelist":Z
    :goto_0
    if-eqz v1, :cond_1

    .line 153
    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->selectWhitelistedCiphers([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 164
    :goto_1
    return-object v2

    .line 148
    .end local v1    # "haveWhitelist":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 159
    .restart local v1    # "haveWhitelist":Z
    :cond_1
    invoke-static {v0}, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->excludeWeakCiphers([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public getSupportedCipherSuites()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 189
    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->delegate:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v2}, Ljavax/net/ssl/SSLSocketFactory;->getSupportedCipherSuites()[Ljava/lang/String;

    move-result-object v1

    .line 191
    .local v1, "suites":[Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/http/custom/TLSSocketFactory;->excludeWeakCiphers([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 194
    .local v0, "strongSuites":[Ljava/lang/String;
    return-object v0
.end method
