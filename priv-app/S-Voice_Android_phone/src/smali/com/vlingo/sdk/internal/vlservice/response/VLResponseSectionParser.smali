.class public abstract Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;
.super Ljava/lang/Object;
.source "VLResponseSectionParser.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/xml/XmlHandler;


# instance fields
.field protected responseParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;)V
    .locals 1
    .param p1, "responseParser"    # Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;->responseParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    .line 19
    iput-object p1, p0, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;->responseParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    .line 20
    return-void
.end method


# virtual methods
.method public beginDocument()V
    .locals 0

    .prologue
    .line 26
    return-void
.end method

.method public characters([C)V
    .locals 0
    .param p1, "cData"    # [C

    .prologue
    .line 28
    return-void
.end method

.method public endDocument()V
    .locals 0

    .prologue
    .line 27
    return-void
.end method

.method public abstract handlesElement(I)Z
.end method

.method public onParseBegin([C)V
    .locals 0
    .param p1, "xml"    # [C

    .prologue
    .line 23
    return-void
.end method
