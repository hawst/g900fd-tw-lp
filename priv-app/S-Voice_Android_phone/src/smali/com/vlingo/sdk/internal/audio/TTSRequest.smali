.class public Lcom/vlingo/sdk/internal/audio/TTSRequest;
.super Ljava/lang/Object;
.source "TTSRequest.java"


# instance fields
.field public body:Ljava/lang/String;

.field public gender:Ljava/lang/String;

.field public senderAddress:Ljava/lang/String;

.field public senderDisplayName:Ljava/lang/String;

.field public senderOnly:Z

.field public speechRate:Ljava/lang/String;

.field public subject:Ljava/lang/String;

.field public type:Ljava/lang/String;

.field public wordLimit:I


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->wordLimit:I

    .line 12
    const-string/jumbo v0, "Normal"

    iput-object v0, p0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->speechRate:Ljava/lang/String;

    .line 16
    return-void
.end method

.method public static getEmail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/vlingo/sdk/internal/audio/TTSRequest;
    .locals 2
    .param p0, "senderName"    # Ljava/lang/String;
    .param p1, "subject"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;
    .param p3, "senderOnly"    # Z

    .prologue
    .line 26
    new-instance v0, Lcom/vlingo/sdk/internal/audio/TTSRequest;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/audio/TTSRequest;-><init>()V

    .line 27
    .local v0, "req":Lcom/vlingo/sdk/internal/audio/TTSRequest;
    const-string/jumbo v1, "Email"

    iput-object v1, v0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->type:Ljava/lang/String;

    .line 28
    iput-object p0, v0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->senderDisplayName:Ljava/lang/String;

    .line 29
    iput-object p1, v0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->subject:Ljava/lang/String;

    .line 30
    iput-object p2, v0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->body:Ljava/lang/String;

    .line 31
    iput-boolean p3, v0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->senderOnly:Z

    .line 32
    return-object v0
.end method

.method public static getMMS(Ljava/lang/String;Ljava/lang/String;Z)Lcom/vlingo/sdk/internal/audio/TTSRequest;
    .locals 2
    .param p0, "senderName"    # Ljava/lang/String;
    .param p1, "subject"    # Ljava/lang/String;
    .param p2, "senderOnly"    # Z

    .prologue
    .line 45
    new-instance v0, Lcom/vlingo/sdk/internal/audio/TTSRequest;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/audio/TTSRequest;-><init>()V

    .line 46
    .local v0, "req":Lcom/vlingo/sdk/internal/audio/TTSRequest;
    const-string/jumbo v1, "MMS"

    iput-object v1, v0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->type:Ljava/lang/String;

    .line 47
    iput-object p0, v0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->senderDisplayName:Ljava/lang/String;

    .line 48
    iput-object p1, v0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->subject:Ljava/lang/String;

    .line 49
    iput-boolean p2, v0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->senderOnly:Z

    .line 50
    return-object v0
.end method

.method public static getSMS(Ljava/lang/String;Ljava/lang/String;Z)Lcom/vlingo/sdk/internal/audio/TTSRequest;
    .locals 2
    .param p0, "senderName"    # Ljava/lang/String;
    .param p1, "body"    # Ljava/lang/String;
    .param p2, "senderOnly"    # Z

    .prologue
    .line 36
    new-instance v0, Lcom/vlingo/sdk/internal/audio/TTSRequest;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/audio/TTSRequest;-><init>()V

    .line 37
    .local v0, "req":Lcom/vlingo/sdk/internal/audio/TTSRequest;
    const-string/jumbo v1, "SMS"

    iput-object v1, v0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->type:Ljava/lang/String;

    .line 38
    iput-object p0, v0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->senderDisplayName:Ljava/lang/String;

    .line 39
    iput-object p1, v0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->body:Ljava/lang/String;

    .line 40
    iput-boolean p2, v0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->senderOnly:Z

    .line 41
    return-object v0
.end method

.method public static getText(Ljava/lang/String;)Lcom/vlingo/sdk/internal/audio/TTSRequest;
    .locals 2
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 19
    new-instance v0, Lcom/vlingo/sdk/internal/audio/TTSRequest;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/audio/TTSRequest;-><init>()V

    .line 20
    .local v0, "req":Lcom/vlingo/sdk/internal/audio/TTSRequest;
    const-string/jumbo v1, "Confirm"

    iput-object v1, v0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->type:Ljava/lang/String;

    .line 21
    iput-object p0, v0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->body:Ljava/lang/String;

    .line 22
    return-object v0
.end method


# virtual methods
.method public setGender(Ljava/lang/String;)V
    .locals 0
    .param p1, "gender"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->gender:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public setSpeechRate(Ljava/lang/String;)V
    .locals 0
    .param p1, "rate"    # Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->speechRate:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public setWordLimit(I)V
    .locals 0
    .param p1, "limit"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/vlingo/sdk/internal/audio/TTSRequest;->wordLimit:I

    .line 59
    return-void
.end method
