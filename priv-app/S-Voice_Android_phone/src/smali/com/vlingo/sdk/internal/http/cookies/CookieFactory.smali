.class public Lcom/vlingo/sdk/internal/http/cookies/CookieFactory;
.super Ljava/lang/Object;
.source "CookieFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/internal/http/cookies/Cookie;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v1, Lcom/vlingo/sdk/internal/http/cookies/CookieFactory;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManagerSingleton;->getInstance()Lcom/vlingo/sdk/internal/http/cookies/CookieJarManager;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManager;->createCookie(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/sdk/internal/http/cookies/Cookie;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
