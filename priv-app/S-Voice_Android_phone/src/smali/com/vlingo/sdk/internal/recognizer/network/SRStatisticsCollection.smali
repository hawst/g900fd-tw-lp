.class public Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;
.super Lcom/vlingo/sdk/internal/http/BaseHttpCallback;
.source "SRStatisticsCollection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$1;,
        Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;
    }
.end annotation


# static fields
.field private static final SEND_WAIT_TIME:I = 0x2710


# instance fields
.field private acceptedText:Ljava/lang/String;

.field private final collection:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/BaseHttpCallback;-><init>()V

    .line 35
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;->collection:Ljava/util/Vector;

    .line 80
    return-void
.end method


# virtual methods
.method public addStatistics(Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;)V
    .locals 4
    .param p1, "statistics"    # Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;

    .prologue
    .line 40
    if-nez p1, :cond_1

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 43
    :cond_1
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;->collection:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v2

    .line 44
    .local v2, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_2

    .line 45
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;->collection:Ljava/util/Vector;

    invoke-virtual {v3, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;

    .line 46
    .local v0, "group":Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;
    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;->mergeStatistics(Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;)Z
    invoke-static {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;->access$000(Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 44
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 51
    .end local v0    # "group":Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;
    :cond_2
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;

    const/4 v3, 0x0

    invoke-direct {v0, p1, v3}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/SRStatistics;Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$1;)V

    .line 52
    .restart local v0    # "group":Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;->collection:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public getAcceptedText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;->acceptedText:Ljava/lang/String;

    return-object v0
.end method

.method public getCollectionElements()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;->collection:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public onRetrysExhausted()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method public schedule(Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;)V
    .locals 3
    .param p1, "serverDetails"    # Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;
    .param p2, "clientMeta"    # Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    .param p3, "softwareMeta"    # Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    .prologue
    .line 68
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;->collection:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    .line 69
    .local v0, "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<*>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 70
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;

    .line 71
    .local v1, "group":Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;
    invoke-virtual {v1, p1, p2, p3}, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;->schedule(Lcom/vlingo/sdk/internal/recognizer/network/SRServerDetails;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;)V

    goto :goto_0

    .line 73
    .end local v1    # "group":Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection$SRStatisticsGroup;
    :cond_0
    return-void
.end method

.method public setAcceptedText(Ljava/lang/String;)V
    .locals 0
    .param p1, "acceptedText"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/SRStatisticsCollection;->acceptedText:Ljava/lang/String;

    .line 57
    return-void
.end method
