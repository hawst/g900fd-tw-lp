.class public final enum Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;
.super Ljava/lang/Enum;
.source "LMTTItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/lmtt/LMTTItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ChangeType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

.field public static final enum DELETE:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

.field public static final enum INSERT:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

.field public static final enum NOCHANGE:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

.field public static final enum UPDATE:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 16
    new-instance v0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    const-string/jumbo v1, "UPDATE"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;->UPDATE:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    new-instance v0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    const-string/jumbo v1, "INSERT"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;->INSERT:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    new-instance v0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    const-string/jumbo v1, "DELETE"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;->DELETE:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    new-instance v0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    const-string/jumbo v1, "NOCHANGE"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;->NOCHANGE:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    sget-object v1, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;->UPDATE:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;->INSERT:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;->DELETE:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;->NOCHANGE:Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;->$VALUES:[Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 16
    const-class v0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;->$VALUES:[Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    invoke-virtual {v0}, [Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/sdk/internal/lmtt/LMTTItem$ChangeType;

    return-object v0
.end method
