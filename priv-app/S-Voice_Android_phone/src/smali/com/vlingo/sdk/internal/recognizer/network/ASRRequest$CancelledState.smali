.class Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;
.super Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;
.source "ASRRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CancelledState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V
    .locals 1

    .prologue
    .line 1172
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;
    .param p2, "x1"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$1;

    .prologue
    .line 1172
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;-><init>(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V

    return-void
.end method


# virtual methods
.method public cancel(Z)V
    .locals 1
    .param p1, "timedOut"    # Z

    .prologue
    .line 1194
    const-string/jumbo v0, "cancel(): ignored"

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->log(Ljava/lang/String;)V

    .line 1195
    return-void
.end method

.method public endAudio()Z
    .locals 1

    .prologue
    .line 1178
    const-string/jumbo v0, "endAudio(): ignored"

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->log(Ljava/lang/String;)V

    .line 1179
    const/4 v0, 0x1

    return v0
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 1189
    const-string/jumbo v0, "finish(): ignored"

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->log(Ljava/lang/String;)V

    .line 1190
    return-void
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 1174
    const/4 v0, 0x1

    return v0
.end method

.method public onRun()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1205
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->stop()V
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$4300(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)V

    .line 1207
    const-string/jumbo v2, "[LatencyCheck] CancelledState clearing queue"

    invoke-virtual {p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->log(Ljava/lang/String;)V

    .line 1208
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getAudioQueue()Ljava/util/concurrent/BlockingQueue;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$1800(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/util/concurrent/BlockingQueue;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/BlockingQueue;->clear()V

    .line 1210
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getAudioOutputStream()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3800(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->isOpen()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1212
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    const-string/jumbo v3, "sending cancellation to server ..."

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->serverCheckLog(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/String;)V

    .line 1214
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getAudioOutputStream()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3800(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;->close()Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$AudioOutputStream;

    .line 1217
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getOutputStream()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3500(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    move-result-object v2

    const-string/jumbo v3, "cancel"

    const-string/jumbo v4, "text/xml"

    const-string/jumbo v5, "<cancel/>"

    invoke-virtual {v2, v3, v4, v5}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->writeField(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1218
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getOutputStream()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3500(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->flush()V

    .line 1219
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getOutputStream()Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3500(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/http/custom/MPOutputStream;->close()V

    .line 1223
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;
    invoke-static {v3}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->consume(Ljava/io/InputStream;)[B
    invoke-static {v2, v3}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$4100(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/io/InputStream;)[B

    move-result-object v1

    .line 1224
    .local v1, "httpResponse":[B
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "cancel response = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v1, :cond_1

    const-string/jumbo v2, ""

    :goto_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->log(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1228
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;

    move-result-object v2

    .end local v1    # "httpResponse":[B
    :goto_1
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 1233
    :cond_0
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "cancel complete @ "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getTimingString()Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$4600(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->serverCheckLog(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$700(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;Ljava/lang/String;)V

    .line 1234
    return-void

    .line 1224
    .restart local v1    # "httpResponse":[B
    :cond_1
    :try_start_1
    new-instance v2, Ljava/lang/String;

    const-string/jumbo v4, "UTF-8"

    invoke-direct {v2, v1, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1225
    .end local v1    # "httpResponse":[B
    :catch_0
    move-exception v0

    .line 1226
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->log(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1228
    iget-object v2, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;
    invoke-static {v2}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;

    move-result-object v2

    goto :goto_1

    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->this$0:Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->getConnection()Ljava/net/HttpURLConnection;
    invoke-static {v3}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;->access$3000(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest;)Ljava/net/HttpURLConnection;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    throw v2
.end method

.method public sendAudio([BII)V
    .locals 1
    .param p1, "audio"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 1184
    const-string/jumbo v0, "sendAudio(): ignored"

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->log(Ljava/lang/String;)V

    .line 1185
    return-void
.end method

.method public setState(Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;)V
    .locals 2
    .param p1, "state"    # Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$State;

    .prologue
    .line 1199
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "[LatencyCheck] setState("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "): ignored"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/recognizer/network/ASRRequest$CancelledState;->log(Ljava/lang/String;)V

    .line 1200
    return-void
.end method
