.class Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;
.super Landroid/os/Handler;
.source "VLTrainerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/VLTrainerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NotificationHandler"
.end annotation


# static fields
.field static final ERROR:I = 0x2

.field static final SUCCESS:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/VLTrainerImpl;


# direct methods
.method constructor <init>(Lcom/vlingo/sdk/internal/VLTrainerImpl;)V
    .locals 1

    .prologue
    .line 42
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLTrainerImpl;

    .line 43
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 44
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x0

    .line 50
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLTrainerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLTrainerImpl;->mTrainerListener:Lcom/vlingo/sdk/training/VLTrainerListener;
    invoke-static {v3}, Lcom/vlingo/sdk/internal/VLTrainerImpl;->access$000(Lcom/vlingo/sdk/internal/VLTrainerImpl;)Lcom/vlingo/sdk/training/VLTrainerListener;

    move-result-object v2

    .line 51
    .local v2, "l":Lcom/vlingo/sdk/training/VLTrainerListener;
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLTrainerImpl;

    const/4 v4, 0x0

    # setter for: Lcom/vlingo/sdk/internal/VLTrainerImpl;->mTrainerListener:Lcom/vlingo/sdk/training/VLTrainerListener;
    invoke-static {v3, v4}, Lcom/vlingo/sdk/internal/VLTrainerImpl;->access$002(Lcom/vlingo/sdk/internal/VLTrainerImpl;Lcom/vlingo/sdk/training/VLTrainerListener;)Lcom/vlingo/sdk/training/VLTrainerListener;

    .line 53
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLTrainerImpl;

    invoke-virtual {v3, v5}, Lcom/vlingo/sdk/internal/VLTrainerImpl;->setBusy(Z)V

    .line 57
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLTrainerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLTrainerImpl;->semaphore:Ljava/util/concurrent/Semaphore;
    invoke-static {v3}, Lcom/vlingo/sdk/internal/VLTrainerImpl;->access$100(Lcom/vlingo/sdk/internal/VLTrainerImpl;)Ljava/util/concurrent/Semaphore;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    .line 59
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 61
    :pswitch_0
    if-eqz v2, :cond_0

    .line 62
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/util/HashMap;

    invoke-interface {v2, v3}, Lcom/vlingo/sdk/training/VLTrainerListener;->onUpdateReceived(Ljava/util/HashMap;)V

    goto :goto_0

    .line 66
    :pswitch_1
    if-eqz v2, :cond_0

    .line 67
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, [Ljava/lang/Object;

    check-cast v3, [Ljava/lang/Object;

    aget-object v0, v3, v5

    check-cast v0, Lcom/vlingo/sdk/training/VLTrainerErrors;

    .line 68
    .local v0, "errorCode":Lcom/vlingo/sdk/training/VLTrainerErrors;
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, [Ljava/lang/Object;

    check-cast v3, [Ljava/lang/Object;

    const/4 v4, 0x1

    aget-object v1, v3, v4

    check-cast v1, Ljava/lang/String;

    .line 69
    .local v1, "errorMsg":Ljava/lang/String;
    invoke-interface {v2, v0, v1}, Lcom/vlingo/sdk/training/VLTrainerListener;->onError(Lcom/vlingo/sdk/training/VLTrainerErrors;Ljava/lang/String;)V

    goto :goto_0

    .line 59
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method declared-synchronized notifyError(Lcom/vlingo/sdk/training/VLTrainerErrors;Ljava/lang/String;)V
    .locals 4
    .param p1, "error"    # Lcom/vlingo/sdk/training/VLTrainerErrors;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 80
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLTrainerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLTrainerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/VLTrainerImpl;->access$200(Lcom/vlingo/sdk/internal/VLTrainerImpl;)Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    monitor-exit p0

    return-void

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized notifySuccess(Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 76
    .local p1, "itemCount":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;Ljava/lang/Integer;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;->this$0:Lcom/vlingo/sdk/internal/VLTrainerImpl;

    # getter for: Lcom/vlingo/sdk/internal/VLTrainerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/VLTrainerImpl;->access$200(Lcom/vlingo/sdk/internal/VLTrainerImpl;)Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    monitor-exit p0

    return-void

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
