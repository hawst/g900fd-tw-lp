.class public final Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;
.super Ljava/lang/Object;
.source "TaggedResults.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ParseGroup"
.end annotation


# instance fields
.field private final confidence:F

.field private final parseType:Ljava/lang/String;

.field private final tags:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(FLjava/lang/String;I)V
    .locals 1
    .param p1, "confidence"    # F
    .param p2, "parseType"    # Ljava/lang/String;
    .param p3, "num"    # I

    .prologue
    .line 345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 346
    iput p1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->confidence:F

    .line 347
    iput-object p2, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->parseType:Ljava/lang/String;

    .line 348
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0, p3}, Ljava/util/Vector;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->tags:Ljava/util/Vector;

    .line 349
    return-void
.end method

.method synthetic constructor <init>(FLjava/lang/String;ILcom/vlingo/sdk/internal/recognizer/results/TaggedResults$1;)V
    .locals 0
    .param p1, "x0"    # F
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # I
    .param p4, "x3"    # Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$1;

    .prologue
    .line 339
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;-><init>(FLjava/lang/String;I)V

    return-void
.end method

.method static synthetic access$200(Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;)Ljava/util/Vector;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

    .prologue
    .line 339
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->tags:Ljava/util/Vector;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

    .prologue
    .line 339
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->parseType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;)F
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;

    .prologue
    .line 339
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->confidence:F

    return v0
.end method


# virtual methods
.method public getConfidence()F
    .locals 1

    .prologue
    .line 371
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->confidence:F

    return v0
.end method

.method public getNumTags()I
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->tags:Ljava/util/Vector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->tags:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getParseType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->parseType:Ljava/lang/String;

    return-object v0
.end method

.method public getTags()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 385
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->tags:Ljava/util/Vector;

    return-object v0
.end method

.method public isTagsEmpty()Z
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->tags:Ljava/util/Vector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->tags:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lookupTagByName(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 406
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->tags:Ljava/util/Vector;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->tags:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 407
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->tags:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v1

    .line 408
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 409
    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->tags:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;

    .line 410
    .local v2, "tag":Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;
    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 415
    .end local v0    # "i":I
    .end local v1    # "size":I
    .end local v2    # "tag":Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;
    :goto_1
    return-object v2

    .line 408
    .restart local v0    # "i":I
    .restart local v1    # "size":I
    .restart local v2    # "tag":Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 415
    .end local v0    # "i":I
    .end local v1    # "size":I
    .end local v2    # "tag":Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 353
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\n         ParseGroup: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->confidence:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->parseType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 354
    .local v1, "rs":Ljava/lang/String;
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->tags:Ljava/util/Vector;

    if-eqz v4, :cond_0

    .line 355
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->tags:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v2

    .line 356
    .local v2, "size":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\n            Tags: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 357
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 358
    iget-object v4, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$ParseGroup;->tags:Ljava/util/Vector;

    invoke-virtual {v4, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;

    .line 359
    .local v3, "tag":Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 357
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 362
    .end local v0    # "i":I
    .end local v2    # "size":I
    .end local v3    # "tag":Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults$Tag;
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " null"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 364
    :cond_1
    return-object v1
.end method
