.class Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;
.super Ljava/lang/Object;
.source "Recognizer.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/recognizer/Recognizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NetworkListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)V
    .locals 0

    .prologue
    .line 490
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/recognizer/Recognizer;
    .param p2, "x1"    # Lcom/vlingo/sdk/internal/recognizer/Recognizer$1;

    .prologue
    .line 490
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;-><init>(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)V

    return-void
.end method


# virtual methods
.method public onRecognitionResponse(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
    .locals 2
    .param p1, "response"    # Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReader:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$1000(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 515
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReader:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$1000(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->getGuttId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->setGuttId(Ljava/lang/String;)V

    .line 517
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$600(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v0

    sget-object v1, Lcom/vlingo/sdk/recognition/RecognitionMode;->HYBRID:Lcom/vlingo/sdk/recognition/RecognitionMode;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mArbiter:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$1100(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 518
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mArbiter:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$1100(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->notifyOfNetworkResult(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    .line 526
    :cond_1
    :goto_0
    return-void

    .line 519
    :cond_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$600(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v0

    sget-object v1, Lcom/vlingo/sdk/recognition/RecognitionMode;->CLOUD:Lcom/vlingo/sdk/recognition/RecognitionMode;

    if-ne v0, v1, :cond_1

    .line 520
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mConsumerListener:Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$1200(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;->onRecognitionResponse(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V

    .line 521
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->cleanup()V
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$1300(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)V

    goto :goto_0
.end method

.method public onRecognizerError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V
    .locals 2
    .param p1, "recError"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 499
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReader:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$1000(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 500
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mDataReader:Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$1000(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mNetworkRecognizer:Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$800(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/sdk/internal/recognizer/network/INetworkRecognizer;->getGuttId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/reader/DataReader;->setGuttId(Ljava/lang/String;)V

    .line 502
    :cond_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$600(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v0

    sget-object v1, Lcom/vlingo/sdk/recognition/RecognitionMode;->HYBRID:Lcom/vlingo/sdk/recognition/RecognitionMode;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mArbiter:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$1100(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 503
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mArbiter:Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$1100(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/arbiter/HybridArbiter;->notifyOfNetworkError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V

    .line 510
    :cond_1
    :goto_0
    return-void

    .line 504
    :cond_2
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    # getter for: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->mMode:Lcom/vlingo/sdk/recognition/RecognitionMode;
    invoke-static {v0}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$600(Lcom/vlingo/sdk/internal/recognizer/Recognizer;)Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v0

    sget-object v1, Lcom/vlingo/sdk/recognition/RecognitionMode;->CLOUD:Lcom/vlingo/sdk/recognition/RecognitionMode;

    if-ne v0, v1, :cond_1

    .line 505
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    sget-object v1, Lcom/vlingo/sdk/recognition/RecognitionMode;->CLOUD:Lcom/vlingo/sdk/recognition/RecognitionMode;

    # invokes: Lcom/vlingo/sdk/internal/recognizer/Recognizer;->handleError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V
    invoke-static {v0, p1, p2, v1}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->access$900(Lcom/vlingo/sdk/internal/recognizer/Recognizer;Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;Lcom/vlingo/sdk/recognition/RecognitionMode;)V

    goto :goto_0
.end method

.method public onRecognizerStateChanged(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;Ljava/lang/Object;)V
    .locals 1
    .param p1, "recState"    # Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 494
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/Recognizer$NetworkListener;->this$0:Lcom/vlingo/sdk/internal/recognizer/Recognizer;

    invoke-virtual {v0, p1, p2}, Lcom/vlingo/sdk/internal/recognizer/Recognizer;->handleStateChange(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;Ljava/lang/Object;)V

    .line 495
    return-void
.end method
