.class public Lcom/vlingo/sdk/internal/audio/AMRUtil;
.super Ljava/lang/Object;
.source "AMRUtil.java"


# static fields
.field private static final AMR_HEADER:[B

.field private static final FRAME_HEADER:I = 0x4

.field private static final FRAME_SIZE:I = 0xd

.field private static final FRAME_TIME:I = 0x14

.field private static final TAG:Ljava/lang/String;

.field private static final frameSizes:[S

.field private static final magicNum:[B


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 27
    const-class v0, Lcom/vlingo/sdk/internal/audio/AMRUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/audio/AMRUtil;->TAG:Ljava/lang/String;

    .line 36
    new-array v0, v3, [B

    sput-object v0, Lcom/vlingo/sdk/internal/audio/AMRUtil;->AMR_HEADER:[B

    .line 38
    sget-object v0, Lcom/vlingo/sdk/internal/audio/AMRUtil;->AMR_HEADER:[B

    const/4 v1, 0x0

    const/16 v2, 0x23

    aput-byte v2, v0, v1

    .line 39
    sget-object v0, Lcom/vlingo/sdk/internal/audio/AMRUtil;->AMR_HEADER:[B

    const/4 v1, 0x1

    const/16 v2, 0x21

    aput-byte v2, v0, v1

    .line 40
    sget-object v0, Lcom/vlingo/sdk/internal/audio/AMRUtil;->AMR_HEADER:[B

    const/4 v1, 0x2

    const/16 v2, 0x41

    aput-byte v2, v0, v1

    .line 41
    sget-object v0, Lcom/vlingo/sdk/internal/audio/AMRUtil;->AMR_HEADER:[B

    const/4 v1, 0x3

    const/16 v2, 0x4d

    aput-byte v2, v0, v1

    .line 42
    sget-object v0, Lcom/vlingo/sdk/internal/audio/AMRUtil;->AMR_HEADER:[B

    const/4 v1, 0x4

    const/16 v2, 0x52

    aput-byte v2, v0, v1

    .line 43
    sget-object v0, Lcom/vlingo/sdk/internal/audio/AMRUtil;->AMR_HEADER:[B

    const/4 v1, 0x5

    const/16 v2, 0xa

    aput-byte v2, v0, v1

    .line 97
    new-array v0, v3, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/vlingo/sdk/internal/audio/AMRUtil;->magicNum:[B

    .line 98
    const/16 v0, 0x10

    new-array v0, v0, [S

    fill-array-data v0, :array_1

    sput-object v0, Lcom/vlingo/sdk/internal/audio/AMRUtil;->frameSizes:[S

    return-void

    .line 97
    :array_0
    .array-data 1
        0x23t
        0x21t
        0x41t
        0x4dt
        0x52t
        0xat
    .end array-data

    .line 98
    nop

    :array_1
    .array-data 2
        0xcs
        0xds
        0xfs
        0x11s
        0x13s
        0x14s
        0x1as
        0x1fs
        0x5s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
        0x0s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addPaddingToAMR([BIII)[B
    .locals 8
    .param p0, "data"    # [B
    .param p1, "off"    # I
    .param p2, "len"    # I
    .param p3, "paddingMillis"    # I

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x0

    .line 62
    invoke-static {p0, p1, p2}, Lcom/vlingo/sdk/internal/audio/AMRUtil;->isAMRAudioOK([BII)Z

    move-result v0

    .line 63
    .local v0, "amrAudioOK":Z
    if-nez v0, :cond_0

    .line 83
    .end local p0    # "data":[B
    :goto_0
    return-object p0

    .line 69
    .restart local p0    # "data":[B
    :cond_0
    mul-int/lit8 v4, p3, 0xd

    div-int/lit8 v2, v4, 0x14

    .line 70
    .local v2, "iPaddedFrames":I
    mul-int/lit8 v4, v2, 0x2

    add-int/2addr v4, p2

    new-array v3, v4, [B

    .line 71
    .local v3, "paddedAMR":[B
    sget-object v4, Lcom/vlingo/sdk/internal/audio/AMRUtil;->AMR_HEADER:[B

    sget-object v5, Lcom/vlingo/sdk/internal/audio/AMRUtil;->AMR_HEADER:[B

    array-length v5, v5

    invoke-static {v4, v6, v3, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 73
    sget-object v4, Lcom/vlingo/sdk/internal/audio/AMRUtil;->AMR_HEADER:[B

    array-length v1, v4

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_1

    .line 74
    aput-byte v7, v3, v1

    .line 73
    add-int/lit8 v1, v1, 0xd

    goto :goto_1

    .line 77
    :cond_1
    sget-object v4, Lcom/vlingo/sdk/internal/audio/AMRUtil;->AMR_HEADER:[B

    array-length v4, v4

    add-int/2addr v4, p1

    sget-object v5, Lcom/vlingo/sdk/internal/audio/AMRUtil;->AMR_HEADER:[B

    array-length v5, v5

    add-int/2addr v5, v2

    sget-object v6, Lcom/vlingo/sdk/internal/audio/AMRUtil;->AMR_HEADER:[B

    array-length v6, v6

    sub-int v6, p2, v6

    invoke-static {p0, v4, v3, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 79
    sget-object v4, Lcom/vlingo/sdk/internal/audio/AMRUtil;->AMR_HEADER:[B

    array-length v4, v4

    add-int/2addr v4, v2

    add-int v1, v4, p2

    :goto_2
    array-length v4, v3

    if-ge v1, v4, :cond_2

    .line 80
    aput-byte v7, v3, v1

    .line 79
    add-int/lit8 v1, v1, 0xd

    goto :goto_2

    :cond_2
    move-object p0, v3

    .line 83
    goto :goto_0
.end method

.method public static isAMRAudioOK([BII)Z
    .locals 4
    .param p0, "data"    # [B
    .param p1, "off"    # I
    .param p2, "len"    # I

    .prologue
    .line 47
    if-eqz p0, :cond_0

    const/16 v2, 0xa

    if-ge p2, v2, :cond_2

    .line 48
    :cond_0
    const/4 v0, 0x0

    .line 58
    :cond_1
    :goto_0
    return v0

    .line 51
    :cond_2
    const/4 v0, 0x1

    .line 52
    .local v0, "containsHeader":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    sget-object v2, Lcom/vlingo/sdk/internal/audio/AMRUtil;->AMR_HEADER:[B

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 53
    sget-object v2, Lcom/vlingo/sdk/internal/audio/AMRUtil;->AMR_HEADER:[B

    aget-byte v2, v2, v1

    add-int v3, v1, p1

    aget-byte v3, p0, v3

    if-eq v2, v3, :cond_3

    .line 54
    const/4 v0, 0x0

    .line 55
    goto :goto_0

    .line 52
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static readInAMRMaxFrames(Ljava/io/InputStream;I)[B
    .locals 16
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "fakeAMRMilliseconds"    # I

    .prologue
    .line 102
    const/4 v11, -0x1

    .line 103
    .local v11, "maxFrames":I
    const/4 v14, 0x1

    move/from16 v0, p1

    if-ge v0, v14, :cond_0

    .line 104
    const/4 v4, 0x0

    .line 203
    :goto_0
    return-object v4

    .line 106
    :cond_0
    div-int/lit8 v11, p1, 0x14

    .line 109
    const/4 v7, 0x0

    .line 110
    .local v7, "fakeAMRLen":I
    const/4 v4, 0x0

    .line 112
    .local v4, "data":[B
    new-instance v1, Ljava/io/DataInputStream;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 117
    .local v1, "amrFile":Ljava/io/DataInputStream;
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v14, 0x1f4

    invoke-direct {v2, v14}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 118
    .local v2, "baos":Ljava/io/ByteArrayOutputStream;
    const/16 v14, 0x1f4

    new-array v3, v14, [B

    .line 122
    .local v3, "buf":[B
    const/4 v14, 0x6

    :try_start_0
    invoke-virtual {v1, v3, v7, v14}, Ljava/io/DataInputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v14

    add-int/2addr v7, v14

    .line 129
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    const/4 v14, 0x6

    if-ge v10, v14, :cond_2

    .line 130
    aget-byte v14, v3, v10

    sget-object v15, Lcom/vlingo/sdk/internal/audio/AMRUtil;->magicNum:[B

    aget-byte v15, v15, v10

    if-eq v14, v15, :cond_1

    .line 133
    const/4 v4, 0x0

    goto :goto_0

    .line 123
    .end local v10    # "i":I
    :catch_0
    move-exception v6

    .line 124
    .local v6, "e1":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    .line 125
    const/4 v4, 0x0

    goto :goto_0

    .line 129
    .end local v6    # "e1":Ljava/io/IOException;
    .restart local v10    # "i":I
    :cond_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 138
    :cond_2
    const/4 v14, 0x0

    const/4 v15, 0x6

    invoke-virtual {v2, v3, v14, v15}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 141
    const/4 v12, 0x0

    .line 143
    .local v12, "readInFrames":I
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataInputStream;->read()I

    move-result v9

    .line 144
    .local v9, "header":I
    const/4 v8, 0x0

    .line 145
    .local v8, "frameLen":I
    :cond_3
    :goto_2
    if-ge v12, v11, :cond_6

    const/4 v14, -0x1

    if-eq v9, v14, :cond_6

    .line 149
    int-to-byte v14, v9

    aput-byte v14, v3, v8

    .line 151
    add-int/lit8 v8, v8, 0x1

    .line 152
    shr-int/lit8 v14, v9, 0x3

    and-int/lit8 v9, v14, 0xf

    .line 155
    sget-object v14, Lcom/vlingo/sdk/internal/audio/AMRUtil;->frameSizes:[S

    aget-short v13, v14, v9

    .line 158
    .local v13, "size":S
    if-eqz v13, :cond_4

    .line 159
    invoke-virtual {v1, v3, v8, v13}, Ljava/io/DataInputStream;->read([BII)I

    move-result v14

    add-int/lit8 v8, v14, 0x1

    .line 160
    add-int/lit8 v12, v12, 0x1

    .line 170
    :cond_4
    const/4 v14, 0x0

    add-int/lit8 v15, v13, 0x1

    invoke-virtual {v2, v3, v14, v15}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 171
    add-int/2addr v7, v8

    .line 172
    const/4 v8, 0x0

    .line 174
    invoke-virtual {v1}, Ljava/io/DataInputStream;->read()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v9

    .line 175
    const/4 v14, -0x1

    if-ne v9, v14, :cond_5

    .line 179
    :cond_5
    if-lt v12, v11, :cond_3

    goto :goto_2

    .line 184
    .end local v8    # "frameLen":I
    .end local v9    # "header":I
    .end local v13    # "size":S
    :catch_1
    move-exception v5

    .line 185
    .local v5, "e":Ljava/io/IOException;
    sget-object v14, Lcom/vlingo/sdk/internal/audio/AMRUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v15, "AMR FILE READ EXCEPTION IO exception while reading in AMR file"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    .line 191
    .end local v5    # "e":Ljava/io/IOException;
    :cond_6
    :try_start_2
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    .line 192
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 193
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v4

    goto/16 :goto_0

    .line 194
    :catch_2
    move-exception v5

    .line 195
    .restart local v5    # "e":Ljava/io/IOException;
    sget-object v14, Lcom/vlingo/sdk/internal/audio/AMRUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v15, "AMR FILE CLOSE OR WRITE EXCEPTION AMR file: IO exception while closing or writing to internal buffer"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0
.end method
