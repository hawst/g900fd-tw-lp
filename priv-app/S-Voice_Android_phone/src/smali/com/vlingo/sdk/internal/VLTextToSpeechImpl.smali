.class public final Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;
.super Lcom/vlingo/sdk/internal/VLComponentImpl;
.source "VLTextToSpeechImpl.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/http/HttpCallback;
.implements Lcom/vlingo/sdk/tts/VLTextToSpeech;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$2;,
        Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;
    }
.end annotation


# instance fields
.field private mCurrentRequest:Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

.field private mFilename:Ljava/lang/String;

.field private mNotificationHandler:Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;

.field private mTextToSpeechListener:Lcom/vlingo/sdk/tts/VLTextToSpeechListener;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V
    .locals 1
    .param p1, "manager"    # Lcom/vlingo/sdk/internal/VLComponentManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/VLComponentImpl;-><init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V

    .line 92
    new-instance v0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;-><init>(Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;

    .line 93
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;)Lcom/vlingo/sdk/tts/VLTextToSpeechListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mTextToSpeechListener:Lcom/vlingo/sdk/tts/VLTextToSpeechListener;

    return-object v0
.end method

.method static synthetic access$002(Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;Lcom/vlingo/sdk/tts/VLTextToSpeechListener;)Lcom/vlingo/sdk/tts/VLTextToSpeechListener;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;
    .param p1, "x1"    # Lcom/vlingo/sdk/tts/VLTextToSpeechListener;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mTextToSpeechListener:Lcom/vlingo/sdk/tts/VLTextToSpeechListener;

    return-object p1
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mCurrentRequest:Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    return-object v0
.end method

.method static synthetic access$102(Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;)Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;
    .param p1, "x1"    # Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mCurrentRequest:Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    return-object p1
.end method

.method private static getTTSRequest(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;)Lcom/vlingo/sdk/internal/audio/TTSRequest;
    .locals 12
    .param p0, "request"    # Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 254
    invoke-virtual {p0}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->getType()Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    move-result-object v4

    .line 255
    .local v4, "type":Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;
    sget-object v8, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$2;->$SwitchMap$com$vlingo$sdk$tts$VLTextToSpeechRequest$Type:[I

    invoke-virtual {v4}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 266
    invoke-virtual {p0}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/vlingo/sdk/internal/audio/TTSRequest;->getText(Ljava/lang/String;)Lcom/vlingo/sdk/internal/audio/TTSRequest;

    move-result-object v3

    .line 271
    .local v3, "ttsRequest":Lcom/vlingo/sdk/internal/audio/TTSRequest;
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->getVoice()Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    move-result-object v5

    .line 272
    .local v5, "voice":Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;
    invoke-virtual {p0}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->getSpeechRate()Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    move-result-object v1

    .line 274
    .local v1, "rate":Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;
    sget-object v6, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;->MALE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    if-ne v5, v6, :cond_3

    const-string/jumbo v0, "Male"

    .line 276
    .local v0, "gender":Ljava/lang/String;
    :goto_1
    sget-object v6, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$2;->$SwitchMap$com$vlingo$sdk$tts$VLTextToSpeechRequest$SpeechRate:[I

    invoke-virtual {v1}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_1

    .line 282
    const-string/jumbo v2, "Normal"

    .line 285
    .local v2, "speechRate":Ljava/lang/String;
    :goto_2
    invoke-virtual {v3, v0}, Lcom/vlingo/sdk/internal/audio/TTSRequest;->setGender(Ljava/lang/String;)V

    .line 286
    invoke-virtual {p0}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->getMaxWords()I

    move-result v6

    invoke-virtual {v3, v6}, Lcom/vlingo/sdk/internal/audio/TTSRequest;->setWordLimit(I)V

    .line 287
    invoke-virtual {v3, v2}, Lcom/vlingo/sdk/internal/audio/TTSRequest;->setSpeechRate(Ljava/lang/String;)V

    .line 289
    return-object v3

    .line 257
    .end local v0    # "gender":Ljava/lang/String;
    .end local v1    # "rate":Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;
    .end local v2    # "speechRate":Ljava/lang/String;
    .end local v3    # "ttsRequest":Lcom/vlingo/sdk/internal/audio/TTSRequest;
    .end local v5    # "voice":Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;
    :pswitch_0
    invoke-virtual {p0}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->getMsgFrom()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->getMsgSubject()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->getText()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->getMsgReadyBody()Z

    move-result v11

    if-nez v11, :cond_0

    :goto_3
    invoke-static {v8, v9, v10, v6}, Lcom/vlingo/sdk/internal/audio/TTSRequest;->getEmail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/vlingo/sdk/internal/audio/TTSRequest;

    move-result-object v3

    .line 258
    .restart local v3    # "ttsRequest":Lcom/vlingo/sdk/internal/audio/TTSRequest;
    goto :goto_0

    .end local v3    # "ttsRequest":Lcom/vlingo/sdk/internal/audio/TTSRequest;
    :cond_0
    move v6, v7

    .line 257
    goto :goto_3

    .line 260
    :pswitch_1
    invoke-virtual {p0}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->getMsgFrom()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->getText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->getMsgReadyBody()Z

    move-result v10

    if-nez v10, :cond_1

    :goto_4
    invoke-static {v8, v9, v6}, Lcom/vlingo/sdk/internal/audio/TTSRequest;->getSMS(Ljava/lang/String;Ljava/lang/String;Z)Lcom/vlingo/sdk/internal/audio/TTSRequest;

    move-result-object v3

    .line 261
    .restart local v3    # "ttsRequest":Lcom/vlingo/sdk/internal/audio/TTSRequest;
    goto :goto_0

    .end local v3    # "ttsRequest":Lcom/vlingo/sdk/internal/audio/TTSRequest;
    :cond_1
    move v6, v7

    .line 260
    goto :goto_4

    .line 263
    :pswitch_2
    invoke-virtual {p0}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->getMsgFrom()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->getMsgSubject()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->getMsgReadyBody()Z

    move-result v10

    if-nez v10, :cond_2

    :goto_5
    invoke-static {v8, v9, v6}, Lcom/vlingo/sdk/internal/audio/TTSRequest;->getMMS(Ljava/lang/String;Ljava/lang/String;Z)Lcom/vlingo/sdk/internal/audio/TTSRequest;

    move-result-object v3

    .line 264
    .restart local v3    # "ttsRequest":Lcom/vlingo/sdk/internal/audio/TTSRequest;
    goto :goto_0

    .end local v3    # "ttsRequest":Lcom/vlingo/sdk/internal/audio/TTSRequest;
    :cond_2
    move v6, v7

    .line 263
    goto :goto_5

    .line 274
    .restart local v1    # "rate":Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;
    .restart local v3    # "ttsRequest":Lcom/vlingo/sdk/internal/audio/TTSRequest;
    .restart local v5    # "voice":Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;
    :cond_3
    const-string/jumbo v0, "Female"

    goto :goto_1

    .line 277
    .restart local v0    # "gender":Ljava/lang/String;
    :pswitch_3
    const-string/jumbo v2, "VerySlow"

    .restart local v2    # "speechRate":Ljava/lang/String;
    goto :goto_2

    .line 278
    .end local v2    # "speechRate":Ljava/lang/String;
    :pswitch_4
    const-string/jumbo v2, "Slow"

    .restart local v2    # "speechRate":Ljava/lang/String;
    goto :goto_2

    .line 279
    .end local v2    # "speechRate":Ljava/lang/String;
    :pswitch_5
    const-string/jumbo v2, "Fast"

    .restart local v2    # "speechRate":Ljava/lang/String;
    goto :goto_2

    .line 280
    .end local v2    # "speechRate":Ljava/lang/String;
    :pswitch_6
    const-string/jumbo v2, "VeryFast"

    .restart local v2    # "speechRate":Ljava/lang/String;
    goto :goto_2

    .line 255
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 276
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 146
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->validateInstance()V

    .line 148
    monitor-enter p0

    .line 149
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mCurrentRequest:Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mCurrentRequest:Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;->cancel()V

    .line 151
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mCurrentRequest:Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    .line 153
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    iput-object v1, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mTextToSpeechListener:Lcom/vlingo/sdk/tts/VLTextToSpeechListener;

    .line 155
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;->cancelNotifications()V

    .line 156
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->setBusy(Z)V

    .line 157
    return-void

    .line 153
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public bridge synthetic destroy()V
    .locals 0

    .prologue
    .line 30
    invoke-super {p0}, Lcom/vlingo/sdk/internal/VLComponentImpl;->destroy()V

    return-void
.end method

.method public doesDependOnSettings()Z
    .locals 1

    .prologue
    .line 295
    const/4 v0, 0x1

    return v0
.end method

.method public getSupportedLanguageList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->validateInstance()V

    .line 165
    sget-object v0, Lcom/vlingo/sdk/internal/settings/Settings;->SUPPORTED_LANGUAGES:[Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic isValid()Z
    .locals 1

    .prologue
    .line 30
    invoke-super {p0}, Lcom/vlingo/sdk/internal/VLComponentImpl;->isValid()Z

    move-result v0

    return v0
.end method

.method public declared-synchronized onCancelled(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 243
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method onDestroy()V
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mTextToSpeechListener:Lcom/vlingo/sdk/tts/VLTextToSpeechListener;

    .line 105
    return-void
.end method

.method public declared-synchronized onFailure(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 233
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mCurrentRequest:Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq p1, v0, :cond_0

    .line 241
    :goto_0
    monitor-exit p0

    return-void

    .line 237
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mCurrentRequest:Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    .line 240
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;

    sget-object v1, Lcom/vlingo/sdk/tts/VLTextToSpeechErrors;->ERROR_NETWORK:Lcom/vlingo/sdk/tts/VLTextToSpeechErrors;

    const-string/jumbo v2, "Network error"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;->notifyError(Lcom/vlingo/sdk/tts/VLTextToSpeechErrors;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 233
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onResponse(Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpResponse;)V
    .locals 5
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;
    .param p2, "response"    # Lcom/vlingo/sdk/internal/http/HttpResponse;

    .prologue
    .line 178
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mCurrentRequest:Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq p1, v2, :cond_0

    .line 212
    :goto_0
    monitor-exit p0

    return-void

    .line 182
    :cond_0
    const/4 v2, 0x0

    :try_start_1
    iput-object v2, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mCurrentRequest:Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    .line 185
    iget v2, p2, Lcom/vlingo/sdk/internal/http/HttpResponse;->responseCode:I

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_1

    .line 186
    invoke-static {p2}, Lcom/vlingo/sdk/internal/audio/TTSServerManager;->parseTTSResponse(Lcom/vlingo/sdk/internal/http/HttpResponse;)[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 187
    .local v0, "audioData":[B
    if-eqz v0, :cond_1

    .line 189
    :try_start_2
    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mFilename:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 190
    .local v1, "bos":Ljava/io/BufferedOutputStream;
    const/4 v2, 0x0

    array-length v3, v0

    invoke-virtual {v1, v0, v2, v3}, Ljava/io/BufferedOutputStream;->write([BII)V

    .line 191
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->flush()V

    .line 192
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V

    .line 193
    const/4 v1, 0x0

    .line 194
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;->notifySuccess()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 198
    .end local v1    # "bos":Ljava/io/BufferedOutputStream;
    :catch_0
    move-exception v2

    .line 203
    :try_start_3
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;

    sget-object v3, Lcom/vlingo/sdk/tts/VLTextToSpeechErrors;->ERROR_CLIENT:Lcom/vlingo/sdk/tts/VLTextToSpeechErrors;

    const-string/jumbo v4, "Error creating/writing to file"

    invoke-virtual {v2, v3, v4}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;->notifyError(Lcom/vlingo/sdk/tts/VLTextToSpeechErrors;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 178
    .end local v0    # "audioData":[B
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 211
    :cond_1
    :try_start_4
    iget-object v2, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;

    sget-object v3, Lcom/vlingo/sdk/tts/VLTextToSpeechErrors;->ERROR_SERVER:Lcom/vlingo/sdk/tts/VLTextToSpeechErrors;

    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/http/HttpResponse;->getDataAsString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;->notifyError(Lcom/vlingo/sdk/tts/VLTextToSpeechErrors;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized onTimeout(Lcom/vlingo/sdk/internal/http/HttpRequest;)Z
    .locals 4
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    const/4 v3, 0x1

    .line 218
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mCurrentRequest:Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq p1, v0, :cond_0

    .line 226
    :goto_0
    monitor-exit p0

    return v3

    .line 222
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mCurrentRequest:Lcom/vlingo/sdk/internal/vlservice/VLHttpServiceRequest;

    .line 225
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;

    sget-object v1, Lcom/vlingo/sdk/tts/VLTextToSpeechErrors;->ERROR_NETWORK_TIMEOUT:Lcom/vlingo/sdk/tts/VLTextToSpeechErrors;

    const-string/jumbo v2, "Timeout waiting for request"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$NotificationHandler;->notifyError(Lcom/vlingo/sdk/tts/VLTextToSpeechErrors;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 218
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onWillExecuteRequest(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 172
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public synthesizeToFile(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;Ljava/lang/String;Lcom/vlingo/sdk/tts/VLTextToSpeechListener;)V
    .locals 4
    .param p1, "request"    # Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;
    .param p2, "filename"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/vlingo/sdk/tts/VLTextToSpeechListener;

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->validateInstance()V

    .line 114
    if-nez p1, :cond_0

    .line 115
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "request cannot be null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 117
    :cond_0
    invoke-static {p2}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 118
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "filename cannot be null or empty"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 120
    :cond_1
    if-nez p3, :cond_2

    .line 121
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "listener must be specifed"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 123
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->isBusy()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 124
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "TextToSpeech request already in progress"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 127
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->setBusy(Z)V

    .line 128
    iput-object p3, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mTextToSpeechListener:Lcom/vlingo/sdk/tts/VLTextToSpeechListener;

    .line 129
    iput-object p2, p0, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->mFilename:Ljava/lang/String;

    .line 131
    invoke-static {p1}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->getTTSRequest(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;)Lcom/vlingo/sdk/internal/audio/TTSRequest;

    move-result-object v1

    .line 132
    .local v1, "ttsRequest":Lcom/vlingo/sdk/internal/audio/TTSRequest;
    invoke-virtual {p1}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 133
    .local v0, "language":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;->getHandler()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$1;

    invoke-direct {v3, p0, v1, v0}, Lcom/vlingo/sdk/internal/VLTextToSpeechImpl$1;-><init>(Lcom/vlingo/sdk/internal/VLTextToSpeechImpl;Lcom/vlingo/sdk/internal/audio/TTSRequest;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 139
    return-void
.end method
