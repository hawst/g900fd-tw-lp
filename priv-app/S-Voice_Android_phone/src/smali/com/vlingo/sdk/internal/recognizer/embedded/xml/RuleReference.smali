.class public Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;
.source "RuleReference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference$1;,
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference$RuleBuilder;
    }
.end annotation


# static fields
.field public static final REFERENCE_TAG_NAME:Ljava/lang/String; = "Reference"

.field public static final TEMPLATE_PARAM_NAME:Ljava/lang/String; = "templateName"


# instance fields
.field private templateName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;-><init>()V

    .line 27
    return-void
.end method

.method static synthetic access$102(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 10
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference;->templateName:Ljava/lang/String;

    return-object p1
.end method

.method public static getBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference$RuleBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference$RuleBuilder;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference$1;)V

    return-object v0
.end method


# virtual methods
.method public getTemplateName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/RuleReference;->templateName:Ljava/lang/String;

    return-object v0
.end method
