.class public Lcom/vlingo/sdk/internal/deviceinfo/CountryUtils;
.super Ljava/lang/Object;
.source "CountryUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static mISO2ToISO3CountryCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "iso2Code"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-static {}, Lcom/vlingo/sdk/internal/deviceinfo/CountryCodes;->getInstance()Lcom/vlingo/sdk/internal/deviceinfo/CountryCodes;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/vlingo/sdk/internal/deviceinfo/CountryCodes;->getIso3FromIso2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 17
    .local v0, "iso3Code":Ljava/lang/String;
    return-object v0
.end method

.method public static mISO3ToISO2CountryCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "iso3Code"    # Ljava/lang/String;

    .prologue
    .line 11
    invoke-static {}, Lcom/vlingo/sdk/internal/deviceinfo/CountryCodes;->getInstance()Lcom/vlingo/sdk/internal/deviceinfo/CountryCodes;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/vlingo/sdk/internal/deviceinfo/CountryCodes;->getIso2FromIso3(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 12
    .local v0, "iso2Code":Ljava/lang/String;
    return-object v0
.end method
