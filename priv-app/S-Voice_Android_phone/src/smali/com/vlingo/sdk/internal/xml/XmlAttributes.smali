.class public Lcom/vlingo/sdk/internal/xml/XmlAttributes;
.super Ljava/lang/Object;
.source "XmlAttributes.java"


# static fields
.field public static final ATTRIBUTE_UNDEF:B


# instance fields
.field private attributeList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/sdk/internal/xml/XmlAttribute;",
            ">;"
        }
    .end annotation
.end field

.field private xmlAttributes:Lcom/vlingo/sdk/internal/util/ToIntHashtable;


# direct methods
.method public constructor <init>(ILcom/vlingo/sdk/internal/util/ToIntHashtable;)V
    .locals 1
    .param p1, "initialCapacity"    # I
    .param p2, "xmlAttributes"    # Lcom/vlingo/sdk/internal/util/ToIntHashtable;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p2, p0, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->xmlAttributes:Lcom/vlingo/sdk/internal/util/ToIntHashtable;

    .line 37
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0, p1}, Ljava/util/Vector;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    .line 38
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/sdk/internal/util/ToIntHashtable;)V
    .locals 1
    .param p1, "xmlAttributes"    # Lcom/vlingo/sdk/internal/util/ToIntHashtable;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->xmlAttributes:Lcom/vlingo/sdk/internal/util/ToIntHashtable;

    .line 45
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    .line 46
    return-void
.end method


# virtual methods
.method public add(BLjava/lang/String;)V
    .locals 2
    .param p1, "type"    # B
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    new-instance v1, Lcom/vlingo/sdk/internal/xml/XmlAttribute;

    invoke-direct {v1, p1, p2}, Lcom/vlingo/sdk/internal/xml/XmlAttribute;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 54
    return-void
.end method

.method public getAttribute(I)Lcom/vlingo/sdk/internal/xml/XmlAttribute;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 115
    iget-object v0, p0, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/xml/XmlAttribute;

    return-object v0
.end method

.method public getAttributeType([CII)B
    .locals 2
    .param p1, "data"    # [C
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 60
    invoke-static {p1, p2, p3}, Ljava/lang/String;->valueOf([CII)Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, "name":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->xmlAttributes:Lcom/vlingo/sdk/internal/util/ToIntHashtable;

    invoke-interface {v1, v0}, Lcom/vlingo/sdk/internal/util/ToIntHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 63
    iget-object v1, p0, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->xmlAttributes:Lcom/vlingo/sdk/internal/util/ToIntHashtable;

    invoke-interface {v1, v0}, Lcom/vlingo/sdk/internal/util/ToIntHashtable;->get(Ljava/lang/Object;)I

    move-result v1

    int-to-byte v1, v1

    .line 65
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public getType(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 79
    iget-object v1, p0, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 80
    iget-object v1, p0, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/xml/XmlAttribute;

    .line 81
    .local v0, "attribute":Lcom/vlingo/sdk/internal/xml/XmlAttribute;
    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/xml/XmlAttribute;->getType()I

    move-result v1

    .line 85
    .end local v0    # "attribute":Lcom/vlingo/sdk/internal/xml/XmlAttribute;
    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getValue(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 92
    iget-object v1, p0, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/xml/XmlAttribute;

    .line 94
    .local v0, "attribute":Lcom/vlingo/sdk/internal/xml/XmlAttribute;
    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/xml/XmlAttribute;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 98
    .end local v0    # "attribute":Lcom/vlingo/sdk/internal/xml/XmlAttribute;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public lookup(I)Ljava/lang/String;
    .locals 3
    .param p1, "key"    # I

    .prologue
    .line 103
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 104
    iget-object v2, p0, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->attributeList:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/xml/XmlAttribute;

    .line 105
    .local v0, "attribute":Lcom/vlingo/sdk/internal/xml/XmlAttribute;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/xml/XmlAttribute;->getType()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 106
    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/xml/XmlAttribute;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 108
    .end local v0    # "attribute":Lcom/vlingo/sdk/internal/xml/XmlAttribute;
    :goto_1
    return-object v2

    .line 103
    .restart local v0    # "attribute":Lcom/vlingo/sdk/internal/xml/XmlAttribute;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 108
    .end local v0    # "attribute":Lcom/vlingo/sdk/internal/xml/XmlAttribute;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method
