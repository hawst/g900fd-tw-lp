.class public abstract Lcom/vlingo/sdk/internal/vlservice/VLServiceUtil;
.super Ljava/lang/Object;
.source "VLServiceUtil.java"


# static fields
.field public static final CHINESE_APP_CHANNEL:Ljava/lang/String; = "Preinstall Free China"

.field public static final EURO_APP_CHANNEL:Ljava/lang/String; = "Preinstall Free"

.field public static final VLINGO_PROTOCOL_VERSION:Ljava/lang/String; = "3.8"

.field static final XVL_PROTOCOL_HEADER_VALUE:Ljava/lang/String; = "Version=3.8; ResponseEncoding="


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addLocationHeader(Ljava/util/Hashtable;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;)V
    .locals 5
    .param p1, "client"    # Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/vlingo/sdk/internal/recognizer/ClientMeta;",
            ")V"
        }
    .end annotation

    .prologue
    .line 307
    .local p0, "headers":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/sdk/VLSdk;->appAllowsLocationSending()Z

    move-result v3

    if-nez v3, :cond_1

    .line 328
    :cond_0
    :goto_0
    return-void

    .line 310
    :cond_1
    const-string/jumbo v3, "obey_device_location_settings"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/vlingo/sdk/internal/settings/Settings;->getPersistentBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 311
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 312
    .local v0, "context":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 313
    const-string/jumbo v3, "location"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/LocationManager;

    .line 314
    .local v1, "lm":Landroid/location/LocationManager;
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getLocation()Ljava/lang/String;

    move-result-object v2

    .line 315
    .local v2, "location":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    const-string/jumbo v3, "gps"

    invoke-virtual {v1, v3}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 316
    const-string/jumbo v3, "X-vllocation"

    invoke-virtual {p0, v3, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 320
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "lm":Landroid/location/LocationManager;
    .end local v2    # "location":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/sdk/VLSdk;->getLocationOn()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/sdk/VLSdk;->appAllowsLocationSending()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 321
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getLocation()Ljava/lang/String;

    move-result-object v2

    .line 322
    .restart local v2    # "location":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 323
    const-string/jumbo v3, "X-vllocation"

    invoke-virtual {p0, v3, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static addStandardVlingoHttpHeaders(Ljava/util/Hashtable;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Lcom/vlingo/sdk/internal/recognizer/SRContext;)V
    .locals 1
    .param p1, "client"    # Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    .param p2, "software"    # Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;
    .param p3, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/vlingo/sdk/internal/recognizer/ClientMeta;",
            "Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;",
            "Lcom/vlingo/sdk/internal/recognizer/SRContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 77
    .local p0, "headers":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0, p3}, Lcom/vlingo/sdk/internal/vlservice/VLServiceUtil;->addStandardVlingoHttpHeaders(Ljava/util/Hashtable;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/SRContext;)V

    .line 78
    return-void
.end method

.method public static addStandardVlingoHttpHeaders(Ljava/util/Hashtable;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/SRContext;)V
    .locals 38
    .param p1, "client"    # Lcom/vlingo/sdk/internal/recognizer/ClientMeta;
    .param p2, "software"    # Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;
    .param p3, "language"    # Ljava/lang/String;
    .param p4, "srContext"    # Lcom/vlingo/sdk/internal/recognizer/SRContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/vlingo/sdk/internal/recognizer/ClientMeta;",
            "Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/SRContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 89
    .local p0, "headers":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v35, "Cache-Control"

    const-string/jumbo v36, "no-cache,no-store,max-age=0,no-transform"

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    const-string/jumbo v35, "X-vlprotocol"

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v35

    if-nez v35, :cond_0

    .line 92
    const-string/jumbo v35, "text/xml"

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/vlservice/VLServiceUtil;->setProtocolHeader(Ljava/util/Hashtable;Ljava/lang/String;)V

    .line 95
    :cond_0
    if-eqz p4, :cond_12

    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getDMHeaderKVPairs()Ljava/util/HashMap;

    move-result-object v12

    .line 98
    .local v12, "extraHeaders":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_0
    invoke-static {}, Lcom/vlingo/sdk/internal/util/Screen;->getInstance()Lcom/vlingo/sdk/internal/util/Screen;

    move-result-object v22

    .line 99
    .local v22, "screen":Lcom/vlingo/sdk/internal/util/Screen;
    const-string/jumbo v35, "screen.maxWidth"

    move-object/from16 v0, v35

    invoke-virtual {v12, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_13

    const-string/jumbo v35, "screen.maxWidth"

    move-object/from16 v0, v35

    invoke-virtual {v12, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/lang/String;

    move-object/from16 v17, v35

    .line 101
    .local v17, "maxWidth":Ljava/lang/String;
    :goto_1
    const-string/jumbo v35, "screen.plotWidth"

    move-object/from16 v0, v35

    invoke-virtual {v12, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_14

    const-string/jumbo v35, "screen.plotWidth"

    move-object/from16 v0, v35

    invoke-virtual {v12, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/lang/String;

    move-object/from16 v19, v35

    .line 103
    .local v19, "plotWidth":Ljava/lang/String;
    :goto_2
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    .local v9, "displayParameters":Ljava/lang/StringBuilder;
    invoke-virtual/range {v22 .. v22}, Lcom/vlingo/sdk/internal/util/Screen;->getWidth()I

    move-result v35

    if-lez v35, :cond_1

    .line 105
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "Width="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string/jumbo v35, "screen.width"

    move-object/from16 v0, v35

    invoke-virtual {v12, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_15

    const-string/jumbo v35, "screen.width"

    move-object/from16 v0, v35

    invoke-virtual {v12, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/lang/String;

    :goto_3
    move-object/from16 v0, v36

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    const-string/jumbo v35, "screen.width"

    move-object/from16 v0, v35

    invoke-virtual {v12, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    :cond_1
    invoke-static {}, Lcom/vlingo/sdk/internal/util/Screen;->getMagnification()F

    move-result v35

    const/16 v36, 0x0

    cmpl-float v35, v35, v36

    if-lez v35, :cond_2

    .line 109
    invoke-static {v9}, Lcom/vlingo/sdk/internal/vlservice/VLServiceUtil;->updateWithDivider(Ljava/lang/StringBuilder;)V

    .line 110
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "Mag="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string/jumbo v35, "screen.magnification"

    move-object/from16 v0, v35

    invoke-virtual {v12, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_16

    const-string/jumbo v35, "screen.magnification"

    move-object/from16 v0, v35

    invoke-virtual {v12, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/lang/String;

    :goto_4
    move-object/from16 v0, v36

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    const-string/jumbo v35, "screen.magnification"

    move-object/from16 v0, v35

    invoke-virtual {v12, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    :cond_2
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v35

    if-lez v35, :cond_3

    .line 115
    invoke-static {v9}, Lcom/vlingo/sdk/internal/vlservice/VLServiceUtil;->updateWithDivider(Ljava/lang/StringBuilder;)V

    .line 116
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "MaxWidth="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    const-string/jumbo v35, "screen.maxWidth"

    move-object/from16 v0, v35

    invoke-virtual {v12, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    :cond_3
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v35

    if-lez v35, :cond_4

    .line 120
    invoke-static {v9}, Lcom/vlingo/sdk/internal/vlservice/VLServiceUtil;->updateWithDivider(Ljava/lang/StringBuilder;)V

    .line 121
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "PlotWidth="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    const-string/jumbo v35, "screen.plotWidth"

    move-object/from16 v0, v35

    invoke-virtual {v12, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    :cond_4
    const-string/jumbo v35, "X-vldisplaygeometry"

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    const-string/jumbo v35, "tos_accepted_date"

    const/16 v36, 0x0

    invoke-static/range {v35 .. v36}, Lcom/vlingo/sdk/internal/settings/Settings;->getPersistentString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 128
    .local v27, "tosAcceptedDate":Ljava/lang/String;
    if-eqz v27, :cond_5

    .line 129
    const-string/jumbo v35, "x-vleula"

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v37, "tos_accepted_date="

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    :cond_5
    invoke-static/range {p3 .. p3}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v35

    if-eqz v35, :cond_17

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getLanguage()Ljava/lang/String;

    move-result-object v16

    .line 139
    .local v16, "languageLocale":Ljava/lang/String;
    :goto_5
    new-instance v21, Ljava/lang/StringBuffer;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuffer;-><init>()V

    .line 140
    .local v21, "sb":Ljava/lang/StringBuffer;
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "DeviceMake="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getDeviceMake()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v21

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 141
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "; DeviceOSName="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getDeviceOSName()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v21

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 142
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "; DeviceModel="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getDeviceModel()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v21

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 143
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "; DeviceOS="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getDeviceOS()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v21

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 144
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "; Language="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v21

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 145
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "; ConnectionType="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-static {}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getInstance()Lcom/vlingo/sdk/internal/net/ConnectionManager;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->getCurrentConnectionType()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v21

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 146
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "; Carrier="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getCarrier()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v21

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 147
    if-eqz v27, :cond_6

    .line 148
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getCarrierCountry()Ljava/lang/String;

    move-result-object v3

    .line 149
    .local v3, "carrierCountry":Ljava/lang/String;
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v35

    if-lez v35, :cond_6

    .line 150
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "; CarrierCountry="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v21

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 154
    .end local v3    # "carrierCountry":Ljava/lang/String;
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getDeviceID()Ljava/lang/String;

    move-result-object v6

    .line 155
    .local v6, "deviceId":Ljava/lang/String;
    if-eqz v6, :cond_7

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v35

    if-lez v35, :cond_7

    .line 156
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "; DeviceID="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v21

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 168
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getPhoneNumber()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->length()I

    move-result v35

    if-lez v35, :cond_8

    .line 169
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "; PhoneNumber="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getPhoneNumber()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v21

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 172
    :cond_8
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "; AudioDevice="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-static {}, Lcom/vlingo/sdk/internal/audio/AudioDevice;->getInstance()Lcom/vlingo/sdk/internal/audio/AudioDevice;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Lcom/vlingo/sdk/internal/audio/AudioDevice;->getAudioDeviceName()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v21

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 173
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v30

    .line 175
    .local v30, "vlclient":Ljava/lang/String;
    const-string/jumbo v35, "X-vlclient"

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "Name="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {p2 .. p2}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getName()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string/jumbo v36, "; Version="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {p2 .. p2}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getVersion()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    .line 185
    .local v32, "xvlSoftware":Ljava/lang/String;
    const-string/jumbo v4, ""

    .line 186
    .local v4, "channel":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->isChinesePhone()Z

    move-result v35

    if-eqz v35, :cond_18

    .line 187
    const-string/jumbo v4, "Preinstall Free China"

    .line 191
    :goto_6
    if-eqz v4, :cond_9

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v35

    if-lez v35, :cond_9

    .line 192
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v35

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string/jumbo v36, "; AppChannel="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    .line 196
    :cond_9
    const-string/jumbo v35, "X-vlsoftware"

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "Name="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {p2 .. p2}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getSdkName()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string/jumbo v36, "; Version="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {p2 .. p2}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getSdkVersion()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    .line 200
    .local v31, "xvlSdk":Ljava/lang/String;
    const-string/jumbo v35, "X-vlsdk"

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    invoke-static/range {p0 .. p1}, Lcom/vlingo/sdk/internal/vlservice/VLServiceUtil;->addLocationHeader(Ljava/util/Hashtable;Lcom/vlingo/sdk/internal/recognizer/ClientMeta;)V

    .line 203
    const-string/jumbo v35, "X-vllocation"

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v35

    if-nez v35, :cond_a

    const-string/jumbo v35, "isGPSDisabled"

    move-object/from16 v0, v35

    invoke-virtual {v12, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v35

    if-nez v35, :cond_a

    .line 204
    const-string/jumbo v35, "isGPSDisabled"

    const-string/jumbo v36, "true"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-virtual {v12, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    :cond_a
    const-string/jumbo v35, "X-vlsr"

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v37, "AppID="

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {p2 .. p2}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getAppId()Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    const-string/jumbo v35, "Content-Type"

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v35

    if-nez v35, :cond_b

    .line 211
    const-string/jumbo v35, "Content-Type"

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v37, "application/octet-stream;boundary="

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    sget-object v37, Lcom/vlingo/sdk/internal/recognizer/sr3/HttpConnectionAdapter;->ivBoundary:Ljava/lang/String;

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    :cond_b
    const-string/jumbo v35, "Accept-Charset"

    const-string/jumbo v36, "utf-8,ISO-8859-1;q=0.5,*;q=0.5"

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    const/16 v35, 0x0

    const/16 v36, 0x2

    move-object/from16 v0, v16

    move/from16 v1, v35

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    .line 216
    .local v15, "lang":Ljava/lang/String;
    const-string/jumbo v35, "Accept-Language"

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v36

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string/jumbo v37, ","

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string/jumbo v37, ";q=0.7,en;q=0.5"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    if-eqz p4, :cond_1b

    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->isDMRequest()Z

    move-result v35

    if-eqz v35, :cond_1b

    .line 220
    const-string/jumbo v35, "X-vvs"

    const-string/jumbo v36, "Version=2.0"

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    new-instance v23, Ljava/text/SimpleDateFormat;

    const-string/jumbo v35, "yyyy-MM-dd"

    sget-object v36, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v23

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 226
    .local v23, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "Date="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    new-instance v36, Ljava/util/Date;

    invoke-direct/range {v36 .. v36}, Ljava/util/Date;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    .line 227
    .local v33, "xvlclientdate":Ljava/lang/String;
    const-string/jumbo v35, "X-vlclientdate"

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    new-instance v24, Ljava/text/SimpleDateFormat;

    const-string/jumbo v35, "yyyy-MM-dd HH:mm:ss zzz"

    sget-object v36, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v24

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 231
    .local v24, "sdf2":Ljava/text/SimpleDateFormat;
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    .line 234
    .local v34, "xvldm":Ljava/lang/StringBuilder;
    const-string/jumbo v35, "ClientTime="

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    new-instance v36, Ljava/util/Date;

    invoke-direct/range {v36 .. v36}, Ljava/util/Date;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getUsername()Ljava/lang/String;

    move-result-object v28

    .line 238
    .local v28, "username":Ljava/lang/String;
    if-nez v28, :cond_c

    .line 239
    const-string/jumbo v28, ""

    .line 241
    :cond_c
    const-string/jumbo v35, "(;|=)+"

    const-string/jumbo v36, ""

    move-object/from16 v0, v28

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 242
    invoke-static/range {v28 .. v28}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v35

    if-nez v35, :cond_d

    .line 243
    const-string/jumbo v35, "; UserName="

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    :cond_d
    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getDialogGUID()Ljava/lang/String;

    move-result-object v7

    .line 248
    .local v7, "dialogGUID":Ljava/lang/String;
    invoke-static {v7}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v35

    if-nez v35, :cond_e

    .line 249
    const-string/jumbo v35, "; DialogGUID="

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    :cond_e
    invoke-virtual/range {p4 .. p4}, Lcom/vlingo/sdk/internal/recognizer/SRContext;->getDialogTurnNumber()I

    move-result v8

    .line 254
    .local v8, "dialogTurn":I
    const/16 v35, -0x1

    move/from16 v0, v35

    if-le v8, v0, :cond_f

    .line 255
    const-string/jumbo v35, "; TurnNumber="

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 259
    :cond_f
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Lcom/vlingo/sdk/VLSdk;->getDebugSettings()Lcom/vlingo/sdk/util/SDKDebugSettings;

    move-result-object v10

    .line 260
    .local v10, "ds":Lcom/vlingo/sdk/util/SDKDebugSettings;
    if-eqz v10, :cond_10

    invoke-virtual {v10}, Lcom/vlingo/sdk/util/SDKDebugSettings;->isForceNonDM()Z

    move-result v35

    if-eqz v35, :cond_10

    .line 261
    const-string/jumbo v35, "; DisableDM=true"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    :cond_10
    const-string/jumbo v35, "; Use24HourTime="

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v36

    invoke-static/range {v36 .. v36}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 267
    const-string/jumbo v35, "X-vldm"

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 272
    .local v5, "customHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/StringBuilder;>;"
    invoke-virtual {v12}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v35

    invoke-interface/range {v35 .. v35}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_7
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v35

    if-eqz v35, :cond_1a

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 275
    .local v14, "key":Ljava/lang/String;
    const-string/jumbo v35, "#"

    move-object/from16 v0, v35

    invoke-virtual {v14, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_19

    .line 276
    const-string/jumbo v35, "#"

    move-object/from16 v0, v35

    invoke-virtual {v14, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v25

    .line 277
    .local v25, "splitted":[Ljava/lang/String;
    const/16 v35, 0x0

    aget-object v20, v25, v35

    .line 278
    .local v20, "prefix":Ljava/lang/String;
    const/16 v35, 0x1

    aget-object v18, v25, v35

    .line 283
    .end local v25    # "splitted":[Ljava/lang/String;
    .local v18, "name":Ljava/lang/String;
    :goto_8
    move-object/from16 v0, v20

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/StringBuilder;

    .line 284
    .local v26, "stringBuilder":Ljava/lang/StringBuilder;
    if-nez v26, :cond_11

    .line 285
    new-instance v26, Ljava/lang/StringBuilder;

    .end local v26    # "stringBuilder":Ljava/lang/StringBuilder;
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    .line 287
    .restart local v26    # "stringBuilder":Ljava/lang/StringBuilder;
    :cond_11
    move-object/from16 v0, v26

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string/jumbo v36, "="

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual {v12, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/lang/String;

    move-object/from16 v0, v36

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string/jumbo v36, "; "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 288
    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7

    .line 95
    .end local v4    # "channel":Ljava/lang/String;
    .end local v5    # "customHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/StringBuilder;>;"
    .end local v6    # "deviceId":Ljava/lang/String;
    .end local v7    # "dialogGUID":Ljava/lang/String;
    .end local v8    # "dialogTurn":I
    .end local v9    # "displayParameters":Ljava/lang/StringBuilder;
    .end local v10    # "ds":Lcom/vlingo/sdk/util/SDKDebugSettings;
    .end local v12    # "extraHeaders":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v14    # "key":Ljava/lang/String;
    .end local v15    # "lang":Ljava/lang/String;
    .end local v16    # "languageLocale":Ljava/lang/String;
    .end local v17    # "maxWidth":Ljava/lang/String;
    .end local v18    # "name":Ljava/lang/String;
    .end local v19    # "plotWidth":Ljava/lang/String;
    .end local v20    # "prefix":Ljava/lang/String;
    .end local v21    # "sb":Ljava/lang/StringBuffer;
    .end local v22    # "screen":Lcom/vlingo/sdk/internal/util/Screen;
    .end local v23    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v24    # "sdf2":Ljava/text/SimpleDateFormat;
    .end local v26    # "stringBuilder":Ljava/lang/StringBuilder;
    .end local v27    # "tosAcceptedDate":Ljava/lang/String;
    .end local v28    # "username":Ljava/lang/String;
    .end local v30    # "vlclient":Ljava/lang/String;
    .end local v31    # "xvlSdk":Ljava/lang/String;
    .end local v32    # "xvlSoftware":Ljava/lang/String;
    .end local v33    # "xvlclientdate":Ljava/lang/String;
    .end local v34    # "xvldm":Ljava/lang/StringBuilder;
    :cond_12
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    goto/16 :goto_0

    .line 99
    .restart local v12    # "extraHeaders":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v22    # "screen":Lcom/vlingo/sdk/internal/util/Screen;
    :cond_13
    const-string/jumbo v35, "max.width"

    const-string/jumbo v36, "0"

    invoke-static/range {v35 .. v36}, Lcom/vlingo/sdk/internal/settings/Settings;->getPersistentString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_1

    .line 101
    .restart local v17    # "maxWidth":Ljava/lang/String;
    :cond_14
    const-string/jumbo v35, "plot.width"

    const-string/jumbo v36, "0"

    invoke-static/range {v35 .. v36}, Lcom/vlingo/sdk/internal/settings/Settings;->getPersistentString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    goto/16 :goto_2

    .line 105
    .restart local v9    # "displayParameters":Ljava/lang/StringBuilder;
    .restart local v19    # "plotWidth":Ljava/lang/String;
    :cond_15
    invoke-virtual/range {v22 .. v22}, Lcom/vlingo/sdk/internal/util/Screen;->getWidth()I

    move-result v35

    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v35

    goto/16 :goto_3

    .line 110
    :cond_16
    invoke-static {}, Lcom/vlingo/sdk/internal/util/Screen;->getMagnification()F

    move-result v35

    invoke-static/range {v35 .. v35}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v35

    goto/16 :goto_4

    .restart local v27    # "tosAcceptedDate":Ljava/lang/String;
    :cond_17
    move-object/from16 v16, p3

    .line 137
    goto/16 :goto_5

    .line 189
    .restart local v4    # "channel":Ljava/lang/String;
    .restart local v6    # "deviceId":Ljava/lang/String;
    .restart local v16    # "languageLocale":Ljava/lang/String;
    .restart local v21    # "sb":Ljava/lang/StringBuffer;
    .restart local v30    # "vlclient":Ljava/lang/String;
    .restart local v32    # "xvlSoftware":Ljava/lang/String;
    :cond_18
    const-string/jumbo v4, "Preinstall Free"

    goto/16 :goto_6

    .line 280
    .restart local v5    # "customHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/StringBuilder;>;"
    .restart local v7    # "dialogGUID":Ljava/lang/String;
    .restart local v8    # "dialogTurn":I
    .restart local v10    # "ds":Lcom/vlingo/sdk/util/SDKDebugSettings;
    .restart local v13    # "i$":Ljava/util/Iterator;
    .restart local v14    # "key":Ljava/lang/String;
    .restart local v15    # "lang":Ljava/lang/String;
    .restart local v23    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v24    # "sdf2":Ljava/text/SimpleDateFormat;
    .restart local v28    # "username":Ljava/lang/String;
    .restart local v31    # "xvlSdk":Ljava/lang/String;
    .restart local v33    # "xvlclientdate":Ljava/lang/String;
    .restart local v34    # "xvldm":Ljava/lang/StringBuilder;
    :cond_19
    const-string/jumbo v20, "X-vlconfiguration"

    .line 281
    .restart local v20    # "prefix":Ljava/lang/String;
    move-object/from16 v18, v14

    .restart local v18    # "name":Ljava/lang/String;
    goto :goto_8

    .line 291
    .end local v14    # "key":Ljava/lang/String;
    .end local v18    # "name":Ljava/lang/String;
    .end local v20    # "prefix":Ljava/lang/String;
    :cond_1a
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v35

    invoke-interface/range {v35 .. v35}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_9
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v35

    if-eqz v35, :cond_1c

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/Map$Entry;

    .line 292
    .local v11, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/StringBuilder;>;"
    invoke-interface {v11}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/StringBuilder;

    .line 293
    .local v29, "value":Ljava/lang/StringBuilder;
    const-string/jumbo v35, ";"

    move-object/from16 v0, v29

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v35

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->length()I

    move-result v36

    add-int/lit8 v36, v36, -0x1

    move-object/from16 v0, v29

    move/from16 v1, v35

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 294
    invoke-interface {v11}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v35

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_9

    .line 299
    .end local v5    # "customHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/StringBuilder;>;"
    .end local v7    # "dialogGUID":Ljava/lang/String;
    .end local v8    # "dialogTurn":I
    .end local v10    # "ds":Lcom/vlingo/sdk/util/SDKDebugSettings;
    .end local v11    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/StringBuilder;>;"
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v23    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v24    # "sdf2":Ljava/text/SimpleDateFormat;
    .end local v28    # "username":Ljava/lang/String;
    .end local v29    # "value":Ljava/lang/StringBuilder;
    .end local v33    # "xvlclientdate":Ljava/lang/String;
    .end local v34    # "xvldm":Ljava/lang/StringBuilder;
    :cond_1b
    const-string/jumbo v35, "X-vvs"

    const-string/jumbo v36, "Version=1.0"

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    :cond_1c
    return-void
.end method

.method public static addVLServiceCookies(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Hashtable;
    .locals 1
    .param p1, "domain"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    .local p0, "cookies":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez p0, :cond_0

    .line 58
    new-instance p0, Ljava/util/Hashtable;

    .end local p0    # "cookies":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/util/Hashtable;-><init>()V

    .line 59
    .restart local p0    # "cookies":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    invoke-static {}, Lcom/vlingo/sdk/internal/vlservice/VLServiceCookieManager;->getInstance()Lcom/vlingo/sdk/internal/http/cookies/CookieJarManager;

    move-result-object v0

    invoke-interface {v0, p0, p1, p2}, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManager;->addAllCookiesToHashtable(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    return-object p0
.end method

.method public static getSpeakerID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    invoke-static {}, Lcom/vlingo/sdk/internal/vlservice/VLServiceCookieManager;->getInstance()Lcom/vlingo/sdk/internal/http/cookies/CookieJarManager;

    move-result-object v0

    const-string/jumbo v1, "VLSPEAKER"

    invoke-interface {v0, v1}, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManager;->getCookieValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static handleResponseCookies(Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)V
    .locals 1
    .param p0, "cookies"    # Lcom/vlingo/sdk/internal/http/cookies/CookieJar;

    .prologue
    .line 67
    invoke-static {}, Lcom/vlingo/sdk/internal/vlservice/VLServiceCookieManager;->getInstance()Lcom/vlingo/sdk/internal/http/cookies/CookieJarManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManager;->mergeCookies(Lcom/vlingo/sdk/internal/http/cookies/CookieJar;)V

    .line 68
    return-void
.end method

.method public static setProtocolHeader(Ljava/util/Hashtable;Ljava/lang/String;)V
    .locals 3
    .param p1, "responseEncoding"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 73
    .local p0, "headers":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v0, "X-vlprotocol"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Version=3.8; ResponseEncoding="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    return-void
.end method

.method private static updateWithDivider(Ljava/lang/StringBuilder;)V
    .locals 1
    .param p0, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 82
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 83
    const-string/jumbo v0, "; "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    :cond_0
    return-void
.end method
