.class final Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$1;
.super Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;
.source "AbstractRule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;->getThresholdBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder",
        "<",
        "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ListTagBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method protected getChildBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;->getBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;

    move-result-object v0

    return-object v0
.end method

.method protected getChildTagName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    const-string/jumbo v0, "Param"

    return-object v0
.end method

.method public getTagName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    const-string/jumbo v0, "Threshold"

    return-object v0
.end method
