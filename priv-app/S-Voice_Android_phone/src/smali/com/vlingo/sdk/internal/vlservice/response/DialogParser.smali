.class public Lcom/vlingo/sdk/internal/vlservice/response/DialogParser;
.super Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;
.source "DialogParser.java"


# instance fields
.field private final XML_ATTR_GUID:I

.field private final XML_ATTR_TURN:I

.field private final XML_ELEMENT_DIALOGSTATE:I

.field private final XML_ELEMENT_VVS:I

.field currentParameterValueStartPos:I

.field origXML:[C


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;)V
    .locals 1
    .param p1, "parser"    # Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;-><init>(Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;)V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/DialogParser;->origXML:[C

    .line 32
    const-string/jumbo v0, "VV"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/DialogParser;->XML_ELEMENT_VVS:I

    .line 33
    const-string/jumbo v0, "DialogState"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/DialogParser;->XML_ELEMENT_DIALOGSTATE:I

    .line 36
    const-string/jumbo v0, "dialog-guid"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/DialogParser;->XML_ATTR_GUID:I

    .line 37
    const-string/jumbo v0, "turn"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/DialogParser;->XML_ATTR_TURN:I

    .line 38
    return-void
.end method


# virtual methods
.method public beginElement(ILcom/vlingo/sdk/internal/xml/XmlAttributes;[CI)V
    .locals 7
    .param p1, "elementType"    # I
    .param p2, "attributes"    # Lcom/vlingo/sdk/internal/xml/XmlAttributes;
    .param p3, "cData"    # [C
    .param p4, "elementEndPosition"    # I

    .prologue
    .line 49
    iget v6, p0, Lcom/vlingo/sdk/internal/vlservice/response/DialogParser;->XML_ELEMENT_VVS:I

    if-ne v6, p1, :cond_3

    .line 50
    if-eqz p2, :cond_1

    .line 51
    iget v6, p0, Lcom/vlingo/sdk/internal/vlservice/response/DialogParser;->XML_ATTR_GUID:I

    invoke-virtual {p2, v6}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v3

    .line 52
    .local v3, "guid":Ljava/lang/String;
    iget v6, p0, Lcom/vlingo/sdk/internal/vlservice/response/DialogParser;->XML_ATTR_TURN:I

    invoke-virtual {p2, v6}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v5

    .line 54
    .local v5, "turnStr":Ljava/lang/String;
    const/4 v4, -0x1

    .line 55
    .local v4, "turn":I
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_0

    .line 56
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 58
    :cond_0
    iget-object v6, p0, Lcom/vlingo/sdk/internal/vlservice/response/DialogParser;->responseParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    invoke-virtual {v6}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->getResponse()Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->setDialogGuid(Ljava/lang/String;)V

    .line 59
    iget-object v6, p0, Lcom/vlingo/sdk/internal/vlservice/response/DialogParser;->responseParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    invoke-virtual {v6}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->getResponse()Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->setDialogTurn(I)V

    .line 61
    .end local v3    # "guid":Ljava/lang/String;
    .end local v4    # "turn":I
    .end local v5    # "turnStr":Ljava/lang/String;
    :cond_1
    iget-object v6, p0, Lcom/vlingo/sdk/internal/vlservice/response/DialogParser;->responseParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    invoke-virtual {v6}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->onSectionComplete()V

    .line 74
    :cond_2
    :goto_0
    return-void

    .line 63
    :cond_3
    iget v6, p0, Lcom/vlingo/sdk/internal/vlservice/response/DialogParser;->XML_ELEMENT_DIALOGSTATE:I

    if-ne v6, p1, :cond_2

    .line 65
    :try_start_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p3}, Ljava/lang/String;-><init>([C)V

    .line 66
    .local v1, "dialogDataBase64":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/sdk/internal/util/Base64;->decode(Ljava/lang/String;)[B

    move-result-object v0

    .line 67
    .local v0, "dialogData":[B
    iget-object v6, p0, Lcom/vlingo/sdk/internal/vlservice/response/DialogParser;->responseParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    invoke-virtual {v6}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->getResponse()Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLServiceResponse;->setDialogState([B)V
    :try_end_0
    .catch Lcom/vlingo/sdk/internal/util/Base64DecoderException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 68
    .end local v0    # "dialogData":[B
    .end local v1    # "dialogDataBase64":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 71
    .local v2, "e":Lcom/vlingo/sdk/internal/util/Base64DecoderException;
    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/util/Base64DecoderException;->printStackTrace()V

    goto :goto_0
.end method

.method public endElement(II)V
    .locals 1
    .param p1, "elementType"    # I
    .param p2, "elementStartPosition"    # I

    .prologue
    .line 77
    iget v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/DialogParser;->XML_ELEMENT_DIALOGSTATE:I

    if-ne v0, p1, :cond_0

    .line 92
    iget-object v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/DialogParser;->responseParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->onSectionComplete()V

    .line 94
    :cond_0
    return-void
.end method

.method public handlesElement(I)Z
    .locals 1
    .param p1, "elementType"    # I

    .prologue
    .line 41
    iget v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/DialogParser;->XML_ELEMENT_VVS:I

    if-eq v0, p1, :cond_0

    iget v0, p0, Lcom/vlingo/sdk/internal/vlservice/response/DialogParser;->XML_ELEMENT_DIALOGSTATE:I

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onParseBegin([C)V
    .locals 0
    .param p1, "xml"    # [C

    .prologue
    .line 45
    iput-object p1, p0, Lcom/vlingo/sdk/internal/vlservice/response/DialogParser;->origXML:[C

    .line 46
    return-void
.end method
