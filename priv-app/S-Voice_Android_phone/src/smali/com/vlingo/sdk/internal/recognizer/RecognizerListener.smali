.class public interface abstract Lcom/vlingo/sdk/internal/recognizer/RecognizerListener;
.super Ljava/lang/Object;
.source "RecognizerListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;,
        Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;
    }
.end annotation


# virtual methods
.method public abstract onRecognitionResponse(Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;)V
.end method

.method public abstract onRecognizerError(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerError;Ljava/lang/String;)V
.end method

.method public abstract onRecognizerStateChanged(Lcom/vlingo/sdk/internal/recognizer/RecognizerListener$RecognizerState;Ljava/lang/Object;)V
.end method
