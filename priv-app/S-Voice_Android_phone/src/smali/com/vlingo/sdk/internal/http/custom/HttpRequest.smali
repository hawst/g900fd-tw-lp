.class public Lcom/vlingo/sdk/internal/http/custom/HttpRequest;
.super Ljava/lang/Object;
.source "HttpRequest.java"


# instance fields
.field private ivClientDout:Ljava/io/DataOutputStream;

.field private ivClientOut:Ljava/io/OutputStream;

.field private ivFinished:Z

.field private ivHeaders:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private ivHeadersSent:Z

.field private ivInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

.field private ivMethod:Ljava/lang/String;

.field private ivPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;)V
    .locals 6
    .param p1, "interaction"    # Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    .prologue
    const/4 v2, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-boolean v2, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivFinished:Z

    .line 30
    iput-boolean v2, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivHeadersSent:Z

    .line 32
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    iput-object v2, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivHeaders:Ljava/util/Hashtable;

    .line 34
    const-string/jumbo v2, "POST"

    iput-object v2, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivMethod:Ljava/lang/String;

    .line 42
    if-nez p1, :cond_0

    .line 43
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "interaction may not be null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 45
    :cond_0
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    .line 47
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getHTTPConnection()Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "host":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getHTTPConnection()Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->getPort()I

    move-result v1

    .line 49
    .local v1, "port":I
    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivHeaders:Ljava/util/Hashtable;

    const-string/jumbo v3, "Host"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivHeaders:Ljava/util/Hashtable;

    const-string/jumbo v3, "User-Agent"

    const-string/jumbo v4, "Vlingo HttpClient 2.0"

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    return-void
.end method

.method private checkFinished()V
    .locals 2

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivFinished:Z

    if-eqz v0, :cond_0

    .line 82
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "HttpRequest is closed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_0
    return-void
.end method

.method private checkHeaderSent()V
    .locals 2

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivHeadersSent:Z

    if-eqz v0, :cond_0

    .line 88
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Headers already sent, too late to specify this information"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_0
    return-void
.end method

.method private ensureHeadersSent()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    iget-boolean v1, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivHeadersSent:Z

    if-nez v1, :cond_1

    .line 96
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getHTTPConnection()Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->getOutputStream(Lcom/vlingo/sdk/internal/http/custom/HttpRequest;)Ljava/io/DataOutputStream;

    move-result-object v0

    .line 98
    .local v0, "dout":Ljava/io/DataOutputStream;
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivPath:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 99
    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "Path is null"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 101
    :cond_0
    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->initOutputStream(Ljava/io/DataOutputStream;)V

    .line 103
    .end local v0    # "dout":Ljava/io/DataOutputStream;
    :cond_1
    return-void
.end method

.method private initOutputStream(Ljava/io/DataOutputStream;)V
    .locals 3
    .param p1, "out"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    iget-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivHeaders:Ljava/util/Hashtable;

    const-string/jumbo v2, "Transfer-Encoding"

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 111
    .local v0, "transferCoding":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string/jumbo v1, "chunked"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113
    new-instance v1, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;

    invoke-direct {v1, p1}, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivClientOut:Ljava/io/OutputStream;

    .line 114
    new-instance v1, Ljava/io/DataOutputStream;

    iget-object v2, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivClientOut:Ljava/io/OutputStream;

    invoke-direct {v1, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v1, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivClientDout:Ljava/io/DataOutputStream;

    .line 121
    :goto_0
    return-void

    .line 118
    :cond_0
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivClientOut:Ljava/io/OutputStream;

    .line 119
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivClientDout:Ljava/io/DataOutputStream;

    goto :goto_0
.end method


# virtual methods
.method public finish()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->checkFinished()V

    .line 143
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ensureHeadersSent()V

    .line 145
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivClientOut:Ljava/io/OutputStream;

    instance-of v0, v0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivClientOut:Ljava/io/OutputStream;

    check-cast v0, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/ChunkedOutputStream;->writeLastChunk()V

    .line 150
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivFinished:Z

    .line 152
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivInteraction:Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/http/custom/HttpInteraction;->getHTTPConnection()Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vlingo/sdk/internal/http/custom/VHttpConnection;->notifyRequestDone(Lcom/vlingo/sdk/internal/http/custom/HttpRequest;)V

    .line 153
    return-void
.end method

.method public getMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivMethod:Ljava/lang/String;

    return-object v0
.end method

.method public getOutputStream()Ljava/io/DataOutputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ensureHeadersSent()V

    .line 133
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivClientDout:Ljava/io/DataOutputStream;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivPath:Ljava/lang/String;

    return-object v0
.end method

.method public isFinished()Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivFinished:Z

    return v0
.end method

.method public sendHeaders()Ljava/util/Hashtable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Hashtable",
            "<**>;"
        }
    .end annotation

    .prologue
    .line 156
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivHeaders:Ljava/util/Hashtable;

    return-object v0
.end method

.method public setHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->checkHeaderSent()V

    .line 126
    iget-object v0, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivHeaders:Ljava/util/Hashtable;

    invoke-virtual {v0, p1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    return-void
.end method

.method public setMethod(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->checkHeaderSent()V

    .line 67
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivMethod:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->checkHeaderSent()V

    .line 56
    iput-object p1, p0, Lcom/vlingo/sdk/internal/http/custom/HttpRequest;->ivPath:Ljava/lang/String;

    .line 57
    return-void
.end method
