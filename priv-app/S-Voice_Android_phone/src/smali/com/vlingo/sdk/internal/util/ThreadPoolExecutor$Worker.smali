.class Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;
.super Ljava/lang/Object;
.source "ThreadPoolExecutor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Worker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;


# direct methods
.method private constructor <init>(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;
    .param p2, "x1"    # Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$1;

    .prologue
    .line 172
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;-><init>(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 177
    :cond_0
    :goto_0
    const/4 v3, 0x0

    .line 178
    .local v3, "fut":Lcom/vlingo/sdk/internal/util/Future;
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # getter for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$100(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Ljava/util/Vector;

    move-result-object v7

    monitor-enter v7

    .line 179
    :goto_1
    :try_start_0
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # getter for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->paused:Z
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$200(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # getter for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$100(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Ljava/util/Vector;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Vector;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_1
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # getter for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->shutDown:Z
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$300(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v6

    if-nez v6, :cond_2

    .line 189
    :try_start_1
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # getter for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$100(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Ljava/util/Vector;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 190
    :catch_0
    move-exception v2

    .line 191
    .local v2, "e":Ljava/lang/InterruptedException;
    goto :goto_1

    .line 195
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :cond_2
    :try_start_2
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # getter for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->shutDown:Z
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$300(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 197
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # getter for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->workers:Ljava/util/Vector;
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$400(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Ljava/util/Vector;

    move-result-object v8

    monitor-enter v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 198
    :try_start_3
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # getter for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->workers:Ljava/util/Vector;
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$400(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Ljava/util/Vector;

    move-result-object v6

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 199
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # operator-- for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->availWorkers:I
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$510(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)I

    .line 200
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 201
    :try_start_4
    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 258
    :goto_2
    return-void

    .line 200
    :catchall_0
    move-exception v6

    :try_start_5
    monitor-exit v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v6

    .line 207
    :catchall_1
    move-exception v6

    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v6

    .line 205
    :cond_3
    :try_start_7
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # getter for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$100(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Ljava/util/Vector;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Vector;->firstElement()Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/vlingo/sdk/internal/util/Future;

    move-object v3, v0

    .line 206
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # getter for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$100(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Ljava/util/Vector;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 207
    monitor-exit v7
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 209
    const/4 v5, 0x0

    .line 210
    .local v5, "task":Ljava/lang/Runnable;
    monitor-enter v3

    .line 211
    :try_start_8
    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/util/Future;->isCancelled()Z

    move-result v6

    if-nez v6, :cond_4

    .line 212
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # operator-- for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->availWorkers:I
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$510(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)I

    .line 213
    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/util/Future;->getRunnable()Ljava/lang/Runnable;

    move-result-object v5

    .line 214
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/vlingo/sdk/internal/util/Future;->setThread(Ljava/lang/Thread;)V

    .line 216
    :cond_4
    monitor-exit v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 218
    if-eqz v5, :cond_0

    .line 219
    const/4 v1, 0x1

    .line 223
    .local v1, "completedTask":Z
    :try_start_9
    invoke-interface {v5}, Ljava/lang/Runnable;->run()V

    .line 226
    # invokes: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->isRunnableRetry(Ljava/lang/Runnable;)Z
    invoke-static {v5}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$600(Ljava/lang/Runnable;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 229
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # getter for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$100(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Ljava/util/Vector;

    move-result-object v7

    monitor-enter v7
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1

    .line 230
    :try_start_a
    # invokes: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->isRunnableOrdered(Ljava/lang/Runnable;)Z
    invoke-static {v5}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$700(Ljava/lang/Runnable;)Z

    move-result v6

    if-nez v6, :cond_5

    # invokes: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->isRunnableHighPriority(Ljava/lang/Runnable;)Z
    invoke-static {v5}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$800(Ljava/lang/Runnable;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 231
    :cond_5
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # getter for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$100(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Ljava/util/Vector;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v6, v3, v8}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    .line 234
    :goto_3
    monitor-exit v7
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 235
    const/4 v1, 0x0

    .line 242
    :cond_6
    :goto_4
    monitor-enter v3

    .line 243
    if-eqz v1, :cond_7

    .line 244
    :try_start_b
    invoke-virtual {v3}, Lcom/vlingo/sdk/internal/util/Future;->complete()V

    .line 245
    :cond_7
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lcom/vlingo/sdk/internal/util/Future;->setThread(Ljava/lang/Thread;)V

    .line 246
    monitor-exit v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    .line 247
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # operator++ for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->availWorkers:I
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$508(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)I

    .line 248
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # getter for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->dynamic:Z
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$1000(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 250
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # getter for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$100(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Ljava/util/Vector;

    move-result-object v7

    monitor-enter v7

    .line 251
    :try_start_c
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # getter for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$100(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Ljava/util/Vector;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Vector;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 252
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # getter for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->workers:Ljava/util/Vector;
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$400(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Ljava/util/Vector;

    move-result-object v8

    monitor-enter v8
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 253
    :try_start_d
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # getter for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->workers:Ljava/util/Vector;
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$400(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Ljava/util/Vector;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v6

    iget-object v9, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # getter for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->minPoolSize:I
    invoke-static {v9}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$1100(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)I

    move-result v9

    if-le v6, v9, :cond_9

    .line 256
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # operator-- for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->availWorkers:I
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$510(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)I

    .line 257
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # getter for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->workers:Ljava/util/Vector;
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$400(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Ljava/util/Vector;

    move-result-object v6

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 258
    monitor-exit v8
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_6

    :try_start_e
    monitor-exit v7

    goto/16 :goto_2

    .line 262
    :catchall_2
    move-exception v6

    monitor-exit v7
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    throw v6

    .line 216
    .end local v1    # "completedTask":Z
    :catchall_3
    move-exception v6

    :try_start_f
    monitor-exit v3
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    throw v6

    .line 233
    .restart local v1    # "completedTask":Z
    :cond_8
    :try_start_10
    iget-object v6, p0, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor$Worker;->this$0:Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;

    # getter for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->queue:Ljava/util/Vector;
    invoke-static {v6}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$100(Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;)Ljava/util/Vector;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_3

    .line 234
    :catchall_4
    move-exception v6

    monitor-exit v7
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    :try_start_11
    throw v6
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_1

    .line 237
    :catch_1
    move-exception v4

    .line 238
    .local v4, "t":Ljava/lang/Throwable;
    # getter for: Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/sdk/internal/util/ThreadPoolExecutor;->access$900()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Exception executing task "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", Exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", Message: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 246
    .end local v4    # "t":Ljava/lang/Throwable;
    :catchall_5
    move-exception v6

    :try_start_12
    monitor-exit v3
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    throw v6

    .line 260
    :cond_9
    :try_start_13
    monitor-exit v8
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_6

    .line 262
    :cond_a
    :try_start_14
    monitor-exit v7
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_2

    goto/16 :goto_0

    .line 260
    :catchall_6
    move-exception v6

    :try_start_15
    monitor-exit v8
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_6

    :try_start_16
    throw v6
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_2
.end method
