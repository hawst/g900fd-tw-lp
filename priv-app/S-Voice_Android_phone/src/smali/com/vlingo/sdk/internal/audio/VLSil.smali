.class public Lcom/vlingo/sdk/internal/audio/VLSil;
.super Ljava/lang/Object;
.source "VLSil.java"


# static fields
.field private static final FRAME_LENGTH:F = 0.02f

.field private static final MIN_ENERGY:F = 0.0f

.field private static final VLSIL_HISTO_SIZE:I = 0x64

.field private static final VLSIL_MAX_FRAME_SIZE:I = 0x140

.field public static final VLSIL_NO_SPEECH_DETECTED:I = -0x1

.field public static final VLSIL_OK:I


# instance fields
.field currentSpeechIndex:I

.field energyHisto:[I

.field minVoiceDuration:F

.field minVoiceLevel:F

.field mostRecentSpeechSample:I

.field numAboveCurrentSpeechIndex:I

.field numFrames:I

.field numFramesAboveSpeechThresh:I

.field numSamplesField:I

.field sampleRate:I

.field samplesField:[S

.field samplesSoFar:I

.field silenceThreshold:F

.field voicePortion:F


# direct methods
.method public constructor <init>(IFFFF)V
    .locals 1
    .param p1, "sampleRate"    # I
    .param p2, "silenceThreshold"    # F
    .param p3, "minVoiceDuration"    # F
    .param p4, "voicePortion"    # F
    .param p5, "minVoiceLevel"    # F

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/16 v0, 0x64

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->energyHisto:[I

    .line 31
    const/16 v0, 0x140

    new-array v0, v0, [S

    iput-object v0, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->samplesField:[S

    .line 39
    iput p1, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->sampleRate:I

    .line 40
    iput p4, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->voicePortion:F

    .line 41
    iput p2, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->silenceThreshold:F

    .line 42
    iput p3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->minVoiceDuration:F

    .line 43
    iput p5, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->minVoiceLevel:F

    .line 44
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/audio/VLSil;->initForUtterance()V

    .line 45
    return-void
.end method

.method private computeEnergy([SI)F
    .locals 10
    .param p1, "samples"    # [S
    .param p2, "numSamples"    # I

    .prologue
    .line 112
    const/4 v4, 0x0

    .line 113
    .local v4, "sumsq":F
    const/4 v3, 0x0

    .line 114
    .local v3, "sum":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 115
    aget-short v2, p1, v0

    .line 116
    .local v2, "smoothsample":I
    mul-int v6, v2, v2

    int-to-float v6, v6

    add-float/2addr v4, v6

    .line 117
    add-int/2addr v3, v2

    .line 114
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 119
    .end local v2    # "smoothsample":I
    :cond_0
    div-int v1, v3, p2

    .line 121
    .local v1, "mean":I
    int-to-float v6, p2

    div-float v6, v4, v6

    float-to-int v6, v6

    mul-int v7, v1, v1

    sub-int v5, v6, v7

    .line 123
    .local v5, "total":I
    const/4 v6, 0x1

    if-ge v5, v6, :cond_1

    .line 124
    const/4 v5, 0x1

    .line 127
    :cond_1
    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    int-to-double v8, v5

    invoke-static {v8, v9}, Ljava/lang/Math;->log10(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    double-to-float v6, v6

    return v6
.end method

.method private updateHistoAndSpeechThresh(F)F
    .locals 8
    .param p1, "energy"    # F

    .prologue
    const/16 v7, 0x64

    const/4 v6, 0x0

    .line 131
    sub-float v3, p1, v6

    float-to-int v0, v3

    .line 133
    .local v0, "histoIndex":I
    if-gez v0, :cond_0

    .line 134
    const/4 v0, 0x0

    .line 136
    :cond_0
    if-lt v0, v7, :cond_1

    .line 137
    const/16 v0, 0x63

    .line 140
    :cond_1
    iget v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numFrames:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numFrames:I

    .line 141
    iget-object v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->energyHisto:[I

    aget v4, v3, v0

    add-int/lit8 v4, v4, 0x1

    aput v4, v3, v0

    .line 143
    iget v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numFrames:I

    int-to-float v3, v3

    iget v4, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->voicePortion:F

    mul-float/2addr v3, v4

    float-to-int v2, v3

    .line 145
    .local v2, "targetNumAboveThresh":I
    const/4 v3, 0x1

    if-ge v2, v3, :cond_2

    .line 146
    const/4 v2, 0x1

    .line 148
    :cond_2
    iget v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->currentSpeechIndex:I

    if-le v0, v3, :cond_3

    .line 149
    iget v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numAboveCurrentSpeechIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numAboveCurrentSpeechIndex:I

    .line 152
    :cond_3
    iget v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numAboveCurrentSpeechIndex:I

    if-le v3, v2, :cond_6

    .line 154
    iget v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->currentSpeechIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->currentSpeechIndex:I

    .line 155
    :goto_0
    iget v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->currentSpeechIndex:I

    if-ge v3, v7, :cond_4

    .line 156
    iget v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numAboveCurrentSpeechIndex:I

    iget-object v4, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->energyHisto:[I

    iget v5, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->currentSpeechIndex:I

    aget v4, v4, v5

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numAboveCurrentSpeechIndex:I

    .line 157
    iget v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numAboveCurrentSpeechIndex:I

    if-gt v3, v2, :cond_5

    .line 178
    :cond_4
    iget v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->currentSpeechIndex:I

    int-to-float v3, v3

    add-float/2addr v3, v6

    return v3

    .line 160
    :cond_5
    iget v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->currentSpeechIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->currentSpeechIndex:I

    goto :goto_0

    .line 163
    :cond_6
    iget v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numAboveCurrentSpeechIndex:I

    if-ge v3, v2, :cond_4

    .line 165
    :goto_1
    iget v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->currentSpeechIndex:I

    if-ltz v3, :cond_4

    .line 166
    iget v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numAboveCurrentSpeechIndex:I

    iget-object v4, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->energyHisto:[I

    iget v5, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->currentSpeechIndex:I

    aget v4, v4, v5

    add-int v1, v3, v4

    .line 167
    .local v1, "newNumAbove":I
    if-gt v1, v2, :cond_4

    .line 169
    iget v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->currentSpeechIndex:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->currentSpeechIndex:I

    .line 170
    iput v1, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numAboveCurrentSpeechIndex:I

    goto :goto_1
.end method


# virtual methods
.method public initForUtterance()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 48
    iput v2, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->samplesSoFar:I

    .line 49
    const/4 v1, -0x1

    iput v1, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->mostRecentSpeechSample:I

    .line 50
    iput v2, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numSamplesField:I

    .line 51
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x64

    if-ge v0, v1, :cond_0

    .line 52
    iget-object v1, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->energyHisto:[I

    aput v2, v1, v0

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54
    :cond_0
    iput v2, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->currentSpeechIndex:I

    .line 55
    iput v2, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numFrames:I

    .line 56
    iput v2, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numAboveCurrentSpeechIndex:I

    .line 57
    iput v2, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numFramesAboveSpeechThresh:I

    .line 58
    return-void
.end method

.method public processShortArray([SI)I
    .locals 11
    .param p1, "speechSamples"    # [S
    .param p2, "numSpeechSamples"    # I

    .prologue
    const v10, 0x3ca3d70a    # 0.02f

    .line 63
    const/4 v6, 0x0

    .line 64
    .local v6, "sampleIndex":I
    iget v9, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->sampleRate:I

    int-to-float v9, v9

    mul-float/2addr v9, v10

    float-to-int v5, v9

    .line 65
    .local v5, "numSamplesInFrame":I
    iget v9, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->minVoiceDuration:F

    div-float/2addr v9, v10

    float-to-int v4, v9

    .line 66
    .local v4, "minSpeechFrames":I
    const/4 v3, 0x0

    .line 67
    .local v3, "maxEnergy":I
    const/16 v9, 0x140

    if-le v5, v9, :cond_0

    .line 68
    const/16 v5, 0x140

    .line 71
    :cond_0
    iget v1, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numSamplesField:I

    .line 72
    .local v1, "index":I
    :goto_0
    if-ge v6, p2, :cond_5

    move v2, v1

    .end local v1    # "index":I
    .local v2, "index":I
    move v7, v6

    .line 74
    .end local v6    # "sampleIndex":I
    .local v7, "sampleIndex":I
    :goto_1
    if-ge v2, v5, :cond_1

    if-ge v7, p2, :cond_1

    .line 75
    iget-object v9, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->samplesField:[S

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "sampleIndex":I
    .restart local v6    # "sampleIndex":I
    aget-short v10, p1, v7

    aput-short v10, v9, v2

    move v2, v1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    move v7, v6

    .end local v6    # "sampleIndex":I
    .restart local v7    # "sampleIndex":I
    goto :goto_1

    .line 78
    :cond_1
    if-ne v2, v5, :cond_6

    .line 80
    iget-object v9, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->samplesField:[S

    invoke-direct {p0, v9, v5}, Lcom/vlingo/sdk/internal/audio/VLSil;->computeEnergy([SI)F

    move-result v0

    .line 81
    .local v0, "energy":F
    int-to-float v9, v3

    cmpl-float v9, v0, v9

    if-lez v9, :cond_2

    .line 82
    float-to-int v3, v0

    .line 84
    :cond_2
    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/audio/VLSil;->updateHistoAndSpeechThresh(F)F

    move-result v8

    .line 87
    .local v8, "speechThresh":F
    iget v9, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->silenceThreshold:F

    sub-float v9, v8, v9

    cmpl-float v9, v0, v9

    if-ltz v9, :cond_4

    iget v9, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->minVoiceLevel:F

    cmpl-float v9, v0, v9

    if-ltz v9, :cond_4

    .line 88
    iget v9, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numFramesAboveSpeechThresh:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numFramesAboveSpeechThresh:I

    .line 89
    iget v9, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numFramesAboveSpeechThresh:I

    if-lt v9, v4, :cond_3

    .line 90
    iget v9, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->samplesSoFar:I

    iput v9, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->mostRecentSpeechSample:I

    .line 97
    :cond_3
    :goto_2
    iget v9, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->samplesSoFar:I

    add-int/2addr v9, v5

    iput v9, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->samplesSoFar:I

    .line 98
    const/4 v1, 0x0

    .end local v2    # "index":I
    .restart local v1    # "index":I
    move v6, v7

    .line 99
    .end local v7    # "sampleIndex":I
    .restart local v6    # "sampleIndex":I
    goto :goto_0

    .line 94
    .end local v1    # "index":I
    .end local v6    # "sampleIndex":I
    .restart local v2    # "index":I
    .restart local v7    # "sampleIndex":I
    :cond_4
    const/4 v9, 0x0

    iput v9, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numFramesAboveSpeechThresh:I

    goto :goto_2

    .line 101
    .end local v0    # "energy":F
    .end local v2    # "index":I
    .end local v7    # "sampleIndex":I
    .end local v8    # "speechThresh":F
    .restart local v1    # "index":I
    .restart local v6    # "sampleIndex":I
    :cond_5
    iput v1, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->numSamplesField:I

    .line 103
    iget v9, p0, Lcom/vlingo/sdk/internal/audio/VLSil;->mostRecentSpeechSample:I

    return v9

    .end local v1    # "index":I
    .end local v6    # "sampleIndex":I
    .restart local v2    # "index":I
    .restart local v7    # "sampleIndex":I
    :cond_6
    move v1, v2

    .end local v2    # "index":I
    .restart local v1    # "index":I
    move v6, v7

    .end local v7    # "sampleIndex":I
    .restart local v6    # "sampleIndex":I
    goto :goto_0
.end method
