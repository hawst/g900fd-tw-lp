.class public final Lcom/vlingo/sdk/internal/VLTrainerImpl;
.super Lcom/vlingo/sdk/internal/VLComponentImpl;
.source "VLTrainerImpl.java"

# interfaces
.implements Lcom/vlingo/sdk/internal/http/HttpCallback;
.implements Lcom/vlingo/sdk/training/VLTrainer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/VLTrainerImpl$2;,
        Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;
    }
.end annotation


# instance fields
.field private mNotificationHandler:Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;

.field private mTrainerListener:Lcom/vlingo/sdk/training/VLTrainerListener;

.field private semaphore:Ljava/util/concurrent/Semaphore;


# direct methods
.method public constructor <init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V
    .locals 2
    .param p1, "manager"    # Lcom/vlingo/sdk/internal/VLComponentManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    const/4 v1, 0x1

    .line 85
    invoke-direct {p0, p1, p2}, Lcom/vlingo/sdk/internal/VLComponentImpl;-><init>(Lcom/vlingo/sdk/internal/VLComponentManager;Landroid/os/Handler;)V

    .line 31
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v1, v1}, Ljava/util/concurrent/Semaphore;-><init>(IZ)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl;->semaphore:Ljava/util/concurrent/Semaphore;

    .line 90
    new-instance v0, Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;-><init>(Lcom/vlingo/sdk/internal/VLTrainerImpl;)V

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;

    .line 91
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/sdk/internal/VLTrainerImpl;)Lcom/vlingo/sdk/training/VLTrainerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLTrainerImpl;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl;->mTrainerListener:Lcom/vlingo/sdk/training/VLTrainerListener;

    return-object v0
.end method

.method static synthetic access$002(Lcom/vlingo/sdk/internal/VLTrainerImpl;Lcom/vlingo/sdk/training/VLTrainerListener;)Lcom/vlingo/sdk/training/VLTrainerListener;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLTrainerImpl;
    .param p1, "x1"    # Lcom/vlingo/sdk/training/VLTrainerListener;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl;->mTrainerListener:Lcom/vlingo/sdk/training/VLTrainerListener;

    return-object p1
.end method

.method static synthetic access$100(Lcom/vlingo/sdk/internal/VLTrainerImpl;)Ljava/util/concurrent/Semaphore;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLTrainerImpl;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl;->semaphore:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/sdk/internal/VLTrainerImpl;)Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/sdk/internal/VLTrainerImpl;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;

    return-object v0
.end method

.method private static lmttType2TrainerType(Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 258
    .local p0, "lmttCounts":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;Ljava/lang/Integer;>;"
    if-nez p0, :cond_1

    .line 259
    const/4 v2, 0x0

    .line 270
    :cond_0
    return-object v2

    .line 261
    :cond_1
    new-instance v2, Ljava/util/HashMap;

    invoke-virtual {p0}, Ljava/util/HashMap;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(I)V

    .line 262
    .local v2, "trainerCounts":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;

    .line 263
    .local v1, "lmttType":Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;
    sget-object v3, Lcom/vlingo/sdk/internal/VLTrainerImpl$2;->$SwitchMap$com$vlingo$sdk$internal$lmtt$LMTTItem$LmttItemType:[I

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 264
    :pswitch_0
    sget-object v3, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->CONTACT:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 265
    :pswitch_1
    sget-object v3, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->SONG:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 266
    :pswitch_2
    sget-object v3, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->PLAYLIST:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 263
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private sendUpdate(Lcom/vlingo/sdk/training/VLTrainerUpdateList;Ljava/lang/String;Lcom/vlingo/sdk/training/VLTrainerListener;Z)V
    .locals 7
    .param p1, "list"    # Lcom/vlingo/sdk/training/VLTrainerUpdateList;
    .param p2, "language"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/vlingo/sdk/training/VLTrainerListener;
    .param p4, "doWhole"    # Z

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLTrainerImpl;->validateInstance()V

    .line 221
    if-nez p1, :cond_0

    .line 222
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "list cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 224
    :cond_0
    instance-of v0, p1, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;

    if-nez v0, :cond_1

    .line 225
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "VLTrainerUpdateList instance must be obtained through VLTrainer.getUpdateListIntance()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move-object v0, p1

    .line 227
    check-cast v0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 228
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "list cannot be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 230
    :cond_2
    invoke-static {p2}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 231
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "language cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 233
    :cond_3
    if-nez p3, :cond_4

    .line 234
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "listener must be specifed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 237
    :cond_4
    invoke-virtual {p0}, Lcom/vlingo/sdk/internal/VLTrainerImpl;->getHandler()Landroid/os/Handler;

    move-result-object v6

    new-instance v0, Lcom/vlingo/sdk/internal/VLTrainerImpl$1;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p1

    move v4, p4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/sdk/internal/VLTrainerImpl$1;-><init>(Lcom/vlingo/sdk/internal/VLTrainerImpl;Lcom/vlingo/sdk/training/VLTrainerListener;Lcom/vlingo/sdk/training/VLTrainerUpdateList;ZLjava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 255
    return-void
.end method


# virtual methods
.method public clearAllItems(Ljava/lang/String;Lcom/vlingo/sdk/training/VLTrainerListener;)V
    .locals 2
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/vlingo/sdk/training/VLTrainerListener;

    .prologue
    .line 146
    new-instance v0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;-><init>()V

    .line 147
    .local v0, "list":Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;
    sget-object v1, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->CONTACT:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->addNOOPItem(Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;)V

    .line 148
    sget-object v1, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->SONG:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->addNOOPItem(Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;)V

    .line 149
    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, p2, v1}, Lcom/vlingo/sdk/internal/VLTrainerImpl;->sendUpdate(Lcom/vlingo/sdk/training/VLTrainerUpdateList;Ljava/lang/String;Lcom/vlingo/sdk/training/VLTrainerListener;Z)V

    .line 150
    return-void
.end method

.method public clearContactItems(Ljava/lang/String;Lcom/vlingo/sdk/training/VLTrainerListener;)V
    .locals 2
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/vlingo/sdk/training/VLTrainerListener;

    .prologue
    .line 126
    new-instance v0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;-><init>()V

    .line 127
    .local v0, "list":Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;
    sget-object v1, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->CONTACT:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->addNOOPItem(Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;)V

    .line 128
    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, p2, v1}, Lcom/vlingo/sdk/internal/VLTrainerImpl;->sendUpdate(Lcom/vlingo/sdk/training/VLTrainerUpdateList;Ljava/lang/String;Lcom/vlingo/sdk/training/VLTrainerListener;Z)V

    .line 129
    return-void
.end method

.method public clearMusicItems(Ljava/lang/String;Lcom/vlingo/sdk/training/VLTrainerListener;)V
    .locals 2
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/vlingo/sdk/training/VLTrainerListener;

    .prologue
    .line 136
    new-instance v0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;-><init>()V

    .line 137
    .local v0, "list":Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;
    sget-object v1, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->SONG:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->addNOOPItem(Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;)V

    .line 138
    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, p2, v1}, Lcom/vlingo/sdk/internal/VLTrainerImpl;->sendUpdate(Lcom/vlingo/sdk/training/VLTrainerUpdateList;Ljava/lang/String;Lcom/vlingo/sdk/training/VLTrainerListener;Z)V

    .line 139
    return-void
.end method

.method public bridge synthetic destroy()V
    .locals 0

    .prologue
    .line 30
    invoke-super {p0}, Lcom/vlingo/sdk/internal/VLComponentImpl;->destroy()V

    return-void
.end method

.method public doesDependOnSettings()Z
    .locals 1

    .prologue
    .line 276
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic isValid()Z
    .locals 1

    .prologue
    .line 30
    invoke-super {p0}, Lcom/vlingo/sdk/internal/VLComponentImpl;->isValid()Z

    move-result v0

    return v0
.end method

.method public onCancelled(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 208
    return-void
.end method

.method onDestroy()V
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl;->mTrainerListener:Lcom/vlingo/sdk/training/VLTrainerListener;

    .line 103
    return-void
.end method

.method public onFailure(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 205
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;

    sget-object v1, Lcom/vlingo/sdk/training/VLTrainerErrors;->ERROR_NETWORK:Lcom/vlingo/sdk/training/VLTrainerErrors;

    const-string/jumbo v2, "Network error"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;->notifyError(Lcom/vlingo/sdk/training/VLTrainerErrors;Ljava/lang/String;)V

    .line 206
    return-void
.end method

.method public onResponse(Lcom/vlingo/sdk/internal/http/HttpRequest;Lcom/vlingo/sdk/internal/http/HttpResponse;)V
    .locals 5
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;
    .param p2, "response"    # Lcom/vlingo/sdk/internal/http/HttpResponse;

    .prologue
    .line 172
    sget-object v1, Lcom/vlingo/sdk/training/VLTrainerErrors;->ERROR_SERVER:Lcom/vlingo/sdk/training/VLTrainerErrors;

    .line 174
    .local v1, "error":Lcom/vlingo/sdk/training/VLTrainerErrors;
    iget v3, p2, Lcom/vlingo/sdk/internal/http/HttpResponse;->responseCode:I

    const/16 v4, 0xc8

    if-ne v3, v4, :cond_1

    .line 175
    invoke-static {p2}, Lcom/vlingo/sdk/internal/lmtt/LMTTComm;->parseLMTTResponse(Lcom/vlingo/sdk/internal/http/HttpResponse;)Ljava/util/HashMap;

    move-result-object v0

    .line 176
    .local v0, "counts":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;Ljava/lang/Integer;>;"
    if-eqz v0, :cond_1

    .line 177
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 178
    invoke-static {v0}, Lcom/vlingo/sdk/internal/VLTrainerImpl;->lmttType2TrainerType(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v2

    .line 179
    .local v2, "trainerCounts":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;Ljava/lang/Integer;>;"
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 181
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;

    invoke-virtual {v3, v2}, Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;->notifySuccess(Ljava/util/HashMap;)V

    .line 197
    .end local v0    # "counts":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;Ljava/lang/Integer;>;"
    .end local v2    # "trainerCounts":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;Ljava/lang/Integer;>;"
    :goto_0
    return-void

    .line 185
    .restart local v0    # "counts":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;Ljava/lang/Integer;>;"
    .restart local v2    # "trainerCounts":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;Ljava/lang/Integer;>;"
    :cond_0
    sget-object v1, Lcom/vlingo/sdk/training/VLTrainerErrors;->ERROR_SERVER:Lcom/vlingo/sdk/training/VLTrainerErrors;

    .line 196
    .end local v0    # "counts":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;Ljava/lang/Integer;>;"
    .end local v2    # "trainerCounts":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;Ljava/lang/Integer;>;"
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;

    invoke-virtual {p2}, Lcom/vlingo/sdk/internal/http/HttpResponse;->getDataAsString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;->notifyError(Lcom/vlingo/sdk/training/VLTrainerErrors;Ljava/lang/String;)V

    goto :goto_0

    .line 189
    .restart local v0    # "counts":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/sdk/internal/lmtt/LMTTItem$LmttItemType;Ljava/lang/Integer;>;"
    :cond_2
    sget-object v1, Lcom/vlingo/sdk/training/VLTrainerErrors;->ERROR_RATE_LIMIT:Lcom/vlingo/sdk/training/VLTrainerErrors;

    goto :goto_1
.end method

.method public onTimeout(Lcom/vlingo/sdk/internal/http/HttpRequest;)Z
    .locals 3
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/vlingo/sdk/internal/VLTrainerImpl;->mNotificationHandler:Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;

    sget-object v1, Lcom/vlingo/sdk/training/VLTrainerErrors;->ERROR_NETWORK_TIMEOUT:Lcom/vlingo/sdk/training/VLTrainerErrors;

    const-string/jumbo v2, "Timeout waiting for request"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/sdk/internal/VLTrainerImpl$NotificationHandler;->notifyError(Lcom/vlingo/sdk/training/VLTrainerErrors;Ljava/lang/String;)V

    .line 201
    const/4 v0, 0x1

    return v0
.end method

.method public onWillExecuteRequest(Lcom/vlingo/sdk/internal/http/HttpRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/sdk/internal/http/HttpRequest;

    .prologue
    .line 168
    return-void
.end method

.method public sendFullUpdate(Lcom/vlingo/sdk/training/VLTrainerUpdateList;Ljava/lang/String;Lcom/vlingo/sdk/training/VLTrainerListener;)V
    .locals 1
    .param p1, "list"    # Lcom/vlingo/sdk/training/VLTrainerUpdateList;
    .param p2, "language"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/vlingo/sdk/training/VLTrainerListener;

    .prologue
    .line 118
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/vlingo/sdk/internal/VLTrainerImpl;->sendUpdate(Lcom/vlingo/sdk/training/VLTrainerUpdateList;Ljava/lang/String;Lcom/vlingo/sdk/training/VLTrainerListener;Z)V

    .line 119
    return-void
.end method

.method public sendPartialUpdate(Lcom/vlingo/sdk/training/VLTrainerUpdateList;Ljava/lang/String;Lcom/vlingo/sdk/training/VLTrainerListener;)V
    .locals 1
    .param p1, "list"    # Lcom/vlingo/sdk/training/VLTrainerUpdateList;
    .param p2, "language"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/vlingo/sdk/training/VLTrainerListener;

    .prologue
    .line 110
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/vlingo/sdk/internal/VLTrainerImpl;->sendUpdate(Lcom/vlingo/sdk/training/VLTrainerUpdateList;Ljava/lang/String;Lcom/vlingo/sdk/training/VLTrainerListener;Z)V

    .line 111
    return-void
.end method

.method public updateTrainerModelLanguage(Ljava/lang/String;Lcom/vlingo/sdk/training/VLTrainerListener;)V
    .locals 2
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/vlingo/sdk/training/VLTrainerListener;

    .prologue
    .line 157
    new-instance v0, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;-><init>()V

    .line 158
    .local v0, "list":Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;
    sget-object v1, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->CONTACT:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->addNOOPItem(Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;)V

    .line 159
    sget-object v1, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->SONG:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/VLTrainerUpdateListImpl;->addNOOPItem(Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;)V

    .line 160
    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, p2, v1}, Lcom/vlingo/sdk/internal/VLTrainerImpl;->sendUpdate(Lcom/vlingo/sdk/training/VLTrainerUpdateList;Ljava/lang/String;Lcom/vlingo/sdk/training/VLTrainerListener;Z)V

    .line 161
    return-void
.end method
