.class public Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;
.super Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;
.source "TaggedResultsParser.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final ATTRIBUTE_C:I

.field private final ATTRIBUTE_CA:I

.field private final ATTRIBUTE_CF:I

.field private final ATTRIBUTE_GUTTID:I

.field private final ATTRIBUTE_ID:I

.field private final ATTRIBUTE_N:I

.field private final ATTRIBUTE_NAME:I

.field private final ATTRIBUTE_NS:I

.field private final ATTRIBUTE_NSD:I

.field private final ATTRIBUTE_R:I

.field private final ATTRIBUTE_TYPE:I

.field private final RESULT_ALT:I

.field private final RESULT_CHOICE:I

.field private final RESULT_PARSE_GRP:I

.field private final RESULT_RECOGNITION:I

.field private final RESULT_TAG:I

.field private final RESULT_TAG_CHOICE:I

.field private final RESULT_TAG_LIST:I

.field private final RESULT_UTT_LIST:I

.field private final RESULT_WORD:I

.field private final RESULT_WORD_LIST:I

.field private choiceCount:I

.field private curParsedWord:Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

.field private guttid:Ljava/lang/String;

.field private recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

.field srResultsParser:Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;

.field private tagResults:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

.field private wordCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;)V
    .locals 3
    .param p1, "parser"    # Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 60
    iget-object v0, p1, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->parser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    invoke-direct {p0, v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseSectionParser;-><init>(Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;)V

    .line 49
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    .line 50
    iput v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->choiceCount:I

    .line 51
    iput v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->wordCount:I

    .line 53
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->guttid:Ljava/lang/String;

    .line 55
    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->tagResults:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    .line 62
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->srResultsParser:Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;

    .line 65
    const-string/jumbo v0, "Recognition"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_RECOGNITION:I

    .line 66
    const-string/jumbo v0, "Alternates"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_ALT:I

    .line 67
    const-string/jumbo v0, "UL"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_UTT_LIST:I

    .line 68
    const-string/jumbo v0, "WL"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_WORD_LIST:I

    .line 69
    const-string/jumbo v0, "T"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_TAG_LIST:I

    .line 70
    const-string/jumbo v0, "w"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_WORD:I

    .line 71
    const-string/jumbo v0, "c"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_CHOICE:I

    .line 72
    const-string/jumbo v0, "pg"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_PARSE_GRP:I

    .line 73
    const-string/jumbo v0, "tag"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_TAG:I

    .line 74
    const-string/jumbo v0, "tl"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_TAG_CHOICE:I

    .line 77
    const-string/jumbo v0, "guttid"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_GUTTID:I

    .line 78
    const-string/jumbo v0, "n"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_N:I

    .line 79
    const-string/jumbo v0, "r"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_R:I

    .line 80
    const-string/jumbo v0, "id"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_ID:I

    .line 81
    const-string/jumbo v0, "ns"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_NS:I

    .line 82
    const-string/jumbo v0, "nsd"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_NSD:I

    .line 83
    const-string/jumbo v0, "c"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_C:I

    .line 84
    const-string/jumbo v0, "cf"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_CF:I

    .line 85
    const-string/jumbo v0, "t"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_TYPE:I

    .line 86
    const-string/jumbo v0, "nm"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_NAME:I

    .line 87
    const-string/jumbo v0, "ca"

    invoke-virtual {p1, v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_CA:I

    .line 88
    return-void
.end method

.method private handleChoiceInUL(Lcom/vlingo/sdk/internal/xml/XmlAttributes;[C)V
    .locals 9
    .param p1, "attributes"    # Lcom/vlingo/sdk/internal/xml/XmlAttributes;
    .param p2, "cData"    # [C

    .prologue
    const/4 v8, 0x0

    .line 354
    if-eqz p1, :cond_1

    .line 356
    iget v6, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_C:I

    invoke-virtual {p1, v6}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v6

    const/high16 v7, -0x40800000    # -1.0f

    invoke-static {v6, v7}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->parseFloat(Ljava/lang/String;F)F

    move-result v1

    .line 357
    .local v1, "confidence":F
    iget v6, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_CF:I

    invoke-virtual {p1, v6}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v0

    .line 358
    .local v0, "cannonical":Ljava/lang/String;
    iget v6, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_N:I

    invoke-virtual {p1, v6}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v8}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->parseInt(Ljava/lang/String;I)I

    move-result v2

    .line 359
    .local v2, "numWords":I
    if-lez v2, :cond_0

    .line 360
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    iget-object v3, v6, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttList:[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 361
    .local v3, "uttList":[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    iget v6, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->choiceCount:I

    new-array v7, v2, [Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aput-object v7, v3, v6

    .line 362
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    iget-object v5, v6, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttListConf:[F

    .line 363
    .local v5, "uttListConf":[F
    iget v6, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->choiceCount:I

    aput v1, v5, v6

    .line 364
    iget-object v6, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    iget-object v4, v6, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttListCannonical:[Ljava/lang/String;

    .line 365
    .local v4, "uttListCannonical":[Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 366
    if-eqz v0, :cond_0

    iget v6, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->choiceCount:I

    aget-object v6, v4, v6

    if-nez v6, :cond_0

    .line 367
    iget v6, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->choiceCount:I

    aput-object v0, v4, v6

    .line 371
    .end local v3    # "uttList":[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    .end local v4    # "uttListCannonical":[Ljava/lang/String;
    .end local v5    # "uttListConf":[F
    :cond_0
    iput v8, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->wordCount:I

    .line 375
    .end local v0    # "cannonical":Ljava/lang/String;
    .end local v1    # "confidence":F
    .end local v2    # "numWords":I
    :goto_0
    return-void

    .line 373
    :cond_1
    sget-object v6, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->TAG:Ljava/lang/String;

    const-string/jumbo v7, "PAR5 warning: no attributes for choice in utt list"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private handleChoiceInWL(Lcom/vlingo/sdk/internal/xml/XmlAttributes;[C)V
    .locals 12
    .param p1, "attributes"    # Lcom/vlingo/sdk/internal/xml/XmlAttributes;
    .param p2, "cData"    # [C

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 316
    if-eqz p2, :cond_1

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p2}, Ljava/lang/String;-><init>([C)V

    .line 319
    .local v1, "choiceString":Ljava/lang/String;
    :goto_0
    const/4 v7, 0x1

    .line 320
    .local v7, "numAlign":I
    const/4 v5, 0x0

    .line 321
    .local v5, "ns":Z
    const/4 v6, 0x0

    .line 322
    .local v6, "nsd":Z
    const/4 v2, 0x0

    .line 324
    .local v2, "ct":Z
    if-eqz p1, :cond_8

    .line 325
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->getLength()I

    move-result v11

    if-ge v3, v11, :cond_8

    .line 326
    invoke-virtual {p1, v3}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->getType(I)I

    move-result v0

    .line 327
    .local v0, "attributeType":I
    iget v11, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_CA:I

    if-ne v11, v0, :cond_3

    .line 328
    invoke-virtual {p1, v3}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->getValue(I)Ljava/lang/String;

    move-result-object v8

    .line 329
    .local v8, "s":Ljava/lang/String;
    if-eqz v8, :cond_2

    const-string/jumbo v11, "t"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    move v2, v9

    .line 325
    .end local v8    # "s":Ljava/lang/String;
    :cond_0
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 316
    .end local v0    # "attributeType":I
    .end local v1    # "choiceString":Ljava/lang/String;
    .end local v2    # "ct":Z
    .end local v3    # "i":I
    .end local v5    # "ns":Z
    .end local v6    # "nsd":Z
    .end local v7    # "numAlign":I
    :cond_1
    const-string/jumbo v1, "(null)"

    goto :goto_0

    .restart local v0    # "attributeType":I
    .restart local v1    # "choiceString":Ljava/lang/String;
    .restart local v2    # "ct":Z
    .restart local v3    # "i":I
    .restart local v5    # "ns":Z
    .restart local v6    # "nsd":Z
    .restart local v7    # "numAlign":I
    .restart local v8    # "s":Ljava/lang/String;
    :cond_2
    move v2, v10

    .line 329
    goto :goto_2

    .line 331
    .end local v8    # "s":Ljava/lang/String;
    :cond_3
    iget v11, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_R:I

    if-ne v11, v0, :cond_4

    .line 332
    invoke-virtual {p1, v3}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->getValue(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    goto :goto_2

    .line 334
    :cond_4
    iget v11, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_NS:I

    if-ne v11, v0, :cond_6

    .line 335
    invoke-virtual {p1, v3}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->getValue(I)Ljava/lang/String;

    move-result-object v8

    .line 336
    .restart local v8    # "s":Ljava/lang/String;
    if-eqz v8, :cond_5

    const-string/jumbo v11, "t"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    move v5, v9

    :goto_3
    goto :goto_2

    :cond_5
    move v5, v10

    goto :goto_3

    .line 338
    .end local v8    # "s":Ljava/lang/String;
    :cond_6
    iget v11, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_NSD:I

    if-ne v11, v0, :cond_0

    .line 339
    invoke-virtual {p1, v3}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->getValue(I)Ljava/lang/String;

    move-result-object v8

    .line 340
    .restart local v8    # "s":Ljava/lang/String;
    if-eqz v8, :cond_7

    const-string/jumbo v11, "t"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    move v6, v9

    :goto_4
    goto :goto_2

    :cond_7
    move v6, v10

    goto :goto_4

    .line 345
    .end local v0    # "attributeType":I
    .end local v3    # "i":I
    .end local v8    # "s":Ljava/lang/String;
    :cond_8
    iget-object v9, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->curParsedWord:Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    invoke-virtual {v9, v1, v7}, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->addResultsFromString(Ljava/lang/String;I)Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;

    move-result-object v4

    .line 346
    .local v4, "newChoice":Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;
    if-eqz v4, :cond_9

    .line 347
    iput-boolean v5, v4, Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;->noSpace:Z

    .line 348
    iput-boolean v6, v4, Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;->noSpaceNumber:Z

    .line 349
    iput-boolean v2, v4, Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;->capitalized:Z

    .line 351
    :cond_9
    return-void
.end method

.method private handleWordInUL(Lcom/vlingo/sdk/internal/xml/XmlAttributes;[C)V
    .locals 15
    .param p1, "attributes"    # Lcom/vlingo/sdk/internal/xml/XmlAttributes;
    .param p2, "cData"    # [C

    .prologue
    .line 261
    if-eqz p2, :cond_1

    new-instance v11, Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-direct {v11, v0}, Ljava/lang/String;-><init>([C)V

    .line 264
    .local v11, "wordString":Ljava/lang/String;
    :goto_0
    const/4 v4, -0x1

    .line 265
    .local v4, "id":I
    const/4 v6, 0x0

    .line 266
    .local v6, "ns":Z
    const/4 v7, 0x0

    .line 267
    .local v7, "nsd":Z
    const/4 v2, 0x0

    .line 269
    .local v2, "ct":Z
    if-eqz p1, :cond_8

    .line 270
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->getLength()I

    move-result v12

    if-ge v3, v12, :cond_8

    .line 271
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->getType(I)I

    move-result v1

    .line 272
    .local v1, "attributeType":I
    iget v12, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_CA:I

    if-ne v12, v1, :cond_3

    .line 273
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->getValue(I)Ljava/lang/String;

    move-result-object v8

    .line 274
    .local v8, "s":Ljava/lang/String;
    if-eqz v8, :cond_2

    const-string/jumbo v12, "t"

    invoke-virtual {v8, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    const/4 v2, 0x1

    .line 270
    .end local v8    # "s":Ljava/lang/String;
    :cond_0
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 261
    .end local v1    # "attributeType":I
    .end local v2    # "ct":Z
    .end local v3    # "i":I
    .end local v4    # "id":I
    .end local v6    # "ns":Z
    .end local v7    # "nsd":Z
    .end local v11    # "wordString":Ljava/lang/String;
    :cond_1
    const-string/jumbo v11, "(null)"

    goto :goto_0

    .line 274
    .restart local v1    # "attributeType":I
    .restart local v2    # "ct":Z
    .restart local v3    # "i":I
    .restart local v4    # "id":I
    .restart local v6    # "ns":Z
    .restart local v7    # "nsd":Z
    .restart local v8    # "s":Ljava/lang/String;
    .restart local v11    # "wordString":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    .line 276
    .end local v8    # "s":Ljava/lang/String;
    :cond_3
    iget v12, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_ID:I

    if-ne v12, v1, :cond_4

    .line 277
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->getValue(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    goto :goto_2

    .line 279
    :cond_4
    iget v12, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_NS:I

    if-ne v12, v1, :cond_6

    .line 280
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->getValue(I)Ljava/lang/String;

    move-result-object v8

    .line 281
    .restart local v8    # "s":Ljava/lang/String;
    if-eqz v8, :cond_5

    const-string/jumbo v12, "t"

    invoke-virtual {v8, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    const/4 v6, 0x1

    :goto_3
    goto :goto_2

    :cond_5
    const/4 v6, 0x0

    goto :goto_3

    .line 283
    .end local v8    # "s":Ljava/lang/String;
    :cond_6
    iget v12, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_NSD:I

    if-ne v12, v1, :cond_0

    .line 284
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->getValue(I)Ljava/lang/String;

    move-result-object v8

    .line 285
    .restart local v8    # "s":Ljava/lang/String;
    if-eqz v8, :cond_7

    const-string/jumbo v12, "t"

    invoke-virtual {v8, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    const/4 v7, 0x1

    :goto_4
    goto :goto_2

    :cond_7
    const/4 v7, 0x0

    goto :goto_4

    .line 289
    .end local v1    # "attributeType":I
    .end local v3    # "i":I
    .end local v8    # "s":Ljava/lang/String;
    :cond_8
    new-instance v5, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    invoke-direct {v5, v11}, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;-><init>(Ljava/lang/String;)V

    .line 290
    .local v5, "newWord":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    iput-boolean v6, v5, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->noSpace:Z

    .line 291
    iput-boolean v7, v5, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->noSpaceNumber:Z

    .line 292
    iput-boolean v2, v5, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->capitalized:Z

    .line 293
    if-ltz v4, :cond_9

    iget-object v12, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    iget-object v12, v12, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->wordList:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    array-length v12, v12

    if-gt v4, v12, :cond_9

    .line 294
    iput v4, v5, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->id:I

    .line 295
    iget-object v12, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    iget-object v12, v12, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->wordList:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aget-object v10, v12, v4

    .line 296
    .local v10, "wordListWord":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    if-eqz v10, :cond_c

    .line 297
    iget-object v12, v10, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iChoices:[Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;

    iput-object v12, v5, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iChoices:[Lcom/vlingo/sdk/internal/recognizer/results/RecChoice;

    .line 298
    iget v12, v10, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iN:I

    iput v12, v5, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->iN:I

    .line 299
    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->adjustChoiceIndex()Z

    .line 304
    .end local v10    # "wordListWord":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    :cond_9
    :goto_5
    iget v12, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->choiceCount:I

    if-nez v12, :cond_a

    .line 307
    iget-object v12, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    iget v13, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->wordCount:I

    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->copy()Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->addNBest(ILcom/vlingo/sdk/internal/recognizer/results/RecNBest;)V

    .line 309
    :cond_a
    iget-object v12, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    iget-object v9, v12, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttList:[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 310
    .local v9, "uttList":[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    iget v12, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->wordCount:I

    iget v13, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->choiceCount:I

    aget-object v13, v9, v13

    array-length v13, v13

    if-ge v12, v13, :cond_b

    .line 311
    iget v12, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->choiceCount:I

    aget-object v12, v9, v12

    iget v13, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->wordCount:I

    add-int/lit8 v14, v13, 0x1

    iput v14, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->wordCount:I

    aput-object v5, v12, v13

    .line 313
    :cond_b
    return-void

    .line 301
    .end local v9    # "uttList":[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    .restart local v10    # "wordListWord":Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    :cond_c
    sget-object v12, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "PAR4 warning: word at index "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " not found"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method private handleWordInWL(Lcom/vlingo/sdk/internal/xml/XmlAttributes;[C)V
    .locals 8
    .param p1, "attributes"    # Lcom/vlingo/sdk/internal/xml/XmlAttributes;
    .param p2, "cData"    # [C

    .prologue
    .line 232
    if-eqz p1, :cond_4

    .line 233
    const/4 v3, 0x0

    .line 234
    .local v3, "n":I
    const/4 v2, -0x1

    .line 236
    .local v2, "id":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->getLength()I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 237
    invoke-virtual {p1, v1}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->getType(I)I

    move-result v0

    .line 238
    .local v0, "attributeType":I
    iget v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_N:I

    if-ne v5, v0, :cond_1

    .line 239
    invoke-virtual {p1, v1}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->getValue(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 236
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 241
    :cond_1
    iget v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_ID:I

    if-ne v5, v0, :cond_0

    .line 242
    invoke-virtual {p1, v1}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->getValue(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 245
    .end local v0    # "attributeType":I
    :cond_2
    new-instance v5, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    invoke-direct {v5, v3}, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;-><init>(I)V

    iput-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->curParsedWord:Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 247
    if-ltz v2, :cond_3

    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    iget-object v5, v5, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->wordList:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    array-length v5, v5

    if-gt v2, v5, :cond_3

    .line 248
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->curParsedWord:Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    iput v2, v5, Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;->id:I

    .line 249
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    iget-object v4, v5, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->wordList:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 250
    .local v4, "wordList":[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    iget-object v5, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->curParsedWord:Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    aput-object v5, v4, v2

    .line 258
    .end local v1    # "i":I
    .end local v2    # "id":I
    .end local v3    # "n":I
    .end local v4    # "wordList":[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    :goto_2
    return-void

    .line 253
    .restart local v1    # "i":I
    .restart local v2    # "id":I
    .restart local v3    # "n":I
    :cond_3
    sget-object v5, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "PAR2 warning: word id "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " out of range"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 256
    .end local v1    # "i":I
    .end local v2    # "id":I
    .end local v3    # "n":I
    :cond_4
    sget-object v5, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "PAR3 warning: no attributes for word "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    iget-object v7, v7, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->wordList:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    array-length v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " in word list"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private newRecResults()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 95
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->curParsedWord:Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 96
    iput v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->choiceCount:I

    .line 97
    iput v2, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->wordCount:I

    .line 99
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    if-eqz v1, :cond_0

    .line 101
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    iget-object v0, v1, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->wordList:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 103
    .local v0, "wordList":[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    new-instance v1, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    const/16 v2, 0xa

    iget-object v3, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->guttid:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;-><init>(ILjava/lang/String;)V

    iput-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    .line 104
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    iput-object v0, v1, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->wordList:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 107
    .end local v0    # "wordList":[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    :cond_0
    return-void
.end method

.method private static parseFloat(Ljava/lang/String;F)F
    .locals 1
    .param p0, "string"    # Ljava/lang/String;
    .param p1, "defaultValue"    # F

    .prologue
    .line 379
    move v0, p1

    .line 380
    .local v0, "result":F
    if-eqz p0, :cond_0

    .line 381
    invoke-static {p0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .line 383
    :cond_0
    return v0
.end method

.method private static parseInt(Ljava/lang/String;I)I
    .locals 1
    .param p0, "string"    # Ljava/lang/String;
    .param p1, "defaultValue"    # I

    .prologue
    .line 388
    move v0, p1

    .line 389
    .local v0, "result":I
    if-eqz p0, :cond_0

    .line 390
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 392
    :cond_0
    return v0
.end method


# virtual methods
.method public beginElement(ILcom/vlingo/sdk/internal/xml/XmlAttributes;[CI)V
    .locals 20
    .param p1, "elementType"    # I
    .param p2, "attributes"    # Lcom/vlingo/sdk/internal/xml/XmlAttributes;
    .param p3, "cData"    # [C
    .param p4, "elementEndPosition"    # I

    .prologue
    .line 111
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_RECOGNITION:I

    move/from16 v17, v0

    move/from16 v0, p1

    move/from16 v1, v17

    if-ne v0, v1, :cond_1

    .line 112
    const/16 v17, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    .line 113
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->choiceCount:I

    .line 114
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->wordCount:I

    .line 115
    const/16 v17, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->curParsedWord:Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 116
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_GUTTID:I

    move/from16 v17, v0

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->guttid:Ljava/lang/String;

    .line 117
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->srResultsParser:Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->getResponse()Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->guttid:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->setGUttId(Ljava/lang/String;)V

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_ALT:I

    move/from16 v17, v0

    move/from16 v0, p1

    move/from16 v1, v17

    if-ne v0, v1, :cond_2

    .line 120
    const/16 v17, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    .line 121
    new-instance v17, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->guttid:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-direct/range {v17 .. v18}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->tagResults:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    goto :goto_0

    .line 123
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_WORD_LIST:I

    move/from16 v17, v0

    move/from16 v0, p1

    move/from16 v1, v17

    if-ne v0, v1, :cond_5

    .line 124
    if-eqz p2, :cond_4

    .line 125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-object/from16 v17, v0

    if-nez v17, :cond_3

    .line 126
    new-instance v17, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    const/16 v18, 0xa

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->guttid:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-direct/range {v17 .. v19}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;-><init>(ILjava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    .line 127
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_N:I

    move/from16 v17, v0

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->parseInt(Ljava/lang/String;I)I

    move-result v7

    .line 128
    .local v7, "numNBests":I
    if-lez v7, :cond_0

    .line 129
    new-array v0, v7, [Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    move-object/from16 v16, v0

    .line 130
    .local v16, "wordList":[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->wordList:[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 131
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    if-ge v5, v7, :cond_0

    .line 132
    const/16 v17, 0x0

    aput-object v17, v16, v5

    .line 131
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 135
    .end local v5    # "i":I
    .end local v7    # "numNBests":I
    .end local v16    # "wordList":[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    :cond_4
    sget-object v17, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->TAG:Ljava/lang/String;

    const-string/jumbo v18, "PAR1 warning: no attributes for word list"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 138
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_UTT_LIST:I

    move/from16 v17, v0

    move/from16 v0, p1

    move/from16 v1, v17

    if-ne v0, v1, :cond_8

    .line 140
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-object/from16 v17, v0

    if-nez v17, :cond_6

    .line 141
    new-instance v17, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    const/16 v18, 0xa

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->guttid:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-direct/range {v17 .. v19}, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;-><init>(ILjava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    .line 142
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_N:I

    move/from16 v17, v0

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->parseInt(Ljava/lang/String;I)I

    move-result v9

    .line 143
    .local v9, "numSentences":I
    if-lez v9, :cond_7

    .line 144
    new-array v13, v9, [[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 145
    .local v13, "uttList":[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    new-array v15, v9, [F

    .line 146
    .local v15, "uttListConf":[F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iput-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttList:[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 147
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iput-object v15, v0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttListConf:[F

    .line 148
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_2
    if-ge v5, v9, :cond_7

    .line 149
    const/16 v17, 0x0

    aput-object v17, v13, v5

    .line 148
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 152
    .end local v5    # "i":I
    .end local v13    # "uttList":[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    .end local v15    # "uttListConf":[F
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->choiceIndex:I

    .line 153
    const/16 v17, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->curParsedWord:Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 154
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->choiceCount:I

    goto/16 :goto_0

    .line 156
    .end local v9    # "numSentences":I
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_TAG_LIST:I

    move/from16 v17, v0

    move/from16 v0, p1

    move/from16 v1, v17

    if-ne v0, v1, :cond_9

    .line 157
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_N:I

    move/from16 v17, v0

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->parseInt(Ljava/lang/String;I)I

    move-result v10

    .line 158
    .local v10, "numTagList":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->tagResults:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->onTagList(I)V

    goto/16 :goto_0

    .line 160
    .end local v10    # "numTagList":I
    :cond_9
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_PARSE_GRP:I

    move/from16 v17, v0

    move/from16 v0, p1

    move/from16 v1, v17

    if-ne v0, v1, :cond_a

    .line 161
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_C:I

    move/from16 v17, v0

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v17

    const/high16 v18, -0x40800000    # -1.0f

    invoke-static/range {v17 .. v18}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->parseFloat(Ljava/lang/String;F)F

    move-result v4

    .line 162
    .local v4, "confidence":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_TYPE:I

    move/from16 v17, v0

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v12

    .line 163
    .local v12, "parseType":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_N:I

    move/from16 v17, v0

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->parseInt(Ljava/lang/String;I)I

    move-result v8

    .line 164
    .local v8, "numParseGroups":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->tagResults:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v12, v8}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->onParseGroup(FLjava/lang/String;I)V

    goto/16 :goto_0

    .line 166
    .end local v4    # "confidence":F
    .end local v8    # "numParseGroups":I
    .end local v12    # "parseType":Ljava/lang/String;
    :cond_a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_TAG:I

    move/from16 v17, v0

    move/from16 v0, p1

    move/from16 v1, v17

    if-ne v0, v1, :cond_d

    .line 167
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->newRecResults()V

    .line 168
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_NAME:I

    move/from16 v17, v0

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v6

    .line 169
    .local v6, "name":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_CF:I

    move/from16 v17, v0

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v3

    .line 170
    .local v3, "cannonical":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->ATTRIBUTE_N:I

    move/from16 v17, v0

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->parseInt(Ljava/lang/String;I)I

    move-result v11

    .line 171
    .local v11, "numTagSentences":I
    if-lez v11, :cond_c

    .line 172
    new-array v13, v11, [[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 173
    .restart local v13    # "uttList":[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    new-array v15, v11, [F

    .line 174
    .restart local v15    # "uttListConf":[F
    new-array v14, v11, [Ljava/lang/String;

    .line 175
    .local v14, "uttListCannonical":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iput-object v13, v0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttList:[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 176
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iput-object v15, v0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttListConf:[F

    .line 177
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iput-object v14, v0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttListCannonical:[Ljava/lang/String;

    .line 178
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_3
    if-ge v5, v11, :cond_b

    .line 179
    const/16 v17, 0x0

    aput-object v17, v13, v5

    .line 178
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 181
    :cond_b
    if-eqz v3, :cond_c

    .line 185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttListCannonical:[Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-object v3, v17, v18

    .line 188
    .end local v5    # "i":I
    .end local v13    # "uttList":[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;
    .end local v14    # "uttListCannonical":[Ljava/lang/String;
    .end local v15    # "uttListConf":[F
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->tagResults:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->onTag(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 190
    .end local v3    # "cannonical":Ljava/lang/String;
    .end local v6    # "name":Ljava/lang/String;
    .end local v11    # "numTagSentences":I
    :cond_d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_WORD:I

    move/from16 v17, v0

    move/from16 v0, p1

    move/from16 v1, v17

    if-ne v0, v1, :cond_f

    .line 191
    const/16 v17, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->curParsedWord:Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    .line 192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttList:[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    move-object/from16 v17, v0

    if-eqz v17, :cond_e

    .line 194
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->handleWordInUL(Lcom/vlingo/sdk/internal/xml/XmlAttributes;[C)V

    goto/16 :goto_0

    .line 197
    :cond_e
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->handleWordInWL(Lcom/vlingo/sdk/internal/xml/XmlAttributes;[C)V

    goto/16 :goto_0

    .line 201
    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_TAG_CHOICE:I

    move/from16 v17, v0

    move/from16 v0, p1

    move/from16 v1, v17

    if-eq v0, v1, :cond_10

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_CHOICE:I

    move/from16 v17, v0

    move/from16 v0, p1

    move/from16 v1, v17

    if-ne v0, v1, :cond_0

    .line 202
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttList:[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    move-object/from16 v17, v0

    if-eqz v17, :cond_11

    .line 204
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->handleChoiceInUL(Lcom/vlingo/sdk/internal/xml/XmlAttributes;[C)V

    goto/16 :goto_0

    .line 205
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->curParsedWord:Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    .line 206
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->handleChoiceInWL(Lcom/vlingo/sdk/internal/xml/XmlAttributes;[C)V

    goto/16 :goto_0
.end method

.method public endElement(II)V
    .locals 2
    .param p1, "elementType"    # I
    .param p2, "elementStartPosition"    # I

    .prologue
    .line 214
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_ALT:I

    if-ne v0, p1, :cond_1

    .line 215
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->srResultsParser:Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/results/SRResponseParser;->getResponse()Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->tagResults:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/results/SRRecognitionResponse;->setTaggedResults(Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;)V

    .line 216
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->responseParser:Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/vlservice/response/VLResponseParser;->onSectionComplete()V

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_UTT_LIST:I

    if-ne v0, p1, :cond_2

    .line 219
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->tagResults:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->onUttResults(Lcom/vlingo/sdk/internal/recognizer/results/RecResults;)V

    goto :goto_0

    .line 221
    :cond_2
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_TAG:I

    if-ne v0, p1, :cond_3

    .line 222
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->tagResults:Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResults;->onTagResults(Lcom/vlingo/sdk/internal/recognizer/results/RecResults;)V

    goto :goto_0

    .line 224
    :cond_3
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_CHOICE:I

    if-eq v0, p1, :cond_4

    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_TAG_CHOICE:I

    if-ne v0, p1, :cond_0

    .line 225
    :cond_4
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->recResults:Lcom/vlingo/sdk/internal/recognizer/results/RecResults;

    iget-object v0, v0, Lcom/vlingo/sdk/internal/recognizer/results/RecResults;->uttList:[[Lcom/vlingo/sdk/internal/recognizer/results/RecNBest;

    if-eqz v0, :cond_0

    .line 226
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->choiceCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->choiceCount:I

    goto :goto_0
.end method

.method public handlesElement(I)Z
    .locals 1
    .param p1, "elementType"    # I

    .prologue
    .line 91
    iget v0, p0, Lcom/vlingo/sdk/internal/recognizer/results/TaggedResultsParser;->RESULT_RECOGNITION:I

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
