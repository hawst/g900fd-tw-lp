.class public abstract Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;
.super Ljava/lang/Object;
.source "AbstractRule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$TtaAbstractRulesListBuilder;
    }
.end annotation


# static fields
.field public static final DEFAULT_LANG_NAME:Ljava/lang/String; = "default"

.field public static final THRESHOLD_TAG_NAME:Ljava/lang/String; = "Threshold"

.field public static final TTA_RULE_SET_TAG_NAME:Ljava/lang/String; = "RuleSet"


# instance fields
.field protected arbitrationInfoMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;",
            ">;"
        }
    .end annotation
.end field

.field protected fieldId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    return-void
.end method

.method public static getAbstractRulesListBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 51
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$TtaAbstractRulesListBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$TtaAbstractRulesListBuilder;-><init>(Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$1;)V

    return-object v0
.end method

.method public static getThresholdBuilder()Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/EmbeddedRecognizerSettingsTagBuilder",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/sdk/internal/recognizer/embedded/xml/ParameterTag;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 128
    new-instance v0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$1;

    invoke-direct {v0}, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule$1;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getArbitrationInfo(Ljava/lang/String;)Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;
    .locals 3
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 42
    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;->arbitrationInfoMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;

    .line 43
    .local v0, "arbitrationInfo":Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;
    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;->arbitrationInfoMap:Ljava/util/Map;

    const-string/jumbo v2, "default"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;

    :goto_0
    return-object v1

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method

.method public getArbitrationInfoMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;->arbitrationInfoMap:Ljava/util/Map;

    return-object v0
.end method

.method public getFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;->fieldId:Ljava/lang/String;

    return-object v0
.end method

.method public getTemplateName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    return-object v0
.end method

.method public setArbitrationInfoMap(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p1, "arbitrationInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/sdk/internal/recognizer/arbiter/ThresholdInfo;>;"
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;->arbitrationInfoMap:Ljava/util/Map;

    .line 39
    return-void
.end method

.method public setFieldId(Ljava/lang/String;)V
    .locals 0
    .param p1, "fieldId"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/vlingo/sdk/internal/recognizer/embedded/xml/AbstractRule;->fieldId:Ljava/lang/String;

    .line 35
    return-void
.end method
