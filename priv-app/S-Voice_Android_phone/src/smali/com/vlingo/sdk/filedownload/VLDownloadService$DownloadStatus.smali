.class public final enum Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;
.super Ljava/lang/Enum;
.source "VLDownloadService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/filedownload/VLDownloadService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DownloadStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

.field public static final enum FAILED:Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

.field public static final enum PAUSED:Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

.field public static final enum PENDING:Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

.field public static final enum RUNNING:Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

.field private static final STATUSES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum SUCCESSFUL:Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;


# instance fields
.field private index:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x0

    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 146
    new-instance v5, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    const-string/jumbo v6, "FAILED"

    const/16 v7, 0x10

    invoke-direct {v5, v6, v11, v7}, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;->FAILED:Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    .line 147
    new-instance v5, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    const-string/jumbo v6, "PAUSED"

    invoke-direct {v5, v6, v8, v10}, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;->PAUSED:Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    .line 148
    new-instance v5, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    const-string/jumbo v6, "PENDING"

    invoke-direct {v5, v6, v9, v8}, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;->PENDING:Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    .line 149
    new-instance v5, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    const-string/jumbo v6, "RUNNING"

    invoke-direct {v5, v6, v12, v9}, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;->RUNNING:Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    .line 150
    new-instance v5, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    const-string/jumbo v6, "SUCCESSFUL"

    const/16 v7, 0x8

    invoke-direct {v5, v6, v10, v7}, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;->SUCCESSFUL:Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    .line 145
    const/4 v5, 0x5

    new-array v5, v5, [Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    sget-object v6, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;->FAILED:Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    aput-object v6, v5, v11

    sget-object v6, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;->PAUSED:Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    aput-object v6, v5, v8

    sget-object v6, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;->PENDING:Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    aput-object v6, v5, v9

    sget-object v6, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;->RUNNING:Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    aput-object v6, v5, v12

    sget-object v6, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;->SUCCESSFUL:Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    aput-object v6, v5, v10

    sput-object v5, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;->$VALUES:[Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    .line 155
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 156
    .local v4, "statusMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;>;"
    invoke-static {}, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;->values()[Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    move-result-object v0

    .local v0, "arr$":[Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 157
    .local v3, "status":Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;
    invoke-virtual {v3}, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;->getIndex()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 159
    .end local v3    # "status":Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;
    :cond_0
    sput-object v4, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;->STATUSES:Ljava/util/Map;

    .line 160
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 163
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 164
    iput p3, p0, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;->index:I

    .line 165
    return-void
.end method

.method public static getStatusByIndex(I)Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;
    .locals 2
    .param p0, "index"    # I

    .prologue
    .line 172
    sget-object v0, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;->STATUSES:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 145
    const-class v0, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;
    .locals 1

    .prologue
    .line 145
    sget-object v0, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;->$VALUES:[Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    invoke-virtual {v0}, [Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;

    return-object v0
.end method


# virtual methods
.method public getIndex()I
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lcom/vlingo/sdk/filedownload/VLDownloadService$DownloadStatus;->index:I

    return v0
.end method
