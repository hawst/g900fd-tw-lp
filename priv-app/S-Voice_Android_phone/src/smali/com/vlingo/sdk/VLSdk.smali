.class public final Lcom/vlingo/sdk/VLSdk;
.super Ljava/lang/Object;
.source "VLSdk.java"


# static fields
.field public static final EMBEDDED_RECOGNIZER_SUPPORTED:Ljava/lang/String; = "false"

.field public static final INCLUDE_LTS_ASSERTS:Ljava/lang/String; = "false"

.field public static final INCLUDE_LTS_ASSERTS_FALSE:Ljava/lang/String; = "false"

.field public static VERSION:Ljava/lang/String;

.field private static smAssetsExtracted:Z

.field private static smInstance:Lcom/vlingo/sdk/VLSdk;


# instance fields
.field private mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

.field private mDebugSettings:Lcom/vlingo/sdk/util/SDKDebugSettings;

.field private mIsInvalid:Z

.field private mLocationOn:Z

.field private mLogServerURI:Ljava/lang/String;

.field private mRecoServerURI:Ljava/lang/String;

.field private mSdkHandler:Landroid/os/Handler;

.field private mTTSServerURI:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const-string/jumbo v0, "2.0.1111"

    sput-object v0, Lcom/vlingo/sdk/VLSdk;->VERSION:Ljava/lang/String;

    .line 84
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/sdk/VLSdk;->smAssetsExtracted:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/util/SDKDebugSettings;Z)V
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appId"    # Ljava/lang/String;
    .param p3, "appName"    # Ljava/lang/String;
    .param p4, "appVersion"    # Ljava/lang/String;
    .param p5, "salesCode"    # Ljava/lang/String;
    .param p6, "locationOn"    # Z
    .param p7, "recoServerURI"    # Ljava/lang/String;
    .param p8, "ttsServerURI"    # Ljava/lang/String;
    .param p9, "logServerURI"    # Ljava/lang/String;
    .param p10, "helloServerURI"    # Ljava/lang/String;
    .param p11, "lmttServerURI"    # Ljava/lang/String;
    .param p12, "debugSettings"    # Lcom/vlingo/sdk/util/SDKDebugSettings;
    .param p13, "edmEnabled"    # Z

    .prologue
    .line 250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/sdk/VLSdk;->mIsInvalid:Z

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    move-object/from16 v9, p11

    move/from16 v10, p6

    move-object/from16 v11, p12

    move-object/from16 v12, p5

    move/from16 v13, p13

    .line 255
    invoke-direct/range {v0 .. v13}, Lcom/vlingo/sdk/VLSdk;->updateSettings(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/sdk/util/SDKDebugSettings;Ljava/lang/String;Z)V

    .line 258
    invoke-static/range {p1 .. p1}, Lcom/vlingo/sdk/VLSdk;->extractAssetsToFiles(Landroid/content/Context;)V

    .line 260
    new-instance v14, Landroid/os/HandlerThread;

    const-string/jumbo v0, "SDKWorker"

    invoke-direct {v14, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 261
    .local v14, "ht":Landroid/os/HandlerThread;
    invoke-virtual {v14}, Landroid/os/HandlerThread;->start()V

    .line 262
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {v14}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mSdkHandler:Landroid/os/Handler;

    .line 264
    new-instance v0, Lcom/vlingo/sdk/internal/VLComponentManager;

    iget-object v1, p0, Lcom/vlingo/sdk/VLSdk;->mSdkHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/vlingo/sdk/internal/VLComponentManager;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    .line 265
    return-void
.end method

.method public static declared-synchronized create(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/util/SDKDebugSettings;Z)Lcom/vlingo/sdk/VLSdk;
    .locals 16
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "appVersion"    # Ljava/lang/String;
    .param p4, "salesCode"    # Ljava/lang/String;
    .param p5, "locationOn"    # Z
    .param p6, "recoServerURI"    # Ljava/lang/String;
    .param p7, "ttsServerURI"    # Ljava/lang/String;
    .param p8, "logServerURI"    # Ljava/lang/String;
    .param p9, "helloServerURI"    # Ljava/lang/String;
    .param p10, "lmttServerURI"    # Ljava/lang/String;
    .param p11, "debugSettings"    # Lcom/vlingo/sdk/util/SDKDebugSettings;
    .param p12, "edmEnabled"    # Z

    .prologue
    .line 118
    const-class v15, Lcom/vlingo/sdk/VLSdk;

    monitor-enter v15

    :try_start_0
    sget-object v1, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;

    if-eqz v1, :cond_0

    .line 119
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "singleton VLSdk already exists!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    :catchall_0
    move-exception v1

    monitor-exit v15

    throw v1

    .line 121
    :cond_0
    :try_start_1
    new-instance v1, Lcom/vlingo/sdk/VLSdk;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move/from16 v14, p12

    invoke-direct/range {v1 .. v14}, Lcom/vlingo/sdk/VLSdk;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/util/SDKDebugSettings;Z)V

    sput-object v1, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;

    .line 123
    sget-object v1, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;

    iget-object v1, v1, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/VLComponentManager;->getEmbeddedDialogManager()Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;

    move-result-object v1

    move/from16 v0, p12

    invoke-interface {v1, v0}, Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;->setEnabled(Z)V

    .line 124
    if-eqz p12, :cond_1

    .line 125
    sget-object v1, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;

    iget-object v1, v1, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/VLComponentManager;->getEmbeddedDialogManager()Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;->backgroundInit()V

    .line 127
    :cond_1
    sget-object v1, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v15

    return-object v1
.end method

.method public static declared-synchronized destroy()V
    .locals 2

    .prologue
    .line 338
    const-class v1, Lcom/vlingo/sdk/VLSdk;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;

    if-eqz v0, :cond_0

    .line 339
    sget-object v0, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;

    invoke-direct {v0}, Lcom/vlingo/sdk/VLSdk;->destroyInternal()V

    .line 341
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 342
    monitor-exit v1

    return-void

    .line 338
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static destroyAllExceptComponents()V
    .locals 0

    .prologue
    .line 166
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->destroy()V

    .line 167
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->destroy()V

    .line 168
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->destroy()V

    .line 170
    invoke-static {}, Lcom/vlingo/sdk/internal/audio/AudioDevice;->destroy()V

    .line 171
    invoke-static {}, Lcom/vlingo/sdk/internal/http/HttpManager;->destroy()V

    .line 172
    invoke-static {}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->destroy()V

    .line 173
    invoke-static {}, Lcom/vlingo/sdk/internal/location/LocationUtils;->destroy()V

    .line 174
    invoke-static {}, Lcom/vlingo/sdk/internal/util/TimerSingleton;->destroy()V

    .line 175
    invoke-static {}, Lcom/vlingo/sdk/internal/net/ConnectionManager;->destroy()V

    .line 176
    invoke-static {}, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManagerSingleton;->cleanup()V

    .line 177
    invoke-static {}, Lcom/vlingo/sdk/internal/http/cookies/CookieJarFactory;->cleanup()V

    .line 178
    return-void
.end method

.method private destroyInternal()V
    .locals 2

    .prologue
    .line 345
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 347
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    iget-object v0, v0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->destroyAll()V

    .line 349
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->destroyAllExceptComponents()V

    .line 351
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    iget-object v0, v0, Lcom/vlingo/sdk/VLSdk;->mSdkHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 352
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/vlingo/sdk/VLSdk;->mSdkHandler:Landroid/os/Handler;

    .line 354
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/vlingo/sdk/VLSdk;->mIsInvalid:Z

    .line 355
    return-void
.end method

.method public static doesEmbeddedRecognizerSupported()Z
    .locals 1

    .prologue
    .line 571
    const-string/jumbo v0, "false"

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static declared-synchronized extractAssetsToFiles(Landroid/content/Context;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 272
    const-class v6, Lcom/vlingo/sdk/VLSdk;

    monitor-enter v6

    :try_start_0
    sget-boolean v5, Lcom/vlingo/sdk/VLSdk;->smAssetsExtracted:Z

    if-nez v5, :cond_3

    .line 275
    invoke-static {}, Lcom/vlingo/sdk/internal/settings/Settings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 276
    .local v2, "editor":Landroid/content/SharedPreferences$Editor;
    const/4 v1, 0x0

    .line 278
    .local v1, "doCommit":Z
    const-string/jumbo v5, "vlsdk_lib"

    const/4 v7, 0x0

    invoke-virtual {p0, v5, v7}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    .line 279
    .local v3, "libDir":Ljava/io/File;
    const-string/jumbo v5, "vlsdk_lib_s2.0.zip"

    invoke-static {v5, v3, v2}, Lcom/vlingo/sdk/internal/util/FileUtils;->extractAssetZipIfNeeded(Ljava/lang/String;Ljava/io/File;Landroid/content/SharedPreferences$Editor;)Z

    move-result v5

    or-int/2addr v1, v5

    .line 281
    const-string/jumbo v5, "vlsdk_raw"

    const/4 v7, 0x0

    invoke-virtual {p0, v5, v7}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v4

    .line 283
    .local v4, "rawDir":Ljava/io/File;
    const-string/jumbo v0, "vlsdk_acoustic_raw_s2.0.zip"

    .line 284
    .local v0, "acousticRawFilename":Ljava/lang/String;
    const-string/jumbo v5, "false"

    const-string/jumbo v7, "false"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 288
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "test_"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/sdk/internal/util/FileUtils;->doesAssetFileExist(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 289
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "test_"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 304
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/vlingo/sdk/internal/util/FileUtils;->doesAssetFileExist(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 314
    :cond_1
    :goto_1
    const-string/jumbo v5, "false"

    const-string/jumbo v7, "false"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 315
    const-string/jumbo v5, "vlsdk_lts_raw.zip"

    invoke-static {v5, v4, v2}, Lcom/vlingo/sdk/internal/util/FileUtils;->extractAssetZipIfNeeded(Ljava/lang/String;Ljava/io/File;Landroid/content/SharedPreferences$Editor;)Z

    move-result v5

    or-int/2addr v1, v5

    .line 318
    :cond_2
    const/4 v5, 0x1

    sput-boolean v5, Lcom/vlingo/sdk/VLSdk;->smAssetsExtracted:Z

    .line 319
    if-eqz v1, :cond_3

    .line 320
    invoke-static {v2}, Lcom/vlingo/sdk/internal/settings/Settings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 328
    .end local v0    # "acousticRawFilename":Ljava/lang/String;
    .end local v1    # "doCommit":Z
    .end local v2    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v3    # "libDir":Ljava/io/File;
    .end local v4    # "rawDir":Ljava/io/File;
    :cond_3
    monitor-exit v6

    return-void

    .line 294
    .restart local v0    # "acousticRawFilename":Ljava/lang/String;
    .restart local v1    # "doCommit":Z
    .restart local v2    # "editor":Landroid/content/SharedPreferences$Editor;
    .restart local v3    # "libDir":Ljava/io/File;
    .restart local v4    # "rawDir":Ljava/io/File;
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 297
    :cond_5
    :try_start_1
    invoke-static {v0}, Lcom/vlingo/sdk/internal/util/FileUtils;->doesAssetFileExist(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 300
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "test_"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 311
    :cond_6
    invoke-static {v0, v4, v2}, Lcom/vlingo/sdk/internal/util/FileUtils;->extractAssetZipIfNeeded(Ljava/lang/String;Ljava/io/File;Landroid/content/SharedPreferences$Editor;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    or-int/2addr v1, v5

    goto :goto_1

    .line 272
    .end local v0    # "acousticRawFilename":Ljava/lang/String;
    .end local v1    # "doCommit":Z
    .end local v2    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v3    # "libDir":Ljava/io/File;
    .end local v4    # "rawDir":Ljava/io/File;
    :catchall_0
    move-exception v5

    monitor-exit v6

    throw v5
.end method

.method public static getInstance()Lcom/vlingo/sdk/VLSdk;
    .locals 2

    .prologue
    .line 242
    sget-object v0, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;

    if-nez v0, :cond_0

    .line 243
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "VLSdk not initialized!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 245
    :cond_0
    sget-object v0, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;

    return-object v0
.end method

.method private static partlyDestroyOnReload()V
    .locals 1

    .prologue
    .line 158
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 160
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    iget-object v0, v0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->destroySettingsDependentComponents()V

    .line 162
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->destroyAllExceptComponents()V

    .line 163
    return-void
.end method

.method public static reload(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/sdk/util/SDKDebugSettings;Z)Lcom/vlingo/sdk/VLSdk;
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "appVersion"    # Ljava/lang/String;
    .param p4, "salesCode"    # Ljava/lang/String;
    .param p5, "locationOn"    # Z
    .param p6, "recoServerURI"    # Ljava/lang/String;
    .param p7, "ttsServerURI"    # Ljava/lang/String;
    .param p8, "logServerURI"    # Ljava/lang/String;
    .param p9, "helloServerURI"    # Ljava/lang/String;
    .param p10, "lmttServerURI"    # Ljava/lang/String;
    .param p11, "debugSettings"    # Lcom/vlingo/sdk/util/SDKDebugSettings;
    .param p12, "edmEnabled"    # Z

    .prologue
    .line 149
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->partlyDestroyOnReload()V

    .line 151
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    move/from16 v10, p5

    move-object/from16 v11, p11

    move-object/from16 v12, p4

    move/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/vlingo/sdk/VLSdk;->updateSettings(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/sdk/util/SDKDebugSettings;Ljava/lang/String;Z)V

    .line 154
    sget-object v0, Lcom/vlingo/sdk/VLSdk;->smInstance:Lcom/vlingo/sdk/VLSdk;

    return-object v0
.end method

.method private updateSettings(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/sdk/util/SDKDebugSettings;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appId"    # Ljava/lang/String;
    .param p3, "appName"    # Ljava/lang/String;
    .param p4, "appVersion"    # Ljava/lang/String;
    .param p5, "recoServerURI"    # Ljava/lang/String;
    .param p6, "ttsServerURI"    # Ljava/lang/String;
    .param p7, "logServerURI"    # Ljava/lang/String;
    .param p8, "helloServerURI"    # Ljava/lang/String;
    .param p9, "lmttServerURI"    # Ljava/lang/String;
    .param p10, "locationOn"    # Z
    .param p11, "debugSettings"    # Lcom/vlingo/sdk/util/SDKDebugSettings;
    .param p12, "salesCode"    # Ljava/lang/String;
    .param p13, "edmEnabled"    # Z

    .prologue
    .line 184
    if-nez p1, :cond_0

    .line 185
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "context cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 187
    :cond_0
    invoke-static {p2}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 188
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "appId must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 190
    :cond_1
    invoke-static {p3}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 191
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "appName must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 193
    :cond_2
    invoke-static {p4}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 194
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "appVersion must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 196
    :cond_3
    invoke-static {p5}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 197
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "recoServerURI must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 199
    :cond_4
    invoke-static {p6}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 200
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "ttsServerURI must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_5
    invoke-static {p7}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 203
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "logServerURI must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 205
    :cond_6
    invoke-static {p8}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 206
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "helloServerURI must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 208
    :cond_7
    invoke-static {p9}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 209
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "lmttServerURI must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 212
    :cond_8
    iput-object p5, p0, Lcom/vlingo/sdk/VLSdk;->mRecoServerURI:Ljava/lang/String;

    .line 213
    iput-object p6, p0, Lcom/vlingo/sdk/VLSdk;->mTTSServerURI:Ljava/lang/String;

    .line 214
    iput-object p7, p0, Lcom/vlingo/sdk/VLSdk;->mLogServerURI:Ljava/lang/String;

    .line 215
    iput-boolean p10, p0, Lcom/vlingo/sdk/VLSdk;->mLocationOn:Z

    .line 216
    iput-object p11, p0, Lcom/vlingo/sdk/VLSdk;->mDebugSettings:Lcom/vlingo/sdk/util/SDKDebugSettings;

    .line 218
    invoke-static {p5}, Lcom/vlingo/sdk/internal/AndroidServerDetails;->setServerName(Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mTTSServerURI:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/AndroidServerDetails;->setTTSServerName(Ljava/lang/String;)V

    .line 220
    invoke-static {p7}, Lcom/vlingo/sdk/internal/AndroidServerDetails;->setLogServerName(Ljava/lang/String;)V

    .line 221
    invoke-static {p8}, Lcom/vlingo/sdk/internal/AndroidServerDetails;->setHelloServerName(Ljava/lang/String;)V

    .line 222
    invoke-static {p9}, Lcom/vlingo/sdk/internal/AndroidServerDetails;->setLMTTServerName(Ljava/lang/String;)V

    .line 223
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->init(Landroid/content/Context;)V

    .line 224
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->setAppId(Ljava/lang/String;)V

    .line 225
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->setAppName(Ljava/lang/String;)V

    .line 226
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->setAppVersion(Ljava/lang/String;)V

    .line 227
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v0

    invoke-virtual {v0, p12}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->setSalesCode(Ljava/lang/String;)V

    .line 229
    const-class v0, Lcom/vlingo/sdk/internal/vlservice/AndroidVLServiceCookieManager;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/http/cookies/CookieJarManagerSingleton;->setCookieJarManagerImpl(Ljava/lang/Class;)V

    .line 230
    const-class v0, Lcom/vlingo/sdk/internal/http/cookies/AndroidCookieJar;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/http/cookies/CookieJarFactory;->setCookieJarImpl(Ljava/lang/Class;)V

    .line 231
    return-void
.end method

.method private static validateInstance()V
    .locals 2

    .prologue
    .line 575
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    iget-boolean v0, v0, Lcom/vlingo/sdk/VLSdk;->mIsInvalid:Z

    if-eqz v0, :cond_0

    .line 576
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "VLSdk instance is no longer valid!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 578
    :cond_0
    return-void
.end method


# virtual methods
.method public appAllowsLocationSending()Z
    .locals 1

    .prologue
    .line 523
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 524
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->isChinesePhone()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public doesUseEDM()Z
    .locals 1

    .prologue
    .line 567
    invoke-virtual {p0}, Lcom/vlingo/sdk/VLSdk;->getEmbeddedDialogManager()Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 551
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getAppId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 555
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 559
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/SoftwareMeta;->getVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDebugSettings()Lcom/vlingo/sdk/util/SDKDebugSettings;
    .locals 1

    .prologue
    .line 563
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mDebugSettings:Lcom/vlingo/sdk/util/SDKDebugSettings;

    return-object v0
.end method

.method public getDeviceID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 541
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 542
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getDeviceID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEmbeddedDialogManager()Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;
    .locals 2

    .prologue
    .line 397
    const/4 v0, 0x0

    .line 398
    .local v0, "result":Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 399
    iget-object v1, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    if-eqz v1, :cond_0

    .line 400
    iget-object v1, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/VLComponentManager;->getEmbeddedDialogManager()Lcom/vlingo/sdk/edm/VLEmbeddedDialogManager;

    move-result-object v0

    .line 403
    :cond_0
    return-object v0
.end method

.method public getEmbeddedTrainer()Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainer;
    .locals 2

    .prologue
    .line 380
    const/4 v0, 0x0

    .line 381
    .local v0, "result":Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainer;
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 382
    iget-object v1, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    if-eqz v1, :cond_0

    .line 383
    iget-object v1, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/VLComponentManager;->getEmbeddedTrainer()Lcom/vlingo/sdk/trainingembedded/VLEmbeddedTrainer;

    move-result-object v0

    .line 386
    :cond_0
    return-object v0
.end method

.method public getLocationOn()Z
    .locals 1

    .prologue
    .line 514
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 515
    iget-boolean v0, p0, Lcom/vlingo/sdk/VLSdk;->mLocationOn:Z

    return v0
.end method

.method public getLogServerURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 501
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 502
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mLogServerURI:Ljava/lang/String;

    return-object v0
.end method

.method public getRecoServerURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 479
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 480
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mRecoServerURI:Ljava/lang/String;

    return-object v0
.end method

.method public getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;
    .locals 2

    .prologue
    .line 364
    const/4 v0, 0x0

    .line 365
    .local v0, "recognizer":Lcom/vlingo/sdk/recognition/VLRecognizer;
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 366
    iget-object v1, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    if-eqz v1, :cond_0

    .line 367
    iget-object v1, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v1}, Lcom/vlingo/sdk/internal/VLComponentManager;->getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;

    move-result-object v0

    .line 370
    :cond_0
    return-object v0
.end method

.method public getSpeakerID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 546
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 547
    invoke-static {}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getInstance()Lcom/vlingo/sdk/internal/recognizer/ClientMeta;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/recognizer/ClientMeta;->getSpeakerID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSpeechDetector()Lcom/vlingo/sdk/recognition/speech/VLSpeechDetector;
    .locals 1

    .prologue
    .line 424
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 425
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->getSpeechDetector()Lcom/vlingo/sdk/recognition/speech/VLSpeechDetector;

    move-result-object v0

    return-object v0
.end method

.method public getSpotter()Lcom/vlingo/sdk/recognition/spotter/VLSpotter;
    .locals 1

    .prologue
    .line 413
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 414
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->getSpotter()Lcom/vlingo/sdk/recognition/spotter/VLSpotter;

    move-result-object v0

    return-object v0
.end method

.method public getTTSServerURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 490
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 491
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mTTSServerURI:Ljava/lang/String;

    return-object v0
.end method

.method public getTextToSpeech()Lcom/vlingo/sdk/tts/VLTextToSpeech;
    .locals 1

    .prologue
    .line 446
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 447
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->getTextToSpeech()Lcom/vlingo/sdk/tts/VLTextToSpeech;

    move-result-object v0

    return-object v0
.end method

.method public getTrainer()Lcom/vlingo/sdk/training/VLTrainer;
    .locals 1

    .prologue
    .line 435
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 436
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->getTrainer()Lcom/vlingo/sdk/training/VLTrainer;

    move-result-object v0

    return-object v0
.end method

.method public getVLDownloadService()Lcom/vlingo/sdk/filedownload/VLDownloadService;
    .locals 1

    .prologue
    .line 468
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 469
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->getDownloadService()Lcom/vlingo/sdk/filedownload/VLDownloadService;

    move-result-object v0

    return-object v0
.end method

.method public getVLServices()Lcom/vlingo/sdk/services/VLServices;
    .locals 1

    .prologue
    .line 457
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->validateInstance()V

    .line 458
    iget-object v0, p0, Lcom/vlingo/sdk/VLSdk;->mComponentManager:Lcom/vlingo/sdk/internal/VLComponentManager;

    invoke-virtual {v0}, Lcom/vlingo/sdk/internal/VLComponentManager;->getVLServices()Lcom/vlingo/sdk/services/VLServices;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 533
    sget-object v0, Lcom/vlingo/sdk/VLSdk;->VERSION:Ljava/lang/String;

    return-object v0
.end method
