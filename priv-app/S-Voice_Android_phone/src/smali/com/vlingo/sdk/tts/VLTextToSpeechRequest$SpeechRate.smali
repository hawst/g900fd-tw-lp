.class public final enum Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;
.super Ljava/lang/Enum;
.source "VLTextToSpeechRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SpeechRate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

.field public static final enum FAST:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

.field public static final enum NORMAL:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

.field public static final enum SLOW:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

.field public static final enum VERY_FAST:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

.field public static final enum VERY_SLOW:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    const-string/jumbo v1, "VERY_SLOW"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;->VERY_SLOW:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    new-instance v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    const-string/jumbo v1, "SLOW"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;->SLOW:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    new-instance v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    const-string/jumbo v1, "NORMAL"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;->NORMAL:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    new-instance v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    const-string/jumbo v1, "FAST"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;->FAST:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    new-instance v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    const-string/jumbo v1, "VERY_FAST"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;->VERY_FAST:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    sget-object v1, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;->VERY_SLOW:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;->SLOW:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;->NORMAL:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;->FAST:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;->VERY_FAST:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;->$VALUES:[Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 24
    const-class v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;->$VALUES:[Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    invoke-virtual {v0}, [Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    return-object v0
.end method
