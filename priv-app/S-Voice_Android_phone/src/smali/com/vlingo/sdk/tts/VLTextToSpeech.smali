.class public interface abstract Lcom/vlingo/sdk/tts/VLTextToSpeech;
.super Ljava/lang/Object;
.source "VLTextToSpeech.java"

# interfaces
.implements Lcom/vlingo/sdk/VLComponent;


# virtual methods
.method public abstract cancel()V
.end method

.method public abstract getSupportedLanguageList()[Ljava/lang/String;
.end method

.method public abstract synthesizeToFile(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;Ljava/lang/String;Lcom/vlingo/sdk/tts/VLTextToSpeechListener;)V
.end method
