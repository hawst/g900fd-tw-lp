.class public final enum Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;
.super Ljava/lang/Enum;
.source "VLTextToSpeechRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VoiceType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

.field public static final enum FEMALE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

.field public static final enum MALE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 22
    new-instance v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    const-string/jumbo v1, "MALE"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;->MALE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    new-instance v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    const-string/jumbo v1, "FEMALE"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;->FEMALE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    sget-object v1, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;->MALE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;->FEMALE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;->$VALUES:[Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;->$VALUES:[Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    invoke-virtual {v0}, [Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    return-object v0
.end method
