.class public final Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;
.super Ljava/lang/Object;
.source "VLTextToSpeechRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$1;,
        Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;,
        Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;,
        Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;,
        Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;
    }
.end annotation


# static fields
.field public static final DEFAULT_LANGUAGE:Ljava/lang/String; = "en-US"

.field public static final DEFAULT_MAX_WORDS:I = 0x1e

.field public static final DEFAULT_READ_MSG_BODY:Z = true

.field public static final DEFAULT_SPEECH_RATE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

.field public static final DEFAULT_TYPE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

.field public static final DEFAULT_VOICE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;


# instance fields
.field private language:Ljava/lang/String;

.field private maxWords:I

.field private msgFrom:Ljava/lang/String;

.field private msgReadBody:Z

.field private msgSubject:Ljava/lang/String;

.field private speechRate:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

.field private text:Ljava/lang/String;

.field private type:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

.field private voice:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;->FEMALE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    sput-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->DEFAULT_VOICE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    .line 40
    sget-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;->NORMAL:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    sput-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->DEFAULT_SPEECH_RATE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    .line 41
    sget-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;->PLAIN:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    sput-object v0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->DEFAULT_TYPE:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    return-void
.end method

.method private constructor <init>(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    # getter for: Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->language:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->access$100(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->language:Ljava/lang/String;

    .line 109
    # getter for: Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->text:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->access$200(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->text:Ljava/lang/String;

    .line 110
    # getter for: Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->voice:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;
    invoke-static {p1}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->access$300(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->voice:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    .line 111
    # getter for: Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->type:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;
    invoke-static {p1}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->access$400(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->type:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    .line 112
    # getter for: Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->maxWords:I
    invoke-static {p1}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->access$500(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->maxWords:I

    .line 113
    # getter for: Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->speechRate:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;
    invoke-static {p1}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->access$600(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;)Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->speechRate:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    .line 114
    # getter for: Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->msgFrom:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->access$700(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->msgFrom:Ljava/lang/String;

    .line 115
    # getter for: Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->msgSubject:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->access$800(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->msgSubject:Ljava/lang/String;

    .line 116
    # getter for: Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->msgReadBody:Z
    invoke-static {p1}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;->access$900(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->msgReadBody:Z

    .line 117
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;
    .param p2, "x1"    # Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$1;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;-><init>(Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Builder;)V

    return-void
.end method


# virtual methods
.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->language:Ljava/lang/String;

    return-object v0
.end method

.method public getMaxWords()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->maxWords:I

    return v0
.end method

.method public getMsgFrom()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->msgFrom:Ljava/lang/String;

    return-object v0
.end method

.method public getMsgReadyBody()Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->msgReadBody:Z

    return v0
.end method

.method public getMsgSubject()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->msgSubject:Ljava/lang/String;

    return-object v0
.end method

.method public getSpeechRate()Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->speechRate:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$SpeechRate;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->text:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->type:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$Type;

    return-object v0
.end method

.method public getVoice()Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/vlingo/sdk/tts/VLTextToSpeechRequest;->voice:Lcom/vlingo/sdk/tts/VLTextToSpeechRequest$VoiceType;

    return-object v0
.end method
