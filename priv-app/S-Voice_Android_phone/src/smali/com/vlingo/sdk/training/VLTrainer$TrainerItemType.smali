.class public final enum Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;
.super Ljava/lang/Enum;
.source "VLTrainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/sdk/training/VLTrainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TrainerItemType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

.field public static final enum CONTACT:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

.field public static final enum PLAYLIST:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

.field public static final enum SONG:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33
    new-instance v0, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    const-string/jumbo v1, "CONTACT"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->CONTACT:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    new-instance v0, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    const-string/jumbo v1, "SONG"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->SONG:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    new-instance v0, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    const-string/jumbo v1, "PLAYLIST"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->PLAYLIST:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    sget-object v1, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->CONTACT:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->SONG:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->PLAYLIST:Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->$VALUES:[Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 33
    const-class v0, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->$VALUES:[Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    invoke-virtual {v0}, [Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;

    return-object v0
.end method
