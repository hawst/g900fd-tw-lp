.class public interface abstract Lcom/vlingo/sdk/training/VLTrainer;
.super Ljava/lang/Object;
.source "VLTrainer.java"

# interfaces
.implements Lcom/vlingo/sdk/VLComponent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/sdk/training/VLTrainer$TrainerItemType;
    }
.end annotation


# virtual methods
.method public abstract clearAllItems(Ljava/lang/String;Lcom/vlingo/sdk/training/VLTrainerListener;)V
.end method

.method public abstract clearContactItems(Ljava/lang/String;Lcom/vlingo/sdk/training/VLTrainerListener;)V
.end method

.method public abstract clearMusicItems(Ljava/lang/String;Lcom/vlingo/sdk/training/VLTrainerListener;)V
.end method

.method public abstract sendFullUpdate(Lcom/vlingo/sdk/training/VLTrainerUpdateList;Ljava/lang/String;Lcom/vlingo/sdk/training/VLTrainerListener;)V
.end method

.method public abstract sendPartialUpdate(Lcom/vlingo/sdk/training/VLTrainerUpdateList;Ljava/lang/String;Lcom/vlingo/sdk/training/VLTrainerListener;)V
.end method

.method public abstract updateTrainerModelLanguage(Ljava/lang/String;Lcom/vlingo/sdk/training/VLTrainerListener;)V
.end method
