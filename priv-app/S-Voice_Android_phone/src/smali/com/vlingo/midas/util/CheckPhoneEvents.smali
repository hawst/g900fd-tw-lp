.class public Lcom/vlingo/midas/util/CheckPhoneEvents;
.super Ljava/lang/Object;
.source "CheckPhoneEvents.java"


# static fields
.field private static myInstance:Lcom/vlingo/midas/util/CheckPhoneEvents;


# instance fields
.field private delegate:Lcom/vlingo/midas/gui/ControlFragment;

.field private res:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/util/CheckPhoneEvents;->myInstance:Lcom/vlingo/midas/util/CheckPhoneEvents;

    return-void
.end method

.method private constructor <init>(Lcom/vlingo/midas/gui/ControlFragment;)V
    .locals 1
    .param p1, "carDelegate"    # Lcom/vlingo/midas/gui/ControlFragment;

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object v0, p0, Lcom/vlingo/midas/util/CheckPhoneEvents;->delegate:Lcom/vlingo/midas/gui/ControlFragment;

    .line 38
    iput-object v0, p0, Lcom/vlingo/midas/util/CheckPhoneEvents;->res:Landroid/content/res/Resources;

    .line 49
    iput-object p1, p0, Lcom/vlingo/midas/util/CheckPhoneEvents;->delegate:Lcom/vlingo/midas/gui/ControlFragment;

    .line 50
    iget-object v0, p0, Lcom/vlingo/midas/util/CheckPhoneEvents;->delegate:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/util/CheckPhoneEvents;->res:Landroid/content/res/Resources;

    .line 51
    return-void
.end method

.method public static getInstance(Lcom/vlingo/midas/gui/ControlFragment;)Lcom/vlingo/midas/util/CheckPhoneEvents;
    .locals 1
    .param p0, "carDelegate"    # Lcom/vlingo/midas/gui/ControlFragment;

    .prologue
    .line 41
    sget-object v0, Lcom/vlingo/midas/util/CheckPhoneEvents;->myInstance:Lcom/vlingo/midas/util/CheckPhoneEvents;

    if-eqz v0, :cond_0

    .line 42
    sget-object v0, Lcom/vlingo/midas/util/CheckPhoneEvents;->myInstance:Lcom/vlingo/midas/util/CheckPhoneEvents;

    .line 44
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/vlingo/midas/util/CheckPhoneEvents;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/util/CheckPhoneEvents;-><init>(Lcom/vlingo/midas/gui/ControlFragment;)V

    goto :goto_0
.end method

.method private ttsAlarmDetails(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Ljava/lang/String;
    .locals 12
    .param p1, "alarm"    # Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 268
    const/4 v2, 0x0

    .line 269
    .local v2, "detailTxt":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v6

    .line 270
    .local v6, "title":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBegin()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 271
    .local v5, "millis":Ljava/lang/Long;
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 272
    .local v0, "alarmTime":Landroid/text/format/Time;
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Landroid/text/format/Time;->set(J)V

    .line 273
    new-instance v7, Ljava/util/Date;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-static {v7}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatDisplayTime(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 275
    .local v1, "alarmTimeTxt":Ljava/lang/String;
    const-string/jumbo v7, "%H"

    invoke-virtual {v0, v7}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 276
    .local v4, "hrsTimeTxt":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 278
    .local v3, "hrs":I
    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 279
    iget-object v7, p0, Lcom/vlingo/midas/util/CheckPhoneEvents;->res:Landroid/content/res/Resources;

    sget v8, Lcom/vlingo/midas/R$string;->checkEventTitleTimeDetail:I

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v6, v9, v10

    aput-object v1, v9, v11

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 284
    :goto_0
    return-object v2

    .line 281
    :cond_0
    iget-object v7, p0, Lcom/vlingo/midas/util/CheckPhoneEvents;->res:Landroid/content/res/Resources;

    sget v8, Lcom/vlingo/midas/R$plurals;->checkEventTimeDetail:I

    new-array v9, v11, [Ljava/lang/Object;

    aput-object v1, v9, v10

    invoke-virtual {v7, v8, v3, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private ttsAlarmNumber(Ljava/util/List;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 253
    .local p1, "alarm":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    const-string/jumbo v0, ""

    .line 254
    .local v0, "summary":Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 255
    iget-object v1, p0, Lcom/vlingo/midas/util/CheckPhoneEvents;->res:Landroid/content/res/Resources;

    sget v2, Lcom/vlingo/midas/R$plurals;->numberOfFiredEvents:I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 257
    :cond_0
    return-object v0
.end method

.method private ttsCallDetails(Lcom/vlingo/core/internal/util/LoggedCall;)Ljava/lang/String;
    .locals 12
    .param p1, "call"    # Lcom/vlingo/core/internal/util/LoggedCall;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 310
    const/4 v2, 0x0

    .line 311
    .local v2, "detailTxt":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/LoggedCall;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 312
    .local v6, "name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/LoggedCall;->getDate()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 313
    .local v5, "millis":Ljava/lang/Long;
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 314
    .local v0, "callTime":Landroid/text/format/Time;
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Landroid/text/format/Time;->set(J)V

    .line 315
    new-instance v7, Ljava/util/Date;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-static {v7}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatDisplayTime(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 317
    .local v1, "callTimeTxt":Ljava/lang/String;
    const-string/jumbo v7, "%H"

    invoke-virtual {v0, v7}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 318
    .local v4, "hrsTimeTxt":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 320
    .local v3, "hrs":I
    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 322
    iget-object v7, p0, Lcom/vlingo/midas/util/CheckPhoneEvents;->res:Landroid/content/res/Resources;

    sget v8, Lcom/vlingo/midas/R$plurals;->checkEventNameTimeDetail:I

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v6, v9, v10

    aput-object v1, v9, v11

    invoke-virtual {v7, v8, v3, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 327
    :goto_0
    return-object v2

    .line 324
    :cond_0
    iget-object v7, p0, Lcom/vlingo/midas/util/CheckPhoneEvents;->res:Landroid/content/res/Resources;

    sget v8, Lcom/vlingo/midas/R$plurals;->checkEventTimeDetail:I

    new-array v9, v11, [Ljava/lang/Object;

    aput-object v1, v9, v10

    invoke-virtual {v7, v8, v3, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private ttsCallNumber(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/util/LoggedCall;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 295
    .local p1, "call":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/util/LoggedCall;>;"
    const-string/jumbo v0, ""

    .line 296
    .local v0, "summary":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 297
    iget-object v1, p0, Lcom/vlingo/midas/util/CheckPhoneEvents;->res:Landroid/content/res/Resources;

    sget v2, Lcom/vlingo/midas/R$plurals;->numberOfMissedCalls:I

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 299
    :cond_0
    return-object v0
.end method

.method private ttsEventSummary(Ljava/util/ArrayList;Ljava/util/List;Ljava/util/ArrayList;I)V
    .locals 9
    .param p4, "totalEvents"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/util/LoggedCall;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .local p1, "sms":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;>;"
    .local p2, "alarm":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    .local p3, "call":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/util/LoggedCall;>;"
    const/4 v8, 0x0

    const/4 v6, 0x1

    .line 143
    const-string/jumbo v1, ""

    .line 145
    .local v1, "eventSummary":Ljava/lang/String;
    const-string/jumbo v3, ""

    .line 146
    .local v3, "eventSummaryTxt":Ljava/lang/String;
    const-string/jumbo v2, ""

    .line 147
    .local v2, "eventSummaryAll":Ljava/lang/String;
    const-string/jumbo v4, "language"

    const-string/jumbo v5, "en-US"

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 148
    .local v0, "SetLanguage":Ljava/lang/String;
    const-string/jumbo v4, "ja-JP"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 150
    const/16 v4, 0x9

    if-le p4, v4, :cond_3

    .line 152
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventJapaneseMoreTenYouHave:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-interface {v4, v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 175
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0, p3}, Lcom/vlingo/midas/util/CheckPhoneEvents;->ttsCallNumber(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 176
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-gtz v4, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 177
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 179
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0, p1}, Lcom/vlingo/midas/util/CheckPhoneEvents;->ttsSmsNumber(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 180
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 181
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 183
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0, p2}, Lcom/vlingo/midas/util/CheckPhoneEvents;->ttsAlarmNumber(Ljava/util/List;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 185
    const-string/jumbo v4, "ko-KR"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 186
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 198
    :goto_1
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->ttsAnyway(Ljava/lang/String;)V

    .line 199
    return-void

    .line 155
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventYouHave:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-interface {v4, v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 158
    :cond_4
    const-string/jumbo v4, "ru-RU"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 160
    const/4 v4, 0x2

    if-eq p4, v4, :cond_5

    const/4 v4, 0x3

    if-eq p4, v4, :cond_5

    const/4 v4, 0x4

    if-ne p4, v4, :cond_6

    .line 162
    :cond_5
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventRussianTwoThreeFourYouHave:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-interface {v4, v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 163
    :cond_6
    rem-int/lit8 v4, p4, 0xa

    if-ne v4, v6, :cond_7

    .line 165
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventOneYouHave:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-interface {v4, v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 167
    :cond_7
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventYouHave:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-interface {v4, v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 171
    :cond_8
    iget-object v4, p0, Lcom/vlingo/midas/util/CheckPhoneEvents;->res:Landroid/content/res/Resources;

    sget v5, Lcom/vlingo/midas/R$string;->check_phone_event_main:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 188
    :cond_9
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1
.end method

.method private ttsOneEvent(Ljava/util/ArrayList;Ljava/util/List;Ljava/util/ArrayList;I)V
    .locals 7
    .param p4, "totalEvent"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/util/LoggedCall;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .local p1, "sms":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;>;"
    .local p2, "alarm":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    .local p3, "call":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/util/LoggedCall;>;"
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 96
    iget-object v3, p0, Lcom/vlingo/midas/util/CheckPhoneEvents;->res:Landroid/content/res/Resources;

    sget v4, Lcom/vlingo/midas/R$string;->check_phone_event_main:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 98
    .local v1, "eventSummary":Ljava/lang/String;
    const-string/jumbo v2, ""

    .line 99
    .local v2, "eventSummaryAll":Ljava/lang/String;
    const-string/jumbo v3, "language"

    const-string/jumbo v4, "en-US"

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 100
    .local v0, "SetLanguage":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v6, v3, :cond_2

    .line 101
    const-string/jumbo v3, "ko-KR"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 102
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p1}, Lcom/vlingo/midas/util/CheckPhoneEvents;->ttsSmsNumber(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ". "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 106
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;

    invoke-direct {p0, v3}, Lcom/vlingo/midas/util/CheckPhoneEvents;->ttsSmsDetails(Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 131
    :cond_0
    :goto_1
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->ttsAnyway(Ljava/lang/String;)V

    .line 132
    return-void

    .line 104
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, p1}, Lcom/vlingo/midas/util/CheckPhoneEvents;->ttsSmsNumber(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 107
    :cond_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    if-ne v6, v3, :cond_4

    .line 108
    const-string/jumbo v3, "ko-KR"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 109
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p2}, Lcom/vlingo/midas/util/CheckPhoneEvents;->ttsAlarmNumber(Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ". "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 113
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-direct {p0, v3}, Lcom/vlingo/midas/util/CheckPhoneEvents;->ttsAlarmDetails(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 111
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, p2}, Lcom/vlingo/midas/util/CheckPhoneEvents;->ttsAlarmNumber(Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 114
    :cond_4
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v6, v3, :cond_0

    .line 115
    const-string/jumbo v3, "ko-KR"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 116
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p3}, Lcom/vlingo/midas/util/CheckPhoneEvents;->ttsCallNumber(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ". "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 120
    :goto_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/util/LoggedCall;

    invoke-direct {p0, v3}, Lcom/vlingo/midas/util/CheckPhoneEvents;->ttsCallDetails(Lcom/vlingo/core/internal/util/LoggedCall;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 118
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, p3}, Lcom/vlingo/midas/util/CheckPhoneEvents;->ttsCallNumber(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_3
.end method

.method private ttsSmsDetails(Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;)Ljava/lang/String;
    .locals 14
    .param p1, "sms"    # Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 224
    const/4 v1, 0x0

    .line 225
    .local v1, "detailTxt":Ljava/lang/String;
    iget-object v7, p1, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;->name:Ljava/lang/String;

    .line 226
    .local v7, "name":Ljava/lang/String;
    iget-object v8, p1, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;->address:Ljava/lang/String;

    invoke-static {v8}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 227
    .local v0, "address":Ljava/lang/String;
    iget-wide v8, p1, Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;->date:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 228
    .local v4, "millis":Ljava/lang/Long;
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    .line 229
    .local v5, "msgTime":Landroid/text/format/Time;
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Landroid/text/format/Time;->set(J)V

    .line 230
    new-instance v8, Ljava/util/Date;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-direct {v8, v9, v10}, Ljava/util/Date;-><init>(J)V

    invoke-static {v8}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatDisplayTime(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    .line 232
    .local v6, "msgTimeTxt":Ljava/lang/String;
    const-string/jumbo v8, "%H"

    invoke-virtual {v5, v8}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 233
    .local v3, "hrsTimeTxt":Ljava/lang/String;
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 234
    .local v2, "hrs":I
    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    .line 235
    iget-object v8, p0, Lcom/vlingo/midas/util/CheckPhoneEvents;->res:Landroid/content/res/Resources;

    sget v9, Lcom/vlingo/midas/R$plurals;->checkEventNameTimeDetail:I

    new-array v10, v13, [Ljava/lang/Object;

    aput-object v7, v10, v11

    aput-object v6, v10, v12

    invoke-virtual {v8, v9, v2, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 242
    :goto_0
    return-object v1

    .line 236
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    .line 237
    iget-object v8, p0, Lcom/vlingo/midas/util/CheckPhoneEvents;->res:Landroid/content/res/Resources;

    sget v9, Lcom/vlingo/midas/R$plurals;->checkEventNameTimeDetail:I

    new-array v10, v13, [Ljava/lang/Object;

    aput-object v0, v10, v11

    aput-object v6, v10, v12

    invoke-virtual {v8, v9, v2, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 239
    :cond_1
    iget-object v8, p0, Lcom/vlingo/midas/util/CheckPhoneEvents;->res:Landroid/content/res/Resources;

    sget v9, Lcom/vlingo/midas/R$plurals;->checkEventTimeDetail:I

    new-array v10, v12, [Ljava/lang/Object;

    aput-object v6, v10, v11

    invoke-virtual {v8, v9, v2, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private ttsSmsNumber(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 209
    .local p1, "sms":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;>;"
    const-string/jumbo v0, ""

    .line 210
    .local v0, "summary":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 211
    iget-object v1, p0, Lcom/vlingo/midas/util/CheckPhoneEvents;->res:Landroid/content/res/Resources;

    sget v2, Lcom/vlingo/midas/R$plurals;->numberOfUnreadSMS:I

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 213
    :cond_0
    return-object v0
.end method


# virtual methods
.method public check()Z
    .locals 10

    .prologue
    const/16 v9, 0x32

    const/4 v7, 0x1

    .line 58
    const/4 v4, 0x0

    .line 59
    .local v4, "numSms":I
    iget-object v8, p0, Lcom/vlingo/midas/util/CheckPhoneEvents;->delegate:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v8}, Lcom/vlingo/midas/gui/ControlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-static {v8, v9}, Lcom/vlingo/core/internal/util/SMSUtil;->getLastNUnreadMessages(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v5

    .line 60
    .local v5, "sms":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/util/SMSUtil$TextMessage;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 62
    const/4 v2, 0x0

    .line 64
    .local v2, "numFiredAlarms":I
    iget-object v8, p0, Lcom/vlingo/midas/util/CheckPhoneEvents;->delegate:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v8}, Lcom/vlingo/midas/gui/ControlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-static {v8, v9}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getFiredCalendarAlerts(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v0

    .line 65
    .local v0, "firedAlarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 67
    const/4 v3, 0x0

    .line 68
    .local v3, "numMissedCalls":I
    iget-object v8, p0, Lcom/vlingo/midas/util/CheckPhoneEvents;->delegate:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v8}, Lcom/vlingo/midas/gui/ControlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-static {v8, v9}, Lcom/vlingo/core/internal/util/CallLogUtil;->getLastNMissedCalls(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v1

    .line 69
    .local v1, "missedCalls":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/util/LoggedCall;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 71
    add-int v8, v4, v2

    add-int v6, v8, v3

    .line 73
    .local v6, "totalEvents":I
    if-ne v6, v7, :cond_0

    .line 74
    invoke-direct {p0, v5, v0, v1, v6}, Lcom/vlingo/midas/util/CheckPhoneEvents;->ttsOneEvent(Ljava/util/ArrayList;Ljava/util/List;Ljava/util/ArrayList;I)V

    .line 84
    :goto_0
    return v7

    .line 76
    :cond_0
    if-le v6, v7, :cond_1

    .line 77
    invoke-direct {p0, v5, v0, v1, v6}, Lcom/vlingo/midas/util/CheckPhoneEvents;->ttsEventSummary(Ljava/util/ArrayList;Ljava/util/List;Ljava/util/ArrayList;I)V

    goto :goto_0

    .line 82
    :cond_1
    const/4 v7, 0x0

    goto :goto_0
.end method
