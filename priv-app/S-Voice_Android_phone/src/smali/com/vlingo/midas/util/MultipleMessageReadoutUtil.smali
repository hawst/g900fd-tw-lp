.class public Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;
.super Ljava/lang/Object;
.source "MultipleMessageReadoutUtil.java"


# static fields
.field static multipleMessageReadoutUtil:Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;


# instance fields
.field messagehandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V
    .locals 0
    .param p1, "messagehandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;->messagehandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 20
    return-void
.end method

.method public static getInstance()Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;->multipleMessageReadoutUtil:Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;

    if-nez v0, :cond_0

    .line 12
    new-instance v0, Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;

    invoke-direct {v0}, Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;-><init>()V

    sput-object v0, Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;->multipleMessageReadoutUtil:Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;

    .line 13
    :cond_0
    sget-object v0, Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;->multipleMessageReadoutUtil:Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;

    return-object v0
.end method


# virtual methods
.method public getMessagehandlerListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;->messagehandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    return-object v0
.end method

.method public setMessagehandlerListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V
    .locals 0
    .param p1, "messagehandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;->messagehandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 34
    return-void
.end method
