.class public Lcom/vlingo/midas/util/log/PreloadAppLogging;
.super Ljava/lang/Object;
.source "PreloadAppLogging.java"


# static fields
.field public static final APP_ID:Ljava/lang/String; = "com.vlingo.midas"

.field public static final EXTRA_LAUNCH_VIA_BT:Ljava/lang/String; = "BT"

.field public static final EXTRA_LAUNCH_VIA_EARPHONE:Ljava/lang/String; = "EARPHONE"

.field public static final EXTRA_LAUNCH_VIA_HOMEKEY:Ljava/lang/String; = "HOMEKEY"

.field public static final EXTRA_LAUNCH_VIA_ICON:Ljava/lang/String; = "ICON"

.field public static final EXTRA_LAUNCH_VIA_WAKEUP:Ljava/lang/String; = "WAKEUP"

.field public static final FEATURE_CALL:Ljava/lang/String; = "CALL"

.field public static final FEATURE_LAUNCH:Ljava/lang/String; = "LAUN"

.field public static final FEATURE_OPEN_APP:Ljava/lang/String; = "OPEN"

.field public static final FEATURE_READ_NEWS:Ljava/lang/String; = "NEWS"

.field public static final FEATURE_SEND_MESSAGE:Ljava/lang/String; = "SMSG"

.field public static final FEATURE_SET_ALARM:Ljava/lang/String; = "ALAR"

.field public static final FEATURE_SET_WAKE_UP_COMMAND_BEGIN:Ljava/lang/String; = "WKCS"

.field public static final FEATURE_SET_WAKE_UP_COMMAND_END:Ljava/lang/String; = "WKCE"

.field public static final FEATURE_VOICE_WAKE_UP_ENABLE:Ljava/lang/String; = "WKON"

.field public static final FEATURE_VOICE_WAKE_UP_IDLE:Ljava/lang/String; = "WKID"

.field public static final FEATURE_VOICE_WAKE_UP_SCREEN_OFF:Ljava/lang/String; = "WKAM"

.field public static final FEATURE_WEATHER:Ljava/lang/String; = "WEAT"

.field public static final TAG:Ljava/lang/String; = "AppLogging"

.field private static isSurvetMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/util/log/PreloadAppLogging;->isSurvetMode:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getVersionOfContextProviders(Landroid/content/Context;)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 102
    const/4 v2, -0x1

    .line 104
    .local v2, "version":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string/jumbo v4, "com.samsung.android.providers.context"

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 106
    .local v1, "pInfo":Landroid/content/pm/PackageInfo;
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    .end local v1    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v2

    .line 107
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string/jumbo v3, "AppLogging"

    const-string/jumbo v4, "[SW] Could not find ContextProvider"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "feature"    # Ljava/lang/String;

    .prologue
    .line 61
    sget-boolean v2, Lcom/vlingo/midas/util/log/PreloadAppLogging;->isSurvetMode:Z

    if-nez v2, :cond_0

    .line 62
    const-string/jumbo v2, "TRUE"

    invoke-static {}, Lcom/samsung/android/app/floatingfeature/SFloatingFeature;->getInstance()Lcom/samsung/android/app/floatingfeature/SFloatingFeature;

    move-result-object v3

    const-string/jumbo v4, "SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE"

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/floatingfeature/SFloatingFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    sput-boolean v2, Lcom/vlingo/midas/util/log/PreloadAppLogging;->isSurvetMode:Z

    .line 64
    :cond_0
    sget-boolean v2, Lcom/vlingo/midas/util/log/PreloadAppLogging;->isSurvetMode:Z

    if-eqz v2, :cond_1

    .line 65
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 66
    .local v1, "cv":Landroid/content/ContentValues;
    const-string/jumbo v2, "app_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string/jumbo v2, "feature"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 71
    .local v0, "broadcastIntent":Landroid/content/Intent;
    const-string/jumbo v2, "com.samsung.android.providers.context.log.action.USE_APP_FEATURE_SURVEY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    const-string/jumbo v2, "data"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 74
    const-string/jumbo v2, "com.samsung.android.providers.context"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 76
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 99
    .end local v0    # "broadcastIntent":Landroid/content/Intent;
    .end local v1    # "cv":Landroid/content/ContentValues;
    :cond_1
    return-void
.end method

.method public static insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "feature"    # Ljava/lang/String;
    .param p3, "extra"    # Ljava/lang/String;

    .prologue
    .line 40
    sget-boolean v2, Lcom/vlingo/midas/util/log/PreloadAppLogging;->isSurvetMode:Z

    if-nez v2, :cond_0

    .line 41
    const-string/jumbo v2, "TRUE"

    invoke-static {}, Lcom/samsung/android/app/floatingfeature/SFloatingFeature;->getInstance()Lcom/samsung/android/app/floatingfeature/SFloatingFeature;

    move-result-object v3

    const-string/jumbo v4, "SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE"

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/floatingfeature/SFloatingFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    sput-boolean v2, Lcom/vlingo/midas/util/log/PreloadAppLogging;->isSurvetMode:Z

    .line 43
    :cond_0
    sget-boolean v2, Lcom/vlingo/midas/util/log/PreloadAppLogging;->isSurvetMode:Z

    if-eqz v2, :cond_1

    .line 44
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 45
    .local v1, "cv":Landroid/content/ContentValues;
    const-string/jumbo v2, "app_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    const-string/jumbo v2, "feature"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    const-string/jumbo v2, "extra"

    invoke-virtual {v1, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 51
    .local v0, "broadcastIntent":Landroid/content/Intent;
    const-string/jumbo v2, "com.samsung.android.providers.context.log.action.USE_APP_FEATURE_SURVEY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 52
    const-string/jumbo v2, "data"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 54
    const-string/jumbo v2, "com.samsung.android.providers.context"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 58
    .end local v0    # "broadcastIntent":Landroid/content/Intent;
    .end local v1    # "cv":Landroid/content/ContentValues;
    :cond_1
    return-void
.end method
