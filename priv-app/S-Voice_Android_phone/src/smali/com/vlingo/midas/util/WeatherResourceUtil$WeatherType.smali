.class public final enum Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;
.super Ljava/lang/Enum;
.source "WeatherResourceUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/util/WeatherResourceUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WeatherType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

.field public static final enum Clear:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

.field public static final enum Cold:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

.field public static final enum Flurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

.field public static final enum Fog:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

.field public static final enum Hot:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

.field public static final enum Ice:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

.field public static final enum MosltyClear:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

.field public static final enum MostlyCloudy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

.field public static final enum MostlyCloudyNight:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

.field public static final enum MostlyCloudyWithFlurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

.field public static final enum MostlyCloudyWithThunderShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

.field public static final enum PartlySunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

.field public static final enum PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

.field public static final enum Rain:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

.field public static final enum RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

.field public static final enum Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

.field public static final enum Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

.field public static final enum Sunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

.field public static final enum Thunderstorms:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

.field public static final enum Windy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 18
    new-instance v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "Sunny"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Sunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .line 19
    new-instance v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "PartlySunny"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .line 20
    new-instance v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "MostlyCloudy"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .line 21
    new-instance v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "Fog"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Fog:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .line 22
    new-instance v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "Showers"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .line 23
    new-instance v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "PartlySunnyWithShowers"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .line 24
    new-instance v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "Thunderstorms"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Thunderstorms:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .line 25
    new-instance v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "MostlyCloudyWithThunderShowers"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyWithThunderShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .line 26
    new-instance v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "Rain"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Rain:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .line 27
    new-instance v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "Flurries"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Flurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .line 28
    new-instance v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "MostlyCloudyWithFlurries"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyWithFlurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .line 29
    new-instance v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "Snow"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .line 30
    new-instance v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "Ice"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Ice:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .line 31
    new-instance v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "RainAndSnowMixed"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .line 32
    new-instance v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "Hot"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Hot:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .line 33
    new-instance v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "Cold"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Cold:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .line 34
    new-instance v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "Windy"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Windy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .line 35
    new-instance v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "Clear"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Clear:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .line 36
    new-instance v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "MosltyClear"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MosltyClear:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .line 37
    new-instance v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "MostlyCloudyNight"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyNight:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .line 17
    const/16 v0, 0x14

    new-array v0, v0, [Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Sunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Fog:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Thunderstorms:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyWithThunderShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Rain:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Flurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyWithFlurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Ice:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Hot:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Cold:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Windy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Clear:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MosltyClear:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyNight:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->$VALUES:[Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->$VALUES:[Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0}, [Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    return-object v0
.end method
