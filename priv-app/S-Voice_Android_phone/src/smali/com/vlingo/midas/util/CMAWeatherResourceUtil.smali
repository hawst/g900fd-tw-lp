.class public Lcom/vlingo/midas/util/CMAWeatherResourceUtil;
.super Ljava/lang/Object;
.source "CMAWeatherResourceUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;
    }
.end annotation


# static fields
.field private static final NO_DRAWABLE:I = 0x63

.field private static final weatherCodeMapping:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;",
            ">;"
        }
    .end annotation
.end field

.field private static final weatherCodeToImage:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final weatherCodeToSmallImage:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mWeatherCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    new-instance v0, Landroid/util/SparseArray;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    sput-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    .line 41
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/4 v1, 0x0

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Sunny:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 42
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/4 v1, 0x1

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Cloudy:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 43
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/4 v1, 0x2

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Overcast:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 44
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/4 v1, 0x3

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Shower:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 45
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/4 v1, 0x4

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->ThunderShower:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 46
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->ThunderShowerwithHail:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 47
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Sleet:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 48
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->LightRain:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 49
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x8

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->LightRain:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 50
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x9

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->HeavyRain:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 51
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0xa

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Storm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 52
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0xb

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Storm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 53
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0xc

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Storm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 54
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0xd

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->SnowFlurry:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 55
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0xe

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->LightSnow:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 56
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0xf

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->LightSnow:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 57
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x10

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->HeavySnow:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 58
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x11

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->SnowStorm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 59
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x12

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Foggy:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 60
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x13

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Sleet:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 61
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x14

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->DustStorm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 62
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x15

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->LightRain:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 63
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x16

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->HeavyRain:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 64
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x17

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->HeavyRain:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 65
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x18

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Storm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 66
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x19

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Storm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 67
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x1a

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->LightSnow:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 68
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x1b

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->HeavySnow:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 69
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x1c

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->HeavySnow:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 70
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x1d

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->DustStorm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 71
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x1e

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->DustStorm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 72
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x1f

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->DustStorm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 73
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x35

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Haze:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 75
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    .line 76
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Sunny:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_01:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Overcast:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_02:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Cloudy:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_03:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->LightRain:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_04:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Foggy:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_05:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Storm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_06:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Shower:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_07:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->ThunderShower:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_08:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->LightSnow:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_10:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->SnowFlurry:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_11:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->HeavySnow:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_12:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Sleet:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_13:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->SnowStorm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_14:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->ThunderShowerwithHail:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_21:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->HeavyRain:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_20:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->DustStorm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_22:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Haze:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_05:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    .line 97
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Sunny:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_01:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Overcast:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_02:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Cloudy:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_03:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->LightRain:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_04:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Foggy:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_05:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Storm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_06:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Shower:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_07:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->ThunderShower:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_08:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->LightSnow:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_10:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->SnowFlurry:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_11:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->HeavySnow:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_12:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Sleet:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_13:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->SnowStorm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_15:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->ThunderShowerwithHail:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_21:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->HeavyRain:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_20:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->DustStorm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_22:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Haze:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_05:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "weatherCode"    # I

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iput p1, p0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->mWeatherCode:I

    .line 119
    return-void
.end method

.method public static WeatherCodeToWeatherType(I)Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;
    .locals 1
    .param p0, "weatherCode"    # I

    .prologue
    .line 122
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    return-object v0
.end method

.method private getWeatherImage(Ljava/util/HashMap;Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;)I
    .locals 2
    .param p2, "weathertype"    # Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;",
            ")I"
        }
    .end annotation

    .prologue
    .line 256
    .local p1, "weatherImageMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;Ljava/lang/Integer;>;"
    invoke-virtual {p1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 257
    .local v0, "ret":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 258
    const/16 v1, 0x63

    .line 259
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method private imageForWeather()I
    .locals 2

    .prologue
    .line 132
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    iget v1, p0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->mWeatherCode:I

    invoke-static {v1}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->WeatherCodeToWeatherType(I)Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->getWeatherImage(Ljava/util/HashMap;Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;)I

    move-result v0

    return v0
.end method

.method private imageForWeatherBackground()I
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->mWeatherCode:I

    packed-switch v0, :pswitch_data_0

    .line 183
    :pswitch_0
    const/16 v0, 0x63

    :goto_0
    return v0

    .line 139
    :pswitch_1
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_05:I

    goto :goto_0

    .line 141
    :pswitch_2
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_03:I

    goto :goto_0

    .line 150
    :pswitch_3
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_04:I

    goto :goto_0

    .line 166
    :pswitch_4
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_01:I

    goto :goto_0

    .line 178
    :pswitch_5
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_02:I

    goto :goto_0

    .line 137
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_3
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private imageForWeatherSevenBackground()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->mWeatherCode:I

    packed-switch v0, :pswitch_data_0

    .line 235
    :pswitch_0
    const/16 v0, 0x63

    :goto_0
    return v0

    .line 191
    :pswitch_1
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_05:I

    goto :goto_0

    .line 193
    :pswitch_2
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_03:I

    goto :goto_0

    .line 202
    :pswitch_3
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_04:I

    goto :goto_0

    .line 218
    :pswitch_4
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_01:I

    goto :goto_0

    .line 230
    :pswitch_5
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_02:I

    goto :goto_0

    .line 189
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_3
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private imageForWeatherSmallSize()I
    .locals 2

    .prologue
    .line 127
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    iget v1, p0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->mWeatherCode:I

    invoke-static {v1}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->WeatherCodeToWeatherType(I)Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->getWeatherImage(Ljava/util/HashMap;Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public changeToNightBackground(I)I
    .locals 1
    .param p1, "dayBackground"    # I

    .prologue
    .line 263
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_01:I

    if-ne p1, v0, :cond_0

    .line 264
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_01_n:I

    .line 276
    :goto_0
    return v0

    .line 265
    :cond_0
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_02:I

    if-ne p1, v0, :cond_1

    .line 266
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_02_n:I

    goto :goto_0

    .line 267
    :cond_1
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_03:I

    if-ne p1, v0, :cond_2

    .line 268
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_03_n:I

    goto :goto_0

    .line 269
    :cond_2
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_04:I

    if-ne p1, v0, :cond_3

    .line 270
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_04_n:I

    goto :goto_0

    .line 271
    :cond_3
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_05:I

    if-ne p1, v0, :cond_4

    .line 272
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_05_n:I

    goto :goto_0

    .line 273
    :cond_4
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_06:I

    if-ne p1, v0, :cond_5

    .line 274
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_06_n:I

    goto :goto_0

    .line 276
    :cond_5
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_03_n:I

    goto :goto_0
.end method

.method public getWeatherBackgroundDrawable()I
    .locals 1

    .prologue
    .line 244
    invoke-direct {p0}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->imageForWeatherBackground()I

    move-result v0

    return v0
.end method

.method public getWeatherDrawable()I
    .locals 1

    .prologue
    .line 240
    invoke-direct {p0}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->imageForWeather()I

    move-result v0

    return v0
.end method

.method public getWeatherDrawableSmallSize()I
    .locals 1

    .prologue
    .line 252
    invoke-direct {p0}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->imageForWeatherSmallSize()I

    move-result v0

    return v0
.end method

.method public getWeatherSevenBackgroundDrawable()I
    .locals 1

    .prologue
    .line 248
    invoke-direct {p0}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->imageForWeatherSevenBackground()I

    move-result v0

    return v0
.end method
