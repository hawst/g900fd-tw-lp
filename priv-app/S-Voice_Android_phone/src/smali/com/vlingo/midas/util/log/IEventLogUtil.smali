.class interface abstract Lcom/vlingo/midas/util/log/IEventLogUtil;
.super Ljava/lang/Object;
.source "IEventLogUtil.java"


# virtual methods
.method public abstract disabled(Landroid/content/SharedPreferences$Editor;)V
.end method

.method public abstract enabled(Landroid/content/SharedPreferences$Editor;)V
.end method

.method public abstract isEnabled()Z
.end method

.method public abstract onRecognitionComplete()V
.end method

.method public abstract onRecognitionError()V
.end method

.method public abstract onRecognitionResult(Ljava/lang/String;)V
.end method

.method public abstract onRecognitionStart()V
.end method
