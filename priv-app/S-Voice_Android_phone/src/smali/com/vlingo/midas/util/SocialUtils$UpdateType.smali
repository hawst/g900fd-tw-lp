.class public final enum Lcom/vlingo/midas/util/SocialUtils$UpdateType;
.super Ljava/lang/Enum;
.source "SocialUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/util/SocialUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UpdateType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/util/SocialUtils$UpdateType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/util/SocialUtils$UpdateType;

.field public static final enum ALL:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

.field public static final enum FACEBOOK_ONLY:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

.field public static final enum NONE:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

.field public static final enum TWITTER_ONLY:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

.field public static final enum WEIBO_ONLY:Lcom/vlingo/midas/util/SocialUtils$UpdateType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 41
    new-instance v0, Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    const-string/jumbo v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/SocialUtils$UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->NONE:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    new-instance v0, Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    const-string/jumbo v1, "ALL"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/midas/util/SocialUtils$UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->ALL:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    new-instance v0, Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    const-string/jumbo v1, "FACEBOOK_ONLY"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/midas/util/SocialUtils$UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->FACEBOOK_ONLY:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    new-instance v0, Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    const-string/jumbo v1, "TWITTER_ONLY"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/midas/util/SocialUtils$UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->TWITTER_ONLY:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    new-instance v0, Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    const-string/jumbo v1, "WEIBO_ONLY"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/midas/util/SocialUtils$UpdateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->WEIBO_ONLY:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    .line 40
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    sget-object v1, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->NONE:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->ALL:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->FACEBOOK_ONLY:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->TWITTER_ONLY:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->WEIBO_ONLY:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->$VALUES:[Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/util/SocialUtils$UpdateType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 40
    const-class v0, Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/util/SocialUtils$UpdateType;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->$VALUES:[Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    invoke-virtual {v0}, [Lcom/vlingo/midas/util/SocialUtils$UpdateType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    return-object v0
.end method
