.class public Lcom/vlingo/midas/util/SamsungAudioSourceUtil;
.super Ljava/lang/Object;
.source "SamsungAudioSourceUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/AudioSourceUtil;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/util/SamsungAudioSourceUtil$1;
    }
.end annotation


# static fields
.field private static final audioSourcePairAsrDrivingMode:Lcom/vlingo/core/internal/audio/AudioSourcePair;

.field private static final audioSourcePairAsrDrivingModeUnderJ:Lcom/vlingo/core/internal/audio/AudioSourcePair;

.field private static final audioSourcePairAsrRegular:Lcom/vlingo/core/internal/audio/AudioSourcePair;

.field private static final audioSourcePairAsrRegularUnderJ:Lcom/vlingo/core/internal/audio/AudioSourcePair;

.field private static final audioSourcePairGeneralRecognition:Lcom/vlingo/core/internal/audio/AudioSourcePair;

.field private static final audioSourcePairRecording:Lcom/vlingo/core/internal/audio/AudioSourcePair;

.field private static final audioSourcePairSpotterDrivingMode:Lcom/vlingo/core/internal/audio/AudioSourcePair;

.field private static final audioSourcePairSpotterDrivingModeUnderJ:Lcom/vlingo/core/internal/audio/AudioSourcePair;

.field private static final audioSourcePairSpotterEchoCancellationForMusic:Lcom/vlingo/core/internal/audio/AudioSourcePair;

.field private static final audioSourcePairSpotterEchoCancellationForTts:Lcom/vlingo/core/internal/audio/AudioSourcePair;

.field private static final audioSourcePairSpotterRegular:Lcom/vlingo/core/internal/audio/AudioSourcePair;

.field private static final audioSourcePairSpotterRegularUnderJ:Lcom/vlingo/core/internal/audio/AudioSourcePair;

.field private static final knownBadSettingValue:Ljava/lang/String; = "-1"


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0x13

    const/16 v3, 0x11

    const/4 v2, 0x1

    const/4 v1, 0x6

    .line 42
    new-instance v0, Lcom/vlingo/core/internal/audio/AudioSourcePair;

    invoke-direct {v0, v1, v1}, Lcom/vlingo/core/internal/audio/AudioSourcePair;-><init>(II)V

    sput-object v0, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairAsrRegular:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    .line 43
    new-instance v0, Lcom/vlingo/core/internal/audio/AudioSourcePair;

    invoke-direct {v0, v1, v1}, Lcom/vlingo/core/internal/audio/AudioSourcePair;-><init>(II)V

    sput-object v0, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairAsrRegularUnderJ:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    .line 46
    new-instance v0, Lcom/vlingo/core/internal/audio/AudioSourcePair;

    invoke-direct {v0, v3, v1}, Lcom/vlingo/core/internal/audio/AudioSourcePair;-><init>(II)V

    sput-object v0, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairAsrDrivingMode:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    .line 47
    new-instance v0, Lcom/vlingo/core/internal/audio/AudioSourcePair;

    invoke-direct {v0, v3, v1}, Lcom/vlingo/core/internal/audio/AudioSourcePair;-><init>(II)V

    sput-object v0, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairAsrDrivingModeUnderJ:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    .line 50
    new-instance v0, Lcom/vlingo/core/internal/audio/AudioSourcePair;

    invoke-direct {v0, v1, v1}, Lcom/vlingo/core/internal/audio/AudioSourcePair;-><init>(II)V

    sput-object v0, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairSpotterRegular:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    .line 51
    new-instance v0, Lcom/vlingo/core/internal/audio/AudioSourcePair;

    invoke-direct {v0, v1, v1}, Lcom/vlingo/core/internal/audio/AudioSourcePair;-><init>(II)V

    sput-object v0, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairSpotterRegularUnderJ:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    .line 54
    new-instance v0, Lcom/vlingo/core/internal/audio/AudioSourcePair;

    invoke-direct {v0, v1, v1}, Lcom/vlingo/core/internal/audio/AudioSourcePair;-><init>(II)V

    sput-object v0, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairSpotterDrivingMode:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    .line 55
    new-instance v0, Lcom/vlingo/core/internal/audio/AudioSourcePair;

    invoke-direct {v0, v1, v1}, Lcom/vlingo/core/internal/audio/AudioSourcePair;-><init>(II)V

    sput-object v0, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairSpotterDrivingModeUnderJ:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    .line 58
    new-instance v0, Lcom/vlingo/core/internal/audio/AudioSourcePair;

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/audio/AudioSourcePair;-><init>(II)V

    sput-object v0, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairGeneralRecognition:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    .line 61
    new-instance v0, Lcom/vlingo/core/internal/audio/AudioSourcePair;

    invoke-direct {v0, v4, v1}, Lcom/vlingo/core/internal/audio/AudioSourcePair;-><init>(II)V

    sput-object v0, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairSpotterEchoCancellationForMusic:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    .line 64
    new-instance v0, Lcom/vlingo/core/internal/audio/AudioSourcePair;

    invoke-direct {v0, v4, v1}, Lcom/vlingo/core/internal/audio/AudioSourcePair;-><init>(II)V

    sput-object v0, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairSpotterEchoCancellationForTts:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    .line 67
    new-instance v0, Lcom/vlingo/core/internal/audio/AudioSourcePair;

    invoke-direct {v0, v2, v1}, Lcom/vlingo/core/internal/audio/AudioSourcePair;-><init>(II)V

    sput-object v0, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairRecording:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public chooseAudioSource(Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)I
    .locals 9
    .param p1, "type"    # Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;
    .param p2, "audioSourceType"    # Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    .prologue
    const/16 v8, 0x10

    .line 80
    sget-object v6, Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;->UNSPECIFIED:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    if-ne v6, p2, :cond_0

    .line 81
    sget-object v6, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->RECOGNITION:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    if-ne p1, v6, :cond_5

    .line 82
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 83
    sget-object p2, Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;->ASR_DRIVING_MODE:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    .line 103
    :cond_0
    :goto_0
    const-string/jumbo v6, "use_echo_cancel_for_spotter"

    const/4 v7, 0x1

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 105
    .local v5, "useEchoCancellation":Z
    sget-object v6, Lcom/vlingo/midas/util/SamsungAudioSourceUtil$1;->$SwitchMap$com$vlingo$core$internal$audio$MicrophoneStream$AudioSourceType:[I

    invoke-virtual {p2}, Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 172
    sget-object v2, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairGeneralRecognition:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    .line 176
    .local v2, "audioSourcePair":Lcom/vlingo/core/internal/audio/AudioSourcePair;
    :cond_1
    :goto_1
    sget-object v6, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->LOCALRECORD:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    if-ne p1, v6, :cond_2

    .line 177
    sget-object v2, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairRecording:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    .line 180
    :cond_2
    invoke-virtual {v2}, Lcom/vlingo/core/internal/audio/AudioSourcePair;->getPreferred()I

    move-result v1

    .line 181
    .local v1, "audioSource":I
    const-string/jumbo v6, "use_non_j_audio_sources"

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 182
    invoke-virtual {v2}, Lcom/vlingo/core/internal/audio/AudioSourcePair;->getFallback()I

    move-result v1

    .line 186
    :cond_3
    return v1

    .line 85
    .end local v1    # "audioSource":I
    .end local v2    # "audioSourcePair":Lcom/vlingo/core/internal/audio/AudioSourcePair;
    .end local v5    # "useEchoCancellation":Z
    :cond_4
    sget-object p2, Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;->ASR_REGULAR:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    goto :goto_0

    .line 87
    :cond_5
    sget-object v6, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->PHRASESPOTTING:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    if-ne p1, v6, :cond_7

    .line 88
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 89
    sget-object p2, Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;->SPOTTER_DRIVING_MODE:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    goto :goto_0

    .line 91
    :cond_6
    sget-object p2, Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;->SPOTTER_REGULAR:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    goto :goto_0

    .line 94
    :cond_7
    sget-object p2, Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;->GENERAL_VOICE_RECOGNITION:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    goto :goto_0

    .line 107
    .restart local v5    # "useEchoCancellation":Z
    :pswitch_0
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v6, v8, :cond_8

    .line 108
    sget-object v2, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairAsrRegular:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    .restart local v2    # "audioSourcePair":Lcom/vlingo/core/internal/audio/AudioSourcePair;
    goto :goto_1

    .line 112
    .end local v2    # "audioSourcePair":Lcom/vlingo/core/internal/audio/AudioSourcePair;
    :cond_8
    sget-object v2, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairAsrRegularUnderJ:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    .line 116
    .restart local v2    # "audioSourcePair":Lcom/vlingo/core/internal/audio/AudioSourcePair;
    goto :goto_1

    .line 118
    .end local v2    # "audioSourcePair":Lcom/vlingo/core/internal/audio/AudioSourcePair;
    :pswitch_1
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v6, v8, :cond_9

    .line 119
    sget-object v2, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairAsrDrivingMode:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    .restart local v2    # "audioSourcePair":Lcom/vlingo/core/internal/audio/AudioSourcePair;
    goto :goto_1

    .line 123
    .end local v2    # "audioSourcePair":Lcom/vlingo/core/internal/audio/AudioSourcePair;
    :cond_9
    sget-object v2, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairAsrDrivingModeUnderJ:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    .line 127
    .restart local v2    # "audioSourcePair":Lcom/vlingo/core/internal/audio/AudioSourcePair;
    goto :goto_1

    .line 132
    .end local v2    # "audioSourcePair":Lcom/vlingo/core/internal/audio/AudioSourcePair;
    :pswitch_2
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-string/jumbo v7, "audio"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 133
    .local v0, "am":Landroid/media/AudioManager;
    const/4 v3, 0x0

    .line 134
    .local v3, "isTts":Z
    invoke-static {}, Lcom/vlingo/midas/gui/ConversationActivity;->getLatestWidget()Lcom/vlingo/midas/gui/Widget;

    move-result-object v4

    .line 135
    .local v4, "latestWidget":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<*>;"
    if-eqz v4, :cond_a

    .line 136
    invoke-virtual {v4}, Lcom/vlingo/midas/gui/Widget;->isPlayingAudio()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 137
    const/4 v3, 0x1

    .line 144
    :cond_a
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v6, v8, :cond_b

    .line 145
    sget-object v2, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairSpotterDrivingMode:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    .line 150
    .restart local v2    # "audioSourcePair":Lcom/vlingo/core/internal/audio/AudioSourcePair;
    :goto_2
    if-eqz v5, :cond_1

    .line 151
    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 152
    if-eqz v3, :cond_c

    .line 153
    sget-object v2, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairSpotterEchoCancellationForTts:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    goto :goto_1

    .line 147
    .end local v2    # "audioSourcePair":Lcom/vlingo/core/internal/audio/AudioSourcePair;
    :cond_b
    sget-object v2, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairSpotterDrivingModeUnderJ:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    .restart local v2    # "audioSourcePair":Lcom/vlingo/core/internal/audio/AudioSourcePair;
    goto :goto_2

    .line 157
    :cond_c
    sget-object v2, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;->audioSourcePairSpotterEchoCancellationForMusic:Lcom/vlingo/core/internal/audio/AudioSourcePair;

    goto :goto_1

    .line 105
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public chooseChannelConfig(I)I
    .locals 1
    .param p1, "audioSource"    # I

    .prologue
    .line 219
    const/4 v0, 0x2

    return v0
.end method

.method public chooseMicSampleRate()I
    .locals 6

    .prologue
    const/16 v4, 0x3e80

    .line 190
    const/16 v2, 0x3e80

    .line 191
    .local v2, "SAMPLE_RATE_16KHZ":I
    const/16 v3, 0x1f40

    .line 192
    .local v3, "SAMPLE_RATE_8KHZ":I
    const/16 v1, 0x3e80

    .line 193
    .local v1, "DEFAULT_SAMPLE_RATE":I
    const/16 v0, 0x1f40

    .line 197
    .local v0, "BLUETOOTH_SAMPLE_RATE":I
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 209
    :cond_0
    :goto_0
    return v4

    .line 201
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 202
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->getBluetoothSampleRate()I

    move-result v4

    .line 205
    .local v4, "sampleRate":I
    goto :goto_0
.end method

.method public notifyAudioSourceSet(I)V
    .locals 0
    .param p1, "audioSource"    # I

    .prologue
    .line 226
    return-void
.end method
