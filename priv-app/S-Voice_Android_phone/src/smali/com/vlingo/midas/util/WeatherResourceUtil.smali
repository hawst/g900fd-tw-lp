.class public Lcom/vlingo/midas/util/WeatherResourceUtil;
.super Ljava/lang/Object;
.source "WeatherResourceUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/util/WeatherResourceUtil$1;,
        Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;
    }
.end annotation


# static fields
.field private static final NO_DRAWABLE:I = 0x65

.field private static final weatherCodeMapping:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;",
            ">;"
        }
    .end annotation
.end field

.field private static final weatherCodeMappingWeatherNews:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;",
            ">;"
        }
    .end annotation
.end field

.field private static final weatherCodeToImage:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final weatherCodeToSmallImage:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private contentProvider:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

.field private mWeatherCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x28

    .line 46
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v3}, Landroid/util/SparseArray;-><init>(I)V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    .line 47
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/4 v1, 0x1

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Sunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 48
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/4 v1, 0x2

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Sunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 49
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/4 v1, 0x3

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 50
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/4 v1, 0x4

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 51
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 52
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 53
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 54
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x8

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 57
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0xb

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Fog:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 58
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0xc

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 59
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0xd

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 60
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0xe

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 61
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0xf

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Thunderstorms:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 62
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x10

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyWithThunderShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 63
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x11

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyWithThunderShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 64
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x12

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Rain:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 65
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x13

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Flurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 66
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x14

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyWithFlurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 67
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x15

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyWithFlurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 68
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x16

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 69
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x17

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 70
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x18

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Ice:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 71
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x19

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Ice:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 72
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x1a

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Ice:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 75
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x1d

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 76
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x1e

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Hot:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 77
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x1f

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Cold:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 78
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x20

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Windy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 79
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x21

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Clear:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 80
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x22

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MosltyClear:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 81
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x23

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MosltyClear:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 82
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x24

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MosltyClear:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 83
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x25

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MosltyClear:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 84
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x26

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyNight:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 85
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x27

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 86
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 87
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x29

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Thunderstorms:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 88
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x2a

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Thunderstorms:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 89
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x2b

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Flurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 90
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    const/16 v1, 0x2c

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 92
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    .line 93
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x64

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Sunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 94
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x65

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 95
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x66

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 96
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x67

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 97
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x68

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Flurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 98
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x69

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Flurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 99
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x6a

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 100
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x6b

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 101
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x6c

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 102
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x6e

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 103
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x6f

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 104
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x70

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 105
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x71

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 106
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x72

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 107
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x73

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 108
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x74

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 109
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x75

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 110
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x76

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 111
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x77

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 112
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x78

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 113
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x79

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 114
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x7a

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 115
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x7b

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Sunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 116
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x7c

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Sunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 117
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x7d

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 118
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x7e

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 119
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x7f

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 120
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x80

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 121
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x81

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 122
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x82

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Sunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 123
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x83

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Sunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 124
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x84

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 125
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x8c

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 126
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xa0

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 127
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xaa

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 128
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xb5

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MosltyClear:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 129
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xc8

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 130
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xc9

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 131
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xca

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 132
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xcb

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 133
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xcc

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyWithFlurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 134
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xcd

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyWithFlurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 135
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xce

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 136
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xcf

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 137
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xd0

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 138
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xd1

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 139
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xd2

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 140
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xd3

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 141
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xd4

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 142
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xd5

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 143
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xd6

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 144
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xd7

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 145
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xd8

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 146
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xd9

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 147
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xda

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 148
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xdb

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 149
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xdc

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 150
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xdd

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 151
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xde

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 152
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xdf

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xe0

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 154
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xe1

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 155
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xe2

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 156
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xe3

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 157
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xe4

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 158
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xe5

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 159
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xe6

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 160
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xe7

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 161
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xf0

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 162
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0xfa

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 163
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x104

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 164
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x10e

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 165
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x119

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 166
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x12c

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Rain:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 167
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x12d

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Rain:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 168
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x12e

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 169
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x12f

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 170
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x130

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Rain:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 171
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x132

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Rain:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 172
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x134

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Rain:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 173
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x135

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 174
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x137

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 175
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x139

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 176
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x13a

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 177
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x13b

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 178
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x13c

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 179
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x13d

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 180
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x140

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 181
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x141

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 182
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x142

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 183
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x143

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 184
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x144

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 185
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x145

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 186
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x146

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 187
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x147

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 188
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x148

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Rain:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 189
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x149

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Rain:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 190
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x154

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 191
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x15e

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Rain:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 192
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x169

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 193
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x173

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 194
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x190

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 195
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x191

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 196
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x192

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 197
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x193

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 198
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x195

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 199
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x196

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 200
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x197

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 201
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x199

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 202
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x19b

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 203
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x19d

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 204
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x19e

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 205
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x1a4

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Flurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 206
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x1a5

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 207
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x1a6

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 208
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x1a7

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 209
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x1a8

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 210
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x1a9

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 211
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x1aa

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 212
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x1ab

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 213
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x1ae

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Ice:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 214
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x1c2

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 215
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x1f4

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Sunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 216
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x226

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Hot:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 217
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x228

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Hot:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 218
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x229

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 219
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x22e

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Hot:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 220
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x232

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Hot:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 221
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x233

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Hot:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 222
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x238

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Thunderstorms:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 223
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x23c

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 224
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x23d

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 225
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x246

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Hot:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 226
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x247

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 227
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x352

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Thunderstorms:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 228
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x353

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Thunderstorms:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 229
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x354

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Thunderstorms:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 230
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x355

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Rain:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 231
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x356

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 232
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x357

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Thunderstorms:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 233
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x35d

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Thunderstorms:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 234
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x35e

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Thunderstorms:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 235
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x35f

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Rain:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 236
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x360

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 237
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x361

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Thunderstorms:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 238
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x367

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 239
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x368

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyWithThunderShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 240
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x369

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Rain:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 241
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x36a

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 242
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x371

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 243
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x372

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Thunderstorms:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 244
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x373

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Rain:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 245
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    const/16 v1, 0x374

    sget-object v2, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 249
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    .line 250
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Sunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_01:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_02:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_03:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Fog:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_05:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_06:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_07:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Thunderstorms:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_08:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyWithThunderShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_09:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Rain:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_04:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Flurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_10:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyWithFlurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_11:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_12:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Ice:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_14:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_13:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Hot:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_15:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Cold:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_16:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Windy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_17:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Clear:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_18:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MosltyClear:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_19:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyNight:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_b_19:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    .line 272
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Sunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_01:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_02:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_03:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Fog:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_05:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_06:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_07:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Thunderstorms:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_08:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyWithThunderShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_09:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Rain:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_04:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Flurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_10:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyWithFlurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_11:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_12:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Ice:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_15:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_13:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Hot:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_16:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Cold:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_17:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Windy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_14:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Clear:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_18:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MosltyClear:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_19:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyNight:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_icon_s_19:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    return-void
.end method

.method public constructor <init>(ILcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;)V
    .locals 0
    .param p1, "weatherCode"    # I
    .param p2, "provider"    # Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    .prologue
    .line 295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296
    iput p1, p0, Lcom/vlingo/midas/util/WeatherResourceUtil;->mWeatherCode:I

    .line 297
    iput-object p2, p0, Lcom/vlingo/midas/util/WeatherResourceUtil;->contentProvider:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    .line 298
    return-void
.end method

.method public static WeatherCodeToWeatherType(ILcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;)Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;
    .locals 1
    .param p0, "weatherCode"    # I
    .param p1, "provider"    # Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    .prologue
    .line 301
    sget-object v0, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;->WEATHERNEWS:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    if-ne p1, v0, :cond_0

    .line 302
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMappingWeatherNews:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .line 304
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeMapping:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    goto :goto_0
.end method

.method private getWeatherImage(Ljava/util/HashMap;Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;)I
    .locals 2
    .param p2, "weatherCode"    # Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;",
            ")I"
        }
    .end annotation

    .prologue
    .line 521
    .local p1, "weatherImageMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;Ljava/lang/Integer;>;"
    invoke-virtual {p1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 522
    .local v0, "ret":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 523
    const/16 v1, 0x65

    .line 524
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method private imageForWeather()I
    .locals 3

    .prologue
    .line 314
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToImage:Ljava/util/HashMap;

    iget v1, p0, Lcom/vlingo/midas/util/WeatherResourceUtil;->mWeatherCode:I

    iget-object v2, p0, Lcom/vlingo/midas/util/WeatherResourceUtil;->contentProvider:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    invoke-static {v1, v2}, Lcom/vlingo/midas/util/WeatherResourceUtil;->WeatherCodeToWeatherType(ILcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;)Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/util/WeatherResourceUtil;->getWeatherImage(Ljava/util/HashMap;Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;)I

    move-result v0

    return v0
.end method

.method private imageForWeatherBackground()I
    .locals 3

    .prologue
    .line 364
    iget-object v1, p0, Lcom/vlingo/midas/util/WeatherResourceUtil;->contentProvider:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    sget-object v2, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;->WEATHERNEWS:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    if-ne v1, v2, :cond_0

    .line 365
    iget v1, p0, Lcom/vlingo/midas/util/WeatherResourceUtil;->mWeatherCode:I

    iget-object v2, p0, Lcom/vlingo/midas/util/WeatherResourceUtil;->contentProvider:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    invoke-static {v1, v2}, Lcom/vlingo/midas/util/WeatherResourceUtil;->WeatherCodeToWeatherType(ILcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;)Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    move-result-object v0

    .line 366
    .local v0, "type":Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;
    invoke-direct {p0, v0}, Lcom/vlingo/midas/util/WeatherResourceUtil;->imageForWeatherBackgroundFromWeatherType(Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;)I

    move-result v1

    .line 429
    .end local v0    # "type":Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;
    :goto_0
    return v1

    .line 369
    :cond_0
    iget v1, p0, Lcom/vlingo/midas/util/WeatherResourceUtil;->mWeatherCode:I

    packed-switch v1, :pswitch_data_0

    .line 429
    :pswitch_0
    const/16 v1, 0x65

    goto :goto_0

    .line 377
    :pswitch_1
    sget v1, Lcom/vlingo/midas/R$drawable;->weather_01:I

    goto :goto_0

    .line 386
    :pswitch_2
    sget v1, Lcom/vlingo/midas/R$drawable;->weather_02:I

    goto :goto_0

    .line 395
    :pswitch_3
    sget v1, Lcom/vlingo/midas/R$drawable;->weather_03:I

    goto :goto_0

    .line 402
    :pswitch_4
    sget v1, Lcom/vlingo/midas/R$drawable;->weather_04:I

    goto :goto_0

    .line 407
    :pswitch_5
    sget v1, Lcom/vlingo/midas/R$drawable;->weather_05:I

    goto :goto_0

    .line 414
    :pswitch_6
    sget v1, Lcom/vlingo/midas/R$drawable;->weather_06:I

    goto :goto_0

    .line 417
    :pswitch_7
    sget v1, Lcom/vlingo/midas/R$drawable;->weather_07:I

    goto :goto_0

    .line 423
    :pswitch_8
    sget v1, Lcom/vlingo/midas/R$drawable;->weather_08:I

    goto :goto_0

    .line 426
    :pswitch_9
    sget v1, Lcom/vlingo/midas/R$drawable;->weather_09:I

    goto :goto_0

    .line 369
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_9
        :pswitch_8
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_6
        :pswitch_6
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private imageForWeatherBackgroundFromWeatherType(Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;)I
    .locals 2
    .param p1, "type"    # Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    .prologue
    .line 318
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil$1;->$SwitchMap$com$vlingo$midas$util$WeatherResourceUtil$WeatherType:[I

    invoke-virtual {p1}, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 360
    const/16 v0, 0x65

    :goto_0
    return v0

    .line 320
    :pswitch_0
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_05:I

    goto :goto_0

    .line 322
    :pswitch_1
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_08:I

    goto :goto_0

    .line 324
    :pswitch_2
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_02:I

    goto :goto_0

    .line 326
    :pswitch_3
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_07:I

    goto :goto_0

    .line 328
    :pswitch_4
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_09:I

    goto :goto_0

    .line 330
    :pswitch_5
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_08:I

    goto :goto_0

    .line 332
    :pswitch_6
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_03:I

    goto :goto_0

    .line 334
    :pswitch_7
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_04:I

    goto :goto_0

    .line 336
    :pswitch_8
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_04:I

    goto :goto_0

    .line 338
    :pswitch_9
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_02:I

    goto :goto_0

    .line 340
    :pswitch_a
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_06:I

    goto :goto_0

    .line 342
    :pswitch_b
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_03:I

    goto :goto_0

    .line 344
    :pswitch_c
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_01:I

    goto :goto_0

    .line 346
    :pswitch_d
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_01:I

    goto :goto_0

    .line 348
    :pswitch_e
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_01:I

    goto :goto_0

    .line 350
    :pswitch_f
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_01:I

    goto :goto_0

    .line 352
    :pswitch_10
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_02:I

    goto :goto_0

    .line 354
    :pswitch_11
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_05:I

    goto :goto_0

    .line 356
    :pswitch_12
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_06:I

    goto :goto_0

    .line 358
    :pswitch_13
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_04:I

    goto :goto_0

    .line 318
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method private imageForWeatherSevenBackground()I
    .locals 3

    .prologue
    .line 435
    iget-object v1, p0, Lcom/vlingo/midas/util/WeatherResourceUtil;->contentProvider:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    sget-object v2, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;->WEATHERNEWS:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    if-ne v1, v2, :cond_0

    .line 436
    iget v1, p0, Lcom/vlingo/midas/util/WeatherResourceUtil;->mWeatherCode:I

    iget-object v2, p0, Lcom/vlingo/midas/util/WeatherResourceUtil;->contentProvider:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    invoke-static {v1, v2}, Lcom/vlingo/midas/util/WeatherResourceUtil;->WeatherCodeToWeatherType(ILcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;)Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    move-result-object v0

    .line 437
    .local v0, "type":Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;
    invoke-direct {p0, v0}, Lcom/vlingo/midas/util/WeatherResourceUtil;->imageForWeatherBackgroundFromWeatherType(Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;)I

    move-result v1

    .line 500
    .end local v0    # "type":Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;
    :goto_0
    return v1

    .line 440
    :cond_0
    iget v1, p0, Lcom/vlingo/midas/util/WeatherResourceUtil;->mWeatherCode:I

    packed-switch v1, :pswitch_data_0

    .line 500
    :pswitch_0
    const/16 v1, 0x65

    goto :goto_0

    .line 448
    :pswitch_1
    sget v1, Lcom/vlingo/midas/R$drawable;->weather_01:I

    goto :goto_0

    .line 457
    :pswitch_2
    sget v1, Lcom/vlingo/midas/R$drawable;->weather_02:I

    goto :goto_0

    .line 466
    :pswitch_3
    sget v1, Lcom/vlingo/midas/R$drawable;->weather_03:I

    goto :goto_0

    .line 473
    :pswitch_4
    sget v1, Lcom/vlingo/midas/R$drawable;->weather_04:I

    goto :goto_0

    .line 478
    :pswitch_5
    sget v1, Lcom/vlingo/midas/R$drawable;->weather_05:I

    goto :goto_0

    .line 485
    :pswitch_6
    sget v1, Lcom/vlingo/midas/R$drawable;->weather_06:I

    goto :goto_0

    .line 488
    :pswitch_7
    sget v1, Lcom/vlingo/midas/R$drawable;->weather_07:I

    goto :goto_0

    .line 494
    :pswitch_8
    sget v1, Lcom/vlingo/midas/R$drawable;->weather_08:I

    goto :goto_0

    .line 497
    :pswitch_9
    sget v1, Lcom/vlingo/midas/R$drawable;->weather_09:I

    goto :goto_0

    .line 440
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_9
        :pswitch_8
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_6
        :pswitch_6
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private imageForWeatherSmallSize()I
    .locals 3

    .prologue
    .line 309
    sget-object v0, Lcom/vlingo/midas/util/WeatherResourceUtil;->weatherCodeToSmallImage:Ljava/util/HashMap;

    iget v1, p0, Lcom/vlingo/midas/util/WeatherResourceUtil;->mWeatherCode:I

    iget-object v2, p0, Lcom/vlingo/midas/util/WeatherResourceUtil;->contentProvider:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    invoke-static {v1, v2}, Lcom/vlingo/midas/util/WeatherResourceUtil;->WeatherCodeToWeatherType(ILcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;)Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/util/WeatherResourceUtil;->getWeatherImage(Ljava/util/HashMap;Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public changeToNightBackground(I)I
    .locals 1
    .param p1, "dayBackground"    # I

    .prologue
    .line 528
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_01:I

    if-ne p1, v0, :cond_1

    .line 529
    sget p1, Lcom/vlingo/midas/R$drawable;->weather_01_n:I

    .line 543
    .end local p1    # "dayBackground":I
    :cond_0
    :goto_0
    return p1

    .line 530
    .restart local p1    # "dayBackground":I
    :cond_1
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_02:I

    if-ne p1, v0, :cond_2

    .line 531
    sget p1, Lcom/vlingo/midas/R$drawable;->weather_02_n:I

    goto :goto_0

    .line 532
    :cond_2
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_03:I

    if-ne p1, v0, :cond_3

    .line 533
    sget p1, Lcom/vlingo/midas/R$drawable;->weather_03_n:I

    goto :goto_0

    .line 534
    :cond_3
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_04:I

    if-ne p1, v0, :cond_4

    .line 535
    sget p1, Lcom/vlingo/midas/R$drawable;->weather_04_n:I

    goto :goto_0

    .line 536
    :cond_4
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_05:I

    if-ne p1, v0, :cond_5

    .line 537
    sget p1, Lcom/vlingo/midas/R$drawable;->weather_05_n:I

    goto :goto_0

    .line 538
    :cond_5
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_06:I

    if-ne p1, v0, :cond_6

    .line 539
    sget p1, Lcom/vlingo/midas/R$drawable;->weather_06_n:I

    goto :goto_0

    .line 540
    :cond_6
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_07:I

    if-ne p1, v0, :cond_0

    .line 541
    sget p1, Lcom/vlingo/midas/R$drawable;->weather_07_n:I

    goto :goto_0
.end method

.method public changeToVerticalBackground(I)I
    .locals 1
    .param p1, "landBackground"    # I

    .prologue
    .line 547
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_01:I

    if-ne p1, v0, :cond_0

    .line 548
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_01_v:I

    .line 580
    :goto_0
    return v0

    .line 549
    :cond_0
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_02:I

    if-ne p1, v0, :cond_1

    .line 550
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_02_v:I

    goto :goto_0

    .line 551
    :cond_1
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_03:I

    if-ne p1, v0, :cond_2

    .line 552
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_03_v:I

    goto :goto_0

    .line 553
    :cond_2
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_04:I

    if-ne p1, v0, :cond_3

    .line 554
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_04_v:I

    goto :goto_0

    .line 555
    :cond_3
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_05:I

    if-ne p1, v0, :cond_4

    .line 556
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_05_v:I

    goto :goto_0

    .line 557
    :cond_4
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_06:I

    if-ne p1, v0, :cond_5

    .line 558
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_06_v:I

    goto :goto_0

    .line 559
    :cond_5
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_07:I

    if-ne p1, v0, :cond_6

    .line 560
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_07_v:I

    goto :goto_0

    .line 561
    :cond_6
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_08:I

    if-ne p1, v0, :cond_7

    .line 562
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_08_v:I

    goto :goto_0

    .line 563
    :cond_7
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_09:I

    if-ne p1, v0, :cond_8

    .line 564
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_09_v:I

    goto :goto_0

    .line 565
    :cond_8
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_01_n:I

    if-ne p1, v0, :cond_9

    .line 566
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_01_v_n:I

    goto :goto_0

    .line 567
    :cond_9
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_02_n:I

    if-ne p1, v0, :cond_a

    .line 568
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_02_v_n:I

    goto :goto_0

    .line 569
    :cond_a
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_03_n:I

    if-ne p1, v0, :cond_b

    .line 570
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_03_v_n:I

    goto :goto_0

    .line 571
    :cond_b
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_04_n:I

    if-ne p1, v0, :cond_c

    .line 572
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_04_v_n:I

    goto :goto_0

    .line 573
    :cond_c
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_05_n:I

    if-ne p1, v0, :cond_d

    .line 574
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_05_v_n:I

    goto :goto_0

    .line 575
    :cond_d
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_06_n:I

    if-ne p1, v0, :cond_e

    .line 576
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_06_v_n:I

    goto :goto_0

    .line 577
    :cond_e
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_07_n:I

    if-ne p1, v0, :cond_f

    .line 578
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_07_v_n:I

    goto :goto_0

    .line 580
    :cond_f
    sget v0, Lcom/vlingo/midas/R$drawable;->weather_03_v:I

    goto :goto_0
.end method

.method public getWeatherBackgroundDrawable()I
    .locals 1

    .prologue
    .line 509
    invoke-direct {p0}, Lcom/vlingo/midas/util/WeatherResourceUtil;->imageForWeatherBackground()I

    move-result v0

    return v0
.end method

.method public getWeatherDrawable()I
    .locals 1

    .prologue
    .line 505
    invoke-direct {p0}, Lcom/vlingo/midas/util/WeatherResourceUtil;->imageForWeather()I

    move-result v0

    return v0
.end method

.method public getWeatherDrawableSmallSize()I
    .locals 1

    .prologue
    .line 517
    invoke-direct {p0}, Lcom/vlingo/midas/util/WeatherResourceUtil;->imageForWeatherSmallSize()I

    move-result v0

    return v0
.end method

.method public getWeatherSevenBackgroundDrawable()I
    .locals 1

    .prologue
    .line 513
    invoke-direct {p0}, Lcom/vlingo/midas/util/WeatherResourceUtil;->imageForWeatherSevenBackground()I

    move-result v0

    return v0
.end method
