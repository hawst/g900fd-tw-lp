.class public Lcom/vlingo/midas/util/Typefaces;
.super Ljava/lang/Object;
.source "Typefaces.java"


# static fields
.field private static final typefaceHash:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Typeface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/vlingo/midas/util/Typefaces;->typefaceHash:Ljava/util/Hashtable;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getTypeface(Ljava/lang/String;)Landroid/graphics/Typeface;
    .locals 4
    .param p0, "fontPath"    # Ljava/lang/String;

    .prologue
    .line 11
    sget-object v2, Lcom/vlingo/midas/util/Typefaces;->typefaceHash:Ljava/util/Hashtable;

    monitor-enter v2

    .line 12
    :try_start_0
    sget-object v1, Lcom/vlingo/midas/util/Typefaces;->typefaceHash:Ljava/util/Hashtable;

    invoke-virtual {v1, p0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 13
    const/4 v0, 0x0

    .line 15
    .local v0, "typeface":Landroid/graphics/Typeface;
    :try_start_1
    invoke-static {p0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 18
    if-nez v0, :cond_0

    .line 19
    :try_start_2
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    monitor-exit v2

    .line 24
    .end local v0    # "typeface":Landroid/graphics/Typeface;
    :goto_0
    return-object v1

    .line 21
    .restart local v0    # "typeface":Landroid/graphics/Typeface;
    :cond_0
    sget-object v1, Lcom/vlingo/midas/util/Typefaces;->typefaceHash:Ljava/util/Hashtable;

    invoke-virtual {v1, p0, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    .end local v0    # "typeface":Landroid/graphics/Typeface;
    :cond_1
    :goto_1
    sget-object v1, Lcom/vlingo/midas/util/Typefaces;->typefaceHash:Ljava/util/Hashtable;

    invoke-virtual {v1, p0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_4

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    :goto_2
    monitor-exit v2

    goto :goto_0

    .line 25
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 16
    .restart local v0    # "typeface":Landroid/graphics/Typeface;
    :catch_0
    move-exception v1

    .line 18
    if-nez v0, :cond_2

    .line 19
    :try_start_3
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    monitor-exit v2

    goto :goto_0

    .line 21
    :cond_2
    sget-object v1, Lcom/vlingo/midas/util/Typefaces;->typefaceHash:Ljava/util/Hashtable;

    invoke-virtual {v1, p0, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 18
    :catchall_1
    move-exception v1

    if-nez v0, :cond_3

    .line 19
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    monitor-exit v2

    goto :goto_0

    .line 21
    :cond_3
    sget-object v3, Lcom/vlingo/midas/util/Typefaces;->typefaceHash:Ljava/util/Hashtable;

    invoke-virtual {v3, p0, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    throw v1

    .line 24
    .end local v0    # "typeface":Landroid/graphics/Typeface;
    :cond_4
    sget-object v1, Lcom/vlingo/midas/util/Typefaces;->typefaceHash:Ljava/util/Hashtable;

    invoke-virtual {v1, p0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Typeface;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method
