.class public Lcom/vlingo/midas/util/ErrorCodeUtils;
.super Ljava/lang/Object;
.source "ErrorCodeUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/util/ErrorCodeUtils$1;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method

.method public static getLocalizedMessageForErrorCode(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "error"    # Lcom/vlingo/sdk/recognition/VLRecognitionErrors;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    sget v1, Lcom/vlingo/midas/R$string;->reco_error_0:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 17
    .local v0, "localizedMessage":Ljava/lang/String;
    sget-object v1, Lcom/vlingo/midas/util/ErrorCodeUtils$1;->$SwitchMap$com$vlingo$sdk$recognition$VLRecognitionErrors:[I

    invoke-virtual {p0}, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 46
    :goto_0
    return-object v0

    .line 19
    :pswitch_0
    sget v1, Lcom/vlingo/midas/R$string;->reco_error_1:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 20
    goto :goto_0

    .line 22
    :pswitch_1
    sget v1, Lcom/vlingo/midas/R$string;->reco_error_2:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 23
    goto :goto_0

    .line 25
    :pswitch_2
    sget v1, Lcom/vlingo/midas/R$string;->core_mic_in_use:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 26
    goto :goto_0

    .line 28
    :pswitch_3
    sget v1, Lcom/vlingo/midas/R$string;->reco_error_4:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 29
    goto :goto_0

    .line 31
    :pswitch_4
    sget v1, Lcom/vlingo/midas/R$string;->reco_error_5:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 32
    goto :goto_0

    .line 34
    :pswitch_5
    sget v1, Lcom/vlingo/midas/R$string;->reco_error_6:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 35
    goto :goto_0

    .line 37
    :pswitch_6
    sget v1, Lcom/vlingo/midas/R$string;->reco_error_7:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 38
    goto :goto_0

    .line 40
    :pswitch_7
    sget v1, Lcom/vlingo/midas/R$string;->reco_error_8:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 41
    goto :goto_0

    .line 43
    :pswitch_8
    sget v1, Lcom/vlingo/midas/R$string;->reco_error_10:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 17
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
