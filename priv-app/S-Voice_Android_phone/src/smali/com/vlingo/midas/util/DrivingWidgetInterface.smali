.class public interface abstract Lcom/vlingo/midas/util/DrivingWidgetInterface;
.super Ljava/lang/Object;
.source "DrivingWidgetInterface.java"


# virtual methods
.method public abstract getDecreasedHeight()I
.end method

.method public abstract getProperHeight()I
.end method

.method public abstract isDecreasedSize()Z
.end method

.method public abstract setNightMode(Z)I
.end method

.method public abstract setWidgetToDecreasedSize(Z)I
.end method

.method public abstract startAnimationTranslate(Landroid/view/View;)V
.end method
