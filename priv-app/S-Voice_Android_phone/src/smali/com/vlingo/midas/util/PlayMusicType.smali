.class public final enum Lcom/vlingo/midas/util/PlayMusicType;
.super Ljava/lang/Enum;
.source "PlayMusicType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/util/PlayMusicType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/util/PlayMusicType;

.field public static final enum ALBUM:Lcom/vlingo/midas/util/PlayMusicType;

.field public static final enum ARTIST:Lcom/vlingo/midas/util/PlayMusicType;

.field public static final enum MOOD:Lcom/vlingo/midas/util/PlayMusicType;

.field public static final enum NEXT:Lcom/vlingo/midas/util/PlayMusicType;

.field public static final enum PAUSE:Lcom/vlingo/midas/util/PlayMusicType;

.field public static final enum PLAY:Lcom/vlingo/midas/util/PlayMusicType;

.field public static final enum PLAYLIST:Lcom/vlingo/midas/util/PlayMusicType;

.field public static final enum PREVIOUS:Lcom/vlingo/midas/util/PlayMusicType;

.field public static final enum SONGLIST:Lcom/vlingo/midas/util/PlayMusicType;

.field public static final enum TITLE:Lcom/vlingo/midas/util/PlayMusicType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4
    new-instance v0, Lcom/vlingo/midas/util/PlayMusicType;

    const-string/jumbo v1, "ALBUM"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/midas/util/PlayMusicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/PlayMusicType;->ALBUM:Lcom/vlingo/midas/util/PlayMusicType;

    .line 5
    new-instance v0, Lcom/vlingo/midas/util/PlayMusicType;

    const-string/jumbo v1, "ARTIST"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/midas/util/PlayMusicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/PlayMusicType;->ARTIST:Lcom/vlingo/midas/util/PlayMusicType;

    .line 6
    new-instance v0, Lcom/vlingo/midas/util/PlayMusicType;

    const-string/jumbo v1, "NEXT"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/midas/util/PlayMusicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/PlayMusicType;->NEXT:Lcom/vlingo/midas/util/PlayMusicType;

    .line 7
    new-instance v0, Lcom/vlingo/midas/util/PlayMusicType;

    const-string/jumbo v1, "PAUSE"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/midas/util/PlayMusicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/PlayMusicType;->PAUSE:Lcom/vlingo/midas/util/PlayMusicType;

    .line 8
    new-instance v0, Lcom/vlingo/midas/util/PlayMusicType;

    const-string/jumbo v1, "PLAY"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/midas/util/PlayMusicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/PlayMusicType;->PLAY:Lcom/vlingo/midas/util/PlayMusicType;

    .line 9
    new-instance v0, Lcom/vlingo/midas/util/PlayMusicType;

    const-string/jumbo v1, "PLAYLIST"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/PlayMusicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/PlayMusicType;->PLAYLIST:Lcom/vlingo/midas/util/PlayMusicType;

    .line 10
    new-instance v0, Lcom/vlingo/midas/util/PlayMusicType;

    const-string/jumbo v1, "PREVIOUS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/PlayMusicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/PlayMusicType;->PREVIOUS:Lcom/vlingo/midas/util/PlayMusicType;

    .line 11
    new-instance v0, Lcom/vlingo/midas/util/PlayMusicType;

    const-string/jumbo v1, "TITLE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/PlayMusicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/PlayMusicType;->TITLE:Lcom/vlingo/midas/util/PlayMusicType;

    .line 12
    new-instance v0, Lcom/vlingo/midas/util/PlayMusicType;

    const-string/jumbo v1, "SONGLIST"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/PlayMusicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/PlayMusicType;->SONGLIST:Lcom/vlingo/midas/util/PlayMusicType;

    .line 13
    new-instance v0, Lcom/vlingo/midas/util/PlayMusicType;

    const-string/jumbo v1, "MOOD"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/PlayMusicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/PlayMusicType;->MOOD:Lcom/vlingo/midas/util/PlayMusicType;

    .line 3
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/vlingo/midas/util/PlayMusicType;

    sget-object v1, Lcom/vlingo/midas/util/PlayMusicType;->ALBUM:Lcom/vlingo/midas/util/PlayMusicType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/midas/util/PlayMusicType;->ARTIST:Lcom/vlingo/midas/util/PlayMusicType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/midas/util/PlayMusicType;->NEXT:Lcom/vlingo/midas/util/PlayMusicType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/midas/util/PlayMusicType;->PAUSE:Lcom/vlingo/midas/util/PlayMusicType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/midas/util/PlayMusicType;->PLAY:Lcom/vlingo/midas/util/PlayMusicType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/midas/util/PlayMusicType;->PLAYLIST:Lcom/vlingo/midas/util/PlayMusicType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/midas/util/PlayMusicType;->PREVIOUS:Lcom/vlingo/midas/util/PlayMusicType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/midas/util/PlayMusicType;->TITLE:Lcom/vlingo/midas/util/PlayMusicType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vlingo/midas/util/PlayMusicType;->SONGLIST:Lcom/vlingo/midas/util/PlayMusicType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vlingo/midas/util/PlayMusicType;->MOOD:Lcom/vlingo/midas/util/PlayMusicType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/midas/util/PlayMusicType;->$VALUES:[Lcom/vlingo/midas/util/PlayMusicType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/util/PlayMusicType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/vlingo/midas/util/PlayMusicType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/util/PlayMusicType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/util/PlayMusicType;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/vlingo/midas/util/PlayMusicType;->$VALUES:[Lcom/vlingo/midas/util/PlayMusicType;

    invoke-virtual {v0}, [Lcom/vlingo/midas/util/PlayMusicType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/util/PlayMusicType;

    return-object v0
.end method
