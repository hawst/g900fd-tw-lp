.class public Lcom/vlingo/midas/util/HelpHubCommon;
.super Ljava/lang/Object;
.source "HelpHubCommon.java"


# static fields
.field static final AUTHORITY:Ljava/lang/String; = "com.samsung.helphub.provider.downloadable"

.field static final PROVIDER_URI:Ljava/lang/String; = "content://com.samsung.helphub.provider.downloadable"

.field static final mPackageName:Ljava/lang/String; = "com.samsung.helpplugin"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isDownloadable(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 78
    const/4 v0, 0x0

    .line 79
    .local v0, "cursor":Landroid/database/Cursor;
    const/4 v1, 0x0

    .line 93
    .local v1, "download_plugin":I
    return v1
.end method

.method public static isResourceAvailable(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    const/4 v1, 0x0

    .line 60
    .local v1, "result":Z
    invoke-static {p0}, Lcom/vlingo/midas/util/HelpHubCommon;->isDownloadable(Landroid/content/Context;)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 61
    const/4 v0, 0x0

    .line 63
    .local v0, "info":Landroid/content/pm/ApplicationInfo;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string/jumbo v3, "com.samsung.helpplugin"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 67
    :goto_0
    if-eqz v0, :cond_0

    .line 68
    const/4 v1, 0x1

    .line 73
    .end local v0    # "info":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_1
    return v1

    .line 71
    :cond_1
    const/4 v1, 0x1

    goto :goto_1

    .line 65
    .restart local v0    # "info":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static requestDownloadingResource(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/vlingo/midas/R$string;->help:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->voice_prompt_confirmation_cancel:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->voice_prompt_confirmation_ok:I

    new-instance v3, Lcom/vlingo/midas/util/HelpHubCommon$1;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/util/HelpHubCommon$1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 49
    .local v0, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    sget v1, Lcom/vlingo/midas/R$string;->downloadable_dialog_popup_text_for_chinese:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 54
    :goto_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 55
    return-void

    .line 52
    :cond_0
    sget v1, Lcom/vlingo/midas/R$string;->downloadable_dialog_popup_text:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method
