.class public Lcom/vlingo/midas/util/SViewCoverUtils;
.super Ljava/lang/Object;
.source "SViewCoverUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/util/SViewCoverUtils$OnSViewCoverStateChangeListener;
    }
.end annotation


# static fields
.field private static isCoverGUISet:Z

.field private static isSViewCoverOpen:Z

.field private static lstListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/util/SViewCoverUtils$OnSViewCoverStateChangeListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x1

    sput-boolean v0, Lcom/vlingo/midas/util/SViewCoverUtils;->isSViewCoverOpen:Z

    .line 10
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/util/SViewCoverUtils;->isCoverGUISet:Z

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/vlingo/midas/util/SViewCoverUtils;->lstListeners:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method

.method public static addListener(Lcom/vlingo/midas/util/SViewCoverUtils$OnSViewCoverStateChangeListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/vlingo/midas/util/SViewCoverUtils$OnSViewCoverStateChangeListener;

    .prologue
    .line 29
    sget-object v0, Lcom/vlingo/midas/util/SViewCoverUtils;->lstListeners:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    return-void
.end method

.method public static isCoverGUISet()Z
    .locals 1

    .prologue
    .line 47
    sget-boolean v0, Lcom/vlingo/midas/util/SViewCoverUtils;->isCoverGUISet:Z

    return v0
.end method

.method public static isSViewCoverOpen()Z
    .locals 1

    .prologue
    .line 22
    sget-boolean v0, Lcom/vlingo/midas/util/SViewCoverUtils;->isSViewCoverOpen:Z

    return v0
.end method

.method private static notifyListeners(Z)V
    .locals 3
    .param p0, "isSViewCoverOpen"    # Z

    .prologue
    .line 37
    sget-object v2, Lcom/vlingo/midas/util/SViewCoverUtils;->lstListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/util/SViewCoverUtils$OnSViewCoverStateChangeListener;

    .line 38
    .local v1, "listener":Lcom/vlingo/midas/util/SViewCoverUtils$OnSViewCoverStateChangeListener;
    invoke-interface {v1, p0}, Lcom/vlingo/midas/util/SViewCoverUtils$OnSViewCoverStateChangeListener;->OnCoverStateChange(Z)V

    goto :goto_0

    .line 40
    .end local v1    # "listener":Lcom/vlingo/midas/util/SViewCoverUtils$OnSViewCoverStateChangeListener;
    :cond_0
    return-void
.end method

.method public static removeListener(Lcom/vlingo/midas/util/SViewCoverUtils$OnSViewCoverStateChangeListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/vlingo/midas/util/SViewCoverUtils$OnSViewCoverStateChangeListener;

    .prologue
    .line 33
    sget-object v0, Lcom/vlingo/midas/util/SViewCoverUtils;->lstListeners:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 34
    return-void
.end method

.method public static setCoverGUI(Z)V
    .locals 0
    .param p0, "coverGUISet"    # Z

    .prologue
    .line 43
    sput-boolean p0, Lcom/vlingo/midas/util/SViewCoverUtils;->isCoverGUISet:Z

    .line 44
    return-void
.end method

.method public static setSViewCoverState(Z)V
    .locals 1
    .param p0, "isCoverOpen"    # Z

    .prologue
    .line 15
    sget-boolean v0, Lcom/vlingo/midas/util/SViewCoverUtils;->isSViewCoverOpen:Z

    if-eq v0, p0, :cond_0

    .line 16
    sput-boolean p0, Lcom/vlingo/midas/util/SViewCoverUtils;->isSViewCoverOpen:Z

    .line 17
    invoke-static {p0}, Lcom/vlingo/midas/util/SViewCoverUtils;->notifyListeners(Z)V

    .line 19
    :cond_0
    return-void
.end method
