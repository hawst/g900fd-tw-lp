.class public Lcom/vlingo/midas/util/MiniModeUtils;
.super Ljava/lang/Object;
.source "MiniModeUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/util/MiniModeUtils$OnMiniModeChangeListener;
    }
.end annotation


# static fields
.field private static final KEY_SVOICE_MINI_MODE:Ljava/lang/String; = "is_svoice_mini_mode"

.field private static isSVoiceMiniMode:Z

.field private static lstListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/util/MiniModeUtils$OnMiniModeChangeListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode:Z

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/vlingo/midas/util/MiniModeUtils;->lstListeners:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method public static addListener(Lcom/vlingo/midas/util/MiniModeUtils$OnMiniModeChangeListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/vlingo/midas/util/MiniModeUtils$OnMiniModeChangeListener;

    .prologue
    .line 38
    sget-object v0, Lcom/vlingo/midas/util/MiniModeUtils;->lstListeners:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    return-void
.end method

.method public static isSVoiceMiniMode()Z
    .locals 1

    .prologue
    .line 17
    sget-boolean v0, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode:Z

    return v0
.end method

.method private static notifyListeners(Z)V
    .locals 3
    .param p0, "isSVoiceMiniMode"    # Z

    .prologue
    .line 46
    sget-object v2, Lcom/vlingo/midas/util/MiniModeUtils;->lstListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/util/MiniModeUtils$OnMiniModeChangeListener;

    .line 47
    .local v1, "listener":Lcom/vlingo/midas/util/MiniModeUtils$OnMiniModeChangeListener;
    invoke-interface {v1, p0}, Lcom/vlingo/midas/util/MiniModeUtils$OnMiniModeChangeListener;->onMiniModeChange(Z)V

    goto :goto_0

    .line 49
    .end local v1    # "listener":Lcom/vlingo/midas/util/MiniModeUtils$OnMiniModeChangeListener;
    :cond_0
    return-void
.end method

.method public static removeListener(Lcom/vlingo/midas/util/MiniModeUtils$OnMiniModeChangeListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/vlingo/midas/util/MiniModeUtils$OnMiniModeChangeListener;

    .prologue
    .line 42
    sget-object v0, Lcom/vlingo/midas/util/MiniModeUtils;->lstListeners:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 43
    return-void
.end method

.method public static setMiniModeValueinDB(Z)V
    .locals 3
    .param p0, "isMiniMode"    # Z

    .prologue
    .line 30
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/VlingoApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "is_svoice_mini_mode"

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 31
    return-void

    .line 30
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setSVoiceMiniMode(Z)V
    .locals 3
    .param p0, "isMiniMode"    # Z

    .prologue
    .line 21
    sget-boolean v0, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode:Z

    if-eq v0, p0, :cond_0

    .line 22
    const-string/jumbo v0, "MiniModeUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "SVoiceMiniMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 23
    sput-boolean p0, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode:Z

    .line 24
    invoke-static {p0}, Lcom/vlingo/midas/util/MiniModeUtils;->notifyListeners(Z)V

    .line 26
    :cond_0
    invoke-static {p0}, Lcom/vlingo/midas/util/MiniModeUtils;->setMiniModeValueinDB(Z)V

    .line 27
    return-void
.end method
