.class public abstract Lcom/vlingo/midas/util/log/EventLogUtil;
.super Ljava/lang/Object;
.source "EventLogUtil.java"

# interfaces
.implements Lcom/vlingo/midas/util/log/IEventLogUtil;


# static fields
.field protected static final LOG_TAG:Ljava/lang/String; = "Vlingo"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public disabled(Landroid/content/SharedPreferences$Editor;)V
    .locals 2
    .param p1, "editor"    # Landroid/content/SharedPreferences$Editor;

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/vlingo/midas/util/log/EventLogUtil;->getSettingKey()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 35
    return-void
.end method

.method public enabled(Landroid/content/SharedPreferences$Editor;)V
    .locals 2
    .param p1, "editor"    # Landroid/content/SharedPreferences$Editor;

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/vlingo/midas/util/log/EventLogUtil;->getSettingKey()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 24
    return-void
.end method

.method protected abstract getRecognitionCompleteMessage()Ljava/lang/String;
.end method

.method protected abstract getRecognitionErrorMessage()Ljava/lang/String;
.end method

.method protected abstract getRecognitionResultMessage(Ljava/lang/String;)Ljava/lang/String;
.end method

.method protected abstract getRecognitionStartMessage()Ljava/lang/String;
.end method

.method protected abstract getSettingKey()Ljava/lang/String;
.end method

.method public isEnabled()Z
    .locals 3

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/vlingo/midas/util/log/EventLogUtil;->getSettingKey()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 41
    .local v0, "isEnabled":Z
    return v0
.end method

.method public onRecognitionComplete()V
    .locals 2

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/vlingo/midas/util/log/EventLogUtil;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    const-string/jumbo v0, "Vlingo"

    invoke-virtual {p0}, Lcom/vlingo/midas/util/log/EventLogUtil;->getRecognitionCompleteMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    :cond_0
    return-void
.end method

.method public onRecognitionError()V
    .locals 2

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/vlingo/midas/util/log/EventLogUtil;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    const-string/jumbo v0, "Vlingo"

    invoke-virtual {p0}, Lcom/vlingo/midas/util/log/EventLogUtil;->getRecognitionErrorMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    :cond_0
    return-void
.end method

.method public onRecognitionResult(Ljava/lang/String;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/vlingo/midas/util/log/EventLogUtil;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    const-string/jumbo v0, "Vlingo"

    invoke-virtual {p0, p1}, Lcom/vlingo/midas/util/log/EventLogUtil;->getRecognitionResultMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    :cond_0
    return-void
.end method

.method public onRecognitionStart()V
    .locals 2

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/vlingo/midas/util/log/EventLogUtil;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    const-string/jumbo v0, "Vlingo"

    invoke-virtual {p0}, Lcom/vlingo/midas/util/log/EventLogUtil;->getRecognitionStartMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    :cond_0
    return-void
.end method
