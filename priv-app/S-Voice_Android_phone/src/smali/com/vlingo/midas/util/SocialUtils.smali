.class public Lcom/vlingo/midas/util/SocialUtils;
.super Ljava/lang/Object;
.source "SocialUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/util/SocialUtils$1;,
        Lcom/vlingo/midas/util/SocialUtils$UpdateType;
    }
.end annotation


# static fields
.field public static final CONTROLLER_LOGIN:Ljava/lang/String; = "com.vlingo.core.internal.dialogmanager.controllers.socialupdatecontroller"

.field public static final EXTRA_CONTROLLER_LOGIN_RESULT:Ljava/lang/String; = "extra_controller_login_result"

.field private static mIntent:Landroid/content/Intent;

.field private static updateType:Lcom/vlingo/midas/util/SocialUtils$UpdateType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    return-void
.end method

.method public static clearUpdateType()V
    .locals 1

    .prologue
    .line 209
    sget-object v0, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->NONE:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    sput-object v0, Lcom/vlingo/midas/util/SocialUtils;->updateType:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    .line 211
    return-void
.end method

.method public static getAPIs(Lcom/vlingo/midas/social/api/SocialAPI$SocialCallback;)Lcom/vlingo/core/internal/util/SparseArrayMap;
    .locals 4
    .param p0, "socialActivity"    # Lcom/vlingo/midas/social/api/SocialAPI$SocialCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/midas/social/api/SocialAPI$SocialCallback;",
            ")",
            "Lcom/vlingo/core/internal/util/SparseArrayMap",
            "<",
            "Lcom/vlingo/midas/social/api/SocialAPI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    new-instance v0, Lcom/vlingo/core/internal/util/SparseArrayMap;

    invoke-direct {v0}, Lcom/vlingo/core/internal/util/SparseArrayMap;-><init>()V

    .line 64
    .local v0, "apis":Lcom/vlingo/core/internal/util/SparseArrayMap;, "Lcom/vlingo/core/internal/util/SparseArrayMap<Lcom/vlingo/midas/social/api/SocialAPI;>;"
    const/16 v2, 0x8

    new-instance v3, Lcom/vlingo/midas/social/api/FacebookAPI;

    move-object v1, p0

    check-cast v1, Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;

    invoke-direct {v3, v1}, Lcom/vlingo/midas/social/api/FacebookAPI;-><init>(Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;)V

    invoke-virtual {v0, v2, v3}, Lcom/vlingo/core/internal/util/SparseArrayMap;->put(ILjava/lang/Object;)V

    .line 65
    const/4 v2, 0x4

    new-instance v3, Lcom/vlingo/midas/social/api/TwitterAPI;

    move-object v1, p0

    check-cast v1, Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;

    invoke-direct {v3, v1}, Lcom/vlingo/midas/social/api/TwitterAPI;-><init>(Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;)V

    invoke-virtual {v0, v2, v3}, Lcom/vlingo/core/internal/util/SparseArrayMap;->put(ILjava/lang/Object;)V

    .line 66
    const/16 v1, 0x20

    new-instance v2, Lcom/vlingo/midas/social/api/WeiboAPI;

    check-cast p0, Lcom/vlingo/midas/social/api/WeiboAPI$WeiboCallBack;

    .end local p0    # "socialActivity":Lcom/vlingo/midas/social/api/SocialAPI$SocialCallback;
    invoke-direct {v2, p0}, Lcom/vlingo/midas/social/api/WeiboAPI;-><init>(Lcom/vlingo/midas/social/api/WeiboAPI$WeiboCallBack;)V

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/util/SparseArrayMap;->put(ILjava/lang/Object;)V

    .line 67
    return-object v0
.end method

.method public static getIntentAfterTTS()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 228
    sget-object v0, Lcom/vlingo/midas/util/SocialUtils;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public static getSocialNetworkNameList()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 72
    .local v0, "socialNetworks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string/jumbo v1, "all"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    const-string/jumbo v1, "weibo"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    :goto_0
    return-object v0

    .line 76
    :cond_0
    const-string/jumbo v1, "twitter"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    const-string/jumbo v1, "facebook"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static getUpdateType()Lcom/vlingo/midas/util/SocialUtils$UpdateType;
    .locals 1

    .prologue
    .line 191
    sget-object v0, Lcom/vlingo/midas/util/SocialUtils;->updateType:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    return-object v0
.end method

.method public static loginIntent(Z)Landroid/content/Intent;
    .locals 2
    .param p0, "success"    # Z

    .prologue
    .line 185
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.controllers.socialupdatecontroller"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 186
    .local v0, "loginIntent":Landroid/content/Intent;
    const-string/jumbo v1, "extra_controller_login_result"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 187
    return-object v0
.end method

.method public static loginToNetwork(Lcom/vlingo/midas/social/api/SocialNetworkType;)Ljava/lang/String;
    .locals 10
    .param p0, "socialNetworkType"    # Lcom/vlingo/midas/social/api/SocialNetworkType;

    .prologue
    const/16 v9, 0x20

    const/4 v8, 0x4

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 89
    const/4 v2, 0x0

    .line 91
    .local v2, "loginMessage":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/vlingo/midas/util/SocialUtils;->getAPIs(Lcom/vlingo/midas/social/api/SocialAPI$SocialCallback;)Lcom/vlingo/core/internal/util/SparseArrayMap;

    move-result-object v0

    .line 92
    .local v0, "apis":Lcom/vlingo/core/internal/util/SparseArrayMap;, "Lcom/vlingo/core/internal/util/SparseArrayMap<Lcom/vlingo/midas/social/api/SocialAPI;>;"
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 93
    .local v1, "ctx":Landroid/content/Context;
    sget-object v4, Lcom/vlingo/midas/util/SocialUtils$1;->$SwitchMap$com$vlingo$midas$social$api$SocialNetworkType:[I

    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/SocialNetworkType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 128
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 129
    const-string/jumbo v4, "weibo_account"

    invoke-static {v4, v6}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_0

    .line 132
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_login_to_weibo_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v2

    .line 133
    sget-object v3, Lcom/vlingo/midas/social/api/SocialNetworkType;->WEIBO:Lcom/vlingo/midas/social/api/SocialNetworkType;

    .line 134
    .local v3, "typeForLogin":Lcom/vlingo/midas/social/api/SocialNetworkType;
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v5

    invoke-virtual {v0, v9}, Lcom/vlingo/core/internal/util/SparseArrayMap;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v5, v1, v3, v4}, Lcom/vlingo/midas/VlingoApplication;->startSocialLogin(Landroid/content/Context;Lcom/vlingo/midas/social/api/SocialNetworkType;Lcom/vlingo/midas/social/api/SocialAPI;)V

    .line 164
    .end local v3    # "typeForLogin":Lcom/vlingo/midas/social/api/SocialNetworkType;
    :cond_0
    :goto_0
    return-object v2

    .line 95
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->facebookSSO()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 96
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->getLoginToNetworkErrorMsg()Ljava/lang/String;

    move-result-object v2

    .line 101
    :goto_1
    move-object v3, p0

    .line 103
    .restart local v3    # "typeForLogin":Lcom/vlingo/midas/social/api/SocialNetworkType;
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v5

    invoke-virtual {v0, v7}, Lcom/vlingo/core/internal/util/SparseArrayMap;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v5, v1, v3, v4}, Lcom/vlingo/midas/VlingoApplication;->startSocialLogin(Landroid/content/Context;Lcom/vlingo/midas/social/api/SocialNetworkType;Lcom/vlingo/midas/social/api/SocialAPI;)V

    goto :goto_0

    .line 98
    .end local v3    # "typeForLogin":Lcom/vlingo/midas/social/api/SocialNetworkType;
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_login_to_facebook_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 108
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->twitterSSO()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 109
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->getLoginToNetworkErrorMsg()Ljava/lang/String;

    move-result-object v2

    .line 115
    :goto_2
    move-object v3, p0

    .line 117
    .restart local v3    # "typeForLogin":Lcom/vlingo/midas/social/api/SocialNetworkType;
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v5

    invoke-virtual {v0, v8}, Lcom/vlingo/core/internal/util/SparseArrayMap;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v5, v1, v3, v4}, Lcom/vlingo/midas/VlingoApplication;->startSocialLogin(Landroid/content/Context;Lcom/vlingo/midas/social/api/SocialNetworkType;Lcom/vlingo/midas/social/api/SocialAPI;)V

    goto :goto_0

    .line 111
    .end local v3    # "typeForLogin":Lcom/vlingo/midas/social/api/SocialNetworkType;
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_login_to_facebook_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 121
    :pswitch_2
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_login_to_weibo_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v2

    .line 122
    move-object v3, p0

    .line 124
    .restart local v3    # "typeForLogin":Lcom/vlingo/midas/social/api/SocialNetworkType;
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v5

    invoke-virtual {v0, v9}, Lcom/vlingo/core/internal/util/SparseArrayMap;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v5, v1, v3, v4}, Lcom/vlingo/midas/VlingoApplication;->startSocialLogin(Landroid/content/Context;Lcom/vlingo/midas/social/api/SocialNetworkType;Lcom/vlingo/midas/social/api/SocialAPI;)V

    goto :goto_0

    .line 139
    .end local v3    # "typeForLogin":Lcom/vlingo/midas/social/api/SocialNetworkType;
    :cond_3
    const-string/jumbo v4, "facebook_account"

    invoke-static {v4, v6}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string/jumbo v4, "twitter_account"

    invoke-static {v4, v6}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_4

    .line 143
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_login_to_twitter_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v2

    .line 144
    sget-object v3, Lcom/vlingo/midas/social/api/SocialNetworkType;->TWITTER:Lcom/vlingo/midas/social/api/SocialNetworkType;

    .line 145
    .restart local v3    # "typeForLogin":Lcom/vlingo/midas/social/api/SocialNetworkType;
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v5

    invoke-virtual {v0, v8}, Lcom/vlingo/core/internal/util/SparseArrayMap;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v5, v1, v3, v4}, Lcom/vlingo/midas/VlingoApplication;->startSocialLogin(Landroid/content/Context;Lcom/vlingo/midas/social/api/SocialNetworkType;Lcom/vlingo/midas/social/api/SocialAPI;)V

    goto/16 :goto_0

    .line 147
    .end local v3    # "typeForLogin":Lcom/vlingo/midas/social/api/SocialNetworkType;
    :cond_4
    const-string/jumbo v4, "twitter_account"

    invoke-static {v4, v6}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string/jumbo v4, "facebook_account"

    invoke-static {v4, v6}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_5

    .line 151
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_login_to_facebook_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v2

    .line 152
    sget-object v3, Lcom/vlingo/midas/social/api/SocialNetworkType;->FACEBOOK:Lcom/vlingo/midas/social/api/SocialNetworkType;

    .line 153
    .restart local v3    # "typeForLogin":Lcom/vlingo/midas/social/api/SocialNetworkType;
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v5

    invoke-virtual {v0, v7}, Lcom/vlingo/core/internal/util/SparseArrayMap;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v5, v1, v3, v4}, Lcom/vlingo/midas/VlingoApplication;->startSocialLogin(Landroid/content/Context;Lcom/vlingo/midas/social/api/SocialNetworkType;Lcom/vlingo/midas/social/api/SocialAPI;)V

    goto/16 :goto_0

    .line 156
    .end local v3    # "typeForLogin":Lcom/vlingo/midas/social/api/SocialNetworkType;
    :cond_5
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_login_to_network_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v2

    .line 157
    move-object v3, p0

    .line 158
    .restart local v3    # "typeForLogin":Lcom/vlingo/midas/social/api/SocialNetworkType;
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v5

    invoke-virtual {v0, v7}, Lcom/vlingo/core/internal/util/SparseArrayMap;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v5, v1, v3, v4}, Lcom/vlingo/midas/VlingoApplication;->startSocialLogin(Landroid/content/Context;Lcom/vlingo/midas/social/api/SocialNetworkType;Lcom/vlingo/midas/social/api/SocialAPI;)V

    goto/16 :goto_0

    .line 93
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static setIntentAfterTTS(Landroid/content/Intent;)V
    .locals 0
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 224
    sput-object p0, Lcom/vlingo/midas/util/SocialUtils;->mIntent:Landroid/content/Intent;

    .line 225
    return-void
.end method

.method public static setNetworkPicture(ILandroid/graphics/Bitmap;)V
    .locals 1
    .param p0, "type"    # I
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 168
    sparse-switch p0, :sswitch_data_0

    .line 182
    :goto_0
    return-void

    .line 170
    :sswitch_0
    const-string/jumbo v0, "facebook_picture"

    invoke-static {v0, p1}, Lcom/vlingo/core/internal/settings/Settings;->setImage(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 174
    :sswitch_1
    const-string/jumbo v0, "twitter_picture"

    invoke-static {v0, p1}, Lcom/vlingo/core/internal/settings/Settings;->setImage(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 178
    :sswitch_2
    const-string/jumbo v0, "weibo_picture"

    invoke-static {v0, p1}, Lcom/vlingo/core/internal/settings/Settings;->setImage(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 168
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x8 -> :sswitch_0
        0x20 -> :sswitch_2
    .end sparse-switch
.end method

.method public static setUpdateType(Lcom/vlingo/midas/social/api/SocialNetworkType;)V
    .locals 1
    .param p0, "socialType"    # Lcom/vlingo/midas/social/api/SocialNetworkType;

    .prologue
    .line 195
    sget-object v0, Lcom/vlingo/midas/social/api/SocialNetworkType;->ALL:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-ne p0, v0, :cond_0

    .line 196
    sget-object v0, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->ALL:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    sput-object v0, Lcom/vlingo/midas/util/SocialUtils;->updateType:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    .line 206
    :goto_0
    return-void

    .line 197
    :cond_0
    sget-object v0, Lcom/vlingo/midas/social/api/SocialNetworkType;->FACEBOOK:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-ne p0, v0, :cond_1

    .line 198
    sget-object v0, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->FACEBOOK_ONLY:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    sput-object v0, Lcom/vlingo/midas/util/SocialUtils;->updateType:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    goto :goto_0

    .line 199
    :cond_1
    sget-object v0, Lcom/vlingo/midas/social/api/SocialNetworkType;->TWITTER:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-ne p0, v0, :cond_2

    .line 200
    sget-object v0, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->TWITTER_ONLY:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    sput-object v0, Lcom/vlingo/midas/util/SocialUtils;->updateType:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    goto :goto_0

    .line 201
    :cond_2
    sget-object v0, Lcom/vlingo/midas/social/api/SocialNetworkType;->WEIBO:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-ne p0, v0, :cond_3

    .line 202
    sget-object v0, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->WEIBO_ONLY:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    sput-object v0, Lcom/vlingo/midas/util/SocialUtils;->updateType:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    goto :goto_0

    .line 204
    :cond_3
    sget-object v0, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->NONE:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    sput-object v0, Lcom/vlingo/midas/util/SocialUtils;->updateType:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    goto :goto_0
.end method
