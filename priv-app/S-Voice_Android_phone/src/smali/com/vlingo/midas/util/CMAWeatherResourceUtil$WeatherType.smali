.class public final enum Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;
.super Ljava/lang/Enum;
.source "CMAWeatherResourceUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/util/CMAWeatherResourceUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WeatherType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

.field public static final enum Cloudy:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

.field public static final enum DustStorm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

.field public static final enum Foggy:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

.field public static final enum Haze:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

.field public static final enum HeavyRain:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

.field public static final enum HeavySnow:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

.field public static final enum LightRain:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

.field public static final enum LightSnow:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

.field public static final enum Overcast:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

.field public static final enum Shower:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

.field public static final enum Sleet:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

.field public static final enum SnowFlurry:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

.field public static final enum SnowStorm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

.field public static final enum Storm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

.field public static final enum Sunny:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

.field public static final enum ThunderShower:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

.field public static final enum ThunderShowerwithHail:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 15
    new-instance v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "Sunny"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Sunny:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    .line 16
    new-instance v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "Cloudy"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Cloudy:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    .line 17
    new-instance v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "Overcast"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Overcast:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    .line 18
    new-instance v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "Shower"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Shower:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    .line 19
    new-instance v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "ThunderShower"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->ThunderShower:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    .line 20
    new-instance v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "ThunderShowerwithHail"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->ThunderShowerwithHail:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    .line 21
    new-instance v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "Sleet"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Sleet:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    .line 22
    new-instance v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "LightRain"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->LightRain:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    .line 23
    new-instance v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "HeavyRain"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->HeavyRain:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    .line 24
    new-instance v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "Storm"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Storm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    .line 25
    new-instance v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "SnowFlurry"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->SnowFlurry:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    .line 26
    new-instance v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "LightSnow"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->LightSnow:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    .line 27
    new-instance v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "HeavySnow"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->HeavySnow:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    .line 28
    new-instance v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "SnowStorm"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->SnowStorm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    .line 29
    new-instance v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "Foggy"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Foggy:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    .line 30
    new-instance v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "DustStorm"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->DustStorm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    .line 31
    new-instance v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    const-string/jumbo v1, "Haze"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Haze:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    .line 14
    const/16 v0, 0x11

    new-array v0, v0, [Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Sunny:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Cloudy:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Overcast:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Shower:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->ThunderShower:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->ThunderShowerwithHail:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Sleet:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->LightRain:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->HeavyRain:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Storm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->SnowFlurry:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->LightSnow:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->HeavySnow:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->SnowStorm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Foggy:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->DustStorm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Haze:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->$VALUES:[Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->$VALUES:[Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    invoke-virtual {v0}, [Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    return-object v0
.end method
