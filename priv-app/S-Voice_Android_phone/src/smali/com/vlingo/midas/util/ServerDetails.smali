.class public Lcom/vlingo/midas/util/ServerDetails;
.super Ljava/lang/Object;
.source "ServerDetails.java"

# interfaces
.implements Lcom/vlingo/core/internal/util/CoreServerInfo;


# static fields
.field public static final DEFAULT_ASR_SERVER_HOST:Ljava/lang/String; = "samsungtasr.vlingo.com"

.field public static final DEFAULT_HELLO_SERVER_HOST:Ljava/lang/String; = "samsungtasr.vlingo.com"

.field public static final DEFAULT_LMTT_SERVER_HOST:Ljava/lang/String; = "samsungtlmtt.vlingo.com"

.field public static final DEFAULT_LOG_SERVER_HOST:Ljava/lang/String; = "samsungtasr.vlingo.com"

.field public static final DEFAULT_TTS_SERVER_HOST:Ljava/lang/String; = "samsungttts.vlingo.com"

.field public static final DEFAULT_VCS_SERVER_HOST:Ljava/lang/String; = "samsungtvcs.vlingo.com"

.field private static smInstance:Lcom/vlingo/midas/util/ServerDetails;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/vlingo/midas/util/ServerDetails;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/vlingo/midas/util/ServerDetails;->smInstance:Lcom/vlingo/midas/util/ServerDetails;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Lcom/vlingo/midas/util/ServerDetails;

    invoke-direct {v0}, Lcom/vlingo/midas/util/ServerDetails;-><init>()V

    sput-object v0, Lcom/vlingo/midas/util/ServerDetails;->smInstance:Lcom/vlingo/midas/util/ServerDetails;

    .line 50
    :cond_0
    sget-object v0, Lcom/vlingo/midas/util/ServerDetails;->smInstance:Lcom/vlingo/midas/util/ServerDetails;

    return-object v0
.end method

.method private getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-static {p1, p2}, Lcom/vlingo/midas/samsungutils/configuration/NduUtils;->getNduHost(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 85
    invoke-static {p1, p2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getASRHost()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55
    const-string/jumbo v0, "SERVER_NAME"

    const-string/jumbo v1, "samsungtasr.vlingo.com"

    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/util/ServerDetails;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHelloHost()Ljava/lang/String;
    .locals 2

    .prologue
    .line 80
    const-string/jumbo v0, "HELLO_HOST_NAME"

    const-string/jumbo v1, "samsungtasr.vlingo.com"

    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/util/ServerDetails;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLMTTHost()Ljava/lang/String;
    .locals 2

    .prologue
    .line 75
    const-string/jumbo v0, "LMTT_HOST_NAME"

    const-string/jumbo v1, "samsungtlmtt.vlingo.com"

    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/util/ServerDetails;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLogHost()Ljava/lang/String;
    .locals 2

    .prologue
    .line 70
    const-string/jumbo v0, "EVENTLOG_HOST_NAME"

    const-string/jumbo v1, "samsungtasr.vlingo.com"

    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/util/ServerDetails;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTTSHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    return-object v0
.end method

.method public getVCSHost()Ljava/lang/String;
    .locals 2

    .prologue
    .line 65
    const-string/jumbo v0, "SERVICES_HOST_NAME"

    const-string/jumbo v1, "samsungtvcs.vlingo.com"

    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/util/ServerDetails;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
