.class public Lcom/vlingo/midas/util/log/AsrEventLogUtil;
.super Lcom/vlingo/midas/util/log/EventLogUtil;
.source "AsrEventLogUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/vlingo/midas/util/log/EventLogUtil;-><init>()V

    return-void
.end method


# virtual methods
.method protected getRecognitionCompleteMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    const-string/jumbo v0, "TEST_PLATFORM: VOICE_SEARCH_COMPLETE"

    return-object v0
.end method

.method protected getRecognitionErrorMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    const-string/jumbo v0, "TEST_PLATFORM: ERROR"

    return-object v0
.end method

.method protected getRecognitionResultMessage(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "TEST_PLATFORM: RESULTS: result: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez p1, :cond_0

    const-string/jumbo v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected getRecognitionStartMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    const-string/jumbo v0, "TEST_PLATFORM: SPEAK_NOW"

    return-object v0
.end method

.method public getSettingKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    const-string/jumbo v0, "asr_event_logging"

    return-object v0
.end method
