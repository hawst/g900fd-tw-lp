.class public Lcom/vlingo/midas/datadeletion/DataDeletionUtil;
.super Ljava/lang/Object;
.source "DataDeletionUtil.java"


# static fields
.field private static final CLEAR_DATA_INTENT:Ljava/lang/String; = "com.vlingo.midas.datadeletion.ClearAppData"

.field private static final CLEAR_DATA_INTENT_SUFFIX:Ljava/lang/String; = ".datadeletion.ClearAppData"

.field private static final CLEAR_DATA_PERMISSION:Ljava/lang/String; = "com.vlingo.midas.permission.Clearappdata"

.field private static final KEY_RELATED_APP_NAMES_FOR_DATA_DELETOING:Ljava/lang/String; = "com.vlingo.midas.datadeletion.RelatedAppNames"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
