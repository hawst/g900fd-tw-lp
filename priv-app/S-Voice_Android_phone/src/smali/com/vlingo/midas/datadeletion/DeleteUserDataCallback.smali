.class public Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;
.super Ljava/lang/Object;
.source "DeleteUserDataCallback.java"

# interfaces
.implements Lcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback$DataDeletionListener;
    }
.end annotation


# instance fields
.field private listener:Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback$DataDeletionListener;

.field private final mContext:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private userDataDeletionConfirmServerFailed:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "userDataDeletionConfirmServerFailed"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p2, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->userDataDeletionConfirmServerFailed:Ljava/lang/String;

    .line 32
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->mContext:Ljava/lang/ref/WeakReference;

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback$DataDeletionListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "userDataDeletionConfirmServerFailed"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback$DataDeletionListener;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p2, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->userDataDeletionConfirmServerFailed:Ljava/lang/String;

    .line 38
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->mContext:Ljava/lang/ref/WeakReference;

    .line 39
    iput-object p3, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->listener:Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback$DataDeletionListener;

    .line 40
    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/String;)V
    .locals 2
    .param p1, "failureInfo"    # Ljava/lang/String;

    .prologue
    .line 67
    iget-object v1, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 68
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 77
    :goto_0
    return-void

    .line 75
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->listener:Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback$DataDeletionListener;

    invoke-interface {v1}, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback$DataDeletionListener;->onFailure()V

    goto :goto_0
.end method

.method public onSuccess(Lcom/vlingo/core/internal/userdata/UserDataManager$ServerDataDeletionListener;)V
    .locals 2
    .param p1, "callback"    # Lcom/vlingo/core/internal/userdata/UserDataManager$ServerDataDeletionListener;

    .prologue
    .line 44
    iget-object v1, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 45
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 56
    :goto_0
    return-void

    .line 54
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->deleteLocalApplicationData()V

    .line 55
    iget-object v1, p0, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;->listener:Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback$DataDeletionListener;

    invoke-interface {v1, p1}, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback$DataDeletionListener;->onSuccess(Lcom/vlingo/core/internal/userdata/UserDataManager$ServerDataDeletionListener;)V

    goto :goto_0
.end method
