.class public Lcom/vlingo/midas/news/NewsManager;
.super Ljava/lang/Object;
.source "NewsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/news/NewsManager$ClientReceiver;,
        Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;
    }
.end annotation


# static fields
.field private static final Flip_SStoken:Ljava/lang/String; = "819b8dcb"

.field private static final NEWS:Ljava/lang/String; = "news"

.field private static final NewsDaemonPackageName:Ljava/lang/String; = "com.sec.android.daemonapp"

.field private static final PartialNewsDaemonPackageName:Ljava/lang/String; = "com.sec.android.daemonapp.ap.yonhapnews"

.field private static _instance:Lcom/vlingo/midas/news/NewsManager;

.field private static mYonHapReceiver:Landroid/content/BroadcastReceiver;


# instance fields
.field private bUsingYonhapNews:Z

.field private currentIndex:I

.field private mCurrentLocale:Ljava/util/Locale;

.field private mError:Lflipboard/api/FlipManager$ErrorMessage;

.field private mFetchListener:Lflipboard/api/FlipFetchListener;

.field private final mManager:Lflipboard/api/FlipManager;

.field private mRequestedLocale:Ljava/util/Locale;

.field private mThread:Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;

.field private final newsItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/news/NewsItem;",
            ">;"
        }
    .end annotation
.end field

.field private requestTypeFlags:I

.field private widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/news/NewsManager;->mYonHapReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/news/NewsManager;->newsItems:Ljava/util/List;

    .line 40
    iput v1, p0, Lcom/vlingo/midas/news/NewsManager;->currentIndex:I

    .line 42
    iput-object v2, p0, Lcom/vlingo/midas/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    .line 43
    iput-object v2, p0, Lcom/vlingo/midas/news/NewsManager;->mRequestedLocale:Ljava/util/Locale;

    .line 44
    sget-object v0, Lflipboard/api/FlipManager$ErrorMessage;->DUPLICATE_REQUEST:Lflipboard/api/FlipManager$ErrorMessage;

    iput-object v0, p0, Lcom/vlingo/midas/news/NewsManager;->mError:Lflipboard/api/FlipManager$ErrorMessage;

    .line 45
    iput v1, p0, Lcom/vlingo/midas/news/NewsManager;->requestTypeFlags:I

    .line 46
    iput-object v2, p0, Lcom/vlingo/midas/news/NewsManager;->mThread:Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;

    .line 47
    iput-boolean v1, p0, Lcom/vlingo/midas/news/NewsManager;->bUsingYonhapNews:Z

    .line 49
    new-instance v0, Lcom/vlingo/midas/news/NewsManager$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/news/NewsManager$1;-><init>(Lcom/vlingo/midas/news/NewsManager;)V

    iput-object v0, p0, Lcom/vlingo/midas/news/NewsManager;->mFetchListener:Lflipboard/api/FlipFetchListener;

    .line 164
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lflipboard/api/FlipManager;->getInstance(Landroid/content/Context;)Lflipboard/api/FlipManager;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/news/NewsManager;->mManager:Lflipboard/api/FlipManager;

    .line 166
    return-void
.end method

.method static synthetic access$002(Lcom/vlingo/midas/news/NewsManager;Lflipboard/api/FlipManager$ErrorMessage;)Lflipboard/api/FlipManager$ErrorMessage;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/news/NewsManager;
    .param p1, "x1"    # Lflipboard/api/FlipManager$ErrorMessage;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/vlingo/midas/news/NewsManager;->mError:Lflipboard/api/FlipManager$ErrorMessage;

    return-object p1
.end method

.method static synthetic access$100(Lcom/vlingo/midas/news/NewsManager;)Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/news/NewsManager;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/midas/news/NewsManager;->mThread:Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/news/NewsManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/news/NewsManager;

    .prologue
    .line 30
    iget v0, p0, Lcom/vlingo/midas/news/NewsManager;->requestTypeFlags:I

    return v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/news/NewsManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/news/NewsManager;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/vlingo/midas/news/NewsManager;->getFlipNewsData()V

    return-void
.end method

.method static synthetic access$400(Lcom/vlingo/midas/news/NewsManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/news/NewsManager;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/vlingo/midas/news/NewsManager;->getYonhapNewsData()V

    return-void
.end method

.method static synthetic access$500(Lcom/vlingo/midas/news/NewsManager;)Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/news/NewsManager;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/midas/news/NewsManager;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    return-object v0
.end method

.method static synthetic access$602(Lcom/vlingo/midas/news/NewsManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/news/NewsManager;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/vlingo/midas/news/NewsManager;->bUsingYonhapNews:Z

    return p1
.end method

.method public static deinit()V
    .locals 3

    .prologue
    .line 259
    invoke-static {}, Lcom/vlingo/midas/news/NewsManager;->isNewsDaemonAvailable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 260
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/vlingo/midas/news/NewsManager;->startYonhapNewsDaemon(Z)V

    .line 262
    sget-object v1, Lcom/vlingo/midas/news/NewsManager;->mYonHapReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 263
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/news/NewsManager;->mYonHapReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 266
    :cond_0
    const/4 v1, 0x0

    sput-object v1, Lcom/vlingo/midas/news/NewsManager;->mYonHapReceiver:Landroid/content/BroadcastReceiver;

    .line 268
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    .line 269
    .local v0, "df":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DriveNewsWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getWidgetSpecificProperties(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 270
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DriveNewsWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->removeWidgetSpecificProperty(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)V

    .line 275
    :cond_1
    return-void
.end method

.method private declared-synchronized getFlipNewsData()V
    .locals 7

    .prologue
    .line 374
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/vlingo/midas/news/NewsManager;->mManager:Lflipboard/api/FlipManager;

    const-string/jumbo v4, "news"

    iget-object v5, p0, Lcom/vlingo/midas/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    sget-object v6, Lflipboard/api/FlipManager$ContentType;->TEXT:Lflipboard/api/FlipManager$ContentType;

    invoke-virtual {v3, v4, v5, v6}, Lflipboard/api/FlipManager;->getItemsForFeed(Ljava/lang/String;Ljava/util/Locale;Lflipboard/api/FlipManager$ContentType;)Ljava/util/List;

    move-result-object v2

    .line 376
    .local v2, "items":Ljava/util/List;, "Ljava/util/List<Lflipboard/api/FlipboardItem;>;"
    const-string/jumbo v3, "NewsManager-flipNews"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, " refreshNewsData "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 379
    iget-object v3, p0, Lcom/vlingo/midas/news/NewsManager;->newsItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 380
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/api/FlipboardItem;

    .line 381
    .local v1, "item":Lflipboard/api/FlipboardItem;
    iget-object v3, p0, Lcom/vlingo/midas/news/NewsManager;->newsItems:Ljava/util/List;

    new-instance v4, Lcom/vlingo/midas/news/NewsItem;

    invoke-direct {v4, v1}, Lcom/vlingo/midas/news/NewsItem;-><init>(Lflipboard/api/FlipboardItem;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 374
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lflipboard/api/FlipboardItem;
    .end local v2    # "items":Ljava/util/List;, "Ljava/util/List<Lflipboard/api/FlipboardItem;>;"
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 384
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v2    # "items":Ljava/util/List;, "Ljava/util/List<Lflipboard/api/FlipboardItem;>;"
    :cond_0
    const/4 v3, 0x0

    :try_start_1
    invoke-direct {p0, v3}, Lcom/vlingo/midas/news/NewsManager;->updateCurrent(I)Lcom/vlingo/midas/news/NewsItem;

    move-result-object v3

    if-nez v3, :cond_1

    .line 385
    invoke-direct {p0}, Lcom/vlingo/midas/news/NewsManager;->reset()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/vlingo/midas/news/NewsManager;
    .locals 2

    .prologue
    .line 322
    const-class v1, Lcom/vlingo/midas/news/NewsManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/midas/news/NewsManager;->_instance:Lcom/vlingo/midas/news/NewsManager;

    if-nez v0, :cond_0

    .line 323
    new-instance v0, Lcom/vlingo/midas/news/NewsManager;

    invoke-direct {v0}, Lcom/vlingo/midas/news/NewsManager;-><init>()V

    sput-object v0, Lcom/vlingo/midas/news/NewsManager;->_instance:Lcom/vlingo/midas/news/NewsManager;

    .line 325
    :cond_0
    sget-object v0, Lcom/vlingo/midas/news/NewsManager;->_instance:Lcom/vlingo/midas/news/NewsManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 322
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getYonhapNewsData()V
    .locals 20

    .prologue
    .line 395
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    .line 396
    .local v9, "ctx":Landroid/content/Context;
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    .line 397
    .local v19, "typeCategory":Ljava/lang/String;
    const/4 v10, 0x0

    .line 399
    .local v10, "cursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/midas/news/NewsManager;->newsItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 401
    if-eqz v9, :cond_2

    .line 402
    :try_start_0
    new-instance v16, Ljava/lang/StringBuffer;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuffer;-><init>()V

    .line 403
    .local v16, "sb":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "NEWS_CATEGORY"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 404
    const-string/jumbo v1, " = ?"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 406
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/news/YonhapNewsDaemon$YonhapNewsColumns;->TABLE_URI:Landroid/net/Uri;

    sget-object v3, Lcom/vlingo/midas/news/YonhapNewsDaemon$YonhapNewsColumns;->CONTENTS_COLS:[Ljava/lang/String;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v19, v5, v6

    const-string/jumbo v6, "NEWS_CATEGORY ASC, NEWS_PUBDATE DESC"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 412
    new-instance v14, Lcom/vlingo/midas/news/YonhapNewsContainer;

    invoke-direct {v14}, Lcom/vlingo/midas/news/YonhapNewsContainer;-><init>()V

    .line 414
    .local v14, "news":Lcom/vlingo/midas/news/YonhapNewsContainer;
    if-eqz v10, :cond_1

    .line 415
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 416
    const-string/jumbo v1, "NEWS_LINK"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 417
    .local v13, "linkColumn":I
    const-string/jumbo v1, "NEWS_TIME"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 418
    .local v17, "timeIndex":I
    const-string/jumbo v1, "NEWS_CATEGORY"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 419
    .local v7, "categoryIndex":I
    const-string/jumbo v1, "NEWS_TITLE"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 420
    .local v18, "titleIndex":I
    const-string/jumbo v1, "NEWS_IMAGEURL"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 421
    .local v12, "imageUrlIndex":I
    const-string/jumbo v1, "NEWS_IMAGEDATA"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 422
    .local v11, "imageDataIndex":I
    const-string/jumbo v1, "NEWS_CONTENTTEXT"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 423
    .local v8, "contentTextIndex":I
    const-string/jumbo v1, "NEWS_DATE"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 424
    .local v15, "newsDateIndex":I
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_1

    .line 425
    invoke-interface {v10, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/vlingo/midas/news/YonhapNewsContainer;->NewsLink:Ljava/lang/String;

    .line 426
    move/from16 v0, v17

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v14, Lcom/vlingo/midas/news/YonhapNewsContainer;->NewsTime:Ljava/lang/Long;

    .line 427
    invoke-interface {v10, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/vlingo/midas/news/YonhapNewsContainer;->NewsCategory:Ljava/lang/String;

    .line 428
    move/from16 v0, v18

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/vlingo/midas/news/YonhapNewsContainer;->NewsTitle:Ljava/lang/String;

    .line 429
    invoke-interface {v10, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/vlingo/midas/news/YonhapNewsContainer;->NewsImageUrl:Ljava/lang/String;

    .line 430
    invoke-interface {v10, v11}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    iput-object v1, v14, Lcom/vlingo/midas/news/YonhapNewsContainer;->NewsImageData:[B

    .line 431
    invoke-interface {v10, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v14, Lcom/vlingo/midas/news/YonhapNewsContainer;->NewsContentText:Ljava/lang/String;

    .line 432
    invoke-interface {v10, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v14, Lcom/vlingo/midas/news/YonhapNewsContainer;->NewsPublishTime:Ljava/lang/Long;

    .line 434
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 435
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/midas/news/NewsManager;->newsItems:Ljava/util/List;

    new-instance v2, Lcom/vlingo/midas/news/NewsItem;

    invoke-direct {v2, v14}, Lcom/vlingo/midas/news/NewsItem;-><init>(Lcom/vlingo/midas/news/YonhapNewsContainer;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 436
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 437
    :try_start_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 445
    .end local v7    # "categoryIndex":I
    .end local v8    # "contentTextIndex":I
    .end local v11    # "imageDataIndex":I
    .end local v12    # "imageUrlIndex":I
    .end local v13    # "linkColumn":I
    .end local v14    # "news":Lcom/vlingo/midas/news/YonhapNewsContainer;
    .end local v15    # "newsDateIndex":I
    .end local v16    # "sb":Ljava/lang/StringBuffer;
    .end local v17    # "timeIndex":I
    .end local v18    # "titleIndex":I
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_0

    .line 446
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v1

    .line 436
    .restart local v7    # "categoryIndex":I
    .restart local v8    # "contentTextIndex":I
    .restart local v11    # "imageDataIndex":I
    .restart local v12    # "imageUrlIndex":I
    .restart local v13    # "linkColumn":I
    .restart local v14    # "news":Lcom/vlingo/midas/news/YonhapNewsContainer;
    .restart local v15    # "newsDateIndex":I
    .restart local v16    # "sb":Ljava/lang/StringBuffer;
    .restart local v17    # "timeIndex":I
    .restart local v18    # "titleIndex":I
    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v1

    .line 441
    .end local v7    # "categoryIndex":I
    .end local v8    # "contentTextIndex":I
    .end local v11    # "imageDataIndex":I
    .end local v12    # "imageUrlIndex":I
    .end local v13    # "linkColumn":I
    .end local v15    # "newsDateIndex":I
    .end local v17    # "timeIndex":I
    .end local v18    # "titleIndex":I
    :cond_1
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/news/NewsManager;->updateCurrent(I)Lcom/vlingo/midas/news/NewsItem;

    move-result-object v1

    if-nez v1, :cond_2

    .line 442
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/news/NewsManager;->reset()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 445
    .end local v14    # "news":Lcom/vlingo/midas/news/YonhapNewsContainer;
    .end local v16    # "sb":Ljava/lang/StringBuffer;
    :cond_2
    if-eqz v10, :cond_3

    .line 446
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 450
    :cond_3
    return-void
.end method

.method public static init()V
    .locals 4

    .prologue
    .line 246
    invoke-static {}, Lcom/vlingo/midas/news/NewsManager;->isNewsDaemonAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    new-instance v0, Lcom/vlingo/midas/news/NewsManager$ClientReceiver;

    invoke-static {}, Lcom/vlingo/midas/news/NewsManager;->getInstance()Lcom/vlingo/midas/news/NewsManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/news/NewsManager$ClientReceiver;-><init>(Lcom/vlingo/midas/news/NewsManager;Lcom/vlingo/midas/news/NewsManager$1;)V

    sput-object v0, Lcom/vlingo/midas/news/NewsManager;->mYonHapReceiver:Landroid/content/BroadcastReceiver;

    .line 249
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/news/NewsManager;->mYonHapReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "com.sec.android.daemonapp.ap.yonhapnews.intent.action.YONHAPNEWS_DATE_SYNC"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 251
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/midas/news/NewsManager;->startYonhapNewsDaemon(Z)V

    .line 254
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/news/NewsManager;->getInstance()Lcom/vlingo/midas/news/NewsManager;

    move-result-object v0

    invoke-direct {v0}, Lcom/vlingo/midas/news/NewsManager;->isFlipAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 256
    :cond_1
    return-void
.end method

.method private isFlipAvailable()Z
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/vlingo/midas/news/NewsManager;->mManager:Lflipboard/api/FlipManager;

    invoke-virtual {v0}, Lflipboard/api/FlipManager;->isFlipboardAvailable()Z

    move-result v0

    return v0
.end method

.method private static isNewsDaemonAvailable()Z
    .locals 6

    .prologue
    .line 278
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 279
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v3, 0x0

    .line 282
    .local v3, "ret":Z
    :try_start_0
    const-string/jumbo v4, "com.sec.android.daemonapp"

    const/16 v5, 0x80

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 283
    const/4 v3, 0x1

    :goto_0
    move v4, v3

    .line 314
    :goto_1
    return v4

    .line 287
    :catch_0
    move-exception v0

    .line 291
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    const-string/jumbo v4, "com.sec.android.daemonapp.ap.yonhapnews"

    const/16 v5, 0x80

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 292
    const/4 v3, 0x1

    goto :goto_0

    .line 293
    :catch_1
    move-exception v1

    .line 294
    .local v1, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 295
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private isYonhapAvailable()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 223
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getLocaleForIsoLanguage()Ljava/util/Locale;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    .line 225
    const-string/jumbo v3, "US"

    iget-object v4, p0, Lcom/vlingo/midas/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string/jumbo v3, "es"

    iget-object v4, p0, Lcom/vlingo/midas/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 227
    new-instance v3, Ljava/util/Locale;

    const-string/jumbo v4, "es"

    const-string/jumbo v5, "MX"

    invoke-direct {v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/vlingo/midas/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    .line 230
    :cond_0
    iget-object v3, p0, Lcom/vlingo/midas/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    sget-object v4, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 237
    :cond_1
    :goto_0
    return v1

    .line 232
    :cond_2
    iget-boolean v3, p0, Lcom/vlingo/midas/news/NewsManager;->bUsingYonhapNews:Z

    if-eqz v3, :cond_3

    move v1, v2

    goto :goto_0

    .line 233
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 234
    .local v0, "ctx":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "yonhap_daemon_service_key_service_status"

    invoke-static {v3, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-lez v3, :cond_1

    move v1, v2

    .line 235
    goto :goto_0
.end method

.method private declared-synchronized reset()V
    .locals 1

    .prologue
    .line 351
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/vlingo/midas/news/NewsManager;->currentIndex:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 352
    monitor-exit p0

    return-void

    .line 351
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static startYonhapNewsDaemon(Z)V
    .locals 9
    .param p0, "onOff"    # Z

    .prologue
    .line 454
    const-string/jumbo v1, "com.sec.android.daemonapp.ap.yonhapnews.intent.action.SERVICE_ON_OFF"

    .line 456
    .local v1, "ACTION_YONNEWS_SERVICE_ON_OFF":Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v7, "com.sec.android.daemonapp.ap.yonhapnews.intent.action.SERVICE_ON_OFF"

    invoke-direct {v3, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 458
    .local v3, "intent":Landroid/content/Intent;
    if-nez p0, :cond_0

    .line 459
    const-string/jumbo v7, "START"

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 465
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 466
    .local v2, "appContext":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 467
    .local v6, "packageName":Ljava/lang/String;
    const-string/jumbo v7, "PACKAGE"

    invoke-virtual {v3, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 468
    const-string/jumbo v7, "CP"

    const-string/jumbo v8, "YonhapNews"

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 470
    invoke-virtual {v2, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 473
    new-instance v5, Landroid/content/Intent;

    const-string/jumbo v7, "com.sec.android.daemonapp.ap.yonhapnews.intent.action.refresh"

    invoke-direct {v5, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 474
    .local v5, "intent3":Landroid/content/Intent;
    invoke-virtual {v2, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 476
    const-string/jumbo v0, "com.sec.android.daemonapp.ap.yonhapnews.intent.action.refresh"

    .line 477
    .local v0, "ACTION_DAEMON_NEWS_REFRESH":Ljava/lang/String;
    new-instance v4, Landroid/content/Intent;

    const-string/jumbo v7, "com.sec.android.daemonapp.ap.yonhapnews.intent.action.refresh"

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 478
    .local v4, "intent2":Landroid/content/Intent;
    const-string/jumbo v7, "PACKAGE"

    invoke-virtual {v4, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 479
    const-string/jumbo v7, "CP"

    const-string/jumbo v8, "YonhapNews"

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 480
    invoke-virtual {v2, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 481
    return-void

    .line 462
    .end local v0    # "ACTION_DAEMON_NEWS_REFRESH":Ljava/lang/String;
    .end local v2    # "appContext":Landroid/content/Context;
    .end local v4    # "intent2":Landroid/content/Intent;
    .end local v5    # "intent3":Landroid/content/Intent;
    .end local v6    # "packageName":Ljava/lang/String;
    :cond_0
    const-string/jumbo v7, "START"

    const/4 v8, 0x1

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method private declared-synchronized updateCurrent(I)Lcom/vlingo/midas/news/NewsItem;
    .locals 5
    .param p1, "direction"    # I

    .prologue
    const/4 v1, 0x0

    .line 329
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getLocaleForIsoLanguage()Ljava/util/Locale;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    .line 331
    iget-object v2, p0, Lcom/vlingo/midas/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/vlingo/midas/news/NewsManager;->mRequestedLocale:Ljava/util/Locale;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 347
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v1

    .line 334
    :cond_1
    :try_start_1
    const-string/jumbo v2, "US"

    iget-object v3, p0, Lcom/vlingo/midas/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string/jumbo v2, "es"

    iget-object v3, p0, Lcom/vlingo/midas/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 336
    new-instance v2, Ljava/util/Locale;

    const-string/jumbo v3, "es"

    const-string/jumbo v4, "MX"

    invoke-direct {v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/vlingo/midas/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    .line 339
    :cond_2
    iget-object v2, p0, Lcom/vlingo/midas/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    iget-object v3, p0, Lcom/vlingo/midas/news/NewsManager;->mRequestedLocale:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 342
    iget v2, p0, Lcom/vlingo/midas/news/NewsManager;->currentIndex:I

    add-int v0, v2, p1

    .line 343
    .local v0, "index":I
    if-ltz v0, :cond_0

    iget-object v2, p0, Lcom/vlingo/midas/news/NewsManager;->newsItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 344
    iput v0, p0, Lcom/vlingo/midas/news/NewsManager;->currentIndex:I

    .line 345
    iget-object v1, p0, Lcom/vlingo/midas/news/NewsManager;->newsItems:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/news/NewsItem;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 329
    .end local v0    # "index":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public fetchNews()Z
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 170
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getLocaleForIsoLanguage()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    .line 172
    const-string/jumbo v0, "US"

    iget-object v1, p0, Lcom/vlingo/midas/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "es"

    iget-object v1, p0, Lcom/vlingo/midas/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    new-instance v0, Ljava/util/Locale;

    const-string/jumbo v1, "es"

    const-string/jumbo v2, "MX"

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vlingo/midas/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    .line 177
    :cond_0
    sget-object v0, Lflipboard/api/FlipManager$ErrorMessage;->DUPLICATE_REQUEST:Lflipboard/api/FlipManager$ErrorMessage;

    iput-object v0, p0, Lcom/vlingo/midas/news/NewsManager;->mError:Lflipboard/api/FlipManager$ErrorMessage;

    .line 179
    iget-object v0, p0, Lcom/vlingo/midas/news/NewsManager;->mThread:Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;

    if-eqz v0, :cond_1

    .line 180
    const-string/jumbo v0, "NewsManager"

    const-string/jumbo v1, "mThread is not null!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    :cond_1
    new-instance v0, Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;-><init>(Lcom/vlingo/midas/news/NewsManager;)V

    iput-object v0, p0, Lcom/vlingo/midas/news/NewsManager;->mThread:Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;

    .line 185
    invoke-direct {p0}, Lcom/vlingo/midas/news/NewsManager;->isYonhapAvailable()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 186
    iput v9, p0, Lcom/vlingo/midas/news/NewsManager;->requestTypeFlags:I

    .line 192
    iget-object v0, p0, Lcom/vlingo/midas/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    iput-object v0, p0, Lcom/vlingo/midas/news/NewsManager;->mRequestedLocale:Ljava/util/Locale;

    .line 193
    new-instance v7, Landroid/content/Intent;

    const-string/jumbo v0, "com.sec.android.daemonapp.ap.yonhapnews.intent.action.refresh"

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 194
    .local v7, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 195
    const-string/jumbo v0, "NewsManager"

    const-string/jumbo v1, "request start yonhap request thread"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    .end local v7    # "intent":Landroid/content/Intent;
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/vlingo/midas/news/NewsManager;->mThread:Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;

    invoke-virtual {v0}, Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;->start()V

    move v0, v9

    .line 215
    :goto_1
    return v0

    .line 197
    :cond_3
    invoke-direct {p0}, Lcom/vlingo/midas/news/NewsManager;->isFlipAvailable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 200
    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/vlingo/midas/news/NewsManager;->requestTypeFlags:I

    .line 203
    iget-object v0, p0, Lcom/vlingo/midas/news/NewsManager;->mManager:Lflipboard/api/FlipManager;

    const-string/jumbo v1, "819b8dcb"

    const-string/jumbo v2, "news"

    sget-object v3, Lflipboard/api/FlipManager$ContentType;->TEXT:Lflipboard/api/FlipManager$ContentType;

    iget-object v4, p0, Lcom/vlingo/midas/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    iget-object v5, p0, Lcom/vlingo/midas/news/NewsManager;->mFetchListener:Lflipboard/api/FlipFetchListener;

    invoke-virtual/range {v0 .. v5}, Lflipboard/api/FlipManager;->fetchFlipboardItems(Ljava/lang/String;Ljava/lang/String;Lflipboard/api/FlipManager$ContentType;Ljava/util/Locale;Lflipboard/api/FlipFetchListener;)V

    .line 204
    iget-object v0, p0, Lcom/vlingo/midas/news/NewsManager;->mCurrentLocale:Ljava/util/Locale;

    iput-object v0, p0, Lcom/vlingo/midas/news/NewsManager;->mRequestedLocale:Ljava/util/Locale;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    const-string/jumbo v0, "NewsManager"

    const-string/jumbo v1, "request start flip request thread"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 207
    :catch_0
    move-exception v6

    .line 208
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v8

    .line 209
    goto :goto_1
.end method

.method public getCurrentNews()Lcom/vlingo/midas/news/NewsItem;
    .locals 1

    .prologue
    .line 360
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/midas/news/NewsManager;->updateCurrent(I)Lcom/vlingo/midas/news/NewsItem;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getFirstNews()Lcom/vlingo/midas/news/NewsItem;
    .locals 1

    .prologue
    .line 355
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/vlingo/midas/news/NewsManager;->reset()V

    .line 356
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/midas/news/NewsManager;->updateCurrent(I)Lcom/vlingo/midas/news/NewsItem;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 355
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getNextNews()Lcom/vlingo/midas/news/NewsItem;
    .locals 1

    .prologue
    .line 368
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/vlingo/midas/news/NewsManager;->updateCurrent(I)Lcom/vlingo/midas/news/NewsItem;

    move-result-object v0

    return-object v0
.end method

.method public getPrevNews()Lcom/vlingo/midas/news/NewsItem;
    .locals 1

    .prologue
    .line 364
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/vlingo/midas/news/NewsManager;->updateCurrent(I)Lcom/vlingo/midas/news/NewsItem;

    move-result-object v0

    return-object v0
.end method

.method public getRequestedLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 484
    iget-object v0, p0, Lcom/vlingo/midas/news/NewsManager;->mRequestedLocale:Ljava/util/Locale;

    return-object v0
.end method

.method public declared-synchronized hasCachedNews()Z
    .locals 1

    .prologue
    .line 318
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/news/NewsManager;->newsItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isAvailable()Z
    .locals 1

    .prologue
    .line 241
    invoke-direct {p0}, Lcom/vlingo/midas/news/NewsManager;->isYonhapAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/vlingo/midas/news/NewsManager;->isFlipAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V
    .locals 0
    .param p1, "widgetListener"    # Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .prologue
    .line 390
    iput-object p1, p0, Lcom/vlingo/midas/news/NewsManager;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .line 391
    return-void
.end method
