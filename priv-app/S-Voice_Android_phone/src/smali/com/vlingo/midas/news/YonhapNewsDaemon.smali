.class public Lcom/vlingo/midas/news/YonhapNewsDaemon;
.super Landroid/app/Application;
.source "YonhapNewsDaemon.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/news/YonhapNewsDaemon$SystemColumns;,
        Lcom/vlingo/midas/news/YonhapNewsDaemon$YonhapNewsXmlColumns;,
        Lcom/vlingo/midas/news/YonhapNewsDaemon$YonhapNewsRefContentColumns;,
        Lcom/vlingo/midas/news/YonhapNewsDaemon$YonhapNewsColumns;
    }
.end annotation


# static fields
.field public static final ACTION_BOOT_COMPLETED:Ljava/lang/String; = "android.intent.action.BOOT_COMPLETED"

.field public static final ACTION_DAEMON_NEWS_AUTOREFRESH:Ljava/lang/String; = "com.sec.android.daemonapp.ap.yonhapnews.intent.action.autorefresh"

.field public static final ACTION_DAEMON_NEWS_REFRESH:Ljava/lang/String; = "com.sec.android.daemonapp.ap.yonhapnews.intent.action.refresh"

.field public static final ACTION_DAEMON_NEWS_RELOAD_SETTING:Ljava/lang/String; = "com.sec.android.daemonapp.ap.yonhapnews.intent.action.reload.setting"

.field public static final ACTION_DAEMON_NEWS_SYNC_SETTING:Ljava/lang/String; = "com.sec.android.daemonapp.ap.yonhapnews.intent.action.sync.setting"

.field public static final ACTION_DAEMON_NEWS_UPDATE_RESULT:Ljava/lang/String; = "com.sec.android.daemonapp.ap.yonhapnews.intent.action.update_result"

.field public static final ACTION_WIDGET_NEWS_REFRESH:Ljava/lang/String; = "action.widget.news.refresh"

.field public static final ACTION_WIDGET_NEWS_RELOAD_SETTING:Ljava/lang/String; = "action.widget.news.reload.setting"

.field public static final ACTION_YONHAPNEWS_DAEMON_SERVICE_ON_OFF:Ljava/lang/String; = "com.sec.android.daemonapp.ap.yonhapnews.intent.action.SERVICE_ON_OFF"

.field public static final ACTION_YONHAPNEWS_DATE_SYNC:Ljava/lang/String; = "com.sec.android.daemonapp.ap.yonhapnews.intent.action.YONHAPNEWS_DATE_SYNC"

.field public static final ALARM_SERVICE_CODE:I = 0x4

.field public static final AUTHORITY:Ljava/lang/String; = "com.sec.android.daemonapp.ap.yonhapnews.provider"

.field public static final COMMON_PROCESS:Ljava/lang/String; = "com.sec.android.daemonapp.ap.yonhapnews.intent.extra.common.process"

.field public static final DB_NAME:Ljava/lang/String; = "YonhapNewsDaemon.db"

.field public static final DB_VERSION:I = 0x8

.field public static final DEB_PRINT:Z = true

.field public static final ECONOMICS:I = 0x2

.field public static final ENTERTAINMENT:I = 0x4

.field public static final ERROR_CODE:Ljava/lang/String; = "Error_Code"

.field public static final FUNTION_OFF:I = 0x0

.field public static final FUNTION_ON:I = 0x1

.field public static final GET_NEWS_DATA:Ljava/lang/String; = "Get_News_Data"

.field public static final GET_NEWS_HEAD_DATA:Ljava/lang/String; = "Get_News_Head_Data"

.field public static final HOURS_12:I = 0x2932e00

.field public static final HOURS_3:I = 0xa4cb80

.field public static final HOURS_6:I = 0x1499700

.field public static final LOCK_SCREEN_SERVICE_CODE:I = 0x1

.field public static final MAIN_NEWS:I = 0x0

.field public static final MINUTE:I = 0xea60

.field public static final NETWORK_TIMEOUT:I = 0x4e20

.field public static final NEWS_PROCESS:Ljava/lang/String; = "com.sec.android.daemonapp.ap.yonhapnews.intent.extra.news.process"

.field public static final NEWS_SD_ECON:Ljava/lang/String; = "4"

.field public static final NEWS_SD_ENT:Ljava/lang/String; = "7"

.field public static final NEWS_SD_MAIN:Ljava/lang/String; = "0"

.field public static final NEWS_SD_POLIT:Ljava/lang/String; = "2"

.field public static final NEWS_SD_SOCI:Ljava/lang/String; = "5"

.field public static final NEWS_SD_SPORT:Ljava/lang/String; = "9"

.field public static final NONE:Ljava/lang/String; = "None"

.field public static final POLITICS:I = 0x1

.field public static final REFRESH_12H:I = 0x8

.field public static final REFRESH_15M:I = 0x2

.field public static final REFRESH_1H:I = 0x4

.field public static final REFRESH_1M:I = 0x1

.field public static final REFRESH_24H:I = 0x9

.field public static final REFRESH_2H:I = 0x5

.field public static final REFRESH_30M:I = 0x3

.field public static final REFRESH_3H:I = 0x6

.field public static final REFRESH_6H:I = 0x7

.field public static final REFRESH_NONE:I = 0x0

.field public static final SERVICE_OFF:I = 0x0

.field public static final SERVICE_ON:I = 0x1

.field public static final SET_DISABLE:I = 0x0

.field public static final SET_ENABLE:I = 0x1

.field public static final SOCIAL:I = 0x3

.field public static final SPORTS:I = 0x5

.field public static final STATE_DATA_NOT_FOUND:I = -0x4

.field public static final STATE_DB_ERR:I = -0x3

.field public static final STATE_LOADING:I = 0x1

.field public static final STATE_NETWORK_ERROR:I = -0x2

.field public static final STATE_NETWORK_UNAVAILABLE:I = -0x1

.field public static final STATE_NORMAL:I = 0x0

.field public static final TAG:Ljava/lang/String; = "YonhapNewsDaemon"

.field public static final TOPICWALL_SERVICE_CODE:I = 0x8

.field public static final TYPE:Ljava/lang/String; = "Type"

.field public static final TYPE_DEFAULT:I = 0x0

.field public static final TYPE_PREFERENCE:I = 0x1e

.field public static final TYPE_XML:I = 0x32

.field public static final TYPE_YONHAPNEWS:I = 0x14

.field public static final TYPE_YONHAPNEWS_IMAGE:I = 0x15

.field public static final TYPE_YONHAPNEWS_REF_CONTENT:I = 0x28

.field public static final UPDATED_NEWS:Ljava/lang/String; = "com.sec.android.daemonapp.ap.yonhapnews.intent.action.Updated_news"

.field public static final UPDATED_RESULT:Ljava/lang/String; = "com.sec.android.daemonapp.ap.yonhapnews.intent.action.Updated_result"

.field public static final WALLPAPER_SERVICE_CODE:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method
