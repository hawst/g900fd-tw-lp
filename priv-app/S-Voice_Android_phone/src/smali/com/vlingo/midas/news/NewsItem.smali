.class public Lcom/vlingo/midas/news/NewsItem;
.super Ljava/lang/Object;
.source "NewsItem.java"


# static fields
.field public static final NEWS_FROM_FLIPBOARD:I = 0x1

.field public static final NEWS_FROM_YONHAP:I = 0x2


# instance fields
.field private mAuthor:Ljava/lang/String;

.field private mItem:Lflipboard/api/FlipboardItem;

.field private mLink:Ljava/lang/String;

.field private mNewsCP:I

.field private mText:Ljava/lang/String;

.field private mTimeCreated:J

.field private mTitle:Ljava/lang/String;

.field mUniqueId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/news/YonhapNewsContainer;)V
    .locals 3
    .param p1, "Item"    # Lcom/vlingo/midas/news/YonhapNewsContainer;

    .prologue
    const/4 v2, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/midas/news/NewsItem;->mTimeCreated:J

    .line 12
    iput-object v2, p0, Lcom/vlingo/midas/news/NewsItem;->mText:Ljava/lang/String;

    .line 13
    iput-object v2, p0, Lcom/vlingo/midas/news/NewsItem;->mTitle:Ljava/lang/String;

    .line 14
    iput-object v2, p0, Lcom/vlingo/midas/news/NewsItem;->mAuthor:Ljava/lang/String;

    .line 17
    const/4 v0, 0x1

    iput v0, p0, Lcom/vlingo/midas/news/NewsItem;->mNewsCP:I

    .line 60
    iget-object v0, p1, Lcom/vlingo/midas/news/YonhapNewsContainer;->NewsPublishTime:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/vlingo/midas/news/NewsItem;->mTimeCreated:J

    .line 61
    iget-object v0, p1, Lcom/vlingo/midas/news/YonhapNewsContainer;->NewsID:Ljava/lang/String;

    iput-object v0, p0, Lcom/vlingo/midas/news/NewsItem;->mUniqueId:Ljava/lang/String;

    .line 62
    iget-object v0, p1, Lcom/vlingo/midas/news/YonhapNewsContainer;->NewsContentText:Ljava/lang/String;

    iput-object v0, p0, Lcom/vlingo/midas/news/NewsItem;->mText:Ljava/lang/String;

    .line 63
    iget-object v0, p1, Lcom/vlingo/midas/news/YonhapNewsContainer;->NewsTitle:Ljava/lang/String;

    iput-object v0, p0, Lcom/vlingo/midas/news/NewsItem;->mTitle:Ljava/lang/String;

    .line 64
    iget-object v0, p1, Lcom/vlingo/midas/news/YonhapNewsContainer;->NewsLink:Ljava/lang/String;

    iput-object v0, p0, Lcom/vlingo/midas/news/NewsItem;->mLink:Ljava/lang/String;

    .line 65
    const/4 v0, 0x2

    iput v0, p0, Lcom/vlingo/midas/news/NewsItem;->mNewsCP:I

    .line 66
    iput-object v2, p0, Lcom/vlingo/midas/news/NewsItem;->mItem:Lflipboard/api/FlipboardItem;

    .line 67
    return-void
.end method

.method public constructor <init>(Lflipboard/api/FlipboardItem;)V
    .locals 7
    .param p1, "Item"    # Lflipboard/api/FlipboardItem;

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/vlingo/midas/news/NewsItem;->mTimeCreated:J

    .line 12
    iput-object v4, p0, Lcom/vlingo/midas/news/NewsItem;->mText:Ljava/lang/String;

    .line 13
    iput-object v4, p0, Lcom/vlingo/midas/news/NewsItem;->mTitle:Ljava/lang/String;

    .line 14
    iput-object v4, p0, Lcom/vlingo/midas/news/NewsItem;->mAuthor:Ljava/lang/String;

    .line 17
    iput v6, p0, Lcom/vlingo/midas/news/NewsItem;->mNewsCP:I

    .line 37
    invoke-virtual {p1}, Lflipboard/api/FlipboardItem;->getPageKey()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/news/NewsItem;->mUniqueId:Ljava/lang/String;

    .line 38
    invoke-virtual {p1}, Lflipboard/api/FlipboardItem;->getTimeCreated()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iput-wide v2, p0, Lcom/vlingo/midas/news/NewsItem;->mTimeCreated:J

    .line 39
    invoke-virtual {p1}, Lflipboard/api/FlipboardItem;->getText()Ljava/lang/String;

    move-result-object v1

    .line 41
    .local v1, "target":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 42
    const-string/jumbo v2, "\u00ad"

    const-string/jumbo v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 43
    const/4 v0, 0x0

    .line 45
    .local v0, "count":I
    :cond_0
    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xa

    if-ne v2, v3, :cond_1

    add-int/lit8 v0, v0, 0x1

    .line 47
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 49
    :cond_1
    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/news/NewsItem;->mText:Ljava/lang/String;

    .line 52
    .end local v0    # "count":I
    :cond_2
    invoke-virtual {p1}, Lflipboard/api/FlipboardItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/news/NewsItem;->mTitle:Ljava/lang/String;

    .line 53
    invoke-virtual {p1}, Lflipboard/api/FlipboardItem;->getAuthor()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/news/NewsItem;->mAuthor:Ljava/lang/String;

    .line 55
    iput v6, p0, Lcom/vlingo/midas/news/NewsItem;->mNewsCP:I

    .line 56
    iput-object p1, p0, Lcom/vlingo/midas/news/NewsItem;->mItem:Lflipboard/api/FlipboardItem;

    .line 57
    return-void
.end method


# virtual methods
.method public getAuthor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vlingo/midas/news/NewsItem;->mAuthor:Ljava/lang/String;

    return-object v0
.end method

.method public getNewsCP()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/vlingo/midas/news/NewsItem;->mNewsCP:I

    return v0
.end method

.method public getNewsIntent()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 69
    iget v3, p0, Lcom/vlingo/midas/news/NewsItem;->mNewsCP:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/vlingo/midas/news/NewsItem;->mItem:Lflipboard/api/FlipboardItem;

    if-eqz v3, :cond_0

    .line 70
    iget-object v3, p0, Lcom/vlingo/midas/news/NewsItem;->mItem:Lflipboard/api/FlipboardItem;

    invoke-virtual {v3}, Lflipboard/api/FlipboardItem;->getCategoryId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/vlingo/midas/news/NewsItem;->mItem:Lflipboard/api/FlipboardItem;

    invoke-virtual {v3}, Lflipboard/api/FlipboardItem;->getContentType()Lflipboard/api/FlipManager$ContentType;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/vlingo/midas/news/NewsItem;->mItem:Lflipboard/api/FlipboardItem;

    invoke-virtual {v3}, Lflipboard/api/FlipboardItem;->getId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 71
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 72
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/midas/news/NewsManager;->getInstance()Lcom/vlingo/midas/news/NewsManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/midas/news/NewsManager;->getRequestedLocale()Ljava/util/Locale;

    move-result-object v1

    .line 73
    .local v1, "requestLocale":Ljava/util/Locale;
    new-instance v2, Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "flipboard://showLibraryItem?category="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/midas/news/NewsItem;->mItem:Lflipboard/api/FlipboardItem;

    invoke-virtual {v4}, Lflipboard/api/FlipboardItem;->getCategoryId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "&locale="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "&contentType="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/midas/news/NewsItem;->mItem:Lflipboard/api/FlipboardItem;

    invoke-virtual {v4}, Lflipboard/api/FlipboardItem;->getContentType()Lflipboard/api/FlipManager$ContentType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "&itemId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/midas/news/NewsItem;->mItem:Lflipboard/api/FlipboardItem;

    invoke-virtual {v4}, Lflipboard/api/FlipboardItem;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 75
    .local v2, "url":Ljava/lang/String;
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 87
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "requestLocale":Ljava/util/Locale;
    .end local v2    # "url":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 80
    :cond_0
    iget-object v3, p0, Lcom/vlingo/midas/news/NewsItem;->mLink:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 81
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 82
    .restart local v0    # "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/vlingo/midas/news/NewsItem;->mLink:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0

    .line 87
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/midas/news/NewsItem;->mText:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeCreated()J
    .locals 2

    .prologue
    .line 33
    iget-wide v0, p0, Lcom/vlingo/midas/news/NewsItem;->mTimeCreated:J

    return-wide v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/midas/news/NewsItem;->mTitle:Ljava/lang/String;

    return-object v0
.end method
