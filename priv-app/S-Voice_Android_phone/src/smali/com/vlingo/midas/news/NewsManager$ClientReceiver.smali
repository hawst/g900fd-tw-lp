.class Lcom/vlingo/midas/news/NewsManager$ClientReceiver;
.super Landroid/content/BroadcastReceiver;
.source "NewsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/news/NewsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClientReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/news/NewsManager;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/news/NewsManager;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/vlingo/midas/news/NewsManager$ClientReceiver;->this$0:Lcom/vlingo/midas/news/NewsManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/news/NewsManager;Lcom/vlingo/midas/news/NewsManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/news/NewsManager;
    .param p2, "x1"    # Lcom/vlingo/midas/news/NewsManager$1;

    .prologue
    .line 145
    invoke-direct {p0, p1}, Lcom/vlingo/midas/news/NewsManager$ClientReceiver;-><init>(Lcom/vlingo/midas/news/NewsManager;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 147
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 148
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 149
    const-string/jumbo v1, "com.sec.android.daemonapp.ap.yonhapnews.intent.action.YONHAPNEWS_DATE_SYNC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 151
    iget-object v1, p0, Lcom/vlingo/midas/news/NewsManager$ClientReceiver;->this$0:Lcom/vlingo/midas/news/NewsManager;

    const/4 v2, 0x1

    # setter for: Lcom/vlingo/midas/news/NewsManager;->bUsingYonhapNews:Z
    invoke-static {v1, v2}, Lcom/vlingo/midas/news/NewsManager;->access$602(Lcom/vlingo/midas/news/NewsManager;Z)Z

    .line 152
    const-string/jumbo v1, "NewsManager"

    const-string/jumbo v2, "Receive Yonhap Data sync"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    iget-object v1, p0, Lcom/vlingo/midas/news/NewsManager$ClientReceiver;->this$0:Lcom/vlingo/midas/news/NewsManager;

    # getter for: Lcom/vlingo/midas/news/NewsManager;->mThread:Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;
    invoke-static {v1}, Lcom/vlingo/midas/news/NewsManager;->access$100(Lcom/vlingo/midas/news/NewsManager;)Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/news/NewsManager$ClientReceiver;->this$0:Lcom/vlingo/midas/news/NewsManager;

    # getter for: Lcom/vlingo/midas/news/NewsManager;->mThread:Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;
    invoke-static {v1}, Lcom/vlingo/midas/news/NewsManager;->access$100(Lcom/vlingo/midas/news/NewsManager;)Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lcom/vlingo/midas/news/NewsManager$ClientReceiver;->this$0:Lcom/vlingo/midas/news/NewsManager;

    # getter for: Lcom/vlingo/midas/news/NewsManager;->mThread:Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;
    invoke-static {v1}, Lcom/vlingo/midas/news/NewsManager;->access$100(Lcom/vlingo/midas/news/NewsManager;)Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;->signal()V

    .line 157
    :cond_0
    return-void
.end method
