.class Lcom/vlingo/midas/news/NewsManager$1;
.super Ljava/lang/Object;
.source "NewsManager.java"

# interfaces
.implements Lflipboard/api/FlipFetchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/news/NewsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/news/NewsManager;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/news/NewsManager;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/vlingo/midas/news/NewsManager$1;->this$0:Lcom/vlingo/midas/news/NewsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lflipboard/api/FlipManager$ErrorMessage;)V
    .locals 2
    .param p1, "errMsg"    # Lflipboard/api/FlipManager$ErrorMessage;

    .prologue
    .line 53
    const-string/jumbo v0, "NewsManager-flipNews"

    const-string/jumbo v1, " onFailure "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    sget-object v0, Lflipboard/api/FlipManager$ErrorMessage;->DUPLICATE_REQUEST:Lflipboard/api/FlipManager$ErrorMessage;

    if-ne p1, v0, :cond_0

    .line 56
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/news/NewsManager$1;->onSuccess(Z)V

    .line 62
    :goto_0
    iget-object v0, p0, Lcom/vlingo/midas/news/NewsManager$1;->this$0:Lcom/vlingo/midas/news/NewsManager;

    # getter for: Lcom/vlingo/midas/news/NewsManager;->mThread:Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;
    invoke-static {v0}, Lcom/vlingo/midas/news/NewsManager;->access$100(Lcom/vlingo/midas/news/NewsManager;)Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;->signal()V

    .line 63
    return-void

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/news/NewsManager$1;->this$0:Lcom/vlingo/midas/news/NewsManager;

    # setter for: Lcom/vlingo/midas/news/NewsManager;->mError:Lflipboard/api/FlipManager$ErrorMessage;
    invoke-static {v0, p1}, Lcom/vlingo/midas/news/NewsManager;->access$002(Lcom/vlingo/midas/news/NewsManager;Lflipboard/api/FlipManager$ErrorMessage;)Lflipboard/api/FlipManager$ErrorMessage;

    goto :goto_0
.end method

.method public onSuccess(Z)V
    .locals 3
    .param p1, "arg0"    # Z

    .prologue
    .line 67
    const-string/jumbo v0, "NewsManager-flipNews"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " onSuccess "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    iget-object v0, p0, Lcom/vlingo/midas/news/NewsManager$1;->this$0:Lcom/vlingo/midas/news/NewsManager;

    # getter for: Lcom/vlingo/midas/news/NewsManager;->mThread:Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;
    invoke-static {v0}, Lcom/vlingo/midas/news/NewsManager;->access$100(Lcom/vlingo/midas/news/NewsManager;)Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;->signal()V

    .line 75
    return-void
.end method
