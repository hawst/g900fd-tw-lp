.class public final Lcom/vlingo/midas/news/YonhapNewsDaemon$YonhapNewsRefContentColumns;
.super Ljava/lang/Object;
.source "YonhapNewsDaemon.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/news/YonhapNewsDaemon;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "YonhapNewsRefContentColumns"
.end annotation


# static fields
.field public static final CONTENTS_COLS:[Ljava/lang/String;

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "_id ASC"

.field public static final NEWS_CATEGORY:Ljava/lang/String; = "NEWS_CATEGORY"

.field public static final NEWS_ID:Ljava/lang/String; = "NEWS_ID"

.field public static final NEWS_LINK:Ljava/lang/String; = "NEWS_LINK"

.field public static final NEWS_MAIN_ID:Ljava/lang/String; = "NEWS_MAIN_ID"

.field public static final NEWS_TITLE:Ljava/lang/String; = "NEWS_TITLE"

.field public static final ROWID:Ljava/lang/String; = "_id"

.field public static final TABLE:Ljava/lang/String; = "TABLE_YONHAP_NEWS_REF_CONTENTS"

.field public static final TABLE_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 222
    const-string/jumbo v0, "content://com.sec.android.daemonapp.ap.yonhapnews.provider/TABLE_YONHAP_NEWS_REF_CONTENTS"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/news/YonhapNewsDaemon$YonhapNewsRefContentColumns;->TABLE_URI:Landroid/net/Uri;

    .line 239
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "NEWS_MAIN_ID"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "NEWS_LINK"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "NEWS_ID"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "NEWS_CATEGORY"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "NEWS_TITLE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/midas/news/YonhapNewsDaemon$YonhapNewsRefContentColumns;->CONTENTS_COLS:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
