.class final Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;
.super Ljava/lang/Thread;
.source "NewsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/news/NewsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "FetchNewsThread"
.end annotation


# instance fields
.field private final syncObj:Ljava/lang/Object;

.field final synthetic this$0:Lcom/vlingo/midas/news/NewsManager;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/news/NewsManager;)V
    .locals 1

    .prologue
    .line 81
    iput-object p1, p0, Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;->this$0:Lcom/vlingo/midas/news/NewsManager;

    .line 82
    const-string/jumbo v0, "FetchNewsThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 79
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;->syncObj:Ljava/lang/Object;

    .line 83
    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 93
    const/16 v1, 0x2710

    .line 94
    .local v1, "waitTime":I
    const/4 v2, 0x0

    .line 95
    .local v2, "wasInterrupted":Z
    const-string/jumbo v3, "NewsManagerThread"

    const-string/jumbo v4, "start waiting thread"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    iget-object v4, p0, Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;->syncObj:Ljava/lang/Object;

    monitor-enter v4

    .line 99
    :try_start_0
    iget-object v3, p0, Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;->syncObj:Ljava/lang/Object;

    const-wide/16 v5, 0x2710

    invoke-virtual {v3, v5, v6}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    :goto_0
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 109
    const-string/jumbo v3, "NewsManagerThread"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Not waiting; interrupted=="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    iget-object v3, p0, Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;->this$0:Lcom/vlingo/midas/news/NewsManager;

    # getter for: Lcom/vlingo/midas/news/NewsManager;->requestTypeFlags:I
    invoke-static {v3}, Lcom/vlingo/midas/news/NewsManager;->access$200(Lcom/vlingo/midas/news/NewsManager;)I

    move-result v3

    if-nez v3, :cond_0

    .line 114
    iget-object v3, p0, Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;->this$0:Lcom/vlingo/midas/news/NewsManager;

    # invokes: Lcom/vlingo/midas/news/NewsManager;->getFlipNewsData()V
    invoke-static {v3}, Lcom/vlingo/midas/news/NewsManager;->access$300(Lcom/vlingo/midas/news/NewsManager;)V

    .line 129
    :goto_1
    new-instance v3, Lcom/vlingo/midas/news/NewsManager$FetchNewsThread$1;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/news/NewsManager$FetchNewsThread$1;-><init>(Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;)V

    invoke-static {v3}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 141
    const-string/jumbo v3, "NewsManagerThread"

    const-string/jumbo v4, "maybe interrupted thread ----- "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    return-void

    .line 100
    :catch_0
    move-exception v0

    .line 103
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 104
    const/4 v2, 0x1

    goto :goto_0

    .line 107
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 127
    :cond_0
    iget-object v3, p0, Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;->this$0:Lcom/vlingo/midas/news/NewsManager;

    # invokes: Lcom/vlingo/midas/news/NewsManager;->getYonhapNewsData()V
    invoke-static {v3}, Lcom/vlingo/midas/news/NewsManager;->access$400(Lcom/vlingo/midas/news/NewsManager;)V

    goto :goto_1
.end method

.method public signal()V
    .locals 2

    .prologue
    .line 86
    iget-object v1, p0, Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;->syncObj:Ljava/lang/Object;

    monitor-enter v1

    .line 87
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/news/NewsManager$FetchNewsThread;->syncObj:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 88
    monitor-exit v1

    .line 89
    return-void

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
