.class Lcom/vlingo/midas/tos/TermsOfServiceManager$AcceptListener;
.super Ljava/lang/Object;
.source "TermsOfServiceManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/tos/TermsOfServiceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AcceptListener"
.end annotation


# instance fields
.field private final acceptListener:Landroid/content/DialogInterface$OnClickListener;

.field private final cancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field private final ctx:Landroid/content/Context;

.field private final declineListener:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 0
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "acceptListener"    # Landroid/content/DialogInterface$OnClickListener;
    .param p3, "declineListener"    # Landroid/content/DialogInterface$OnClickListener;
    .param p4, "cancelListener"    # Landroid/content/DialogInterface$OnCancelListener;

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput-object p2, p0, Lcom/vlingo/midas/tos/TermsOfServiceManager$AcceptListener;->acceptListener:Landroid/content/DialogInterface$OnClickListener;

    .line 130
    iput-object p3, p0, Lcom/vlingo/midas/tos/TermsOfServiceManager$AcceptListener;->declineListener:Landroid/content/DialogInterface$OnClickListener;

    .line 131
    iput-object p4, p0, Lcom/vlingo/midas/tos/TermsOfServiceManager$AcceptListener;->cancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 132
    iput-object p1, p0, Lcom/vlingo/midas/tos/TermsOfServiceManager$AcceptListener;->ctx:Landroid/content/Context;

    .line 133
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v1, 0x1

    .line 137
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSamsungDisclaimerAccepted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 142
    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->setSamsungDisclaimerAccepted(Z)V

    .line 143
    iget-object v0, p0, Lcom/vlingo/midas/tos/TermsOfServiceManager$AcceptListener;->ctx:Landroid/content/Context;

    iget-object v1, p0, Lcom/vlingo/midas/tos/TermsOfServiceManager$AcceptListener;->acceptListener:Landroid/content/DialogInterface$OnClickListener;

    iget-object v2, p0, Lcom/vlingo/midas/tos/TermsOfServiceManager$AcceptListener;->declineListener:Landroid/content/DialogInterface$OnClickListener;

    iget-object v3, p0, Lcom/vlingo/midas/tos/TermsOfServiceManager$AcceptListener;->cancelListener:Landroid/content/DialogInterface$OnCancelListener;

    invoke-static {v0, v1, v2, v3}, Lcom/vlingo/midas/tos/TermsOfServiceManager;->getTOSDialog(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 165
    :goto_0
    return-void

    .line 149
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 157
    :cond_1
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->getLmttClientEventHandler()Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;->onTosAccepted()V

    .line 160
    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->setTOSAccepted(Z)V

    .line 163
    iget-object v0, p0, Lcom/vlingo/midas/tos/TermsOfServiceManager$AcceptListener;->acceptListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-interface {v0, p1, p2}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    goto :goto_0
.end method
