.class public final enum Lcom/vlingo/midas/tos/TermsOfServiceView$Text;
.super Ljava/lang/Enum;
.source "TermsOfServiceView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/tos/TermsOfServiceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Text"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/tos/TermsOfServiceView$Text;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/tos/TermsOfServiceView$Text;

.field public static final enum TextSamsung:Lcom/vlingo/midas/tos/TermsOfServiceView$Text;

.field public static final enum TextTerms:Lcom/vlingo/midas/tos/TermsOfServiceView$Text;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27
    new-instance v0, Lcom/vlingo/midas/tos/TermsOfServiceView$Text;

    const-string/jumbo v1, "TextTerms"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/tos/TermsOfServiceView$Text;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/tos/TermsOfServiceView$Text;->TextTerms:Lcom/vlingo/midas/tos/TermsOfServiceView$Text;

    .line 28
    new-instance v0, Lcom/vlingo/midas/tos/TermsOfServiceView$Text;

    const-string/jumbo v1, "TextSamsung"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/midas/tos/TermsOfServiceView$Text;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/tos/TermsOfServiceView$Text;->TextSamsung:Lcom/vlingo/midas/tos/TermsOfServiceView$Text;

    .line 26
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/midas/tos/TermsOfServiceView$Text;

    sget-object v1, Lcom/vlingo/midas/tos/TermsOfServiceView$Text;->TextTerms:Lcom/vlingo/midas/tos/TermsOfServiceView$Text;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/midas/tos/TermsOfServiceView$Text;->TextSamsung:Lcom/vlingo/midas/tos/TermsOfServiceView$Text;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/tos/TermsOfServiceView$Text;->$VALUES:[Lcom/vlingo/midas/tos/TermsOfServiceView$Text;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/tos/TermsOfServiceView$Text;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lcom/vlingo/midas/tos/TermsOfServiceView$Text;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/tos/TermsOfServiceView$Text;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/tos/TermsOfServiceView$Text;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/vlingo/midas/tos/TermsOfServiceView$Text;->$VALUES:[Lcom/vlingo/midas/tos/TermsOfServiceView$Text;

    invoke-virtual {v0}, [Lcom/vlingo/midas/tos/TermsOfServiceView$Text;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/tos/TermsOfServiceView$Text;

    return-object v0
.end method
