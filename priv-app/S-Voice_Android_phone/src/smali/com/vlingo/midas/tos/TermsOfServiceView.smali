.class Lcom/vlingo/midas/tos/TermsOfServiceView;
.super Landroid/widget/LinearLayout;
.source "TermsOfServiceView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/tos/TermsOfServiceView$TabletType;,
        Lcom/vlingo/midas/tos/TermsOfServiceView$Text;
    }
.end annotation


# instance fields
.field m_tosLink:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/vlingo/midas/tos/TermsOfServiceView$Text;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "text"    # Lcom/vlingo/midas/tos/TermsOfServiceView$Text;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 37
    invoke-virtual {p0}, Lcom/vlingo/midas/tos/TermsOfServiceView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$layout;->tos_dialog:I

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 38
    invoke-direct {p0, p2}, Lcom/vlingo/midas/tos/TermsOfServiceView;->init(Lcom/vlingo/midas/tos/TermsOfServiceView$Text;)V

    .line 40
    return-void
.end method

.method private static getBRLocale()Ljava/lang/String;
    .locals 10

    .prologue
    .line 82
    const-string/jumbo v6, ""

    .line 83
    .local v6, "popup_Langueage":Ljava/lang/String;
    const-string/jumbo v5, ""

    .line 84
    .local v5, "popup_Country":Ljava/lang/String;
    const-string/jumbo v4, ""

    .line 85
    .local v4, "mLocale":Ljava/lang/String;
    const/4 v3, 0x0

    .line 86
    .local v3, "mLanguage":Ljava/lang/Process;
    const/4 v2, 0x0

    .line 89
    .local v2, "mCountry":Ljava/lang/Process;
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v7

    const-string/jumbo v8, "getprop persist.sys.language"

    invoke-virtual {v7, v8}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v3

    .line 91
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v7

    const-string/jumbo v8, "getprop persist.sys.country"

    invoke-virtual {v7, v8}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v2

    .line 92
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/InputStreamReader;

    invoke-virtual {v3}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    const-string/jumbo v9, "UTF-8"

    invoke-direct {v7, v8, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v1, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 94
    .local v1, "bLangueage":Ljava/io/BufferedReader;
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/InputStreamReader;

    invoke-virtual {v2}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    const-string/jumbo v9, "UTF-8"

    invoke-direct {v7, v8, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v0, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 96
    .local v0, "bCountry":Ljava/io/BufferedReader;
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .line 97
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 99
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 101
    invoke-virtual {v3}, Ljava/lang/Process;->destroy()V

    .line 102
    invoke-virtual {v2}, Ljava/lang/Process;->destroy()V

    .line 103
    const/4 v3, 0x0

    .line 104
    const/4 v2, 0x0

    .line 105
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 106
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    .end local v0    # "bCountry":Ljava/io/BufferedReader;
    .end local v1    # "bLangueage":Ljava/io/BufferedReader;
    :goto_0
    return-object v4

    .line 108
    :catch_0
    move-exception v7

    goto :goto_0
.end method

.method private init(Lcom/vlingo/midas/tos/TermsOfServiceView$Text;)V
    .locals 4
    .param p1, "text"    # Lcom/vlingo/midas/tos/TermsOfServiceView$Text;

    .prologue
    .line 49
    sget-object v1, Lcom/vlingo/midas/tos/TermsOfServiceView$Text;->TextSamsung:Lcom/vlingo/midas/tos/TermsOfServiceView$Text;

    if-ne p1, v1, :cond_4

    sget v0, Lcom/vlingo/midas/R$string;->tos_dialog_view_terms_samsung:I

    .line 51
    .local v0, "resID":I
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/tos/TermsOfServiceView;->getBRLocale()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "pt_BR"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 52
    sget-object v1, Lcom/vlingo/midas/tos/TermsOfServiceView$Text;->TextSamsung:Lcom/vlingo/midas/tos/TermsOfServiceView$Text;

    if-ne p1, v1, :cond_5

    sget v0, Lcom/vlingo/midas/R$string;->tos_dialog_view_terms_samsung_for_pt_br:I

    .line 55
    :cond_0
    :goto_1
    sget v1, Lcom/vlingo/midas/R$id;->text_tos_link:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/tos/TermsOfServiceView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vlingo/midas/tos/TermsOfServiceView;->m_tosLink:Landroid/widget/TextView;

    .line 56
    iget-object v1, p0, Lcom/vlingo/midas/tos/TermsOfServiceView;->m_tosLink:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/tos/TermsOfServiceView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-eq v1, v2, :cond_1

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v1, v2, :cond_6

    .line 63
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/tos/TermsOfServiceView;->m_tosLink:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/tos/TermsOfServiceView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$color;->edit_text_color_santos_theme:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 68
    :goto_2
    iget-object v1, p0, Lcom/vlingo/midas/tos/TermsOfServiceView;->m_tosLink:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 69
    iget-object v1, p0, Lcom/vlingo/midas/tos/TermsOfServiceView;->m_tosLink:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 70
    iget-object v1, p0, Lcom/vlingo/midas/tos/TermsOfServiceView;->m_tosLink:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/vlingo/midas/tos/TermsOfServiceView;->m_tosLink:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    sget v1, Lcom/vlingo/midas/R$string;->tos_dialog_view_terms_old:I

    if-eq v0, v1, :cond_2

    sget v1, Lcom/vlingo/midas/R$string;->tos_dialog_view_terms_old_for_pt_br:I

    if-ne v0, v1, :cond_3

    .line 72
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/tos/TermsOfServiceView;->m_tosLink:Landroid/widget/TextView;

    new-instance v2, Lcom/vlingo/midas/tos/TermsOfServiceView$1;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/tos/TermsOfServiceView$1;-><init>(Lcom/vlingo/midas/tos/TermsOfServiceView;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    :cond_3
    return-void

    .line 49
    .end local v0    # "resID":I
    :cond_4
    sget v0, Lcom/vlingo/midas/R$string;->tos_dialog_view_terms_old:I

    goto :goto_0

    .line 52
    .restart local v0    # "resID":I
    :cond_5
    sget v0, Lcom/vlingo/midas/R$string;->tos_dialog_view_terms_old_for_pt_br:I

    goto :goto_1

    .line 66
    :cond_6
    iget-object v1, p0, Lcom/vlingo/midas/tos/TermsOfServiceView;->m_tosLink:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/tos/TermsOfServiceView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$color;->edit_text_color_dark_theme:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2
.end method
