.class public Lcom/vlingo/midas/tos/TermsOfServiceManager;
.super Ljava/lang/Object;
.source "TermsOfServiceManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/tos/TermsOfServiceManager$DeclineListener;,
        Lcom/vlingo/midas/tos/TermsOfServiceManager$AcceptListener;
    }
.end annotation


# static fields
.field private static tempDialog:Landroid/app/AlertDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/tos/TermsOfServiceManager;->tempDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168
    return-void
.end method

.method public static dismissTempDlg()V
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/vlingo/midas/tos/TermsOfServiceManager;->tempDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 117
    sget-object v0, Lcom/vlingo/midas/tos/TermsOfServiceManager;->tempDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 118
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/tos/TermsOfServiceManager;->tempDialog:Landroid/app/AlertDialog;

    .line 120
    :cond_0
    return-void
.end method

.method private static getBRLocale()Ljava/lang/String;
    .locals 10

    .prologue
    .line 201
    const-string/jumbo v6, ""

    .line 202
    .local v6, "popup_Langueage":Ljava/lang/String;
    const-string/jumbo v5, ""

    .line 203
    .local v5, "popup_Country":Ljava/lang/String;
    const-string/jumbo v4, ""

    .line 204
    .local v4, "mLocale":Ljava/lang/String;
    const/4 v3, 0x0

    .line 205
    .local v3, "mLanguage":Ljava/lang/Process;
    const/4 v2, 0x0

    .line 208
    .local v2, "mCountry":Ljava/lang/Process;
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v7

    const-string/jumbo v8, "getprop persist.sys.language"

    invoke-virtual {v7, v8}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v3

    .line 210
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v7

    const-string/jumbo v8, "getprop persist.sys.country"

    invoke-virtual {v7, v8}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v2

    .line 211
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/InputStreamReader;

    invoke-virtual {v3}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    const-string/jumbo v9, "UTF-8"

    invoke-direct {v7, v8, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v1, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 213
    .local v1, "bLangueage":Ljava/io/BufferedReader;
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/InputStreamReader;

    invoke-virtual {v2}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    const-string/jumbo v9, "UTF-8"

    invoke-direct {v7, v8, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v0, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 215
    .local v0, "bCountry":Ljava/io/BufferedReader;
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .line 216
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 218
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 220
    invoke-virtual {v3}, Ljava/lang/Process;->destroy()V

    .line 221
    invoke-virtual {v2}, Ljava/lang/Process;->destroy()V

    .line 222
    const/4 v3, 0x0

    .line 223
    const/4 v2, 0x0

    .line 224
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 225
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    .end local v0    # "bCountry":Ljava/io/BufferedReader;
    .end local v1    # "bLangueage":Ljava/io/BufferedReader;
    :goto_0
    return-object v4

    .line 227
    :catch_0
    move-exception v7

    goto :goto_0
.end method

.method public static getTOSDialog(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog;
    .locals 4
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "onAccept"    # Landroid/content/DialogInterface$OnClickListener;
    .param p2, "onDecline"    # Landroid/content/DialogInterface$OnClickListener;
    .param p3, "onCancel"    # Landroid/content/DialogInterface$OnCancelListener;

    .prologue
    const/4 v2, 0x0

    .line 39
    invoke-static {}, Lcom/vlingo/midas/tos/TermsOfServiceManager;->dismissTempDlg()V

    .line 41
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 43
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSamsungDisclaimerAccepted()Z

    move-result v1

    if-nez v1, :cond_2

    .line 47
    new-instance v0, Lcom/vlingo/midas/tos/TermsOfServiceManager$DeclineListener;

    invoke-direct {v0, p0, p2, p3}, Lcom/vlingo/midas/tos/TermsOfServiceManager$DeclineListener;-><init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 49
    .local v0, "declinetListner":Lcom/vlingo/midas/tos/TermsOfServiceManager$DeclineListener;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-static {}, Lcom/vlingo/midas/tos/TermsOfServiceManager;->getBRLocale()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "pt_BR"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/vlingo/midas/R$string;->tos_vlingo_terms_samsung_for_pt_br:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/vlingo/midas/tos/TermsOfServiceView;

    sget-object v3, Lcom/vlingo/midas/tos/TermsOfServiceView$Text;->TextSamsung:Lcom/vlingo/midas/tos/TermsOfServiceView$Text;

    invoke-direct {v2, p0, v3}, Lcom/vlingo/midas/tos/TermsOfServiceView;-><init>(Landroid/content/Context;Lcom/vlingo/midas/tos/TermsOfServiceView$Text;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-static {}, Lcom/vlingo/midas/tos/TermsOfServiceManager;->getBRLocale()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "pt_BR"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/vlingo/midas/R$string;->tos_accept_samsung_for_pt_br:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    new-instance v3, Lcom/vlingo/midas/tos/TermsOfServiceManager$AcceptListener;

    invoke-direct {v3, p0, p1, p2, p3}, Lcom/vlingo/midas/tos/TermsOfServiceManager$AcceptListener;-><init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-virtual {v2, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/vlingo/midas/tos/TermsOfServiceManager$1;

    invoke-direct {v2}, Lcom/vlingo/midas/tos/TermsOfServiceManager$1;-><init>()V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    sput-object v1, Lcom/vlingo/midas/tos/TermsOfServiceManager;->tempDialog:Landroid/app/AlertDialog;

    .line 74
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v1

    const-string/jumbo v2, "tos-accept"

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->helpPageViewed(Ljava/lang/String;)V

    .line 76
    sget-object v1, Lcom/vlingo/midas/tos/TermsOfServiceManager;->tempDialog:Landroid/app/AlertDialog;

    .line 106
    :goto_2
    return-object v1

    .line 49
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/vlingo/midas/R$string;->tos_vlingo_terms_samsung:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/vlingo/midas/R$string;->tos_accept_samsung:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 82
    .end local v0    # "declinetListner":Lcom/vlingo/midas/tos/TermsOfServiceManager$DeclineListener;
    :cond_2
    new-instance v0, Lcom/vlingo/midas/tos/TermsOfServiceManager$DeclineListener;

    invoke-direct {v0, p0, p2, p3}, Lcom/vlingo/midas/tos/TermsOfServiceManager$DeclineListener;-><init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 83
    .restart local v0    # "declinetListner":Lcom/vlingo/midas/tos/TermsOfServiceManager$DeclineListener;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-static {}, Lcom/vlingo/midas/tos/TermsOfServiceManager;->getBRLocale()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "pt_BR"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/vlingo/midas/R$string;->tos_vlingo_terms_for_pt_br:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/vlingo/midas/tos/TermsOfServiceView;

    sget-object v3, Lcom/vlingo/midas/tos/TermsOfServiceView$Text;->TextTerms:Lcom/vlingo/midas/tos/TermsOfServiceView$Text;

    invoke-direct {v2, p0, v3}, Lcom/vlingo/midas/tos/TermsOfServiceView;-><init>(Landroid/content/Context;Lcom/vlingo/midas/tos/TermsOfServiceView$Text;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-static {}, Lcom/vlingo/midas/tos/TermsOfServiceManager;->getBRLocale()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "pt_BR"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/vlingo/midas/R$string;->tos_accept_for_pt_br:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_4
    new-instance v3, Lcom/vlingo/midas/tos/TermsOfServiceManager$AcceptListener;

    invoke-direct {v3, p0, p1, p2, p3}, Lcom/vlingo/midas/tos/TermsOfServiceManager$AcceptListener;-><init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-virtual {v2, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-static {}, Lcom/vlingo/midas/tos/TermsOfServiceManager;->getBRLocale()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "pt_BR"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/vlingo/midas/R$string;->tos_decline_for_pt_br:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_5
    invoke-virtual {v2, v1, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/vlingo/midas/tos/TermsOfServiceManager$2;

    invoke-direct {v2}, Lcom/vlingo/midas/tos/TermsOfServiceManager$2;-><init>()V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    sput-object v1, Lcom/vlingo/midas/tos/TermsOfServiceManager;->tempDialog:Landroid/app/AlertDialog;

    .line 105
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v1

    const-string/jumbo v2, "tos-accept"

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->helpPageViewed(Ljava/lang/String;)V

    .line 106
    sget-object v1, Lcom/vlingo/midas/tos/TermsOfServiceManager;->tempDialog:Landroid/app/AlertDialog;

    goto/16 :goto_2

    .line 83
    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/vlingo/midas/R$string;->tos_vlingo_terms:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_4
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/vlingo/midas/R$string;->tos_accept:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    :cond_5
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/vlingo/midas/R$string;->tos_decline:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_5
.end method

.method public static isTOSRequired()Z
    .locals 1

    .prologue
    .line 111
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSamsungDisclaimerAccepted()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
