.class Lcom/vlingo/midas/tos/TermsOfServiceManager$DeclineListener;
.super Ljava/lang/Object;
.source "TermsOfServiceManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/tos/TermsOfServiceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DeclineListener"
.end annotation


# instance fields
.field ctx:Landroid/content/Context;

.field onCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field onDeclineListener:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 0
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "onDecline"    # Landroid/content/DialogInterface$OnClickListener;
    .param p3, "onCancel"    # Landroid/content/DialogInterface$OnCancelListener;

    .prologue
    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    iput-object p2, p0, Lcom/vlingo/midas/tos/TermsOfServiceManager$DeclineListener;->onDeclineListener:Landroid/content/DialogInterface$OnClickListener;

    .line 175
    iput-object p3, p0, Lcom/vlingo/midas/tos/TermsOfServiceManager$DeclineListener;->onCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 176
    iput-object p1, p0, Lcom/vlingo/midas/tos/TermsOfServiceManager$DeclineListener;->ctx:Landroid/content/Context;

    .line 177
    return-void
.end method


# virtual methods
.method decline()V
    .locals 3

    .prologue
    .line 196
    iget-object v0, p0, Lcom/vlingo/midas/tos/TermsOfServiceManager$DeclineListener;->ctx:Landroid/content/Context;

    iget-object v1, p0, Lcom/vlingo/midas/tos/TermsOfServiceManager$DeclineListener;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->tos_must_accept_to_use:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 197
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 189
    iget-object v0, p0, Lcom/vlingo/midas/tos/TermsOfServiceManager$DeclineListener;->onCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnCancelListener;->onCancel(Landroid/content/DialogInterface;)V

    .line 190
    invoke-virtual {p0}, Lcom/vlingo/midas/tos/TermsOfServiceManager$DeclineListener;->decline()V

    .line 191
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 182
    iget-object v0, p0, Lcom/vlingo/midas/tos/TermsOfServiceManager$DeclineListener;->onDeclineListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-interface {v0, p1, p2}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    .line 183
    invoke-virtual {p0}, Lcom/vlingo/midas/tos/TermsOfServiceManager$DeclineListener;->decline()V

    .line 184
    return-void
.end method
