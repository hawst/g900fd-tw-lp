.class Lcom/vlingo/midas/help/AboutScreenFragment$3;
.super Ljava/lang/Object;
.source "AboutScreenFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/help/AboutScreenFragment;->setAboutText()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/help/AboutScreenFragment;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/help/AboutScreenFragment;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/vlingo/midas/help/AboutScreenFragment$3;->this$0:Lcom/vlingo/midas/help/AboutScreenFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 157
    iget-object v3, p0, Lcom/vlingo/midas/help/AboutScreenFragment$3;->this$0:Lcom/vlingo/midas/help/AboutScreenFragment;

    invoke-virtual {v3}, Lcom/vlingo/midas/help/AboutScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string/jumbo v4, "clipboard"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ClipboardManager;

    .line 158
    .local v1, "clipboard":Landroid/content/ClipboardManager;
    const-string/jumbo v3, "uuid"

    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getUUIDDeviceID()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v0

    .line 159
    .local v0, "clip":Landroid/content/ClipData;
    invoke-virtual {v1, v0}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 162
    if-eqz v0, :cond_0

    .line 163
    iget-object v3, p0, Lcom/vlingo/midas/help/AboutScreenFragment$3;->this$0:Lcom/vlingo/midas/help/AboutScreenFragment;

    invoke-virtual {v3}, Lcom/vlingo/midas/help/AboutScreenFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->text_copied_to_clipboard:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 164
    .local v2, "noti":Ljava/lang/String;
    iget-object v3, p0, Lcom/vlingo/midas/help/AboutScreenFragment$3;->this$0:Lcom/vlingo/midas/help/AboutScreenFragment;

    invoke-virtual {v3}, Lcom/vlingo/midas/help/AboutScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 167
    .end local v2    # "noti":Ljava/lang/String;
    :cond_0
    return-void
.end method
