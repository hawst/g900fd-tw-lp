.class public Lcom/vlingo/midas/help/WhatCanISayScreen;
.super Lcom/vlingo/midas/ui/VLActivity;
.source "WhatCanISayScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;
    }
.end annotation


# static fields
.field public static final EXTRA_SHOW_DONE:Ljava/lang/String; = "wycs.show.done"

.field private static final log:Lcom/vlingo/core/internal/logging/Logger;


# instance fields
.field mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

.field private wcisData:Lcom/vlingo/midas/help/WCISData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/vlingo/midas/help/WhatCanISayScreen;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/help/WhatCanISayScreen;->log:Lcom/vlingo/core/internal/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/vlingo/midas/ui/VLActivity;-><init>()V

    .line 42
    new-instance v0, Lcom/vlingo/midas/help/WCISData;

    invoke-direct {v0}, Lcom/vlingo/midas/help/WCISData;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/help/WhatCanISayScreen;->wcisData:Lcom/vlingo/midas/help/WCISData;

    .line 175
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/help/WhatCanISayScreen;I)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/help/WhatCanISayScreen;
    .param p1, "x1"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/vlingo/midas/help/WhatCanISayScreen;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private checkIcon(I)V
    .locals 1
    .param p1, "ResId"    # I

    .prologue
    .line 281
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_memo_pen:I

    if-ne p1, v0, :cond_1

    .line 282
    iget-object v0, p0, Lcom/vlingo/midas/help/WhatCanISayScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setMemoIcon()V

    .line 295
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_message:I

    if-ne p1, v0, :cond_2

    .line 284
    iget-object v0, p0, Lcom/vlingo/midas/help/WhatCanISayScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setMessageIcon()V

    goto :goto_0

    .line 285
    :cond_2
    sget v0, Lcom/vlingo/midas/R$drawable;->help_navi:I

    if-ne p1, v0, :cond_3

    .line 286
    iget-object v0, p0, Lcom/vlingo/midas/help/WhatCanISayScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setNavigationIcon()V

    goto :goto_0

    .line 287
    :cond_3
    sget v0, Lcom/vlingo/midas/R$drawable;->help_weather:I

    if-ne p1, v0, :cond_4

    .line 288
    iget-object v0, p0, Lcom/vlingo/midas/help/WhatCanISayScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setWeatherIcon()V

    goto :goto_0

    .line 289
    :cond_4
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_planner:I

    if-ne p1, v0, :cond_5

    .line 290
    iget-object v0, p0, Lcom/vlingo/midas/help/WhatCanISayScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setPlannerIcon()V

    goto :goto_0

    .line 291
    :cond_5
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_voice_recorder:I

    if-ne p1, v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/vlingo/midas/help/WhatCanISayScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setVoiceRecorderIcon()V

    goto :goto_0
.end method

.method private getDrawable(I)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1, "ResId"    # I

    .prologue
    .line 298
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_memo_pen:I

    if-ne p1, v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/vlingo/midas/help/WhatCanISayScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getMemoIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 311
    :goto_0
    return-object v0

    .line 300
    :cond_0
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_message:I

    if-ne p1, v0, :cond_1

    .line 301
    iget-object v0, p0, Lcom/vlingo/midas/help/WhatCanISayScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getMessageIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 302
    :cond_1
    sget v0, Lcom/vlingo/midas/R$drawable;->help_navi:I

    if-ne p1, v0, :cond_2

    .line 303
    iget-object v0, p0, Lcom/vlingo/midas/help/WhatCanISayScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getNavigationIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 304
    :cond_2
    sget v0, Lcom/vlingo/midas/R$drawable;->help_weather:I

    if-ne p1, v0, :cond_3

    .line 305
    iget-object v0, p0, Lcom/vlingo/midas/help/WhatCanISayScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getWeatherIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 306
    :cond_3
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_planner:I

    if-ne p1, v0, :cond_4

    .line 307
    iget-object v0, p0, Lcom/vlingo/midas/help/WhatCanISayScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getPlannerIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 308
    :cond_4
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_voice_recorder:I

    if-ne p1, v0, :cond_5

    .line 309
    iget-object v0, p0, Lcom/vlingo/midas/help/WhatCanISayScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getVoiceRecorderIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 311
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getWcisData()Lcom/vlingo/midas/help/WCISData;
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/vlingo/midas/help/WhatCanISayScreen;->wcisData:Lcom/vlingo/midas/help/WCISData;

    return-object v0
.end method

.method protected init()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 88
    sget v6, Lcom/vlingo/midas/R$layout;->wcis_lyt:I

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/help/WhatCanISayScreen;->setContentView(I)V

    .line 90
    invoke-virtual {p0}, Lcom/vlingo/midas/help/WhatCanISayScreen;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    .line 91
    .local v5, "titlebar":Landroid/app/ActionBar;
    if-eqz v5, :cond_1

    .line 94
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 95
    if-eqz v5, :cond_0

    .line 96
    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 103
    :cond_0
    :goto_0
    sget v6, Lcom/vlingo/midas/R$string;->what_can_i_say:I

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/help/WhatCanISayScreen;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 109
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/help/WhatCanISayScreen;->getWcisData()Lcom/vlingo/midas/help/WCISData;

    move-result-object v6

    invoke-virtual {v6, p0}, Lcom/vlingo/midas/help/WCISData;->addItems(Landroid/content/Context;)V

    .line 111
    invoke-virtual {p0}, Lcom/vlingo/midas/help/WhatCanISayScreen;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string/jumbo v7, "wycs.show.done"

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 112
    .local v3, "inIUXMode":Z
    if-eqz v3, :cond_2

    .line 113
    sget v6, Lcom/vlingo/midas/R$id;->done_btn_container:I

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/help/WhatCanISayScreen;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 114
    .local v1, "btnContainer":Landroid/view/ViewGroup;
    invoke-virtual {v1, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 115
    sget v6, Lcom/vlingo/midas/R$id;->finish:I

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/help/WhatCanISayScreen;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 116
    .local v2, "doneBtn":Landroid/view/View;
    new-instance v6, Lcom/vlingo/midas/help/WhatCanISayScreen$1;

    invoke-direct {v6, p0}, Lcom/vlingo/midas/help/WhatCanISayScreen$1;-><init>(Lcom/vlingo/midas/help/WhatCanISayScreen;)V

    invoke-virtual {v2, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    .end local v1    # "btnContainer":Landroid/view/ViewGroup;
    .end local v2    # "doneBtn":Landroid/view/View;
    :cond_2
    sget v6, Lcom/vlingo/midas/R$id;->mainListView:I

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/help/WhatCanISayScreen;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ListView;

    .line 127
    .local v4, "lv":Landroid/widget/ListView;
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 128
    new-instance v0, Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;-><init>(Lcom/vlingo/midas/help/WhatCanISayScreen;)V

    .line 129
    .local v0, "adapter":Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;
    invoke-virtual {v4, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 130
    new-instance v6, Lcom/vlingo/midas/help/WhatCanISayScreen$2;

    invoke-direct {v6, p0}, Lcom/vlingo/midas/help/WhatCanISayScreen$2;-><init>(Lcom/vlingo/midas/help/WhatCanISayScreen;)V

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 164
    return-void

    .line 99
    .end local v0    # "adapter":Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;
    .end local v3    # "inIUXMode":Z
    .end local v4    # "lv":Landroid/widget/ListView;
    :cond_3
    if-eqz v5, :cond_0

    .line 100
    const/16 v6, 0xe

    invoke-virtual {v5, v6}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    goto :goto_0
.end method

.method protected isInIUXMode()Z
    .locals 1

    .prologue
    .line 323
    const/4 v0, 0x1

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 50
    new-instance v8, Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-direct {v8, p0}, Lcom/vlingo/midas/ui/PackageInfoProvider;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/vlingo/midas/help/WhatCanISayScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    .line 51
    sget v8, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_planner:I

    invoke-direct {p0, v8}, Lcom/vlingo/midas/help/WhatCanISayScreen;->checkIcon(I)V

    .line 52
    sget v8, Lcom/vlingo/midas/R$drawable;->help_weather:I

    invoke-direct {p0, v8}, Lcom/vlingo/midas/help/WhatCanISayScreen;->checkIcon(I)V

    .line 53
    sget v8, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_memo_pen:I

    invoke-direct {p0, v8}, Lcom/vlingo/midas/help/WhatCanISayScreen;->checkIcon(I)V

    .line 54
    sget v8, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_message:I

    invoke-direct {p0, v8}, Lcom/vlingo/midas/help/WhatCanISayScreen;->checkIcon(I)V

    .line 55
    sget v8, Lcom/vlingo/midas/R$drawable;->help_navi:I

    invoke-direct {p0, v8}, Lcom/vlingo/midas/help/WhatCanISayScreen;->checkIcon(I)V

    .line 56
    sget v8, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_voice_recorder:I

    invoke-direct {p0, v8}, Lcom/vlingo/midas/help/WhatCanISayScreen;->checkIcon(I)V

    .line 60
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/help/WhatCanISayScreen;->getWindow()Landroid/view/Window;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 61
    .local v3, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string/jumbo v9, "privateFlags"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    .line 62
    .local v4, "privateFlags":Ljava/lang/reflect/Field;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string/jumbo v9, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    .line 63
    .local v5, "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string/jumbo v9, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 65
    .local v1, "disableMultiTray":Ljava/lang/reflect/Field;
    const/4 v0, 0x0

    .local v0, "currentPrivateFlags":I
    const/4 v7, 0x0

    .local v7, "valueofFlagsEnableStatusBar":I
    const/4 v6, 0x0

    .line 66
    .local v6, "valueofFlagsDisableTray":I
    invoke-virtual {v4, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    .line 67
    invoke-virtual {v5, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v7

    .line 68
    invoke-virtual {v1, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v6

    .line 70
    or-int/2addr v0, v7

    .line 71
    or-int/2addr v0, v6

    .line 73
    invoke-virtual {v4, v3, v0}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 74
    invoke-virtual {p0}, Lcom/vlingo/midas/help/WhatCanISayScreen;->getWindow()Landroid/view/Window;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    .end local v0    # "currentPrivateFlags":I
    .end local v1    # "disableMultiTray":Ljava/lang/reflect/Field;
    .end local v3    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v4    # "privateFlags":Ljava/lang/reflect/Field;
    .end local v5    # "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    .end local v6    # "valueofFlagsDisableTray":I
    .end local v7    # "valueofFlagsEnableStatusBar":I
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/midas/help/WhatCanISayScreen;->init()V

    .line 85
    sget v8, Lcom/vlingo/midas/R$style;->MidasBlackThemeSettings:I

    invoke-virtual {p0, v8}, Lcom/vlingo/midas/help/WhatCanISayScreen;->setTheme(I)V

    .line 86
    return-void

    .line 78
    :catch_0
    move-exception v2

    .line 81
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 75
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v8

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 168
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 172
    :goto_0
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 170
    :pswitch_0
    invoke-virtual {p0}, Lcom/vlingo/midas/help/WhatCanISayScreen;->finish()V

    goto :goto_0

    .line 168
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public setWcisData(Lcom/vlingo/midas/help/WCISData;)V
    .locals 0
    .param p1, "wcisData"    # Lcom/vlingo/midas/help/WCISData;

    .prologue
    .line 319
    iput-object p1, p0, Lcom/vlingo/midas/help/WhatCanISayScreen;->wcisData:Lcom/vlingo/midas/help/WCISData;

    .line 320
    return-void
.end method
