.class Lcom/vlingo/midas/help/WCISCompositionScreen$3;
.super Ljava/lang/Object;
.source "WCISCompositionScreen.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/help/WCISCompositionScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/help/WCISCompositionScreen;)V
    .locals 0

    .prologue
    .line 426
    iput-object p1, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$3;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 447
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 454
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 430
    iget-object v0, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$3;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    # getter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->dialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$100(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$3;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    # getter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->dialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$100(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 432
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 433
    iget-object v0, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$3;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    # getter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->dialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$100(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 441
    :cond_0
    :goto_0
    return-void

    .line 435
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$3;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/vlingo/midas/help/WCISCompositionScreen;->containsNoAlphaNumeric(Ljava/lang/String;)Z
    invoke-static {v0, v1}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$300(Lcom/vlingo/midas/help/WCISCompositionScreen;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 436
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$3;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    # getter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->dialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$100(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 439
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$3;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    # getter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->dialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$100(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
