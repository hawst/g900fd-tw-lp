.class public Lcom/vlingo/midas/help/AboutScreen;
.super Lcom/vlingo/midas/ui/VLActivity;
.source "AboutScreen.java"


# static fields
.field public static final SAMSUNG_REVISION_NUMBER:Ljava/lang/String; = "r1"

.field private static mTheme:I


# instance fields
.field private fragment:Lcom/vlingo/midas/help/AboutScreenFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/vlingo/midas/R$style;->CustomBlackNoActionBar:I

    sput v0, Lcom/vlingo/midas/help/AboutScreen;->mTheme:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/vlingo/midas/ui/VLActivity;-><init>()V

    return-void
.end method

.method private showPreferences()V
    .locals 3

    .prologue
    .line 108
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vlingo/midas/help/AboutScreen;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 109
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "is_start_option_menu"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 110
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/help/AboutScreen;->startActivity(Landroid/content/Intent;)V

    .line 111
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/vlingo/midas/help/AboutScreen;->getWindow()Landroid/view/Window;

    move-result-object v9

    const/16 v10, 0x400

    const/16 v11, 0x400

    invoke-virtual {v9, v10, v11}, Landroid/view/Window;->setFlags(II)V

    .line 40
    const/4 v9, 0x1

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/help/AboutScreen;->setRequestedOrientation(I)V

    .line 41
    sget v9, Lcom/vlingo/midas/R$layout;->fragment_container:I

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/help/AboutScreen;->setContentView(I)V

    .line 42
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 43
    .local v3, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/vlingo/midas/help/AboutScreen;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v9

    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    invoke-virtual {v9, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 45
    iget v9, v3, Landroid/util/DisplayMetrics;->density:F

    float-to-double v9, v9

    const-wide/high16 v11, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v9, v11

    const-wide/16 v11, 0x0

    cmpl-double v9, v9, v11

    if-nez v9, :cond_0

    iget v9, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v10, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    add-int/2addr v9, v10

    add-int/lit16 v9, v9, -0x7f0

    if-nez v9, :cond_0

    iget v9, v3, Landroid/util/DisplayMetrics;->xdpi:F

    const v10, 0x4315d32c

    sub-float/2addr v9, v10

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-nez v9, :cond_0

    iget v9, v3, Landroid/util/DisplayMetrics;->ydpi:F

    const v10, 0x431684be

    sub-float/2addr v9, v10

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-eqz v9, :cond_1

    :cond_0
    iget v9, v3, Landroid/util/DisplayMetrics;->density:F

    float-to-double v9, v9

    const-wide/high16 v11, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v9, v11

    const-wide/16 v11, 0x0

    cmpl-double v9, v9, v11

    if-nez v9, :cond_4

    iget v9, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v10, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    add-int/2addr v9, v10

    add-int/lit16 v9, v9, -0x820

    if-nez v9, :cond_4

    iget v9, v3, Landroid/util/DisplayMetrics;->xdpi:F

    const v10, 0x4315d2f2

    sub-float/2addr v9, v10

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-nez v9, :cond_4

    iget v9, v3, Landroid/util/DisplayMetrics;->ydpi:F

    const v10, 0x4316849c

    sub-float/2addr v9, v10

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-nez v9, :cond_4

    .line 51
    :cond_1
    sget v9, Lcom/vlingo/midas/R$style;->DialogSlideAnim:I

    sput v9, Lcom/vlingo/midas/help/AboutScreen;->mTheme:I

    .line 58
    :goto_0
    sget v9, Lcom/vlingo/midas/help/AboutScreen;->mTheme:I

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/help/AboutScreen;->setTheme(I)V

    .line 59
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-virtual {p0}, Lcom/vlingo/midas/help/AboutScreen;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    .line 62
    .local v6, "titlebar":Landroid/app/ActionBar;
    if-eqz v6, :cond_2

    .line 63
    const/16 v9, 0xc

    invoke-virtual {v6, v9}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 64
    sget v9, Lcom/vlingo/midas/R$string;->help_about_vlingo:I

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/help/AboutScreen;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 69
    :cond_2
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/help/AboutScreen;->getWindow()Landroid/view/Window;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 70
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string/jumbo v10, "privateFlags"

    invoke-virtual {v9, v10}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    .line 71
    .local v4, "privateFlags":Ljava/lang/reflect/Field;
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string/jumbo v10, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual {v9, v10}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    .line 72
    .local v5, "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string/jumbo v10, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual {v9, v10}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 74
    .local v1, "disableMultiTray":Ljava/lang/reflect/Field;
    const/4 v0, 0x0

    .local v0, "currentPrivateFlags":I
    const/4 v8, 0x0

    .local v8, "valueofFlagsEnableStatusBar":I
    const/4 v7, 0x0

    .line 75
    .local v7, "valueofFlagsDisableTray":I
    invoke-virtual {v4, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    .line 76
    invoke-virtual {v5, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v8

    .line 77
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v7

    .line 79
    or-int/2addr v0, v8

    .line 80
    or-int/2addr v0, v7

    .line 82
    invoke-virtual {v4, v2, v0}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 83
    invoke-virtual {p0}, Lcom/vlingo/midas/help/AboutScreen;->getWindow()Landroid/view/Window;

    move-result-object v9

    invoke-virtual {v9, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    .end local v0    # "currentPrivateFlags":I
    .end local v1    # "disableMultiTray":Ljava/lang/reflect/Field;
    .end local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v4    # "privateFlags":Ljava/lang/reflect/Field;
    .end local v5    # "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    .end local v7    # "valueofFlagsDisableTray":I
    .end local v8    # "valueofFlagsEnableStatusBar":I
    :goto_1
    new-instance v9, Lcom/vlingo/midas/help/AboutScreenFragment;

    invoke-direct {v9}, Lcom/vlingo/midas/help/AboutScreenFragment;-><init>()V

    iput-object v9, p0, Lcom/vlingo/midas/help/AboutScreen;->fragment:Lcom/vlingo/midas/help/AboutScreenFragment;

    .line 92
    iget-object v9, p0, Lcom/vlingo/midas/help/AboutScreen;->fragment:Lcom/vlingo/midas/help/AboutScreenFragment;

    if-eqz v9, :cond_3

    .line 93
    invoke-virtual {p0}, Lcom/vlingo/midas/help/AboutScreen;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v9

    sget v10, Lcom/vlingo/midas/R$id;->fragment_placeholder:I

    iget-object v11, p0, Lcom/vlingo/midas/help/AboutScreen;->fragment:Lcom/vlingo/midas/help/AboutScreenFragment;

    invoke-virtual {v9, v10, v11}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/FragmentTransaction;->commit()I

    .line 95
    :cond_3
    return-void

    .line 52
    .end local v6    # "titlebar":Landroid/app/ActionBar;
    :cond_4
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 53
    sget v9, Lcom/vlingo/midas/R$style;->MidasTheme:I

    sput v9, Lcom/vlingo/midas/help/AboutScreen;->mTheme:I

    goto/16 :goto_0

    .line 55
    :cond_5
    sget v9, Lcom/vlingo/midas/R$style;->CustomBlackNoActionBar:I

    sput v9, Lcom/vlingo/midas/help/AboutScreen;->mTheme:I

    goto/16 :goto_0

    .line 87
    .restart local v6    # "titlebar":Landroid/app/ActionBar;
    :catch_0
    move-exception v9

    goto :goto_1

    .line 85
    :catch_1
    move-exception v9

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 99
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 103
    :goto_0
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 101
    :pswitch_0
    invoke-virtual {p0}, Lcom/vlingo/midas/help/AboutScreen;->finish()V

    goto :goto_0

    .line 99
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
