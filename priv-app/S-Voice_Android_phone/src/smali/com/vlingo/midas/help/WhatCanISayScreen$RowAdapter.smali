.class public Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;
.super Landroid/widget/BaseAdapter;
.source "WhatCanISayScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/help/WhatCanISayScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RowAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/help/WhatCanISayScreen;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    invoke-virtual {v0}, Lcom/vlingo/midas/help/WhatCanISayScreen;->getWcisData()Lcom/vlingo/midas/help/WCISData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/help/WCISData;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 182
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 185
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v10, 0x0

    .line 218
    iget-object v8, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    invoke-virtual {v8}, Lcom/vlingo/midas/help/WhatCanISayScreen;->getWcisData()Lcom/vlingo/midas/help/WCISData;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/midas/help/WCISData;->getItems()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge p1, v8, :cond_5

    .line 219
    iget-object v8, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    invoke-virtual {v8}, Lcom/vlingo/midas/help/WhatCanISayScreen;->getWcisData()Lcom/vlingo/midas/help/WCISData;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/midas/help/WCISData;->getItems()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/HashMap;

    .line 220
    .local v4, "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v8, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    invoke-virtual {v8}, Lcom/vlingo/midas/help/WhatCanISayScreen;->getWcisData()Lcom/vlingo/midas/help/WCISData;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/midas/help/WCISData;->getInfoItemCount()I

    move-result v3

    .line 222
    .local v3, "infoItemCount":I
    const-string/jumbo v8, "EXTRA_INFO"

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 224
    iget-object v8, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    sget v9, Lcom/vlingo/midas/R$layout;->wcis_info_with_caption_row:I

    invoke-static {v8, v9, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 225
    .local v5, "row":Landroid/view/View;
    sget v8, Lcom/vlingo/midas/R$id;->text1:I

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 226
    .local v6, "text1":Landroid/widget/TextView;
    sget v8, Lcom/vlingo/midas/R$id;->text2:I

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 227
    .local v7, "text2":Landroid/widget/TextView;
    const-string/jumbo v8, "EXTRA_INFO"

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    const-string/jumbo v8, "EXTRA_CAPTION"

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    sget v8, Lcom/vlingo/midas/R$id;->wcis_info_with_caption_row_divider:I

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 230
    .local v0, "divider":Landroid/view/View;
    if-ne p1, v3, :cond_0

    .line 231
    const/16 v8, 0x8

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 275
    .end local v0    # "divider":Landroid/view/View;
    .end local v3    # "infoItemCount":I
    .end local v4    # "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v6    # "text1":Landroid/widget/TextView;
    .end local v7    # "text2":Landroid/widget/TextView;
    :cond_0
    :goto_0
    return-object v5

    .line 234
    .end local v5    # "row":Landroid/view/View;
    .restart local v3    # "infoItemCount":I
    .restart local v4    # "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_1
    const-string/jumbo v8, "EXTRA_SUBHEADING"

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 235
    iget-object v8, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    sget v9, Lcom/vlingo/midas/R$layout;->wcis_subheading_row:I

    invoke-static {v8, v9, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 236
    .restart local v5    # "row":Landroid/view/View;
    sget v8, Lcom/vlingo/midas/R$id;->text1:I

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 237
    .restart local v6    # "text1":Landroid/widget/TextView;
    const-string/jumbo v8, "EXTRA_SUBHEADING"

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 241
    .end local v5    # "row":Landroid/view/View;
    .end local v6    # "text1":Landroid/widget/TextView;
    :cond_2
    iget-object v8, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    sget v9, Lcom/vlingo/midas/R$layout;->wcis_lyt_row:I

    invoke-static {v8, v9, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 243
    .restart local v5    # "row":Landroid/view/View;
    sget v8, Lcom/vlingo/midas/R$id;->text1:I

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 244
    .restart local v6    # "text1":Landroid/widget/TextView;
    sget v8, Lcom/vlingo/midas/R$id;->text2:I

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 246
    .restart local v7    # "text2":Landroid/widget/TextView;
    sget v8, Lcom/vlingo/midas/R$id;->wcis_lyt_row_divider:I

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 247
    .restart local v0    # "divider":Landroid/view/View;
    invoke-virtual {p0}, Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;->getCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ne p1, v8, :cond_3

    .line 248
    const/4 v8, 0x4

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 255
    :cond_3
    sget v8, Lcom/vlingo/midas/R$id;->image:I

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 257
    .local v2, "image":Landroid/widget/ImageView;
    iget-object v9, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    const-string/jumbo v8, "EXTRA_LIST_ICON"

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    # invokes: Lcom/vlingo/midas/help/WhatCanISayScreen;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    invoke-static {v9, v8}, Lcom/vlingo/midas/help/WhatCanISayScreen;->access$000(Lcom/vlingo/midas/help/WhatCanISayScreen;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 258
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_4

    .line 259
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 264
    :goto_1
    const-string/jumbo v8, "EXTRA_LIST_TITLE"

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    const-string/jumbo v8, "EXTRA_LIST_EXAMPLE"

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 261
    :cond_4
    const-string/jumbo v8, "EXTRA_LIST_ICON"

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 274
    .end local v0    # "divider":Landroid/view/View;
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v2    # "image":Landroid/widget/ImageView;
    .end local v3    # "infoItemCount":I
    .end local v4    # "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v5    # "row":Landroid/view/View;
    .end local v6    # "text1":Landroid/widget/TextView;
    .end local v7    # "text2":Landroid/widget/TextView;
    :cond_5
    iget-object v8, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    sget v9, Lcom/vlingo/midas/R$layout;->wcis_info_row:I

    invoke-static {v8, v9, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 275
    .restart local v5    # "row":Landroid/view/View;
    goto/16 :goto_0
.end method

.method public isEnabled(I)Z
    .locals 7
    .param p1, "position"    # I

    .prologue
    .line 195
    const/4 v0, 0x0

    .line 196
    .local v0, "generalHeaderIdx":I
    const/4 v4, 0x4

    .line 197
    .local v4, "wcisHeaderIdx":I
    const/4 v1, 0x1

    .line 199
    .local v1, "isEnabled":Z
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->isAsrEditingEnabled()Z

    move-result v3

    .line 202
    .local v3, "supportedLang":Z
    const-string/jumbo v5, "CLIENT_VERSION"

    const-string/jumbo v6, "Q2"

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "Q2"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 203
    .local v2, "supportedClient":Z
    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    .line 204
    iget-object v5, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    iget-object v5, v5, Lcom/vlingo/midas/help/WhatCanISayScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v5}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasPenFeature()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 205
    add-int/lit8 v4, v4, 0x1

    .line 208
    :cond_0
    iget-object v5, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    invoke-virtual {v5}, Lcom/vlingo/midas/help/WhatCanISayScreen;->isInIUXMode()Z

    move-result v5

    if-nez v5, :cond_2

    .line 209
    if-eq p1, v0, :cond_1

    if-ne p1, v4, :cond_2

    .line 210
    :cond_1
    const/4 v1, 0x0

    .line 213
    :cond_2
    return v1
.end method
