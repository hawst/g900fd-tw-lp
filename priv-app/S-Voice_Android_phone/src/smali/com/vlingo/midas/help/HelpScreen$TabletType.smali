.class public final enum Lcom/vlingo/midas/help/HelpScreen$TabletType;
.super Ljava/lang/Enum;
.source "HelpScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/help/HelpScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "TabletType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/help/HelpScreen$TabletType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/help/HelpScreen$TabletType;

.field public static final enum OTHERS:Lcom/vlingo/midas/help/HelpScreen$TabletType;

.field public static final enum SANTOS_10:Lcom/vlingo/midas/help/HelpScreen$TabletType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 64
    new-instance v0, Lcom/vlingo/midas/help/HelpScreen$TabletType;

    const-string/jumbo v1, "SANTOS_10"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/help/HelpScreen$TabletType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/help/HelpScreen$TabletType;->SANTOS_10:Lcom/vlingo/midas/help/HelpScreen$TabletType;

    new-instance v0, Lcom/vlingo/midas/help/HelpScreen$TabletType;

    const-string/jumbo v1, "OTHERS"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/midas/help/HelpScreen$TabletType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/help/HelpScreen$TabletType;->OTHERS:Lcom/vlingo/midas/help/HelpScreen$TabletType;

    .line 63
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/midas/help/HelpScreen$TabletType;

    sget-object v1, Lcom/vlingo/midas/help/HelpScreen$TabletType;->SANTOS_10:Lcom/vlingo/midas/help/HelpScreen$TabletType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/midas/help/HelpScreen$TabletType;->OTHERS:Lcom/vlingo/midas/help/HelpScreen$TabletType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/help/HelpScreen$TabletType;->$VALUES:[Lcom/vlingo/midas/help/HelpScreen$TabletType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/help/HelpScreen$TabletType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 63
    const-class v0, Lcom/vlingo/midas/help/HelpScreen$TabletType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/help/HelpScreen$TabletType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/help/HelpScreen$TabletType;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/vlingo/midas/help/HelpScreen$TabletType;->$VALUES:[Lcom/vlingo/midas/help/HelpScreen$TabletType;

    invoke-virtual {v0}, [Lcom/vlingo/midas/help/HelpScreen$TabletType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/help/HelpScreen$TabletType;

    return-object v0
.end method
