.class public Lcom/vlingo/midas/help/WCISData;
.super Ljava/lang/Object;
.source "WCISData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/help/WCISData$CustomComparator;
    }
.end annotation


# static fields
.field static final CAPTION:Ljava/lang/String; = "EXTRA_CAPTION"

.field public static final EXTRA_LIST_HEIGHT:Ljava/lang/String; = "EXTRA_LIST_HEIGHT"

.field static final INFO:Ljava/lang/String; = "EXTRA_INFO"

.field public static final INTENT:Ljava/lang/String; = "EXTRA_INTENT"

.field private static final IN_CAR:Ljava/lang/String; = "InCar"

.field public static final LIST_EXAMPLE:Ljava/lang/String; = "EXTRA_LIST_EXAMPLE"

.field public static final LIST_ICON:Ljava/lang/String; = "EXTRA_LIST_ICON"

.field public static final LIST_ICON_DRAWABLE:Ljava/lang/String; = "EXTRA_LIST_ICON_DRAWABLE"

.field public static final LIST_MOVE_ACTION:Ljava/lang/String; = "LIST_MOVE_ACTION"

.field public static final LIST_TITLE:Ljava/lang/String; = "EXTRA_LIST_TITLE"

.field static final SCREEN:Ljava/lang/String; = "EXTRA_SCREEN"

.field public static final SUBHEADING:Ljava/lang/String; = "EXTRA_SUBHEADING"

.field static final mChinaCodes:[Ljava/lang/String;


# instance fields
.field private items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field mValue:Lcom/vlingo/midas/MidasValues;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 47
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "CTC"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/midas/help/WCISData;->mChinaCodes:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/help/WCISData;->items:Ljava/util/ArrayList;

    .line 556
    return-void
.end method

.method private static getSalesCodeProperty()Ljava/lang/String;
    .locals 2

    .prologue
    .line 602
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v0

    const-string/jumbo v1, "ro.csc.sales_code"

    invoke-virtual {v0, v1}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isCTCtelcom()Z
    .locals 6

    .prologue
    .line 606
    invoke-static {}, Lcom/vlingo/midas/help/WCISData;->getSalesCodeProperty()Ljava/lang/String;

    move-result-object v4

    .line 607
    .local v4, "saleCode":Ljava/lang/String;
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_1

    .line 608
    sget-object v0, Lcom/vlingo/midas/help/WCISData;->mChinaCodes:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 609
    .local v1, "code":Ljava/lang/String;
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 610
    const/4 v5, 0x1

    .line 613
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "code":Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :goto_1
    return v5

    .line 608
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "code":Ljava/lang/String;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 613
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "code":Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z
    .locals 4
    .param p1, "feature"    # Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 573
    invoke-virtual {p1}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 579
    :cond_0
    :goto_0
    return v0

    .line 576
    :cond_1
    invoke-virtual {p1}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->getReason()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "InCar"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 577
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 579
    goto :goto_0
.end method

.method private isLt03()Z
    .locals 4

    .prologue
    const/16 v3, 0xa00

    const/16 v2, 0x640

    .line 593
    const/4 v0, 0x0

    .line 594
    .local v0, "returnTablet":Z
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ne v1, v3, :cond_0

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    if-eq v1, v2, :cond_1

    :cond_0
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ne v1, v2, :cond_2

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ne v1, v3, :cond_2

    .line 596
    :cond_1
    const/4 v0, 0x1

    .line 598
    :cond_2
    return v0
.end method


# virtual methods
.method protected addInfoItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "caption"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 536
    .local p3, "screen":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 537
    .local v0, "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string/jumbo v1, "EXTRA_INFO"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 538
    const-string/jumbo v1, "EXTRA_CAPTION"

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 539
    const-string/jumbo v1, "EXTRA_SCREEN"

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 540
    iget-object v1, p0, Lcom/vlingo/midas/help/WCISData;->items:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 541
    return-void
.end method

.method protected addIntentItem(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "caption"    # Ljava/lang/String;
    .param p3, "i"    # Landroid/content/Intent;

    .prologue
    .line 544
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 545
    .local v0, "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string/jumbo v1, "EXTRA_INFO"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 546
    const-string/jumbo v1, "EXTRA_CAPTION"

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 547
    const-string/jumbo v1, "EXTRA_INTENT"

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 548
    iget-object v1, p0, Lcom/vlingo/midas/help/WCISData;->items:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 549
    return-void
.end method

.method protected addItem(Landroid/content/Context;IIIIII)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "title"    # I
    .param p3, "example"    # I
    .param p4, "image"    # I
    .param p5, "subtitle"    # I
    .param p6, "subtitleImage"    # I
    .param p7, "exList"    # I

    .prologue
    .line 61
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-virtual/range {v0 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIIII)V

    .line 62
    return-void
.end method

.method protected addItem(Landroid/content/Context;IIIIIII)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "title"    # I
    .param p3, "example"    # I
    .param p4, "image"    # I
    .param p5, "subtitle"    # I
    .param p6, "subtitleImage"    # I
    .param p7, "exList"    # I
    .param p8, "didYouKnow"    # I

    .prologue
    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 68
    .local v0, "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string/jumbo v1, "EXTRA_LIST_TITLE"

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    const-string/jumbo v1, "EXTRA_LIST_EXAMPLE"

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    const-string/jumbo v1, "EXTRA_LIST_ICON"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    const-string/jumbo v1, "EXTRA_TITLE_BAR"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    const-string/jumbo v1, "EXTRA_SUBTITLE"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    const-string/jumbo v1, "EXTRA_SUBTITLE_ICON"

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    const-string/jumbo v1, "EXTRA_EXAMPLE_LIST"

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    const-string/jumbo v1, "EXTRA_DID_YOU_KNOW"

    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    invoke-virtual {p0}, Lcom/vlingo/midas/help/WCISData;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    return-void
.end method

.method public addItems(Landroid/content/Context;)V
    .locals 19
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 84
    new-instance v1, Lcom/vlingo/midas/MidasValues;

    move-object/from16 v0, p1

    invoke-direct {v1, v0}, Lcom/vlingo/midas/MidasValues;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vlingo/midas/help/WCISData;->mValue:Lcom/vlingo/midas/MidasValues;

    .line 86
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v1

    if-eqz v1, :cond_16

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "zh_CN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_16

    const/16 v18, 0x1

    .line 89
    .local v18, "isSetKorEngAtChineseBuild":Z
    :goto_0
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->CALL:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasCallEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vlingo/midas/help/WCISData;->mValue:Lcom/vlingo/midas/MidasValues;

    invoke-virtual {v1}, Lcom/vlingo/midas/MidasValues;->isVideoCallingSupported()Z

    move-result v1

    if-eqz v1, :cond_17

    .line 91
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_dial:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_dial_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_phone:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_voice_dial_call_contacts:I

    sget v7, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_phone:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_voice_dial_car_examples:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    .line 112
    :cond_0
    :goto_1
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->MESSAGING:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasMessaging()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 113
    sget v3, Lcom/vlingo/midas/R$string;->text_message:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_text_message_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_message:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_sms_speak_text:I

    sget v7, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_message:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_sms_examples:I

    sget v9, Lcom/vlingo/midas/R$string;->did_you_know_message:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v9}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIIII)V

    .line 124
    :cond_1
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->CONTACT:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 125
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_findcontact:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_findcontact_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_contacts:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_find_contact:I

    sget v7, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_contacts:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_findcontact_examples:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    .line 138
    :cond_2
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->MEMO:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_19

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string/jumbo v2, "EK-GC200"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_19

    .line 139
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v1

    if-nez v1, :cond_18

    .line 140
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_memo:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_memo_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_memo_pen:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_memo:I

    sget v7, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_memo_pen:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_memo_examples:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    .line 169
    :cond_3
    :goto_2
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->EVENT:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 170
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v1

    if-nez v1, :cond_1a

    .line 171
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_schedule:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_schedule_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_planner:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_schedule:I

    sget v7, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_planner:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_schedule_examples:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    .line 201
    :cond_4
    :goto_3
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->TASK:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v1

    if-nez v1, :cond_5

    .line 202
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_task:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_task_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_planner:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_task:I

    sget v7, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_planner:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_task_examples:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    .line 212
    :cond_5
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->MUSIC:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 213
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v1

    if-nez v1, :cond_1d

    .line 214
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->offerCarMode()Z

    move-result v1

    if-nez v1, :cond_1c

    .line 215
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_music:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_music_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_music:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_music:I

    sget v7, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_music:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_music_examples:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    .line 248
    :cond_6
    :goto_4
    if-nez v18, :cond_7

    .line 249
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->SOCIAL:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v1

    if-nez v1, :cond_7

    .line 250
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 251
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_social_update:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_social_update_sub1_cn:I

    sget v5, Lcom/vlingo/midas/R$drawable;->help_social_hub:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_social_update_cn:I

    sget v7, Lcom/vlingo/midas/R$drawable;->help_social_hub:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_social_update_examples_cn:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    .line 272
    :cond_7
    :goto_5
    if-eqz v18, :cond_1f

    .line 273
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->SEARCH:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v1

    if-nez v1, :cond_8

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "en_US"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 274
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_search:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_search_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->help_search:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_search_speak_query:I

    sget v7, Lcom/vlingo/midas/R$drawable;->help_search:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_search_examples_cn:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    .line 318
    :cond_8
    :goto_6
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->OPENAPP:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v1

    if-nez v1, :cond_9

    .line 319
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_open_app:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_open_app_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_applications:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_open_app:I

    sget v7, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_applications:I

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isAmericanPhone()Z

    move-result v1

    if-eqz v1, :cond_22

    sget v8, Lcom/vlingo/midas/R$array;->wcis_open_app_examples_american:I

    :goto_7
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    .line 328
    :cond_9
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->VOICERECORD:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v1

    if-nez v1, :cond_a

    .line 329
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_recordvoice:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_recordvoice_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_voice_recorder:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_recordvoice:I

    sget v7, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_voice_recorder:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_recordvoice_examples:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    .line 360
    :cond_a
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->ALARM:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v1

    if-nez v1, :cond_b

    .line 361
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_set_an_alarm:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_set_an_alarm_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_clock:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_alarm:I

    sget v7, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_clock:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_alarm_examples:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    .line 371
    :cond_b
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->TIMER:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v1

    if-nez v1, :cond_c

    .line 372
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_start_timer:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_start_timer_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_clock:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_timer:I

    sget v7, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_clock:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_timer_examples:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    .line 395
    :cond_c
    sget v3, Lcom/vlingo/midas/R$string;->wcis_simple_setting_controls:I

    .line 396
    .local v3, "wcis_simple_setting_controls_id":I
    sget v8, Lcom/vlingo/midas/R$array;->wcis_simple_setting_controls_examples:I

    .line 397
    .local v8, "wcis_simple_setting_controls_examples_id":I
    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_simple_setting_controls_sub1:I

    .line 399
    .local v4, "help_wcis_simple_setting_controls_sub1_id":I
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 400
    sget v3, Lcom/vlingo/midas/R$string;->wcis_simple_setting_controls_for_chinese:I

    .line 401
    sget v8, Lcom/vlingo/midas/R$array;->wcis_simple_setting_controls_examples_for_chinese:I

    .line 402
    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_simple_setting_controls_sub1_for_chinese:I

    .line 405
    :cond_d
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->SETTINGS:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v1

    if-nez v1, :cond_e

    .line 406
    sget v5, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_settings:I

    sget v7, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_settings:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move v6, v3

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    .line 416
    :cond_e
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->hasCarModeApp()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 417
    sget v11, Lcom/vlingo/midas/R$string;->help_wcis_driving_mode:I

    sget v12, Lcom/vlingo/midas/R$string;->help_wcis_driving_mode_sub1:I

    sget v13, Lcom/vlingo/midas/R$drawable;->help_icon_drivingmode:I

    sget v14, Lcom/vlingo/midas/R$string;->wcis_driving_mode:I

    sget v15, Lcom/vlingo/midas/R$drawable;->help_icon_drivingmode:I

    sget v16, Lcom/vlingo/midas/R$array;->wcis_driving_mode_examples:I

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    invoke-virtual/range {v9 .. v16}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    .line 426
    :cond_f
    if-nez v18, :cond_15

    .line 427
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->NAVIGATION:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-nez v1, :cond_10

    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->MAPS:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_11

    :cond_10
    invoke-static {}, Lcom/vlingo/midas/help/WCISData;->isCTCtelcom()Z

    move-result v1

    if-nez v1, :cond_11

    .line 428
    sget v11, Lcom/vlingo/midas/R$string;->help_wcis_nav:I

    sget v12, Lcom/vlingo/midas/R$string;->help_wcis_nav_sub1:I

    sget v13, Lcom/vlingo/midas/R$drawable;->help_navi:I

    sget v14, Lcom/vlingo/midas/R$string;->wcis_nav:I

    sget v15, Lcom/vlingo/midas/R$drawable;->help_navi:I

    sget v16, Lcom/vlingo/midas/R$array;->wcis_nav_examples:I

    sget v17, Lcom/vlingo/midas/R$string;->did_you_know_navigate:I

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    invoke-virtual/range {v9 .. v17}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIIII)V

    .line 439
    :cond_11
    sget v11, Lcom/vlingo/midas/R$string;->help_wcis_news:I

    sget v12, Lcom/vlingo/midas/R$string;->help_wcis_news_sub1:I

    sget v13, Lcom/vlingo/midas/R$drawable;->help_news:I

    sget v14, Lcom/vlingo/midas/R$string;->wcis_news:I

    sget v15, Lcom/vlingo/midas/R$drawable;->help_news:I

    sget v16, Lcom/vlingo/midas/R$array;->wcis_news_examples:I

    sget v17, Lcom/vlingo/midas/R$string;->did_you_know_news:I

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    invoke-virtual/range {v9 .. v17}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIIII)V

    .line 449
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->WEATHER:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 450
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v1

    if-eqz v1, :cond_23

    .line 451
    sget v11, Lcom/vlingo/midas/R$string;->help_wcis_check_weather:I

    sget v12, Lcom/vlingo/midas/R$string;->help_wcis_check_weather_sub1:I

    sget v13, Lcom/vlingo/midas/R$drawable;->help_icon_weather_cn:I

    sget v14, Lcom/vlingo/midas/R$string;->wcis_check_weather:I

    sget v15, Lcom/vlingo/midas/R$drawable;->help_icon_weather_cn:I

    sget v16, Lcom/vlingo/midas/R$array;->wcis_check_weather_examples:I

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    invoke-virtual/range {v9 .. v16}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    .line 470
    :cond_12
    :goto_8
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->ANSWER:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_13

    const/4 v1, 0x5

    invoke-static {v1}, Lcom/vlingo/midas/ClientConfiguration;->isEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_13

    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v1

    if-nez v1, :cond_13

    .line 471
    sget v11, Lcom/vlingo/midas/R$string;->help_wcis_get_an_answer:I

    sget v12, Lcom/vlingo/midas/R$string;->help_wcis_get_an_answer_sub1:I

    sget v13, Lcom/vlingo/midas/R$drawable;->help_qa:I

    sget v14, Lcom/vlingo/midas/R$string;->wcis_get_an_answers:I

    sget v15, Lcom/vlingo/midas/R$drawable;->help_qa:I

    sget v16, Lcom/vlingo/midas/R$array;->wcis_get_an_answers_examples:I

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    invoke-virtual/range {v9 .. v16}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    .line 482
    :cond_13
    const/16 v1, 0xf

    invoke-static {v1}, Lcom/vlingo/midas/ClientConfiguration;->isEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v1

    if-nez v1, :cond_14

    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v1

    if-nez v1, :cond_14

    .line 483
    sget v11, Lcom/vlingo/midas/R$string;->help_wcis_movie:I

    sget v12, Lcom/vlingo/midas/R$string;->help_wcis_movie_sub1_naver:I

    sget v13, Lcom/vlingo/midas/R$drawable;->help_icon_movie:I

    sget v14, Lcom/vlingo/midas/R$string;->wcis_movie_naver:I

    sget v15, Lcom/vlingo/midas/R$drawable;->help_icon_movie:I

    sget v16, Lcom/vlingo/midas/R$array;->wcis_movies_examples_naver:I

    sget v17, Lcom/vlingo/midas/R$string;->did_you_know_movies:I

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    invoke-virtual/range {v9 .. v17}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIIII)V

    .line 495
    :cond_14
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->LOCALSEARCH:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v1

    if-nez v1, :cond_15

    .line 496
    const/16 v1, 0xf

    invoke-static {v1}, Lcom/vlingo/midas/ClientConfiguration;->isEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_24

    .line 497
    sget v11, Lcom/vlingo/midas/R$string;->help_wcis_local_listings_naver:I

    sget v12, Lcom/vlingo/midas/R$string;->help_wcis_local_listings_sub1_naver:I

    sget v13, Lcom/vlingo/midas/R$drawable;->help_icon_local_search:I

    sget v14, Lcom/vlingo/midas/R$string;->wcis_local_listings_naver:I

    sget v15, Lcom/vlingo/midas/R$drawable;->help_icon_local_search:I

    sget v16, Lcom/vlingo/midas/R$array;->wcis_local_listings_examples_naver:I

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    invoke-virtual/range {v9 .. v16}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    .line 533
    :cond_15
    :goto_9
    return-void

    .line 86
    .end local v3    # "wcis_simple_setting_controls_id":I
    .end local v4    # "help_wcis_simple_setting_controls_sub1_id":I
    .end local v8    # "wcis_simple_setting_controls_examples_id":I
    .end local v18    # "isSetKorEngAtChineseBuild":Z
    :cond_16
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 101
    .restart local v18    # "isSetKorEngAtChineseBuild":Z
    :cond_17
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_dial:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_dial_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_phone:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_voice_dial_call_contacts:I

    sget v7, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_phone:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_voice_dial_car_examples_us:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    goto/16 :goto_1

    .line 150
    :cond_18
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_memo:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_memo_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_memo_pen:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_memo:I

    sget v7, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_memo_pen:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_memo_in_car_examples:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    goto/16 :goto_2

    .line 159
    :cond_19
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->MEMO_ADD:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string/jumbo v2, "EK-GC200"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 160
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_memo:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_memo_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_memo_pen:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_memo:I

    sget v7, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_memo_pen:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_memo_in_car_examples:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    goto/16 :goto_2

    .line 181
    :cond_1a
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_schedule:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_schedule_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_planner:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_schedule:I

    sget v7, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_planner:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_schedule_in_car_examples:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    goto/16 :goto_3

    .line 190
    :cond_1b
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->EVENT_ADD:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->EVENT_LOOKUP:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 191
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_schedule:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_schedule_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_planner:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_schedule:I

    sget v7, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_planner:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_schedule_in_car_examples:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    goto/16 :goto_3

    .line 225
    :cond_1c
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_music:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_music_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_music:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_music:I

    sget v7, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_music:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_music_examples:I

    sget v9, Lcom/vlingo/midas/R$string;->did_you_know_music:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v9}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIIII)V

    goto/16 :goto_4

    .line 237
    :cond_1d
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_music:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_music_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_music:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_music:I

    sget v7, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_music:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_music_examples:I

    sget v9, Lcom/vlingo/midas/R$string;->did_you_know_music_in_car:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v9}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIIII)V

    goto/16 :goto_4

    .line 260
    :cond_1e
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_social_update:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_social_update_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->help_social_hub:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_social_update:I

    sget v7, Lcom/vlingo/midas/R$drawable;->help_social_hub:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_social_update_examples:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    goto/16 :goto_5

    .line 284
    :cond_1f
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->SEARCH:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISData;->isDfcEnabled(Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v1

    if-nez v1, :cond_8

    .line 285
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v1

    if-eqz v1, :cond_20

    .line 286
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_search:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_search_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->help_search:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_search_speak_query:I

    sget v7, Lcom/vlingo/midas/R$drawable;->help_search:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_search_examples_cn:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    goto/16 :goto_6

    .line 295
    :cond_20
    const/16 v1, 0xf

    invoke-static {v1}, Lcom/vlingo/midas/ClientConfiguration;->isEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 296
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_search:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_search_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->help_search:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_search_speak_query:I

    sget v7, Lcom/vlingo/midas/R$drawable;->help_search:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_search_examples_naver_add:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    goto/16 :goto_6

    .line 306
    :cond_21
    sget v3, Lcom/vlingo/midas/R$string;->help_wcis_search:I

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_search_sub1:I

    sget v5, Lcom/vlingo/midas/R$drawable;->help_search:I

    sget v6, Lcom/vlingo/midas/R$string;->wcis_search_speak_query:I

    sget v7, Lcom/vlingo/midas/R$drawable;->help_search:I

    sget v8, Lcom/vlingo/midas/R$array;->wcis_search_examples:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    goto/16 :goto_6

    .line 319
    :cond_22
    sget v8, Lcom/vlingo/midas/R$array;->wcis_open_app_examples:I

    goto/16 :goto_7

    .line 460
    .restart local v3    # "wcis_simple_setting_controls_id":I
    .restart local v4    # "help_wcis_simple_setting_controls_sub1_id":I
    .restart local v8    # "wcis_simple_setting_controls_examples_id":I
    :cond_23
    sget v11, Lcom/vlingo/midas/R$string;->help_wcis_check_weather:I

    sget v12, Lcom/vlingo/midas/R$string;->help_wcis_check_weather_sub1:I

    sget v13, Lcom/vlingo/midas/R$drawable;->help_weather:I

    sget v14, Lcom/vlingo/midas/R$string;->wcis_check_weather:I

    sget v15, Lcom/vlingo/midas/R$drawable;->help_weather:I

    sget v16, Lcom/vlingo/midas/R$array;->wcis_check_weather_examples:I

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    invoke-virtual/range {v9 .. v16}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIII)V

    goto/16 :goto_8

    .line 505
    :cond_24
    const/16 v1, 0xa

    invoke-static {v1}, Lcom/vlingo/midas/ClientConfiguration;->isEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 507
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v1

    if-eqz v1, :cond_25

    .line 508
    sget v11, Lcom/vlingo/midas/R$string;->help_wcis_local_listings:I

    sget v12, Lcom/vlingo/midas/R$string;->help_wcis_local_listings_sub1:I

    sget v13, Lcom/vlingo/midas/R$drawable;->help_icon_local_search_cn:I

    sget v14, Lcom/vlingo/midas/R$string;->wcis_local_listings:I

    sget v15, Lcom/vlingo/midas/R$drawable;->help_icon_local_search_cn:I

    sget v16, Lcom/vlingo/midas/R$array;->wcis_local_listings_examples:I

    const/16 v17, 0x0

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    invoke-virtual/range {v9 .. v17}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIIII)V

    goto/16 :goto_9

    .line 518
    :cond_25
    sget v11, Lcom/vlingo/midas/R$string;->help_wcis_local_listings:I

    sget v12, Lcom/vlingo/midas/R$string;->help_wcis_local_listings_sub1:I

    sget v13, Lcom/vlingo/midas/R$drawable;->help_icon_local_search:I

    sget v14, Lcom/vlingo/midas/R$string;->wcis_local_listings:I

    sget v15, Lcom/vlingo/midas/R$drawable;->help_icon_local_search:I

    sget v16, Lcom/vlingo/midas/R$array;->wcis_local_listings_examples:I

    sget v17, Lcom/vlingo/midas/R$string;->did_you_know_local_listings:I

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    invoke-virtual/range {v9 .. v17}, Lcom/vlingo/midas/help/WCISData;->addItem(Landroid/content/Context;IIIIIII)V

    goto/16 :goto_9
.end method

.method protected addSubHeading(Ljava/lang/String;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 551
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 552
    .local v0, "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string/jumbo v1, "EXTRA_SUBHEADING"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 553
    iget-object v1, p0, Lcom/vlingo/midas/help/WCISData;->items:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 554
    return-void
.end method

.method public getInfoItemCount()I
    .locals 4

    .prologue
    .line 584
    const/4 v0, 0x0

    .line 585
    .local v0, "count":I
    iget-object v3, p0, Lcom/vlingo/midas/help/WCISData;->items:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    .line 586
    .local v2, "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string/jumbo v3, "EXTRA_INFO"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 587
    .end local v2    # "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_1
    return v0
.end method

.method public getItems()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 569
    iget-object v0, p0, Lcom/vlingo/midas/help/WCISData;->items:Ljava/util/ArrayList;

    return-object v0
.end method
