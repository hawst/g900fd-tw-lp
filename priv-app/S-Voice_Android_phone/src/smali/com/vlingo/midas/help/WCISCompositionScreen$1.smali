.class Lcom/vlingo/midas/help/WCISCompositionScreen$1;
.super Ljava/lang/Object;
.source "WCISCompositionScreen.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/help/WCISCompositionScreen;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/help/WCISCompositionScreen;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 210
    iget-object v6, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    iget-object v6, v6, Lcom/vlingo/midas/help/WCISCompositionScreen;->inflater:Landroid/view/LayoutInflater;

    sget v7, Lcom/vlingo/midas/R$layout;->dialog_edittext:I

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 211
    .local v3, "linear":Landroid/widget/LinearLayout;
    iget-object v6, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    sget v7, Lcom/vlingo/midas/R$string;->settings_incar_homeaddress_title:I

    invoke-virtual {v6, v7}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 212
    .local v5, "title":Ljava/lang/String;
    iget-object v6, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    sget v7, Lcom/vlingo/midas/R$string;->ok_uc:I

    invoke-virtual {v6, v7}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 213
    .local v4, "save":Ljava/lang/String;
    iget-object v6, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    sget v7, Lcom/vlingo/midas/R$string;->cancel:I

    invoke-virtual {v6, v7}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 215
    .local v0, "cancel":Ljava/lang/String;
    iget-object v7, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    sget v6, Lcom/vlingo/midas/R$id;->edittext:I

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    # setter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->editText:Landroid/widget/EditText;
    invoke-static {v7, v6}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$002(Lcom/vlingo/midas/help/WCISCompositionScreen;Landroid/widget/EditText;)Landroid/widget/EditText;

    .line 216
    iget-object v6, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    # getter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->editText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$000(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/widget/EditText;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 217
    iget-object v6, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    # getter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->editText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$000(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/widget/EditText;

    move-result-object v6

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setMaxLines(I)V

    .line 218
    const-string/jumbo v6, "car_nav_home_address"

    invoke-static {v6, v8}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 219
    .local v2, "homeAddress":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_0

    .line 220
    iget-object v6, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    # getter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->editText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$000(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/widget/EditText;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 222
    :cond_0
    iget-object v6, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    # getter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->editText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$000(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/widget/EditText;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/EditText;->requestFocus()Z

    .line 223
    iget-object v6, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    # getter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->editText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$000(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/widget/EditText;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 224
    iget-object v6, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    # getter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->editText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$000(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/widget/EditText;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/EditText;->selectAll()V

    .line 225
    iget-object v6, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    # getter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->editText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$000(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/widget/EditText;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 226
    iget-object v6, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    # getter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->editText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$000(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/widget/EditText;

    move-result-object v6

    new-instance v7, Landroid/widget/Scroller;

    iget-object v8, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    invoke-virtual {v8}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setScroller(Landroid/widget/Scroller;)V

    .line 227
    iget-object v6, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    # getter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->editText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$000(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/widget/EditText;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setMaxLines(I)V

    .line 228
    iget-object v6, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    # getter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->editText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$000(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/widget/EditText;

    move-result-object v6

    new-array v7, v9, [Landroid/text/InputFilter;

    new-instance v8, Landroid/text/InputFilter$LengthFilter;

    const/16 v9, 0x12c

    invoke-direct {v8, v9}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v8, v7, v10

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 229
    iget-object v6, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    # getter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->editText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$000(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/widget/EditText;

    move-result-object v6

    new-instance v7, Lcom/vlingo/midas/help/WCISCompositionScreen$1$1;

    invoke-direct {v7, p0}, Lcom/vlingo/midas/help/WCISCompositionScreen$1$1;-><init>(Lcom/vlingo/midas/help/WCISCompositionScreen$1;)V

    const-wide/16 v8, 0xc8

    invoke-virtual {v6, v7, v8, v9}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 249
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v6, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    invoke-direct {v1, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 250
    .local v1, "d":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 251
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 252
    new-instance v6, Lcom/vlingo/midas/help/WCISCompositionScreen$1$2;

    invoke-direct {v6, p0}, Lcom/vlingo/midas/help/WCISCompositionScreen$1$2;-><init>(Lcom/vlingo/midas/help/WCISCompositionScreen$1;)V

    invoke-virtual {v1, v4, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 275
    new-instance v6, Lcom/vlingo/midas/help/WCISCompositionScreen$1$3;

    invoke-direct {v6, p0}, Lcom/vlingo/midas/help/WCISCompositionScreen$1$3;-><init>(Lcom/vlingo/midas/help/WCISCompositionScreen$1;)V

    invoke-virtual {v1, v0, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 290
    iget-object v6, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v7

    # setter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->dialog:Landroid/app/AlertDialog;
    invoke-static {v6, v7}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$102(Lcom/vlingo/midas/help/WCISCompositionScreen;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 291
    iget-object v6, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    # getter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->editText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$000(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/widget/EditText;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    # getter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->textWather:Landroid/text/TextWatcher;
    invoke-static {v7}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$200(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/text/TextWatcher;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 292
    return-void
.end method
