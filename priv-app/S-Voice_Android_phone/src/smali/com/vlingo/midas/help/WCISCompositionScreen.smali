.class public Lcom/vlingo/midas/help/WCISCompositionScreen;
.super Lcom/vlingo/midas/ui/VLActivity;
.source "WCISCompositionScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/help/WCISCompositionScreen$TabletType;
    }
.end annotation


# static fields
.field public static final EXTRA_DID_YOU_KNOW:Ljava/lang/String; = "EXTRA_DID_YOU_KNOW"

.field public static final EXTRA_EXAMPLE_LIST:Ljava/lang/String; = "EXTRA_EXAMPLE_LIST"

.field public static final EXTRA_SUBTITLE:Ljava/lang/String; = "EXTRA_SUBTITLE"

.field public static final EXTRA_SUBTITLE_ICON:Ljava/lang/String; = "EXTRA_SUBTITLE_ICON"

.field public static final EXTRA_TITLE_BAR:Ljava/lang/String; = "EXTRA_TITLE_BAR"

.field private static final GONE:I = 0x8

.field private static alphaPattern:Ljava/util/regex/Pattern;

.field private static final log:Lcom/vlingo/core/internal/logging/Logger;

.field private static mTheme:I


# instance fields
.field private btnSetHomeAddress:Landroid/widget/Button;

.field private currentTablet:Lcom/vlingo/midas/help/WCISCompositionScreen$TabletType;

.field private customTitleSupported:Z

.field private dialog:Landroid/app/AlertDialog;

.field private editText:Landroid/widget/EditText;

.field inflater:Landroid/view/LayoutInflater;

.field private mDensity:F

.field private mOldLX:F

.field private mOldLY:F

.field private mOldPX:F

.field private mOldPY:F

.field private mOldX:F

.field private mOldY:F

.field mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

.field private my_orientation:I

.field private settingClicked:Landroid/view/View$OnClickListener;

.field private textWather:Landroid/text/TextWatcher;

.field private topTitle:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 58
    const-class v0, Lcom/vlingo/midas/help/WCISCompositionScreen;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/help/WCISCompositionScreen;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 75
    sget v0, Lcom/vlingo/midas/R$style;->CustomNoActionBar:I

    sput v0, Lcom/vlingo/midas/help/WCISCompositionScreen;->mTheme:I

    .line 85
    const-string/jumbo v0, ".*\\p{Alnum}.*"

    const/16 v1, 0x20

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/help/WCISCompositionScreen;->alphaPattern:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Lcom/vlingo/midas/ui/VLActivity;-><init>()V

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->customTitleSupported:Z

    .line 76
    iput v1, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->mDensity:F

    .line 77
    iput v1, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->mOldX:F

    .line 78
    iput v1, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->mOldY:F

    .line 79
    iput v1, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->mOldPX:F

    .line 80
    iput v1, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->mOldPY:F

    .line 82
    iput v1, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->mOldLX:F

    .line 83
    iput v1, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->mOldLY:F

    .line 91
    sget-object v0, Lcom/vlingo/midas/help/WCISCompositionScreen$TabletType;->OTHERS:Lcom/vlingo/midas/help/WCISCompositionScreen$TabletType;

    iput-object v0, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->currentTablet:Lcom/vlingo/midas/help/WCISCompositionScreen$TabletType;

    .line 343
    new-instance v0, Lcom/vlingo/midas/help/WCISCompositionScreen$2;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/help/WCISCompositionScreen$2;-><init>(Lcom/vlingo/midas/help/WCISCompositionScreen;)V

    iput-object v0, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->settingClicked:Landroid/view/View$OnClickListener;

    .line 426
    new-instance v0, Lcom/vlingo/midas/help/WCISCompositionScreen$3;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/help/WCISCompositionScreen$3;-><init>(Lcom/vlingo/midas/help/WCISCompositionScreen;)V

    iput-object v0, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->textWather:Landroid/text/TextWatcher;

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/help/WCISCompositionScreen;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->editText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$002(Lcom/vlingo/midas/help/WCISCompositionScreen;Landroid/widget/EditText;)Landroid/widget/EditText;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/help/WCISCompositionScreen;
    .param p1, "x1"    # Landroid/widget/EditText;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->editText:Landroid/widget/EditText;

    return-object p1
.end method

.method static synthetic access$100(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/help/WCISCompositionScreen;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->dialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$102(Lcom/vlingo/midas/help/WCISCompositionScreen;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/help/WCISCompositionScreen;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->dialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/text/TextWatcher;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/help/WCISCompositionScreen;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->textWather:Landroid/text/TextWatcher;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/help/WCISCompositionScreen;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/help/WCISCompositionScreen;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/vlingo/midas/help/WCISCompositionScreen;->containsNoAlphaNumeric(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private checkIcon(I)V
    .locals 1
    .param p1, "ResId"    # I

    .prologue
    .line 367
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_memo_pen:I

    if-ne p1, v0, :cond_1

    .line 368
    iget-object v0, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setMemoIcon()V

    .line 381
    :cond_0
    :goto_0
    return-void

    .line 369
    :cond_1
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_message:I

    if-ne p1, v0, :cond_2

    .line 370
    iget-object v0, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setMessageIcon()V

    goto :goto_0

    .line 371
    :cond_2
    sget v0, Lcom/vlingo/midas/R$drawable;->help_navi:I

    if-ne p1, v0, :cond_3

    .line 372
    iget-object v0, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setNavigationIcon()V

    goto :goto_0

    .line 373
    :cond_3
    sget v0, Lcom/vlingo/midas/R$drawable;->help_weather:I

    if-ne p1, v0, :cond_4

    .line 374
    iget-object v0, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setWeatherIcon()V

    goto :goto_0

    .line 375
    :cond_4
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_planner:I

    if-ne p1, v0, :cond_5

    .line 376
    iget-object v0, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setPlannerIcon()V

    goto :goto_0

    .line 377
    :cond_5
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_voice_recorder:I

    if-ne p1, v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setVoiceRecorderIcon()V

    goto :goto_0
.end method

.method private containsNoAlphaNumeric(Ljava/lang/String;)Z
    .locals 2
    .param p1, "promptString"    # Ljava/lang/String;

    .prologue
    .line 458
    sget-object v0, Lcom/vlingo/midas/help/WCISCompositionScreen;->alphaPattern:Ljava/util/regex/Pattern;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getDrawable(I)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p1, "ResId"    # I

    .prologue
    .line 384
    const/4 v0, 0x0

    .line 386
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    sget v1, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_memo_pen:I

    if-ne p1, v1, :cond_0

    .line 387
    iget-object v1, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v1}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getMemoIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v1, v0

    .line 405
    :goto_0
    return-object v1

    .line 389
    :cond_0
    sget v1, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_message:I

    if-ne p1, v1, :cond_1

    .line 390
    iget-object v1, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v1}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getMessageIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v1, v0

    .line 391
    goto :goto_0

    .line 392
    :cond_1
    sget v1, Lcom/vlingo/midas/R$drawable;->help_navi:I

    if-ne p1, v1, :cond_2

    .line 393
    iget-object v1, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v1}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getNavigationIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v1, v0

    .line 394
    goto :goto_0

    .line 395
    :cond_2
    sget v1, Lcom/vlingo/midas/R$drawable;->help_weather:I

    if-ne p1, v1, :cond_3

    .line 396
    iget-object v1, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v1}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getWeatherIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v1, v0

    .line 397
    goto :goto_0

    .line 398
    :cond_3
    sget v1, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_planner:I

    if-ne p1, v1, :cond_4

    .line 399
    iget-object v1, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v1}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getPlannerIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v1, v0

    .line 400
    goto :goto_0

    .line 401
    :cond_4
    sget v1, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_voice_recorder:I

    if-ne p1, v1, :cond_5

    .line 402
    iget-object v1, p0, Lcom/vlingo/midas/help/WCISCompositionScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v1}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getVoiceRecorderIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v1, v0

    .line 403
    goto :goto_0

    .line 405
    :cond_5
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setTextFromHtml(IIZ)V
    .locals 7
    .param p1, "textResId"    # I
    .param p2, "textViewResId"    # I
    .param p3, "array"    # Z

    .prologue
    .line 410
    invoke-virtual {p0, p2}, Lcom/vlingo/midas/help/WCISCompositionScreen;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 411
    .local v5, "tv":Landroid/widget/TextView;
    if-eqz v5, :cond_0

    if-eqz p2, :cond_0

    .line 412
    if-nez p3, :cond_1

    .line 413
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 423
    :cond_0
    :goto_0
    return-void

    .line 415
    :cond_1
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 416
    .local v1, "dyk":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, p1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    .line 417
    .local v4, "s":Ljava/lang/String;
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 416
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 420
    .end local v4    # "s":Ljava/lang/String;
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private showPreferences()V
    .locals 3

    .prologue
    .line 349
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 350
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "is_start_option_menu"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 351
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->startActivity(Landroid/content/Intent;)V

    .line 352
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 328
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 329
    sget v2, Lcom/vlingo/midas/R$id;->left_container:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/help/WCISCompositionScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 330
    .local v0, "leftContainer":Landroid/widget/RelativeLayout;
    sget v2, Lcom/vlingo/midas/R$id;->right_container:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/help/WCISCompositionScreen;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 331
    .local v1, "rightContainer":Landroid/widget/RelativeLayout;
    if-eqz p1, :cond_0

    .line 332
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 333
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 334
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 340
    :cond_0
    :goto_0
    return-void

    .line 335
    :cond_1
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 336
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 337
    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 25
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 98
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v21

    if-eqz v21, :cond_0

    .line 99
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/help/WCISCompositionScreen;->setRequestedOrientation(I)V

    .line 103
    :cond_0
    new-instance v13, Landroid/util/DisplayMetrics;

    invoke-direct {v13}, Landroid/util/DisplayMetrics;-><init>()V

    .line 104
    .local v13, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 105
    iget v0, v13, Landroid/util/DisplayMetrics;->density:F

    move/from16 v21, v0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/help/WCISCompositionScreen;->mDensity:F

    .line 107
    iget v0, v13, Landroid/util/DisplayMetrics;->density:F

    move/from16 v21, v0

    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v21, v0

    const-wide/high16 v23, 0x3ff0000000000000L    # 1.0

    cmpl-double v21, v21, v23

    if-nez v21, :cond_1

    iget v0, v13, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v21, v0

    iget v0, v13, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v22, v0

    add-int v21, v21, v22

    const/16 v22, 0x7f0

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_1

    iget v0, v13, Landroid/util/DisplayMetrics;->xdpi:F

    move/from16 v21, v0

    const v22, 0x4315d32c

    invoke-static/range {v21 .. v22}, Ljava/lang/Float;->compare(FF)I

    move-result v21

    if-nez v21, :cond_1

    iget v0, v13, Landroid/util/DisplayMetrics;->ydpi:F

    move/from16 v21, v0

    const v22, 0x431684be

    invoke-static/range {v21 .. v22}, Ljava/lang/Float;->compare(FF)I

    move-result v21

    if-eqz v21, :cond_2

    :cond_1
    iget v0, v13, Landroid/util/DisplayMetrics;->density:F

    move/from16 v21, v0

    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v21, v0

    const-wide/high16 v23, 0x3ff0000000000000L    # 1.0

    cmpl-double v21, v21, v23

    if-nez v21, :cond_b

    iget v0, v13, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v21, v0

    iget v0, v13, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v22, v0

    add-int v21, v21, v22

    const/16 v22, 0x820

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_b

    iget v0, v13, Landroid/util/DisplayMetrics;->xdpi:F

    move/from16 v21, v0

    const v22, 0x4315d2f2

    invoke-static/range {v21 .. v22}, Ljava/lang/Float;->compare(FF)I

    move-result v21

    if-nez v21, :cond_b

    iget v0, v13, Landroid/util/DisplayMetrics;->ydpi:F

    move/from16 v21, v0

    const v22, 0x4316849c

    invoke-static/range {v21 .. v22}, Ljava/lang/Float;->compare(FF)I

    move-result v21

    if-nez v21, :cond_b

    .line 115
    :cond_2
    sget v21, Lcom/vlingo/midas/R$style;->DialogSlideAnim:I

    sput v21, Lcom/vlingo/midas/help/WCISCompositionScreen;->mTheme:I

    .line 116
    sget v21, Lcom/vlingo/midas/help/WCISCompositionScreen;->mTheme:I

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/help/WCISCompositionScreen;->setTheme(I)V

    .line 122
    :goto_0
    invoke-super/range {p0 .. p1}, Lcom/vlingo/midas/ui/VLActivity;->onCreate(Landroid/os/Bundle;)V

    .line 124
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 126
    const/16 v21, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/help/WCISCompositionScreen;->requestWindowFeature(I)Z

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/help/WCISCompositionScreen;->customTitleSupported:Z

    .line 128
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getIntent()Landroid/content/Intent;

    move-result-object v21

    const-string/jumbo v22, "EXTRA_TITLE_BAR"

    const/16 v23, 0x0

    invoke-virtual/range {v21 .. v23}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    .line 129
    .local v17, "titleBarResId":I
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getIntent()Landroid/content/Intent;

    move-result-object v21

    const-string/jumbo v22, "EXTRA_SUBTITLE_ICON"

    const/16 v23, 0x0

    invoke-virtual/range {v21 .. v23}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    .line 130
    .local v10, "iconResId":I
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getIntent()Landroid/content/Intent;

    move-result-object v21

    const-string/jumbo v22, "EXTRA_SUBTITLE"

    const/16 v23, 0x0

    invoke-virtual/range {v21 .. v23}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v16

    .line 131
    .local v16, "subtitleResId":I
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getIntent()Landroid/content/Intent;

    move-result-object v21

    const-string/jumbo v22, "EXTRA_EXAMPLE_LIST"

    const/16 v23, 0x0

    invoke-virtual/range {v21 .. v23}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    .line 132
    .local v9, "exArrResId":I
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getIntent()Landroid/content/Intent;

    move-result-object v21

    const-string/jumbo v22, "EXTRA_DID_YOU_KNOW"

    const/16 v23, 0x0

    invoke-virtual/range {v21 .. v23}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 134
    .local v5, "didYouKnowResId":I
    sget v21, Lcom/vlingo/midas/R$layout;->compose_wcis:I

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/help/WCISCompositionScreen;->setContentView(I)V

    .line 135
    if-eqz v17, :cond_5

    .line 138
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getActionBar()Landroid/app/ActionBar;

    move-result-object v18

    .line 140
    .local v18, "titlebar":Landroid/app/ActionBar;
    if-eqz v18, :cond_4

    .line 143
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v21

    if-eqz v21, :cond_c

    .line 144
    const/16 v21, 0x8

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 156
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 161
    :cond_4
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/help/WCISCompositionScreen;->setTitle(Ljava/lang/CharSequence;)V

    .line 164
    .end local v18    # "titlebar":Landroid/app/ActionBar;
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/view/Display;->getRotation()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/view/Display;->getRotation()I

    move-result v21

    const/16 v22, 0x3

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_7

    .line 169
    :cond_6
    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/help/WCISCompositionScreen;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 173
    :cond_7
    new-instance v21, Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/ui/PackageInfoProvider;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/help/WCISCompositionScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    .line 177
    if-eqz v10, :cond_8

    .line 178
    sget v21, Lcom/vlingo/midas/R$id;->wycs_detail_image:I

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/help/WCISCompositionScreen;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    .line 180
    .local v11, "iv":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/vlingo/midas/help/WCISCompositionScreen;->checkIcon(I)V

    .line 181
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 182
    .local v7, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v7, :cond_e

    .line 183
    invoke-virtual {v11, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 188
    .end local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v11    # "iv":Landroid/widget/ImageView;
    :cond_8
    :goto_2
    sget v21, Lcom/vlingo/midas/R$id;->wycs_detail_title_text:I

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/midas/help/WCISCompositionScreen;->setTextFromHtml(IIZ)V

    .line 190
    sget v21, Lcom/vlingo/midas/R$array;->wcis_music_examples:I

    move/from16 v0, v21

    if-ne v9, v0, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/help/WCISCompositionScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-object/from16 v21, v0

    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasRadio()Z

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_f

    .line 191
    sget v21, Lcom/vlingo/midas/R$array;->wcis_music_examples_no_radio:I

    sget v22, Lcom/vlingo/midas/R$id;->text_examples:I

    const/16 v23, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/midas/help/WCISCompositionScreen;->setTextFromHtml(IIZ)V

    .line 195
    :goto_3
    if-nez v5, :cond_10

    .line 196
    sget v21, Lcom/vlingo/midas/R$id;->header_did_you_know:I

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/help/WCISCompositionScreen;->findViewById(I)Landroid/view/View;

    move-result-object v21

    const/16 v22, 0x8

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->setVisibility(I)V

    .line 197
    sget v21, Lcom/vlingo/midas/R$id;->text_did_you_know:I

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/help/WCISCompositionScreen;->findViewById(I)Landroid/view/View;

    move-result-object v21

    const/16 v22, 0x8

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->setVisibility(I)V

    .line 202
    :goto_4
    const-string/jumbo v21, "layout_inflater"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/view/LayoutInflater;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/help/WCISCompositionScreen;->inflater:Landroid/view/LayoutInflater;

    .line 204
    sget v21, Lcom/vlingo/midas/R$id;->button_set_home_address:I

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/help/WCISCompositionScreen;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/Button;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/help/WCISCompositionScreen;->btnSetHomeAddress:Landroid/widget/Button;

    .line 205
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/help/WCISCompositionScreen;->btnSetHomeAddress:Landroid/widget/Button;

    move-object/from16 v21, v0

    if-eqz v21, :cond_9

    .line 206
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/help/WCISCompositionScreen;->btnSetHomeAddress:Landroid/widget/Button;

    move-object/from16 v21, v0

    new-instance v22, Lcom/vlingo/midas/help/WCISCompositionScreen$1;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/help/WCISCompositionScreen$1;-><init>(Lcom/vlingo/midas/help/WCISCompositionScreen;)V

    invoke-virtual/range {v21 .. v22}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 295
    :cond_9
    sget v21, Lcom/vlingo/midas/R$string;->help_wcis_nav:I

    move/from16 v0, v17

    move/from16 v1, v21

    if-eq v0, v1, :cond_a

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/help/WCISCompositionScreen;->btnSetHomeAddress:Landroid/widget/Button;

    move-object/from16 v21, v0

    const/16 v22, 0x8

    invoke-virtual/range {v21 .. v22}, Landroid/widget/Button;->setVisibility(I)V

    .line 301
    :cond_a
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getWindow()Landroid/view/Window;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v12

    .line 302
    .local v12, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v21

    const-string/jumbo v22, "privateFlags"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v14

    .line 303
    .local v14, "privateFlags":Ljava/lang/reflect/Field;
    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v21

    const-string/jumbo v22, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v15

    .line 304
    .local v15, "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v21

    const-string/jumbo v22, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v6

    .line 306
    .local v6, "disableMultiTray":Ljava/lang/reflect/Field;
    const/4 v4, 0x0

    .local v4, "currentPrivateFlags":I
    const/16 v20, 0x0

    .local v20, "valueofFlagsEnableStatusBar":I
    const/16 v19, 0x0

    .line 307
    .local v19, "valueofFlagsDisableTray":I
    invoke-virtual {v14, v12}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v4

    .line 308
    invoke-virtual {v15, v12}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v20

    .line 309
    invoke-virtual {v6, v12}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v19

    .line 311
    or-int v4, v4, v20

    .line 312
    or-int v4, v4, v19

    .line 314
    invoke-virtual {v14, v12, v4}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 315
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getWindow()Landroid/view/Window;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 324
    .end local v4    # "currentPrivateFlags":I
    .end local v6    # "disableMultiTray":Ljava/lang/reflect/Field;
    .end local v12    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v14    # "privateFlags":Ljava/lang/reflect/Field;
    .end local v15    # "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    .end local v19    # "valueofFlagsDisableTray":I
    .end local v20    # "valueofFlagsEnableStatusBar":I
    :goto_5
    return-void

    .line 118
    .end local v5    # "didYouKnowResId":I
    .end local v9    # "exArrResId":I
    .end local v10    # "iconResId":I
    .end local v16    # "subtitleResId":I
    .end local v17    # "titleBarResId":I
    :cond_b
    sget v21, Lcom/vlingo/midas/R$style;->CustomNoActionBar:I

    sput v21, Lcom/vlingo/midas/help/WCISCompositionScreen;->mTheme:I

    goto/16 :goto_0

    .line 146
    .restart local v5    # "didYouKnowResId":I
    .restart local v9    # "exArrResId":I
    .restart local v10    # "iconResId":I
    .restart local v16    # "subtitleResId":I
    .restart local v17    # "titleBarResId":I
    .restart local v18    # "titlebar":Landroid/app/ActionBar;
    :cond_c
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v21

    if-eqz v21, :cond_d

    .line 147
    if-eqz v18, :cond_3

    .line 148
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    sget v22, Lcom/vlingo/midas/R$drawable;->tw_ab_transparent_dark_holo:I

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 150
    const/16 v21, 0xc

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    goto/16 :goto_1

    .line 154
    :cond_d
    const/16 v21, 0xe

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    goto/16 :goto_1

    .line 185
    .end local v18    # "titlebar":Landroid/app/ActionBar;
    .restart local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    .restart local v11    # "iv":Landroid/widget/ImageView;
    :cond_e
    invoke-virtual {v11, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 193
    .end local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v11    # "iv":Landroid/widget/ImageView;
    :cond_f
    sget v21, Lcom/vlingo/midas/R$id;->text_examples:I

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v9, v1, v2}, Lcom/vlingo/midas/help/WCISCompositionScreen;->setTextFromHtml(IIZ)V

    goto/16 :goto_3

    .line 199
    :cond_10
    sget v21, Lcom/vlingo/midas/R$id;->text_did_you_know:I

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v5, v1, v2}, Lcom/vlingo/midas/help/WCISCompositionScreen;->setTextFromHtml(IIZ)V

    goto/16 :goto_4

    .line 319
    :catch_0
    move-exception v8

    .line 322
    .local v8, "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 316
    .end local v8    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v21

    goto :goto_5
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 357
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 361
    :goto_0
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 359
    :pswitch_0
    invoke-virtual {p0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->finish()V

    goto :goto_0

    .line 357
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 464
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onResume()V

    .line 466
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 467
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v2, "com.vlingo.client.app.action.ACTIVITY_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 468
    const-string/jumbo v2, "com.vlingo.client.app.extra.STATE"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 469
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 474
    sget v2, Lcom/vlingo/midas/R$id;->wycs_detail_title_text:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/help/WCISCompositionScreen;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 475
    .local v1, "subTitle":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 476
    invoke-virtual {p0}, Lcom/vlingo/midas/help/WCISCompositionScreen;->finish()V

    .line 479
    :cond_0
    return-void
.end method
