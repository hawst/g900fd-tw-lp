.class Lcom/vlingo/midas/help/HelpScreen$1;
.super Ljava/lang/Object;
.source "HelpScreen.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/help/HelpScreen;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/help/HelpScreen;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/help/HelpScreen;)V
    .locals 0

    .prologue
    .line 220
    iput-object p1, p0, Lcom/vlingo/midas/help/HelpScreen$1;->this$0:Lcom/vlingo/midas/help/HelpScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v7, 0x0

    .line 223
    iget-object v4, p0, Lcom/vlingo/midas/help/HelpScreen$1;->this$0:Lcom/vlingo/midas/help/HelpScreen;

    invoke-virtual {v4}, Lcom/vlingo/midas/help/HelpScreen;->getWcisData()Lcom/vlingo/midas/help/WCISData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/midas/help/WCISData;->getItems()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/HashMap;

    const-string/jumbo v5, "EXTRA_TITLE_BAR"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_2

    .line 224
    iget-object v4, p0, Lcom/vlingo/midas/help/HelpScreen$1;->this$0:Lcom/vlingo/midas/help/HelpScreen;

    invoke-virtual {v4}, Lcom/vlingo/midas/help/HelpScreen;->getWcisData()Lcom/vlingo/midas/help/WCISData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/midas/help/WCISData;->getItems()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/HashMap;

    const-string/jumbo v5, "EXTRA_SCREEN"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 225
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iget-object v4, p0, Lcom/vlingo/midas/help/HelpScreen$1;->this$0:Lcom/vlingo/midas/help/HelpScreen;

    invoke-virtual {v4}, Lcom/vlingo/midas/help/HelpScreen;->getWcisData()Lcom/vlingo/midas/help/WCISData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/midas/help/WCISData;->getItems()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/HashMap;

    const-string/jumbo v5, "EXTRA_INTENT"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 226
    .local v1, "i":Landroid/content/Intent;
    if-eqz v0, :cond_1

    .line 227
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lcom/vlingo/midas/help/HelpScreen$1;->this$0:Lcom/vlingo/midas/help/HelpScreen;

    invoke-direct {v2, v4, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 228
    .local v2, "intent":Landroid/content/Intent;
    const-string/jumbo v4, "wycs.is.iux"

    invoke-virtual {v2, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 229
    const-string/jumbo v4, "svoice"

    iget-object v5, p0, Lcom/vlingo/midas/help/HelpScreen$1;->this$0:Lcom/vlingo/midas/help/HelpScreen;

    invoke-virtual {v5}, Lcom/vlingo/midas/help/HelpScreen;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string/jumbo v6, "svoice"

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 230
    iget-object v4, p0, Lcom/vlingo/midas/help/HelpScreen$1;->this$0:Lcom/vlingo/midas/help/HelpScreen;

    invoke-virtual {v4, v2}, Lcom/vlingo/midas/help/HelpScreen;->startActivity(Landroid/content/Intent;)V

    .line 246
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v1    # "i":Landroid/content/Intent;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 231
    .restart local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .restart local v1    # "i":Landroid/content/Intent;
    :cond_1
    if-eqz v1, :cond_0

    .line 232
    iget-object v4, p0, Lcom/vlingo/midas/help/HelpScreen$1;->this$0:Lcom/vlingo/midas/help/HelpScreen;

    invoke-virtual {v4, v1}, Lcom/vlingo/midas/help/HelpScreen;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 234
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v1    # "i":Landroid/content/Intent;
    :cond_2
    iget-object v4, p0, Lcom/vlingo/midas/help/HelpScreen$1;->this$0:Lcom/vlingo/midas/help/HelpScreen;

    invoke-virtual {v4}, Lcom/vlingo/midas/help/HelpScreen;->getWcisData()Lcom/vlingo/midas/help/WCISData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/midas/help/WCISData;->getItems()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge p3, v4, :cond_3

    .line 235
    iget-object v4, p0, Lcom/vlingo/midas/help/HelpScreen$1;->this$0:Lcom/vlingo/midas/help/HelpScreen;

    invoke-virtual {v4}, Lcom/vlingo/midas/help/HelpScreen;->getWcisData()Lcom/vlingo/midas/help/WCISData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/midas/help/WCISData;->getItems()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    .line 236
    .local v3, "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lcom/vlingo/midas/help/HelpScreen$1;->this$0:Lcom/vlingo/midas/help/HelpScreen;

    const-class v5, Lcom/vlingo/midas/help/WCISCompositionScreen;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 237
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string/jumbo v5, "EXTRA_TITLE_BAR"

    const-string/jumbo v4, "EXTRA_TITLE_BAR"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 238
    const-string/jumbo v5, "EXTRA_SUBTITLE"

    const-string/jumbo v4, "EXTRA_SUBTITLE"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 239
    const-string/jumbo v5, "EXTRA_SUBTITLE_ICON"

    const-string/jumbo v4, "EXTRA_SUBTITLE_ICON"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 240
    const-string/jumbo v5, "EXTRA_EXAMPLE_LIST"

    const-string/jumbo v4, "EXTRA_EXAMPLE_LIST"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 241
    const-string/jumbo v5, "EXTRA_DID_YOU_KNOW"

    const-string/jumbo v4, "EXTRA_DID_YOU_KNOW"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 242
    iget-object v4, p0, Lcom/vlingo/midas/help/HelpScreen$1;->this$0:Lcom/vlingo/midas/help/HelpScreen;

    invoke-virtual {v4, v2}, Lcom/vlingo/midas/help/HelpScreen;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 244
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_3
    iget-object v4, p0, Lcom/vlingo/midas/help/HelpScreen$1;->this$0:Lcom/vlingo/midas/help/HelpScreen;

    new-instance v5, Landroid/content/Intent;

    iget-object v6, p0, Lcom/vlingo/midas/help/HelpScreen$1;->this$0:Lcom/vlingo/midas/help/HelpScreen;

    const-class v7, Lcom/vlingo/midas/ui/SimpleIconListScreen;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/help/HelpScreen;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
