.class public Lcom/vlingo/midas/help/AboutScreenFragment;
.super Landroid/app/Fragment;
.source "AboutScreenFragment.java"

# interfaces
.implements Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnBackPressedListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ValidFragment"
    }
.end annotation


# static fields
.field public static final SAMSUNG_REVISION_NUMBER:Ljava/lang/String; = "r1"


# instance fields
.field private button_clicked:I

.field private deviceID:Ljava/lang/String;

.field private mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

.field rootView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/help/AboutScreenFragment;->button_clicked:I

    .line 48
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;)V
    .locals 1
    .param p1, "onFragmentLoadListener"    # Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/help/AboutScreenFragment;->button_clicked:I

    .line 43
    iput-object p1, p0, Lcom/vlingo/midas/help/AboutScreenFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/help/AboutScreenFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/help/AboutScreenFragment;

    .prologue
    .line 32
    iget v0, p0, Lcom/vlingo/midas/help/AboutScreenFragment;->button_clicked:I

    return v0
.end method

.method static synthetic access$002(Lcom/vlingo/midas/help/AboutScreenFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/help/AboutScreenFragment;
    .param p1, "x1"    # I

    .prologue
    .line 32
    iput p1, p0, Lcom/vlingo/midas/help/AboutScreenFragment;->button_clicked:I

    return p1
.end method

.method private setAboutText()V
    .locals 7

    .prologue
    .line 147
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget v4, Lcom/vlingo/midas/R$string;->device_id:I

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/help/AboutScreenFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/help/AboutScreenFragment;->deviceID:Ljava/lang/String;

    .line 148
    const/4 v0, 0x1

    .line 149
    .local v0, "showUUID":Z
    iget-object v3, p0, Lcom/vlingo/midas/help/AboutScreenFragment;->rootView:Landroid/view/View;

    sget v4, Lcom/vlingo/midas/R$id;->text_version:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 150
    .local v1, "tv":Landroid/widget/TextView;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget v4, Lcom/vlingo/midas/R$string;->help_version:I

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/help/AboutScreenFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vlingo/midas/help/AboutScreenFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->space:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getVersion()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "r1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    iget-object v3, p0, Lcom/vlingo/midas/help/AboutScreenFragment;->rootView:Landroid/view/View;

    sget v4, Lcom/vlingo/midas/R$id;->uuid_text:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 153
    .local v2, "uuid":Landroid/widget/TextView;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/vlingo/midas/help/AboutScreenFragment;->deviceID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getUUIDDeviceID()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/vlingo/midas/help/AboutScreenFragment;->deviceID:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    rsub-int/lit8 v6, v6, 0x28

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    new-instance v3, Lcom/vlingo/midas/help/AboutScreenFragment$3;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/help/AboutScreenFragment$3;-><init>(Lcom/vlingo/midas/help/AboutScreenFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/vlingo/midas/help/AboutScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;

    if-eqz v0, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/vlingo/midas/help/AboutScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->onHeadlineSelected(I)V

    .line 189
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 52
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 53
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/vlingo/midas/help/AboutScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$layout;->settings_path_view:I

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 55
    .local v2, "view":Landroid/view/View;
    sget v3, Lcom/vlingo/midas/R$id;->fragment_description:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 56
    .local v1, "textView":Landroid/widget/TextView;
    sget v3, Lcom/vlingo/midas/R$id;->icon:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 57
    .local v0, "iconView":Landroid/widget/ImageView;
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 58
    invoke-virtual {p0}, Lcom/vlingo/midas/help/AboutScreenFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->help_menu8:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    iget-object v3, p0, Lcom/vlingo/midas/help/AboutScreenFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    if-eqz v3, :cond_0

    .line 60
    iget-object v3, p0, Lcom/vlingo/midas/help/AboutScreenFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    const/4 v4, 0x1

    invoke-interface {v3, v2, v4}, Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;->updatePathHeader(Landroid/view/View;I)V

    .line 62
    .end local v0    # "iconView":Landroid/widget/ImageView;
    .end local v1    # "textView":Landroid/widget/TextView;
    .end local v2    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v8, 0x8

    .line 68
    sget v6, Lcom/vlingo/midas/R$layout;->help_about:I

    const/4 v7, 0x0

    invoke-virtual {p1, v6, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/midas/help/AboutScreenFragment;->rootView:Landroid/view/View;

    .line 69
    iget-object v6, p0, Lcom/vlingo/midas/help/AboutScreenFragment;->rootView:Landroid/view/View;

    sget v7, Lcom/vlingo/midas/R$id;->about_layout:I

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 70
    .local v2, "layout":Landroid/view/ViewGroup;
    iget-object v6, p0, Lcom/vlingo/midas/help/AboutScreenFragment;->rootView:Landroid/view/View;

    sget v7, Lcom/vlingo/midas/R$id;->help_about_text_powered_by:I

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 72
    .local v4, "mAboutText":Landroid/widget/TextView;
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 74
    iget-object v6, p0, Lcom/vlingo/midas/help/AboutScreenFragment;->rootView:Landroid/view/View;

    sget v7, Lcom/vlingo/midas/R$id;->help_about_title:I

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 75
    .local v0, "aboutTitle":Landroid/widget/TextView;
    const/high16 v6, 0x41f00000    # 30.0f

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextSize(F)V

    .line 78
    .end local v0    # "aboutTitle":Landroid/widget/TextView;
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 80
    if-eqz v4, :cond_1

    .line 81
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 83
    :cond_1
    sget v6, Lcom/vlingo/midas/R$id;->ImageView01:I

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 84
    .local v5, "sensoryLogoImage":Landroid/widget/ImageView;
    if-eqz v5, :cond_2

    .line 85
    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 108
    .end local v5    # "sensoryLogoImage":Landroid/widget/ImageView;
    :cond_2
    :goto_0
    iget-object v6, p0, Lcom/vlingo/midas/help/AboutScreenFragment;->rootView:Landroid/view/View;

    sget v7, Lcom/vlingo/midas/R$id;->image_logo:I

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 109
    .local v3, "logoImage":Landroid/widget/ImageView;
    sget v6, Lcom/vlingo/midas/R$drawable;->voice_talk_nuance:I

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 110
    iget-object v6, p0, Lcom/vlingo/midas/help/AboutScreenFragment;->rootView:Landroid/view/View;

    sget v7, Lcom/vlingo/midas/R$id;->btn_tos:I

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 112
    .local v1, "btn":Landroid/widget/Button;
    new-instance v6, Lcom/vlingo/midas/help/AboutScreenFragment$1;

    invoke-direct {v6, p0}, Lcom/vlingo/midas/help/AboutScreenFragment$1;-><init>(Lcom/vlingo/midas/help/AboutScreenFragment;)V

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v6, p0, Lcom/vlingo/midas/help/AboutScreenFragment;->rootView:Landroid/view/View;

    sget v7, Lcom/vlingo/midas/R$id;->btn_privacy:I

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "btn":Landroid/widget/Button;
    check-cast v1, Landroid/widget/Button;

    .line 127
    .restart local v1    # "btn":Landroid/widget/Button;
    new-instance v6, Lcom/vlingo/midas/help/AboutScreenFragment$2;

    invoke-direct {v6, p0}, Lcom/vlingo/midas/help/AboutScreenFragment$2;-><init>(Lcom/vlingo/midas/help/AboutScreenFragment;)V

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    invoke-direct {p0}, Lcom/vlingo/midas/help/AboutScreenFragment;->setAboutText()V

    .line 141
    iget-object v6, p0, Lcom/vlingo/midas/help/AboutScreenFragment;->rootView:Landroid/view/View;

    return-object v6

    .line 88
    .end local v1    # "btn":Landroid/widget/Button;
    .end local v3    # "logoImage":Landroid/widget/ImageView;
    :cond_3
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 89
    if-eqz v4, :cond_2

    .line 90
    const/4 v6, -0x1

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 94
    :cond_4
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 95
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 96
    sget v6, Lcom/vlingo/midas/R$color;->solid_white:I

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    goto :goto_0

    .line 97
    :cond_5
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 98
    sget v6, Lcom/vlingo/midas/R$color;->help_about_bg_color:I

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    goto :goto_0

    .line 100
    :cond_6
    sget v6, Lcom/vlingo/midas/R$color;->solid_black:I

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    goto :goto_0

    .line 102
    :cond_7
    invoke-virtual {p0}, Lcom/vlingo/midas/help/AboutScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    sget v7, Lcom/vlingo/midas/R$drawable;->about_bg:I

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getVersion()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v2, v7, v8}, Lcom/vlingo/core/internal/VlingoAndroidCore;->regBackgroundImage(Landroid/content/Context;Landroid/view/ViewGroup;ILjava/lang/String;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 176
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 177
    iput v3, p0, Lcom/vlingo/midas/help/AboutScreenFragment;->button_clicked:I

    .line 178
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vlingo/midas/help/AboutScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 179
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.client.app.action.ACTIVITY_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 180
    const-string/jumbo v1, "com.vlingo.client.app.extra.STATE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 181
    invoke-virtual {p0}, Lcom/vlingo/midas/help/AboutScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 182
    return-void
.end method
