.class Lcom/vlingo/midas/help/AboutScreenFragment$2;
.super Ljava/lang/Object;
.source "AboutScreenFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/help/AboutScreenFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/help/AboutScreenFragment;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/help/AboutScreenFragment;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/vlingo/midas/help/AboutScreenFragment$2;->this$0:Lcom/vlingo/midas/help/AboutScreenFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 129
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->about_privacy_url:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 130
    .local v1, "uri":Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 131
    .local v0, "i":Landroid/content/Intent;
    invoke-static {v0}, Lcom/vlingo/midas/util/IntentResolver;->isavailable(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 132
    iget-object v2, p0, Lcom/vlingo/midas/help/AboutScreenFragment$2;->this$0:Lcom/vlingo/midas/help/AboutScreenFragment;

    # getter for: Lcom/vlingo/midas/help/AboutScreenFragment;->button_clicked:I
    invoke-static {v2}, Lcom/vlingo/midas/help/AboutScreenFragment;->access$000(Lcom/vlingo/midas/help/AboutScreenFragment;)I

    move-result v2

    if-nez v2, :cond_0

    .line 133
    iget-object v2, p0, Lcom/vlingo/midas/help/AboutScreenFragment$2;->this$0:Lcom/vlingo/midas/help/AboutScreenFragment;

    const/4 v3, 0x1

    # setter for: Lcom/vlingo/midas/help/AboutScreenFragment;->button_clicked:I
    invoke-static {v2, v3}, Lcom/vlingo/midas/help/AboutScreenFragment;->access$002(Lcom/vlingo/midas/help/AboutScreenFragment;I)I

    .line 134
    iget-object v2, p0, Lcom/vlingo/midas/help/AboutScreenFragment$2;->this$0:Lcom/vlingo/midas/help/AboutScreenFragment;

    invoke-virtual {v2, v0}, Lcom/vlingo/midas/help/AboutScreenFragment;->startActivity(Landroid/content/Intent;)V

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    iget-object v2, p0, Lcom/vlingo/midas/help/AboutScreenFragment$2;->this$0:Lcom/vlingo/midas/help/AboutScreenFragment;

    invoke-virtual {v2}, Lcom/vlingo/midas/help/AboutScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "No Matching application installed  to open the link"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
