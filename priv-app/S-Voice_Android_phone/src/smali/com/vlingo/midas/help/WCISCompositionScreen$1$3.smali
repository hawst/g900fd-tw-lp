.class Lcom/vlingo/midas/help/WCISCompositionScreen$1$3;
.super Ljava/lang/Object;
.source "WCISCompositionScreen.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/help/WCISCompositionScreen$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/help/WCISCompositionScreen$1;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/help/WCISCompositionScreen$1;)V
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1$3;->this$1:Lcom/vlingo/midas/help/WCISCompositionScreen$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 279
    iget-object v2, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1$3;->this$1:Lcom/vlingo/midas/help/WCISCompositionScreen$1;

    iget-object v2, v2, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    const-string/jumbo v3, "input_method"

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 280
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v2, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1$3;->this$1:Lcom/vlingo/midas/help/WCISCompositionScreen$1;

    iget-object v2, v2, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    # getter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->editText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$000(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 281
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    .line 283
    const-wide/16 v2, 0x96

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 288
    :goto_0
    return-void

    .line 284
    :catch_0
    move-exception v0

    .line 286
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
