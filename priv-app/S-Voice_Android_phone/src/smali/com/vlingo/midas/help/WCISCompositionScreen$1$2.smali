.class Lcom/vlingo/midas/help/WCISCompositionScreen$1$2;
.super Ljava/lang/Object;
.source "WCISCompositionScreen.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/help/WCISCompositionScreen$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/help/WCISCompositionScreen$1;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/help/WCISCompositionScreen$1;)V
    .locals 0

    .prologue
    .line 252
    iput-object p1, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1$2;->this$1:Lcom/vlingo/midas/help/WCISCompositionScreen$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 256
    iget-object v4, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1$2;->this$1:Lcom/vlingo/midas/help/WCISCompositionScreen$1;

    iget-object v4, v4, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    # getter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->editText:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$000(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 257
    .local v1, "homeAddress":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 259
    .local v2, "homeAddressCheck":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    array-length v4, v4

    if-gtz v4, :cond_0

    .line 260
    const-string/jumbo v4, "car_nav_home_address"

    const-string/jumbo v5, ""

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    :goto_0
    iget-object v4, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1$2;->this$1:Lcom/vlingo/midas/help/WCISCompositionScreen$1;

    iget-object v4, v4, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    const-string/jumbo v5, "input_method"

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/help/WCISCompositionScreen;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    .line 266
    .local v3, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v4, p0, Lcom/vlingo/midas/help/WCISCompositionScreen$1$2;->this$1:Lcom/vlingo/midas/help/WCISCompositionScreen$1;

    iget-object v4, v4, Lcom/vlingo/midas/help/WCISCompositionScreen$1;->this$0:Lcom/vlingo/midas/help/WCISCompositionScreen;

    # getter for: Lcom/vlingo/midas/help/WCISCompositionScreen;->editText:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/vlingo/midas/help/WCISCompositionScreen;->access$000(Lcom/vlingo/midas/help/WCISCompositionScreen;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 268
    const-wide/16 v4, 0x96

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    :goto_1
    return-void

    .line 262
    .end local v3    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    const-string/jumbo v4, "car_nav_home_address"

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 269
    .restart local v3    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :catch_0
    move-exception v0

    .line 271
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method
