.class Lcom/vlingo/midas/help/WhatCanISayScreen$2;
.super Ljava/lang/Object;
.source "WhatCanISayScreen.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/help/WhatCanISayScreen;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/help/WhatCanISayScreen;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$2;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 9
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 133
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v5, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$2;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    invoke-virtual {v5}, Lcom/vlingo/midas/help/WhatCanISayScreen;->getWcisData()Lcom/vlingo/midas/help/WCISData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/midas/help/WCISData;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/HashMap;

    const-string/jumbo v6, "EXTRA_CAPTION"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 134
    .local v0, "caption":Ljava/lang/String;
    if-eqz v0, :cond_0

    iget-object v5, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$2;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    sget v6, Lcom/vlingo/midas/R$string;->wcis_how_to_use_VC_caption:I

    invoke-virtual {v5, v6}, Lcom/vlingo/midas/help/WhatCanISayScreen;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 135
    iget-object v5, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$2;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    new-instance v6, Landroid/content/Intent;

    iget-object v7, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$2;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    const-class v8, Lcom/vlingo/midas/help/AboutScreen;

    invoke-direct {v6, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v5, v6}, Lcom/vlingo/midas/help/WhatCanISayScreen;->startActivity(Landroid/content/Intent;)V

    .line 137
    :cond_0
    iget-object v5, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$2;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    invoke-virtual {v5}, Lcom/vlingo/midas/help/WhatCanISayScreen;->getWcisData()Lcom/vlingo/midas/help/WCISData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/midas/help/WCISData;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/HashMap;

    const-string/jumbo v6, "EXTRA_TITLE_BAR"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_3

    .line 138
    iget-object v5, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$2;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    invoke-virtual {v5}, Lcom/vlingo/midas/help/WhatCanISayScreen;->getWcisData()Lcom/vlingo/midas/help/WCISData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/midas/help/WCISData;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/HashMap;

    const-string/jumbo v6, "EXTRA_SCREEN"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    .line 139
    .local v1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iget-object v5, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$2;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    invoke-virtual {v5}, Lcom/vlingo/midas/help/WhatCanISayScreen;->getWcisData()Lcom/vlingo/midas/help/WCISData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/midas/help/WCISData;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/HashMap;

    const-string/jumbo v6, "EXTRA_INTENT"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    .line 140
    .local v2, "i":Landroid/content/Intent;
    if-eqz v1, :cond_2

    iget-object v5, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$2;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    invoke-virtual {v5}, Lcom/vlingo/midas/help/WhatCanISayScreen;->getWcisData()Lcom/vlingo/midas/help/WCISData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/midas/help/WCISData;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/HashMap;

    const-string/jumbo v6, "EXTRA_SCREEN"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    const-class v6, Lcom/vlingo/midas/iux/UsingVlingoScreen;

    if-eq v5, v6, :cond_2

    .line 141
    new-instance v3, Landroid/content/Intent;

    iget-object v5, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$2;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    invoke-direct {v3, v5, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 142
    .local v3, "intent":Landroid/content/Intent;
    const-string/jumbo v5, "wycs.is.iux"

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 143
    iget-object v5, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$2;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    invoke-virtual {v5, v3}, Lcom/vlingo/midas/help/WhatCanISayScreen;->startActivity(Landroid/content/Intent;)V

    .line 162
    .end local v1    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "i":Landroid/content/Intent;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_0
    return-void

    .line 144
    .restart local v1    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .restart local v2    # "i":Landroid/content/Intent;
    :cond_2
    if-eqz v2, :cond_1

    .line 145
    iget-object v5, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$2;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    invoke-virtual {v5, v2}, Lcom/vlingo/midas/help/WhatCanISayScreen;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 148
    .end local v1    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "i":Landroid/content/Intent;
    :cond_3
    iget-object v5, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$2;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    invoke-virtual {v5}, Lcom/vlingo/midas/help/WhatCanISayScreen;->getWcisData()Lcom/vlingo/midas/help/WCISData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/midas/help/WCISData;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge p3, v5, :cond_4

    .line 149
    iget-object v5, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$2;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    invoke-virtual {v5}, Lcom/vlingo/midas/help/WhatCanISayScreen;->getWcisData()Lcom/vlingo/midas/help/WCISData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/midas/help/WCISData;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/HashMap;

    .line 150
    .local v4, "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v3, Landroid/content/Intent;

    iget-object v5, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$2;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    const-class v6, Lcom/vlingo/midas/help/WCISCompositionScreen;

    invoke-direct {v3, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 151
    .restart local v3    # "intent":Landroid/content/Intent;
    const-string/jumbo v6, "EXTRA_TITLE_BAR"

    const-string/jumbo v5, "EXTRA_TITLE_BAR"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v3, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 152
    const-string/jumbo v6, "EXTRA_SUBTITLE"

    const-string/jumbo v5, "EXTRA_SUBTITLE"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v3, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 153
    const-string/jumbo v6, "EXTRA_SUBTITLE_ICON"

    const-string/jumbo v5, "EXTRA_SUBTITLE_ICON"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v3, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 154
    const-string/jumbo v6, "EXTRA_EXAMPLE_LIST"

    const-string/jumbo v5, "EXTRA_EXAMPLE_LIST"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v3, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 155
    const-string/jumbo v6, "EXTRA_DID_YOU_KNOW"

    const-string/jumbo v5, "EXTRA_DID_YOU_KNOW"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v3, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 157
    iget-object v5, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$2;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    invoke-virtual {v5, v3}, Lcom/vlingo/midas/help/WhatCanISayScreen;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 160
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_4
    iget-object v5, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$2;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    new-instance v6, Landroid/content/Intent;

    iget-object v7, p0, Lcom/vlingo/midas/help/WhatCanISayScreen$2;->this$0:Lcom/vlingo/midas/help/WhatCanISayScreen;

    const-class v8, Lcom/vlingo/midas/ui/SimpleIconListScreen;

    invoke-direct {v6, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v5, v6}, Lcom/vlingo/midas/help/WhatCanISayScreen;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
