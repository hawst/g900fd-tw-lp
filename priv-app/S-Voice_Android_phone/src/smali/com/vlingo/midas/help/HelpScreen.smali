.class public Lcom/vlingo/midas/help/HelpScreen;
.super Lcom/vlingo/midas/help/WhatCanISayScreen;
.source "HelpScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/help/HelpScreen$TabletType;
    }
.end annotation


# static fields
.field public static final EXTRA_IS_IUX:Ljava/lang/String; = "wycs.is.iux"

.field private static log:Lcom/vlingo/core/internal/logging/Logger;

.field private static mTheme:I


# instance fields
.field protected currentTablet:Lcom/vlingo/midas/help/HelpScreen$TabletType;

.field private customTitleSupported:Z

.field private inIUXMode:Z

.field private lv:Landroid/widget/ListView;

.field private mDensity:F

.field private mOldLX:F

.field private mOldLY:F

.field private mOldPX:F

.field private mOldPY:F

.field private mOldX:F

.field private mOldY:F

.field private my_orientation:I

.field private prevPosition:I

.field private final settingClicked:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/vlingo/midas/help/HelpScreen;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/help/HelpScreen;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 49
    sget v0, Lcom/vlingo/midas/R$style;->CustomNoActionBar:I

    sput v0, Lcom/vlingo/midas/help/HelpScreen;->mTheme:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Lcom/vlingo/midas/help/WhatCanISayScreen;-><init>()V

    .line 45
    iput-boolean v1, p0, Lcom/vlingo/midas/help/HelpScreen;->inIUXMode:Z

    .line 50
    iput v0, p0, Lcom/vlingo/midas/help/HelpScreen;->mDensity:F

    .line 51
    iput v0, p0, Lcom/vlingo/midas/help/HelpScreen;->mOldX:F

    .line 52
    iput v0, p0, Lcom/vlingo/midas/help/HelpScreen;->mOldY:F

    .line 53
    iput-boolean v1, p0, Lcom/vlingo/midas/help/HelpScreen;->customTitleSupported:Z

    .line 57
    iput v0, p0, Lcom/vlingo/midas/help/HelpScreen;->mOldPX:F

    .line 58
    iput v0, p0, Lcom/vlingo/midas/help/HelpScreen;->mOldPY:F

    .line 60
    iput v0, p0, Lcom/vlingo/midas/help/HelpScreen;->mOldLX:F

    .line 61
    iput v0, p0, Lcom/vlingo/midas/help/HelpScreen;->mOldLY:F

    .line 67
    sget-object v0, Lcom/vlingo/midas/help/HelpScreen$TabletType;->OTHERS:Lcom/vlingo/midas/help/HelpScreen$TabletType;

    iput-object v0, p0, Lcom/vlingo/midas/help/HelpScreen;->currentTablet:Lcom/vlingo/midas/help/HelpScreen$TabletType;

    .line 259
    new-instance v0, Lcom/vlingo/midas/help/HelpScreen$2;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/help/HelpScreen$2;-><init>(Lcom/vlingo/midas/help/HelpScreen;)V

    iput-object v0, p0, Lcom/vlingo/midas/help/HelpScreen;->settingClicked:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method protected addItems()V
    .locals 3

    .prologue
    .line 279
    iget-boolean v0, p0, Lcom/vlingo/midas/help/HelpScreen;->inIUXMode:Z

    if-nez v0, :cond_0

    .line 280
    invoke-virtual {p0}, Lcom/vlingo/midas/help/HelpScreen;->getWcisData()Lcom/vlingo/midas/help/WCISData;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/midas/help/HelpScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->wcis_general_help:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/help/WCISData;->addSubHeading(Ljava/lang/String;)V

    .line 281
    invoke-virtual {p0}, Lcom/vlingo/midas/help/HelpScreen;->getWcisData()Lcom/vlingo/midas/help/WCISData;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/midas/help/HelpScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->what_can_i_say:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/help/WCISData;->addSubHeading(Ljava/lang/String;)V

    .line 283
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/help/HelpScreen;->getWcisData()Lcom/vlingo/midas/help/WCISData;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vlingo/midas/help/WCISData;->addItems(Landroid/content/Context;)V

    .line 284
    return-void
.end method

.method protected init()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 134
    invoke-virtual {p0, v4}, Lcom/vlingo/midas/help/HelpScreen;->requestWindowFeature(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/vlingo/midas/help/HelpScreen;->customTitleSupported:Z

    .line 136
    sget v2, Lcom/vlingo/midas/R$layout;->wcis_top:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/help/HelpScreen;->setContentView(I)V

    .line 140
    invoke-virtual {p0}, Lcom/vlingo/midas/help/HelpScreen;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 147
    .local v1, "titlebar":Landroid/app/ActionBar;
    if-eqz v1, :cond_0

    .line 148
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 149
    invoke-virtual {v1, v4}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 150
    sget v2, Lcom/vlingo/midas/R$string;->help:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/help/HelpScreen;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 163
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/midas/help/HelpScreen;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "wycs.show.done"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/vlingo/midas/help/HelpScreen;->inIUXMode:Z

    .line 165
    sget v2, Lcom/vlingo/midas/R$id;->wycs_top_list:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/help/HelpScreen;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/vlingo/midas/help/HelpScreen;->lv:Landroid/widget/ListView;

    .line 166
    invoke-virtual {p0}, Lcom/vlingo/midas/help/HelpScreen;->addItems()V

    .line 168
    new-instance v0, Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;-><init>(Lcom/vlingo/midas/help/WhatCanISayScreen;)V

    .line 169
    .local v0, "adapter":Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;
    iget-object v2, p0, Lcom/vlingo/midas/help/HelpScreen;->lv:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 170
    iget-object v2, p0, Lcom/vlingo/midas/help/HelpScreen;->lv:Landroid/widget/ListView;

    invoke-virtual {v2, v6}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 171
    iget-object v2, p0, Lcom/vlingo/midas/help/HelpScreen;->lv:Landroid/widget/ListView;

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setFocusableInTouchMode(Z)V

    .line 220
    iget-object v2, p0, Lcom/vlingo/midas/help/HelpScreen;->lv:Landroid/widget/ListView;

    new-instance v3, Lcom/vlingo/midas/help/HelpScreen$1;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/help/HelpScreen$1;-><init>(Lcom/vlingo/midas/help/HelpScreen;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 249
    invoke-virtual {p0}, Lcom/vlingo/midas/help/HelpScreen;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    invoke-virtual {p0}, Lcom/vlingo/midas/help/HelpScreen;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    .line 254
    :cond_1
    invoke-virtual {p0, v6}, Lcom/vlingo/midas/help/HelpScreen;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 256
    :cond_2
    return-void

    .line 152
    .end local v0    # "adapter":Lcom/vlingo/midas/help/WhatCanISayScreen$RowAdapter;
    :cond_3
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 153
    invoke-virtual {p0}, Lcom/vlingo/midas/help/HelpScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->tw_ab_transparent_dark_holo:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 155
    invoke-virtual {v1, v4}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    goto :goto_0

    .line 158
    :cond_4
    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 159
    sget v2, Lcom/vlingo/midas/R$string;->help:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/help/HelpScreen;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected isInIUXMode()Z
    .locals 1

    .prologue
    .line 322
    iget-boolean v0, p0, Lcom/vlingo/midas/help/HelpScreen;->inIUXMode:Z

    if-eqz v0, :cond_0

    .line 323
    const/4 v0, 0x1

    .line 325
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 317
    invoke-super {p0}, Lcom/vlingo/midas/help/WhatCanISayScreen;->onBackPressed()V

    .line 318
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 288
    invoke-super {p0, p1}, Lcom/vlingo/midas/help/WhatCanISayScreen;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 289
    sget v2, Lcom/vlingo/midas/R$id;->left_container:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/help/HelpScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 290
    .local v0, "leftContainer":Landroid/widget/RelativeLayout;
    sget v2, Lcom/vlingo/midas/R$id;->right_container:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/help/HelpScreen;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 291
    .local v1, "rightContainer":Landroid/widget/RelativeLayout;
    if-eqz p1, :cond_0

    .line 292
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 293
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 294
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 300
    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 301
    return-void

    .line 295
    :cond_1
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 296
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 297
    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/vlingo/midas/help/HelpScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    const/4 v9, 0x1

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/help/HelpScreen;->setRequestedOrientation(I)V

    .line 79
    :cond_0
    new-instance v4, Landroid/util/DisplayMetrics;

    invoke-direct {v4}, Landroid/util/DisplayMetrics;-><init>()V

    .line 80
    .local v4, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/vlingo/midas/help/HelpScreen;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v9

    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    invoke-virtual {v9, v4}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 81
    iget v9, v4, Landroid/util/DisplayMetrics;->density:F

    iput v9, p0, Lcom/vlingo/midas/help/HelpScreen;->mDensity:F

    .line 83
    iget v9, v4, Landroid/util/DisplayMetrics;->density:F

    float-to-double v9, v9

    const-wide/high16 v11, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v9, v11

    const-wide/16 v11, 0x0

    cmpl-double v9, v9, v11

    if-nez v9, :cond_1

    iget v9, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v10, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    add-int/2addr v9, v10

    add-int/lit16 v9, v9, -0x7f0

    if-nez v9, :cond_1

    iget v9, v4, Landroid/util/DisplayMetrics;->xdpi:F

    const v10, 0x4315d32c

    sub-float/2addr v9, v10

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-nez v9, :cond_1

    iget v9, v4, Landroid/util/DisplayMetrics;->ydpi:F

    const v10, 0x431684be

    sub-float/2addr v9, v10

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-eqz v9, :cond_2

    :cond_1
    iget v9, v4, Landroid/util/DisplayMetrics;->density:F

    float-to-double v9, v9

    const-wide/high16 v11, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v9, v11

    const-wide/16 v11, 0x0

    cmpl-double v9, v9, v11

    if-nez v9, :cond_4

    iget v9, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v10, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    add-int/2addr v9, v10

    add-int/lit16 v9, v9, -0x820

    if-nez v9, :cond_4

    iget v9, v4, Landroid/util/DisplayMetrics;->xdpi:F

    const v10, 0x4315d2f2

    sub-float/2addr v9, v10

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-nez v9, :cond_4

    iget v9, v4, Landroid/util/DisplayMetrics;->ydpi:F

    const v10, 0x4316849c

    sub-float/2addr v9, v10

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-nez v9, :cond_4

    .line 89
    :cond_2
    sget v9, Lcom/vlingo/midas/R$style;->DialogSlideAnim:I

    sput v9, Lcom/vlingo/midas/help/HelpScreen;->mTheme:I

    .line 90
    sget v9, Lcom/vlingo/midas/help/HelpScreen;->mTheme:I

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/help/HelpScreen;->setTheme(I)V

    .line 95
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 96
    sget v9, Lcom/vlingo/midas/R$style;->MidasBlackThemeSettings:I

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/help/HelpScreen;->setTheme(I)V

    .line 103
    :cond_3
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/help/HelpScreen;->getWindow()Landroid/view/Window;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 104
    .local v3, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string/jumbo v10, "privateFlags"

    invoke-virtual {v9, v10}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    .line 105
    .local v5, "privateFlags":Ljava/lang/reflect/Field;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string/jumbo v10, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual {v9, v10}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v6

    .line 106
    .local v6, "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string/jumbo v10, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual {v9, v10}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 108
    .local v1, "disableMultiTray":Ljava/lang/reflect/Field;
    const/4 v0, 0x0

    .local v0, "currentPrivateFlags":I
    const/4 v8, 0x0

    .local v8, "valueofFlagsEnableStatusBar":I
    const/4 v7, 0x0

    .line 109
    .local v7, "valueofFlagsDisableTray":I
    invoke-virtual {v5, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    .line 110
    invoke-virtual {v6, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v8

    .line 111
    invoke-virtual {v1, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v7

    .line 113
    or-int/2addr v0, v8

    .line 114
    or-int/2addr v0, v7

    .line 116
    invoke-virtual {v5, v3, v0}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 117
    invoke-virtual {p0}, Lcom/vlingo/midas/help/HelpScreen;->getWindow()Landroid/view/Window;

    move-result-object v9

    invoke-virtual {v9, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    .end local v0    # "currentPrivateFlags":I
    .end local v1    # "disableMultiTray":Ljava/lang/reflect/Field;
    .end local v3    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v5    # "privateFlags":Ljava/lang/reflect/Field;
    .end local v6    # "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    .end local v7    # "valueofFlagsDisableTray":I
    .end local v8    # "valueofFlagsEnableStatusBar":I
    :goto_1
    invoke-super {p0, p1}, Lcom/vlingo/midas/help/WhatCanISayScreen;->onCreate(Landroid/os/Bundle;)V

    .line 129
    return-void

    .line 92
    :cond_4
    sget v9, Lcom/vlingo/midas/R$style;->CustomNoActionBar:I

    sput v9, Lcom/vlingo/midas/help/HelpScreen;->mTheme:I

    goto :goto_0

    .line 121
    :catch_0
    move-exception v2

    .line 124
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 118
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v9

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 268
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 272
    :goto_0
    invoke-super {p0, p1}, Lcom/vlingo/midas/help/WhatCanISayScreen;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 270
    :pswitch_0
    invoke-virtual {p0}, Lcom/vlingo/midas/help/HelpScreen;->finish()V

    goto :goto_0

    .line 268
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 306
    invoke-super {p0}, Lcom/vlingo/midas/help/WhatCanISayScreen;->onResume()V

    .line 308
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 309
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.client.app.action.ACTIVITY_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 310
    const-string/jumbo v1, "com.vlingo.client.app.extra.STATE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 311
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/help/HelpScreen;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 312
    return-void
.end method
