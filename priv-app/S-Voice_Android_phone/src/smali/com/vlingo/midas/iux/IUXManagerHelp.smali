.class public Lcom/vlingo/midas/iux/IUXManagerHelp;
.super Ljava/lang/Object;
.source "IUXManagerHelp.java"


# static fields
.field public static EXTRA_ONLY_TOS:Ljava/lang/String;

.field public static EXTRA_PENDING_INTENT:Ljava/lang/String;

.field public static startActivity:Landroid/app/Activity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-string/jumbo v0, "com.vlingo.client.iux.pending_intent"

    sput-object v0, Lcom/vlingo/midas/iux/IUXManagerHelp;->EXTRA_PENDING_INTENT:Ljava/lang/String;

    .line 22
    const-string/jumbo v0, "com.vlingo.client.iux.only_tos"

    sput-object v0, Lcom/vlingo/midas/iux/IUXManagerHelp;->EXTRA_ONLY_TOS:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static finishIUX(Landroid/app/Activity;)V
    .locals 3
    .param p0, "parentActivity"    # Landroid/app/Activity;

    .prologue
    .line 56
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManagerHelp;->isIUXComplete()Z

    move-result v2

    if-nez v2, :cond_0

    .line 59
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/vlingo/midas/iux/IUXManagerHelp;->setIUXComplete(Z)V

    .line 60
    invoke-static {}, Lcom/vlingo/core/internal/util/SayHello;->sendHello()Z

    move-result v1

    .line 61
    .local v1, "sentHello":Z
    if-nez v1, :cond_0

    .line 71
    .end local v1    # "sentHello":Z
    :cond_0
    sget-object v2, Lcom/vlingo/midas/iux/IUXManagerHelp;->startActivity:Landroid/app/Activity;

    instance-of v2, v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    if-eqz v2, :cond_1

    .line 72
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 73
    .local v0, "i":Landroid/content/Intent;
    const/high16 v2, 0x4000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 74
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 75
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 77
    .end local v0    # "i":Landroid/content/Intent;
    :cond_1
    return-void
.end method

.method public static isIUXComplete()Z
    .locals 2

    .prologue
    .line 30
    const-string/jumbo v0, "iux_complete"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isIUXIntroRequired()Z
    .locals 2

    .prologue
    .line 42
    const-string/jumbo v0, "car_iux_intro_required"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isTOSAccepted()Z
    .locals 2

    .prologue
    .line 26
    const-string/jumbo v0, "tos_accepted"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static processIUX(Landroid/app/Activity;)V
    .locals 0
    .param p0, "parentActivity"    # Landroid/app/Activity;

    .prologue
    .line 50
    invoke-static {p0}, Lcom/vlingo/midas/iux/IUXManagerHelp;->finishIUX(Landroid/app/Activity;)V

    .line 51
    return-void
.end method

.method public static requiresIUX()Z
    .locals 1

    .prologue
    .line 46
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManagerHelp;->isIUXIntroRequired()Z

    move-result v0

    return v0
.end method

.method public static setIUXComplete(Z)V
    .locals 1
    .param p0, "complete"    # Z

    .prologue
    .line 34
    const-string/jumbo v0, "iux_complete"

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 35
    return-void
.end method

.method public static setIUXIntroRequired(Z)V
    .locals 1
    .param p0, "b"    # Z

    .prologue
    .line 38
    const-string/jumbo v0, "car_iux_intro_required"

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 39
    return-void
.end method
