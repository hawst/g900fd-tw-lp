.class public Lcom/vlingo/midas/iux/IUXManager;
.super Ljava/lang/Object;
.source "IUXManager.java"


# static fields
.field public static EXTRA_ONLY_TOS:Ljava/lang/String;

.field public static EXTRA_PENDING_INTENT:Ljava/lang/String;

.field private static isProcessing:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/iux/IUXManager;->isProcessing:Z

    .line 25
    const-string/jumbo v0, "com.vlingo.client.iux.pending_intent"

    sput-object v0, Lcom/vlingo/midas/iux/IUXManager;->EXTRA_PENDING_INTENT:Ljava/lang/String;

    .line 26
    const-string/jumbo v0, "com.vlingo.client.iux.only_tos"

    sput-object v0, Lcom/vlingo/midas/iux/IUXManager;->EXTRA_ONLY_TOS:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static finishIUX(Landroid/app/Activity;)V
    .locals 3
    .param p0, "parentActivity"    # Landroid/app/Activity;

    .prologue
    .line 80
    if-eqz p0, :cond_0

    .line 83
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getMainActivityClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 84
    .local v0, "i":Landroid/content/Intent;
    sget-object v1, Lcom/vlingo/midas/gui/DataCheckActivity;->DIALOG_PROCESSED:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 85
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->setShowAgainBackgroundData(Z)V

    .line 86
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 87
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 89
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public static isIUXComplete()Z
    .locals 2

    .prologue
    .line 33
    const-string/jumbo v0, "iux_complete"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isIUXIntroRequired()Z
    .locals 2

    .prologue
    .line 45
    const-string/jumbo v0, "car_iux_intro_required"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isProcessing()Z
    .locals 1

    .prologue
    .line 73
    sget-boolean v0, Lcom/vlingo/midas/iux/IUXManager;->isProcessing:Z

    return v0
.end method

.method public static isTOSAccepted()Z
    .locals 2

    .prologue
    .line 29
    const-string/jumbo v0, "tos_accepted"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static processIUX(Landroid/app/Activity;Z)V
    .locals 3
    .param p0, "parentActivity"    # Landroid/app/Activity;
    .param p1, "exitAfterTermsOfService"    # Z

    .prologue
    const/4 v2, 0x1

    .line 53
    sput-boolean v2, Lcom/vlingo/midas/iux/IUXManager;->isProcessing:Z

    .line 57
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 58
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 60
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 61
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 62
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isIUXComplete()Z

    move-result v1

    if-nez v1, :cond_0

    .line 63
    invoke-static {v2}, Lcom/vlingo/midas/iux/IUXManager;->setIUXComplete(Z)V

    .line 70
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    invoke-static {p0}, Lcom/vlingo/midas/iux/IUXManager;->finishIUX(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public static requiresIUX()Z
    .locals 1

    .prologue
    .line 49
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isIUXIntroRequired()Z

    move-result v0

    return v0
.end method

.method public static setIUXComplete(Z)V
    .locals 1
    .param p0, "complete"    # Z

    .prologue
    .line 37
    const-string/jumbo v0, "iux_complete"

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 38
    return-void
.end method

.method public static setIUXIntroRequired(Z)V
    .locals 1
    .param p0, "b"    # Z

    .prologue
    .line 41
    const-string/jumbo v0, "car_iux_intro_required"

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 42
    return-void
.end method
