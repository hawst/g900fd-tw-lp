.class public Lcom/vlingo/midas/iux/UsingVlingoScreen;
.super Lcom/vlingo/midas/iux/IUXBaseActivity;
.source "UsingVlingoScreen.java"


# static fields
.field public static final EXTRA_IS_IUX:Ljava/lang/String; = "wycs.is.iux"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/vlingo/midas/iux/IUXBaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method applyHtml(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 60
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/iux/UsingVlingoScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 61
    .local v0, "tv":Landroid/widget/TextView;
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 26
    invoke-super {p0, p1}, Lcom/vlingo/midas/iux/IUXBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v0

    const-string/jumbo v1, "setup-usingvlingo"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->helpPageViewed(Ljava/lang/String;)V

    .line 57
    return-void
.end method
