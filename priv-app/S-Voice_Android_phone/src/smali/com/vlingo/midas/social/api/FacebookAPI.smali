.class public Lcom/vlingo/midas/social/api/FacebookAPI;
.super Lcom/vlingo/midas/social/api/SocialAPI;
.source "FacebookAPI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;,
        Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;,
        Lcom/vlingo/midas/social/api/FacebookAPI$LoginDialogListener;
    }
.end annotation


# static fields
.field private static final ACCEPTED_TEXT_TAG_STRING:Ljava/lang/String; = "facebook:status"

.field private static final APP_ID:Ljava/lang/String;

.field public static final ERROR_FACEBOOK:I = 0x2

.field public static final ERROR_FILE_NOT_FOUND:I = 0x3

.field public static final ERROR_IO_EXCEPTION:I = 0x4

.field public static final ERROR_JSON:I = 0x1

.field public static final ERROR_MALFORMED_URL:I = 0x5

.field public static final FACEBOOK_ACCOUNT_TYPE:Ljava/lang/String; = "com.facebook.auth.login"

.field private static final FB_ID:Ljava/lang/String; = "147940453810"

.field private static final INTENT_STRING:Ljava/lang/String; = "facebook"

.field public static final ON_FACEBOOK_ERROR:Ljava/lang/String; = "on_facebook_error"

.field private static final PERMISSIONS:[Ljava/lang/String;

.field public static final SAMSUNG_FB_ACCOUNT_TYPE:Ljava/lang/String; = "com.sec.android.app.sns3.facebook"

.field public static final TYPE_INTENT:I = 0x8


# instance fields
.field private volatile callback:Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;

.field private final fb:Lcom/facebook/android/Facebook;

.field private final runner:Lcom/facebook/android/AsyncFacebookRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 49
    const-string/jumbo v0, "facebook_app_id"

    const-string/jumbo v1, "39010226174"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/social/api/FacebookAPI;->APP_ID:Ljava/lang/String;

    .line 51
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "publish_stream"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "offline_access"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/midas/social/api/FacebookAPI;->PERMISSIONS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;)V
    .locals 19
    .param p1, "callback"    # Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;

    .prologue
    .line 66
    const-string/jumbo v3, "facebook_account"

    const-string/jumbo v4, "facebook_picture_url"

    const-string/jumbo v5, "facebook"

    const/4 v6, -0x1

    const/4 v7, -0x1

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v8, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_facebook_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v8}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$string;)I

    move-result v8

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v9, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_facebook:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v9}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$string;)I

    move-result v9

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v10, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_wcis_social_facebook:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v10}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$string;)I

    move-result v10

    const/4 v11, -0x1

    const/4 v12, -0x1

    const/16 v13, 0x8

    const/4 v14, -0x1

    const v15, 0x7fffffff

    const/16 v16, 0x1

    const/16 v17, 0x1

    const-string/jumbo v18, "facebook:status"

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v18}, Lcom/vlingo/midas/social/api/SocialAPI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIIIIIIIZZLjava/lang/String;)V

    .line 72
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/social/api/FacebookAPI;->callback:Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;

    .line 73
    new-instance v2, Lcom/facebook/android/Facebook;

    sget-object v3, Lcom/vlingo/midas/social/api/FacebookAPI;->APP_ID:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/facebook/android/Facebook;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/vlingo/midas/social/api/FacebookAPI;->fb:Lcom/facebook/android/Facebook;

    .line 74
    new-instance v2, Lcom/facebook/android/AsyncFacebookRunner;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/midas/social/api/FacebookAPI;->fb:Lcom/facebook/android/Facebook;

    invoke-direct {v2, v3}, Lcom/facebook/android/AsyncFacebookRunner;-><init>(Lcom/facebook/android/Facebook;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/vlingo/midas/social/api/FacebookAPI;->runner:Lcom/facebook/android/AsyncFacebookRunner;

    .line 75
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/social/api/FacebookAPI;->refreshCredentials()V

    .line 76
    return-void
.end method

.method static synthetic access$300(Lcom/vlingo/midas/social/api/FacebookAPI;)Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/FacebookAPI;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vlingo/midas/social/api/FacebookAPI;->callback:Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;

    return-object v0
.end method

.method static synthetic access$400(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Lcom/facebook/android/FacebookError;
        }
    .end annotation

    .prologue
    .line 29
    invoke-static {p0}, Lcom/vlingo/midas/social/api/FacebookAPI;->parseJson(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method private static parseJson(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 6
    .param p0, "response"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Lcom/facebook/android/FacebookError;
        }
    .end annotation

    .prologue
    .line 409
    const-string/jumbo v2, "false"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 410
    new-instance v2, Lcom/facebook/android/FacebookError;

    const-string/jumbo v3, "request failed"

    invoke-direct {v2, v3}, Lcom/facebook/android/FacebookError;-><init>(Ljava/lang/String;)V

    throw v2

    .line 412
    :cond_0
    const-string/jumbo v2, "true"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 413
    const-string/jumbo p0, "{value : true}"

    .line 415
    :cond_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 419
    .local v1, "json":Lorg/json/JSONObject;
    const-string/jumbo v2, "error"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 420
    const-string/jumbo v2, "error"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 421
    .local v0, "error":Lorg/json/JSONObject;
    new-instance v2, Lcom/facebook/android/FacebookError;

    const-string/jumbo v3, "message"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "type"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, Lcom/facebook/android/FacebookError;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    throw v2

    .line 423
    .end local v0    # "error":Lorg/json/JSONObject;
    :cond_2
    const-string/jumbo v2, "error_code"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string/jumbo v2, "error_msg"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 424
    new-instance v2, Lcom/facebook/android/FacebookError;

    const-string/jumbo v3, "error_msg"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, ""

    const-string/jumbo v5, "error_code"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-direct {v2, v3, v4, v5}, Lcom/facebook/android/FacebookError;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    throw v2

    .line 426
    :cond_3
    const-string/jumbo v2, "error_code"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 427
    new-instance v2, Lcom/facebook/android/FacebookError;

    const-string/jumbo v3, "request failed"

    const-string/jumbo v4, ""

    const-string/jumbo v5, "error_code"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-direct {v2, v3, v4, v5}, Lcom/facebook/android/FacebookError;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    throw v2

    .line 429
    :cond_4
    const-string/jumbo v2, "error_msg"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 430
    new-instance v2, Lcom/facebook/android/FacebookError;

    const-string/jumbo v3, "error_msg"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/android/FacebookError;-><init>(Ljava/lang/String;)V

    throw v2

    .line 432
    :cond_5
    const-string/jumbo v2, "error_reason"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 433
    new-instance v2, Lcom/facebook/android/FacebookError;

    const-string/jumbo v3, "error_reason"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/android/FacebookError;-><init>(Ljava/lang/String;)V

    throw v2

    .line 435
    :cond_6
    return-object v1
.end method


# virtual methods
.method public authorizeCallback(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/vlingo/midas/social/api/FacebookAPI;->fb:Lcom/facebook/android/Facebook;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/android/Facebook;->authorizeCallback(IILandroid/content/Intent;)V

    .line 162
    return-void
.end method

.method public clearCredentials()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 93
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/android/Util;->clearCookies(Landroid/content/Context;)V

    .line 94
    const-string/jumbo v0, "facebook_account_name"

    invoke-static {v0, v3}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const-string/jumbo v0, "facebook_token"

    invoke-static {v0, v3}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string/jumbo v0, "facebook_expires"

    const-wide/16 v1, 0x0

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Ljava/lang/String;J)V

    .line 97
    const-string/jumbo v0, "facebook_account"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 98
    const-string/jumbo v0, "facebook_picture"

    invoke-static {v0, v3}, Lcom/vlingo/core/internal/settings/Settings;->setImage(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 99
    const-string/jumbo v0, "facebook_picture_url"

    invoke-static {v0, v3}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method public dismissDialogs()V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/vlingo/midas/social/api/FacebookAPI;->fb:Lcom/facebook/android/Facebook;

    invoke-virtual {v0}, Lcom/facebook/android/Facebook;->dismissDialogs()V

    .line 145
    return-void
.end method

.method public fetchUserData()V
    .locals 6

    .prologue
    .line 208
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 209
    .local v0, "parameters":Landroid/os/Bundle;
    const-string/jumbo v1, "fields"

    const-string/jumbo v2, "picture,name"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    iget-object v1, p0, Lcom/vlingo/midas/social/api/FacebookAPI;->runner:Lcom/facebook/android/AsyncFacebookRunner;

    const-string/jumbo v2, "me"

    new-instance v3, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;

    const-string/jumbo v4, "me"

    const/4 v5, 0x0

    invoke-direct {v3, p0, v4, v5}, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;-><init>(Lcom/vlingo/midas/social/api/FacebookAPI;Ljava/lang/String;Lcom/vlingo/midas/social/api/FacebookAPI$1;)V

    invoke-virtual {v1, v2, v0, v3}, Lcom/facebook/android/AsyncFacebookRunner;->request(Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/android/AsyncFacebookRunner$RequestListener;)V

    .line 211
    return-void
.end method

.method public findUserIDWithName(Ljava/lang/String;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 178
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 179
    .local v0, "parameters":Landroid/os/Bundle;
    const-string/jumbo v1, "fields"

    const-string/jumbo v2, "friends"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    iget-object v1, p0, Lcom/vlingo/midas/social/api/FacebookAPI;->runner:Lcom/facebook/android/AsyncFacebookRunner;

    const-string/jumbo v2, "me"

    new-instance v3, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;

    const-string/jumbo v4, "finduserid"

    const/4 v5, 0x0

    invoke-direct {v3, p0, v4, p1, v5}, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;-><init>(Lcom/vlingo/midas/social/api/FacebookAPI;Ljava/lang/String;Ljava/lang/Object;Lcom/vlingo/midas/social/api/FacebookAPI$1;)V

    invoke-virtual {v1, v2, v0, v3}, Lcom/facebook/android/AsyncFacebookRunner;->request(Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/android/AsyncFacebookRunner$RequestListener;)V

    .line 181
    return-void
.end method

.method public findUserIDWithNames([Ljava/lang/String;)V
    .locals 6
    .param p1, "names"    # [Ljava/lang/String;

    .prologue
    .line 184
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 185
    .local v0, "parameters":Landroid/os/Bundle;
    const-string/jumbo v1, "fields"

    const-string/jumbo v2, "friends"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v1, p0, Lcom/vlingo/midas/social/api/FacebookAPI;->runner:Lcom/facebook/android/AsyncFacebookRunner;

    const-string/jumbo v2, "me"

    new-instance v3, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;

    const-string/jumbo v4, "finduserid"

    const/4 v5, 0x0

    invoke-direct {v3, p0, v4, p1, v5}, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;-><init>(Lcom/vlingo/midas/social/api/FacebookAPI;Ljava/lang/String;Ljava/lang/Object;Lcom/vlingo/midas/social/api/FacebookAPI$1;)V

    invoke-virtual {v1, v2, v0, v3}, Lcom/facebook/android/AsyncFacebookRunner;->request(Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/android/AsyncFacebookRunner$RequestListener;)V

    .line 187
    return-void
.end method

.method public getDialog()Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/vlingo/midas/social/api/FacebookAPI;->fb:Lcom/facebook/android/Facebook;

    invoke-virtual {v0}, Lcom/facebook/android/Facebook;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public getFriends()V
    .locals 6

    .prologue
    .line 172
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 173
    .local v0, "parameters":Landroid/os/Bundle;
    const-string/jumbo v1, "fields"

    const-string/jumbo v2, "friends"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    iget-object v1, p0, Lcom/vlingo/midas/social/api/FacebookAPI;->runner:Lcom/facebook/android/AsyncFacebookRunner;

    const-string/jumbo v2, "me"

    new-instance v3, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;

    const-string/jumbo v4, "me.friends"

    const/4 v5, 0x0

    invoke-direct {v3, p0, v4, v5}, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;-><init>(Lcom/vlingo/midas/social/api/FacebookAPI;Ljava/lang/String;Lcom/vlingo/midas/social/api/FacebookAPI$1;)V

    invoke-virtual {v1, v2, v0, v3}, Lcom/facebook/android/AsyncFacebookRunner;->request(Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/android/AsyncFacebookRunner$RequestListener;)V

    .line 175
    return-void
.end method

.method public isLoggedIn()Z
    .locals 2

    .prologue
    .line 258
    const-string/jumbo v0, "facebook_account"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public likeVlingo()V
    .locals 8

    .prologue
    .line 200
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 202
    .local v0, "parameters":Landroid/os/Bundle;
    const-string/jumbo v1, ""

    .line 203
    .local v1, "suffix":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/midas/social/api/FacebookAPI;->runner:Lcom/facebook/android/AsyncFacebookRunner;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "147940453810/likes"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "POST"

    new-instance v5, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;

    const-string/jumbo v6, "like_vlingo"

    const/4 v7, 0x0

    invoke-direct {v5, p0, v6, v7}, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;-><init>(Lcom/vlingo/midas/social/api/FacebookAPI;Ljava/lang/String;Lcom/vlingo/midas/social/api/FacebookAPI$1;)V

    invoke-virtual {v2, v3, v0, v4, v5}, Lcom/facebook/android/AsyncFacebookRunner;->request(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Lcom/facebook/android/AsyncFacebookRunner$RequestListener;)V

    .line 205
    return-void
.end method

.method public login(Landroid/app/Activity;)Z
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/vlingo/midas/social/api/FacebookAPI;->fb:Lcom/facebook/android/Facebook;

    sget-object v1, Lcom/vlingo/midas/social/api/FacebookAPI;->PERMISSIONS:[Ljava/lang/String;

    new-instance v2, Lcom/vlingo/midas/social/api/FacebookAPI$LoginDialogListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/vlingo/midas/social/api/FacebookAPI$LoginDialogListener;-><init>(Lcom/vlingo/midas/social/api/FacebookAPI;Lcom/vlingo/midas/social/api/FacebookAPI$1;)V

    invoke-virtual {v0, p1, v1, v2}, Lcom/facebook/android/Facebook;->authorize(Landroid/app/Activity;[Ljava/lang/String;Lcom/facebook/android/Facebook$DialogListener;)V

    .line 129
    const/4 v0, 0x1

    return v0
.end method

.method public refreshCredentials()V
    .locals 4

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/FacebookAPI;->isLoggedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/vlingo/midas/social/api/FacebookAPI;->fb:Lcom/facebook/android/Facebook;

    const-string/jumbo v1, "facebook_token"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/android/Facebook;->setAccessToken(Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/vlingo/midas/social/api/FacebookAPI;->fb:Lcom/facebook/android/Facebook;

    const-string/jumbo v1, "facebook_expires"

    const-wide/16 v2, 0x0

    invoke-static {v1, v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/facebook/android/Facebook;->setAccessExpires(J)V

    .line 90
    :cond_0
    return-void
.end method

.method public save()V
    .locals 3

    .prologue
    .line 79
    const-string/jumbo v0, "facebook_token"

    iget-object v1, p0, Lcom/vlingo/midas/social/api/FacebookAPI;->fb:Lcom/facebook/android/Facebook;

    invoke-virtual {v1}, Lcom/facebook/android/Facebook;->getAccessToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string/jumbo v0, "facebook_expires"

    iget-object v1, p0, Lcom/vlingo/midas/social/api/FacebookAPI;->fb:Lcom/facebook/android/Facebook;

    invoke-virtual {v1}, Lcom/facebook/android/Facebook;->getAccessExpires()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Ljava/lang/String;J)V

    .line 81
    const-string/jumbo v0, "facebook_account"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 82
    return-void
.end method

.method public sendMesasge(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "userID"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 191
    return-void
.end method

.method public sendWallPost(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "userID"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 194
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 195
    .local v0, "parameters":Landroid/os/Bundle;
    const-string/jumbo v1, "message"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    iget-object v1, p0, Lcom/vlingo/midas/social/api/FacebookAPI;->runner:Lcom/facebook/android/AsyncFacebookRunner;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "/feed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "POST"

    new-instance v4, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;

    const-string/jumbo v5, "wallpost"

    const/4 v6, 0x0

    invoke-direct {v4, p0, v5, v6}, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;-><init>(Lcom/vlingo/midas/social/api/FacebookAPI;Ljava/lang/String;Lcom/vlingo/midas/social/api/FacebookAPI$1;)V

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/facebook/android/AsyncFacebookRunner;->request(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Lcom/facebook/android/AsyncFacebookRunner$RequestListener;)V

    .line 197
    return-void
.end method

.method public showDialogs()V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/vlingo/midas/social/api/FacebookAPI;->fb:Lcom/facebook/android/Facebook;

    invoke-virtual {v0}, Lcom/facebook/android/Facebook;->showDialogs()V

    .line 152
    return-void
.end method

.method public shutdown()V
    .locals 1

    .prologue
    .line 109
    new-instance v0, Lcom/vlingo/midas/social/api/FacebookAPI$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/social/api/FacebookAPI$1;-><init>(Lcom/vlingo/midas/social/api/FacebookAPI;)V

    iput-object v0, p0, Lcom/vlingo/midas/social/api/FacebookAPI;->callback:Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;

    .line 125
    return-void
.end method

.method public updateDialog()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/vlingo/midas/social/api/FacebookAPI;->fb:Lcom/facebook/android/Facebook;

    invoke-virtual {v0}, Lcom/facebook/android/Facebook;->updateDialog()V

    .line 138
    return-void
.end method

.method public updateDialogs()V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/vlingo/midas/social/api/FacebookAPI;->fb:Lcom/facebook/android/Facebook;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/vlingo/midas/social/api/FacebookAPI;->fb:Lcom/facebook/android/Facebook;

    invoke-virtual {v0}, Lcom/facebook/android/Facebook;->screenOrientationChange()V

    .line 158
    :cond_0
    return-void
.end method

.method public updateStatus(Ljava/lang/String;)V
    .locals 7
    .param p1, "status"    # Ljava/lang/String;

    .prologue
    .line 165
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 166
    .local v0, "parameters":Landroid/os/Bundle;
    const-string/jumbo v1, "message"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    iget-object v1, p0, Lcom/vlingo/midas/social/api/FacebookAPI;->runner:Lcom/facebook/android/AsyncFacebookRunner;

    const-string/jumbo v2, "me/feed"

    const-string/jumbo v3, "POST"

    new-instance v4, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;

    const-string/jumbo v5, "stream.publish"

    const/4 v6, 0x0

    invoke-direct {v4, p0, v5, v6}, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;-><init>(Lcom/vlingo/midas/social/api/FacebookAPI;Ljava/lang/String;Lcom/vlingo/midas/social/api/FacebookAPI$1;)V

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/facebook/android/AsyncFacebookRunner;->request(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Lcom/facebook/android/AsyncFacebookRunner$RequestListener;)V

    .line 169
    return-void
.end method
