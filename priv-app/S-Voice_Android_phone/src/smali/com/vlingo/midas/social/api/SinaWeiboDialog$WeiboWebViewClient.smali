.class Lcom/vlingo/midas/social/api/SinaWeiboDialog$WeiboWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "SinaWeiboDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/social/api/SinaWeiboDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WeiboWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/social/api/SinaWeiboDialog;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/social/api/SinaWeiboDialog;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog$WeiboWebViewClient;->this$0:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/social/api/SinaWeiboDialog;Lcom/vlingo/midas/social/api/SinaWeiboDialog$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/social/api/SinaWeiboDialog;
    .param p2, "x1"    # Lcom/vlingo/midas/social/api/SinaWeiboDialog$1;

    .prologue
    .line 188
    invoke-direct {p0, p1}, Lcom/vlingo/midas/social/api/SinaWeiboDialog$WeiboWebViewClient;-><init>(Lcom/vlingo/midas/social/api/SinaWeiboDialog;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 232
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 235
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog$WeiboWebViewClient;->this$0:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    # invokes: Lcom/vlingo/midas/social/api/SinaWeiboDialog;->removeSpinner()V
    invoke-static {v0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->access$000(Lcom/vlingo/midas/social/api/SinaWeiboDialog;)V

    .line 236
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog$WeiboWebViewClient;->this$0:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    # getter for: Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mWebView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->access$700(Lcom/vlingo/midas/social/api/SinaWeiboDialog;)Landroid/webkit/WebView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 237
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 224
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 225
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog$WeiboWebViewClient;->this$0:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    # getter for: Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mSpinner:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->access$500(Lcom/vlingo/midas/social/api/SinaWeiboDialog;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog$WeiboWebViewClient;->this$0:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    # invokes: Lcom/vlingo/midas/social/api/SinaWeiboDialog;->isWindowShowing()Z
    invoke-static {v0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->access$600(Lcom/vlingo/midas/social/api/SinaWeiboDialog;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog$WeiboWebViewClient;->this$0:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    # getter for: Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mSpinner:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->access$500(Lcom/vlingo/midas/social/api/SinaWeiboDialog;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog$WeiboWebViewClient;->this$0:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    # getter for: Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mSpinner:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->access$500(Lcom/vlingo/midas/social/api/SinaWeiboDialog;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog$WeiboWebViewClient;->this$0:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    # getter for: Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mSpinner:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->access$500(Lcom/vlingo/midas/social/api/SinaWeiboDialog;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 228
    :cond_0
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 213
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog$WeiboWebViewClient;->this$0:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    # getter for: Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mListener:Lcom/weibo/sdk/android/WeiboAuthListener;
    invoke-static {v0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->access$100(Lcom/vlingo/midas/social/api/SinaWeiboDialog;)Lcom/weibo/sdk/android/WeiboAuthListener;

    move-result-object v0

    new-instance v1, Lcom/weibo/sdk/android/WeiboDialogError;

    invoke-direct {v1, p3, p2, p4}, Lcom/weibo/sdk/android/WeiboDialogError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/weibo/sdk/android/WeiboAuthListener;->onError(Lcom/weibo/sdk/android/WeiboDialogError;)V

    .line 217
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog$WeiboWebViewClient;->this$0:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->dismiss()V

    .line 218
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 5
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 194
    const-string/jumbo v2, "sms:"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 195
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 196
    .local v0, "sendIntent":Landroid/content/Intent;
    const-string/jumbo v2, "address"

    const-string/jumbo v3, "sms:"

    const-string/jumbo v4, ""

    invoke-virtual {p2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 197
    const-string/jumbo v2, "vnd.android-dir/mms-sms"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 198
    iget-object v2, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog$WeiboWebViewClient;->this$0:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    invoke-virtual {v2}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 207
    .end local v0    # "sendIntent":Landroid/content/Intent;
    :goto_0
    return v1

    .line 201
    :cond_0
    iget-object v2, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog$WeiboWebViewClient;->this$0:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    # getter for: Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mRedirectUrl:Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->access$300(Lcom/vlingo/midas/social/api/SinaWeiboDialog;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 202
    iget-object v2, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog$WeiboWebViewClient;->this$0:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    # invokes: Lcom/vlingo/midas/social/api/SinaWeiboDialog;->handleRedirectUrl(Landroid/webkit/WebView;Ljava/lang/String;)V
    invoke-static {v2, p1, p2}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->access$400(Lcom/vlingo/midas/social/api/SinaWeiboDialog;Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 203
    invoke-virtual {p1}, Landroid/webkit/WebView;->stopLoading()V

    .line 204
    iget-object v2, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog$WeiboWebViewClient;->this$0:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    invoke-virtual {v2}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->dismiss()V

    goto :goto_0

    .line 207
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method
