.class Lcom/vlingo/midas/social/SocialAccountActivity$13;
.super Ljava/lang/Object;
.source "SocialAccountActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/social/SocialAccountActivity;->onFacebookAPIMethod(Lcom/vlingo/midas/social/api/FacebookAPI;ILjava/lang/String;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

.field final synthetic val$method:Ljava/lang/String;

.field final synthetic val$params:Landroid/os/Bundle;

.field final synthetic val$responseType:I


# direct methods
.method constructor <init>(Lcom/vlingo/midas/social/SocialAccountActivity;Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 538
    iput-object p1, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    iput-object p2, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->val$method:Ljava/lang/String;

    iput p3, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->val$responseType:I

    iput-object p4, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->val$params:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 540
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    iget-boolean v0, v0, Lcom/vlingo/midas/social/SocialAccountActivity;->mDestroyed:Z

    if-eqz v0, :cond_2

    .line 541
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # getter for: Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$100(Lcom/vlingo/midas/social/SocialAccountActivity;)Lcom/vlingo/midas/social/api/SocialAPI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/SocialAPI;->clearCredentials()V

    .line 563
    :cond_1
    :goto_0
    return-void

    .line 544
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->val$method:Ljava/lang/String;

    const-string/jumbo v1, "me"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 545
    iget v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->val$responseType:I

    const/16 v1, 0x385

    if-ne v0, v1, :cond_4

    .line 546
    const-string/jumbo v0, "facebook_account_name"

    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->val$params:Landroid/os/Bundle;

    const-string/jumbo v2, "name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    const-string/jumbo v0, "facebook_picture_url"

    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->val$params:Landroid/os/Bundle;

    const-string/jumbo v2, "picture"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # invokes: Lcom/vlingo/midas/social/SocialAccountActivity;->removeCurrentDialog()V
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$300(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    .line 549
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_3

    .line 550
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    const/16 v1, 0xce

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->showDialog(I)V

    .line 552
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # invokes: Lcom/vlingo/midas/social/SocialAccountActivity;->fetchThumbnail()V
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$400(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    goto :goto_0

    .line 554
    :cond_4
    iget v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->val$responseType:I

    const/16 v1, 0x386

    if-ne v0, v1, :cond_1

    .line 555
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # getter for: Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$100(Lcom/vlingo/midas/social/SocialAccountActivity;)Lcom/vlingo/midas/social/api/SocialAPI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/SocialAPI;->clearCredentials()V

    .line 556
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # invokes: Lcom/vlingo/midas/social/SocialAccountActivity;->removeCurrentDialog()V
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$300(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    .line 557
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    sget v2, Lcom/vlingo/midas/R$string;->social_facebook_comm_error:I

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/social/SocialAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/vlingo/midas/social/SocialAccountActivity;->errorMessage:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$502(Lcom/vlingo/midas/social/SocialAccountActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 559
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 560
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$13;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    const/16 v1, 0xcc

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->showDialog(I)V

    goto :goto_0
.end method
