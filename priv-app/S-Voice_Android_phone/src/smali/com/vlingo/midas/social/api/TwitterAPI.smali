.class public Lcom/vlingo/midas/social/api/TwitterAPI;
.super Lcom/vlingo/midas/social/api/SocialAPI;
.source "TwitterAPI.java"

# interfaces
.implements Lcom/vlingo/midas/social/api/TwitterDialogListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;,
        Lcom/vlingo/midas/social/api/TwitterAPI$GetCredentialsTask;,
        Lcom/vlingo/midas/social/api/TwitterAPI$PostTask;,
        Lcom/vlingo/midas/social/api/TwitterAPI$CheckFriendshipTask;,
        Lcom/vlingo/midas/social/api/TwitterAPI$FollowVlingoTask;,
        Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;
    }
.end annotation


# static fields
.field private static final ACCEPTED_TEXT_TAG_STRING:Ljava/lang/String; = "twitter:def"

.field private static final CALLBACK_URI:Landroid/net/Uri;

.field private static final INTENT_STRING:Ljava/lang/String; = "twitter"

.field public static final RESULT_CANCELLED:I = 0x5

.field public static final RESULT_FAIL:I = 0x1

.field public static final RESULT_FAIL_AUTH:I = 0x2

.field public static final RESULT_FAIL_FORBIDDEN:I = 0x3

.field public static final RESULT_FAIL_TWITTER_DOWN:I = 0x4

.field public static final RESULT_SUCCESS:I = 0x0

.field private static final TWITTER_ACCESS_TOKEN_URL:Ljava/lang/String; = "https://api.twitter.com/oauth/access_token"

.field private static final TWITTER_AUTHORIZE_URL:Ljava/lang/String; = "https://api.twitter.com/oauth/authorize"

.field private static final TWITTER_CONSUMER_KEY:Ljava/lang/String;

.field private static final TWITTER_CONSUMER_SECRET:Ljava/lang/String;

.field private static final TWITTER_REQUEST_TOKEN_URL:Ljava/lang/String; = "https://api.twitter.com/oauth/request_token"

.field public static final TYPE_INTENT:I = 0x4

.field private static final URL_TWITTER_CREATE:Ljava/lang/String; = "https://api.twitter.com/1.1/friendships/create/vlingo.json"

.field private static final URL_TWITTER_FRIENDSHIP:Ljava/lang/String; = "https://api.twitter.com/1.1/friendships/exists.json?user_a={userA}&user_b={userB}"

.field private static final URL_TWITTER_UPDATE:Ljava/lang/String; = "https://api.twitter.com/1.1/statuses/update.json"

.field private static final URL_TWITTER_VERIFY:Ljava/lang/String; = "https://api.twitter.com/1.1/account/verify_credentials.json"


# instance fields
.field private volatile callback:Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;

.field private context:Landroid/content/Context;

.field private mAuthTask:Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;

.field private mClient:Lorg/apache/http/client/HttpClient;

.field private mConsumer:Loauth/signpost/OAuthConsumer;

.field private mProvider:Loauth/signpost/OAuthProvider;

.field private mSecret:Ljava/lang/String;

.field private mToken:Ljava/lang/String;

.field private mTwitterDialog:Lcom/vlingo/midas/social/api/TwitterDialog;

.field private volatile shutdown:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 77
    const-string/jumbo v0, "twitter_consumer_key"

    const-string/jumbo v1, "AGv8Ps3AlFKrf2C1YoFkQ"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/social/api/TwitterAPI;->TWITTER_CONSUMER_KEY:Ljava/lang/String;

    .line 78
    const-string/jumbo v0, "twitter_consumer_secret"

    const-string/jumbo v1, "qeX5TCXa9HPDlpNmhPACOT7sUerHPmD91Oq9nYuw6Q"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/social/api/TwitterAPI;->TWITTER_CONSUMER_SECRET:Ljava/lang/String;

    .line 84
    const-string/jumbo v0, "http://m.vlingo.com/twitterCallback"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/social/api/TwitterAPI;->CALLBACK_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;)V
    .locals 23
    .param p1, "callback"    # Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;

    .prologue
    .line 117
    const-string/jumbo v4, "twitter_account"

    const-string/jumbo v5, "twitter_picture_url"

    const-string/jumbo v6, "twitter"

    const/4 v7, -0x1

    const/4 v8, -0x1

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v3

    sget-object v9, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_twitter_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v3, v9}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$string;)I

    move-result v9

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v3

    sget-object v10, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_twitter:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v3, v10}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$string;)I

    move-result v10

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v3

    sget-object v11, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_wcis_social_twitter:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v3, v11}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$string;)I

    move-result v11

    const/4 v12, -0x1

    const/4 v13, -0x1

    const/4 v14, 0x4

    const/4 v15, -0x1

    const/16 v16, 0x8c

    const/16 v17, 0x1

    const/16 v18, 0x1

    const-string/jumbo v19, "twitter:def"

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v19}, Lcom/vlingo/midas/social/api/SocialAPI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIIIIIIIZZLjava/lang/String;)V

    .line 103
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/vlingo/midas/social/api/TwitterAPI;->mAuthTask:Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;

    .line 105
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/vlingo/midas/social/api/TwitterAPI;->shutdown:Z

    .line 123
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/social/api/TwitterAPI;->callback:Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;

    .line 125
    new-instance v20, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct/range {v20 .. v20}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 126
    .local v20, "parameters":Lorg/apache/http/params/HttpParams;
    sget-object v3, Lorg/apache/http/HttpVersion;->HTTP_1_1:Lorg/apache/http/HttpVersion;

    move-object/from16 v0, v20

    invoke-static {v0, v3}, Lorg/apache/http/params/HttpProtocolParams;->setVersion(Lorg/apache/http/params/HttpParams;Lorg/apache/http/ProtocolVersion;)V

    .line 127
    const-string/jumbo v3, "ISO-8859-1"

    move-object/from16 v0, v20

    invoke-static {v0, v3}, Lorg/apache/http/params/HttpProtocolParams;->setContentCharset(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 128
    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-static {v0, v3}, Lorg/apache/http/params/HttpProtocolParams;->setUseExpectContinue(Lorg/apache/http/params/HttpParams;Z)V

    .line 129
    const/4 v3, 0x1

    move-object/from16 v0, v20

    invoke-static {v0, v3}, Lorg/apache/http/params/HttpConnectionParams;->setTcpNoDelay(Lorg/apache/http/params/HttpParams;Z)V

    .line 130
    const/16 v3, 0x2000

    move-object/from16 v0, v20

    invoke-static {v0, v3}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 132
    new-instance v21, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct/range {v21 .. v21}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 134
    .local v21, "schReg":Lorg/apache/http/conn/scheme/SchemeRegistry;
    new-instance v3, Lorg/apache/http/conn/scheme/Scheme;

    const-string/jumbo v4, "https"

    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v5

    const/16 v6, 0x1bb

    invoke-direct {v3, v4, v5, v6}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 135
    new-instance v22, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 136
    .local v22, "tsccm":Lorg/apache/http/conn/ClientConnectionManager;
    new-instance v3, Lorg/apache/http/impl/client/DefaultHttpClient;

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-direct {v3, v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/vlingo/midas/social/api/TwitterAPI;->mClient:Lorg/apache/http/client/HttpClient;

    .line 138
    new-instance v3, Loauth/signpost/commonshttp/CommonsHttpOAuthConsumer;

    sget-object v4, Lcom/vlingo/midas/social/api/TwitterAPI;->TWITTER_CONSUMER_KEY:Ljava/lang/String;

    sget-object v5, Lcom/vlingo/midas/social/api/TwitterAPI;->TWITTER_CONSUMER_SECRET:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Loauth/signpost/commonshttp/CommonsHttpOAuthConsumer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/vlingo/midas/social/api/TwitterAPI;->mConsumer:Loauth/signpost/OAuthConsumer;

    .line 140
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/social/api/TwitterAPI;->refreshCredentials()V

    .line 141
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/social/api/TwitterAPI;)Lorg/apache/http/client/HttpClient;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/TwitterAPI;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mClient:Lorg/apache/http/client/HttpClient;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/vlingo/midas/social/api/TwitterAPI;)Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/TwitterAPI;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->callback:Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/vlingo/midas/social/api/TwitterAPI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/TwitterAPI;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->shutdown:Z

    return v0
.end method

.method static synthetic access$600(Lcom/vlingo/midas/social/api/TwitterAPI;)Loauth/signpost/OAuthConsumer;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/TwitterAPI;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mConsumer:Loauth/signpost/OAuthConsumer;

    return-object v0
.end method

.method static synthetic access$700()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/vlingo/midas/social/api/TwitterAPI;->CALLBACK_URI:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$800(Lcom/vlingo/midas/social/api/TwitterAPI;)Loauth/signpost/OAuthProvider;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/TwitterAPI;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mProvider:Loauth/signpost/OAuthProvider;

    return-object v0
.end method

.method static synthetic access$900(Lcom/vlingo/midas/social/api/TwitterAPI;)Lcom/vlingo/midas/social/api/TwitterDialog;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/TwitterAPI;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mTwitterDialog:Lcom/vlingo/midas/social/api/TwitterDialog;

    return-object v0
.end method

.method public static startsWith(Ljava/lang/String;)Z
    .locals 1
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 144
    sget-object v0, Lcom/vlingo/midas/social/api/TwitterAPI;->CALLBACK_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public checkFriendshipWithUser(Ljava/lang/String;)V
    .locals 3
    .param p1, "user"    # Ljava/lang/String;

    .prologue
    .line 252
    new-instance v0, Lcom/vlingo/midas/social/api/TwitterAPI$CheckFriendshipTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/midas/social/api/TwitterAPI$CheckFriendshipTask;-><init>(Lcom/vlingo/midas/social/api/TwitterAPI;Lcom/vlingo/midas/social/api/TwitterAPI$1;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/social/api/TwitterAPI$CheckFriendshipTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 253
    return-void
.end method

.method public clearCredentials()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 148
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/android/Util;->clearCookies(Landroid/content/Context;)V

    .line 149
    const-string/jumbo v0, "twitter_account_name"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string/jumbo v0, "twitter_request_secret"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    const-string/jumbo v0, "twitter_request_token"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string/jumbo v0, "twitter_user_secret"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string/jumbo v0, "twitter_user_token"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string/jumbo v0, "twitter_username"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string/jumbo v0, "twitter_picture_url"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const-string/jumbo v0, "twitter_picture"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setImage(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 157
    const-string/jumbo v0, "twitter_account"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 158
    return-void
.end method

.method public dismissDialogs()V
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mAuthTask:Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mAuthTask:Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 261
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mAuthTask:Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->cancel(Z)Z

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mTwitterDialog:Lcom/vlingo/midas/social/api/TwitterDialog;

    if-eqz v0, :cond_1

    .line 264
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mTwitterDialog:Lcom/vlingo/midas/social/api/TwitterDialog;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/TwitterDialog;->dismissDialogs()V

    .line 266
    :cond_1
    return-void
.end method

.method public followVlingo()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 247
    new-instance v1, Lcom/vlingo/midas/social/api/TwitterAPI$FollowVlingoTask;

    invoke-direct {v1, p0, v0}, Lcom/vlingo/midas/social/api/TwitterAPI$FollowVlingoTask;-><init>(Lcom/vlingo/midas/social/api/TwitterAPI;Lcom/vlingo/midas/social/api/TwitterAPI$1;)V

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/vlingo/midas/social/api/TwitterAPI$FollowVlingoTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 248
    const/4 v0, 0x1

    return v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->context:Landroid/content/Context;

    return-object v0
.end method

.method public getDialog()Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mTwitterDialog:Lcom/vlingo/midas/social/api/TwitterDialog;

    return-object v0
.end method

.method public getParams()Lorg/apache/http/params/HttpParams;
    .locals 2

    .prologue
    .line 577
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 579
    .local v0, "params":Lorg/apache/http/params/HttpParams;
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpProtocolParams;->setUseExpectContinue(Lorg/apache/http/params/HttpParams;Z)V

    .line 580
    return-object v0
.end method

.method public login(Landroid/app/Activity;)Z
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v4, 0x1

    .line 219
    iput-object p1, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->context:Landroid/content/Context;

    .line 220
    new-instance v0, Loauth/signpost/commonshttp/CommonsHttpOAuthProvider;

    const-string/jumbo v1, "https://api.twitter.com/oauth/request_token"

    const-string/jumbo v2, "https://api.twitter.com/oauth/access_token"

    const-string/jumbo v3, "https://api.twitter.com/oauth/authorize"

    invoke-direct {v0, v1, v2, v3}, Loauth/signpost/commonshttp/CommonsHttpOAuthProvider;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mProvider:Loauth/signpost/OAuthProvider;

    .line 225
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mProvider:Loauth/signpost/OAuthProvider;

    invoke-interface {v0, v4}, Loauth/signpost/OAuthProvider;->setOAuth10a(Z)V

    .line 227
    new-instance v0, Lcom/vlingo/midas/social/api/TwitterDialog;

    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterAPI;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/vlingo/midas/social/api/TwitterDialog;-><init>(Landroid/content/Context;Lcom/vlingo/midas/social/api/TwitterDialogListener;)V

    iput-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mTwitterDialog:Lcom/vlingo/midas/social/api/TwitterDialog;

    .line 228
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mTwitterDialog:Lcom/vlingo/midas/social/api/TwitterDialog;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/TwitterDialog;->show()V

    .line 230
    new-instance v0, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;-><init>(Lcom/vlingo/midas/social/api/TwitterAPI;Lcom/vlingo/midas/social/api/TwitterAPI$1;)V

    iput-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mAuthTask:Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;

    .line 231
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mAuthTask:Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;

    new-array v1, v4, [Lcom/vlingo/midas/social/api/TwitterAPI;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 232
    return v4
.end method

.method public onCancel()V
    .locals 4

    .prologue
    .line 589
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->callback:Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;->onLoginComplete(IZLjava/lang/String;)V

    .line 590
    return-void
.end method

.method public onComplete(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "values"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 593
    iget-boolean v4, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->shutdown:Z

    if-eqz v4, :cond_0

    .line 640
    :goto_0
    return-void

    .line 594
    :cond_0
    const-string/jumbo v4, "twitter_request_token"

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 595
    .local v2, "token":Ljava/lang/String;
    const-string/jumbo v4, "twitter_request_secret"

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 598
    .local v1, "secret":Ljava/lang/String;
    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    .line 599
    iget-object v4, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mConsumer:Loauth/signpost/OAuthConsumer;

    invoke-interface {v4, v2, v1}, Loauth/signpost/OAuthConsumer;->setTokenWithSecret(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    :cond_1
    const-string/jumbo v4, "oauth_token"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 602
    .local v0, "otoken":Ljava/lang/String;
    const-string/jumbo v4, "oauth_verifier"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 606
    .local v3, "verifier":Ljava/lang/String;
    new-instance v4, Lcom/vlingo/midas/social/api/TwitterAPI$3;

    invoke-direct {v4, p0, v0, v3}, Lcom/vlingo/midas/social/api/TwitterAPI$3;-><init>(Lcom/vlingo/midas/social/api/TwitterAPI;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/vlingo/midas/social/api/TwitterAPI$3;->start()V

    goto :goto_0
.end method

.method public onError(Ljava/lang/String;)V
    .locals 5
    .param p1, "err"    # Ljava/lang/String;

    .prologue
    .line 643
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 646
    .local v0, "apiErrMsg":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->callback:Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;->onLoginComplete(IZLjava/lang/String;)V

    .line 647
    return-void
.end method

.method public onTwitterError(Ljava/lang/String;)V
    .locals 5
    .param p1, "err"    # Ljava/lang/String;

    .prologue
    .line 650
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_twitter_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 653
    .local v0, "twitterErrMsg":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->callback:Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;->onLoginComplete(IZLjava/lang/String;)V

    .line 654
    return-void
.end method

.method public refreshCredentials()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 163
    const-string/jumbo v0, "twitter_user_token"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mToken:Ljava/lang/String;

    .line 164
    const-string/jumbo v0, "twitter_user_secret"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mSecret:Ljava/lang/String;

    .line 165
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mSecret:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mConsumer:Loauth/signpost/OAuthConsumer;

    iget-object v1, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mToken:Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mSecret:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Loauth/signpost/OAuthConsumer;->setTokenWithSecret(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    :cond_0
    return-void
.end method

.method public showDialogs()V
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mTwitterDialog:Lcom/vlingo/midas/social/api/TwitterDialog;

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mTwitterDialog:Lcom/vlingo/midas/social/api/TwitterDialog;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/TwitterDialog;->showDialogs()V

    .line 275
    :cond_0
    return-void
.end method

.method public shutdown()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 175
    iput-boolean v2, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->shutdown:Z

    .line 177
    new-instance v0, Lcom/vlingo/midas/social/api/TwitterAPI$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/social/api/TwitterAPI$1;-><init>(Lcom/vlingo/midas/social/api/TwitterAPI;)V

    iput-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->callback:Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;

    .line 202
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mTwitterDialog:Lcom/vlingo/midas/social/api/TwitterDialog;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mTwitterDialog:Lcom/vlingo/midas/social/api/TwitterDialog;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/TwitterDialog;->dismissDialogs()V

    .line 204
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mTwitterDialog:Lcom/vlingo/midas/social/api/TwitterDialog;

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mAuthTask:Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mAuthTask:Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_1

    .line 207
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mAuthTask:Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;

    invoke-virtual {v0, v2}, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->cancel(Z)Z

    .line 210
    :cond_1
    new-instance v0, Lcom/vlingo/midas/social/api/TwitterAPI$2;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/social/api/TwitterAPI$2;-><init>(Lcom/vlingo/midas/social/api/TwitterAPI;)V

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/TwitterAPI$2;->start()V

    .line 216
    return-void
.end method

.method public updateDialogs()V
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mTwitterDialog:Lcom/vlingo/midas/social/api/TwitterDialog;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI;->mTwitterDialog:Lcom/vlingo/midas/social/api/TwitterDialog;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/TwitterDialog;->updateDialog()V

    .line 281
    :cond_0
    return-void
.end method

.method public updateStatus(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 241
    if-nez p1, :cond_0

    .line 242
    const-string/jumbo p1, ""

    .line 243
    :cond_0
    new-instance v0, Lcom/vlingo/midas/social/api/TwitterAPI$PostTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/midas/social/api/TwitterAPI$PostTask;-><init>(Lcom/vlingo/midas/social/api/TwitterAPI;Lcom/vlingo/midas/social/api/TwitterAPI$1;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/social/api/TwitterAPI$PostTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 244
    return-void
.end method

.method public verifyCredentials()V
    .locals 2

    .prologue
    .line 236
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterAPI;->refreshCredentials()V

    .line 237
    new-instance v0, Lcom/vlingo/midas/social/api/TwitterAPI$GetCredentialsTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/midas/social/api/TwitterAPI$GetCredentialsTask;-><init>(Lcom/vlingo/midas/social/api/TwitterAPI;Lcom/vlingo/midas/social/api/TwitterAPI$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/social/api/TwitterAPI$GetCredentialsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 238
    return-void
.end method
