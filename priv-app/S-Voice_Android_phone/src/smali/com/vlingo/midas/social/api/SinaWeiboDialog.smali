.class public Lcom/vlingo/midas/social/api/SinaWeiboDialog;
.super Landroid/app/Dialog;
.source "SinaWeiboDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/social/api/SinaWeiboDialog$WeiboWebViewClient;
    }
.end annotation


# static fields
.field static final DIMENSIONS_LANDSCAPE:[F

.field static final DIMENSIONS_PORTRAIT:[F

.field static final MARGIN:I = 0x4

.field static final PADDING:I = 0x2

.field private static final URL_OAUTH_ACCESS_AUTH:Ljava/lang/String; = "https://open.weibo.cn/oauth2/authorize"

.field static final WEIBO_RED:I = -0x4b00ffb3


# instance fields
.field FILL:Landroid/widget/FrameLayout$LayoutParams;

.field private mContent:Landroid/widget/LinearLayout;

.field private mListener:Lcom/weibo/sdk/android/WeiboAuthListener;

.field private mRedirectUrl:Ljava/lang/String;

.field private mSpinner:Landroid/app/ProgressDialog;

.field private mTitle:Landroid/widget/TextView;

.field private mUrl:Ljava/lang/String;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 40
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->DIMENSIONS_LANDSCAPE:[F

    .line 41
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->DIMENSIONS_PORTRAIT:[F

    return-void

    .line 40
    :array_0
    .array-data 4
        0x43e60000    # 460.0f
        0x43820000    # 260.0f
    .end array-data

    .line 41
    :array_1
    .array-data 4
        0x43960000    # 300.0f
        0x43d20000    # 420.0f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/weibo/sdk/android/WeiboAuthListener;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appkey"    # Ljava/lang/String;
    .param p3, "redirectURL"    # Ljava/lang/String;
    .param p4, "listener"    # Lcom/weibo/sdk/android/WeiboAuthListener;

    .prologue
    const/4 v2, -0x1

    .line 57
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 46
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iput-object v1, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->FILL:Landroid/widget/FrameLayout$LayoutParams;

    .line 58
    iput-object p3, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mRedirectUrl:Ljava/lang/String;

    .line 59
    new-instance v0, Lcom/weibo/sdk/android/WeiboParameters;

    invoke-direct {v0}, Lcom/weibo/sdk/android/WeiboParameters;-><init>()V

    .line 60
    .local v0, "parameters":Lcom/weibo/sdk/android/WeiboParameters;
    const-string/jumbo v1, "client_id"

    invoke-virtual {v0, v1, p2}, Lcom/weibo/sdk/android/WeiboParameters;->add(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const-string/jumbo v1, "response_type"

    const-string/jumbo v2, "token"

    invoke-virtual {v0, v1, v2}, Lcom/weibo/sdk/android/WeiboParameters;->add(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const-string/jumbo v1, "redirect_uri"

    iget-object v2, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mRedirectUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/weibo/sdk/android/WeiboParameters;->add(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string/jumbo v1, "display"

    const-string/jumbo v2, "mobile"

    invoke-virtual {v0, v1, v2}, Lcom/weibo/sdk/android/WeiboParameters;->add(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "https://open.weibo.cn/oauth2/authorize?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Lcom/weibo/sdk/android/util/Utility;->encodeUrl(Lcom/weibo/sdk/android/WeiboParameters;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mUrl:Ljava/lang/String;

    .line 65
    iput-object p4, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mListener:Lcom/weibo/sdk/android/WeiboAuthListener;

    .line 66
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/social/api/SinaWeiboDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->removeSpinner()V

    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/social/api/SinaWeiboDialog;)Lcom/weibo/sdk/android/WeiboAuthListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mListener:Lcom/weibo/sdk/android/WeiboAuthListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/social/api/SinaWeiboDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mRedirectUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/social/api/SinaWeiboDialog;Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/SinaWeiboDialog;
    .param p1, "x1"    # Landroid/webkit/WebView;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->handleRedirectUrl(Landroid/webkit/WebView;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/vlingo/midas/social/api/SinaWeiboDialog;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mSpinner:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/midas/social/api/SinaWeiboDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->isWindowShowing()Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/vlingo/midas/social/api/SinaWeiboDialog;)Landroid/webkit/WebView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mWebView:Landroid/webkit/WebView;

    return-object v0
.end method

.method private handleRedirectUrl(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 4
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 261
    invoke-static {p2}, Lcom/weibo/sdk/android/util/Utility;->parseUrl(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 263
    .local v2, "values":Landroid/os/Bundle;
    const-string/jumbo v3, "error"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 264
    .local v0, "error":Ljava/lang/String;
    const-string/jumbo v3, "error_code"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 266
    .local v1, "errorCode":Ljava/lang/String;
    if-nez v0, :cond_1

    if-nez v1, :cond_1

    .line 267
    iget-object v3, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mListener:Lcom/weibo/sdk/android/WeiboAuthListener;

    invoke-interface {v3, v2}, Lcom/weibo/sdk/android/WeiboAuthListener;->onComplete(Landroid/os/Bundle;)V

    .line 274
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    if-eqz v0, :cond_0

    const-string/jumbo v3, "access_denied"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 269
    iget-object v3, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mListener:Lcom/weibo/sdk/android/WeiboAuthListener;

    invoke-interface {v3}, Lcom/weibo/sdk/android/WeiboAuthListener;->onCancel()V

    goto :goto_0
.end method

.method private isWindowShowing()Z
    .locals 1

    .prologue
    .line 257
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized removeSpinner()V
    .locals 1

    .prologue
    .line 241
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mSpinner:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 242
    invoke-direct {p0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->isWindowShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    .line 244
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 251
    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mSpinner:Landroid/app/ProgressDialog;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 254
    :cond_0
    monitor-exit p0

    return-void

    .line 241
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 247
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private setUpTitle()V
    .locals 7

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x0

    const/4 v4, 0x4

    .line 149
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->requestWindowFeature(I)Z

    .line 150
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_weibo_icon:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getDrawable(Lcom/vlingo/core/internal/ResourceIdProvider$drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 151
    .local v0, "icon":Landroid/graphics/drawable/Drawable;
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mTitle:Landroid/widget/TextView;

    .line 152
    iget-object v1, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mTitle:Landroid/widget/TextView;

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weibo_dialog_title:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    iget-object v1, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mTitle:Landroid/widget/TextView;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 154
    iget-object v1, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mTitle:Landroid/widget/TextView;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 155
    iget-object v1, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mTitle:Landroid/widget/TextView;

    const v2, -0x4b00ffb3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 156
    iget-object v1, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v6, v4, v4, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 157
    iget-object v1, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 158
    iget-object v1, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 160
    iget-object v1, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mContent:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 161
    return-void
.end method

.method private setUpWebView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 136
    new-instance v0, Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mWebView:Landroid/webkit/WebView;

    .line 137
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 138
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    .line 139
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 140
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 141
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 142
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    .line 143
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/vlingo/midas/social/api/SinaWeiboDialog$WeiboWebViewClient;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/vlingo/midas/social/api/SinaWeiboDialog$WeiboWebViewClient;-><init>(Lcom/vlingo/midas/social/api/SinaWeiboDialog;Lcom/vlingo/midas/social/api/SinaWeiboDialog$1;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 144
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mWebView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mWebView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->FILL:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 146
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mContent:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 147
    return-void
.end method


# virtual methods
.method protected onBack()V
    .locals 1

    .prologue
    .line 101
    :try_start_0
    invoke-direct {p0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->removeSpinner()V

    .line 102
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    .line 104
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->dismiss()V

    .line 109
    return-void

    .line 106
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 70
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 71
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_util_loading:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "loadingMsg":Ljava/lang/String;
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mSpinner:Landroid/app/ProgressDialog;

    .line 74
    iget-object v1, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->requestWindowFeature(I)Z

    .line 75
    iget-object v1, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v1, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mSpinner:Landroid/app/ProgressDialog;

    new-instance v2, Lcom/vlingo/midas/social/api/SinaWeiboDialog$1;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog$1;-><init>(Lcom/vlingo/midas/social/api/SinaWeiboDialog;)V

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 83
    invoke-virtual {p0, v3}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->requestWindowFeature(I)Z

    .line 84
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mContent:Landroid/widget/LinearLayout;

    .line 85
    iget-object v1, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mContent:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 87
    invoke-direct {p0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->setUpTitle()V

    .line 88
    invoke-direct {p0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->setUpWebView()V

    .line 89
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->updateView()V

    .line 91
    new-instance v1, Lcom/vlingo/midas/social/api/SinaWeiboDialog$2;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog$2;-><init>(Lcom/vlingo/midas/social/api/SinaWeiboDialog;)V

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 97
    return-void
.end method

.method public updateDialog()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const v6, 0x3f333333    # 0.7f

    const/high16 v9, 0x3f000000    # 0.5f

    .line 164
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 165
    .local v1, "display":Landroid/view/Display;
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 166
    .local v3, "metrics":Landroid/util/DisplayMetrics;
    iget v4, v3, Landroid/util/DisplayMetrics;->density:F

    .line 168
    .local v4, "scale":F
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isMiniTabletDevice()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 169
    const/4 v5, 0x2

    new-array v0, v5, [F

    .line 170
    .local v0, "dimensions":[F
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v6

    aput v5, v0, v10

    .line 171
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v6

    aput v5, v0, v11

    .line 172
    const/high16 v4, 0x3f800000    # 1.0f

    .line 182
    :goto_0
    iget-object v5, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mContent:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 183
    .local v2, "lp":Landroid/view/ViewGroup$LayoutParams;
    aget v5, v0, v10

    mul-float/2addr v5, v4

    add-float/2addr v5, v9

    float-to-int v5, v5

    iput v5, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 184
    aget v5, v0, v11

    mul-float/2addr v5, v4

    add-float/2addr v5, v9

    float-to-int v5, v5

    iput v5, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 185
    iget-object v5, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mContent:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 186
    return-void

    .line 173
    .end local v0    # "dimensions":[F
    .end local v2    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    float-to-double v5, v4

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    cmpl-double v5, v5, v7

    if-nez v5, :cond_1

    iget v5, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v6, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    add-int/2addr v5, v6

    const/16 v6, 0x7f0

    if-ne v5, v6, :cond_1

    iget v5, v3, Landroid/util/DisplayMetrics;->xdpi:F

    const v6, 0x4315d32c

    cmpl-float v5, v5, v6

    if-nez v5, :cond_1

    iget v5, v3, Landroid/util/DisplayMetrics;->ydpi:F

    const v6, 0x431684be

    cmpl-float v5, v5, v6

    if-nez v5, :cond_1

    .line 176
    sget-object v0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->DIMENSIONS_PORTRAIT:[F

    .line 177
    .restart local v0    # "dimensions":[F
    const/high16 v4, 0x3fc00000    # 1.5f

    goto :goto_0

    .line 179
    .end local v0    # "dimensions":[F
    :cond_1
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v5

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v6

    if-ge v5, v6, :cond_2

    sget-object v0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->DIMENSIONS_PORTRAIT:[F

    .restart local v0    # "dimensions":[F
    :goto_1
    goto :goto_0

    .end local v0    # "dimensions":[F
    :cond_2
    sget-object v0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->DIMENSIONS_LANDSCAPE:[F

    goto :goto_1
.end method

.method public updateView()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const v5, 0x3f333333    # 0.7f

    const/high16 v8, 0x3f000000    # 0.5f

    .line 112
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 113
    .local v1, "display":Landroid/view/Display;
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 114
    .local v2, "metrics":Landroid/util/DisplayMetrics;
    iget v3, v2, Landroid/util/DisplayMetrics;->density:F

    .line 116
    .local v3, "scale":F
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isMiniTabletDevice()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 117
    const/4 v4, 0x2

    new-array v0, v4, [F

    .line 118
    .local v0, "dimensions":[F
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v5

    aput v4, v0, v9

    .line 119
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v5

    aput v4, v0, v10

    .line 120
    const/high16 v3, 0x3f800000    # 1.0f

    .line 130
    :goto_0
    iget-object v4, p0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->mContent:Landroid/widget/LinearLayout;

    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    aget v6, v0, v9

    mul-float/2addr v6, v3

    add-float/2addr v6, v8

    float-to-int v6, v6

    aget v7, v0, v10

    mul-float/2addr v7, v3

    add-float/2addr v7, v8

    float-to-int v7, v7

    invoke-direct {v5, v6, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v4, v5}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 133
    return-void

    .line 121
    .end local v0    # "dimensions":[F
    :cond_0
    float-to-double v4, v3

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, v4, v6

    if-nez v4, :cond_1

    iget v4, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v5, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    add-int/2addr v4, v5

    const/16 v5, 0x7f0

    if-ne v4, v5, :cond_1

    iget v4, v2, Landroid/util/DisplayMetrics;->xdpi:F

    const v5, 0x4315d32c

    cmpl-float v4, v4, v5

    if-nez v4, :cond_1

    iget v4, v2, Landroid/util/DisplayMetrics;->ydpi:F

    const v5, 0x431684be

    cmpl-float v4, v4, v5

    if-nez v4, :cond_1

    .line 124
    sget-object v0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->DIMENSIONS_PORTRAIT:[F

    .line 125
    .restart local v0    # "dimensions":[F
    const/high16 v3, 0x3fc00000    # 1.5f

    goto :goto_0

    .line 127
    .end local v0    # "dimensions":[F
    :cond_1
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v5

    if-ge v4, v5, :cond_2

    sget-object v0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->DIMENSIONS_PORTRAIT:[F

    .restart local v0    # "dimensions":[F
    :goto_1
    goto :goto_0

    .end local v0    # "dimensions":[F
    :cond_2
    sget-object v0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->DIMENSIONS_LANDSCAPE:[F

    goto :goto_1
.end method
