.class Lcom/vlingo/midas/social/api/WeiboAPI$1;
.super Ljava/lang/Object;
.source "WeiboAPI.java"

# interfaces
.implements Lcom/weibo/sdk/android/net/RequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/social/api/WeiboAPI;->fetchUserData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/social/api/WeiboAPI;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/social/api/WeiboAPI;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/vlingo/midas/social/api/WeiboAPI$1;->this$0:Lcom/vlingo/midas/social/api/WeiboAPI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Ljava/lang/String;)V
    .locals 6
    .param p1, "response"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x3

    .line 170
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 171
    .local v1, "jSon":Lorg/json/JSONObject;
    const-string/jumbo v2, "screen_name"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v2, "profile_image_url"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 172
    iget-object v2, p0, Lcom/vlingo/midas/social/api/WeiboAPI$1;->this$0:Lcom/vlingo/midas/social/api/WeiboAPI;

    const-string/jumbo v3, "screen_name"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "profile_image_url"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/vlingo/midas/social/api/WeiboAPI;->saveUserData(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iget-object v2, p0, Lcom/vlingo/midas/social/api/WeiboAPI$1;->this$0:Lcom/vlingo/midas/social/api/WeiboAPI;

    iget-object v2, v2, Lcom/vlingo/midas/social/api/WeiboAPI;->mCallBack:Lcom/vlingo/midas/social/api/WeiboAPI$WeiboCallBack;

    if-eqz v2, :cond_0

    .line 174
    iget-object v2, p0, Lcom/vlingo/midas/social/api/WeiboAPI$1;->this$0:Lcom/vlingo/midas/social/api/WeiboAPI;

    iget-object v2, v2, Lcom/vlingo/midas/social/api/WeiboAPI;->mCallBack:Lcom/vlingo/midas/social/api/WeiboAPI$WeiboCallBack;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/vlingo/midas/social/api/WeiboAPI$WeiboCallBack;->onWeiboSuccess(I)V

    .line 182
    .end local v1    # "jSon":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    return-void

    .line 176
    .restart local v1    # "jSon":Lorg/json/JSONObject;
    :cond_1
    iget-object v2, p0, Lcom/vlingo/midas/social/api/WeiboAPI$1;->this$0:Lcom/vlingo/midas/social/api/WeiboAPI;

    const/4 v3, 0x3

    # invokes: Lcom/vlingo/midas/social/api/WeiboAPI;->notifyError(I)V
    invoke-static {v2, v3}, Lcom/vlingo/midas/social/api/WeiboAPI;->access$100(Lcom/vlingo/midas/social/api/WeiboAPI;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 179
    .end local v1    # "jSon":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 180
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/vlingo/midas/social/api/WeiboAPI$1;->this$0:Lcom/vlingo/midas/social/api/WeiboAPI;

    # invokes: Lcom/vlingo/midas/social/api/WeiboAPI;->notifyError(I)V
    invoke-static {v2, v5}, Lcom/vlingo/midas/social/api/WeiboAPI;->access$100(Lcom/vlingo/midas/social/api/WeiboAPI;I)V

    goto :goto_0
.end method

.method public onError(Lcom/weibo/sdk/android/WeiboException;)V
    .locals 2
    .param p1, "arg0"    # Lcom/weibo/sdk/android/WeiboException;

    .prologue
    .line 164
    iget-object v0, p0, Lcom/vlingo/midas/social/api/WeiboAPI$1;->this$0:Lcom/vlingo/midas/social/api/WeiboAPI;

    const/4 v1, 0x3

    # invokes: Lcom/vlingo/midas/social/api/WeiboAPI;->notifyError(I)V
    invoke-static {v0, v1}, Lcom/vlingo/midas/social/api/WeiboAPI;->access$100(Lcom/vlingo/midas/social/api/WeiboAPI;I)V

    .line 165
    return-void
.end method

.method public onIOException(Ljava/io/IOException;)V
    .locals 2
    .param p1, "arg0"    # Ljava/io/IOException;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/vlingo/midas/social/api/WeiboAPI$1;->this$0:Lcom/vlingo/midas/social/api/WeiboAPI;

    const/4 v1, 0x3

    # invokes: Lcom/vlingo/midas/social/api/WeiboAPI;->notifyException(I)V
    invoke-static {v0, v1}, Lcom/vlingo/midas/social/api/WeiboAPI;->access$000(Lcom/vlingo/midas/social/api/WeiboAPI;I)V

    .line 160
    return-void
.end method
