.class Lcom/vlingo/midas/social/api/TwitterAPI$CheckFriendshipTask;
.super Landroid/os/AsyncTask;
.source "TwitterAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/social/api/TwitterAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckFriendshipTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/social/api/TwitterAPI;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/social/api/TwitterAPI;)V
    .locals 0

    .prologue
    .line 401
    iput-object p1, p0, Lcom/vlingo/midas/social/api/TwitterAPI$CheckFriendshipTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/social/api/TwitterAPI;Lcom/vlingo/midas/social/api/TwitterAPI$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/social/api/TwitterAPI;
    .param p2, "x1"    # Lcom/vlingo/midas/social/api/TwitterAPI$1;

    .prologue
    .line 401
    invoke-direct {p0, p1}, Lcom/vlingo/midas/social/api/TwitterAPI$CheckFriendshipTask;-><init>(Lcom/vlingo/midas/social/api/TwitterAPI;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 401
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/social/api/TwitterAPI$CheckFriendshipTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 405
    const/4 v3, 0x0

    .line 407
    .local v3, "response":Ljava/lang/String;
    :try_start_0
    const-string/jumbo v6, "twitter_username"

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 408
    .local v5, "username":Ljava/lang/String;
    const-string/jumbo v4, "https://api.twitter.com/1.1/friendships/exists.json?user_a={userA}&user_b={userB}"

    .line 409
    .local v4, "twitterUrl":Ljava/lang/String;
    const-string/jumbo v6, "{userA}"

    invoke-static {v5}, Lcom/vlingo/core/internal/util/UrlUtils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v6, v7}, Lcom/vlingo/core/internal/util/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 410
    const-string/jumbo v6, "{userB}"

    const/4 v7, 0x0

    aget-object v7, p1, v7

    invoke-static {v7}, Lcom/vlingo/core/internal/util/UrlUtils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v6, v7}, Lcom/vlingo/core/internal/util/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 415
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v2, v4}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 416
    .local v2, "get":Lorg/apache/http/client/methods/HttpGet;
    iget-object v6, p0, Lcom/vlingo/midas/social/api/TwitterAPI$CheckFriendshipTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->mClient:Lorg/apache/http/client/HttpClient;
    invoke-static {v6}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$000(Lcom/vlingo/midas/social/api/TwitterAPI;)Lorg/apache/http/client/HttpClient;

    move-result-object v6

    new-instance v7, Lorg/apache/http/impl/client/BasicResponseHandler;

    invoke-direct {v7}, Lorg/apache/http/impl/client/BasicResponseHandler;-><init>()V

    invoke-interface {v6, v2, v7}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Ljava/lang/String;

    move-object v3, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_3

    .line 426
    .end local v2    # "get":Lorg/apache/http/client/methods/HttpGet;
    .end local v4    # "twitterUrl":Ljava/lang/String;
    .end local v5    # "username":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 417
    :catch_0
    move-exception v1

    .line 418
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 419
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v1

    .line 420
    .local v1, "e":Lorg/apache/http/client/ClientProtocolException;
    invoke-virtual {v1}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_0

    .line 421
    .end local v1    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_2
    move-exception v1

    .line 422
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 423
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 424
    .local v1, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 401
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/social/api/TwitterAPI$CheckFriendshipTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 5
    .param p1, "response"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 430
    if-eqz p1, :cond_0

    .line 431
    iget-object v1, p0, Lcom/vlingo/midas/social/api/TwitterAPI$CheckFriendshipTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->callback:Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;
    invoke-static {v1}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$1000(Lcom/vlingo/midas/social/api/TwitterAPI;)Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;

    move-result-object v1

    const-string/jumbo v2, "true"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x0

    invoke-interface {v1, v4, v2, v3}, Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;->onVlingoFriendshipExists(IZLjava/lang/String;)V

    .line 436
    :goto_0
    return-void

    .line 433
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_twitter_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 434
    .local v0, "errMsg":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/midas/social/api/TwitterAPI$CheckFriendshipTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->callback:Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;
    invoke-static {v1}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$1000(Lcom/vlingo/midas/social/api/TwitterAPI;)Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2, v4, v0}, Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;->onVlingoFriendshipExists(IZLjava/lang/String;)V

    goto :goto_0
.end method
