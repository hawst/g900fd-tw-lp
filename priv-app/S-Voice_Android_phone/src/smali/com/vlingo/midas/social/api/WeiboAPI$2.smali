.class Lcom/vlingo/midas/social/api/WeiboAPI$2;
.super Ljava/lang/Object;
.source "WeiboAPI.java"

# interfaces
.implements Lcom/weibo/sdk/android/net/RequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/social/api/WeiboAPI;->getUserUID()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/social/api/WeiboAPI;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/social/api/WeiboAPI;)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Lcom/vlingo/midas/social/api/WeiboAPI$2;->this$0:Lcom/vlingo/midas/social/api/WeiboAPI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Ljava/lang/String;)V
    .locals 7
    .param p1, "response"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x3

    .line 203
    const-wide/16 v2, 0x0

    .line 205
    .local v2, "userUID":J
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 206
    .local v1, "jSon":Lorg/json/JSONObject;
    const-string/jumbo v4, "uid"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 207
    const-string/jumbo v4, "uid"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 208
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    .line 209
    iget-object v4, p0, Lcom/vlingo/midas/social/api/WeiboAPI$2;->this$0:Lcom/vlingo/midas/social/api/WeiboAPI;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/social/api/WeiboAPI;->saveUserUID(Ljava/lang/Long;)V

    .line 210
    iget-object v4, p0, Lcom/vlingo/midas/social/api/WeiboAPI$2;->this$0:Lcom/vlingo/midas/social/api/WeiboAPI;

    invoke-virtual {v4}, Lcom/vlingo/midas/social/api/WeiboAPI;->fetchUserData()V

    .line 221
    .end local v1    # "jSon":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    return-void

    .line 215
    .restart local v1    # "jSon":Lorg/json/JSONObject;
    :cond_1
    iget-object v4, p0, Lcom/vlingo/midas/social/api/WeiboAPI$2;->this$0:Lcom/vlingo/midas/social/api/WeiboAPI;

    const/4 v5, 0x3

    # invokes: Lcom/vlingo/midas/social/api/WeiboAPI;->notifyError(I)V
    invoke-static {v4, v5}, Lcom/vlingo/midas/social/api/WeiboAPI;->access$100(Lcom/vlingo/midas/social/api/WeiboAPI;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 218
    .end local v1    # "jSon":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/vlingo/midas/social/api/WeiboAPI$2;->this$0:Lcom/vlingo/midas/social/api/WeiboAPI;

    # invokes: Lcom/vlingo/midas/social/api/WeiboAPI;->notifyError(I)V
    invoke-static {v4, v6}, Lcom/vlingo/midas/social/api/WeiboAPI;->access$100(Lcom/vlingo/midas/social/api/WeiboAPI;I)V

    goto :goto_0
.end method

.method public onError(Lcom/weibo/sdk/android/WeiboException;)V
    .locals 2
    .param p1, "arg0"    # Lcom/weibo/sdk/android/WeiboException;

    .prologue
    .line 198
    iget-object v0, p0, Lcom/vlingo/midas/social/api/WeiboAPI$2;->this$0:Lcom/vlingo/midas/social/api/WeiboAPI;

    const/4 v1, 0x3

    # invokes: Lcom/vlingo/midas/social/api/WeiboAPI;->notifyError(I)V
    invoke-static {v0, v1}, Lcom/vlingo/midas/social/api/WeiboAPI;->access$100(Lcom/vlingo/midas/social/api/WeiboAPI;I)V

    .line 199
    return-void
.end method

.method public onIOException(Ljava/io/IOException;)V
    .locals 2
    .param p1, "arg0"    # Ljava/io/IOException;

    .prologue
    .line 193
    iget-object v0, p0, Lcom/vlingo/midas/social/api/WeiboAPI$2;->this$0:Lcom/vlingo/midas/social/api/WeiboAPI;

    const/4 v1, 0x3

    # invokes: Lcom/vlingo/midas/social/api/WeiboAPI;->notifyException(I)V
    invoke-static {v0, v1}, Lcom/vlingo/midas/social/api/WeiboAPI;->access$000(Lcom/vlingo/midas/social/api/WeiboAPI;I)V

    .line 194
    return-void
.end method
