.class Lcom/vlingo/midas/social/SocialAccountActivity$11;
.super Ljava/lang/Object;
.source "SocialAccountActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/social/SocialAccountActivity;->fetchThumbnail()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/social/SocialAccountActivity;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/social/SocialAccountActivity;)V
    .locals 0

    .prologue
    .line 442
    iput-object p1, p0, Lcom/vlingo/midas/social/SocialAccountActivity$11;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 445
    iget-object v4, p0, Lcom/vlingo/midas/social/SocialAccountActivity$11;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # getter for: Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;
    invoke-static {v4}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$100(Lcom/vlingo/midas/social/SocialAccountActivity;)Lcom/vlingo/midas/social/api/SocialAPI;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/midas/social/api/SocialAPI;->getLoginUrl()Ljava/lang/String;

    move-result-object v3

    .line 450
    .local v3, "url":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 451
    .local v2, "picURL":Ljava/net/URL;
    invoke-virtual {v2}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-static {v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 452
    .local v1, "image":Landroid/graphics/Bitmap;
    iget-object v4, p0, Lcom/vlingo/midas/social/SocialAccountActivity$11;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # getter for: Lcom/vlingo/midas/social/SocialAccountActivity;->type:I
    invoke-static {v4}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$600(Lcom/vlingo/midas/social/SocialAccountActivity;)I

    move-result v4

    invoke-static {v4, v1}, Lcom/vlingo/midas/util/SocialUtils;->setNetworkPicture(ILandroid/graphics/Bitmap;)V

    .line 453
    invoke-virtual {v2}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 461
    .end local v1    # "image":Landroid/graphics/Bitmap;
    .end local v2    # "picURL":Ljava/net/URL;
    :goto_0
    iget-object v4, p0, Lcom/vlingo/midas/social/SocialAccountActivity$11;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    new-instance v5, Lcom/vlingo/midas/social/SocialAccountActivity$11$1;

    invoke-direct {v5, p0}, Lcom/vlingo/midas/social/SocialAccountActivity$11$1;-><init>(Lcom/vlingo/midas/social/SocialAccountActivity$11;)V

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/social/SocialAccountActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 493
    return-void

    .line 454
    :catch_0
    move-exception v0

    .line 455
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
