.class Lcom/vlingo/midas/social/SocialAccountActivity$10;
.super Ljava/lang/Object;
.source "SocialAccountActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/social/SocialAccountActivity;->onLoginComplete(IZLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

.field final synthetic val$errMessage:Ljava/lang/String;

.field final synthetic val$result:I


# direct methods
.method constructor <init>(Lcom/vlingo/midas/social/SocialAccountActivity;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lcom/vlingo/midas/social/SocialAccountActivity$10;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    iput p2, p0, Lcom/vlingo/midas/social/SocialAccountActivity$10;->val$result:I

    iput-object p3, p0, Lcom/vlingo/midas/social/SocialAccountActivity$10;->val$errMessage:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 413
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$10;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$10;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    iget-boolean v0, v0, Lcom/vlingo/midas/social/SocialAccountActivity;->mDestroyed:Z

    if-eqz v0, :cond_2

    .line 414
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$10;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # getter for: Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$100(Lcom/vlingo/midas/social/SocialAccountActivity;)Lcom/vlingo/midas/social/api/SocialAPI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/SocialAPI;->clearCredentials()V

    .line 431
    :cond_1
    :goto_0
    return-void

    .line 417
    :cond_2
    iget v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$10;->val$result:I

    if-nez v0, :cond_4

    .line 418
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$10;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_3

    .line 419
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$10;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    const/16 v1, 0xce

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->showDialog(I)V

    .line 420
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$10;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # getter for: Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$100(Lcom/vlingo/midas/social/SocialAccountActivity;)Lcom/vlingo/midas/social/api/SocialAPI;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/social/api/TwitterAPI;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/TwitterAPI;->verifyCredentials()V

    goto :goto_0

    .line 422
    :cond_4
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$10;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    iget v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity$10;->val$result:I

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->twitterResultFailed(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 423
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$10;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # getter for: Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$100(Lcom/vlingo/midas/social/SocialAccountActivity;)Lcom/vlingo/midas/social/api/SocialAPI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/SocialAPI;->clearCredentials()V

    .line 424
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$10;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity$10;->val$errMessage:Ljava/lang/String;

    # setter for: Lcom/vlingo/midas/social/SocialAccountActivity;->errorMessage:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$502(Lcom/vlingo/midas/social/SocialAccountActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 425
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$10;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 426
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$10;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    const/16 v1, 0xcc

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->showDialog(I)V

    goto :goto_0

    .line 428
    :cond_5
    iget v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$10;->val$result:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 429
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$10;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # invokes: Lcom/vlingo/midas/social/SocialAccountActivity;->cancelLoginDialog()V
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$200(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    goto :goto_0
.end method
