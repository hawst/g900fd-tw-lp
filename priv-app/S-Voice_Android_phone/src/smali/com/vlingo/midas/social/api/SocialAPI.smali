.class public abstract Lcom/vlingo/midas/social/api/SocialAPI;
.super Ljava/lang/Object;
.source "SocialAPI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/social/api/SocialAPI$SocialCallback;
    }
.end annotation


# static fields
.field public static final SOCIAL_NETWORK_TYPE_STRING_ALL:Ljava/lang/String; = "all"

.field public static final SOCIAL_NETWORK_TYPE_STRING_FACEBOOK:Ljava/lang/String; = "facebook"

.field public static final SOCIAL_NETWORK_TYPE_STRING_QZONE:Ljava/lang/String; = "qzone"

.field public static final SOCIAL_NETWORK_TYPE_STRING_TWITTER:Ljava/lang/String; = "twitter"

.field public static final SOCIAL_NETWORK_TYPE_STRING_WEIBO:Ljava/lang/String; = "weibo"

.field public static final TYPE_INTENT:I = 0x0

.field public static final UPDATE_STATUS_DONE:I = 0x2

.field public static final UPDATE_STATUS_ERR:I = 0x3

.field public static final UPDATE_STATUS_NONE:I = 0x0

.field public static final UPDATE_STATUS_WORKING:I = 0x1


# instance fields
.field private acceptedTextTagString:Ljava/lang/String;

.field private btnId:I

.field private charLimit:I

.field private enabled:Z

.field private hasLoginDialog:Z

.field private intentString:Ljava/lang/String;

.field private loginDialogId:I

.field private loginMessageId:I

.field private loginSetting:Ljava/lang/String;

.field private logoutDialogId:I

.field private logoutMessageId:I

.field private offImageResourceId:I

.field private onImageResourceId:I

.field private supportsLogout:Z

.field private typeId:I

.field private updateStatus:I

.field private urlId:Ljava/lang/String;

.field private wcisId:I


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIIIIIIIZZLjava/lang/String;)V
    .locals 2
    .param p1, "loginSetting"    # Ljava/lang/String;
    .param p2, "urlId"    # Ljava/lang/String;
    .param p3, "intentString"    # Ljava/lang/String;
    .param p4, "loginMessageId"    # I
    .param p5, "loginDialogId"    # I
    .param p6, "logoutMessageId"    # I
    .param p7, "logoutDialogId"    # I
    .param p8, "wcisId"    # I
    .param p9, "onImageResourceId"    # I
    .param p10, "offImageResourceId"    # I
    .param p11, "typeId"    # I
    .param p12, "btnId"    # I
    .param p13, "charLimit"    # I
    .param p14, "hasLoginDialog"    # Z
    .param p15, "supportsLogout"    # Z
    .param p16, "acceptedTextTagString"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v1, 0x0

    iput v1, p0, Lcom/vlingo/midas/social/api/SocialAPI;->updateStatus:I

    .line 53
    iput-object p2, p0, Lcom/vlingo/midas/social/api/SocialAPI;->urlId:Ljava/lang/String;

    .line 54
    iput-object p1, p0, Lcom/vlingo/midas/social/api/SocialAPI;->loginSetting:Ljava/lang/String;

    .line 55
    iput p4, p0, Lcom/vlingo/midas/social/api/SocialAPI;->loginMessageId:I

    .line 56
    iput p5, p0, Lcom/vlingo/midas/social/api/SocialAPI;->loginDialogId:I

    .line 57
    iput p6, p0, Lcom/vlingo/midas/social/api/SocialAPI;->logoutMessageId:I

    .line 58
    iput p7, p0, Lcom/vlingo/midas/social/api/SocialAPI;->logoutDialogId:I

    .line 59
    move/from16 v0, p14

    iput-boolean v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->hasLoginDialog:Z

    .line 60
    iput p8, p0, Lcom/vlingo/midas/social/api/SocialAPI;->wcisId:I

    .line 61
    iput p9, p0, Lcom/vlingo/midas/social/api/SocialAPI;->onImageResourceId:I

    .line 62
    iput p10, p0, Lcom/vlingo/midas/social/api/SocialAPI;->offImageResourceId:I

    .line 63
    iput-object p3, p0, Lcom/vlingo/midas/social/api/SocialAPI;->intentString:Ljava/lang/String;

    .line 64
    iput p11, p0, Lcom/vlingo/midas/social/api/SocialAPI;->typeId:I

    .line 65
    iput p12, p0, Lcom/vlingo/midas/social/api/SocialAPI;->btnId:I

    .line 66
    iput p13, p0, Lcom/vlingo/midas/social/api/SocialAPI;->charLimit:I

    .line 67
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->acceptedTextTagString:Ljava/lang/String;

    .line 68
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/vlingo/midas/social/api/SocialAPI;->enabled:Z

    .line 69
    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->supportsLogout:Z

    .line 70
    return-void
.end method


# virtual methods
.method public authorizeCallback(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 117
    return-void
.end method

.method public btnId()I
    .locals 1

    .prologue
    .line 176
    iget v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->btnId:I

    return v0
.end method

.method public charLimit()I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->charLimit:I

    return v0
.end method

.method public abstract clearCredentials()V
.end method

.method public abstract dismissDialogs()V
.end method

.method public followVlingo()Z
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x0

    return v0
.end method

.method public getAcceptedTextTagString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->acceptedTextTagString:Ljava/lang/String;

    return-object v0
.end method

.method public abstract getDialog()Landroid/app/Dialog;
.end method

.method public getLoginUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->urlId:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUpdateStatus()I
    .locals 1

    .prologue
    .line 188
    iget v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->updateStatus:I

    return v0
.end method

.method public includesLoginDialog()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->hasLoginDialog:Z

    return v0
.end method

.method public intentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->intentString:Ljava/lang/String;

    return-object v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 168
    iget-boolean v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->enabled:Z

    return v0
.end method

.method public isLoggedIn()Z
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->loginSetting:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public login(Landroid/app/Activity;)Z
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->hasLoginDialog:Z

    if-nez v0, :cond_0

    .line 105
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Calling wrong login method, call loginUserPW"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public loginDialogId()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->loginDialogId:I

    return v0
.end method

.method public loginMessageId()I
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->loginMessageId:I

    return v0
.end method

.method public loginUserPW(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->hasLoginDialog:Z

    if-eqz v0, :cond_0

    .line 111
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Calling wrong login method, call login"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public logoutDialogId()I
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->logoutDialogId:I

    return v0
.end method

.method public logoutMessageId()I
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->logoutMessageId:I

    return v0
.end method

.method public offImageResourceId()I
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->offImageResourceId:I

    return v0
.end method

.method public onImageResourceId()I
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->onImageResourceId:I

    return v0
.end method

.method public abstract refreshCredentials()V
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1, "on"    # Z

    .prologue
    .line 164
    iput-boolean p1, p0, Lcom/vlingo/midas/social/api/SocialAPI;->enabled:Z

    .line 165
    return-void
.end method

.method public setUpdateStatus(I)V
    .locals 0
    .param p1, "status"    # I

    .prologue
    .line 184
    iput p1, p0, Lcom/vlingo/midas/social/api/SocialAPI;->updateStatus:I

    .line 185
    return-void
.end method

.method public abstract showDialogs()V
.end method

.method public shutdown()V
    .locals 0

    .prologue
    .line 121
    return-void
.end method

.method public supportsLogout()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->supportsLogout:Z

    return v0
.end method

.method public typeId()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->typeId:I

    return v0
.end method

.method public abstract updateDialogs()V
.end method

.method public abstract updateStatus(Ljava/lang/String;)V
.end method

.method public wcisId()I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/vlingo/midas/social/api/SocialAPI;->wcisId:I

    return v0
.end method
