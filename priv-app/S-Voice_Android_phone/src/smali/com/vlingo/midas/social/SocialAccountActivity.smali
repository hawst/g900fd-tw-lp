.class public Lcom/vlingo/midas/social/SocialAccountActivity;
.super Lcom/vlingo/midas/ui/VLActivity;
.source "SocialAccountActivity.java"

# interfaces
.implements Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;
.implements Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;
.implements Lcom/vlingo/midas/social/api/WeiboAPI$WeiboCallBack;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/social/SocialAccountActivity$ScreenEventBroadcastReceiver;
    }
.end annotation


# static fields
.field public static final SHOW_POPUP_FOLLOW_VLINGO:I = 0xcd

.field public static final SHOW_POPUP_LOGGING_IN:I = 0xcb

.field public static final SHOW_POPUP_LOGGING_IN_FAIL:I = 0xcc

.field public static final SHOW_POPUP_LOGOUT:I = 0xca

.field public static final SHOW_POPUP_NO_GOOGLE_ACCOUNT:I = 0xd0

.field public static final SHOW_POPUP_NO_NETWORK:I = 0xcf

.field public static final SHOW_POPUP_PLEASE_WAIT:I = 0xce

.field public static final SHOW_POPUP_TEXT_ENTRY:I = 0xc9


# instance fields
.field private volatile curDialogID:I

.field private volatile errorMessage:Ljava/lang/String;

.field mDestroyed:Z

.field private volatile progressDialog:Landroid/app/ProgressDialog;

.field private screenEventBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

.field private type:I

.field private updateType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vlingo/midas/ui/VLActivity;-><init>()V

    .line 70
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->curDialogID:I

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->mDestroyed:Z

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    .line 586
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/social/SocialAccountActivity;)Lcom/vlingo/midas/social/api/SocialAPI;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/SocialAccountActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/social/SocialAccountActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/social/SocialAccountActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vlingo/midas/social/SocialAccountActivity;->cancelLoginDialog()V

    return-void
.end method

.method static synthetic access$300(Lcom/vlingo/midas/social/SocialAccountActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/social/SocialAccountActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vlingo/midas/social/SocialAccountActivity;->removeCurrentDialog()V

    return-void
.end method

.method static synthetic access$400(Lcom/vlingo/midas/social/SocialAccountActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/social/SocialAccountActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vlingo/midas/social/SocialAccountActivity;->fetchThumbnail()V

    return-void
.end method

.method static synthetic access$500(Lcom/vlingo/midas/social/SocialAccountActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/SocialAccountActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->errorMessage:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/vlingo/midas/social/SocialAccountActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/social/SocialAccountActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->errorMessage:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/vlingo/midas/social/SocialAccountActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/SocialAccountActivity;

    .prologue
    .line 54
    iget v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->type:I

    return v0
.end method

.method static synthetic access$700(Lcom/vlingo/midas/social/SocialAccountActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/SocialAccountActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->updateType:Ljava/lang/String;

    return-object v0
.end method

.method private cancelLoginDialog()V
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/SocialAPI;->clearCredentials()V

    .line 252
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/vlingo/midas/util/SocialUtils;->loginIntent(Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 253
    invoke-virtual {p0}, Lcom/vlingo/midas/social/SocialAccountActivity;->finish()V

    .line 254
    return-void
.end method

.method private fetchThumbnail()V
    .locals 2

    .prologue
    .line 442
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/vlingo/midas/social/SocialAccountActivity$11;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/social/SocialAccountActivity$11;-><init>(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 495
    return-void
.end method

.method private haveConnection()Z
    .locals 8

    .prologue
    .line 127
    const-string/jumbo v6, "connectivity"

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/social/SocialAccountActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 128
    .local v1, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getAllNetworkInfo()[Landroid/net/NetworkInfo;

    move-result-object v5

    .line 129
    .local v5, "ni":[Landroid/net/NetworkInfo;
    if-eqz v5, :cond_2

    .line 130
    move-object v0, v5

    .local v0, "arr$":[Landroid/net/NetworkInfo;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    .line 131
    .local v4, "n":Landroid/net/NetworkInfo;
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 132
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "MOBILE"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "WIFI"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "WIMAX"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "MOBILE2"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "BLUETOOTH_TETHER"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 133
    :cond_0
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 134
    const/4 v6, 0x1

    .line 138
    .end local v0    # "arr$":[Landroid/net/NetworkInfo;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "n":Landroid/net/NetworkInfo;
    :goto_1
    return v6

    .line 130
    .restart local v0    # "arr$":[Landroid/net/NetworkInfo;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    .restart local v4    # "n":Landroid/net/NetworkInfo;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 138
    .end local v0    # "arr$":[Landroid/net/NetworkInfo;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "n":Landroid/net/NetworkInfo;
    :cond_2
    const/4 v6, 0x0

    goto :goto_1
.end method

.method private removeCurrentDialog()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 257
    iget v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->curDialogID:I

    if-eq v0, v1, :cond_0

    .line 258
    iget v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->curDialogID:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->removeDialog(I)V

    .line 259
    iput v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->curDialogID:I

    .line 261
    :cond_0
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 143
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/midas/ui/VLActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 144
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v0, p1, p2, p3}, Lcom/vlingo/midas/social/api/SocialAPI;->authorizeCallback(IILandroid/content/Intent;)V

    .line 145
    return-void
.end method

.method public onCheckinResult(ZLjava/util/Hashtable;)V
    .locals 0
    .param p1, "success"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 353
    .local p2, "params":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Object;Ljava/lang/Object;>;"
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 580
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 581
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    if-eqz v0, :cond_0

    .line 582
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/SocialAPI;->updateDialogs()V

    .line 584
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v4, 0xca

    const/4 v3, 0x0

    .line 79
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onCreate(Landroid/os/Bundle;)V

    .line 83
    invoke-static {p0}, Lcom/vlingo/midas/util/SocialUtils;->getAPIs(Lcom/vlingo/midas/social/api/SocialAPI$SocialCallback;)Lcom/vlingo/core/internal/util/SparseArrayMap;

    move-result-object v0

    .line 84
    .local v0, "apis":Lcom/vlingo/core/internal/util/SparseArrayMap;, "Lcom/vlingo/core/internal/util/SparseArrayMap<Lcom/vlingo/midas/social/api/SocialAPI;>;"
    invoke-virtual {p0}, Lcom/vlingo/midas/social/SocialAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "android.intent.extra.INTENT"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->type:I

    .line 85
    iget v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->type:I

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/SparseArrayMap;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/social/api/SocialAPI;

    iput-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    .line 86
    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    if-nez v1, :cond_0

    .line 87
    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "Must specify an Intent.EXTRA_TYPE of either TWITTER (2), or FACEBOOK(3), or QZone(4), or Weibo(5)"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 91
    :cond_0
    const-string/jumbo v1, "twitter_dialog_flags"

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_1

    .line 92
    const-string/jumbo v1, "twitter_dialog_flags"

    const/16 v2, 0x220

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 94
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/social/SocialAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "choice"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 95
    invoke-virtual {p0}, Lcom/vlingo/midas/social/SocialAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "choice"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->updateType:Ljava/lang/String;

    .line 97
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/social/SocialAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "android.intent.action.MAIN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 98
    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v1}, Lcom/vlingo/midas/social/api/SocialAPI;->isLoggedIn()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v1}, Lcom/vlingo/midas/social/api/SocialAPI;->supportsLogout()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/vlingo/midas/social/SocialAccountActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_5

    .line 99
    invoke-virtual {p0}, Lcom/vlingo/midas/social/SocialAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "logout_social_network"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 100
    invoke-virtual {p0, v4}, Lcom/vlingo/midas/social/SocialAccountActivity;->showDialog(I)V

    .line 124
    :cond_3
    :goto_0
    return-void

    .line 101
    :cond_4
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v1

    if-nez v1, :cond_3

    .line 102
    invoke-virtual {p0, v4}, Lcom/vlingo/midas/social/SocialAccountActivity;->showDialog(I)V

    goto :goto_0

    .line 106
    :cond_5
    invoke-direct {p0}, Lcom/vlingo/midas/social/SocialAccountActivity;->haveConnection()Z

    move-result v1

    if-nez v1, :cond_6

    invoke-virtual {p0}, Lcom/vlingo/midas/social/SocialAccountActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_6

    .line 107
    sget v1, Lcom/vlingo/midas/R$string;->no_network:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->errorMessage:Ljava/lang/String;

    .line 108
    const/16 v1, 0xcc

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->showDialog(I)V

    goto :goto_0

    .line 110
    :cond_6
    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v1}, Lcom/vlingo/midas/social/api/SocialAPI;->includesLoginDialog()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 111
    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v1}, Lcom/vlingo/midas/social/api/SocialAPI;->intentType()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "facebook"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->facebookSSO()Z

    move-result v1

    if-nez v1, :cond_3

    .line 113
    :cond_7
    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v1}, Lcom/vlingo/midas/social/api/SocialAPI;->intentType()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "twitter"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->twitterSSO()Z

    move-result v1

    if-nez v1, :cond_3

    .line 116
    :cond_8
    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v1, p0}, Lcom/vlingo/midas/social/api/SocialAPI;->login(Landroid/app/Activity;)Z

    goto :goto_0

    .line 120
    :cond_9
    invoke-virtual {p0}, Lcom/vlingo/midas/social/SocialAccountActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_3

    .line 121
    const/16 v1, 0xc9

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->showDialog(I)V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .param p1, "id"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 264
    iput p1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->curDialogID:I

    .line 265
    packed-switch p1, :pswitch_data_0

    .line 345
    :pswitch_0
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    :goto_0
    return-object v1

    .line 267
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 268
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v2}, Lcom/vlingo/midas/social/api/SocialAPI;->logoutDialogId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 269
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v2}, Lcom/vlingo/midas/social/api/SocialAPI;->logoutMessageId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 271
    sget v1, Lcom/vlingo/midas/R$string;->social_no:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/vlingo/midas/social/SocialAccountActivity$1;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/social/SocialAccountActivity$1;-><init>(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 277
    sget v1, Lcom/vlingo/midas/R$string;->social_yes:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/vlingo/midas/social/SocialAccountActivity$2;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/social/SocialAccountActivity$2;-><init>(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 289
    new-instance v1, Lcom/vlingo/midas/social/SocialAccountActivity$3;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/social/SocialAccountActivity$3;-><init>(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 294
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    .line 297
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :pswitch_2
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->progressDialog:Landroid/app/ProgressDialog;

    .line 298
    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v3}, Lcom/vlingo/midas/social/api/SocialAPI;->wcisId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 299
    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->progressDialog:Landroid/app/ProgressDialog;

    sget v2, Lcom/vlingo/midas/R$string;->social_wait_logging_in:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/social/SocialAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 300
    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 301
    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 302
    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->progressDialog:Landroid/app/ProgressDialog;

    goto/16 :goto_0

    .line 304
    :pswitch_3
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->progressDialog:Landroid/app/ProgressDialog;

    .line 305
    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v3}, Lcom/vlingo/midas/social/api/SocialAPI;->wcisId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 306
    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->progressDialog:Landroid/app/ProgressDialog;

    sget v2, Lcom/vlingo/midas/R$string;->social_wait_logging_in:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/social/SocialAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 307
    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 308
    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 309
    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->progressDialog:Landroid/app/ProgressDialog;

    goto/16 :goto_0

    .line 311
    :pswitch_4
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/vlingo/midas/R$string;->car_iux_oops:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/social/SocialAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->errorMessage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->car_iux_ok:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/social/SocialAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/midas/social/SocialAccountActivity$5;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/social/SocialAccountActivity$5;-><init>(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/vlingo/midas/social/SocialAccountActivity$4;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/social/SocialAccountActivity$4;-><init>(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto/16 :goto_0

    .line 326
    :pswitch_5
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/vlingo/midas/R$string;->car_iux_oops:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/social/SocialAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->facebooksso_google_account:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/social/SocialAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->car_iux_ok:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/social/SocialAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vlingo/midas/social/SocialAccountActivity$7;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/social/SocialAccountActivity$7;-><init>(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/vlingo/midas/social/SocialAccountActivity$6;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/social/SocialAccountActivity$6;-><init>(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto/16 :goto_0

    .line 265
    :pswitch_data_0
    .packed-switch 0xca
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 149
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onDestroy()V

    .line 151
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/SocialAPI;->shutdown()V

    .line 155
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->mDestroyed:Z

    .line 156
    return-void
.end method

.method public onFacebookAPILogin(Lcom/vlingo/midas/social/api/FacebookAPI;ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "api"    # Lcom/vlingo/midas/social/api/FacebookAPI;
    .param p2, "responseType"    # I
    .param p3, "params"    # Landroid/os/Bundle;

    .prologue
    .line 510
    new-instance v0, Lcom/vlingo/midas/social/SocialAccountActivity$12;

    invoke-direct {v0, p0, p2, p1}, Lcom/vlingo/midas/social/SocialAccountActivity$12;-><init>(Lcom/vlingo/midas/social/SocialAccountActivity;ILcom/vlingo/midas/social/api/FacebookAPI;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 535
    return-void
.end method

.method public onFacebookAPIMethod(Lcom/vlingo/midas/social/api/FacebookAPI;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "api"    # Lcom/vlingo/midas/social/api/FacebookAPI;
    .param p2, "responseType"    # I
    .param p3, "method"    # Ljava/lang/String;
    .param p4, "params"    # Landroid/os/Bundle;

    .prologue
    .line 538
    new-instance v0, Lcom/vlingo/midas/social/SocialAccountActivity$13;

    invoke-direct {v0, p0, p3, p2, p4}, Lcom/vlingo/midas/social/SocialAccountActivity$13;-><init>(Lcom/vlingo/midas/social/SocialAccountActivity;Ljava/lang/String;ILandroid/os/Bundle;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 565
    return-void
.end method

.method public onFollowVlingoComplete(ILjava/lang/String;)V
    .locals 0
    .param p1, "result"    # I
    .param p2, "errMessage"    # Ljava/lang/String;

    .prologue
    .line 570
    return-void
.end method

.method public onLoginComplete(IZLjava/lang/String;)V
    .locals 1
    .param p1, "result"    # I
    .param p2, "success"    # Z
    .param p3, "errMessage"    # Ljava/lang/String;

    .prologue
    .line 411
    new-instance v0, Lcom/vlingo/midas/social/SocialAccountActivity$10;

    invoke-direct {v0, p0, p1, p3}, Lcom/vlingo/midas/social/SocialAccountActivity$10;-><init>(Lcom/vlingo/midas/social/SocialAccountActivity;ILjava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 433
    return-void
.end method

.method public onLoginResult(ZLjava/util/Hashtable;)V
    .locals 1
    .param p1, "success"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 356
    .local p2, "params":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Object;Ljava/lang/Object;>;"
    new-instance v0, Lcom/vlingo/midas/social/SocialAccountActivity$8;

    invoke-direct {v0, p0, p1, p2}, Lcom/vlingo/midas/social/SocialAccountActivity$8;-><init>(Lcom/vlingo/midas/social/SocialAccountActivity;ZLjava/util/Hashtable;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 379
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 160
    iget v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->curDialogID:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 161
    iget v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->curDialogID:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->removeDialog(I)V

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->screenEventBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 165
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->screenEventBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 166
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->screenEventBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 173
    :cond_1
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onPause()V

    .line 174
    return-void
.end method

.method protected onResume()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 178
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onResume()V

    .line 180
    new-instance v2, Lcom/vlingo/midas/social/SocialAccountActivity$ScreenEventBroadcastReceiver;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/vlingo/midas/social/SocialAccountActivity$ScreenEventBroadcastReceiver;-><init>(Lcom/vlingo/midas/social/SocialAccountActivity;Lcom/vlingo/midas/social/SocialAccountActivity$1;)V

    iput-object v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->screenEventBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 181
    iget-object v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->screenEventBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string/jumbo v4, "android.intent.action.SCREEN_OFF"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/vlingo/midas/social/SocialAccountActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 183
    const/4 v0, 0x0

    .line 185
    .local v0, "dialog":Lcom/facebook/android/FbDialog;
    iget-object v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v2}, Lcom/vlingo/midas/social/api/SocialAPI;->intentType()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "facebook"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->facebookSSO()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 186
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->isFacebookLoggedIn()Z

    move-result v2

    if-nez v2, :cond_1

    .line 187
    iget-object v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->updateType:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->updateType:Ljava/lang/String;

    const-string/jumbo v3, "all"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 188
    invoke-static {p0, v7, v5}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->startFacebookSSO(Landroid/content/Context;ZZ)V

    .line 192
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/midas/social/SocialAccountActivity;->finish()V

    .line 244
    :goto_1
    return-void

    .line 190
    :cond_0
    invoke-static {p0, v5}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->startFacebookSSO(Landroid/content/Context;Z)V

    goto :goto_0

    .line 194
    :cond_1
    iget v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->curDialogID:I

    if-eq v2, v6, :cond_2

    invoke-virtual {p0}, Lcom/vlingo/midas/social/SocialAccountActivity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_2

    .line 195
    iget v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->curDialogID:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/social/SocialAccountActivity;->showDialog(I)V

    .line 236
    :cond_2
    :goto_2
    iget-object v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    if-eqz v2, :cond_3

    .line 237
    iget-object v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v2}, Lcom/vlingo/midas/social/api/SocialAPI;->updateDialogs()V

    .line 240
    :cond_3
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 241
    .local v1, "i":Landroid/content/Intent;
    const-string/jumbo v2, "com.vlingo.client.app.action.ACTIVITY_STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 242
    const-string/jumbo v2, "com.vlingo.client.app.extra.STATE"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 243
    invoke-virtual {p0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_1

    .line 197
    .end local v1    # "i":Landroid/content/Intent;
    :cond_4
    iget-object v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v2}, Lcom/vlingo/midas/social/api/SocialAPI;->intentType()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "twitter"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->twitterSSO()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 198
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->isTwitterLoggedIn()Z

    move-result v2

    if-nez v2, :cond_6

    .line 199
    iget-object v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->updateType:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->updateType:Ljava/lang/String;

    const-string/jumbo v3, "all"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 200
    invoke-static {p0, v7, v5}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->startTwitterSSO(Landroid/content/Context;ZZ)V

    .line 204
    :goto_3
    invoke-virtual {p0}, Lcom/vlingo/midas/social/SocialAccountActivity;->finish()V

    goto :goto_1

    .line 202
    :cond_5
    invoke-static {p0, v5}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->startTwitterSSO(Landroid/content/Context;Z)V

    goto :goto_3

    .line 206
    :cond_6
    iget v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->curDialogID:I

    if-eq v2, v6, :cond_2

    invoke-virtual {p0}, Lcom/vlingo/midas/social/SocialAccountActivity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_2

    .line 207
    iget v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->curDialogID:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/social/SocialAccountActivity;->showDialog(I)V

    goto :goto_2

    .line 210
    :cond_7
    iget-object v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v2}, Lcom/vlingo/midas/social/api/SocialAPI;->intentType()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "facebook"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v2}, Lcom/vlingo/midas/social/api/SocialAPI;->intentType()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "weibo"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 211
    :cond_8
    iget v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->curDialogID:I

    if-eq v2, v6, :cond_9

    invoke-virtual {p0}, Lcom/vlingo/midas/social/SocialAccountActivity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_9

    .line 212
    iget v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->curDialogID:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/social/SocialAccountActivity;->showDialog(I)V

    .line 215
    :cond_9
    iget-object v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    if-eqz v2, :cond_2

    .line 216
    iget-object v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v2}, Lcom/vlingo/midas/social/api/SocialAPI;->showDialogs()V

    goto/16 :goto_2

    .line 219
    :cond_a
    iget-object v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v2}, Lcom/vlingo/midas/social/api/SocialAPI;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .end local v0    # "dialog":Lcom/facebook/android/FbDialog;
    check-cast v0, Lcom/facebook/android/FbDialog;

    .line 220
    .restart local v0    # "dialog":Lcom/facebook/android/FbDialog;
    if-eqz v0, :cond_2

    .line 221
    iget-boolean v2, v0, Lcom/facebook/android/FbDialog;->isDialogActive:Z

    if-eqz v2, :cond_c

    .line 222
    iget v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->curDialogID:I

    if-eq v2, v6, :cond_b

    invoke-virtual {p0}, Lcom/vlingo/midas/social/SocialAccountActivity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_b

    .line 223
    iget v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->curDialogID:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/social/SocialAccountActivity;->showDialog(I)V

    .line 225
    :cond_b
    iget-object v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    if-eqz v2, :cond_2

    .line 226
    iget-object v2, p0, Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;

    invoke-virtual {v2}, Lcom/vlingo/midas/social/api/SocialAPI;->showDialogs()V

    goto/16 :goto_2

    .line 230
    :cond_c
    iput-boolean v7, v0, Lcom/facebook/android/FbDialog;->isDialogActive:Z

    .line 231
    invoke-virtual {p0}, Lcom/vlingo/midas/social/SocialAccountActivity;->finish()V

    goto/16 :goto_2
.end method

.method public onUpdateComplete(ILjava/lang/String;)V
    .locals 0
    .param p1, "result"    # I
    .param p2, "errMessage"    # Ljava/lang/String;

    .prologue
    .line 386
    return-void
.end method

.method public onVerifyComplete(ILjava/lang/String;)V
    .locals 1
    .param p1, "result"    # I
    .param p2, "errMessage"    # Ljava/lang/String;

    .prologue
    .line 389
    new-instance v0, Lcom/vlingo/midas/social/SocialAccountActivity$9;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/midas/social/SocialAccountActivity$9;-><init>(Lcom/vlingo/midas/social/SocialAccountActivity;I)V

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 408
    return-void
.end method

.method public onVlingoFriendshipExists(IZLjava/lang/String;)V
    .locals 0
    .param p1, "result"    # I
    .param p2, "exists"    # Z
    .param p3, "errMessage"    # Ljava/lang/String;

    .prologue
    .line 576
    return-void
.end method

.method public onWeiboFail(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 606
    if-nez p1, :cond_0

    .line 607
    invoke-direct {p0}, Lcom/vlingo/midas/social/SocialAccountActivity;->cancelLoginDialog()V

    .line 609
    :cond_0
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 610
    new-instance v0, Lcom/vlingo/midas/social/SocialAccountActivity$14;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/social/SocialAccountActivity$14;-><init>(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 621
    :cond_1
    return-void
.end method

.method public onWeiboSuccess(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 625
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 626
    new-instance v0, Lcom/vlingo/midas/social/SocialAccountActivity$15;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/social/SocialAccountActivity$15;-><init>(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 631
    :cond_0
    return-void
.end method

.method public twitterResultFailed(I)Z
    .locals 2
    .param p1, "result"    # I

    .prologue
    const/4 v0, 0x1

    .line 437
    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
