.class Lcom/vlingo/midas/social/SocialAccountActivity$14;
.super Ljava/lang/Object;
.source "SocialAccountActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/social/SocialAccountActivity;->onWeiboFail(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/social/SocialAccountActivity;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/social/SocialAccountActivity;)V
    .locals 0

    .prologue
    .line 610
    iput-object p1, p0, Lcom/vlingo/midas/social/SocialAccountActivity$14;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 612
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$14;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # getter for: Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$100(Lcom/vlingo/midas/social/SocialAccountActivity;)Lcom/vlingo/midas/social/api/SocialAPI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/SocialAPI;->clearCredentials()V

    .line 613
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$14;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # invokes: Lcom/vlingo/midas/social/SocialAccountActivity;->removeCurrentDialog()V
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$300(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    .line 614
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$14;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity$14;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    sget v2, Lcom/vlingo/midas/R$string;->social_weibo_login_error:I

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/social/SocialAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/vlingo/midas/social/SocialAccountActivity;->errorMessage:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$502(Lcom/vlingo/midas/social/SocialAccountActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 615
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$14;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 616
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$14;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    const/16 v1, 0xcc

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->showDialog(I)V

    .line 617
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$14;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # invokes: Lcom/vlingo/midas/social/SocialAccountActivity;->cancelLoginDialog()V
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$200(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    .line 618
    return-void
.end method
