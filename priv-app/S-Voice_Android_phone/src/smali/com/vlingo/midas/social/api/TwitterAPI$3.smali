.class Lcom/vlingo/midas/social/api/TwitterAPI$3;
.super Ljava/lang/Thread;
.source "TwitterAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/social/api/TwitterAPI;->onComplete(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

.field final synthetic val$otoken:Ljava/lang/String;

.field final synthetic val$verifier:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/social/api/TwitterAPI;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 606
    iput-object p1, p0, Lcom/vlingo/midas/social/api/TwitterAPI$3;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    iput-object p2, p0, Lcom/vlingo/midas/social/api/TwitterAPI$3;->val$otoken:Ljava/lang/String;

    iput-object p3, p0, Lcom/vlingo/midas/social/api/TwitterAPI$3;->val$verifier:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    .line 609
    iget-object v3, p0, Lcom/vlingo/midas/social/api/TwitterAPI$3;->val$otoken:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/vlingo/midas/social/api/TwitterAPI$3;->val$verifier:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 612
    :try_start_0
    iget-object v3, p0, Lcom/vlingo/midas/social/api/TwitterAPI$3;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->shutdown:Z
    invoke-static {v3}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$1100(Lcom/vlingo/midas/social/api/TwitterAPI;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 638
    :goto_0
    return-void

    .line 613
    :cond_0
    iget-object v3, p0, Lcom/vlingo/midas/social/api/TwitterAPI$3;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->mProvider:Loauth/signpost/OAuthProvider;
    invoke-static {v3}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$800(Lcom/vlingo/midas/social/api/TwitterAPI;)Loauth/signpost/OAuthProvider;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/midas/social/api/TwitterAPI$3;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->mConsumer:Loauth/signpost/OAuthConsumer;
    invoke-static {v4}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$600(Lcom/vlingo/midas/social/api/TwitterAPI;)Loauth/signpost/OAuthConsumer;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/midas/social/api/TwitterAPI$3;->val$verifier:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Loauth/signpost/OAuthProvider;->retrieveAccessToken(Loauth/signpost/OAuthConsumer;Ljava/lang/String;)V

    .line 614
    const-string/jumbo v3, "twitter_user_token"

    iget-object v4, p0, Lcom/vlingo/midas/social/api/TwitterAPI$3;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->mConsumer:Loauth/signpost/OAuthConsumer;
    invoke-static {v4}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$600(Lcom/vlingo/midas/social/api/TwitterAPI;)Loauth/signpost/OAuthConsumer;

    move-result-object v4

    invoke-interface {v4}, Loauth/signpost/OAuthConsumer;->getToken()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    const-string/jumbo v3, "twitter_user_secret"

    iget-object v4, p0, Lcom/vlingo/midas/social/api/TwitterAPI$3;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->mConsumer:Loauth/signpost/OAuthConsumer;
    invoke-static {v4}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$600(Lcom/vlingo/midas/social/api/TwitterAPI;)Loauth/signpost/OAuthConsumer;

    move-result-object v4

    invoke-interface {v4}, Loauth/signpost/OAuthConsumer;->getTokenSecret()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    const-string/jumbo v3, "twitter_request_token"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    const-string/jumbo v3, "twitter_request_secret"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    iget-object v3, p0, Lcom/vlingo/midas/social/api/TwitterAPI$3;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->callback:Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;
    invoke-static {v3}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$1000(Lcom/vlingo/midas/social/api/TwitterAPI;)Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-interface {v3, v4, v5, v6}, Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;->onLoginComplete(IZLjava/lang/String;)V
    :try_end_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Loauth/signpost/exception/OAuthNotAuthorizedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Loauth/signpost/exception/OAuthCommunicationException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 621
    :catch_0
    move-exception v2

    .line 622
    .local v2, "e":Loauth/signpost/exception/OAuthMessageSignerException;
    invoke-virtual {v2}, Loauth/signpost/exception/OAuthMessageSignerException;->printStackTrace()V

    .line 631
    .end local v2    # "e":Loauth/signpost/exception/OAuthMessageSignerException;
    :goto_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_err_auth1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 632
    .local v0, "authErr1Msg":Ljava/lang/String;
    iget-object v3, p0, Lcom/vlingo/midas/social/api/TwitterAPI$3;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->callback:Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;
    invoke-static {v3}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$1000(Lcom/vlingo/midas/social/api/TwitterAPI;)Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;

    move-result-object v3

    invoke-interface {v3, v8, v7, v0}, Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;->onLoginComplete(IZLjava/lang/String;)V

    goto :goto_0

    .line 623
    .end local v0    # "authErr1Msg":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 624
    .local v2, "e":Loauth/signpost/exception/OAuthNotAuthorizedException;
    invoke-virtual {v2}, Loauth/signpost/exception/OAuthNotAuthorizedException;->printStackTrace()V

    goto :goto_1

    .line 625
    .end local v2    # "e":Loauth/signpost/exception/OAuthNotAuthorizedException;
    :catch_2
    move-exception v2

    .line 626
    .local v2, "e":Loauth/signpost/exception/OAuthExpectationFailedException;
    invoke-virtual {v2}, Loauth/signpost/exception/OAuthExpectationFailedException;->printStackTrace()V

    goto :goto_1

    .line 627
    .end local v2    # "e":Loauth/signpost/exception/OAuthExpectationFailedException;
    :catch_3
    move-exception v2

    .line 628
    .local v2, "e":Loauth/signpost/exception/OAuthCommunicationException;
    invoke-virtual {v2}, Loauth/signpost/exception/OAuthCommunicationException;->printStackTrace()V

    goto :goto_1

    .line 635
    .end local v2    # "e":Loauth/signpost/exception/OAuthCommunicationException;
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_err_auth2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    .line 636
    .local v1, "authErr2Msg":Ljava/lang/String;
    iget-object v3, p0, Lcom/vlingo/midas/social/api/TwitterAPI$3;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->callback:Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;
    invoke-static {v3}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$1000(Lcom/vlingo/midas/social/api/TwitterAPI;)Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;

    move-result-object v3

    invoke-interface {v3, v8, v7, v1}, Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;->onLoginComplete(IZLjava/lang/String;)V

    goto/16 :goto_0
.end method
