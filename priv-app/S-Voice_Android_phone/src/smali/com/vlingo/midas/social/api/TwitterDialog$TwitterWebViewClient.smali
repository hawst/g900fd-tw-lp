.class Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "TwitterDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/social/api/TwitterDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TwitterWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/social/api/TwitterDialog;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/social/api/TwitterDialog;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/social/api/TwitterDialog;Lcom/vlingo/midas/social/api/TwitterDialog$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/social/api/TwitterDialog;
    .param p2, "x1"    # Lcom/vlingo/midas/social/api/TwitterDialog$1;

    .prologue
    .line 229
    invoke-direct {p0, p1}, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;-><init>(Lcom/vlingo/midas/social/api/TwitterDialog;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 292
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 293
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    # getter for: Lcom/vlingo/midas/social/api/TwitterDialog;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/vlingo/midas/social/api/TwitterDialog;->access$800(Lcom/vlingo/midas/social/api/TwitterDialog;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient$1;-><init>(Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 305
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 265
    invoke-static {p2}, Lcom/vlingo/midas/social/api/TwitterAPI;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 266
    invoke-static {p2}, Lcom/facebook/android/Util;->parseUrl(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 267
    .local v2, "values":Landroid/os/Bundle;
    const-string/jumbo v4, "error_reason"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 268
    .local v1, "error":Ljava/lang/String;
    const-string/jumbo v4, "denied"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 269
    .local v0, "denied":Ljava/lang/String;
    const-string/jumbo v4, "done"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    .line 270
    .local v3, "wasCancelled":Z
    if-eqz v3, :cond_1

    .line 271
    iget-object v4, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    # getter for: Lcom/vlingo/midas/social/api/TwitterDialog;->mListener:Lcom/vlingo/midas/social/api/TwitterDialogListener;
    invoke-static {v4}, Lcom/vlingo/midas/social/api/TwitterDialog;->access$100(Lcom/vlingo/midas/social/api/TwitterDialog;)Lcom/vlingo/midas/social/api/TwitterDialogListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/midas/social/api/TwitterDialogListener;->onCancel()V

    .line 279
    :goto_0
    iget-object v4, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    invoke-virtual {v4}, Lcom/vlingo/midas/social/api/TwitterDialog;->dismiss()V

    .line 286
    .end local v0    # "denied":Ljava/lang/String;
    .end local v1    # "error":Ljava/lang/String;
    .end local v2    # "values":Landroid/os/Bundle;
    .end local v3    # "wasCancelled":Z
    :cond_0
    :goto_1
    return-void

    .line 272
    .restart local v0    # "denied":Ljava/lang/String;
    .restart local v1    # "error":Ljava/lang/String;
    .restart local v2    # "values":Landroid/os/Bundle;
    .restart local v3    # "wasCancelled":Z
    :cond_1
    if-eqz v1, :cond_2

    .line 273
    iget-object v4, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    # getter for: Lcom/vlingo/midas/social/api/TwitterDialog;->mListener:Lcom/vlingo/midas/social/api/TwitterDialogListener;
    invoke-static {v4}, Lcom/vlingo/midas/social/api/TwitterDialog;->access$100(Lcom/vlingo/midas/social/api/TwitterDialog;)Lcom/vlingo/midas/social/api/TwitterDialogListener;

    move-result-object v4

    invoke-interface {v4, v1}, Lcom/vlingo/midas/social/api/TwitterDialogListener;->onTwitterError(Ljava/lang/String;)V

    goto :goto_0

    .line 274
    :cond_2
    if-eqz v0, :cond_3

    .line 275
    iget-object v4, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    # getter for: Lcom/vlingo/midas/social/api/TwitterDialog;->mListener:Lcom/vlingo/midas/social/api/TwitterDialogListener;
    invoke-static {v4}, Lcom/vlingo/midas/social/api/TwitterDialog;->access$100(Lcom/vlingo/midas/social/api/TwitterDialog;)Lcom/vlingo/midas/social/api/TwitterDialogListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/midas/social/api/TwitterDialogListener;->onCancel()V

    goto :goto_0

    .line 277
    :cond_3
    iget-object v4, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    # getter for: Lcom/vlingo/midas/social/api/TwitterDialog;->mListener:Lcom/vlingo/midas/social/api/TwitterDialogListener;
    invoke-static {v4}, Lcom/vlingo/midas/social/api/TwitterDialog;->access$100(Lcom/vlingo/midas/social/api/TwitterDialog;)Lcom/vlingo/midas/social/api/TwitterDialogListener;

    move-result-object v4

    invoke-interface {v4, v2}, Lcom/vlingo/midas/social/api/TwitterDialogListener;->onComplete(Landroid/os/Bundle;)V

    goto :goto_0

    .line 282
    .end local v0    # "denied":Ljava/lang/String;
    .end local v1    # "error":Ljava/lang/String;
    .end local v2    # "values":Landroid/os/Bundle;
    .end local v3    # "wasCancelled":Z
    :cond_4
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 283
    iget-object v4, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    # getter for: Lcom/vlingo/midas/social/api/TwitterDialog;->mSpinner:Landroid/app/ProgressDialog;
    invoke-static {v4}, Lcom/vlingo/midas/social/api/TwitterDialog;->access$300(Lcom/vlingo/midas/social/api/TwitterDialog;)Landroid/app/ProgressDialog;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    # invokes: Lcom/vlingo/midas/social/api/TwitterDialog;->isWindowShowing()Z
    invoke-static {v4}, Lcom/vlingo/midas/social/api/TwitterDialog;->access$400(Lcom/vlingo/midas/social/api/TwitterDialog;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    # getter for: Lcom/vlingo/midas/social/api/TwitterDialog;->mSpinner:Landroid/app/ProgressDialog;
    invoke-static {v4}, Lcom/vlingo/midas/social/api/TwitterDialog;->access$300(Lcom/vlingo/midas/social/api/TwitterDialog;)Landroid/app/ProgressDialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    # getter for: Lcom/vlingo/midas/social/api/TwitterDialog;->mSpinner:Landroid/app/ProgressDialog;
    invoke-static {v4}, Lcom/vlingo/midas/social/api/TwitterDialog;->access$300(Lcom/vlingo/midas/social/api/TwitterDialog;)Landroid/app/ProgressDialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    if-eqz v4, :cond_0

    goto :goto_1
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 256
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 257
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    # getter for: Lcom/vlingo/midas/social/api/TwitterDialog;->mListener:Lcom/vlingo/midas/social/api/TwitterDialogListener;
    invoke-static {v0}, Lcom/vlingo/midas/social/api/TwitterDialog;->access$100(Lcom/vlingo/midas/social/api/TwitterDialog;)Lcom/vlingo/midas/social/api/TwitterDialogListener;

    move-result-object v0

    invoke-interface {v0, p3}, Lcom/vlingo/midas/social/api/TwitterDialogListener;->onError(Ljava/lang/String;)V

    .line 258
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/TwitterDialog;->dismiss()V

    .line 259
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 5
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 235
    invoke-static {p2}, Lcom/facebook/android/Util;->parseUrl(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 236
    .local v2, "values":Landroid/os/Bundle;
    const-string/jumbo v4, "error_reason"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 237
    .local v1, "error":Ljava/lang/String;
    const-string/jumbo v4, "denied"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 238
    .local v0, "denied":Ljava/lang/String;
    invoke-static {p2}, Lcom/vlingo/midas/social/api/TwitterAPI;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 239
    if-eqz v1, :cond_1

    .line 240
    iget-object v3, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    # getter for: Lcom/vlingo/midas/social/api/TwitterDialog;->mListener:Lcom/vlingo/midas/social/api/TwitterDialogListener;
    invoke-static {v3}, Lcom/vlingo/midas/social/api/TwitterDialog;->access$100(Lcom/vlingo/midas/social/api/TwitterDialog;)Lcom/vlingo/midas/social/api/TwitterDialogListener;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/vlingo/midas/social/api/TwitterDialogListener;->onTwitterError(Ljava/lang/String;)V

    .line 246
    :goto_0
    iget-object v3, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    invoke-virtual {v3}, Lcom/vlingo/midas/social/api/TwitterDialog;->dismiss()V

    .line 247
    const/4 v3, 0x1

    .line 251
    :cond_0
    :goto_1
    return v3

    .line 241
    :cond_1
    if-eqz v0, :cond_2

    .line 242
    iget-object v3, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    # getter for: Lcom/vlingo/midas/social/api/TwitterDialog;->mListener:Lcom/vlingo/midas/social/api/TwitterDialogListener;
    invoke-static {v3}, Lcom/vlingo/midas/social/api/TwitterDialog;->access$100(Lcom/vlingo/midas/social/api/TwitterDialog;)Lcom/vlingo/midas/social/api/TwitterDialogListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/midas/social/api/TwitterDialogListener;->onCancel()V

    goto :goto_0

    .line 244
    :cond_2
    iget-object v3, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    # getter for: Lcom/vlingo/midas/social/api/TwitterDialog;->mListener:Lcom/vlingo/midas/social/api/TwitterDialogListener;
    invoke-static {v3}, Lcom/vlingo/midas/social/api/TwitterDialog;->access$100(Lcom/vlingo/midas/social/api/TwitterDialog;)Lcom/vlingo/midas/social/api/TwitterDialogListener;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/vlingo/midas/social/api/TwitterDialogListener;->onComplete(Landroid/os/Bundle;)V

    goto :goto_0

    .line 248
    :cond_3
    const-string/jumbo v4, "touch"

    invoke-virtual {p2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_1
.end method
