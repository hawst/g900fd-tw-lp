.class Lcom/vlingo/midas/social/SocialAccountActivity$11$1;
.super Ljava/lang/Object;
.source "SocialAccountActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/social/SocialAccountActivity$11;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/social/SocialAccountActivity$11;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/social/SocialAccountActivity$11;)V
    .locals 0

    .prologue
    .line 461
    iput-object p1, p0, Lcom/vlingo/midas/social/SocialAccountActivity$11$1;->this$1:Lcom/vlingo/midas/social/SocialAccountActivity$11;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 463
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$11$1;->this$1:Lcom/vlingo/midas/social/SocialAccountActivity$11;

    iget-object v0, v0, Lcom/vlingo/midas/social/SocialAccountActivity$11;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$11$1;->this$1:Lcom/vlingo/midas/social/SocialAccountActivity$11;

    iget-object v0, v0, Lcom/vlingo/midas/social/SocialAccountActivity$11;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    iget-boolean v0, v0, Lcom/vlingo/midas/social/SocialAccountActivity;->mDestroyed:Z

    if-eqz v0, :cond_1

    .line 464
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$11$1;->this$1:Lcom/vlingo/midas/social/SocialAccountActivity$11;

    iget-object v0, v0, Lcom/vlingo/midas/social/SocialAccountActivity$11;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # getter for: Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$100(Lcom/vlingo/midas/social/SocialAccountActivity;)Lcom/vlingo/midas/social/api/SocialAPI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/SocialAPI;->clearCredentials()V

    .line 491
    :goto_0
    return-void

    .line 467
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$11$1;->this$1:Lcom/vlingo/midas/social/SocialAccountActivity$11;

    iget-object v0, v0, Lcom/vlingo/midas/social/SocialAccountActivity$11;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # invokes: Lcom/vlingo/midas/social/SocialAccountActivity;->removeCurrentDialog()V
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$300(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    .line 468
    const-string/jumbo v0, "key_social_login_attemp_for_resume"

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 469
    const-string/jumbo v0, "key_social_login_attemp_for_user_leave_hint"

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 470
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$11$1;->this$1:Lcom/vlingo/midas/social/SocialAccountActivity$11;

    iget-object v0, v0, Lcom/vlingo/midas/social/SocialAccountActivity$11;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->finish()V

    .line 473
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$11$1;->this$1:Lcom/vlingo/midas/social/SocialAccountActivity$11;

    iget-object v0, v0, Lcom/vlingo/midas/social/SocialAccountActivity$11;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # getter for: Lcom/vlingo/midas/social/SocialAccountActivity;->updateType:Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$700(Lcom/vlingo/midas/social/SocialAccountActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$11$1;->this$1:Lcom/vlingo/midas/social/SocialAccountActivity$11;

    iget-object v0, v0, Lcom/vlingo/midas/social/SocialAccountActivity$11;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # getter for: Lcom/vlingo/midas/social/SocialAccountActivity;->updateType:Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$700(Lcom/vlingo/midas/social/SocialAccountActivity;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "all"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 474
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 475
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$11$1;->this$1:Lcom/vlingo/midas/social/SocialAccountActivity$11;

    iget-object v0, v0, Lcom/vlingo/midas/social/SocialAccountActivity$11;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-static {v2}, Lcom/vlingo/midas/util/SocialUtils;->loginIntent(Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 477
    :cond_2
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->facebookSSO()Z

    move-result v0

    if-nez v0, :cond_3

    .line 478
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/social/api/SocialNetworkType;->FACEBOOK:Lcom/vlingo/midas/social/api/SocialNetworkType;

    new-instance v3, Lcom/vlingo/midas/social/api/FacebookAPI;

    iget-object v4, p0, Lcom/vlingo/midas/social/SocialAccountActivity$11$1;->this$1:Lcom/vlingo/midas/social/SocialAccountActivity$11;

    iget-object v4, v4, Lcom/vlingo/midas/social/SocialAccountActivity$11;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-direct {v3, v4}, Lcom/vlingo/midas/social/api/FacebookAPI;-><init>(Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/vlingo/midas/VlingoApplication;->startSocialLogin(Landroid/content/Context;Lcom/vlingo/midas/social/api/SocialNetworkType;Lcom/vlingo/midas/social/api/SocialAPI;)V

    goto :goto_0

    .line 480
    :cond_3
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->isFacebookLoggedIn()Z

    move-result v0

    if-nez v0, :cond_4

    .line 481
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$11$1;->this$1:Lcom/vlingo/midas/social/SocialAccountActivity$11;

    iget-object v0, v0, Lcom/vlingo/midas/social/SocialAccountActivity$11;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    const/4 v1, 0x0

    invoke-static {v0, v2, v1}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->startFacebookSSO(Landroid/content/Context;ZZ)V

    goto :goto_0

    .line 483
    :cond_4
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$11$1;->this$1:Lcom/vlingo/midas/social/SocialAccountActivity$11;

    iget-object v0, v0, Lcom/vlingo/midas/social/SocialAccountActivity$11;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-static {v2}, Lcom/vlingo/midas/util/SocialUtils;->loginIntent(Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 489
    :cond_5
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$11$1;->this$1:Lcom/vlingo/midas/social/SocialAccountActivity$11;

    iget-object v0, v0, Lcom/vlingo/midas/social/SocialAccountActivity$11;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-static {v2}, Lcom/vlingo/midas/util/SocialUtils;->loginIntent(Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
