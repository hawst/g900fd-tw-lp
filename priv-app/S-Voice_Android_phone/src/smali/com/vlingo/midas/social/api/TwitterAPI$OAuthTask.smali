.class Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;
.super Landroid/os/AsyncTask;
.source "TwitterAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/social/api/TwitterAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OAuthTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/vlingo/midas/social/api/TwitterAPI;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private authUrl:Ljava/lang/String;

.field private error:I

.field private parent:Lcom/vlingo/midas/social/api/TwitterAPI;

.field final synthetic this$0:Lcom/vlingo/midas/social/api/TwitterAPI;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/social/api/TwitterAPI;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 283
    iput-object p1, p0, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 284
    iput-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->authUrl:Ljava/lang/String;

    .line 285
    iput-object v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->parent:Lcom/vlingo/midas/social/api/TwitterAPI;

    .line 286
    const/4 v0, 0x1

    iput v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->error:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/social/api/TwitterAPI;Lcom/vlingo/midas/social/api/TwitterAPI$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/social/api/TwitterAPI;
    .param p2, "x1"    # Lcom/vlingo/midas/social/api/TwitterAPI$1;

    .prologue
    .line 283
    invoke-direct {p0, p1}, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;-><init>(Lcom/vlingo/midas/social/api/TwitterAPI;)V

    return-void
.end method

.method private showTwitterDialog(Ljava/lang/Boolean;)V
    .locals 5
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    const/4 v4, 0x0

    .line 331
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 334
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->mTwitterDialog:Lcom/vlingo/midas/social/api/TwitterDialog;
    invoke-static {v2}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$900(Lcom/vlingo/midas/social/api/TwitterAPI;)Lcom/vlingo/midas/social/api/TwitterDialog;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->authUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/social/api/TwitterDialog;->updateWebView(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 340
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    .line 341
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    invoke-virtual {v2}, Lcom/vlingo/midas/social/api/TwitterAPI;->clearCredentials()V

    .line 342
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_twitter_err_login:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    .line 343
    .local v1, "updateErrMsg":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->callback:Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;
    invoke-static {v2}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$1000(Lcom/vlingo/midas/social/api/TwitterAPI;)Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;

    move-result-object v2

    iget v3, p0, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->error:I

    invoke-interface {v2, v3, v4, v1}, Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;->onLoginComplete(IZLjava/lang/String;)V

    .line 346
    .end local v1    # "updateErrMsg":Ljava/lang/String;
    :cond_1
    return-void

    .line 336
    :catch_0
    move-exception v0

    .line 337
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/vlingo/midas/social/api/TwitterAPI;)Ljava/lang/Boolean;
    .locals 5
    .param p1, "params"    # [Lcom/vlingo/midas/social/api/TwitterAPI;

    .prologue
    const/4 v4, 0x0

    .line 290
    aget-object v1, p1, v4

    iput-object v1, p0, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->parent:Lcom/vlingo/midas/social/api/TwitterAPI;

    .line 292
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->mProvider:Loauth/signpost/OAuthProvider;
    invoke-static {v1}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$800(Lcom/vlingo/midas/social/api/TwitterAPI;)Loauth/signpost/OAuthProvider;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->mConsumer:Loauth/signpost/OAuthConsumer;
    invoke-static {v2}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$600(Lcom/vlingo/midas/social/api/TwitterAPI;)Loauth/signpost/OAuthConsumer;

    move-result-object v2

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->CALLBACK_URI:Landroid/net/Uri;
    invoke-static {}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$700()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Loauth/signpost/OAuthProvider;->retrieveRequestToken(Loauth/signpost/OAuthConsumer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->authUrl:Ljava/lang/String;

    .line 293
    const-string/jumbo v1, "twitter_request_token"

    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->mConsumer:Loauth/signpost/OAuthConsumer;
    invoke-static {v2}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$600(Lcom/vlingo/midas/social/api/TwitterAPI;)Loauth/signpost/OAuthConsumer;

    move-result-object v2

    invoke-interface {v2}, Loauth/signpost/OAuthConsumer;->getToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    const-string/jumbo v1, "twitter_request_secret"

    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->mConsumer:Loauth/signpost/OAuthConsumer;
    invoke-static {v2}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$600(Lcom/vlingo/midas/social/api/TwitterAPI;)Loauth/signpost/OAuthConsumer;

    move-result-object v2

    invoke-interface {v2}, Loauth/signpost/OAuthConsumer;->getTokenSecret()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Loauth/signpost/exception/OAuthNotAuthorizedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Loauth/signpost/exception/OAuthCommunicationException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v1

    .line 311
    :goto_0
    return-object v1

    .line 299
    :catch_0
    move-exception v0

    .line 300
    .local v0, "e":Loauth/signpost/exception/OAuthMessageSignerException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthMessageSignerException;->printStackTrace()V

    .line 311
    .end local v0    # "e":Loauth/signpost/exception/OAuthMessageSignerException;
    :goto_1
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    .line 301
    :catch_1
    move-exception v0

    .line 302
    .local v0, "e":Loauth/signpost/exception/OAuthNotAuthorizedException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthNotAuthorizedException;->printStackTrace()V

    goto :goto_1

    .line 303
    .end local v0    # "e":Loauth/signpost/exception/OAuthNotAuthorizedException;
    :catch_2
    move-exception v0

    .line 304
    .local v0, "e":Loauth/signpost/exception/OAuthExpectationFailedException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthExpectationFailedException;->printStackTrace()V

    goto :goto_1

    .line 305
    .end local v0    # "e":Loauth/signpost/exception/OAuthExpectationFailedException;
    :catch_3
    move-exception v0

    .line 306
    .local v0, "e":Loauth/signpost/exception/OAuthCommunicationException;
    const/4 v1, 0x4

    iput v1, p0, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->error:I

    .line 307
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthCommunicationException;->printStackTrace()V

    goto :goto_1

    .line 308
    .end local v0    # "e":Loauth/signpost/exception/OAuthCommunicationException;
    :catch_4
    move-exception v0

    .line 309
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 283
    check-cast p1, [Lcom/vlingo/midas/social/api/TwitterAPI;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->doInBackground([Lcom/vlingo/midas/social/api/TwitterAPI;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 325
    if-eqz p1, :cond_0

    .line 326
    invoke-direct {p0, p1}, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->showTwitterDialog(Ljava/lang/Boolean;)V

    .line 327
    :cond_0
    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 283
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->onCancelled(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 318
    invoke-direct {p0, p1}, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->showTwitterDialog(Ljava/lang/Boolean;)V

    .line 319
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 283
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/social/api/TwitterAPI$OAuthTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
