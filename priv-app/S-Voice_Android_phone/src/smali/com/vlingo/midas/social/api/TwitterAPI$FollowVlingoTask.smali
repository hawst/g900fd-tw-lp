.class Lcom/vlingo/midas/social/api/TwitterAPI$FollowVlingoTask;
.super Landroid/os/AsyncTask;
.source "TwitterAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/social/api/TwitterAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FollowVlingoTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lorg/json/JSONObject;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/social/api/TwitterAPI;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/social/api/TwitterAPI;)V
    .locals 0

    .prologue
    .line 353
    iput-object p1, p0, Lcom/vlingo/midas/social/api/TwitterAPI$FollowVlingoTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/social/api/TwitterAPI;Lcom/vlingo/midas/social/api/TwitterAPI$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/social/api/TwitterAPI;
    .param p2, "x1"    # Lcom/vlingo/midas/social/api/TwitterAPI$1;

    .prologue
    .line 353
    invoke-direct {p0, p1}, Lcom/vlingo/midas/social/api/TwitterAPI$FollowVlingoTask;-><init>(Lcom/vlingo/midas/social/api/TwitterAPI;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 353
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/social/api/TwitterAPI$FollowVlingoTask;->doInBackground([Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 9
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 356
    const/4 v1, 0x0

    .line 358
    .local v1, "jso":Lorg/json/JSONObject;
    :try_start_0
    new-instance v4, Lorg/apache/http/client/methods/HttpPost;

    const-string/jumbo v6, "https://api.twitter.com/1.1/friendships/create/vlingo.json"

    invoke-direct {v4, v6}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 359
    .local v4, "post":Lorg/apache/http/client/methods/HttpPost;
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 360
    .local v3, "out":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/http/message/BasicNameValuePair;>;"
    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v7, "follow"

    const-string/jumbo v8, "true"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 361
    new-instance v6, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    const-string/jumbo v7, "UTF-8"

    invoke-direct {v6, v3, v7}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {v4, v6}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 362
    iget-object v6, p0, Lcom/vlingo/midas/social/api/TwitterAPI$FollowVlingoTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    invoke-virtual {v6}, Lcom/vlingo/midas/social/api/TwitterAPI;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v6

    invoke-virtual {v4, v6}, Lorg/apache/http/client/methods/HttpPost;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 363
    iget-object v6, p0, Lcom/vlingo/midas/social/api/TwitterAPI$FollowVlingoTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->mConsumer:Loauth/signpost/OAuthConsumer;
    invoke-static {v6}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$600(Lcom/vlingo/midas/social/api/TwitterAPI;)Loauth/signpost/OAuthConsumer;

    move-result-object v6

    invoke-interface {v6, v4}, Loauth/signpost/OAuthConsumer;->sign(Ljava/lang/Object;)Loauth/signpost/http/HttpRequest;

    .line 364
    iget-object v6, p0, Lcom/vlingo/midas/social/api/TwitterAPI$FollowVlingoTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->mClient:Lorg/apache/http/client/HttpClient;
    invoke-static {v6}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$000(Lcom/vlingo/midas/social/api/TwitterAPI;)Lorg/apache/http/client/HttpClient;

    move-result-object v6

    new-instance v7, Lorg/apache/http/impl/client/BasicResponseHandler;

    invoke-direct {v7}, Lorg/apache/http/impl/client/BasicResponseHandler;-><init>()V

    invoke-interface {v6, v4, v7}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 365
    .local v5, "response":Ljava/lang/String;
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Loauth/signpost/exception/OAuthCommunicationException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_7

    .end local v1    # "jso":Lorg/json/JSONObject;
    .local v2, "jso":Lorg/json/JSONObject;
    move-object v1, v2

    .line 383
    .end local v2    # "jso":Lorg/json/JSONObject;
    .end local v3    # "out":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/http/message/BasicNameValuePair;>;"
    .end local v4    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v5    # "response":Ljava/lang/String;
    .restart local v1    # "jso":Lorg/json/JSONObject;
    :goto_0
    return-object v1

    .line 366
    :catch_0
    move-exception v0

    .line 367
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 368
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v0

    .line 369
    .local v0, "e":Loauth/signpost/exception/OAuthMessageSignerException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthMessageSignerException;->printStackTrace()V

    goto :goto_0

    .line 370
    .end local v0    # "e":Loauth/signpost/exception/OAuthMessageSignerException;
    :catch_2
    move-exception v0

    .line 371
    .local v0, "e":Loauth/signpost/exception/OAuthExpectationFailedException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthExpectationFailedException;->printStackTrace()V

    goto :goto_0

    .line 372
    .end local v0    # "e":Loauth/signpost/exception/OAuthExpectationFailedException;
    :catch_3
    move-exception v0

    .line 373
    .local v0, "e":Loauth/signpost/exception/OAuthCommunicationException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthCommunicationException;->printStackTrace()V

    goto :goto_0

    .line 374
    .end local v0    # "e":Loauth/signpost/exception/OAuthCommunicationException;
    :catch_4
    move-exception v0

    .line 375
    .local v0, "e":Lorg/apache/http/client/ClientProtocolException;
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_0

    .line 376
    .end local v0    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_5
    move-exception v0

    .line 377
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 378
    .end local v0    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v0

    .line 379
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 380
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_7
    move-exception v0

    .line 381
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 353
    check-cast p1, Lorg/json/JSONObject;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/social/api/TwitterAPI$FollowVlingoTask;->onPostExecute(Lorg/json/JSONObject;)V

    return-void
.end method

.method protected onPostExecute(Lorg/json/JSONObject;)V
    .locals 4
    .param p1, "jso"    # Lorg/json/JSONObject;

    .prologue
    .line 387
    if-eqz p1, :cond_0

    .line 388
    iget-object v1, p0, Lcom/vlingo/midas/social/api/TwitterAPI$FollowVlingoTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->callback:Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;
    invoke-static {v1}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$1000(Lcom/vlingo/midas/social/api/TwitterAPI;)Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;->onFollowVlingoComplete(ILjava/lang/String;)V

    .line 393
    :goto_0
    return-void

    .line 390
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_twitter_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 391
    .local v0, "errMsg":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/midas/social/api/TwitterAPI$FollowVlingoTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->callback:Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;
    invoke-static {v1}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$1000(Lcom/vlingo/midas/social/api/TwitterAPI;)Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2, v0}, Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;->onFollowVlingoComplete(ILjava/lang/String;)V

    goto :goto_0
.end method
