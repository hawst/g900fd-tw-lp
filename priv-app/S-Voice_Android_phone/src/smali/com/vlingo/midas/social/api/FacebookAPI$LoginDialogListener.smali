.class Lcom/vlingo/midas/social/api/FacebookAPI$LoginDialogListener;
.super Ljava/lang/Object;
.source "FacebookAPI.java"

# interfaces
.implements Lcom/facebook/android/Facebook$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/social/api/FacebookAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoginDialogListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/social/api/FacebookAPI;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/social/api/FacebookAPI;)V
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lcom/vlingo/midas/social/api/FacebookAPI$LoginDialogListener;->this$0:Lcom/vlingo/midas/social/api/FacebookAPI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/social/api/FacebookAPI;Lcom/vlingo/midas/social/api/FacebookAPI$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/social/api/FacebookAPI;
    .param p2, "x1"    # Lcom/vlingo/midas/social/api/FacebookAPI$1;

    .prologue
    .line 224
    invoke-direct {p0, p1}, Lcom/vlingo/midas/social/api/FacebookAPI$LoginDialogListener;-><init>(Lcom/vlingo/midas/social/api/FacebookAPI;)V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 4

    .prologue
    .line 253
    iget-object v0, p0, Lcom/vlingo/midas/social/api/FacebookAPI$LoginDialogListener;->this$0:Lcom/vlingo/midas/social/api/FacebookAPI;

    # getter for: Lcom/vlingo/midas/social/api/FacebookAPI;->callback:Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;
    invoke-static {v0}, Lcom/vlingo/midas/social/api/FacebookAPI;->access$300(Lcom/vlingo/midas/social/api/FacebookAPI;)Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/social/api/FacebookAPI$LoginDialogListener;->this$0:Lcom/vlingo/midas/social/api/FacebookAPI;

    const/16 v2, 0x387

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;->onFacebookAPILogin(Lcom/vlingo/midas/social/api/FacebookAPI;ILandroid/os/Bundle;)V

    .line 254
    return-void
.end method

.method public onComplete(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "values"    # Landroid/os/Bundle;

    .prologue
    .line 228
    iget-object v0, p0, Lcom/vlingo/midas/social/api/FacebookAPI$LoginDialogListener;->this$0:Lcom/vlingo/midas/social/api/FacebookAPI;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/FacebookAPI;->save()V

    .line 229
    iget-object v0, p0, Lcom/vlingo/midas/social/api/FacebookAPI$LoginDialogListener;->this$0:Lcom/vlingo/midas/social/api/FacebookAPI;

    # getter for: Lcom/vlingo/midas/social/api/FacebookAPI;->callback:Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;
    invoke-static {v0}, Lcom/vlingo/midas/social/api/FacebookAPI;->access$300(Lcom/vlingo/midas/social/api/FacebookAPI;)Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/social/api/FacebookAPI$LoginDialogListener;->this$0:Lcom/vlingo/midas/social/api/FacebookAPI;

    const/16 v2, 0x385

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;->onFacebookAPILogin(Lcom/vlingo/midas/social/api/FacebookAPI;ILandroid/os/Bundle;)V

    .line 230
    return-void
.end method

.method public onError(Lcom/facebook/android/DialogError;)V
    .locals 4
    .param p1, "error"    # Lcom/facebook/android/DialogError;

    .prologue
    .line 244
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 245
    .local v0, "params":Landroid/os/Bundle;
    const-string/jumbo v1, "error"

    invoke-virtual {p1}, Lcom/facebook/android/DialogError;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const-string/jumbo v1, "on_facebook_error"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 247
    iget-object v1, p0, Lcom/vlingo/midas/social/api/FacebookAPI$LoginDialogListener;->this$0:Lcom/vlingo/midas/social/api/FacebookAPI;

    # getter for: Lcom/vlingo/midas/social/api/FacebookAPI;->callback:Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;
    invoke-static {v1}, Lcom/vlingo/midas/social/api/FacebookAPI;->access$300(Lcom/vlingo/midas/social/api/FacebookAPI;)Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/social/api/FacebookAPI$LoginDialogListener;->this$0:Lcom/vlingo/midas/social/api/FacebookAPI;

    const/16 v3, 0x386

    invoke-interface {v1, v2, v3, v0}, Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;->onFacebookAPILogin(Lcom/vlingo/midas/social/api/FacebookAPI;ILandroid/os/Bundle;)V

    .line 248
    return-void
.end method

.method public onFacebookError(Lcom/facebook/android/FacebookError;)V
    .locals 4
    .param p1, "error"    # Lcom/facebook/android/FacebookError;

    .prologue
    .line 235
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 236
    .local v0, "params":Landroid/os/Bundle;
    const-string/jumbo v1, "error"

    invoke-virtual {p1}, Lcom/facebook/android/FacebookError;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const-string/jumbo v1, "on_facebook_error"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 238
    iget-object v1, p0, Lcom/vlingo/midas/social/api/FacebookAPI$LoginDialogListener;->this$0:Lcom/vlingo/midas/social/api/FacebookAPI;

    # getter for: Lcom/vlingo/midas/social/api/FacebookAPI;->callback:Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;
    invoke-static {v1}, Lcom/vlingo/midas/social/api/FacebookAPI;->access$300(Lcom/vlingo/midas/social/api/FacebookAPI;)Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/social/api/FacebookAPI$LoginDialogListener;->this$0:Lcom/vlingo/midas/social/api/FacebookAPI;

    const/16 v3, 0x386

    invoke-interface {v1, v2, v3, v0}, Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;->onFacebookAPILogin(Lcom/vlingo/midas/social/api/FacebookAPI;ILandroid/os/Bundle;)V

    .line 239
    return-void
.end method
