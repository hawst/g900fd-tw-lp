.class public Lcom/vlingo/midas/social/api/WeiboAPI;
.super Lcom/vlingo/midas/social/api/SocialAPI;
.source "WeiboAPI.java"

# interfaces
.implements Lcom/weibo/sdk/android/net/RequestListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/social/api/WeiboAPI$WeiboCallBack;,
        Lcom/vlingo/midas/social/api/WeiboAPI$AuthDialogListener;
    }
.end annotation


# static fields
.field private static final ACCEPTED_TEXT_TAG_STRING:Ljava/lang/String; = "weibo:status"

.field private static final CONSUMER_KEY:Ljava/lang/String;

.field private static final INTENT_STRING:Ljava/lang/String; = "weibo"

.field private static final REDIRECT_URL:Ljava/lang/String;

.field public static final TYPE_INTENT:I = 0x20

.field public static final WEIBO_CANCELLED:I = 0x0

.field public static final WEIBO_LOGIN_FAILED:I = 0x3

.field public static final WEIBO_LOGIN_SUCCESS:I = 0x1

.field public static final WEIBO_SHARE_FAILED:I = 0x4

.field public static final WEIBO_SHARE_SUCCESS:I = 0x2

.field public static accessToken:Lcom/weibo/sdk/android/Oauth2AccessToken;


# instance fields
.field private dialog:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

.field mCallBack:Lcom/vlingo/midas/social/api/WeiboAPI$WeiboCallBack;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    const-string/jumbo v0, "weibo_app_id"

    const-string/jumbo v1, "3328388872"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/social/api/WeiboAPI;->CONSUMER_KEY:Ljava/lang/String;

    .line 37
    const-string/jumbo v0, "weibo_redirect_url"

    const-string/jumbo v1, "http://www.nuance.com/"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/social/api/WeiboAPI;->REDIRECT_URL:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/vlingo/midas/social/api/WeiboAPI$WeiboCallBack;)V
    .locals 19
    .param p1, "callback"    # Lcom/vlingo/midas/social/api/WeiboAPI$WeiboCallBack;

    .prologue
    .line 53
    const-string/jumbo v3, "weibo_account"

    const-string/jumbo v4, "weibo_picture_url"

    const-string/jumbo v5, "weibo"

    const/4 v6, -0x1

    const/4 v7, -0x1

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v8, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_weibo_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v8}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$string;)I

    move-result v8

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v9, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_weibo:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v9}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$string;)I

    move-result v9

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v10, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_wcis_social_weibo:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v10}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$string;)I

    move-result v10

    const/4 v11, -0x1

    const/4 v12, -0x1

    const/16 v13, 0x20

    const/4 v14, -0x1

    const v15, 0x7fffffff

    const/16 v16, 0x1

    const/16 v17, 0x1

    const-string/jumbo v18, "weibo:status"

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v18}, Lcom/vlingo/midas/social/api/SocialAPI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIIIIIIIZZLjava/lang/String;)V

    .line 46
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/vlingo/midas/social/api/WeiboAPI;->dialog:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    .line 59
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/social/api/WeiboAPI;->mCallBack:Lcom/vlingo/midas/social/api/WeiboAPI$WeiboCallBack;

    .line 60
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/social/api/WeiboAPI;->refreshCredentials()V

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/social/api/WeiboAPI;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/WeiboAPI;
    .param p1, "x1"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/vlingo/midas/social/api/WeiboAPI;->notifyException(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/social/api/WeiboAPI;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/WeiboAPI;
    .param p1, "x1"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/vlingo/midas/social/api/WeiboAPI;->notifyError(I)V

    return-void
.end method

.method private notifyError(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 228
    iget-object v0, p0, Lcom/vlingo/midas/social/api/WeiboAPI;->mCallBack:Lcom/vlingo/midas/social/api/WeiboAPI$WeiboCallBack;

    invoke-interface {v0, p1}, Lcom/vlingo/midas/social/api/WeiboAPI$WeiboCallBack;->onWeiboFail(I)V

    .line 229
    return-void
.end method

.method private notifyException(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 234
    iget-object v0, p0, Lcom/vlingo/midas/social/api/WeiboAPI;->mCallBack:Lcom/vlingo/midas/social/api/WeiboAPI$WeiboCallBack;

    invoke-interface {v0, p1}, Lcom/vlingo/midas/social/api/WeiboAPI$WeiboCallBack;->onWeiboFail(I)V

    .line 235
    return-void
.end method


# virtual methods
.method public clearCredentials()V
    .locals 5

    .prologue
    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    .line 67
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/android/Util;->clearCookies(Landroid/content/Context;)V

    .line 68
    const-string/jumbo v0, "weibo_token"

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string/jumbo v0, "weibo_expires_in"

    invoke-static {v0, v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Ljava/lang/String;J)V

    .line 70
    const-string/jumbo v0, "weibo_account"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 71
    const-string/jumbo v0, "weibo_picture"

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setImage(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 72
    const-string/jumbo v0, "weibo_picture_url"

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string/jumbo v0, "weibo_user_uid"

    invoke-static {v0, v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Ljava/lang/String;J)V

    .line 75
    return-void
.end method

.method public dismissDialogs()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/vlingo/midas/social/api/WeiboAPI;->dialog:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/vlingo/midas/social/api/WeiboAPI;->dialog:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/vlingo/midas/social/api/WeiboAPI;->dialog:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->dismiss()V

    .line 98
    :cond_0
    return-void
.end method

.method public fetchUserData()V
    .locals 5

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/WeiboAPI;->getCredentialsValues()Lcom/weibo/sdk/android/Oauth2AccessToken;

    move-result-object v2

    sput-object v2, Lcom/vlingo/midas/social/api/WeiboAPI;->accessToken:Lcom/weibo/sdk/android/Oauth2AccessToken;

    .line 153
    const-string/jumbo v2, "weibo_user_uid"

    const-wide/16 v3, 0x0

    invoke-static {v2, v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 154
    .local v1, "userUID":Ljava/lang/Long;
    new-instance v0, Lcom/weibo/sdk/android/api/UsersAPI;

    sget-object v2, Lcom/vlingo/midas/social/api/WeiboAPI;->accessToken:Lcom/weibo/sdk/android/Oauth2AccessToken;

    invoke-direct {v0, v2}, Lcom/weibo/sdk/android/api/UsersAPI;-><init>(Lcom/weibo/sdk/android/Oauth2AccessToken;)V

    .line 155
    .local v0, "userAPI":Lcom/weibo/sdk/android/api/UsersAPI;
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    new-instance v4, Lcom/vlingo/midas/social/api/WeiboAPI$1;

    invoke-direct {v4, p0}, Lcom/vlingo/midas/social/api/WeiboAPI$1;-><init>(Lcom/vlingo/midas/social/api/WeiboAPI;)V

    invoke-virtual {v0, v2, v3, v4}, Lcom/weibo/sdk/android/api/UsersAPI;->show(JLcom/weibo/sdk/android/net/RequestListener;)V

    .line 184
    return-void
.end method

.method protected getCredentialsValues()Lcom/weibo/sdk/android/Oauth2AccessToken;
    .locals 5

    .prologue
    .line 145
    const-string/jumbo v2, "weibo_token"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146
    .local v0, "accesToken":Ljava/lang/String;
    const-string/jumbo v2, "weibo_expires_in"

    const-wide/16 v3, 0x0

    invoke-static {v2, v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 147
    .local v1, "expiresIn":Ljava/lang/Long;
    new-instance v2, Lcom/weibo/sdk/android/Oauth2AccessToken;

    invoke-virtual {v1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/weibo/sdk/android/Oauth2AccessToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public getDialog()Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/vlingo/midas/social/api/WeiboAPI;->dialog:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    return-object v0
.end method

.method public getUserUID()V
    .locals 2

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/WeiboAPI;->getCredentialsValues()Lcom/weibo/sdk/android/Oauth2AccessToken;

    move-result-object v1

    sput-object v1, Lcom/vlingo/midas/social/api/WeiboAPI;->accessToken:Lcom/weibo/sdk/android/Oauth2AccessToken;

    .line 188
    new-instance v0, Lcom/weibo/sdk/android/api/AccountAPI;

    sget-object v1, Lcom/vlingo/midas/social/api/WeiboAPI;->accessToken:Lcom/weibo/sdk/android/Oauth2AccessToken;

    invoke-direct {v0, v1}, Lcom/weibo/sdk/android/api/AccountAPI;-><init>(Lcom/weibo/sdk/android/Oauth2AccessToken;)V

    .line 189
    .local v0, "accAPI":Lcom/weibo/sdk/android/api/AccountAPI;
    new-instance v1, Lcom/vlingo/midas/social/api/WeiboAPI$2;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/social/api/WeiboAPI$2;-><init>(Lcom/vlingo/midas/social/api/WeiboAPI;)V

    invoke-virtual {v0, v1}, Lcom/weibo/sdk/android/api/AccountAPI;->getUid(Lcom/weibo/sdk/android/net/RequestListener;)V

    .line 223
    return-void
.end method

.method public login(Landroid/app/Activity;)Z
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 120
    new-instance v0, Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    sget-object v1, Lcom/vlingo/midas/social/api/WeiboAPI;->CONSUMER_KEY:Ljava/lang/String;

    sget-object v2, Lcom/vlingo/midas/social/api/WeiboAPI;->REDIRECT_URL:Ljava/lang/String;

    new-instance v3, Lcom/vlingo/midas/social/api/WeiboAPI$AuthDialogListener;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/social/api/WeiboAPI$AuthDialogListener;-><init>(Lcom/vlingo/midas/social/api/WeiboAPI;)V

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/weibo/sdk/android/WeiboAuthListener;)V

    iput-object v0, p0, Lcom/vlingo/midas/social/api/WeiboAPI;->dialog:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    .line 121
    const/4 v0, 0x1

    return v0
.end method

.method public onComplete(Ljava/lang/String;)V
    .locals 2
    .param p1, "response"    # Ljava/lang/String;

    .prologue
    .line 242
    iget-object v0, p0, Lcom/vlingo/midas/social/api/WeiboAPI;->mCallBack:Lcom/vlingo/midas/social/api/WeiboAPI$WeiboCallBack;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/vlingo/midas/social/api/WeiboAPI$WeiboCallBack;->onWeiboSuccess(I)V

    .line 243
    return-void
.end method

.method public onError(Lcom/weibo/sdk/android/WeiboException;)V
    .locals 1
    .param p1, "arg0"    # Lcom/weibo/sdk/android/WeiboException;

    .prologue
    .line 247
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/vlingo/midas/social/api/WeiboAPI;->notifyError(I)V

    .line 248
    return-void
.end method

.method public onIOException(Ljava/io/IOException;)V
    .locals 1
    .param p1, "arg0"    # Ljava/io/IOException;

    .prologue
    .line 254
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/vlingo/midas/social/api/WeiboAPI;->notifyException(I)V

    .line 255
    return-void
.end method

.method public refreshCredentials()V
    .locals 2

    .prologue
    .line 86
    const-string/jumbo v0, "weibo_account"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/WeiboAPI;->getCredentialsValues()Lcom/weibo/sdk/android/Oauth2AccessToken;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/social/api/WeiboAPI;->accessToken:Lcom/weibo/sdk/android/Oauth2AccessToken;

    .line 89
    :cond_0
    return-void
.end method

.method protected saveCredentialsValues(Lcom/weibo/sdk/android/Oauth2AccessToken;)V
    .locals 3
    .param p1, "token"    # Lcom/weibo/sdk/android/Oauth2AccessToken;

    .prologue
    .line 126
    const-string/jumbo v0, "weibo_token"

    invoke-virtual {p1}, Lcom/weibo/sdk/android/Oauth2AccessToken;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string/jumbo v0, "weibo_expires_in"

    invoke-virtual {p1}, Lcom/weibo/sdk/android/Oauth2AccessToken;->getExpiresTime()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Ljava/lang/String;J)V

    .line 128
    const-string/jumbo v0, "weibo_account"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 129
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/WeiboAPI;->getUserUID()V

    .line 130
    return-void
.end method

.method protected saveUserData(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "pictureURL"    # Ljava/lang/String;

    .prologue
    .line 139
    const-string/jumbo v0, "weibo_account_name"

    invoke-static {v0, p1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const-string/jumbo v0, "weibo_picture_url"

    invoke-static {v0, p2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    return-void
.end method

.method protected saveUserUID(Ljava/lang/Long;)V
    .locals 3
    .param p1, "userUID"    # Ljava/lang/Long;

    .prologue
    .line 134
    const-string/jumbo v0, "weibo_user_uid"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Ljava/lang/String;J)V

    .line 135
    return-void
.end method

.method public showDialogs()V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/vlingo/midas/social/api/WeiboAPI;->dialog:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/vlingo/midas/social/api/WeiboAPI;->dialog:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->show()V

    .line 105
    :cond_0
    return-void
.end method

.method public updateDialogs()V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/vlingo/midas/social/api/WeiboAPI;->dialog:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/vlingo/midas/social/api/WeiboAPI;->dialog:Lcom/vlingo/midas/social/api/SinaWeiboDialog;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/SinaWeiboDialog;->updateDialog()V

    .line 112
    :cond_0
    return-void
.end method

.method public updateStatus(Ljava/lang/String;)V
    .locals 3
    .param p1, "status"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 79
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/WeiboAPI;->getCredentialsValues()Lcom/weibo/sdk/android/Oauth2AccessToken;

    move-result-object v1

    sput-object v1, Lcom/vlingo/midas/social/api/WeiboAPI;->accessToken:Lcom/weibo/sdk/android/Oauth2AccessToken;

    .line 80
    new-instance v0, Lcom/weibo/sdk/android/api/StatusesAPI;

    sget-object v1, Lcom/vlingo/midas/social/api/WeiboAPI;->accessToken:Lcom/weibo/sdk/android/Oauth2AccessToken;

    invoke-direct {v0, v1}, Lcom/weibo/sdk/android/api/StatusesAPI;-><init>(Lcom/weibo/sdk/android/Oauth2AccessToken;)V

    .line 81
    .local v0, "weiboAPI":Lcom/weibo/sdk/android/api/StatusesAPI;
    invoke-virtual {v0, p1, v2, v2, p0}, Lcom/weibo/sdk/android/api/StatusesAPI;->update(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/weibo/sdk/android/net/RequestListener;)V

    .line 82
    return-void
.end method
