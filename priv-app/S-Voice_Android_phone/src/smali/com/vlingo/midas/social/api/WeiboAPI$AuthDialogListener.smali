.class Lcom/vlingo/midas/social/api/WeiboAPI$AuthDialogListener;
.super Ljava/lang/Object;
.source "WeiboAPI.java"

# interfaces
.implements Lcom/weibo/sdk/android/WeiboAuthListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/social/api/WeiboAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AuthDialogListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/social/api/WeiboAPI;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/social/api/WeiboAPI;)V
    .locals 0

    .prologue
    .line 258
    iput-object p1, p0, Lcom/vlingo/midas/social/api/WeiboAPI$AuthDialogListener;->this$0:Lcom/vlingo/midas/social/api/WeiboAPI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 2

    .prologue
    .line 274
    iget-object v0, p0, Lcom/vlingo/midas/social/api/WeiboAPI$AuthDialogListener;->this$0:Lcom/vlingo/midas/social/api/WeiboAPI;

    const/4 v1, 0x0

    # invokes: Lcom/vlingo/midas/social/api/WeiboAPI;->notifyError(I)V
    invoke-static {v0, v1}, Lcom/vlingo/midas/social/api/WeiboAPI;->access$100(Lcom/vlingo/midas/social/api/WeiboAPI;I)V

    .line 275
    return-void
.end method

.method public onComplete(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "values"    # Landroid/os/Bundle;

    .prologue
    .line 262
    const-string/jumbo v2, "access_token"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 263
    .local v1, "token":Ljava/lang/String;
    const-string/jumbo v2, "expires_in"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 264
    .local v0, "expiresIn":Ljava/lang/String;
    new-instance v2, Lcom/weibo/sdk/android/Oauth2AccessToken;

    invoke-direct {v2, v1, v0}, Lcom/weibo/sdk/android/Oauth2AccessToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v2, Lcom/vlingo/midas/social/api/WeiboAPI;->accessToken:Lcom/weibo/sdk/android/Oauth2AccessToken;

    .line 265
    sget-object v2, Lcom/vlingo/midas/social/api/WeiboAPI;->accessToken:Lcom/weibo/sdk/android/Oauth2AccessToken;

    invoke-virtual {v2}, Lcom/weibo/sdk/android/Oauth2AccessToken;->isSessionValid()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 266
    iget-object v2, p0, Lcom/vlingo/midas/social/api/WeiboAPI$AuthDialogListener;->this$0:Lcom/vlingo/midas/social/api/WeiboAPI;

    sget-object v3, Lcom/vlingo/midas/social/api/WeiboAPI;->accessToken:Lcom/weibo/sdk/android/Oauth2AccessToken;

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/social/api/WeiboAPI;->saveCredentialsValues(Lcom/weibo/sdk/android/Oauth2AccessToken;)V

    .line 270
    :cond_0
    return-void
.end method

.method public onError(Lcom/weibo/sdk/android/WeiboDialogError;)V
    .locals 2
    .param p1, "e"    # Lcom/weibo/sdk/android/WeiboDialogError;

    .prologue
    .line 279
    iget-object v0, p0, Lcom/vlingo/midas/social/api/WeiboAPI$AuthDialogListener;->this$0:Lcom/vlingo/midas/social/api/WeiboAPI;

    const/4 v1, 0x3

    # invokes: Lcom/vlingo/midas/social/api/WeiboAPI;->notifyError(I)V
    invoke-static {v0, v1}, Lcom/vlingo/midas/social/api/WeiboAPI;->access$100(Lcom/vlingo/midas/social/api/WeiboAPI;I)V

    .line 280
    return-void
.end method

.method public onWeiboException(Lcom/weibo/sdk/android/WeiboException;)V
    .locals 2
    .param p1, "e"    # Lcom/weibo/sdk/android/WeiboException;

    .prologue
    .line 284
    iget-object v0, p0, Lcom/vlingo/midas/social/api/WeiboAPI$AuthDialogListener;->this$0:Lcom/vlingo/midas/social/api/WeiboAPI;

    const/4 v1, 0x3

    # invokes: Lcom/vlingo/midas/social/api/WeiboAPI;->notifyException(I)V
    invoke-static {v0, v1}, Lcom/vlingo/midas/social/api/WeiboAPI;->access$000(Lcom/vlingo/midas/social/api/WeiboAPI;I)V

    .line 285
    return-void
.end method
