.class Lcom/vlingo/midas/social/SocialAccountActivity$ScreenEventBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SocialAccountActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/social/SocialAccountActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScreenEventBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/social/SocialAccountActivity;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/social/SocialAccountActivity;)V
    .locals 0

    .prologue
    .line 586
    iput-object p1, p0, Lcom/vlingo/midas/social/SocialAccountActivity$ScreenEventBroadcastReceiver;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/social/SocialAccountActivity;Lcom/vlingo/midas/social/SocialAccountActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/social/SocialAccountActivity;
    .param p2, "x1"    # Lcom/vlingo/midas/social/SocialAccountActivity$1;

    .prologue
    .line 586
    invoke-direct {p0, p1}, Lcom/vlingo/midas/social/SocialAccountActivity$ScreenEventBroadcastReceiver;-><init>(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 589
    if-eqz p2, :cond_0

    .line 590
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 591
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 592
    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity$ScreenEventBroadcastReceiver;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # getter for: Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;
    invoke-static {v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$100(Lcom/vlingo/midas/social/SocialAccountActivity;)Lcom/vlingo/midas/social/api/SocialAPI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/social/api/SocialAPI;->isLoggedIn()Z

    move-result v1

    if-nez v1, :cond_0

    .line 594
    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity$ScreenEventBroadcastReceiver;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # invokes: Lcom/vlingo/midas/social/SocialAccountActivity;->cancelLoginDialog()V
    invoke-static {v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$200(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    .line 601
    .end local v0    # "action":Ljava/lang/String;
    :cond_0
    return-void
.end method
