.class final Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;
.super Ljava/lang/Object;
.source "FacebookAPI.java"

# interfaces
.implements Lcom/facebook/android/AsyncFacebookRunner$RequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/social/api/FacebookAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AsyncRequestListener"
.end annotation


# instance fields
.field private final method:Ljava/lang/String;

.field private ob:Ljava/lang/Object;

.field final synthetic this$0:Lcom/vlingo/midas/social/api/FacebookAPI;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/social/api/FacebookAPI;Ljava/lang/String;)V
    .locals 0
    .param p2, "method"    # Ljava/lang/String;

    .prologue
    .line 266
    iput-object p1, p0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->this$0:Lcom/vlingo/midas/social/api/FacebookAPI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 267
    iput-object p2, p0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->method:Ljava/lang/String;

    .line 268
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/social/api/FacebookAPI;Ljava/lang/String;Lcom/vlingo/midas/social/api/FacebookAPI$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/social/api/FacebookAPI;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Lcom/vlingo/midas/social/api/FacebookAPI$1;

    .prologue
    .line 261
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;-><init>(Lcom/vlingo/midas/social/api/FacebookAPI;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Lcom/vlingo/midas/social/api/FacebookAPI;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2, "method"    # Ljava/lang/String;
    .param p3, "ob"    # Ljava/lang/Object;

    .prologue
    .line 270
    iput-object p1, p0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->this$0:Lcom/vlingo/midas/social/api/FacebookAPI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 271
    iput-object p2, p0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->method:Ljava/lang/String;

    .line 272
    iput-object p3, p0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->ob:Ljava/lang/Object;

    .line 273
    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/social/api/FacebookAPI;Ljava/lang/String;Ljava/lang/Object;Lcom/vlingo/midas/social/api/FacebookAPI$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/social/api/FacebookAPI;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Ljava/lang/Object;
    .param p4, "x3"    # Lcom/vlingo/midas/social/api/FacebookAPI$1;

    .prologue
    .line 261
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;-><init>(Lcom/vlingo/midas/social/api/FacebookAPI;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method private onError(Ljava/lang/String;I)V
    .locals 5
    .param p1, "errMessage"    # Ljava/lang/String;
    .param p2, "errId"    # I

    .prologue
    .line 399
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 400
    .local v0, "params":Landroid/os/Bundle;
    const-string/jumbo v1, "error"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    const-string/jumbo v1, "error_id"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 402
    const-string/jumbo v1, "method"

    iget-object v2, p0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->method:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    iget-object v1, p0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->this$0:Lcom/vlingo/midas/social/api/FacebookAPI;

    # getter for: Lcom/vlingo/midas/social/api/FacebookAPI;->callback:Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;
    invoke-static {v1}, Lcom/vlingo/midas/social/api/FacebookAPI;->access$300(Lcom/vlingo/midas/social/api/FacebookAPI;)Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->this$0:Lcom/vlingo/midas/social/api/FacebookAPI;

    const/16 v3, 0x386

    iget-object v4, p0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->method:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;->onFacebookAPIMethod(Lcom/vlingo/midas/social/api/FacebookAPI;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 404
    return-void
.end method


# virtual methods
.method public onComplete(Ljava/lang/String;)V
    .locals 25
    .param p1, "response"    # Ljava/lang/String;

    .prologue
    .line 276
    new-instance v20, Landroid/os/Bundle;

    invoke-direct/range {v20 .. v20}, Landroid/os/Bundle;-><init>()V

    .line 277
    .local v20, "params":Landroid/os/Bundle;
    const-string/jumbo v21, "response"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const-string/jumbo v21, "method"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->method:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    const/4 v14, 0x0

    .line 281
    .local v14, "json":Lorg/json/JSONObject;
    const/4 v9, 0x0

    .line 283
    .local v9, "err":Ljava/lang/String;
    :try_start_0
    # invokes: Lcom/vlingo/midas/social/api/FacebookAPI;->parseJson(Ljava/lang/String;)Lorg/json/JSONObject;
    invoke-static/range {p1 .. p1}, Lcom/vlingo/midas/social/api/FacebookAPI;->access$400(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v14

    .line 284
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->method:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "me"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_2

    .line 285
    const-string/jumbo v21, "name"

    const-string/jumbo v22, "name"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    const-string/jumbo v21, "picture"

    const-string/jumbo v22, "picture"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v22

    const-string/jumbo v23, "data"

    invoke-virtual/range {v22 .. v23}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v22

    const-string/jumbo v23, "url"

    invoke-virtual/range {v22 .. v23}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->this$0:Lcom/vlingo/midas/social/api/FacebookAPI;

    move-object/from16 v21, v0

    # getter for: Lcom/vlingo/midas/social/api/FacebookAPI;->callback:Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;
    invoke-static/range {v21 .. v21}, Lcom/vlingo/midas/social/api/FacebookAPI;->access$300(Lcom/vlingo/midas/social/api/FacebookAPI;)Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->this$0:Lcom/vlingo/midas/social/api/FacebookAPI;

    move-object/from16 v22, v0

    const/16 v23, 0x385

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->method:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    move-object/from16 v4, v20

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;->onFacebookAPIMethod(Lcom/vlingo/midas/social/api/FacebookAPI;ILjava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/facebook/android/FacebookError; {:try_start_0 .. :try_end_0} :catch_1

    .line 373
    :goto_1
    if-eqz v9, :cond_1

    .line 374
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v9, v1}, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->onError(Ljava/lang/String;I)V

    .line 376
    :cond_1
    return-void

    .line 288
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->method:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "me.friends"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_3

    .line 289
    const-string/jumbo v21, "json"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/facebook/android/FacebookError; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 365
    :catch_0
    move-exception v7

    .line 366
    .local v7, "e":Lorg/json/JSONException;
    invoke-virtual {v7}, Lorg/json/JSONException;->printStackTrace()V

    .line 367
    invoke-virtual {v7}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v9

    .line 371
    goto :goto_1

    .line 291
    .end local v7    # "e":Lorg/json/JSONException;
    :cond_3
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->method:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "finduserid"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->ob:Ljava/lang/Object;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    instance-of v0, v0, Ljava/lang/String;

    move/from16 v21, v0

    if-eqz v21, :cond_7

    .line 293
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->ob:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Ljava/lang/String;

    .line 294
    .local v18, "nameToFind":Ljava/lang/String;
    const/16 v17, 0x0

    .local v17, "name":Ljava/lang/String;
    const/4 v12, 0x0

    .local v12, "id":Ljava/lang/String;
    const/16 v16, 0x0

    .line 295
    .local v16, "n":Ljava/lang/String;
    const-string/jumbo v21, "friends"

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    .line 296
    .local v10, "friends":Lorg/json/JSONObject;
    const-string/jumbo v21, "data"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 297
    .local v5, "arr":Lorg/json/JSONArray;
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v15

    .line 298
    .local v15, "len":I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    if-ge v11, v15, :cond_4

    .line 299
    invoke-virtual {v5, v11}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 300
    .local v8, "entry":Lorg/json/JSONObject;
    const-string/jumbo v21, "name"

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 301
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 302
    move-object/from16 v17, v16

    .line 303
    const-string/jumbo v21, "id"

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 307
    .end local v8    # "entry":Lorg/json/JSONObject;
    :cond_4
    if-eqz v17, :cond_5

    .line 308
    const-string/jumbo v21, "name"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    const-string/jumbo v21, "id"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    .end local v5    # "arr":Lorg/json/JSONArray;
    .end local v10    # "friends":Lorg/json/JSONObject;
    .end local v11    # "i":I
    .end local v12    # "id":Ljava/lang/String;
    .end local v15    # "len":I
    .end local v16    # "n":Ljava/lang/String;
    .end local v17    # "name":Ljava/lang/String;
    .end local v18    # "nameToFind":Ljava/lang/String;
    :cond_5
    :goto_3
    const-string/jumbo v21, "json"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/facebook/android/FacebookError; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 368
    :catch_1
    move-exception v7

    .line 369
    .local v7, "e":Lcom/facebook/android/FacebookError;
    invoke-virtual {v7}, Lcom/facebook/android/FacebookError;->printStackTrace()V

    .line 370
    invoke-virtual {v7}, Lcom/facebook/android/FacebookError;->getMessage()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_1

    .line 298
    .end local v7    # "e":Lcom/facebook/android/FacebookError;
    .restart local v5    # "arr":Lorg/json/JSONArray;
    .restart local v8    # "entry":Lorg/json/JSONObject;
    .restart local v10    # "friends":Lorg/json/JSONObject;
    .restart local v11    # "i":I
    .restart local v12    # "id":Ljava/lang/String;
    .restart local v15    # "len":I
    .restart local v16    # "n":Ljava/lang/String;
    .restart local v17    # "name":Ljava/lang/String;
    .restart local v18    # "nameToFind":Ljava/lang/String;
    :cond_6
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 312
    .end local v5    # "arr":Lorg/json/JSONArray;
    .end local v8    # "entry":Lorg/json/JSONObject;
    .end local v10    # "friends":Lorg/json/JSONObject;
    .end local v11    # "i":I
    .end local v12    # "id":Ljava/lang/String;
    .end local v15    # "len":I
    .end local v16    # "n":Ljava/lang/String;
    .end local v17    # "name":Ljava/lang/String;
    .end local v18    # "nameToFind":Ljava/lang/String;
    :cond_7
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->ob:Ljava/lang/Object;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    instance-of v0, v0, [Ljava/lang/String;

    move/from16 v21, v0

    if-eqz v21, :cond_5

    .line 313
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->ob:Ljava/lang/Object;

    move-object/from16 v21, v0

    check-cast v21, [Ljava/lang/String;

    move-object/from16 v0, v21

    check-cast v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    .line 314
    .local v19, "namesToFind":[Ljava/lang/String;
    const/16 v17, 0x0

    .restart local v17    # "name":Ljava/lang/String;
    const/4 v12, 0x0

    .restart local v12    # "id":Ljava/lang/String;
    const/16 v16, 0x0

    .line 315
    .restart local v16    # "n":Ljava/lang/String;
    const-string/jumbo v21, "data"

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 316
    .restart local v5    # "arr":Lorg/json/JSONArray;
    const/16 v18, 0x0

    .line 317
    .restart local v18    # "nameToFind":Ljava/lang/String;
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v15

    .line 318
    .restart local v15    # "len":I
    const/4 v6, 0x0

    .line 319
    .local v6, "done":Z
    const/4 v13, 0x0

    .local v13, "j":I
    :goto_4
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v13, v0, :cond_a

    if-nez v6, :cond_a

    .line 320
    aget-object v18, v19, v13

    .line 321
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_5
    if-ge v11, v15, :cond_8

    if-nez v6, :cond_8

    .line 322
    invoke-virtual {v5, v11}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 323
    .restart local v8    # "entry":Lorg/json/JSONObject;
    const-string/jumbo v21, "name"

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 324
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_9

    .line 325
    move-object/from16 v17, v16

    .line 326
    const-string/jumbo v21, "id"

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 327
    const/4 v6, 0x1

    .line 319
    .end local v8    # "entry":Lorg/json/JSONObject;
    :cond_8
    add-int/lit8 v13, v13, 0x1

    goto :goto_4

    .line 321
    .restart local v8    # "entry":Lorg/json/JSONObject;
    :cond_9
    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    .line 332
    .end local v8    # "entry":Lorg/json/JSONObject;
    .end local v11    # "i":I
    :cond_a
    if-eqz v17, :cond_5

    .line 333
    const-string/jumbo v21, "name"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    const-string/jumbo v21, "id"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 339
    .end local v5    # "arr":Lorg/json/JSONArray;
    .end local v6    # "done":Z
    .end local v12    # "id":Ljava/lang/String;
    .end local v13    # "j":I
    .end local v15    # "len":I
    .end local v16    # "n":Ljava/lang/String;
    .end local v17    # "name":Ljava/lang/String;
    .end local v18    # "nameToFind":Ljava/lang/String;
    .end local v19    # "namesToFind":[Ljava/lang/String;
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->method:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "finduserid_one_name"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_f

    .line 340
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->ob:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Ljava/lang/String;

    .line 341
    .restart local v18    # "nameToFind":Ljava/lang/String;
    const/16 v17, 0x0

    .restart local v17    # "name":Ljava/lang/String;
    const/4 v12, 0x0

    .restart local v12    # "id":Ljava/lang/String;
    const/16 v16, 0x0

    .line 342
    .restart local v16    # "n":Ljava/lang/String;
    const-string/jumbo v21, "data"

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 343
    .restart local v5    # "arr":Lorg/json/JSONArray;
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v15

    .line 344
    .restart local v15    # "len":I
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_6
    if-ge v11, v15, :cond_c

    .line 345
    invoke-virtual {v5, v11}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 346
    .restart local v8    # "entry":Lorg/json/JSONObject;
    const-string/jumbo v21, "name"

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 347
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_e

    .line 348
    move-object/from16 v17, v16

    .line 349
    const-string/jumbo v21, "id"

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 353
    .end local v8    # "entry":Lorg/json/JSONObject;
    :cond_c
    if-eqz v17, :cond_d

    .line 354
    const-string/jumbo v21, "name"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    const-string/jumbo v21, "id"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    :cond_d
    const-string/jumbo v21, "json"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 344
    .restart local v8    # "entry":Lorg/json/JSONObject;
    :cond_e
    add-int/lit8 v11, v11, 0x1

    goto :goto_6

    .line 359
    .end local v5    # "arr":Lorg/json/JSONArray;
    .end local v8    # "entry":Lorg/json/JSONObject;
    .end local v11    # "i":I
    .end local v12    # "id":Ljava/lang/String;
    .end local v15    # "len":I
    .end local v16    # "n":Ljava/lang/String;
    .end local v17    # "name":Ljava/lang/String;
    .end local v18    # "nameToFind":Ljava/lang/String;
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->method:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "wallpost"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_0

    .line 361
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->method:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "like_vlingo"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/facebook/android/FacebookError; {:try_start_3 .. :try_end_3} :catch_1

    move-result v21

    if-eqz v21, :cond_0

    goto/16 :goto_0
.end method

.method public onFacebookError(Lcom/facebook/android/FacebookError;)V
    .locals 2
    .param p1, "e"    # Lcom/facebook/android/FacebookError;

    .prologue
    .line 379
    invoke-virtual {p1}, Lcom/facebook/android/FacebookError;->printStackTrace()V

    .line 380
    invoke-virtual {p1}, Lcom/facebook/android/FacebookError;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->onError(Ljava/lang/String;I)V

    .line 381
    return-void
.end method

.method public onFileNotFoundException(Ljava/io/FileNotFoundException;)V
    .locals 2
    .param p1, "e"    # Ljava/io/FileNotFoundException;

    .prologue
    .line 384
    invoke-virtual {p1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 385
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->onError(Ljava/lang/String;I)V

    .line 386
    return-void
.end method

.method public onIOException(Ljava/io/IOException;)V
    .locals 2
    .param p1, "e"    # Ljava/io/IOException;

    .prologue
    .line 389
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    .line 390
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->onError(Ljava/lang/String;I)V

    .line 391
    return-void
.end method

.method public onMalformedURLException(Ljava/net/MalformedURLException;)V
    .locals 2
    .param p1, "e"    # Ljava/net/MalformedURLException;

    .prologue
    .line 394
    invoke-virtual {p1}, Ljava/net/MalformedURLException;->printStackTrace()V

    .line 395
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/social/api/FacebookAPI$AsyncRequestListener;->onError(Ljava/lang/String;I)V

    .line 396
    return-void
.end method
