.class Lcom/vlingo/midas/social/SocialAccountActivity$12;
.super Ljava/lang/Object;
.source "SocialAccountActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/social/SocialAccountActivity;->onFacebookAPILogin(Lcom/vlingo/midas/social/api/FacebookAPI;ILandroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

.field final synthetic val$api:Lcom/vlingo/midas/social/api/FacebookAPI;

.field final synthetic val$responseType:I


# direct methods
.method constructor <init>(Lcom/vlingo/midas/social/SocialAccountActivity;ILcom/vlingo/midas/social/api/FacebookAPI;)V
    .locals 0

    .prologue
    .line 510
    iput-object p1, p0, Lcom/vlingo/midas/social/SocialAccountActivity$12;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    iput p2, p0, Lcom/vlingo/midas/social/SocialAccountActivity$12;->val$responseType:I

    iput-object p3, p0, Lcom/vlingo/midas/social/SocialAccountActivity$12;->val$api:Lcom/vlingo/midas/social/api/FacebookAPI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 512
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$12;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$12;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    iget-boolean v0, v0, Lcom/vlingo/midas/social/SocialAccountActivity;->mDestroyed:Z

    if-eqz v0, :cond_2

    .line 513
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$12;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # getter for: Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$100(Lcom/vlingo/midas/social/SocialAccountActivity;)Lcom/vlingo/midas/social/api/SocialAPI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/SocialAPI;->clearCredentials()V

    .line 533
    :cond_1
    :goto_0
    return-void

    .line 516
    :cond_2
    iget v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$12;->val$responseType:I

    const/16 v1, 0x385

    if-ne v0, v1, :cond_4

    .line 517
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$12;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # invokes: Lcom/vlingo/midas/social/SocialAccountActivity;->removeCurrentDialog()V
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$300(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    .line 518
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$12;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_3

    .line 519
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$12;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    const/16 v1, 0xce

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->showDialog(I)V

    .line 520
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$12;->val$api:Lcom/vlingo/midas/social/api/FacebookAPI;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/FacebookAPI;->fetchUserData()V

    goto :goto_0

    .line 522
    :cond_4
    iget v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$12;->val$responseType:I

    const/16 v1, 0x386

    if-ne v0, v1, :cond_5

    .line 523
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$12;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # getter for: Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$100(Lcom/vlingo/midas/social/SocialAccountActivity;)Lcom/vlingo/midas/social/api/SocialAPI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/SocialAPI;->clearCredentials()V

    .line 524
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$12;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # invokes: Lcom/vlingo/midas/social/SocialAccountActivity;->removeCurrentDialog()V
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$300(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    .line 525
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$12;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity$12;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    sget v2, Lcom/vlingo/midas/R$string;->social_facebook_login_error:I

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/social/SocialAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/vlingo/midas/social/SocialAccountActivity;->errorMessage:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$502(Lcom/vlingo/midas/social/SocialAccountActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 527
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$12;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 528
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$12;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    const/16 v1, 0xcc

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->showDialog(I)V

    goto :goto_0

    .line 530
    :cond_5
    iget v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$12;->val$responseType:I

    const/16 v1, 0x387

    if-ne v0, v1, :cond_1

    .line 531
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$12;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # invokes: Lcom/vlingo/midas/social/SocialAccountActivity;->cancelLoginDialog()V
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$200(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    goto :goto_0
.end method
