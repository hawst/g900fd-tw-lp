.class public Lcom/vlingo/midas/social/api/TwitterDialog;
.super Landroid/app/Dialog;
.source "TwitterDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;
    }
.end annotation


# static fields
.field static final DIMENSIONS_LANDSCAPE:[F

.field static final DIMENSIONS_PORTRAIT:[F

.field static final DISPLAY_STRING:Ljava/lang/String; = "touch"

.field static final FB_ICON:Ljava/lang/String; = "icon.png"

.field static final FILL:Landroid/widget/FrameLayout$LayoutParams;

.field static final MARGIN:I = 0x4

.field static final PADDING:I = 0x2

.field static final TW_BLUE:I = -0x2f0f06


# instance fields
.field private handler:Landroid/os/Handler;

.field private mContent:Landroid/widget/LinearLayout;

.field private mDialog:Lcom/vlingo/midas/social/api/TwitterDialog;

.field private mListener:Lcom/vlingo/midas/social/api/TwitterDialogListener;

.field private mSpinner:Landroid/app/ProgressDialog;

.field private mTitle:Landroid/widget/TextView;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, -0x1

    .line 39
    new-array v0, v2, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/vlingo/midas/social/api/TwitterDialog;->DIMENSIONS_LANDSCAPE:[F

    .line 40
    new-array v0, v2, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/vlingo/midas/social/api/TwitterDialog;->DIMENSIONS_PORTRAIT:[F

    .line 41
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    sput-object v0, Lcom/vlingo/midas/social/api/TwitterDialog;->FILL:Landroid/widget/FrameLayout$LayoutParams;

    return-void

    .line 39
    :array_0
    .array-data 4
        0x43e60000    # 460.0f
        0x43820000    # 260.0f
    .end array-data

    .line 40
    :array_1
    .array-data 4
        0x43960000    # 300.0f
        0x43d20000    # 420.0f
    .end array-data
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/vlingo/midas/social/api/TwitterDialogListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/vlingo/midas/social/api/TwitterDialogListener;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 57
    iput-object p2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mListener:Lcom/vlingo/midas/social/api/TwitterDialogListener;

    .line 58
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->handler:Landroid/os/Handler;

    .line 59
    iput-object p0, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mDialog:Lcom/vlingo/midas/social/api/TwitterDialog;

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/social/api/TwitterDialog;)Lcom/vlingo/midas/social/api/TwitterDialog;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/TwitterDialog;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mDialog:Lcom/vlingo/midas/social/api/TwitterDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/social/api/TwitterDialog;)Lcom/vlingo/midas/social/api/TwitterDialogListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/TwitterDialog;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mListener:Lcom/vlingo/midas/social/api/TwitterDialogListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/social/api/TwitterDialog;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/TwitterDialog;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mSpinner:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/social/api/TwitterDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/TwitterDialog;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->isWindowShowing()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/vlingo/midas/social/api/TwitterDialog;)Landroid/webkit/WebView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/TwitterDialog;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/midas/social/api/TwitterDialog;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/TwitterDialog;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/midas/social/api/TwitterDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/TwitterDialog;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->removeSpinner()V

    return-void
.end method

.method static synthetic access$800(Lcom/vlingo/midas/social/api/TwitterDialog;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/social/api/TwitterDialog;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private isWindowShowing()Z
    .locals 1

    .prologue
    .line 309
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized removeSpinner()V
    .locals 1

    .prologue
    .line 313
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mSpinner:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 314
    invoke-direct {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->isWindowShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    .line 316
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 323
    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mSpinner:Landroid/app/ProgressDialog;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 326
    :cond_0
    monitor-exit p0

    return-void

    .line 313
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 319
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private setUpTitle()V
    .locals 7

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x0

    const/4 v4, 0x4

    .line 177
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/social/api/TwitterDialog;->requestWindowFeature(I)Z

    .line 178
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_twitter_icon:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/ResourceIdProvider;->getDrawable(Lcom/vlingo/core/internal/ResourceIdProvider$drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 180
    .local v0, "icon":Landroid/graphics/drawable/Drawable;
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_wcis_social_twitter:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    .line 181
    .local v1, "titleMsg":Ljava/lang/String;
    new-instance v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mTitle:Landroid/widget/TextView;

    .line 182
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mTitle:Landroid/widget/TextView;

    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 184
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mTitle:Landroid/widget/TextView;

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 185
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mTitle:Landroid/widget/TextView;

    const v3, -0x2f0f06

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 186
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v6, v4, v4, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 187
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 188
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v0, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 190
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mContent:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 192
    return-void
.end method

.method private setUpWebView(Ljava/lang/String;)V
    .locals 7
    .param p1, "authUrl"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 195
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;

    if-nez v2, :cond_0

    .line 196
    new-instance v2, Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;

    .line 197
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;

    sget-object v3, Lcom/vlingo/midas/social/api/TwitterDialog;->FILL:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 198
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mContent:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 200
    :cond_0
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2, v5}, Landroid/webkit/WebView;->setScrollContainer(Z)V

    .line 201
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2, v5}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 202
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2, v6}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    .line 203
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;

    new-instance v3, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;-><init>(Lcom/vlingo/midas/social/api/TwitterDialog;Lcom/vlingo/midas/social/api/TwitterDialog$1;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 204
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 205
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 206
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 207
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v1

    .line 208
    .local v1, "oldLocale":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 209
    .local v0, "locale":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 210
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 212
    :cond_1
    if-eqz p1, :cond_2

    .line 213
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 215
    :cond_2
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2, v5}, Landroid/webkit/WebView;->setFocusable(Z)V

    .line 216
    iget-object v2, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;

    const/16 v3, 0x82

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->requestFocus(I)Z

    .line 217
    return-void
.end method


# virtual methods
.method public dismissDialogs()V
    .locals 1

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->dismiss()V

    .line 154
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 63
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 64
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_util_loading:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 65
    .local v0, "loadingMsg":Ljava/lang/String;
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mSpinner:Landroid/app/ProgressDialog;

    .line 66
    iget-object v1, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->requestWindowFeature(I)Z

    .line 67
    iget-object v1, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v1, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 69
    iget-object v1, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mSpinner:Landroid/app/ProgressDialog;

    new-instance v2, Lcom/vlingo/midas/social/api/TwitterDialog$1;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/social/api/TwitterDialog$1;-><init>(Lcom/vlingo/midas/social/api/TwitterDialog;)V

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 76
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/Window;->requestFeature(I)Z

    .line 77
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 78
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 80
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mContent:Landroid/widget/LinearLayout;

    .line 81
    iget-object v1, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mContent:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 83
    invoke-direct {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->setUpTitle()V

    .line 84
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/vlingo/midas/social/api/TwitterDialog;->setUpWebView(Ljava/lang/String;)V

    .line 85
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->updateView()V

    .line 87
    new-instance v1, Lcom/vlingo/midas/social/api/TwitterDialog$2;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/social/api/TwitterDialog$2;-><init>(Lcom/vlingo/midas/social/api/TwitterDialog;)V

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/social/api/TwitterDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 92
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 221
    invoke-direct {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->removeSpinner()V

    .line 222
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 226
    :cond_0
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 227
    return-void
.end method

.method public showDialogs()V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mSpinner:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 167
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 168
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->show()V

    .line 170
    :cond_1
    return-void
.end method

.method public updateDialog()V
    .locals 12

    .prologue
    const v6, 0x3f333333    # 0.7f

    const/4 v11, 0x0

    const/4 v10, 0x1

    const/high16 v9, 0x3f000000    # 0.5f

    .line 119
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 120
    .local v1, "display":Landroid/view/Display;
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 121
    .local v3, "metrics":Landroid/util/DisplayMetrics;
    iget v4, v3, Landroid/util/DisplayMetrics;->density:F

    .line 123
    .local v4, "scale":F
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isMiniTabletDevice()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 124
    const/4 v5, 0x2

    new-array v0, v5, [F

    .line 125
    .local v0, "dimensions":[F
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v6

    aput v5, v0, v11

    .line 126
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v6

    aput v5, v0, v10

    .line 127
    const/high16 v4, 0x3f800000    # 1.0f

    .line 137
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    aget v6, v0, v11

    mul-float/2addr v6, v4

    add-float/2addr v6, v9

    float-to-int v6, v6

    aget v7, v0, v10

    mul-float/2addr v7, v4

    add-float/2addr v7, v9

    float-to-int v7, v7

    invoke-virtual {v5, v6, v7}, Landroid/view/Window;->setLayout(II)V

    .line 138
    iget-object v5, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mContent:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 139
    .local v2, "lp":Landroid/view/ViewGroup$LayoutParams;
    aget v5, v0, v11

    mul-float/2addr v5, v4

    add-float/2addr v5, v9

    float-to-int v5, v5

    iput v5, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 140
    aget v5, v0, v10

    mul-float/2addr v5, v4

    add-float/2addr v5, v9

    float-to-int v5, v5

    iput v5, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 141
    iget-object v5, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mContent:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 142
    iget-object v5, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;

    const/4 v6, 0x0

    invoke-virtual {v5, v10, v6}, Landroid/webkit/WebView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 143
    return-void

    .line 128
    .end local v0    # "dimensions":[F
    .end local v2    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    float-to-double v5, v4

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    cmpl-double v5, v5, v7

    if-nez v5, :cond_1

    iget v5, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v6, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    add-int/2addr v5, v6

    const/16 v6, 0x7f0

    if-ne v5, v6, :cond_1

    iget v5, v3, Landroid/util/DisplayMetrics;->xdpi:F

    const v6, 0x4315d32c

    cmpl-float v5, v5, v6

    if-nez v5, :cond_1

    iget v5, v3, Landroid/util/DisplayMetrics;->ydpi:F

    const v6, 0x431684be

    cmpl-float v5, v5, v6

    if-nez v5, :cond_1

    .line 131
    sget-object v0, Lcom/vlingo/midas/social/api/TwitterDialog;->DIMENSIONS_PORTRAIT:[F

    .line 132
    .restart local v0    # "dimensions":[F
    const/high16 v4, 0x3fc00000    # 1.5f

    goto :goto_0

    .line 134
    .end local v0    # "dimensions":[F
    :cond_1
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v5

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v6

    if-ge v5, v6, :cond_2

    sget-object v0, Lcom/vlingo/midas/social/api/TwitterDialog;->DIMENSIONS_PORTRAIT:[F

    .restart local v0    # "dimensions":[F
    :goto_1
    goto :goto_0

    .end local v0    # "dimensions":[F
    :cond_2
    sget-object v0, Lcom/vlingo/midas/social/api/TwitterDialog;->DIMENSIONS_LANDSCAPE:[F

    goto :goto_1
.end method

.method public updateView()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const v5, 0x3f333333    # 0.7f

    const/high16 v8, 0x3f000000    # 0.5f

    .line 95
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 96
    .local v1, "display":Landroid/view/Display;
    invoke-virtual {p0}, Lcom/vlingo/midas/social/api/TwitterDialog;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 97
    .local v2, "metrics":Landroid/util/DisplayMetrics;
    iget v3, v2, Landroid/util/DisplayMetrics;->density:F

    .line 99
    .local v3, "scale":F
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isMiniTabletDevice()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 100
    const/4 v4, 0x2

    new-array v0, v4, [F

    .line 101
    .local v0, "dimensions":[F
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v5

    aput v4, v0, v9

    .line 102
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v5

    aput v4, v0, v10

    .line 103
    const/high16 v3, 0x3f800000    # 1.0f

    .line 113
    :goto_0
    iget-object v4, p0, Lcom/vlingo/midas/social/api/TwitterDialog;->mContent:Landroid/widget/LinearLayout;

    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    aget v6, v0, v9

    mul-float/2addr v6, v3

    add-float/2addr v6, v8

    float-to-int v6, v6

    aget v7, v0, v10

    mul-float/2addr v7, v3

    add-float/2addr v7, v8

    float-to-int v7, v7

    invoke-direct {v5, v6, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v4, v5}, Lcom/vlingo/midas/social/api/TwitterDialog;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 116
    return-void

    .line 104
    .end local v0    # "dimensions":[F
    :cond_0
    float-to-double v4, v3

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, v4, v6

    if-nez v4, :cond_1

    iget v4, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v5, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    add-int/2addr v4, v5

    const/16 v5, 0x7f0

    if-ne v4, v5, :cond_1

    iget v4, v2, Landroid/util/DisplayMetrics;->xdpi:F

    const v5, 0x4315d32c

    cmpl-float v4, v4, v5

    if-nez v4, :cond_1

    iget v4, v2, Landroid/util/DisplayMetrics;->ydpi:F

    const v5, 0x431684be

    cmpl-float v4, v4, v5

    if-nez v4, :cond_1

    .line 107
    sget-object v0, Lcom/vlingo/midas/social/api/TwitterDialog;->DIMENSIONS_PORTRAIT:[F

    .line 108
    .restart local v0    # "dimensions":[F
    const/high16 v3, 0x3fc00000    # 1.5f

    goto :goto_0

    .line 110
    .end local v0    # "dimensions":[F
    :cond_1
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v5

    if-ge v4, v5, :cond_2

    sget-object v0, Lcom/vlingo/midas/social/api/TwitterDialog;->DIMENSIONS_PORTRAIT:[F

    .restart local v0    # "dimensions":[F
    :goto_1
    goto :goto_0

    .end local v0    # "dimensions":[F
    :cond_2
    sget-object v0, Lcom/vlingo/midas/social/api/TwitterDialog;->DIMENSIONS_LANDSCAPE:[F

    goto :goto_1
.end method

.method public updateWebView(Ljava/lang/String;)V
    .locals 0
    .param p1, "authUrl"    # Ljava/lang/String;

    .prologue
    .line 173
    invoke-direct {p0, p1}, Lcom/vlingo/midas/social/api/TwitterDialog;->setUpWebView(Ljava/lang/String;)V

    .line 174
    return-void
.end method
