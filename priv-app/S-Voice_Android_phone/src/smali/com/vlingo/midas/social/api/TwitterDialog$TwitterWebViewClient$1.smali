.class Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient$1;
.super Ljava/lang/Object;
.source "TwitterDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;)V
    .locals 0

    .prologue
    .line 293
    iput-object p1, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient$1;->this$1:Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 295
    iget-object v1, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient$1;->this$1:Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;

    iget-object v1, v1, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    # invokes: Lcom/vlingo/midas/social/api/TwitterDialog;->isWindowShowing()Z
    invoke-static {v1}, Lcom/vlingo/midas/social/api/TwitterDialog;->access$400(Lcom/vlingo/midas/social/api/TwitterDialog;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 296
    iget-object v1, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient$1;->this$1:Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;

    iget-object v1, v1, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    # getter for: Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;
    invoke-static {v1}, Lcom/vlingo/midas/social/api/TwitterDialog;->access$500(Lcom/vlingo/midas/social/api/TwitterDialog;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 297
    .local v0, "title":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 298
    iget-object v1, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient$1;->this$1:Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;

    iget-object v1, v1, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    # getter for: Lcom/vlingo/midas/social/api/TwitterDialog;->mTitle:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/vlingo/midas/social/api/TwitterDialog;->access$600(Lcom/vlingo/midas/social/api/TwitterDialog;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 300
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient$1;->this$1:Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;

    iget-object v1, v1, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    # getter for: Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;
    invoke-static {v1}, Lcom/vlingo/midas/social/api/TwitterDialog;->access$500(Lcom/vlingo/midas/social/api/TwitterDialog;)Landroid/webkit/WebView;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient$1;->this$1:Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;

    iget-object v3, v3, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    # getter for: Lcom/vlingo/midas/social/api/TwitterDialog;->mWebView:Landroid/webkit/WebView;
    invoke-static {v3}, Lcom/vlingo/midas/social/api/TwitterDialog;->access$500(Lcom/vlingo/midas/social/api/TwitterDialog;)Landroid/webkit/WebView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/webkit/WebView;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x4

    invoke-virtual {v1, v2, v3}, Landroid/webkit/WebView;->scrollTo(II)V

    .line 301
    iget-object v1, p0, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient$1;->this$1:Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;

    iget-object v1, v1, Lcom/vlingo/midas/social/api/TwitterDialog$TwitterWebViewClient;->this$0:Lcom/vlingo/midas/social/api/TwitterDialog;

    # invokes: Lcom/vlingo/midas/social/api/TwitterDialog;->removeSpinner()V
    invoke-static {v1}, Lcom/vlingo/midas/social/api/TwitterDialog;->access$700(Lcom/vlingo/midas/social/api/TwitterDialog;)V

    .line 303
    .end local v0    # "title":Ljava/lang/String;
    :cond_1
    return-void
.end method
