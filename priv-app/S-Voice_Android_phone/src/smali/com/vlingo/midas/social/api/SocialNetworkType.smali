.class public final enum Lcom/vlingo/midas/social/api/SocialNetworkType;
.super Ljava/lang/Enum;
.source "SocialNetworkType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/social/api/SocialNetworkType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/social/api/SocialNetworkType;

.field public static final enum ALL:Lcom/vlingo/midas/social/api/SocialNetworkType;

.field public static final enum FACEBOOK:Lcom/vlingo/midas/social/api/SocialNetworkType;

.field public static final enum QZONE:Lcom/vlingo/midas/social/api/SocialNetworkType;

.field public static final enum TWITTER:Lcom/vlingo/midas/social/api/SocialNetworkType;

.field public static final enum WEIBO:Lcom/vlingo/midas/social/api/SocialNetworkType;


# instance fields
.field private socialNetworkType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 14
    new-instance v0, Lcom/vlingo/midas/social/api/SocialNetworkType;

    const-string/jumbo v1, "ALL"

    const-string/jumbo v2, "all"

    invoke-direct {v0, v1, v3, v2}, Lcom/vlingo/midas/social/api/SocialNetworkType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/social/api/SocialNetworkType;->ALL:Lcom/vlingo/midas/social/api/SocialNetworkType;

    .line 15
    new-instance v0, Lcom/vlingo/midas/social/api/SocialNetworkType;

    const-string/jumbo v1, "TWITTER"

    const-string/jumbo v2, "twitter"

    invoke-direct {v0, v1, v4, v2}, Lcom/vlingo/midas/social/api/SocialNetworkType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/social/api/SocialNetworkType;->TWITTER:Lcom/vlingo/midas/social/api/SocialNetworkType;

    .line 16
    new-instance v0, Lcom/vlingo/midas/social/api/SocialNetworkType;

    const-string/jumbo v1, "FACEBOOK"

    const-string/jumbo v2, "facebook"

    invoke-direct {v0, v1, v5, v2}, Lcom/vlingo/midas/social/api/SocialNetworkType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/social/api/SocialNetworkType;->FACEBOOK:Lcom/vlingo/midas/social/api/SocialNetworkType;

    .line 17
    new-instance v0, Lcom/vlingo/midas/social/api/SocialNetworkType;

    const-string/jumbo v1, "WEIBO"

    const-string/jumbo v2, "weibo"

    invoke-direct {v0, v1, v6, v2}, Lcom/vlingo/midas/social/api/SocialNetworkType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/social/api/SocialNetworkType;->WEIBO:Lcom/vlingo/midas/social/api/SocialNetworkType;

    .line 18
    new-instance v0, Lcom/vlingo/midas/social/api/SocialNetworkType;

    const-string/jumbo v1, "QZONE"

    const-string/jumbo v2, "qzone"

    invoke-direct {v0, v1, v7, v2}, Lcom/vlingo/midas/social/api/SocialNetworkType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/midas/social/api/SocialNetworkType;->QZONE:Lcom/vlingo/midas/social/api/SocialNetworkType;

    .line 13
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vlingo/midas/social/api/SocialNetworkType;

    sget-object v1, Lcom/vlingo/midas/social/api/SocialNetworkType;->ALL:Lcom/vlingo/midas/social/api/SocialNetworkType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/midas/social/api/SocialNetworkType;->TWITTER:Lcom/vlingo/midas/social/api/SocialNetworkType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/midas/social/api/SocialNetworkType;->FACEBOOK:Lcom/vlingo/midas/social/api/SocialNetworkType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/midas/social/api/SocialNetworkType;->WEIBO:Lcom/vlingo/midas/social/api/SocialNetworkType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/midas/social/api/SocialNetworkType;->QZONE:Lcom/vlingo/midas/social/api/SocialNetworkType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/vlingo/midas/social/api/SocialNetworkType;->$VALUES:[Lcom/vlingo/midas/social/api/SocialNetworkType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    iput-object p3, p0, Lcom/vlingo/midas/social/api/SocialNetworkType;->socialNetworkType:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public static getSocialNetworkLogo(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p0, "type"    # Ljava/lang/String;

    .prologue
    .line 54
    const-string/jumbo v0, "twitter"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_twitter_icon:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getDrawable(Lcom/vlingo/core/internal/ResourceIdProvider$drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 63
    :goto_0
    return-object v0

    .line 57
    :cond_0
    const-string/jumbo v0, "facebook"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_facebook_icon:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getDrawable(Lcom/vlingo/core/internal/ResourceIdProvider$drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 60
    :cond_1
    const-string/jumbo v0, "weibo"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 61
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_weibo_icon:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getDrawable(Lcom/vlingo/core/internal/ResourceIdProvider$drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 63
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_all_logo:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getDrawable(Lcom/vlingo/core/internal/ResourceIdProvider$drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public static getSocialNetworkType(Ljava/lang/String;)Lcom/vlingo/midas/social/api/SocialNetworkType;
    .locals 1
    .param p0, "type"    # Ljava/lang/String;

    .prologue
    .line 40
    const-string/jumbo v0, "twitter"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    sget-object v0, Lcom/vlingo/midas/social/api/SocialNetworkType;->TWITTER:Lcom/vlingo/midas/social/api/SocialNetworkType;

    .line 49
    :goto_0
    return-object v0

    .line 42
    :cond_0
    const-string/jumbo v0, "facebook"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43
    sget-object v0, Lcom/vlingo/midas/social/api/SocialNetworkType;->FACEBOOK:Lcom/vlingo/midas/social/api/SocialNetworkType;

    goto :goto_0

    .line 44
    :cond_1
    const-string/jumbo v0, "weibo"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 45
    sget-object v0, Lcom/vlingo/midas/social/api/SocialNetworkType;->WEIBO:Lcom/vlingo/midas/social/api/SocialNetworkType;

    goto :goto_0

    .line 46
    :cond_2
    const-string/jumbo v0, "qzone"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 47
    sget-object v0, Lcom/vlingo/midas/social/api/SocialNetworkType;->QZONE:Lcom/vlingo/midas/social/api/SocialNetworkType;

    goto :goto_0

    .line 49
    :cond_3
    sget-object v0, Lcom/vlingo/midas/social/api/SocialNetworkType;->ALL:Lcom/vlingo/midas/social/api/SocialNetworkType;

    goto :goto_0
.end method

.method public static localizedSocialNetworkTypeString(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "type"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 67
    const-string/jumbo v3, "twitter"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 68
    new-instance v1, Lcom/vlingo/midas/social/api/TwitterAPI;

    invoke-direct {v1, v4}, Lcom/vlingo/midas/social/api/TwitterAPI;-><init>(Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;)V

    .line 69
    .local v1, "twApi":Lcom/vlingo/midas/social/api/TwitterAPI;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1}, Lcom/vlingo/midas/social/api/TwitterAPI;->wcisId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 80
    .end local v1    # "twApi":Lcom/vlingo/midas/social/api/TwitterAPI;
    :goto_0
    return-object v3

    .line 71
    :cond_0
    const-string/jumbo v3, "facebook"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 72
    new-instance v0, Lcom/vlingo/midas/social/api/FacebookAPI;

    invoke-direct {v0, v4}, Lcom/vlingo/midas/social/api/FacebookAPI;-><init>(Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;)V

    .line 73
    .local v0, "fbApi":Lcom/vlingo/midas/social/api/FacebookAPI;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/FacebookAPI;->wcisId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 75
    .end local v0    # "fbApi":Lcom/vlingo/midas/social/api/FacebookAPI;
    :cond_1
    const-string/jumbo v3, "weibo"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 76
    new-instance v2, Lcom/vlingo/midas/social/api/WeiboAPI;

    invoke-direct {v2, v4}, Lcom/vlingo/midas/social/api/WeiboAPI;-><init>(Lcom/vlingo/midas/social/api/WeiboAPI$WeiboCallBack;)V

    .line 77
    .local v2, "wbApi":Lcom/vlingo/midas/social/api/WeiboAPI;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2}, Lcom/vlingo/midas/social/api/WeiboAPI;->wcisId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 80
    .end local v2    # "wbApi":Lcom/vlingo/midas/social/api/WeiboAPI;
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_social_prompt_ex1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static of(Ljava/lang/String;)Lcom/vlingo/midas/social/api/SocialNetworkType;
    .locals 5
    .param p0, "type"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-static {}, Lcom/vlingo/midas/social/api/SocialNetworkType;->values()[Lcom/vlingo/midas/social/api/SocialNetworkType;

    move-result-object v0

    .local v0, "arr$":[Lcom/vlingo/midas/social/api/SocialNetworkType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 32
    .local v1, "currentType":Lcom/vlingo/midas/social/api/SocialNetworkType;
    invoke-virtual {v1}, Lcom/vlingo/midas/social/api/SocialNetworkType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 36
    .end local v1    # "currentType":Lcom/vlingo/midas/social/api/SocialNetworkType;
    :goto_1
    return-object v1

    .line 31
    .restart local v1    # "currentType":Lcom/vlingo/midas/social/api/SocialNetworkType;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 36
    .end local v1    # "currentType":Lcom/vlingo/midas/social/api/SocialNetworkType;
    :cond_1
    sget-object v1, Lcom/vlingo/midas/social/api/SocialNetworkType;->ALL:Lcom/vlingo/midas/social/api/SocialNetworkType;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/social/api/SocialNetworkType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 13
    const-class v0, Lcom/vlingo/midas/social/api/SocialNetworkType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/social/api/SocialNetworkType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/social/api/SocialNetworkType;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/vlingo/midas/social/api/SocialNetworkType;->$VALUES:[Lcom/vlingo/midas/social/api/SocialNetworkType;

    invoke-virtual {v0}, [Lcom/vlingo/midas/social/api/SocialNetworkType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/social/api/SocialNetworkType;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vlingo/midas/social/api/SocialNetworkType;->socialNetworkType:Ljava/lang/String;

    return-object v0
.end method
