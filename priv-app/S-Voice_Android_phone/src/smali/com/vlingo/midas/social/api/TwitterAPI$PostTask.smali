.class Lcom/vlingo/midas/social/api/TwitterAPI$PostTask;
.super Landroid/os/AsyncTask;
.source "TwitterAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/social/api/TwitterAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PostTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lorg/json/JSONObject;",
        ">;"
    }
.end annotation


# instance fields
.field private errorType:I

.field final synthetic this$0:Lcom/vlingo/midas/social/api/TwitterAPI;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/social/api/TwitterAPI;)V
    .locals 1

    .prologue
    .line 444
    iput-object p1, p0, Lcom/vlingo/midas/social/api/TwitterAPI$PostTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 445
    const/4 v0, 0x1

    iput v0, p0, Lcom/vlingo/midas/social/api/TwitterAPI$PostTask;->errorType:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/social/api/TwitterAPI;Lcom/vlingo/midas/social/api/TwitterAPI$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/social/api/TwitterAPI;
    .param p2, "x1"    # Lcom/vlingo/midas/social/api/TwitterAPI$1;

    .prologue
    .line 444
    invoke-direct {p0, p1}, Lcom/vlingo/midas/social/api/TwitterAPI$PostTask;-><init>(Lcom/vlingo/midas/social/api/TwitterAPI;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 444
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/social/api/TwitterAPI$PostTask;->doInBackground([Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 9
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 447
    const/4 v1, 0x0

    .line 449
    .local v1, "jso":Lorg/json/JSONObject;
    :try_start_0
    new-instance v4, Lorg/apache/http/client/methods/HttpPost;

    const-string/jumbo v6, "https://api.twitter.com/1.1/statuses/update.json"

    invoke-direct {v4, v6}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 450
    .local v4, "post":Lorg/apache/http/client/methods/HttpPost;
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 451
    .local v3, "out":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/http/message/BasicNameValuePair;>;"
    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v7, "status"

    const/4 v8, 0x0

    aget-object v8, p1, v8

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 452
    new-instance v6, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    const-string/jumbo v7, "UTF-8"

    invoke-direct {v6, v3, v7}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {v4, v6}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 453
    iget-object v6, p0, Lcom/vlingo/midas/social/api/TwitterAPI$PostTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    invoke-virtual {v6}, Lcom/vlingo/midas/social/api/TwitterAPI;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v6

    invoke-virtual {v4, v6}, Lorg/apache/http/client/methods/HttpPost;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 454
    iget-object v6, p0, Lcom/vlingo/midas/social/api/TwitterAPI$PostTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->mConsumer:Loauth/signpost/OAuthConsumer;
    invoke-static {v6}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$600(Lcom/vlingo/midas/social/api/TwitterAPI;)Loauth/signpost/OAuthConsumer;

    move-result-object v6

    invoke-interface {v6, v4}, Loauth/signpost/OAuthConsumer;->sign(Ljava/lang/Object;)Loauth/signpost/http/HttpRequest;

    .line 455
    iget-object v6, p0, Lcom/vlingo/midas/social/api/TwitterAPI$PostTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->mClient:Lorg/apache/http/client/HttpClient;
    invoke-static {v6}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$000(Lcom/vlingo/midas/social/api/TwitterAPI;)Lorg/apache/http/client/HttpClient;

    move-result-object v6

    new-instance v7, Lorg/apache/http/impl/client/BasicResponseHandler;

    invoke-direct {v7}, Lorg/apache/http/impl/client/BasicResponseHandler;-><init>()V

    invoke-interface {v6, v4, v7}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 456
    .local v5, "response":Ljava/lang/String;
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Loauth/signpost/exception/OAuthCommunicationException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_7

    .line 459
    .end local v1    # "jso":Lorg/json/JSONObject;
    .local v2, "jso":Lorg/json/JSONObject;
    const/4 v6, 0x0

    :try_start_1
    iput v6, p0, Lcom/vlingo/midas/social/api/TwitterAPI$PostTask;->errorType:I
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_f
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_1 .. :try_end_1} :catch_e
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Loauth/signpost/exception/OAuthCommunicationException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_8

    move-object v1, v2

    .line 479
    .end local v2    # "jso":Lorg/json/JSONObject;
    .end local v3    # "out":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/http/message/BasicNameValuePair;>;"
    .end local v4    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v5    # "response":Ljava/lang/String;
    .restart local v1    # "jso":Lorg/json/JSONObject;
    :goto_0
    return-object v1

    .line 460
    :catch_0
    move-exception v0

    .line 461
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    :goto_1
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 462
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v0

    .line 463
    .local v0, "e":Loauth/signpost/exception/OAuthMessageSignerException;
    :goto_2
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthMessageSignerException;->printStackTrace()V

    goto :goto_0

    .line 464
    .end local v0    # "e":Loauth/signpost/exception/OAuthMessageSignerException;
    :catch_2
    move-exception v0

    .line 465
    .local v0, "e":Loauth/signpost/exception/OAuthExpectationFailedException;
    :goto_3
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthExpectationFailedException;->printStackTrace()V

    goto :goto_0

    .line 466
    .end local v0    # "e":Loauth/signpost/exception/OAuthExpectationFailedException;
    :catch_3
    move-exception v0

    .line 467
    .local v0, "e":Loauth/signpost/exception/OAuthCommunicationException;
    :goto_4
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthCommunicationException;->printStackTrace()V

    goto :goto_0

    .line 468
    .end local v0    # "e":Loauth/signpost/exception/OAuthCommunicationException;
    :catch_4
    move-exception v0

    .line 469
    .local v0, "e":Lorg/apache/http/client/ClientProtocolException;
    :goto_5
    const/4 v6, 0x3

    iput v6, p0, Lcom/vlingo/midas/social/api/TwitterAPI$PostTask;->errorType:I

    .line 470
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_0

    .line 471
    .end local v0    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_5
    move-exception v0

    .line 472
    .local v0, "e":Ljava/io/IOException;
    :goto_6
    const/4 v6, 0x4

    iput v6, p0, Lcom/vlingo/midas/social/api/TwitterAPI$PostTask;->errorType:I

    .line 473
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 474
    .end local v0    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v0

    .line 475
    .local v0, "e":Lorg/json/JSONException;
    :goto_7
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 476
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_7
    move-exception v0

    .line 477
    .local v0, "e":Ljava/lang/IllegalStateException;
    :goto_8
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 476
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    .end local v1    # "jso":Lorg/json/JSONObject;
    .restart local v2    # "jso":Lorg/json/JSONObject;
    .restart local v3    # "out":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/apache/http/message/BasicNameValuePair;>;"
    .restart local v4    # "post":Lorg/apache/http/client/methods/HttpPost;
    .restart local v5    # "response":Ljava/lang/String;
    :catch_8
    move-exception v0

    move-object v1, v2

    .end local v2    # "jso":Lorg/json/JSONObject;
    .restart local v1    # "jso":Lorg/json/JSONObject;
    goto :goto_8

    .line 474
    .end local v1    # "jso":Lorg/json/JSONObject;
    .restart local v2    # "jso":Lorg/json/JSONObject;
    :catch_9
    move-exception v0

    move-object v1, v2

    .end local v2    # "jso":Lorg/json/JSONObject;
    .restart local v1    # "jso":Lorg/json/JSONObject;
    goto :goto_7

    .line 471
    .end local v1    # "jso":Lorg/json/JSONObject;
    .restart local v2    # "jso":Lorg/json/JSONObject;
    :catch_a
    move-exception v0

    move-object v1, v2

    .end local v2    # "jso":Lorg/json/JSONObject;
    .restart local v1    # "jso":Lorg/json/JSONObject;
    goto :goto_6

    .line 468
    .end local v1    # "jso":Lorg/json/JSONObject;
    .restart local v2    # "jso":Lorg/json/JSONObject;
    :catch_b
    move-exception v0

    move-object v1, v2

    .end local v2    # "jso":Lorg/json/JSONObject;
    .restart local v1    # "jso":Lorg/json/JSONObject;
    goto :goto_5

    .line 466
    .end local v1    # "jso":Lorg/json/JSONObject;
    .restart local v2    # "jso":Lorg/json/JSONObject;
    :catch_c
    move-exception v0

    move-object v1, v2

    .end local v2    # "jso":Lorg/json/JSONObject;
    .restart local v1    # "jso":Lorg/json/JSONObject;
    goto :goto_4

    .line 464
    .end local v1    # "jso":Lorg/json/JSONObject;
    .restart local v2    # "jso":Lorg/json/JSONObject;
    :catch_d
    move-exception v0

    move-object v1, v2

    .end local v2    # "jso":Lorg/json/JSONObject;
    .restart local v1    # "jso":Lorg/json/JSONObject;
    goto :goto_3

    .line 462
    .end local v1    # "jso":Lorg/json/JSONObject;
    .restart local v2    # "jso":Lorg/json/JSONObject;
    :catch_e
    move-exception v0

    move-object v1, v2

    .end local v2    # "jso":Lorg/json/JSONObject;
    .restart local v1    # "jso":Lorg/json/JSONObject;
    goto :goto_2

    .line 460
    .end local v1    # "jso":Lorg/json/JSONObject;
    .restart local v2    # "jso":Lorg/json/JSONObject;
    :catch_f
    move-exception v0

    move-object v1, v2

    .end local v2    # "jso":Lorg/json/JSONObject;
    .restart local v1    # "jso":Lorg/json/JSONObject;
    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 444
    check-cast p1, Lorg/json/JSONObject;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/social/api/TwitterAPI$PostTask;->onPostExecute(Lorg/json/JSONObject;)V

    return-void
.end method

.method protected onPostExecute(Lorg/json/JSONObject;)V
    .locals 4
    .param p1, "jso"    # Lorg/json/JSONObject;

    .prologue
    .line 483
    if-eqz p1, :cond_0

    .line 484
    iget-object v1, p0, Lcom/vlingo/midas/social/api/TwitterAPI$PostTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->callback:Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;
    invoke-static {v1}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$1000(Lcom/vlingo/midas/social/api/TwitterAPI;)Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;->onUpdateComplete(ILjava/lang/String;)V

    .line 495
    :goto_0
    return-void

    .line 487
    :cond_0
    iget v1, p0, Lcom/vlingo/midas/social/api/TwitterAPI$PostTask;->errorType:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 488
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_network_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 493
    .local v0, "updateErrMsg":Ljava/lang/String;
    :goto_1
    iget-object v1, p0, Lcom/vlingo/midas/social/api/TwitterAPI$PostTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->callback:Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;
    invoke-static {v1}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$1000(Lcom/vlingo/midas/social/api/TwitterAPI;)Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;

    move-result-object v1

    iget v2, p0, Lcom/vlingo/midas/social/api/TwitterAPI$PostTask;->errorType:I

    invoke-interface {v1, v2, v0}, Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;->onUpdateComplete(ILjava/lang/String;)V

    goto :goto_0

    .line 491
    .end local v0    # "updateErrMsg":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_update_failed:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "updateErrMsg":Ljava/lang/String;
    goto :goto_1
.end method
