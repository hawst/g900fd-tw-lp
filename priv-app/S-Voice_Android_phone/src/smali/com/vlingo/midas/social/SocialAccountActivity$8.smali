.class Lcom/vlingo/midas/social/SocialAccountActivity$8;
.super Ljava/lang/Object;
.source "SocialAccountActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/social/SocialAccountActivity;->onLoginResult(ZLjava/util/Hashtable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

.field final synthetic val$params:Ljava/util/Hashtable;

.field final synthetic val$success:Z


# direct methods
.method constructor <init>(Lcom/vlingo/midas/social/SocialAccountActivity;ZLjava/util/Hashtable;)V
    .locals 0

    .prologue
    .line 356
    iput-object p1, p0, Lcom/vlingo/midas/social/SocialAccountActivity$8;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    iput-boolean p2, p0, Lcom/vlingo/midas/social/SocialAccountActivity$8;->val$success:Z

    iput-object p3, p0, Lcom/vlingo/midas/social/SocialAccountActivity$8;->val$params:Ljava/util/Hashtable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 358
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$8;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$8;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    iget-boolean v0, v0, Lcom/vlingo/midas/social/SocialAccountActivity;->mDestroyed:Z

    if-eqz v0, :cond_2

    .line 359
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$8;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # getter for: Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$100(Lcom/vlingo/midas/social/SocialAccountActivity;)Lcom/vlingo/midas/social/api/SocialAPI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/SocialAPI;->clearCredentials()V

    .line 377
    :cond_1
    :goto_0
    return-void

    .line 362
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$8;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # invokes: Lcom/vlingo/midas/social/SocialAccountActivity;->removeCurrentDialog()V
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$300(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    .line 364
    iget-boolean v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$8;->val$success:Z

    if-eqz v0, :cond_3

    .line 365
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$8;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # invokes: Lcom/vlingo/midas/social/SocialAccountActivity;->fetchThumbnail()V
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$400(Lcom/vlingo/midas/social/SocialAccountActivity;)V

    goto :goto_0

    .line 368
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/social/SocialAccountActivity$8;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$8;->val$params:Ljava/util/Hashtable;

    const-string/jumbo v2, "error"

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    # setter for: Lcom/vlingo/midas/social/SocialAccountActivity;->errorMessage:Ljava/lang/String;
    invoke-static {v1, v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$502(Lcom/vlingo/midas/social/SocialAccountActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 369
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$8;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # getter for: Lcom/vlingo/midas/social/SocialAccountActivity;->errorMessage:Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$500(Lcom/vlingo/midas/social/SocialAccountActivity;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "cancelled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$8;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # getter for: Lcom/vlingo/midas/social/SocialAccountActivity;->errorMessage:Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$500(Lcom/vlingo/midas/social/SocialAccountActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 370
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$8;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->onBackPressed()V

    .line 371
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$8;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    # getter for: Lcom/vlingo/midas/social/SocialAccountActivity;->socialAPI:Lcom/vlingo/midas/social/api/SocialAPI;
    invoke-static {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->access$100(Lcom/vlingo/midas/social/SocialAccountActivity;)Lcom/vlingo/midas/social/api/SocialAPI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/social/api/SocialAPI;->clearCredentials()V

    goto :goto_0

    .line 374
    :cond_4
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$8;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/social/SocialAccountActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 375
    iget-object v0, p0, Lcom/vlingo/midas/social/SocialAccountActivity$8;->this$0:Lcom/vlingo/midas/social/SocialAccountActivity;

    const/16 v1, 0xcc

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/social/SocialAccountActivity;->showDialog(I)V

    goto :goto_0
.end method
