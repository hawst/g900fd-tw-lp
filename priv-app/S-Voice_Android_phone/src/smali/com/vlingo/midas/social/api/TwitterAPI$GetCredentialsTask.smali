.class Lcom/vlingo/midas/social/api/TwitterAPI$GetCredentialsTask;
.super Landroid/os/AsyncTask;
.source "TwitterAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/social/api/TwitterAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetCredentialsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lorg/json/JSONObject;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/social/api/TwitterAPI;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/social/api/TwitterAPI;)V
    .locals 0

    .prologue
    .line 504
    iput-object p1, p0, Lcom/vlingo/midas/social/api/TwitterAPI$GetCredentialsTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/social/api/TwitterAPI;Lcom/vlingo/midas/social/api/TwitterAPI$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/social/api/TwitterAPI;
    .param p2, "x1"    # Lcom/vlingo/midas/social/api/TwitterAPI$1;

    .prologue
    .line 504
    invoke-direct {p0, p1}, Lcom/vlingo/midas/social/api/TwitterAPI$GetCredentialsTask;-><init>(Lcom/vlingo/midas/social/api/TwitterAPI;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 504
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/social/api/TwitterAPI$GetCredentialsTask;->doInBackground([Ljava/lang/Void;)Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Lorg/json/JSONObject;
    .locals 7
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 507
    const/4 v2, 0x0

    .line 508
    .local v2, "jso":Lorg/json/JSONObject;
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    const-string/jumbo v5, "https://api.twitter.com/1.1/account/verify_credentials.json"

    invoke-direct {v1, v5}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 510
    .local v1, "get":Lorg/apache/http/client/methods/HttpGet;
    :try_start_0
    iget-object v5, p0, Lcom/vlingo/midas/social/api/TwitterAPI$GetCredentialsTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->mConsumer:Loauth/signpost/OAuthConsumer;
    invoke-static {v5}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$600(Lcom/vlingo/midas/social/api/TwitterAPI;)Loauth/signpost/OAuthConsumer;

    move-result-object v5

    invoke-interface {v5, v1}, Loauth/signpost/OAuthConsumer;->sign(Ljava/lang/Object;)Loauth/signpost/http/HttpRequest;

    .line 511
    iget-object v5, p0, Lcom/vlingo/midas/social/api/TwitterAPI$GetCredentialsTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->mClient:Lorg/apache/http/client/HttpClient;
    invoke-static {v5}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$000(Lcom/vlingo/midas/social/api/TwitterAPI;)Lorg/apache/http/client/HttpClient;

    move-result-object v5

    new-instance v6, Lorg/apache/http/impl/client/BasicResponseHandler;

    invoke-direct {v6}, Lorg/apache/http/impl/client/BasicResponseHandler;-><init>()V

    invoke-interface {v5, v1, v6}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 512
    .local v4, "response":Ljava/lang/String;
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Loauth/signpost/exception/OAuthMessageSignerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Loauth/signpost/exception/OAuthExpectationFailedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Loauth/signpost/exception/OAuthCommunicationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_6

    .end local v2    # "jso":Lorg/json/JSONObject;
    .local v3, "jso":Lorg/json/JSONObject;
    move-object v2, v3

    .line 531
    .end local v3    # "jso":Lorg/json/JSONObject;
    .end local v4    # "response":Ljava/lang/String;
    .restart local v2    # "jso":Lorg/json/JSONObject;
    :goto_0
    return-object v2

    .line 516
    :catch_0
    move-exception v0

    .line 517
    .local v0, "e":Loauth/signpost/exception/OAuthMessageSignerException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthMessageSignerException;->printStackTrace()V

    goto :goto_0

    .line 518
    .end local v0    # "e":Loauth/signpost/exception/OAuthMessageSignerException;
    :catch_1
    move-exception v0

    .line 519
    .local v0, "e":Loauth/signpost/exception/OAuthExpectationFailedException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthExpectationFailedException;->printStackTrace()V

    goto :goto_0

    .line 520
    .end local v0    # "e":Loauth/signpost/exception/OAuthExpectationFailedException;
    :catch_2
    move-exception v0

    .line 521
    .local v0, "e":Loauth/signpost/exception/OAuthCommunicationException;
    invoke-virtual {v0}, Loauth/signpost/exception/OAuthCommunicationException;->printStackTrace()V

    goto :goto_0

    .line 522
    .end local v0    # "e":Loauth/signpost/exception/OAuthCommunicationException;
    :catch_3
    move-exception v0

    .line 523
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 524
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_4
    move-exception v0

    .line 525
    .local v0, "e":Lorg/apache/http/client/ClientProtocolException;
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_0

    .line 526
    .end local v0    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_5
    move-exception v0

    .line 527
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 528
    .end local v0    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v0

    .line 529
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 504
    check-cast p1, Lorg/json/JSONObject;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/social/api/TwitterAPI$GetCredentialsTask;->onPostExecute(Lorg/json/JSONObject;)V

    return-void
.end method

.method protected onPostExecute(Lorg/json/JSONObject;)V
    .locals 8
    .param p1, "jso"    # Lorg/json/JSONObject;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 535
    if-eqz p1, :cond_0

    .line 536
    const-string/jumbo v4, "name"

    invoke-virtual {p1, v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 537
    .local v0, "name":Ljava/lang/String;
    const-string/jumbo v4, "screen_name"

    invoke-virtual {p1, v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 538
    .local v2, "screenName":Ljava/lang/String;
    const-string/jumbo v4, "profile_image_url"

    invoke-virtual {p1, v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 540
    .local v1, "pictureURL":Ljava/lang/String;
    const-string/jumbo v4, "twitter_username"

    invoke-static {v4, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    const-string/jumbo v4, "twitter_account_name"

    invoke-static {v4, v0}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    const-string/jumbo v4, "twitter_picture_url"

    invoke-static {v4, v1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    const-string/jumbo v4, "twitter_account"

    invoke-static {v4, v7}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 545
    iget-object v4, p0, Lcom/vlingo/midas/social/api/TwitterAPI$GetCredentialsTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->callback:Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;
    invoke-static {v4}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$1000(Lcom/vlingo/midas/social/api/TwitterAPI;)Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5, v6}, Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;->onVerifyComplete(ILjava/lang/String;)V

    .line 551
    .end local v0    # "name":Ljava/lang/String;
    .end local v1    # "pictureURL":Ljava/lang/String;
    .end local v2    # "screenName":Ljava/lang/String;
    :goto_0
    return-void

    .line 548
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_twitter_update_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v3

    .line 549
    .local v3, "updateErrMsg":Ljava/lang/String;
    iget-object v4, p0, Lcom/vlingo/midas/social/api/TwitterAPI$GetCredentialsTask;->this$0:Lcom/vlingo/midas/social/api/TwitterAPI;

    # getter for: Lcom/vlingo/midas/social/api/TwitterAPI;->callback:Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;
    invoke-static {v4}, Lcom/vlingo/midas/social/api/TwitterAPI;->access$1000(Lcom/vlingo/midas/social/api/TwitterAPI;)Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;

    move-result-object v4

    invoke-interface {v4, v7, v3}, Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;->onVerifyComplete(ILjava/lang/String;)V

    goto :goto_0
.end method
