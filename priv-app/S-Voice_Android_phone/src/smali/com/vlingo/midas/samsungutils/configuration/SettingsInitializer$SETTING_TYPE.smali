.class final enum Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;
.super Ljava/lang/Enum;
.source "SettingsInitializer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "SETTING_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

.field public static final enum BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

.field public static final enum FLOAT:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

.field public static final enum INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

.field public static final enum LONG:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

.field public static final enum STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32
    new-instance v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    const-string/jumbo v1, "BOOLEAN"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    new-instance v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    const-string/jumbo v1, "INTEGER"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    new-instance v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    const-string/jumbo v1, "LONG"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->LONG:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    new-instance v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    const-string/jumbo v1, "FLOAT"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->FLOAT:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    new-instance v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    const-string/jumbo v1, "STRING"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    sget-object v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->LONG:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->FLOAT:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->$VALUES:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 32
    const-class v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->$VALUES:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-virtual {v0}, [Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    return-object v0
.end method
