.class public Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil$MemoIndices;
.super Ljava/lang/Object;
.source "SMemoUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "MemoIndices"
.end annotation


# instance fields
.field final KEY_CONTENT_COL:I

.field public final KEY_DATE_COL:I

.field final KEY_ID_COL:I

.field public final KEY_TEXT_COL:I

.field final KEY_TITLE_COL:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cur"    # Landroid/database/Cursor;

    .prologue
    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219
    const-string/jumbo v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil$MemoIndices;->KEY_ID_COL:I

    .line 220
    const-string/jumbo v0, "Title"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil$MemoIndices;->KEY_TITLE_COL:I

    .line 221
    const-string/jumbo v0, "Content"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil$MemoIndices;->KEY_CONTENT_COL:I

    .line 222
    const-string/jumbo v0, "Text"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil$MemoIndices;->KEY_TEXT_COL:I

    .line 223
    const-string/jumbo v0, "Date"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil$MemoIndices;->KEY_DATE_COL:I

    .line 224
    return-void
.end method
