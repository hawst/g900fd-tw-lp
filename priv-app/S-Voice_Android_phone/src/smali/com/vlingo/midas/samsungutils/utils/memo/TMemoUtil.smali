.class public Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil;
.super Lcom/vlingo/core/internal/memo/MemoUtil;
.source "TMemoUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/memo/IMemoUtil;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil$MemoIndices;
    }
.end annotation


# static fields
.field protected static final KEY_CONTENT:Ljava/lang/String; = "Content"

.field protected static final KEY_CREATE:Ljava/lang/String; = "create_t"

.field protected static final KEY_ID:Ljava/lang/String; = "_id"

.field private static final PROJECTION:[Ljava/lang/String;

.field private static final SORT_ORDER:Ljava/lang/String; = "create_t DESC"

.field private static instance:Lcom/vlingo/core/internal/memo/IMemoUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 51
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "Content"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "create_t"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil;->PROJECTION:[Ljava/lang/String;

    .line 57
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil;->instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/vlingo/core/internal/memo/MemoUtil;-><init>()V

    .line 63
    return-void
.end method

.method public static getInstance()Lcom/vlingo/core/internal/memo/IMemoUtil;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil;->instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

    if-nez v0, :cond_0

    .line 90
    new-instance v0, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil;

    invoke-direct {v0}, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil;-><init>()V

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil;->instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

    .line 92
    :cond_0
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil;->instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

    return-object v0
.end method

.method private getMemo(Landroid/database/Cursor;Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil$MemoIndices;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "indices"    # Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil$MemoIndices;

    .prologue
    .line 70
    new-instance v0, Lcom/vlingo/core/internal/memo/Memo;

    invoke-direct {v0}, Lcom/vlingo/core/internal/memo/Memo;-><init>()V

    .line 71
    .local v0, "memo":Lcom/vlingo/core/internal/memo/Memo;
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil$MemoIndices;->KEY_CONTENT_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setContent(Ljava/lang/String;)V

    .line 72
    invoke-virtual {v0}, Lcom/vlingo/core/internal/memo/Memo;->getContent()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setText(Ljava/lang/String;)V

    .line 73
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil$MemoIndices;->KEY_CONTENT_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setTitle(Ljava/lang/String;)V

    .line 74
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil$MemoIndices;->KEY_ID_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setId(I)V

    .line 75
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil$MemoIndices;->KEY_CREATE_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setDate(Ljava/lang/String;)V

    .line 76
    return-object v0
.end method

.method public static isInstalled()Z
    .locals 2

    .prologue
    .line 85
    const-string/jumbo v0, "com.sec.android.app.memo"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public deleteMemo(Landroid/content/Context;J)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    .line 124
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 125
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->getUpdateContentProviderUri()Landroid/net/Uri;

    move-result-object v1

    .line 129
    .local v1, "contentUri":Landroid/net/Uri;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 131
    .local v2, "rowsDeleted":I
    const/4 v4, 0x1

    if-eq v2, v4, :cond_0

    .line 132
    new-instance v4, Lcom/vlingo/core/internal/memo/MemoUtilException;

    const-string/jumbo v5, "Error in deleting a memo."

    invoke-direct {v4, v5}, Lcom/vlingo/core/internal/memo/MemoUtilException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Lcom/vlingo/core/internal/memo/MemoUtilException; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    .end local v1    # "contentUri":Landroid/net/Uri;
    .end local v2    # "rowsDeleted":I
    :catch_0
    move-exception v3

    .line 138
    .local v3, "sux":Lcom/vlingo/core/internal/memo/MemoUtilException;
    const-string/jumbo v4, "MemoUtilException:"

    invoke-virtual {v3}, Lcom/vlingo/core/internal/memo/MemoUtilException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-virtual {v3}, Lcom/vlingo/core/internal/memo/MemoUtilException;->printStackTrace()V

    .line 140
    throw v3

    .line 135
    .end local v3    # "sux":Lcom/vlingo/core/internal/memo/MemoUtilException;
    .restart local v0    # "contentResolver":Landroid/content/ContentResolver;
    .restart local v1    # "contentUri":Landroid/net/Uri;
    .restart local v2    # "rowsDeleted":I
    :cond_0
    return-void
.end method

.method public getCreateMemoAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    const-string/jumbo v0, "android.intent.action.VOICETALK_NEW_SMEMO"

    return-object v0
.end method

.method protected getMemo(Landroid/database/Cursor;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 80
    new-instance v0, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil$MemoIndices;

    invoke-direct {v0, p1}, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil$MemoIndices;-><init>(Landroid/database/Cursor;)V

    .line 81
    .local v0, "indices":Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil$MemoIndices;
    invoke-direct {p0, p1, v0}, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil;->getMemo(Landroid/database/Cursor;Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil$MemoIndices;)Lcom/vlingo/core/internal/memo/Memo;

    move-result-object v1

    return-object v1
.end method

.method public getMostRecentlyCreatedMemo(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    .line 155
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "Content"

    aput-object v2, v0, v1

    .line 156
    .local v0, "fields":[Ljava/lang/String;
    const-string/jumbo v1, "create_t DESC"

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil;->getMostRecentlyCreatedMemo(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/core/internal/memo/Memo;

    move-result-object v1

    return-object v1
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil;->PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method public getViewMemoAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    const-string/jumbo v0, "android.intent.action.VOICETALK_VIEW_SMEMO"

    return-object v0
.end method

.method public searchMemos(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/memo/Memo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "Content"

    aput-object v2, v0, v1

    .line 149
    .local v0, "fields":[Ljava/lang/String;
    const-string/jumbo v1, "create_t DESC"

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil;->searchMemos(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public updateMemo(Landroid/content/Context;Lcom/vlingo/core/internal/memo/Memo;Lcom/vlingo/core/internal/memo/Memo;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "originalMemo"    # Lcom/vlingo/core/internal/memo/Memo;
    .param p3, "changedMemo"    # Lcom/vlingo/core/internal/memo/Memo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    .line 108
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 109
    .local v2, "values":Landroid/content/ContentValues;
    const-string/jumbo v3, "Content"

    invoke-virtual {p3}, Lcom/vlingo/core/internal/memo/Memo;->getContent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->getUpdateContentProviderUri()Landroid/net/Uri;

    move-result-object v0

    .line 115
    .local v0, "contentUri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p3}, Lcom/vlingo/core/internal/memo/Memo;->getId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 116
    .local v1, "rowsUpdated":I
    const/4 v3, 0x1

    if-eq v1, v3, :cond_0

    .line 117
    new-instance v3, Lcom/vlingo/core/internal/memo/MemoUtilException;

    const-string/jumbo v4, "Error in updating memo."

    invoke-direct {v3, v4}, Lcom/vlingo/core/internal/memo/MemoUtilException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 119
    :cond_0
    return-void
.end method
