.class public Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;
.super Ljava/lang/Object;
.source "IntegratedAppInfo.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo$1;
    }
.end annotation


# static fields
.field private static final AUTHORITY_KMEMO:Ljava/lang/String; = "com.samsung.android.memo"

.field private static AUTHORITY_MEMO:Ljava/lang/String; = null

.field private static AUTHORITY_SMEMO:Ljava/lang/String; = null

.field private static final AUTHORITY_SMEMO2:Ljava/lang/String; = "com.infraware.provider.SNoteProvider"

.field private static final AUTHORITY_SNOTE:Ljava/lang/String; = "com.infraware.provider.SNoteProvider"

.field private static final AUTHORITY_SNOTE2:Ljava/lang/String; = "com.samsung.android.snoteprovider"

.field private static final CONTENT_URI_KMEMO:Landroid/net/Uri;

.field private static final CONTENT_URI_MEMO:Landroid/net/Uri;

.field private static final CONTENT_URI_SMEMO:Landroid/net/Uri;

.field private static final CONTENT_URI_SMEMO2:Landroid/net/Uri;

.field private static final CONTENT_URI_SNOTE:Landroid/net/Uri;

.field private static final CONTENT_URI_SNOTE2:Landroid/net/Uri;

.field private static final EXEC_NAME_MEMO:Ljava/lang/String; = "com.sec.android.app.memo.Memo"

.field private static final EXEC_NAME_SMEMO:Ljava/lang/String; = "com.sec.android.widgetapp.q1_penmemo.MemoListActivity"

.field private static final EXEC_PACKAGE_MEMO:Ljava/lang/String; = "com.sec.android.app.memo"

.field private static final EXEC_PACKAGE_SMEMO:Ljava/lang/String; = "com.sec.android.widgetapp.diotek.smemo"

.field private static final EXEC_PACKAGE_SMEMO2:Ljava/lang/String; = "com.sec.android.app.snotebook"

.field private static final EXEC_PACKAGE_SNOTE:Ljava/lang/String; = "com.sec.android.app.snotebook"

.field private static final EXEC_PACKAGE_SNOTE2:Ljava/lang/String; = "com.samsung.android.snote"

.field private static final INTENT_NAME_CREATE_MEMO:Ljava/lang/String; = "com.sec.android.app.memo.MAKE_NEW_MEMO"

.field private static final INTENT_NAME_CREATE_SMEMO:Ljava/lang/String; = "com.sec.android.widgetapp.diotek.smemo.MAKE_NEW_MEMO"

.field private static final INTENT_NAME_CREATE_SMEMO2:Ljava/lang/String; = "com.sec.android.memo.CREATE_TMEMO"

.field private static final INTENT_NAME_CREATE_SNOTE:Ljava/lang/String; = "com.sec.android.app.snotebook.MAKE_NEW_NOTE"

.field private static final INTENT_NAME_CREATE_SNOTE2:Ljava/lang/String; = "com.sec.android.memo.CREATE_TMEMO"

.field private static final INTENT_NAME_DELETE_SMEMO2:Ljava/lang/String; = "com.sec.android.memo.DELETE_ID"

.field private static final INTENT_NAME_DELETE_SNOTE2:Ljava/lang/String; = "com.sec.android.memo.DELETE_ID"

.field private static final INTENT_NAME_KMEMO:Ljava/lang/String; = "com.samsung.android.intent.action.MEMO_SERVICE"

.field private static final INTENT_NAME_START_FM_RADIO:Ljava/lang/String; = "com.sec.android.app.fm.widget.on"

.field private static final TABLE_KMEMO:Ljava/lang/String; = "memo"

.field private static final TABLE_MEMO:Ljava/lang/String; = "memo/id"

.field private static final TABLE_SMEMO:Ljava/lang/String; = "PenMemo"

.field private static final TABLE_SMEMO2:Ljava/lang/String; = "fileMgr"

.field private static final TABLE_SNOTE:Ljava/lang/String; = "fileMgr"

.field private static final TABLE_SNOTE2:Ljava/lang/String; = "memoControl"

.field private static final UPDATE_CONTENT_URI_KMEMO:Landroid/net/Uri;

.field private static final UPDATE_CONTENT_URI_MEMO:Landroid/net/Uri;

.field private static final UPDATE_CONTENT_URI_SMEMO:Landroid/net/Uri;

.field private static final UPDATE_CONTENT_URI_SMEMO2:Landroid/net/Uri;

.field private static final UPDATE_CONTENT_URI_SNOTE:Landroid/net/Uri;

.field private static final UPDATE_CONTENT_URI_SNOTE2:Landroid/net/Uri;


# instance fields
.field private appType:Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

.field private contentProviderUri:Landroid/net/Uri;

.field private execName:Ljava/lang/String;

.field private execPackage:Ljava/lang/String;

.field private intentNameCreate:Ljava/lang/String;

.field private intentNameDelete:Ljava/lang/String;

.field private intentNameStart:Ljava/lang/String;

.field private isBroadcast:Z

.field private updateContentProviderUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    const-string/jumbo v0, "com.sec.android.widgetapp.q1_penmemo"

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->AUTHORITY_SMEMO:Ljava/lang/String;

    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->AUTHORITY_SMEMO:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "PenMemo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->CONTENT_URI_SMEMO:Landroid/net/Uri;

    .line 35
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->CONTENT_URI_SMEMO:Landroid/net/Uri;

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->UPDATE_CONTENT_URI_SMEMO:Landroid/net/Uri;

    .line 47
    const-string/jumbo v0, "content://com.infraware.provider.SNoteProvider/fileMgr"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->CONTENT_URI_SNOTE:Landroid/net/Uri;

    .line 48
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->CONTENT_URI_SNOTE:Landroid/net/Uri;

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->UPDATE_CONTENT_URI_SNOTE:Landroid/net/Uri;

    .line 56
    const-string/jumbo v0, "content://com.infraware.provider.SNoteProvider/fileMgr"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->CONTENT_URI_SMEMO2:Landroid/net/Uri;

    .line 57
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->CONTENT_URI_SMEMO2:Landroid/net/Uri;

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->UPDATE_CONTENT_URI_SMEMO2:Landroid/net/Uri;

    .line 66
    const-string/jumbo v0, "content://com.samsung.android.snoteprovider/memoControl"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->CONTENT_URI_SNOTE2:Landroid/net/Uri;

    .line 67
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->CONTENT_URI_SNOTE2:Landroid/net/Uri;

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->UPDATE_CONTENT_URI_SNOTE2:Landroid/net/Uri;

    .line 74
    const-string/jumbo v0, "content://com.samsung.android.memo/memo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->CONTENT_URI_KMEMO:Landroid/net/Uri;

    .line 75
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->CONTENT_URI_KMEMO:Landroid/net/Uri;

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->UPDATE_CONTENT_URI_KMEMO:Landroid/net/Uri;

    .line 79
    const-string/jumbo v0, "com.samsung.sec.android"

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->AUTHORITY_MEMO:Ljava/lang/String;

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->AUTHORITY_MEMO:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "memo/id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->CONTENT_URI_MEMO:Landroid/net/Uri;

    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->AUTHORITY_MEMO:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/memo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->UPDATE_CONTENT_URI_MEMO:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;)V
    .locals 4
    .param p1, "appType"    # Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

    .prologue
    const/4 v3, 0x1

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-object p1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->appType:Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

    .line 98
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 99
    .local v0, "context":Landroid/content/Context;
    iput-boolean v3, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->isBroadcast:Z

    .line 100
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo$1;->$SwitchMap$com$vlingo$core$internal$dialogmanager$util$IntegratedAppInfoInterface$IntegrateAppType:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 163
    :goto_0
    return-void

    .line 102
    :pswitch_0
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->isInstalled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 103
    const-string/jumbo v1, "com.samsung.android.intent.action.MEMO_SERVICE"

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->intentNameCreate:Ljava/lang/String;

    .line 104
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->CONTENT_URI_KMEMO:Landroid/net/Uri;

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->contentProviderUri:Landroid/net/Uri;

    .line 105
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->UPDATE_CONTENT_URI_KMEMO:Landroid/net/Uri;

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->updateContentProviderUri:Landroid/net/Uri;

    .line 106
    const-string/jumbo v1, "com.samsung.android.intent.action.MEMO_SERVICE"

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->intentNameDelete:Ljava/lang/String;

    .line 107
    iput-boolean v3, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->isBroadcast:Z

    goto :goto_0

    .line 110
    :cond_0
    const-string/jumbo v1, "com.sec.android.app.snotebook"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/ApplicationUtil;->checkApplicationExists(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 111
    const-string/jumbo v1, "com.sec.android.app.snotebook.MAKE_NEW_NOTE"

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->intentNameCreate:Ljava/lang/String;

    .line 112
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->CONTENT_URI_SNOTE:Landroid/net/Uri;

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->contentProviderUri:Landroid/net/Uri;

    .line 113
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->UPDATE_CONTENT_URI_SNOTE:Landroid/net/Uri;

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->updateContentProviderUri:Landroid/net/Uri;

    goto :goto_0

    .line 116
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;->isInstalled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 117
    const-string/jumbo v1, "com.sec.android.memo.CREATE_TMEMO"

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->intentNameCreate:Ljava/lang/String;

    .line 118
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->CONTENT_URI_SMEMO2:Landroid/net/Uri;

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->contentProviderUri:Landroid/net/Uri;

    .line 119
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->UPDATE_CONTENT_URI_SMEMO2:Landroid/net/Uri;

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->updateContentProviderUri:Landroid/net/Uri;

    .line 120
    const-string/jumbo v1, "com.sec.android.memo.DELETE_ID"

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->intentNameDelete:Ljava/lang/String;

    .line 121
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->isBroadcast:Z

    goto :goto_0

    .line 124
    :cond_2
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->isInstalled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 125
    const-string/jumbo v1, "com.sec.android.memo.CREATE_TMEMO"

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->intentNameCreate:Ljava/lang/String;

    .line 126
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->CONTENT_URI_SNOTE2:Landroid/net/Uri;

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->contentProviderUri:Landroid/net/Uri;

    .line 127
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->UPDATE_CONTENT_URI_SNOTE2:Landroid/net/Uri;

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->updateContentProviderUri:Landroid/net/Uri;

    .line 128
    const-string/jumbo v1, "com.sec.android.memo.DELETE_ID"

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->intentNameDelete:Ljava/lang/String;

    .line 129
    iput-boolean v3, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->isBroadcast:Z

    goto :goto_0

    .line 134
    :cond_3
    const-string/jumbo v1, "com.sec.android.widgetapp.diotek.smemo.MAKE_NEW_MEMO"

    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->isIntentAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string/jumbo v1, "com.sec.android.widgetapp.diotek.smemo"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/util/ApplicationUtil;->checkApplicationExists(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 135
    const-string/jumbo v1, "com.sec.android.app.memo.Memo"

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->execName:Ljava/lang/String;

    .line 136
    const-string/jumbo v1, "com.sec.android.app.memo"

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->execPackage:Ljava/lang/String;

    .line 137
    const-string/jumbo v1, "com.sec.android.app.memo.MAKE_NEW_MEMO"

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->intentNameCreate:Ljava/lang/String;

    .line 138
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->CONTENT_URI_MEMO:Landroid/net/Uri;

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->contentProviderUri:Landroid/net/Uri;

    .line 139
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->UPDATE_CONTENT_URI_MEMO:Landroid/net/Uri;

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->updateContentProviderUri:Landroid/net/Uri;

    goto/16 :goto_0

    .line 146
    :cond_4
    const-string/jumbo v1, "com.sec.android.widgetapp.q1_penmemo.MemoListActivity"

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->execName:Ljava/lang/String;

    .line 147
    const-string/jumbo v1, "com.sec.android.widgetapp.diotek.smemo"

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->execPackage:Ljava/lang/String;

    .line 148
    const-string/jumbo v1, "com.sec.android.widgetapp.diotek.smemo.MAKE_NEW_MEMO"

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->intentNameCreate:Ljava/lang/String;

    .line 149
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->CONTENT_URI_SMEMO:Landroid/net/Uri;

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->contentProviderUri:Landroid/net/Uri;

    .line 150
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->UPDATE_CONTENT_URI_SMEMO:Landroid/net/Uri;

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->updateContentProviderUri:Landroid/net/Uri;

    goto/16 :goto_0

    .line 156
    :pswitch_1
    const-string/jumbo v1, "com.sec.android.app.fm.widget.on"

    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->intentNameStart:Ljava/lang/String;

    goto/16 :goto_0

    .line 100
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private isIntentAvailable(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "action"    # Ljava/lang/String;

    .prologue
    .line 291
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 292
    .local v0, "intent":Landroid/content/Intent;
    iget-boolean v1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->isBroadcast:Z

    if-eqz v1, :cond_0

    .line 293
    invoke-static {p1, v0}, Lcom/vlingo/sdk/internal/util/PackageUtil;->canHandleBroadcastIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    .line 295
    :goto_0
    return v1

    :cond_0
    invoke-static {p1, v0}, Lcom/vlingo/sdk/internal/util/PackageUtil;->canHandleIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method public getAppType()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->appType:Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

    return-object v0
.end method

.method public getContentProviderUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->contentProviderUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getExecName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->execName:Ljava/lang/String;

    return-object v0
.end method

.method public getExecPackage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->execPackage:Ljava/lang/String;

    return-object v0
.end method

.method public getIntentNameCreate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->intentNameCreate:Ljava/lang/String;

    return-object v0
.end method

.method public getIntentNameDelete()Ljava/lang/String;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->intentNameDelete:Ljava/lang/String;

    return-object v0
.end method

.method public getIntentNameStart()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->intentNameStart:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateContentProviderUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->updateContentProviderUri:Landroid/net/Uri;

    return-object v0
.end method

.method public isBroadcast()Z
    .locals 1

    .prologue
    .line 270
    iget-boolean v0, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->isBroadcast:Z

    return v0
.end method

.method public isSNote()Z
    .locals 2

    .prologue
    .line 278
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 279
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v1, "com.sec.android.app.snotebook.MAKE_NEW_NOTE"

    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->isIntentAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public setAppType(Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;)V
    .locals 0
    .param p1, "appType"    # Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->appType:Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

    .line 179
    return-void
.end method

.method public setBroadcast(Z)V
    .locals 0
    .param p1, "broadcast"    # Z

    .prologue
    .line 287
    iput-boolean p1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->isBroadcast:Z

    .line 288
    return-void
.end method

.method public setContentProviderUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "contentProviderUri"    # Landroid/net/Uri;

    .prologue
    .line 226
    iput-object p1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->contentProviderUri:Landroid/net/Uri;

    .line 227
    return-void
.end method

.method public setExecName(Ljava/lang/String;)V
    .locals 0
    .param p1, "execName"    # Ljava/lang/String;

    .prologue
    .line 194
    iput-object p1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->execName:Ljava/lang/String;

    .line 195
    return-void
.end method

.method public setExecPackage(Ljava/lang/String;)V
    .locals 0
    .param p1, "execPackage"    # Ljava/lang/String;

    .prologue
    .line 210
    iput-object p1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->execPackage:Ljava/lang/String;

    .line 211
    return-void
.end method

.method public setIntentNameCreate(Ljava/lang/String;)V
    .locals 0
    .param p1, "intentNameCreate"    # Ljava/lang/String;

    .prologue
    .line 262
    iput-object p1, p0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;->intentNameCreate:Ljava/lang/String;

    .line 263
    return-void
.end method
