.class Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;
.super Ljava/lang/Object;
.source "SettingsInitializer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Setting"
.end annotation


# instance fields
.field private contentObserver:Landroid/database/ContentObserver;

.field private final contentProviderPath:Ljava/lang/String;

.field private final firstRunInitOnly:Z

.field private final name:Ljava/lang/String;

.field private final type:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;
    .param p3, "firstRunInitOnly"    # Z

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->name:Ljava/lang/String;

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "SETTINGS/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentProviderPath:Ljava/lang/String;

    .line 45
    iput-object p2, p0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->type:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    .line 46
    iput-boolean p3, p0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->firstRunInitOnly:Z

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->firstRunInitOnly:Z

    return v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentProviderPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->name:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->type:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Landroid/database/ContentObserver;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentObserver:Landroid/database/ContentObserver;

    return-object v0
.end method

.method static synthetic access$402(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;Landroid/database/ContentObserver;)Landroid/database/ContentObserver;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;
    .param p1, "x1"    # Landroid/database/ContentObserver;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentObserver:Landroid/database/ContentObserver;

    return-object p1
.end method
