.class public final Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;
.super Lcom/vlingo/core/internal/memo/MemoUtil;
.source "KMemoUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/memo/IMemoUtil;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;,
        Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;
    }
.end annotation


# static fields
.field private static final KEY_CONTENT:Ljava/lang/String; = "content"

.field private static final KEY_TITLE:Ljava/lang/String; = "title"

.field public static final MEMO_BASE_URI:Landroid/net/Uri;

.field private static final MEMO_DATA:Ljava/lang/String; = "_data"

.field private static final MEMO_ID:Ljava/lang/String; = "_id"

.field private static final MEMO_LASTMODIFIEDAT:Ljava/lang/String; = "lastModifiedAt"

.field private static final MEMO_PROJECTION:[Ljava/lang/String;

.field public static final MEMO_SERVICE_ACTION:Ljava/lang/String; = "com.samsung.android.intent.action.MEMO_SERVICE"

.field private static final MEMO_STRIPPEDCONTENT:Ljava/lang/String; = "strippedContent"

.field private static final MEMO_TITLE:Ljava/lang/String; = "title"

.field private static final MEMO_VRFILEUUID:Ljava/lang/String; = "vrfileuuid"

.field private static final SORT_ORDER:Ljava/lang/String; = "lastModifiedAt ASC"

.field private static instance:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHolder:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;

.field private mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

.field private mSvcConnection:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 39
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->instance:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;

    .line 228
    const-string/jumbo v0, "content://com.samsung.android.memo/memo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->MEMO_BASE_URI:Landroid/net/Uri;

    .line 241
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "lastModifiedAt"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "strippedContent"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "vrfileuuid"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->MEMO_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Lcom/vlingo/core/internal/memo/MemoUtil;-><init>()V

    .line 258
    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    .line 259
    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mContext:Landroid/content/Context;

    .line 260
    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mListener:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;

    .line 262
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mHolder:Ljava/util/List;

    .line 264
    new-instance v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$1;-><init>(Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;)V

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mSvcConnection:Landroid/content/ServiceConnection;

    .line 47
    return-void
.end method

.method static synthetic access$002(Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;Lcom/samsung/android/app/memo/MemoServiceIF;)Lcom/samsung/android/app/memo/MemoServiceIF;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;
    .param p1, "x1"    # Lcom/samsung/android/app/memo/MemoServiceIF;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    return-object p1
.end method

.method static synthetic access$100(Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->createPendingMemos()V

    return-void
.end method

.method private createPendingMemos()V
    .locals 7

    .prologue
    .line 301
    iget-object v6, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mHolder:Ljava/util/List;

    monitor-enter v6

    .line 303
    :try_start_0
    iget-object v5, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mHolder:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 304
    .local v2, "holder":Landroid/os/Bundle;
    const-string/jumbo v5, "title"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 305
    .local v4, "title":Ljava/lang/String;
    const-string/jumbo v5, "content"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 307
    .local v0, "content":Ljava/lang/String;
    :try_start_1
    invoke-direct {p0, v4, v0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->newMemo1(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    iget-object v5, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mHolder:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 309
    :catch_0
    move-exception v1

    .line 310
    .local v1, "e":Landroid/os/RemoteException;
    :try_start_2
    iget-object v5, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mListener:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;

    if-eqz v5, :cond_0

    .line 311
    iget-object v5, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mListener:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;

    invoke-interface {v5, v4, v0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;->onError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 315
    .end local v0    # "content":Ljava/lang/String;
    .end local v1    # "e":Landroid/os/RemoteException;
    .end local v2    # "holder":Landroid/os/Bundle;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "title":Ljava/lang/String;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    .line 314
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_3
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->unbindService()V

    .line 315
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 316
    return-void
.end method

.method private getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static getInstance()Lcom/vlingo/core/internal/memo/IMemoUtil;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->instance:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;

    invoke-direct {v0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;-><init>()V

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->instance:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;

    .line 53
    :cond_0
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->instance:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;

    return-object v0
.end method

.method private getMemo(Landroid/database/Cursor;Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "indices"    # Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;

    .prologue
    .line 165
    new-instance v0, Lcom/vlingo/core/internal/memo/Memo;

    invoke-direct {v0}, Lcom/vlingo/core/internal/memo/Memo;-><init>()V

    .line 166
    .local v0, "memo":Lcom/vlingo/core/internal/memo/Memo;
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;->KEY_TITLE_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setTitle(Ljava/lang/String;)V

    .line 167
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;->KEY_CONTENT_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setContent(Ljava/lang/String;)V

    .line 168
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;->KEY_TEXT_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setText(Ljava/lang/String;)V

    .line 171
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;->KEY_ID_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setId(I)V

    .line 172
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;->KEY_DATE_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setDate(Ljava/lang/String;)V

    .line 174
    return-object v0
.end method

.method private getUriFromId(J)Landroid/net/Uri;
    .locals 2
    .param p1, "id"    # J

    .prologue
    .line 178
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->getUpdateContentProviderUri()Landroid/net/Uri;

    move-result-object v0

    .line 182
    .local v0, "contentUri":Landroid/net/Uri;
    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static isInstalled()Z
    .locals 4

    .prologue
    .line 57
    const/4 v1, 0x0

    .line 58
    .local v1, "ret":Z
    const-string/jumbo v2, "com.samsung.android.app.memo"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v0

    .line 60
    .local v0, "kmemo_installed":Z
    if-eqz v0, :cond_0

    .line 63
    const/4 v1, 0x1

    .line 66
    :cond_0
    return v1
.end method

.method private newMemo1(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 289
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    invoke-interface {v0}, Lcom/samsung/android/app/memo/MemoServiceIF;->createNew()Ljava/lang/String;

    .line 290
    if-eqz p1, :cond_0

    .line 291
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    invoke-interface {v0, p1}, Lcom/samsung/android/app/memo/MemoServiceIF;->setTitle(Ljava/lang/String;)V

    .line 292
    :cond_0
    if-eqz p2, :cond_1

    .line 293
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    invoke-interface {v0, p2}, Lcom/samsung/android/app/memo/MemoServiceIF;->appendText(Ljava/lang/String;)V

    .line 294
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    invoke-interface {v0}, Lcom/samsung/android/app/memo/MemoServiceIF;->saveCurrent()J

    .line 295
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mListener:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;

    if-eqz v0, :cond_2

    .line 296
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mListener:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;

    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    invoke-interface {v1}, Lcom/samsung/android/app/memo/MemoServiceIF;->getUriWithId()Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;->onCreated(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    :cond_2
    return-void
.end method


# virtual methods
.method public bindService()V
    .locals 4

    .prologue
    .line 323
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->isServiceBinded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 324
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.samsung.android.intent.action.MEMO_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 325
    .local v0, "intent":Landroid/content/Intent;
    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mSvcConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 329
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 327
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->createPendingMemos()V

    goto :goto_0
.end method

.method public deleteMemo(Landroid/content/Context;J)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    .line 128
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 129
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    invoke-direct {p0, p2, p3}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->getUriFromId(J)Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 130
    invoke-direct {p0, p2, p3}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->getUriFromId(J)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 131
    .local v1, "rowsDeleted":I
    const/4 v3, 0x1

    if-eq v1, v3, :cond_0

    .line 132
    new-instance v3, Lcom/vlingo/core/internal/memo/MemoUtilException;

    const-string/jumbo v4, "Error in deleting a memo."

    invoke-direct {v3, v4}, Lcom/vlingo/core/internal/memo/MemoUtilException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Lcom/vlingo/core/internal/memo/MemoUtilException; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    .end local v1    # "rowsDeleted":I
    :catch_0
    move-exception v2

    .line 138
    .local v2, "sux":Lcom/vlingo/core/internal/memo/MemoUtilException;
    const-string/jumbo v3, "MemoUtilException:"

    invoke-virtual {v2}, Lcom/vlingo/core/internal/memo/MemoUtilException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-virtual {v2}, Lcom/vlingo/core/internal/memo/MemoUtilException;->printStackTrace()V

    .line 140
    throw v2

    .line 142
    .end local v2    # "sux":Lcom/vlingo/core/internal/memo/MemoUtilException;
    .restart local v0    # "contentResolver":Landroid/content/ContentResolver;
    :cond_0
    return-void
.end method

.method public getCreateMemoAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    const-string/jumbo v0, "android.intent.action.VIEW"

    return-object v0
.end method

.method public getListener()Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mListener:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;

    return-object v0
.end method

.method protected getMemo(Landroid/database/Cursor;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 153
    new-instance v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;

    invoke-direct {v0, p1}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;-><init>(Landroid/database/Cursor;)V

    .line 154
    .local v0, "indices":Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;
    invoke-direct {p0, p1, v0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->getMemo(Landroid/database/Cursor;Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;)Lcom/vlingo/core/internal/memo/Memo;

    move-result-object v1

    return-object v1
.end method

.method public getMostRecentlyCreatedMemo(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->listRecentMemos()Landroid/database/Cursor;

    move-result-object v0

    .line 103
    .local v0, "cursorMemo":Landroid/database/Cursor;
    const/4 v1, 0x0

    .line 104
    .local v1, "memo":Lcom/vlingo/core/internal/memo/Memo;
    if-eqz v0, :cond_0

    .line 106
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 107
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->getMemo(Landroid/database/Cursor;)Lcom/vlingo/core/internal/memo/Memo;

    move-result-object v1

    .line 108
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 110
    :cond_0
    return-object v1
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->MEMO_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method public getViewMemoAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    const-string/jumbo v0, "android.intent.action.VIEW"

    return-object v0
.end method

.method public isServiceBinded()Z
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public listRecentMemos()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 353
    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->MEMO_BASE_URI:Landroid/net/Uri;

    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->MEMO_PROJECTION:[Ljava/lang/String;

    const-string/jumbo v5, "lastModifiedAt DESC"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public newMemo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;

    .prologue
    .line 278
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 279
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    const-string/jumbo v1, "content"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    iget-object v2, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mHolder:Ljava/util/List;

    monitor-enter v2

    .line 282
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mHolder:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 283
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->bindService()V

    .line 286
    return-void

    .line 283
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public openMemo(J)V
    .locals 2
    .param p1, "_id"    # J

    .prologue
    .line 357
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 358
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->MEMO_BASE_URI:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 359
    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 360
    return-void
.end method

.method public saveMemoData(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 91
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->setContext(Landroid/content/Context;)V

    .line 92
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p2}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->newMemo(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    return-void
.end method

.method public searchMemos(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/memo/Memo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "content"

    aput-object v2, v0, v1

    .line 161
    .local v0, "fields":[Ljava/lang/String;
    const-string/jumbo v1, "lastModifiedAt ASC"

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->searchMemos(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 341
    iput-object p1, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mContext:Landroid/content/Context;

    .line 342
    return-void
.end method

.method public setListener(Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;)V
    .locals 0
    .param p1, "listener"    # Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;

    .prologue
    .line 349
    iput-object p1, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mListener:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoCreationListner;

    .line 350
    return-void
.end method

.method public unbindService()V
    .locals 2

    .prologue
    .line 332
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;

    .line 333
    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mSvcConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 334
    return-void
.end method

.method public updateMemo(Landroid/content/Context;Lcom/vlingo/core/internal/memo/Memo;Lcom/vlingo/core/internal/memo/Memo;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "originalMemo"    # Lcom/vlingo/core/internal/memo/Memo;
    .param p3, "changedMemo"    # Lcom/vlingo/core/internal/memo/Memo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    .line 120
    return-void
.end method
